<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = [
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
            new JMS\SerializerBundle\JMSSerializerBundle(),
            new FOS\RestBundle\FOSRestBundle(),
            new HTEC\BaseModelBundle\BaseModelBundle(),
            new HTEC\BaseAPIBundle\BaseAPIBundle(),
            new HTEC\SmartMeteringModelBundle\SmartMeteringModelBundle(),
            new HTEC\SmartMeteringAdminPanelAPIBundle\SmartMeteringAdminPanelAPIBundle(),
            new HTEC\SinglePageClientBundle\SinglePageClientBundle(),
            new HTEC\SmartMeteringAdminPanelBundle\SmartMeteringAdminPanelBundle(),
            new Symfony\Bundle\AsseticBundle\AsseticBundle(),
            new HTEC\SmartMeteringUtilityAPIBundle\SmartMeteringUtilityAPIBundle(),
        ];

        if (in_array($this->getEnvironment(), ['dev', 'test'], true)) {
            $bundles[] = new Symfony\Bundle\DebugBundle\DebugBundle();
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
        }

        return $bundles;
    }

    public function getRootDir()
    {
        return __DIR__;
    }

    public function getCacheDir()
    {
        if($this->getEnvironment() === 'dev')
        {
            return dirname('/var/tmp/smart-metering/cache/'.$this->getEnvironment());
        }
        else
        {
            return dirname(__DIR__).'/var/cache/'.$this->getEnvironment();
        }
        
    }

    public function getLogDir()
    {
        if($this->getEnvironment() === 'dev')
        {
            return '/var/tmp/smart-metering/logs';
        }
        else
        {
            return dirname(__DIR__).'/var/logs';
        }
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load($this->getRootDir().'/config/config_'.$this->getEnvironment().'.yml');
    }
}
