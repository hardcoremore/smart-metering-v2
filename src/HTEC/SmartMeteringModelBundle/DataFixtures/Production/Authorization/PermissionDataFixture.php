<?php

namespace HTEC\SmartMeteringModelBundle\DataFixtures\Production\Authorization;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use HTEC\BaseModelBundle\DataFixtures\BaseDataFixture;

use HTEC\SmartMeteringModelBundle\Entity\Permission;

class PermissionDataFixture extends BaseDataFixture
{
    const USER_MANAGEMENT_GROUP = 'User Management';
    const REGION_MANAGEMENT_GROUP = 'Region Management';
    const INVETORY_MANAGEMENT_GROUP = 'Invetory Management';
    const CONSUMPTION_MANAGEMENT_GROUP = 'Consumption Management';

    /**
     * {@inheritDoc}
     */
    public function load(InputInterface $input, OutputInterface $output)
    {


        /********************
         * USER MANAGEMENT
         ********************/
        $p = $this->createPermission(
            'Role',
            'Role',
            'HTEC\SmartMeteringModelBundle\Entity\Role',
            array('create', 'view', 'edit', 'delete'),
            self::USER_MANAGEMENT_GROUP
        );

        $this->getEntityManager()->persist($p);



        $p = $this->createPermission(
            'AdminPanelUser',
            'Admin Panel User',
            'HTEC\SmartMeteringModelBundle\Entity\AdminPanelUser',
            array('create', 'view', 'edit', 'delete', 'changePassword'),
            self::USER_MANAGEMENT_GROUP
        );

        $this->getEntityManager()->persist($p);



        /********************
         * REGION MANAGEMENT
         ********************/
        $p = $this->createPermission(
            'City',
            'City',
            'HTEC\SmartMeteringModelBundle\Entity\City',
            array('create', 'view', 'edit', 'delete'),
            self::REGION_MANAGEMENT_GROUP
        );

        $this->getEntityManager()->persist($p);



        $p = $this->createPermission(
            'BusinessDistrict',
            'Business District',
            'HTEC\SmartMeteringModelBundle\Entity\BusinessDistrict',
            array('create', 'view', 'edit', 'delete'),
            self::REGION_MANAGEMENT_GROUP
        );

        $this->getEntityManager()->persist($p);



        $p = $this->createPermission(
            'Customer',
            'Customer',
            'HTEC\SmartMeteringModelBundle\Entity\Customer',
            array('view'),
            self::REGION_MANAGEMENT_GROUP
        );

        $this->getEntityManager()->persist($p);


        /***********************
         * INVENTORY MANAGEMENT
         ***********************/
        $p = $this->createPermission(
            'InjectionSubstation',
            'Injection Substation',
            'HTEC\SmartMeteringModelBundle\Entity\InjectionSubstation',
            array('create', 'view', 'edit', 'delete', 'decommission'),
            self::INVETORY_MANAGEMENT_GROUP
        );

        $this->getEntityManager()->persist($p);



        $p = $this->createPermission(
            'PowerTransformer',
            'Power Transformer',
            'HTEC\SmartMeteringModelBundle\Entity\PowerTransformer',
            array('create', 'view', 'edit', 'delete'),
            self::INVETORY_MANAGEMENT_GROUP
        );

        $this->getEntityManager()->persist($p);



        $p = $this->createPermission(
            'ElectricPole',
            'Electric Pole',
            'HTEC\SmartMeteringModelBundle\Entity\ElectricPole',
            array('create', 'view', 'edit', 'delete'),
            self::INVETORY_MANAGEMENT_GROUP
        );

        $this->getEntityManager()->persist($p);



        $p = $this->createPermission(
            'DistributionTransformer',
            'Distribution Transformer',
            'HTEC\SmartMeteringModelBundle\Entity\DistributionTransformer',
            array('create', 'view', 'edit', 'delete'),
            self::INVETORY_MANAGEMENT_GROUP
        );

        $this->getEntityManager()->persist($p);



        $p = $this->createPermission(
            'Feeder',
            'Feeder',
            'HTEC\SmartMeteringModelBundle\Entity\Feeder',
            array('create', 'view', 'edit', 'delete'),
            self::INVETORY_MANAGEMENT_GROUP
        );

        $this->getEntityManager()->persist($p);



        $p = $this->createPermission(
            'Meter',
            'Meter',
            'HTEC\SmartMeteringModelBundle\Entity\Meter',
            array('create', 'view', 'edit', 'delete'),
            self::INVETORY_MANAGEMENT_GROUP
        );

        $this->getEntityManager()->persist($p);



        $p = $this->createPermission(
            'CustomerNetworkData',
            'Customer Network Data',
            'HTEC\SmartMeteringModelBundle\Entity\CustomerNetworkData',
            array('create', 'view', 'edit', 'delete'),
            self::INVETORY_MANAGEMENT_GROUP
        );

        $this->getEntityManager()->persist($p);



        /**************************
         * CONSUMPTION MANAGEMENT
         **************************/
        $p = $this->createPermission(
            'PowerSource',
            'Power Source',
            'HTEC\SmartMeteringModelBundle\Entity\PowerSource',
            array('create', 'view', 'edit', 'delete'),
            self::CONSUMPTION_MANAGEMENT_GROUP
        );

        $this->getEntityManager()->persist($p);



        $p = $this->createPermission(
            'PowerPurchase',
            'Power Purchase',
            'HTEC\SmartMeteringModelBundle\Entity\PowerPurchase',
            array('create', 'view', 'edit', 'delete', 'approve'),
            self::CONSUMPTION_MANAGEMENT_GROUP
        );

        $this->getEntityManager()->persist($p);

        

        $p = $this->createPermission(
            'PowerForecast',
            'Power Forecast',
            'HTEC\SmartMeteringModelBundle\Entity\PowerForecast',
            array('create', 'view'),
            self::CONSUMPTION_MANAGEMENT_GROUP
        );

        $this->getEntityManager()->persist($p);

        $p = $this->createPermission(
            'PowerAllocate',
            'Power Allocate',
            'HTEC\SmartMeteringModelBundle\Entity\PowerAllocate',
            array('create', 'view', 'edit', 'delete'),
            self::CONSUMPTION_MANAGEMENT_GROUP
        );

        $this->getEntityManager()->persist($p);




        $p = $this->createPermission(
            'PowerSchedule',
            'Power Schedule',
            'HTEC\SmartMeteringModelBundle\Entity\PowerSchedule',
            array('create', 'view', 'edit', 'delete', 'approve'),
            self::CONSUMPTION_MANAGEMENT_GROUP
        );

        $this->getEntityManager()->persist($p);



        $p = $this->createPermission(
            'PowerActivateDeactivate',
            'Power Activate Deactivate',
            'HTEC\SmartMeteringModelBundle\Entity\PowerActivateDeactivate',
            array('create', 'view'),
            self::CONSUMPTION_MANAGEMENT_GROUP
        );

        $this->getEntityManager()->persist($p);

        

        $p = $this->createPermission(
            'PowerAviability',
            'Power Aviability',
            'HTEC\SmartMeteringModelBundle\Entity\PowerAviability',
            array('view'),
            self::CONSUMPTION_MANAGEMENT_GROUP
        );

        $this->getEntityManager()->persist($p);

        $p = $this->createPermission(
            'FeederLoad',
            'Feeder Load',
            'HTEC\SmartMeteringModelBundle\Entity\FeederLoad',
            array('create', 'view', 'edit'),
            self::CONSUMPTION_MANAGEMENT_GROUP
        );

        $this->getEntityManager()->persist($p);




        $p = $this->createPermission(
            'Tariff',
            'Tariff',
            'HTEC\SmartMeteringModelBundle\Entity\Tariff',
            array('create', 'view', 'edit', 'delete'),
            self::CONSUMPTION_MANAGEMENT_GROUP
        );

        $this->getEntityManager()->persist($p);



        $p = $this->createPermission(
            'Appliance',
            'Appliance',
            'HTEC\SmartMeteringModelBundle\Entity\Appliance',
            array('create', 'view', 'edit', 'delete'),
            self::CONSUMPTION_MANAGEMENT_GROUP
        );

        $this->getEntityManager()->persist($p);



        $p = $this->createPermission(
            'OfflineMeterReading',
            'Offline Meter Reading',
            'HTEC\SmartMeteringModelBundle\Entity\OfflineMeterReading',
            array('view', 'approve'),
            self::CONSUMPTION_MANAGEMENT_GROUP
        );
        $this->getEntityManager()->persist($p);


        $p = $this->createPermission(
            'OnlineMeterReading',
            'Online Meter Reading',
            'HTEC\SmartMeteringModelBundle\Entity\OnlineMeterReading',
            array('view', 'approve'),
            self::CONSUMPTION_MANAGEMENT_GROUP
        );

        $this->getEntityManager()->persist($p);
        
        
        $p = $this->createPermission(
            'OfflinePrepaidVendReading',
            'Offline Prepaid Vend Reading',
            'HTEC\SmartMeteringModelBundle\Entity\OfflinePrepaidVendReading',
            array('view'),
            self::CONSUMPTION_MANAGEMENT_GROUP
        );

        $this->getEntityManager()->persist($p);
        
        
        $p = $this->createPermission(
            'GridMeterReading',
            'Grid Meter Reading',
            'HTEC\SmartMeteringModelBundle\Entity\GridMeterReading',
            array('view'),
            self::CONSUMPTION_MANAGEMENT_GROUP
        );

        $this->getEntityManager()->persist($p);
        
        
        $p = $this->createPermission(
            'Promotion',
            'Promotions - Discounts or Rewards',
            'HTEC\SmartMeteringModelBundle\Entity\Promotion',
            array('view'),
            self::CONSUMPTION_MANAGEMENT_GROUP
        );

        $this->getEntityManager()->persist($p);


        $p = $this->createPermission(
            'EstimatedConsumption',
            'Estimated Consumption',
            'HTEC\SmartMeteringModelBundle\Entity\EstimatedConsumption',
            array('create', 'view', 'edit', 'delete'),
            self::CONSUMPTION_MANAGEMENT_GROUP
        );

        $this->getEntityManager()->persist($p);



        $p = $this->createPermission(
            'EstimatedLoad',
            'Estimated Load',
            'HTEC\SmartMeteringModelBundle\Entity\EstimatedLoad',
            array('create', 'view', 'edit', 'delete'),
            self::CONSUMPTION_MANAGEMENT_GROUP
        );

        $this->getEntityManager()->persist($p);

         $p = $this->createPermission(
            'EstimatedLoadAppliance',
            'Estimated Load Appliance',
            'HTEC\SmartMeteringModelBundle\Entity\EstimatedLoadAppliance',
            array('create', 'view', 'edit', 'delete'),
            self::CONSUMPTION_MANAGEMENT_GROUP
        );

        $this->getEntityManager()->persist($p);



        $p = $this->createPermission(
            'ApplianceType',
            'Appliance Type',
            'HTEC\SmartMeteringModelBundle\Entity\ApplianceType',
            array('create', 'view', 'edit', 'delete'),
            self::CONSUMPTION_MANAGEMENT_GROUP
        );

        $this->getEntityManager()->persist($p);


        $p = $this->createPermission(
            'CustomerCategory',
            'Customer Category',
            'HTEC\SmartMeteringModelBundle\Entity\CustomerCategory',
            array('create', 'view', 'edit', 'delete'),
            self::CONSUMPTION_MANAGEMENT_GROUP
        );

        $this->getEntityManager()->persist($p);



        $this->getEntityManager()->flush();
    }

    public function createPermission($identifier, $name, $domain, array $actions, $groupName = '')
    {
        $permission = new Permission();
        $permission->setIdentifier($identifier);
        $permission->setName($name);
        $permission->setDomain($domain);
        $permission->setActions($actions);
        $permission->setGroupName($groupName);

        return $permission;
    }
}
