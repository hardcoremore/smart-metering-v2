<?php

namespace HTEC\SmartMeteringModelBundle\DataFixtures\Production\Authorization;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use HTEC\BaseModelBundle\DataFixtures\BaseDataFixture;

use HTEC\SmartMeteringModelBundle\Entity\Role;

class RoleDataFixture extends BaseDataFixture
{
    /**
     * {@inheritDoc}
     */
    public function load(InputInterface $input, OutputInterface $output)
    {
        // get the super admin user
        $superAdminUser = $this->getEntityManager()
                               ->getRepository('SmartMeteringModelBundle:AdminPanelUser')
                               ->findOneById(1);

        $userRole = new Role();
        $userRole->setName('ROLE_USER');
        $userRole->setRole('ROLE_USER');
        $userRole->setOwner($superAdminUser);

        $this->getEntityManager()->persist($userRole);


        $adminRole = new Role();
        $adminRole->setName('ROLE_ADMIN');
        $adminRole->setRole('ROLE_ADMIN');
        $adminRole->setOwner($superAdminUser);
        
        $this->getEntityManager()->persist($adminRole);


        $superAdminRole = new Role();
        $superAdminRole->setName('ROLE_SUPER_ADMIN');
        $superAdminRole->setRole('ROLE_SUPER_ADMIN');
        $superAdminRole->setOwner($superAdminUser);

        $this->getEntityManager()->persist($superAdminRole);

        $superAdminUser->addRole($userRole);
        $superAdminUser->addRole($adminRole);
        $superAdminUser->addRole($superAdminRole);

        $this->getEntityManager()->flush();


        $availablePermissions = $this->getContainer()->get('htec.sm_model_bundle.model.permission')->readAll();

        $superAdminPermissions = [];

        foreach ($availablePermissions AS $permission)
        {
            $superAdminPermissions[] = [
                'id' => $permission->getIdentifier(),
                'class' => $permission->getDomain(),
                'actions' => $permission->getActions()
            ];
        }

        $this->updateRolePermissions($superAdminPermissions, $superAdminRole);



        $adminPermissions = [];

        foreach ($superAdminPermissions AS $permission)
        {
            if($permission['id'] !== 'Role' && $permission['id'] !== 'AdminPanelUser')
            {
                $adminPermissions[] = $permission;
            }            
        }

        $this->updateRolePermissions($adminPermissions, $adminRole);

        $this->getEntityManager()->flush();
    }

    protected function updateRolePermissions(array $permissions, Role $role)
    {
        $aclProvider = $this->getContainer()->get('security.acl.provider');
        $roleModel = $this->getContainer()->get('htec.sm_model_bundle.model.role');

        for($i = 0, $len = count($permissions); $i < $len; $i++)
        {
            $roleModel->updateRolePermission($permissions[$i], $role, $aclProvider);
        }
    }
}
