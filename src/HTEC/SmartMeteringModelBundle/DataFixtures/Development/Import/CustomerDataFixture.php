<?php

namespace HTEC\SmartMeteringModelBundle\DataFixtures\Development\Import;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use HTEC\BaseModelBundle\DataFixtures\BaseDataFixture;

use HTEC\SmartMeteringModelBundle\Entity\Customer;

class CustomerDataFixture extends BaseDataFixture
{
    private $firstNames = array(
        'Bayo',
        'Taiwo',
        'Wumi',
        'Lukman',
        'Vladimir',
        'Damir',
        'Vladimir',
        'Dragomir',
        'Andrija',
        'Besther'
    );

    private $lastNames = array(
        'Okunowo',
        'Akinseye',
        'Ogheotuoma',
        'Ottun',
        'Trbovic',
        'Mehic',
        'Marinovic',
        'Krstic',
        'Bednarik',
        'Nwsou'
    );

    /**
     * {@inheritDoc}
     */
    public function load(InputInterface $input, OutputInterface $output)
    {
        $customer = null;

        $allBusinesDistricts = $availablePermissions = $this->getContainer()->get('htec.sm_model_bundle.model.business_district')->readAll();
        $allBusinessDistrictsCount = count($allBusinesDistricts);

        $allTariffs = $availablePermissions = $this->getContainer()->get('htec.sm_model_bundle.model.tariff')->readAll();
        $allTariffsCount = count($allTariffs);

        $randomBusinessDistrict = null;
        $randomTariff = null;

        for($i = 0, $len = count($this->firstNames); $i < $len; $i++)
        {
            $customer = new Customer();
            $customer->setName($this->firstNames[$i] . ' ' . $this->lastNames[$i]);
            $customer->setEmail($this->firstNames[$i] . '.' . $this->lastNames[$i] . '@email.com');

            $customer->setAccountNumber(mt_rand(1, 999999999));
            $customer->setAccountType('normal');

            $customer->setCustomerType('super');
            $customer->setCustomerStatus('active');

            $customer->setAddress('Test Address ' . ($i + 1));
            

            $randomBusinessDistrict = mt_rand(0, $allBusinessDistrictsCount -1);
            $customer->setBusinessDistrict($allBusinesDistricts[$randomBusinessDistrict]);

            $randomTariff = mt_rand(0, $allTariffsCount -1);
            $customer->setTariff($allTariffs[$randomTariff]);

            $this->getEntityManager()->persist($customer);

        }

        $this->getEntityManager()->flush();
    }
}
