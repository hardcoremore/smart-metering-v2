<?php

namespace HTEC\SmartMeteringModelBundle\DataFixtures\Development\Import;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use HTEC\BaseModelBundle\DataFixtures\BaseDataFixture;

use HTEC\SmartMeteringModelBundle\Entity\Tariff;

class TariffDataFixture extends BaseDataFixture
{
    /**
     * {@inheritDoc}
     */
    public function load(InputInterface $input, OutputInterface $output)
    {
        $this->createTariff('Residential 1', 'R1', mt_rand(1000, 99999999999) / 100, mt_rand(10, 500));
        $this->createTariff('Residential 2', 'R2', mt_rand(1000, 99999999999) / 100, mt_rand(10, 500));
        $this->createTariff('Residential 3', 'R3', mt_rand(1000, 99999999999) / 100, mt_rand(10, 500));
        $this->createTariff('Residential 4', 'R4', mt_rand(1000, 99999999999) / 100, mt_rand(10, 500));

        $this->createTariff('Commercial 1', 'C1', mt_rand(1000, 99999999999) / 100, mt_rand(10, 500));
        $this->createTariff('Commercial 2', 'C2', mt_rand(1000, 99999999999) / 100, mt_rand(10, 500));
        $this->createTariff('Commercial 3', 'C3', mt_rand(1000, 99999999999) / 100, mt_rand(10, 500));

        $this->createTariff('Industrial 1', 'D1', mt_rand(1000, 99999999999) / 100, mt_rand(10, 500));
        $this->createTariff('Industrial 2', 'D2', mt_rand(1000, 99999999999) / 100, mt_rand(10, 500));
        $this->createTariff('Industrial 3', 'D3', mt_rand(1000, 99999999999) / 100, mt_rand(10, 500));

        $this->createTariff('Street Lights 1', 'S1', mt_rand(1000, 99999999999) / 100, mt_rand(10, 500));
        $this->createTariff('Street Lights 2', 'S2', mt_rand(1000, 99999999999) / 100, mt_rand(10, 500));

        $this->getEntityManager()->flush();
    }

    protected function createTariff($name, $code, $price, $averageConsumptionPerDay)
    {
        $tariff = new Tariff();
        $tariff->setName($name);
        $tariff->setCode($code);
        $tariff->setPrice($price);
        $tariff->setAverageConsumptionPerDay($averageConsumptionPerDay);

        $this->getEntityManager()->persist($tariff);

        return $tariff;
    }
}
