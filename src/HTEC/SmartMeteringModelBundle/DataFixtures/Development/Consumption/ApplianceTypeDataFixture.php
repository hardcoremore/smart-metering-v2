<?php

namespace HTEC\SmartMeteringModelBundle\DataFixtures\Development\Consumption;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use HTEC\BaseModelBundle\DataFixtures\BaseDataFixture;

use HTEC\SmartMeteringModelBundle\Entity\ApplianceType;

class ApplianceTypeDataFixture extends BaseDataFixture
{
    static $NAMES = array(
        'Pressing Iron',
        'Light Bulb',
        'Computer',
        'Heater',
        'Water Cooler',
        'TV set',
        'Hi Fi Audio',
        'Air Conditioner',
        'Water Heater',
        'Stove',
        'Owen',
        'Laundry Washing Machine',
        'Dish Washing Machine',
        'Printer',
        'Factory Robot',
        'Industrial Cooker',
        'Socket Outlet 5 Amps',
        'Socket Outlet 16 Amps',
        'Socket Outlet 24 Amps',
        'Microwave',
        'Fridge',
        'Freezer',
        'Ceiling Fans'
    );


    /**
     * {@inheritDoc}
     */
    public function load(InputInterface $input, OutputInterface $output)
    {
        $allCustomerCategories = $this->getContainer()->get('htec.sm_model_bundle.model.customer_category')->readForSelect();

        $applianceType = null;

        $randomCustomerCategories = null;
        $randomCustomerCategoriesCount = 0;

        for($i = 0, $len = count(self::$NAMES); $i < $len; $i++)
        {
            $applianceType = new ApplianceType();
            $applianceType->setName(self::$NAMES[$i]);
            
            $randomCustomerCategoriesCount = mt_rand(1, 3);

            $randomCustomerCategories = array_rand($allCustomerCategories, $randomCustomerCategoriesCount);

            if(is_array($randomCustomerCategories))
            {
                foreach($randomCustomerCategories AS $value)
                {
                    $applianceType->addCustomerCategory($allCustomerCategories[$value]);
                }
            }
            else
            {
                $applianceType->addCustomerCategory($allCustomerCategories[$randomCustomerCategories]);
            }

            $this->getEntityManager()->persist($applianceType);
        }

        $this->getEntityManager()->flush();
    }
}
