<?php

namespace HTEC\SmartMeteringModelBundle\DataFixtures\Development\Consumption;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use HTEC\BaseModelBundle\DataFixtures\BaseDataFixture;

use HTEC\SmartMeteringModelBundle\Entity\CustomerCategory;

class CustomerCategoryDataFixture extends BaseDataFixture
{
    static $NAMES = array(
        'Residential',
        'Commercial',
        'Industrial',
        'Street Lights',
        'Heavy Industry',
        'Hospital',
        'Single Phase Residential',
        'Public Transportation (Trams, Electrical Busses)'
    );


    /**
     * {@inheritDoc}
     */
    public function load(InputInterface $input, OutputInterface $output)
    {
        $customerCategory = null;

        for($i = 0, $len = count(self::$NAMES); $i < $len; $i++)
        {
            $customerCategory = new CustomerCategory();
            $customerCategory->setName(self::$NAMES[$i]);
            $customerCategory->setDiversityFactor(mt_rand(1, 100) / 100);

            $this->getEntityManager()->persist($customerCategory);
        }

        $this->getEntityManager()->flush();
    }
}
