<?php

namespace HTEC\SmartMeteringModelBundle\DataFixtures\Development\Consumption;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use HTEC\BaseModelBundle\DataFixtures\BaseDataFixture;

use HTEC\SmartMeteringModelBundle\Entity\Appliance;

class ApplianceDataFixture extends BaseDataFixture
{
    static $NAMES = [
        'Sony',
        'LG',
        'Hewlett Packard',
        'Asus',
        'Samsung',
        'Carrier',
        'Pioneer',
        'Weg',
        'Vox',
        'Bosch',
        'Owen',
        'Schneider electric',
        'Universal',
        'Lexmark',
        'Nexus',
        'Rowenta',
        'Panasonic',
        'Phillips',
        'Legrand',
        'Beko',
        'Tesla',
        'Freezer',
        'Midea',
        'Dell',
        'Toshiba',
        'Micubishi',
        'Miele',
        'Ariston',
        'Scanfrost'
    ];

    static $MODELS = [
        'A', 'B', 'C', 'D', 'H', 'F', 'G', 'I', 'J', 'K', 'L', 'W', 'T'
    ];

    /**
     * {@inheritDoc}
     */
    public function load(InputInterface $input, OutputInterface $output)
    {
        $allApplianceTypes = $this->getContainer()->get('htec.sm_model_bundle.model.appliance_type')->readForSelect();
        $allApplianceTypesCount = count($allApplianceTypes);

        $modelsCount = count(self::$MODELS);

        $namesCount = count(self::$NAMES);

        $appliance = null;
        $applianceType = null;

        $randomApplianceTypeAppliancesCount = null;

        for($i = 0, $len = $allApplianceTypesCount; $i < $len; $i++)
        {
            $randomApplianceTypeAppliancesCount = mt_rand(3, 15);

            for($d = 0; $d < $randomApplianceTypeAppliancesCount; $d++)
            {
                $appliance = $allApplianceTypes[mt_rand(0, $allApplianceTypesCount-1)];

                $applianceType = new Appliance();
                $applianceType->setApplianceType($appliance);
                $applianceType->setConsumption(mt_rand(30, 3000));

                $applianceType->setMake(self::$NAMES[mt_rand(0, $namesCount-1)] . ' Model ' . self::$MODELS[mt_rand(0, $modelsCount - 1)] . ' ' . $d);

                $this->getEntityManager()->persist($applianceType);
            }
        }

        $this->getEntityManager()->flush();
    }
}
