<?php

namespace HTEC\SmartMeteringModelBundle\DataFixtures\Development\Inventory;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use HTEC\BaseModelBundle\DataFixtures\BaseDataFixture;

use HTEC\SmartMeteringModelBundle\Entity\Feeder;

class FeederDataFixture extends BaseDataFixture
{

    static $ADDRESS_NAMES = array(
        'Abiola Oriowo',
        'Ade John',
        'Ade Thomas',
        'Kareem Bamidele',
        'Joseph Umunnah Nwokolo',
        'Kazy Stevens',
        'Kehinde Martins',
        'Kenny Smith',
        'Sandra Mark',
        'Serah Mary Nelly',
        'Sola Bola',
        'Sola Ogunyemi',
        'Steve Reid',
        'Tami Johnson',
        'Taye Smith',
        'Temitope Liad',
        'Tobi Thomas',
        'Tom Parker',
        'Yemi Oguns',
        'Yinka Ismail',
        'Wayne Silver',
        'Wayne Faber',
        'Williams Oluwafemi',
        'Wale Sod',
        'Ufuoma George',
        'Tunde Adeyemo Ganiyu',
        'Tokunbo Williams',
        'Tola Diana',
        'Olaouwa Adebogun',
        'Olarenwaju Adaranijo'
    );

    static $NUMBER_OF_FEEDERS = 250;

    /**
     * {@inheritDoc}
     */
    public function load(InputInterface $input, OutputInterface $output)
    {
        $feeder = null;
        $type = '';
        $status = '';

        for($i = 0; $i < self::$NUMBER_OF_FEEDERS; $i++)
        {
            switch(mt_rand(1, 8))
            {
                case 1:
                    $status = Feeder::STATUS_LIVE;
                break;

                case 2:
                    $status = Feeder::STATUS_OFFLINE;
                break;
                case 2:
                    $status = Feeder::STATUS_REPAIRING;
                break;

                default:
                    $status = Feeder::STATUS_LIVE;
                break;
            }

            switch(mt_rand(1, 2))
            {
                case 1:
                    $type = Feeder::TYPE_LOW_VOLTAGE;
                break;

                case 2:
                    $type = Feeder::TYPE_HIGH_VOLTAGE;
                break;
            }

            $feeder = $this->createFeeder(
                'Feeder ' . ucfirst(str_replace('-', ' ' , $type)) .  ' ' . ($i + 1),
                $type,
                $status,
                mt_rand(999, 99999),
                $this->getRandomAddresses()
            );

            $this->getEntityManager()->persist($feeder);
        }

        $this->getEntityManager()->flush();
    }

    public function createFeeder(string $name, string $type, string $status, string $code, array $streetCoverage)
    {
        $feeder = new Feeder();
        $feeder->setName($name);
        $feeder->setType($type);
        $feeder->setStatus($status);
        $feeder->setCode($code);
        $feeder->setStreetCoverage($streetCoverage);

        return $feeder;
    }

    public function getRandomAddresses()
    {
        $addressCount = count(self::$ADDRESS_NAMES);
        $addressRandomNumber = mt_rand(1, 5);
        $addresses = [];

        for($i = 0; $i < $addressRandomNumber; $i++)
        {
            $addresses[] = self::$ADDRESS_NAMES[mt_rand(0, $addressCount - 1)];
        }

        return $addresses;
    }
}
