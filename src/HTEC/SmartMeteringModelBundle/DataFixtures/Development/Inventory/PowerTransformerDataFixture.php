<?php

namespace HTEC\SmartMeteringModelBundle\DataFixtures\Development\Inventory;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use HTEC\BaseModelBundle\DataFixtures\BaseDataFixture;

use HTEC\SmartMeteringModelBundle\Entity\PowerTransformer;

class PowerTransformerDataFixture extends BaseDataFixture
{

    protected $injectionSubstations;
    protected $highVoltageFeeders;
    protected $allPowerSourcesFeeders = [];
    protected $feederModel;

    /**
     * {@inheritDoc}
     */
    public function load(InputInterface $input, OutputInterface $output)
    {
        $this->feederModel = $this->getContainer()->get('htec.sm_model_bundle.model.feeder');
        $this->highVoltageFeeders = $this->feederModel->readForSelect(['type' => 'high-voltage']);

        $this->injectionSubstations = $this->getContainer()->get('htec.sm_model_bundle.model.injection_substation')->readAll();

        $powerSources = $this->getContainer()->get('htec.sm_model_bundle.model.power_source')->readAll();

        foreach($powerSources AS $value)
        {
            foreach($value->getOutputFeeders() AS $feeder)
            {
                $this->allPowerSourcesFeeders[] = $feeder->getId();
            }
        }

        $injectionSubstationsCount = count($this->injectionSubstations);

        $powerTransformersPerInjectionSubstationCount = 0;

        $injectionSubstation = null;

        for($i = 0; $i < $injectionSubstationsCount; $i++)
        {
            $powerTransformersPerInjectionSubstationCount = mt_rand(1, 5);

            $injectionSubstation = $this->injectionSubstations[$i];

            for($c = 0; $c < $powerTransformersPerInjectionSubstationCount; $c++)
            {
                $powerTransformer = new PowerTransformer();
                $powerTransformer->setInjectionSubstation($injectionSubstation);
                $powerTransformer->setName($injectionSubstation->getName() . ' Power Transformer ' . ($c + 1));
                $powerTransformer->setCode(mt_rand(99999,999999999999));
                $powerTransformer->setStatus('live');

                $this->addRandomOutputFeedersToPowerTransformer($powerTransformer, mt_rand(1, 5), $input, $output);

                $this->getEntityManager()->persist($powerTransformer);
            }
        }

        $this->getEntityManager()->flush();
    }

    protected function addRandomOutputFeedersToPowerTransformer(PowerTransformer $powerTransformer,
                                                                int $numberOfOutputFeedersTaAdd,
                                                                InputInterface $input,
                                                                OutputInterface $output)
    {
        $highVoltageFeedersCount = count($this->highVoltageFeeders);
        $outpuFeedersFoundCount = 0;
        $randomFeeder = null;
        $powerTransformerFeeders = [];

        while(true)
        {
            $randomFeeder = $this->highVoltageFeeders[mt_rand(0, $highVoltageFeedersCount - 1)];

            if($outpuFeedersFoundCount === $numberOfOutputFeedersTaAdd)
            {
                break;
            }
            else if(in_array($randomFeeder->getId(), $this->allPowerSourcesFeeders) === false && in_array($randomFeeder, $powerTransformerFeeders) === false)
            {
                $powerTransformerFeeders[] = $randomFeeder;
                $outpuFeedersFoundCount++;
            }

            if($outpuFeedersFoundCount > 10000)
            {
                break;
            }
        }

        foreach ($powerTransformerFeeders AS $feeder)
        {
            $powerTransformer->addOutputFeeder($feeder);
        }
    }
}
