<?php

namespace HTEC\SmartMeteringModelBundle\DataFixtures\Development\Inventory;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use HTEC\BaseModelBundle\DataFixtures\BaseDataFixture;

use HTEC\SmartMeteringModelBundle\Entity\InjectionSubstation;

class InjectionSubstationDataFixture extends BaseDataFixture
{

    protected $powerSources;

    /**
     * {@inheritDoc}
     */
    public function load(InputInterface $input, OutputInterface $output)
    {
        $businessDistricts = $this->getContainer()->get('htec.sm_model_bundle.model.business_district')->readAll();
        $this->powerSources = $this->getContainer()->get('htec.sm_model_bundle.model.power_source')->readAll();

        $powerSourcesCount = count($this->powerSources);

        $businessDistrictsCount = count($businessDistricts);

        $injectionSubstationsPerBusinessDistrictCount = null;
        $powerTransformersPerInjectionSubstationCount = null;

        $businessDistrict = null;
        $powerSource = null;

        for($i = 0; $i < $businessDistrictsCount; $i++)
        {
            $injectionSubstationsPerBusinessDistrictCount = mt_rand(1, 10);

            $businessDistrict = $businessDistricts[$i];

            for($c = 0; $c < $injectionSubstationsPerBusinessDistrictCount; $c++)
            {
                $injectionSubstation = new InjectionSubstation();
                $injectionSubstation->setBusinessDistrict($businessDistrict);
                $injectionSubstation->setName($businessDistrict->getName() . ' Injection Substation ' . ($c + 1));
                $injectionSubstation->setCode(mt_rand(99999,999999999999));
                $injectionSubstation->setAddress($businessDistrict->getName() . ' address ' . $i . '/' . $c);

                $injectionSubstation->setPowerSource($this->powerSources[mt_rand(0, $powerSourcesCount - 1)]);

                $this->getEntityManager()->persist($injectionSubstation);
            }
        }

        $this->getEntityManager()->flush();
    }
}
