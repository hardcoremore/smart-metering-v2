<?php

namespace HTEC\SmartMeteringModelBundle\DataFixtures\Development\Inventory;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use HTEC\BaseModelBundle\DataFixtures\BaseDataFixture;

use HTEC\SmartMeteringModelBundle\Entity\PowerSource;

class PowerSourceDataFixture extends BaseDataFixture
{
    /**
     * {@inheritDoc}
     */
    public function load(InputInterface $input, OutputInterface $output)
    {
        $highVoltageFeeders = $this->getContainer()->get('htec.sm_model_bundle.model.feeder')->readForSelect(['type' => 'high-voltage']);

        $highVoltageFeedersCount = count($highVoltageFeeders);

        $randomOutputFeedersCount = null;
        $randomOutputFeeders = null;
        $powerSource = null;

        for($i = 0; $i < 15; $i++)
        {
            $powerSource = new PowerSource();
            $powerSource->setName('Power source ' . ($i + 1));
            $powerSource->setCapacity(mt_rand(10, 500));
            $powerSource->setAddress('Power source address ' . $i);

            $randomOutputFeedersCount = mt_rand(1, 5);

            $randomOutputFeeders = array_rand($highVoltageFeeders, $randomOutputFeedersCount);

            if(is_array($randomOutputFeeders))
            {
                foreach($randomOutputFeeders AS $value)
                {
                    $powerSource->addOutputFeeder($highVoltageFeeders[$value]);
                }
            }
            else
            {
                $powerSource->addOutputFeeder($highVoltageFeeders[$randomOutputFeeders]);
            }

            $this->getEntityManager()->persist($powerSource);
        }

        $this->getEntityManager()->flush();
    }
}
