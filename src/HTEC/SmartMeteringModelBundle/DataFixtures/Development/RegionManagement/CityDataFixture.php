<?php

namespace HTEC\SmartMeteringModelBundle\DataFixtures\Development\RegionManagement;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use HTEC\BaseModelBundle\DataFixtures\BaseDataFixture;

use HTEC\SmartMeteringModelBundle\Entity\City;

class CityDataFixture extends BaseDataFixture
{

    private $cityNames = array(
        'Lagos',
        'Abuja',
        'Akure',
        'Calabar',
        'Ibadan',
        'Uyo',
        'Asaba',
        'Bauchi',
        'Dutse',
        'Eket',
        'Ikeja',
        'Jimeta',
        'Karu',
        'Lafia',
        'Zaria',
        'Yola',
        'Orlu',
        'Sokoto',
        'Ogaminana',
        'Warri',
        'Suleja'
    );

    /**
     * {@inheritDoc}
     */
    public function load(InputInterface $input, OutputInterface $output)
    {
        $city = null;

        for($i = 0, $len = count($this->cityNames); $i < $len; $i++)
        {
            $city = new City();

            $city->setName($this->cityNames[$i]);

            $this->getEntityManager()->persist($city);
        }

        $this->getEntityManager()->flush();
    }
}
