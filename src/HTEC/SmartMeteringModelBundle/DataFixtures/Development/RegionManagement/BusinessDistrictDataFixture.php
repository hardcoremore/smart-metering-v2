<?php

namespace HTEC\SmartMeteringModelBundle\DataFixtures\Development\RegionManagement;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use HTEC\BaseModelBundle\DataFixtures\BaseDataFixture;

use HTEC\SmartMeteringModelBundle\Entity\BusinessDistrict;

class BusinessDistrictDataFixture extends BaseDataFixture
{

    private $businessDistrictNames = array(
        'Super',
        'Space',
        'Central',
        'New',
        'Another',
        'Great',
        'Special',
        'Regular',
        'Normal',
        'Small',
        'Big',
        'Extra',
        'Large',
        'Power'
    );

    /**
     * {@inheritDoc}
     */
    public function load(InputInterface $input, OutputInterface $output)
    {
        $allCities = $availablePermissions = $this->getContainer()->get('htec.sm_model_bundle.model.city')->readAll();
        $allCitiesCount = count($allCities);

        $businessDistrict = null;

        for($i = 0, $len = count($this->businessDistrictNames); $i < $len; $i++)
        {
            $businessDistrict = new BusinessDistrict();

            $businessDistrict->setName($this->businessDistrictNames[$i] . ' unit');

            $city = $allCities[rand(0, $allCitiesCount - 1)];
 
            $businessDistrict->setCity($city);

            $businessDistrict->setManager('Manager ' . ($i + 1));
            $businessDistrict->setPhoneNumber(rand(10000000, 20000000));
        
            $businessDistrict->setAddress('Address ' . ($i + 1));

            $this->getEntityManager()->persist($businessDistrict);
        }

        $this->getEntityManager()->flush();
    }
}
