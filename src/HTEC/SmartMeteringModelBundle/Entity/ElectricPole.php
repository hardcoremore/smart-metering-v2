<?php

namespace HTEC\SmartMeteringModelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ElectricPole
 *
 * @ORM\Table(name="electric_poles")
 * @ORM\Entity(repositoryClass="HTEC\SmartMeteringModelBundle\Repository\ElectricPoleRepository")
 */
class ElectricPole
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=16)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=16)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=16)
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255, nullable=true)
     */
    private $address;

   /**
     * @ORM\ManyToOne(targetEntity="Feeder", inversedBy="electricPoles")
     * @ORM\JoinColumn(name="feeder_id", referencedColumnName="id")
     */
    private $feeder;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return ElectricPole
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return ElectricPole
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return ElectricPole
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

     /**
     * Set address
     *
     * @param string $address
     *
     * @return ElectricPole
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

     /**
     * Set feeder
     *
     * @param Feeder $feeder
     * @return ElectricPole
     */
    public function setFeeder(Feeder $feeder)
    {
        $this->feeder = $feeder;

        return $this;
    }

    /**
     * Get feeder
     *
     * @return Feeder
     */
    public function getFeeder()
    {
        $this->feeder;
    }
}

