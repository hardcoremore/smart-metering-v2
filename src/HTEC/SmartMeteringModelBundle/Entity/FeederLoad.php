<?php

namespace HTEC\SmartMeteringModelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FeederLoad
 *
 * @ORM\Table(name="feeder_load", uniqueConstraints={@ORM\UniqueConstraint(name="feeder_load", columns={"power_transformer_feeder_id", "date", "time"})})
 *
 * @ORM\Entity(repositoryClass="HTEC\SmartMeteringModelBundle\Repository\FeederLoadRepository")
 *
 *
 */
class FeederLoad
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Feeder", inversedBy="feederLoads")
     * @ORM\JoinColumn(name="power_transformer_feeder_id", referencedColumnName="id")
     */
    private $powerTransformerFeeder;

     /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date")
     */
    private $date;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="time", type="time")
     */
    private $time;

    /**
     * @var string
     *
     * @ORM\Column(name="feederLoad", type="decimal", precision=10, scale=2)
     */
    private $feederLoad;

    /**
     * @ORM\ManyToOne(targetEntity="AdminPanelUser")
     * @ORM\JoinColumn(name="created_by_id", referencedColumnName="id")
     */
    private $createdBy;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set powerTransformerFeeder
     *
     * @param Feeder $powerTransformerFeeder
     *
     * @return FeederLoad
     */
    public function setPowerTransformerFeeder(Feeder $powerTransformerFeeder)
    {
        $this->powerTransformerFeeder = $powerTransformerFeeder;

        return $this;
    }

    /**
     * Get powerTransformerFeeder
     *
     * @return Feeder
     */
    public function getPowerTransformerFeeder()
    {
        return $this->powerTransformerFeeder;
    }

     /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return FeederLoad
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set time
     *
     * @param \DateTime $time
     *
     * @return FeederLoad
     */
    public function setTime($time)
    {
        $this->time = $time;

        return $this;
    }

    /**
     * Get time
     *
     * @return \DateTime
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * Set feederLoad
     *
     * @param string $feederLoad
     *
     * @return FeederLoad
     */
    public function setFeederLoad($feederLoad)
    {
        $this->feederLoad = $feederLoad;

        return $this;
    }

    /**
     * Get feederLoad
     *
     * @return string
     */
    public function getFeederLoad()
    {
        return $this->feederLoad;
    }

    /**
     * Set createdBy
     *
     * @param AdminPanelUser $createdBy
     *
     * @return FeederLoad
     */
    public function setCreatedBy(AdminPanelUser $createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return AdminPanelUser
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }
}

