<?php

namespace HTEC\SmartMeteringModelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Appliance
 *
 * @ORM\Table(name="appliances")
 * @ORM\Entity
 */
class Appliance
{
    public function __construct()
    {
        $this->setCreatedDatetime(new \DateTime());
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="ApplianceType")
     * @ORM\JoinColumn(name="appliance_type_id", referencedColumnName="id")
     */
    private $applianceType;

    /**
     * @var string
     *
     * @ORM\Column(name="make", type="string", length=255)
     */
    private $make;

    /**
     * @var integer
     *
     * @ORM\Column(name="consumption", type="integer")
     */
    private $consumption;

    /**
     * @ORM\ManyToOne(targetEntity="AdminPanelUser")
     * @ORM\JoinColumn(name="created_by_id", referencedColumnName="id")
     */
    private $createdBy;

    /**
     * @ORM\ManyToOne(targetEntity="AdminPanelUser")
     * @ORM\JoinColumn(name="last_edited_by_id", referencedColumnName="id")
     */
    private $lastEditedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_datetime", type="datetime")
     */
    private $createdDatetime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_edited_datetime", type="datetime", nullable=true)
     */
    private $lastEditedDatetime;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

     /**
     * Set applianceType
     *
     * @param ApplianceType $applianceType
     * @return Appliance
     */
    public function setApplianceType(ApplianceType $applianceType = null)
    {
        $this->applianceType = $applianceType;

        return $this;
    }

    /**
     * Get applianceType
     *
     * @return ApplianceType 
     */
    public function getApplianceType()
    {
        return $this->applianceType;
    }

    /**
     * Set make
     *
     * @param string $make
     * @return Appliance
     */
    public function setMake($make)
    {
        $this->make = $make;

        return $this;
    }

    /**
     * Get make
     *
     * @return string 
     */
    public function getMake()
    {
        return $this->make;
    }

    /**
     * Set consumption
     *
     * @param integer $consumption
     * @return Appliance
     */
    public function setConsumption($consumption)
    {
        $this->consumption = $consumption;

        return $this;
    }

    /**
     * Get consumption
     *
     * @return integer 
     */
    public function getConsumption()
    {
        return $this->consumption;
    }

    /**
     * Set createdBy
     *
     * @param AdminPanelUser $createdBy
     * @return Appliance
     */
    public function setCreatedBy(AdminPanelUser $createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return AdminPanelUser 
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set lastEditedBy
     *
     * @param AdminPanelUser $lastEditedBy
     * @return Appliance
     */
    public function setLastEditedBy(AdminPanelUser $lastEditedBy)
    {
        $this->lastEditedBy = $lastEditedBy;

        return $this;
    }

    /**
     * Get lastEditedBy
     *
     * @return AdminPanelUser
     */
    public function getLastEditedBy()
    {
        return $this->lastEditedBy;
    }

    /**
     * Set createdDatetime
     *
     * @param \DateTime $createdDatetime
     * @return Appliance
     */
    public function setCreatedDatetime(\DateTime $createdDatetime)
    {
        $this->createdDatetime = $createdDatetime;

        return $this;
    }

    /**
     * Get createdDatetime
     *
     * @return \DateTime 
     */
    public function getCreatedDatetime()
    {
        return $this->createdDatetime;
    }

    /**
     * Set lastEditedDatetime
     *
     * @param \DateTime $lastEditedDatetime
     * @return Appliance
     */
    public function setLastEditedDatetime(\DateTime $lastEditedDatetime)
    {
        $this->lastEditedDatetime = $lastEditedDatetime;

        return $this;
    }

    /**
     * Get lastEditedDatetime
     *
     * @return \DateTime 
     */
    public function getLastEditedDatetime()
    {
        return $this->lastEditedDatetime;
    }
}
