<?php

namespace HTEC\SmartMeteringModelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * ApplianceType
 *
 * @ORM\Table(name="appliance_types")
 * @ORM\Entity()
 */
class ApplianceType
{
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->customerCategories = new ArrayCollection();
    }

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=128, unique=true)
     */
    private $name;


    /**
    * @ORM\ManyToMany(targetEntity="CustomerCategory", inversedBy="applianceTypes")
    * @ORM\JoinTable(name="appliance_type_customer_categories",
    *      joinColumns={@ORM\JoinColumn(name="appliance_type_id", referencedColumnName="id")},
    *      inverseJoinColumns={@ORM\JoinColumn(name="customer_category_id", referencedColumnName="id")}
    *      )
    *
    */
    private $customerCategories;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return ApplianceType
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add customerCategory
     *
     * @param CustomerCategory $customerCategory
     *
     * @return ApplianceType
     */
    public function addCustomerCategory(CustomerCategory $customerCategory)
    {
        $this->customerCategories[] = $customerCategory;

        return $this;
    }

    /**
     * Remove customerCategory
     *
     * @param CustomerCategory $customerCategory
     */
    public function removeCustomerCategory(CustomerCategory $customerCategory)
    {
        $this->customerCategories->removeElement($customerCategory);
    }

    /**
     * Get customerCategories
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCustomerCategories()
    {
        return $this->customerCategories;
    }
}
