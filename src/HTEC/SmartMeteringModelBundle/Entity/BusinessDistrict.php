<?php

namespace HTEC\SmartMeteringModelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Doctrine\Common\Collections\ArrayCollection;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * BusinessDistrict
 *
 * @ORM\Table(name="business_districts", uniqueConstraints={@ORM\UniqueConstraint(name="businessDistrict_name", columns={"name"})})
 * @ORM\Entity
 *
 * @UniqueEntity(fields="name", message="Business Unit with that name already exists")
 *
 */
class BusinessDistrict
{

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->injectionSubstations = new ArrayCollection();
        $this->customers = new ArrayCollection();
        $this->powerSchedules = new ArrayCollection();
    }


    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=128)
     */
    private $name;
    
    /**
     * @ORM\ManyToOne(targetEntity="City", inversedBy="businessDistricts")
     * @ORM\JoinColumn(name="city_id", referencedColumnName="id")
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="manager", type="string", length=128, nullable=true)
     */
    private $manager;

    /**
     * @var string
     *
     * @ORM\Column(name="phone_number", type="string", length=64, nullable=true)
     */
    private $phoneNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255, nullable=true)
     */
    private $address;

     /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=32, nullable=true)
     */
    private $code;

    /**
    *
    * @ORM\OneToMany(targetEntity="InjectionSubstation", mappedBy="businessDistrict")
    *
    */
    private $injectionSubstations;

    /**
    *
    * @ORM\OneToMany(targetEntity="Customer", mappedBy="businessDistrict")
    *
    */
    private $customers;

    /**
    *
    * @ORM\OneToMany(targetEntity="PowerSchedule", mappedBy="businessDistrict")
    *
    */
    private $powerSchedules;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return BusinessDistrict
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set city
     *
     * @param City $city
     * @return BusinessDistrict
     */
    public function setCity(City $city = null)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return City 
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set manager
     *
     * @param string $manager
     * @return BusinessDistrict
     */
    public function setManager($manager)
    {
        $this->manager = $manager;

        return $this;
    }

    /**
     * Get manager
     *
     * @return string 
     */
    public function getManager()
    {
        return $this->manager;
    }

    /**
     * Set phoneNumber
     *
     * @param string $phoneNumber
     * @return BusinessDistrict
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    /**
     * Get phoneNumber
     *
     * @return string 
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return BusinessDistrict
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return InjectionSubstation
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

     /**
     * Add injectionSubstation
     *
     * @param InjectionSubstation $injectionSubstation
     * @return City
     */
    public function addInjectionSubstation(InjectionSubstation $injectionSubstation)
    {
        $this->injectionSubstations[] = $injectionSubstation;

        return $this;
    }

    /**
     * Remove injectionSubstation
     *
     * @param InjectionSubstation $injectionSubstation
     */
    public function removeInjectionSubstation(InjectionSubstation $injectionSubstation)
    {
        $this->injectionSubstations->removeElement($injectionSubstation);
    }

    /**
     * Get injectionSubstations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getInjectionSubstations()
    {
        return $this->injectionSubstations;
    }

    /**
     * Add customer
     *
     * @param Customer $customer
     * @return Tariff
     */
    public function addCustomer(Customer $customer)
    {
        $this->customers[] = $customer;

        return $this;
    }

    /**
     * Remove customer
     *
     * @param Customer $customer
     */
    public function removeCustomer(Customer $customer)
    {
        $this->customers->removeElement($customer);
    }

    /**
     * Get customers
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCustomers()
    {
        return $this->customers;
    }

    /**
     * Add powerSchedule
     *
     * @param PowerSchedule $powerSchedule
     * @return PowerSource
     */
    public function addPowerSchedule(PowerSchedule $powerSchedule)
    {
        $this->powerSchedules[] = $powerSchedule;

        return $this;
    }

    /**
     * Remove powerSchedule
     *
     * @param PowerSchedule $powerSchedule
     */
    public function removePowerSchedule(PowerSchedule $powerSchedule)
    {
        $this->powerSchedules->removeElement($powerSchedule);
    }

    /**
     * Get injectionSubstations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPowerSchedules()
    {
        return $this->powerSchedules;
    }
}
