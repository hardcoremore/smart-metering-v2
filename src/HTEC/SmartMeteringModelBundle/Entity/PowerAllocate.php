<?php

namespace HTEC\SmartMeteringModelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PowerAllocate
 *
 * @ORM\Table(name="power_allocate")
 * @ORM\Entity(repositoryClass="HTEC\SmartMeteringModelBundle\Repository\PowerAllocateRepository")
 */
class PowerAllocate
{

     /**
     * Constructor
     */
    public function __construct()
    {
        $this->createdDatetime = new \DateTime();
    }


    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="PowerForecast")
     * @ORM\JoinColumn(name="power_forecast_id", referencedColumnName="id")
     */
    private $powerForecast;

    /**
     * @var int
     *
     * @ORM\Column(name="total_power_allocated", type="integer")
     */
    private $totalPowerAllocated;

    /**
     * @ORM\ManyToOne(targetEntity="BusinessDistrict")
     * @ORM\JoinColumn(name="highest_allocation_business_district_id", referencedColumnName="id")
     */
    private $highestAllocationBusinessDistrict;

    /**
     * @var string
     *
     * @ORM\Column(name="highest_business_district_allocation_value", type="decimal", precision=20, scale=4, nullable=true)
     */
    private $highestBusinessDistrictAllocationValue;

    /**
     * @ORM\ManyToOne(targetEntity="AdminPanelUser")
     * @ORM\JoinColumn(name="created_by_id", referencedColumnName="id")
     */
    private $createdBy;

    /**
     * @ORM\ManyToOne(targetEntity="AdminPanelUser")
     * @ORM\JoinColumn(name="last_edited_by_id", referencedColumnName="id")
     */
    private $lastEditedBy;

    /**
     * @var string
     *
     * @ORM\Column(name="created_datetime", type="datetime")
     */
    private $createdDatetime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_edited_datetime", type="datetime", nullable=true)
     */
    private $lastEditedDatetime;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set powerForecast
     *
     * @param integer $powerForecast
     *
     * @return PowerAllocate
     */
    public function setPowerForecast($powerForecast)
    {
        $this->powerForecast = $powerForecast;

        return $this;
    }

    /**
     * Get powerForecast
     *
     * @return int
     */
    public function getPowerForecast()
    {
        return $this->powerForecast;
    }

    /**
     * Set totalPowerAllocated
     *
     * @param integer $totalPowerAllocated
     *
     * @return PowerAllocate
     */
    public function setTotalPowerAllocated($totalPowerAllocated)
    {
        $this->totalPowerAllocated = $totalPowerAllocated;

        return $this;
    }

    /**
     * Get totalPowerAllocated
     *
     * @return int
     */
    public function getTotalPowerAllocated()
    {
        return $this->totalPowerAllocated;
    }

    /**
     * Set highestAllocationBusinessDistrict
     *
     * @param string $highestAllocationBusinessDistrict
     *
     * @return PowerForecast
     */
    public function setHighestAllocationBusinessDistrict(BusinessDistrict $highestAllocationBusinessDistrict)
    {
        $this->highestAllocationBusinessDistrict = $highestAllocationBusinessDistrict;

        return $this;
    }

    /**
     * Get highestAllocationBusinessDistrict
     *
     * @return string
     */
    public function getHighestAllocationBusinessDistrict()
    {
        return $this->highestAllocationBusinessDistrict;
    }

    /**
     * Set highestBusinessDistrictAllocationValue
     *
     * @param string $highestBusinessDistrictAllocationValue
     *
     * @return PowerAllocate
     */
    public function setHighestBusinessDistrictAllocationValue($highestBusinessDistrictAllocationValue)
    {
        $this->highestBusinessDistrictAllocationValue = $highestBusinessDistrictAllocationValue;

        return $this;
    }

    /**
     * Get highestBusinessDistrictAllocationValue
     *
     * @return string
     */
    public function getHighestBusinessDistrictAllocationValue()
    {
        return $this->highestBusinessDistrictAllocationValue;
    }

   /**
     * Set createdBy
     *
     * @param AdminPanelUser $createdBy
     * @return Appliance
     */
    public function setCreatedBy(AdminPanelUser $createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return AdminPanelUser 
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set lastEditedBy
     *
     * @param AdminPanelUser $lastEditedBy
     * @return Appliance
     */
    public function setLastEditedBy(AdminPanelUser $lastEditedBy)
    {
        $this->lastEditedBy = $lastEditedBy;

        return $this;
    }

    /**
     * Get lastEditedBy
     *
     * @return AdminPanelUser
     */
    public function getLastEditedBy()
    {
        return $this->lastEditedBy;
    }

    /**
     * Set createdDatetime
     *
     * @param string $createdDatetime
     * @return Appliance
     */
    public function setCreatedDatetime(string $createdDatetime)
    {
        $this->createdDatetime = $createdDatetime;

        return $this;
    }

    /**
     * Get createdDatetime
     *
     * @return \DateTime 
     */
    public function getCreatedDatetime()
    {
        return $this->createdDatetime;
    }

    /**
     * Set lastEditedDatetime
     *
     * @param \DateTime $lastEditedDatetime
     * @return Appliance
     */
    public function setLastEditedDatetime($lastEditedDatetime)
    {
        $this->lastEditedDatetime = $lastEditedDatetime;

        return $this;
    }

    /**
     * Get lastEditedDatetime
     *
     * @return \DateTime 
     */
    public function getLastEditedDatetime()
    {
        return $this->lastEditedDatetime;
    }
}

