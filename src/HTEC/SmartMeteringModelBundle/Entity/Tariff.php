<?php

namespace HTEC\SmartMeteringModelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * Tariff
 *
 * @ORM\Table(name="tariffs")
 * @ORM\Entity()
 */
class Tariff
{
     /**
     * Constructor
     */
    public function __construct()
    {
        $this->customers = new ArrayCollection();
        $this->powerSchedules = new ArrayCollection();
    }

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=64, unique=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=16, unique=true)
     */
    private $code;

    /**
     * @var decimal
     *
     * @ORM\Column(name="price", type="decimal", precision=20, scale=2)
     */
    private $price;

    /**
     * @var int
     *
     * @ORM\Column(name="average_consumption_per_day", type="bigint", nullable=true)
     */
    private $averageConsumptionPerDay;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;


    /**
    *
    * @ORM\OneToMany(targetEntity="Customer", mappedBy="tariff")
    *
    */
    private $customers;

    /**
    *
    * @ORM\OneToMany(targetEntity="PowerSchedule", mappedBy="tariff")
    *
    */
    private $powerSchedules;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Tariff
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return Tariff
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set price
     *
     * @param decimal $price
     *
     * @return Tariff
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return decimal
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set averageConsumptionPerDay
     *
     * @param integer $averageConsumptionPerDay
     *
     * @return Tariff
     */
    public function setAverageConsumptionPerDay($averageConsumptionPerDay)
    {
        $this->averageConsumptionPerDay = $averageConsumptionPerDay;

        return $this;
    }

    /**
     * Get averageConsumptionPerDay
     *
     * @return int
     */
    public function getAverageConsumptionPerDay()
    {
        return $this->averageConsumptionPerDay;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Tariff
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Add customer
     *
     * @param Customer $customer
     * @return Tariff
     */
    public function addCustomer(Customer $customer)
    {
        $this->customers[] = $customer;

        return $this;
    }

    /**
     * Remove customer
     *
     * @param Customer $customer
     */
    public function removeCustomer(Customer $customer)
    {
        $this->customers->removeElement($customer);
    }

    /**
     * Get customers
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCustomers()
    {
        return $this->customers;
    }

    /**
     * Add powerSchedule
     *
     * @param PowerSchedule $powerSchedule
     * @return Tariff
     */
    public function addPowerSchedule(PowerSchedule $powerSchedule)
    {
        $this->powerSchedules[] = $powerSchedule;

        return $this;
    }

    /**
     * Remove powerSchedule
     *
     * @param PowerSchedule $powerSchedule
     */
    public function removePowerSchedule(PowerSchedule $powerSchedule)
    {
        $this->powerSchedules->removeElement($powerSchedule);
    }

    /**
     * Get powerSchedules
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPowerSchedules()
    {
        return $this->powerSchedules;
    }
}

