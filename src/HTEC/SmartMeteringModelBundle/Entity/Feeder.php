<?php

namespace HTEC\SmartMeteringModelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Feeder
 *
 * @ORM\Table(name="feeders", uniqueConstraints={@ORM\UniqueConstraint(name="feeder_name", columns={"name"})})
 * @ORM\Entity(repositoryClass="HTEC\SmartMeteringModelBundle\Repository\FeederRepository")
 *
 * @UniqueEntity(fields="name", message="Injection Substation with this name already exists")
 *
 */
class Feeder
{
    /*
     * Constructor
     */
    public function __construct()
    {
        $this->meters = new ArrayCollection();
        $this->electricPoles = new ArrayCollection();
        $this->feederLoads = new ArrayCollection();
    }

    const TYPE_LOW_VOLTAGE = 'low-voltage';
    const TYPE_HIGH_VOLTAGE = 'high-voltage';

    const STATUS_LIVE = 'live';
    const STATUS_OFFLINE = 'offline';
    const STATUS_REPAIRING = 'repairing';

    const FORMATION_OVER_HEAD = 'overhead';
    const FORMATION_UNDERGROUND = 'underground';
    const FORMATION_OVERHEAD_UNDERGROUND = 'overhead-underground';


    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=64, unique=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=32)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="formation", type="string", length=32, nullable=true)
     */
    private $formation;

    /**
     * @var integer
     *
     * @ORM\Column(name="length", type="integer", nullable=true)
     */
    private $length;

    /**
     * @var array
     *
     * @ORM\Column(name="streetCoverage", type="json_array", nullable=true)
     */
    private $streetCoverage;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=16)
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=16)
     */
    private $code;

    /**
     * @var smallint
     *
     * @ORM\Column(name="kilovolts", type="smallint", nullable=true)
     */
    private $kilovolts;

    /**
    *
    * @ORM\OneToMany(targetEntity="Meter", mappedBy="feeder")
    *
    */
    private $meters;

    /**
    *
    * @ORM\OneToMany(targetEntity="ElectricPole", mappedBy="feeder")
    *
    */
    private $electricPoles;

    /**
    *
    * @ORM\OneToMany(targetEntity="FeederLoad", mappedBy="powerTransformerFeeder")
    *
    */
    private $feederLoads;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Feeder
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Feeder
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

     /**
     * Set formation
     *
     * @param string $formation
     *
     * @return Feeder
     */
    public function setFormation($formation)
    {
        $this->formation = $formation;

        return $this;
    }

    /**
     * Get formation
     *
     * @return string
     */
    public function getFormation()
    {
        return $this->formation;
    }

     /**
     * Set length
     *
     * @param integer $length
     *
     * @return Feeder
     */
    public function setLength($length)
    {
        $this->length = $length;

        return $this;
    }

    /**
     * Get length
     *
     * @return integer
     */
    public function getLength()
    {
        return $this->length;
    }

    /**
     * Set streetCoverage
     *
     * @param array $streetCoverage
     *
     * @return Feeder
     */
    public function setStreetCoverage(array $streetCoverage)
    {
        $this->streetCoverage = $streetCoverage;

        return $this;
    }

    /**
     * Get streetCoverage
     *
     * @return array
     */
    public function getStreetCoverage()
    {
        return $this->streetCoverage;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Feeder
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return Feeder
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return integer
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set kilovolts
     *
     * @param integer $kilovolts
     *
     * @return Feeder
     */
    public function setKilovolts($kilovolts)
    {
        $this->kilovolts = $kilovolts;

        return $this;
    }

    /**
     * Get kilovolts
     *
     * @return integer
     */
    public function getKilovolts()
    {
        return $this->kilovolts;
    }

    /**
     * Add meter
     *
     * @param Meter $meter
     * @return DistributionTransformer
     */
    public function addMeter(Meter $meter)
    {
        $this->meters[] = $meter;

        return $this;
    }

    /**
     * Remove meter
     *
     * @param Meter $meter
     */
    public function removeMeter(Meter $meter)
    {
        $this->meters->removeElement($meter);
    }

    /**
     * Get meters
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMeters()
    {
        return $this->meters;
    }

    /**
     * Add electricPole
     *
     * @param ElectricPole $electricPole
     * @return DistributionTransformer
     */
    public function addElectricPole(ElectricPole $electricPole)
    {
        $this->electricPoles[] = $electricPole;

        return $this;
    }

    /**
     * Remove electricPole
     *
     * @param ElectricPole $electricPole
     */
    public function removeElectricPole(ElectricPole $electricPole)
    {
        $this->electricPoles->removeElement($electricPole);
    }

    /**
     * Get electricPoles
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getElectricPoles()
    {
        return $this->electricPoles;
    }


    /**
     * Add feederLoad
     *
     * @param FeederLoad $feederLoad
     * @return DistributionTransformer
     */
    public function addFeederLoad(FeederLoad $feederLoad)
    {
        $this->feederLoads[] = $feederLoad;

        return $this;
    }

    /**
     * Remove feederLoad
     *
     * @param FeederLoad $feederLoad
     */
    public function removeFeederLoad(FeederLoad $feederLoad)
    {
        $this->feederLoads->removeElement($feederLoad);
    }

    /**
     * Get feederLoads
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFeederLoads()
    {
        return $this->feederLoads;
    }
}
