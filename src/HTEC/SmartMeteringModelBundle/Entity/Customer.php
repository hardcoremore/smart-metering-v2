<?php

namespace HTEC\SmartMeteringModelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * Customer
 *
 * @ORM\Table(name="customers")
 * @ORM\Entity(repositoryClass="HTEC\SmartMeteringModelBundle\Repository\CustomerRepository")
 */
class Customer
{
     /**
     * Constructor
     */
    public function __construct()
    {
        $this->meters = new ArrayCollection();
    }

    public function __toString() {
        return (string)$this->id;
    }

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

     /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=128)
     */
    private $name;

     /**
     * @var string
     *
     * @ORM\Column(name="account_number", type="string", length=32, unique=true)
     */
    private $accountNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="account_type", type="string", length=32)
     */
    private $accountType;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255)
     */
    private $address;

    /**
     * @ORM\ManyToOne(targetEntity="Tariff", inversedBy="customers")
     * @ORM\JoinColumn(name="tariff_id", referencedColumnName="id")
     */
    private $tariff;

    /**
     * @ORM\ManyToOne(targetEntity="BusinessDistrict", inversedBy="customers")
     * @ORM\JoinColumn(name="business_district_id", referencedColumnName="id")
     */
    private $businessDistrict;

    /**
     * @var string
     *
     * @ORM\Column(name="customer_type", type="string", length=32, nullable=true)
     */
    private $customerType;

    /**
     * @var string
     *
     * @ORM\Column(name="customer_status", type="string", length=32)
     */
    private $customerStatus;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=128, nullable=true, unique=true)
     */
    private $email;

     /**
     * @var string
     *
     * @ORM\Column(name="phone_number", type="string", length=32, nullable=true)
     */
    private $phoneNumber;


    /**
    *
    * @ORM\OneToMany(targetEntity="Meter", mappedBy="customer")
    *
    */
    private $meters;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Customer
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set accountNumber
     *
     * @param string $accountNumber
     *
     * @return Customer
     */
    public function setAccountNumber($accountNumber)
    {
        $this->accountNumber = $accountNumber;

        return $this;
    }

    /**
     * Get accountNumber
     *
     * @return string
     */
    public function getAccountNumber()
    {
        return $this->accountNumber;
    }

    /**
     * Set accountType
     *
     * @param string $accountType
     *
     * @return Customer
     */
    public function setAccountType($accountType)
    {
        $this->accountType = $accountType;

        return $this;
    }

    /**
     * Get accountType
     *
     * @return string
     */
    public function getAccountType()
    {
        return $this->accountType;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return Customer
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set tariff
     *
     * @param Tariff $tariff
     *
     * @return Customer
     */
    public function setTariff(Tariff $tariff)
    {
        $this->tariff = $tariff;

        return $this;
    }

    /**
     * Get tariff
     *
     * @return Tariff
     */
    public function getTariff()
    {
        return $this->tariff;
    }

    /**
     * Set businessDistrict
     *
     * @param BusinessDistrict $businessDistrict
     *
     * @return Customer
     */
    public function setBusinessDistrict(BusinessDistrict $businessDistrict)
    {
        $this->businessDistrict = $businessDistrict;

        return $this;
    }

    /**
     * Get businessDistrict
     *
     * @return BusinessDistrict
     */
    public function getBusinessDistrict()
    {
        return $this->businessDistrict;
    }

    /**
     * Set customerType
     *
     * @param string $customerType
     *
     * @return Customer
     */
    public function setCustomerType($customerType)
    {
        $this->customerType = $customerType;

        return $this;
    }

    /**
     * Get customerType
     *
     * @return string
     */
    public function getCustomerType()
    {
        return $this->customerType;
    }

    /**
     * Set customerStatus
     *
     * @param string $customerStatus
     *
     * @return Customer
     */
    public function setCustomerStatus($customerStatus)
    {
        $this->customerStatus = $customerStatus;

        return $this;
    }

    /**
     * Get customerStatus
     *
     * @return string
     */
    public function getCustomerStatus()
    {
        return $this->customerStatus;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Customer
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

     /**
     * Set phoneNumber
     *
     * @param string $phoneNumber
     *
     * @return Customer
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    /**
     * Get phoneNumber
     *
     * @return string
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * Add meter
     *
     * @param Meter $meter
     * @return Customer
     */
    public function addMeter(Meter $meter)
    {
        $this->meters[] = $meter;

        return $this;
    }

    /**
     * Remove meter
     *
     * @param Meter $meter
     */
    public function removeMeter(Meter $meter)
    {
        $this->meters->removeElement($meter);
    }

    /**
     * Get meters
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMeters()
    {
        return $this->meters;
    }
}

