<?php

namespace HTEC\SmartMeteringModelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ScheduledPower
 *
 * @ORM\Table(name="power_schedules")
 * @ORM\Entity(repositoryClass="HTEC\SmartMeteringModelBundle\Repository\PowerScheduleRepository")
 */
class PowerSchedule
{
    const STATUS_WAITING_APPROVAL = 'waiting_approval';
    const STATUS_APPROVED = 'approved';

    const REPEAT_TYPE_DAILY = 'daily';
    const REPEAT_TYPE_WEEKLY = 'weekly';
    const REPEAT_TYPE_MONTHLY = 'monthly';

    public function __construct()
    {
        $this->createdDatetime = new \DateTime();
        $this->setStatus(self::STATUS_WAITING_APPROVAL);
    }

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="power_type", type="string", length=16)
     */
    private $powerType;

    /**
     * @ORM\ManyToOne(targetEntity="PowerSource", inversedBy="powerSchedules")
     * @ORM\JoinColumn(name="power_source_id", referencedColumnName="id")
     */
    private $powerSource;

    /**
     * @ORM\ManyToOne(targetEntity="BusinessDistrict", inversedBy="powerSchedules")
     * @ORM\JoinColumn(name="business_district_id", referencedColumnName="id")
     */
    private $businessDistrict;

    /**
     * @ORM\ManyToOne(targetEntity="InjectionSubstation", inversedBy="powerSchedules")
     * @ORM\JoinColumn(name="injection_substation_id", referencedColumnName="id")
     */
    private $injectionSubstation;

    /**
     * @ORM\ManyToOne(targetEntity="PowerTransformer", inversedBy="powerSchedules")
     * @ORM\JoinColumn(name="power_transformer_id", referencedColumnName="id")
     */
    private $powerTransformer;

     /**
     * @ORM\ManyToOne(targetEntity="Feeder")
     * @ORM\JoinColumn(name="power_transformer_feeder_id", referencedColumnName="id")
     */
    private $powerTransformerFeeder;

    /**
     * @ORM\ManyToOne(targetEntity="DistributionTransformer", inversedBy="powerSchedules")
     * @ORM\JoinColumn(name="distribution_transformer_id", referencedColumnName="id")
     */
    private $distributionTransformer;

    /**
     * @ORM\ManyToOne(targetEntity="Feeder")
     * @ORM\JoinColumn(name="distribution_transformer_feeder_id", referencedColumnName="id")
     */
    private $distributionTransformerFeeder;

    /**
     * @ORM\ManyToOne(targetEntity="Tariff", inversedBy="powerSchedules")
     * @ORM\JoinColumn(name="tariff_id", referencedColumnName="id")
     */
    private $tariff;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date")
     */
    private $date;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start_time", type="time")
     */
    private $startTime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end_time", type="time")
     */
    private $endTime;

    /**
     * @var string
     *
     * @ORM\Column(name="repeatType", type="string", length=16, nullable=true)
     */
    private $repeatType;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="repeatEnd", type="date", nullable=true)
     */
    private $repeatEnd;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=16)
     */
    private $status;

     /**
     * @ORM\ManyToOne(targetEntity="AdminPanelUser")
     * @ORM\JoinColumn(name="approved_by_id", referencedColumnName="id")
     */
    private $approvedBy;

    /**
     * @ORM\ManyToOne(targetEntity="AdminPanelUser")
     * @ORM\JoinColumn(name="created_by_id", referencedColumnName="id")
     */
    private $createdBy;

    /**
     * @ORM\ManyToOne(targetEntity="AdminPanelUser")
     * @ORM\JoinColumn(name="last_edited_by_id", referencedColumnName="id")
     */
    private $lastEditedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_datetime", type="datetime")
     */
    private $createdDatetime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_edited_datetime", type="datetime", nullable=true)
     */
    private $lastEditedDatetime;

    /**
     * @var \DateTime
     *
     **/
    private $repeatDate;

    public function setRepeatDate($repeatDate)
    {
        $this->repeatDate = $repeatDate;
    }

    public function getRepeatDate()
    {
        return $this->repeatDate;
    }

    public function __clone()
    {
        if($this->powerSource)
        {
            $this->powerSource = clone $this->powerSource;
        }

        if($this->injectionSubstation)
        {
            $this->injectionSubstation = clone $this->injectionSubstation;
        }

        if($this->powerTransformer)
        {
            $this->powerTransformer = clone $this->powerTransformer;    
        }

        if($this->powerTransformerFeeder)
        {
            $this->powerTransformerFeeder = clone $this->powerTransformerFeeder;
        }

        if($this->distributionTransformer)
        {
            $this->distributionTransformer = clone $this->distributionTransformer;    
        }

        if($this->distributionTransformerFeeder)
        {
            $this->distributionTransformerFeeder = clone $this->distributionTransformerFeeder;
        }

        if($this->date instanceof \DateTime)
        {
            $this->date = clone $this->date;
        }

        if($this->startTime instanceof \DateTime)
        {
            $this->startTime = clone $this->startTime;
        }

        if($this->endTime instanceof \DateTime)
        {
            $this->endTime = clone $this->endTime;
        }
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set powerType
     *
     * @param string $powerType
     *
     * @return ScheduledPower
     */
    public function setPowerType($powerType)
    {
        $this->powerType = $powerType;

        return $this;
    }

    /**
     * Get powerType
     *
     * @return string
     */
    public function getPowerType()
    {
        return $this->powerType;
    }

    /**
     * Set powerSource
     *
     * @param integer $powerSource
     *
     * @return ScheduledPower
     */
    public function setPowerSource($powerSource)
    {
        $this->powerSource = $powerSource;

        return $this;
    }

    /**
     * Get powerSource
     *
     * @return int
     */
    public function getPowerSource()
    {
        return $this->powerSource;
    }

    /**
     * Set businessDistrict
     *
     * @param integer $businessDistrict
     *
     * @return ScheduledPower
     */
    public function setBusinessDistrict($businessDistrict)
    {
        $this->businessDistrict = $businessDistrict;

        return $this;
    }

    /**
     * Get businessDistrict
     *
     * @return int
     */
    public function getBusinessDistrict()
    {
        return $this->businessDistrict;
    }

    /**
     * Set injectionSubstation
     *
     * @param string $injectionSubstation
     *
     * @return ScheduledPower
     */
    public function setInjectionSubstation($injectionSubstation)
    {
        $this->injectionSubstation = $injectionSubstation;

        return $this;
    }

    /**
     * Get injectionSubstation
     *
     * @return string
     */
    public function getInjectionSubstation()
    {
        return $this->injectionSubstation;
    }

    /**
     * Set powerTransformer
     *
     * @param integer $powerTransformer
     *
     * @return ScheduledPower
     */
    public function setPowerTransformer($powerTransformer)
    {
        $this->powerTransformer = $powerTransformer;

        return $this;
    }

    /**
     * Get powerTransformer
     *
     * @return int
     */
    public function getPowerTransformer()
    {
        return $this->powerTransformer;
    }

    /**
     * Set powerTransformerFeeder
     *
     * @param integer $powerTransformerFeeder
     *
     * @return ScheduledPower
     */
    public function setPowerTransformerFeeder($powerTransformerFeeder)
    {
        $this->powerTransformerFeeder = $powerTransformerFeeder;

        return $this;
    }

    /**
     * Get powerTransformerFeeder
     *
     * @return int
     */
    public function getPowerTransformerFeeder()
    {
        return $this->powerTransformerFeeder;
    }

    /**
     * Set distributionTransformer
     *
     * @param integer $distributionTransformer
     *
     * @return ScheduledPower
     */
    public function setDistributionTransformer($distributionTransformer)
    {
        $this->distributionTransformer = $distributionTransformer;

        return $this;
    }

    /**
     * Get distributionTransformer
     *
     * @return int
     */
    public function getDistributionTransformer()
    {
        return $this->distributionTransformer;
    }

    /**
     * Set distributionTransformerFeeder
     *
     * @param integer $distributionTransformerFeeder
     *
     * @return ScheduledPower
     */
    public function setDistributionTransformerFeeder($distributionTransformerFeeder)
    {
        $this->distributionTransformerFeeder = $distributionTransformerFeeder;

        return $this;
    }

    /**
     * Get distributionTransformerFeeder
     *
     * @return int
     */
    public function getDistributionTransformerFeeder()
    {
        return $this->distributionTransformerFeeder;
    }

    /**
     * Set tariff
     *
     * @param integer $tariff
     *
     * @return ScheduledPower
     */
    public function setTariff($tariff)
    {
        $this->tariff = $tariff;

        return $this;
    }

    /**
     * Get tariff
     *
     * @return int
     */
    public function getTariff()
    {
        return $this->tariff;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return ScheduledPower
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set startTime
     *
     * @param \DateTime $startTime
     *
     * @return ScheduledPower
     */
    public function setStartTime($startTime)
    {
        $this->startTime = $startTime;

        return $this;
    }

    /**
     * Get startTime
     *
     * @return \DateTime
     */
    public function getStartTime()
    {
        return $this->startTime;
    }

    /**
     * Set endTime
     *
     * @param \DateTime $endTime
     *
     * @return ScheduledPower
     */
    public function setEndTime($endTime)
    {
        $this->endTime = $endTime;

        return $this;
    }

    /**
     * Get endTime
     *
     * @return \DateTime
     */
    public function getEndTime()
    {
        return $this->endTime;
    }

    /**
     * Set repeatType
     *
     * @param string $repeatType
     *
     * @return ScheduledPower
     */
    public function setRepeatType($repeatType)
    {
        $this->repeatType = $repeatType;

        return $this;
    }

    /**
     * Get repeatType
     *
     * @return string
     */
    public function getRepeatType()
    {
        return $this->repeatType;
    }

    /**
     * Set repeatEnd
     *
     * @param \DateTime $repeatEnd
     *
     * @return ScheduledPower
     */
    public function setRepeatEnd($repeatEnd)
    {
        $this->repeatEnd = $repeatEnd;

        return $this;
    }

    /**
     * Get repeatEnd
     *
     * @return \DateTime
     */
    public function getRepeatEnd()
    {
        return $this->repeatEnd;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return ScheduledPower
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set approvedBy
     *
     * @param AdminPanelUser $approvedBy
     * @return PowerSchedule
     */
    public function setApprovedBy(AdminPanelUser $approvedBy = null)
    {
        $this->approvedBy = $approvedBy;

        return $this;
    }

    /**
     * Get approvedBy
     *
     * @return AdminPanelUser 
     */
    public function getApprovedBy()
    {
        return $this->approvedBy;
    }

    /**
     * Set createdBy
     *
     * @param AdminPanelUser $createdBy
     * @return Appliance
     */
    public function setCreatedBy(AdminPanelUser $createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return AdminPanelUser 
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set lastEditedBy
     *
     * @param AdminPanelUser $lastEditedBy
     * @return Appliance
     */
    public function setLastEditedBy(AdminPanelUser $lastEditedBy)
    {
        $this->lastEditedBy = $lastEditedBy;

        return $this;
    }

    /**
     * Get lastEditedBy
     *
     * @return AdminPanelUser
     */
    public function getLastEditedBy()
    {
        return $this->lastEditedBy;
    }

    /**
     * Set createdDatetime
     *
     * @param \DateTime $createdDatetime
     * @return Appliance
     */
    public function setCreatedDatetime($createdDatetime)
    {
        $this->createdDatetime = $createdDatetime;

        return $this;
    }

    /**
     * Get createdDatetime
     *
     * @return \DateTime 
     */
    public function getCreatedDatetime()
    {
        return $this->createdDatetime;
    }

    /**
     * Set lastEditedDatetime
     *
     * @param \DateTime $lastEditedDatetime
     * @return Appliance
     */
    public function setLastEditedDatetime($lastEditedDatetime)
    {
        $this->lastEditedDatetime = $lastEditedDatetime;

        return $this;
    }

    /**
     * Get lastEditedDatetime
     *
     * @return \DateTime 
     */
    public function getLastEditedDatetime()
    {
        return $this->lastEditedDatetime;
    }
}

