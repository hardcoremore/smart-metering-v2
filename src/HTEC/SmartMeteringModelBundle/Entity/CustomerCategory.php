<?php

namespace HTEC\SmartMeteringModelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * CustomerCategory
 *
 * @ORM\Table(name="customer_categories")
 * @ORM\Entity()
 */
class CustomerCategory
{
    public function __construct()
    {
        $this->applianceTypes = new ArrayCollection();
    }

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=64, unique=true)
     */
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity="ApplianceType", mappedBy="customerCategories")
     * @ORM\JoinTable(name="appliance_type_customer_categories",
     *      joinColumns={@ORM\JoinColumn(name="customer_category_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="appliance_type_id", referencedColumnName="id")}
     *      )
     */
    private $applianceTypes;

    /**
     * @var decimal
     *
     * @ORM\Column(name="diversity_factor", type="decimal", precision=10, scale=2)
     */
    private $diversityFactor;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return CustomerCategory
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add applianceType
     *
     * @param AdminPanelUser $applianceType
     * @return Role
     */
    public function addApplianceType(AdminPanelUser $applianceType)
    {
        $this->applianceTypes[] = $applianceType;
    
        return $this;
    }

    /**
     * Remove applianceType
     *
     * @param AdminPanelUser $applianceType
     */
    public function removeApplianceType(AdminPanelUser $applianceType)
    {
        $this->applianceTypes->removeElement($applianceType);
    }

    /**
     * Get applianceTypes
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getApplianceTypes()
    {
        return $this->applianceTypes;
    }

    /**
     * Set diversityFactor
     *
     * @param decimal $diversityFactor
     *
     * @return CustomerCategory
     */
    public function setDiversityFactor($diversityFactor)
    {
        $this->diversityFactor = $diversityFactor;

        return $this;
    }

    /**
     * Get diversityFactor
     *
     * @return decimal
     */
    public function getDiversityFactor()
    {
        return $this->diversityFactor;
    }
}

