<?php

namespace HTEC\SmartMeteringModelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * CustomerNetworkData
 *
 * @ORM\Table(name="customers_network_data", uniqueConstraints={@ORM\UniqueConstraint(name="customer", columns={"customer_id"})})
 * @ORM\Entity(repositoryClass="HTEC\SmartMeteringModelBundle\Repository\CustomerNetworkDataRepository")
 *
 *
 * @UniqueEntity(fields="customer", message="This customer already have network data defined")
 *
 */
class CustomerNetworkData
{
    public function __construct()
    {
        $this->createdDatetime = new \DateTime();
    }

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

     /**
     * @ORM\ManyToOne(targetEntity="Customer")
     * @ORM\JoinColumn(name="customer_id", referencedColumnName="id")
     */
    private $customer;

    /**
     * @ORM\ManyToOne(targetEntity="InjectionSubstation")
     * @ORM\JoinColumn(name="injection_substation_id", referencedColumnName="id")
     */
    private $injectionSubstation;

     /**
     * @ORM\ManyToOne(targetEntity="PowerTransformer")
     * @ORM\JoinColumn(name="power_transformer_id", referencedColumnName="id")
     */
    private $powerTransformer;

    /**
     * @ORM\ManyToOne(targetEntity="Feeder")
     * @ORM\JoinColumn(name="power_transformer_feeder_id", referencedColumnName="id")
     */
    private $powerTransformerFeeder;

    /**
     * @ORM\ManyToOne(targetEntity="ElectricPole")
     * @ORM\JoinColumn(name="high_tension_electric_pole_id", referencedColumnName="id")
     */
    private $highTensionPole;

    /**
     * @ORM\ManyToOne(targetEntity="DistributionTransformer")
     * @ORM\JoinColumn(name="distribution_transformer_id", referencedColumnName="id")
     */
    private $distributionTransformer;

    /**
     * @ORM\ManyToOne(targetEntity="Feeder")
     * @ORM\JoinColumn(name="distribution_transformer_feeder_id", referencedColumnName="id")
     */
    private $distributionTransformerFeeder;

    /**
     * @ORM\ManyToOne(targetEntity="ElectricPole")
     * @ORM\JoinColumn(name="low_tension_electric_pole_id", referencedColumnName="id")
     */
    private $lowTensionPole;

    /**
     * @ORM\ManyToOne(targetEntity="AdminPanelUser")
     * @ORM\JoinColumn(name="created_by_id", referencedColumnName="id")
     */
    private $createdBy;

    /**
     * @ORM\ManyToOne(targetEntity="AdminPanelUser")
     * @ORM\JoinColumn(name="last_edited_by_id", referencedColumnName="id")
     */
    private $lastEditedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_datetime", type="datetime")
     */
    private $createdDatetime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_edited_datetime", type="datetime", nullable=true)
     */
    private $lastEditedDatetime;

     /**
     * @var string
     *
     * @ORM\Column(name="account_number", type="string", length=32, unique=true)
     */
    private $accountNumber;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set customer
     *
     * @param Customer $customer
     *
     * @return CustomerNetworkData
     */
    public function setCustomer(Customer $customer = null)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * Get customer
     *
     * @return Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * Set injectionSubstation
     *
     * @param InjectionSubstation $injectionSubstation
     *
     * @return CustomerNetworkData
     */
    public function setInjectionSubstation(InjectionSubstation $injectionSubstation = null)
    {
        $this->injectionSubstation = $injectionSubstation;

        return $this;
    }

    /**
     * Get injectionSubstation
     *
     * @return InjectionSubstation
     */
    public function getInjectionSubstation()
    {
        return $this->injectionSubstation;
    }

    /**
     * Set powerTransformer
     *
     * @param PowerTransformer $powerTransformer
     *
     * @return CustomerNetworkData
     */
    public function setPowerTransformer(PowerTransformer $powerTransformer = null)
    {
        $this->powerTransformer = $powerTransformer;

        return $this;
    }

    /**
     * Get powerTransformer
     *
     * @return PowerTransformer
     */
    public function getPowerTransformer()
    {
        return $this->powerTransformer;
    }

    /**
     * Set powerTransformerFeeder
     *
     * @param Feeder $powerTransformerFeeder
     *
     * @return CustomerNetworkData
     */
    public function setPowerTransformerFeeder(Feeder $powerTransformerFeeder = null)
    {
        $this->powerTransformerFeeder = $powerTransformerFeeder;

        return $this;
    }

    /**
     * Get powerTransformerFeeder
     *
     * @return Feeder
     */
    public function getPowerTransformerFeeder()
    {
        return $this->powerTransformerFeeder;
    }

    /**
     * Set highTensionPole
     *
     * @param ElectricPole $highTensionPole
     *
     * @return CustomerNetworkData
     */
    public function setHighTensionPole(ElectricPole $highTensionPole = null)
    {
        $this->highTensionPole = $highTensionPole;

        return $this;
    }

    /**
     * Get highTensionPole
     *
     * @return ElectricPole
     */
    public function getHighTensionPole()
    {
        return $this->highTensionPole;
    }

    /**
     * Set distributionTransformer
     *
     * @param DistributionTransformer $distributionTransformer
     *
     * @return CustomerNetworkData
     */
    public function setDistributionTransformer(DistributionTransformer $distributionTransformer = null)
    {
        $this->distributionTransformer = $distributionTransformer;

        return $this;
    }

    /**
     * Get distributionTransformer
     *
     * @return DistributionTransformer
     */
    public function getDistributionTransformer()
    {
        return $this->distributionTransformer;
    }

    /**
     * Set distributionTransformerFeeder
     *
     * @param Feeder $distributionTransformerFeeder
     *
     * @return CustomerNetworkData
     */
    public function setDistributionTransformerFeeder(Feeder $distributionTransformerFeeder = null)
    {
        $this->distributionTransformerFeeder = $distributionTransformerFeeder;

        return $this;
    }

    /**
     * Get distributionTransformerFeeder
     *
     * @return Feeder
     */
    public function getDistributionTransformerFeeder()
    {
        return $this->distributionTransformerFeeder;
    }

    /**
     * Set lowTensionPole
     *
     * @param ElectricPole $lowTensionPole
     *
     * @return CustomerNetworkData
     */
    public function setLowTensionPole(ElectricPole $lowTensionPole = null)
    {
        $this->lowTensionPole = $lowTensionPole;

        return $this;
    }

    /**
     * Get lowTensionPole
     *
     * @return ElectricPole
     */
    public function getLowTensionPole()
    {
        return $this->lowTensionPole;
    }

      /**
     * Set createdBy
     *
     * @param AdminPanelUser $createdBy
     * @return Appliance
     */
    public function setCreatedBy(AdminPanelUser $createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return AdminPanelUser 
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set lastEditedBy
     *
     * @param AdminPanelUser $lastEditedBy
     * @return Appliance
     */
    public function setLastEditedBy(AdminPanelUser $lastEditedBy)
    {
        $this->lastEditedBy = $lastEditedBy;

        return $this;
    }

    /**
     * Get lastEditedBy
     *
     * @return AdminPanelUser
     */
    public function getLastEditedBy()
    {
        return $this->lastEditedBy;
    }

    /**
     * Set createdDatetime
     *
     * @param \DateTime $createdDatetime
     * @return Appliance
     */
    public function setCreatedDatetime(\DateTime $createdDatetime)
    {
        $this->createdDatetime = $createdDatetime;

        return $this;
    }

    /**
     * Get createdDatetime
     *
     * @return \DateTime 
     */
    public function getCreatedDatetime()
    {
        return $this->createdDatetime;
    }

    /**
     * Set lastEditedDatetime
     *
     * @param \DateTime $lastEditedDatetime
     * @return Appliance
     */
    public function setLastEditedDatetime(\DateTime $lastEditedDatetime)
    {
        $this->lastEditedDatetime = $lastEditedDatetime;

        return $this;
    }

    /**
     * Get lastEditedDatetime
     *
     * @return \DateTime 
     */
    public function getLastEditedDatetime()
    {
        return $this->lastEditedDatetime;
    }

     /**
     * Set accountNumber
     *
     * @param string $accountNumber
     *
     * @return Customer
     */
    public function setAccountNumber($accountNumber)
    {
        $this->accountNumber = $accountNumber;

        return $this;
    }

    /**
     * Get accountNumber
     *
     * @return string
     */
    public function getAccountNumber()
    {
        return $this->accountNumber;
    }
}
