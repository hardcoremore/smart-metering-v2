<?php

namespace HTEC\SmartMeteringModelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * PowerForecast
 *
 * @ORM\Table(name="power_forecasts")
 * @ORM\Entity(repositoryClass="HTEC\SmartMeteringModelBundle\Repository\PowerForecastRepository")
 */
class PowerForecast
{

     /**
     * Constructor
     */
    public function __construct()
    {
        $this->createdDatetime = new \DateTime();
        $this->businessDistrictTotals = new ArrayCollection();
        $this->businessDistrictTariffsData = new ArrayCollection();
        $this->tariffData = new ArrayCollection();
    }

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="PowerPurchase")
     * @ORM\JoinColumn(name="power_purchase_id", referencedColumnName="id")
     */
    private $powerPurchase;

    /**
     * @var string
     *
     * @ORM\Column(name="power_type", type="string", length=16)
     */
    private $powerType;

    /**
     * @var int
     *
     * @ORM\Column(name="amount_to_forecast", type="integer")
     */
    private $amountToForecast;

    /**
     * @var int
     *
     * @ORM\Column(name="total_customers_affected", type="integer", nullable=true)
     */
    private $totalCustomersAffected;

    /**
     * @var int
     *
     * @ORM\Column(name="total_business_districts_affected", type="integer", nullable=true)
     */
    private $totalBusinessDistrictsAffected;

    /**
     * @var int
     *
     * @ORM\Column(name="total_tariffs_affected", type="integer", nullable=true)
     */
    private $totalTariffsAffected;

    /**
     * @var string
     *
     * @ORM\Column(name="average_allocation_per_customer", type="decimal", precision=20, scale=4, nullable=true)
     */
    private $averageAllocationPerCustomer;

    /**
     * @ORM\ManyToOne(targetEntity="BusinessDistrict")
     * @ORM\JoinColumn(name="highest_allocation_business_district_id", referencedColumnName="id")
     */
    private $highestAllocationBusinessDistrict;

    /**
     * @var string
     *
     * @ORM\Column(name="highest_business_district_allocation_value", type="decimal", precision=20, scale=4, nullable=true)
     */
    private $highestBusinessDistrictAllocationValue;

   /**
     * @ORM\ManyToOne(targetEntity="Tariff")
     * @ORM\JoinColumn(name="highest_allocation_tariff_id", referencedColumnName="id")
     */
    private $highestAllocationTariff;

    /**
     * @var string
     *
     * @ORM\Column(name="highest_tariff_allocation_value", type="decimal", precision=20, scale=4, nullable=true)
     */
    private $highestTariffAllocationValue;

    /**
    *
    * @ORM\OneToMany(targetEntity="PowerForecastTariffData", mappedBy="powerForecast")
    *
    */
    private $powerForecastTariffData;

    /**
    *
    * @ORM\OneToMany(targetEntity="PowerForecastBusinessDistrictTariffData", mappedBy="powerForecast")
    *
    */
    private $businessDistrictTariffsData;

    /**
    *
    * @ORM\OneToMany(targetEntity="PowerForecastBusinessDistrictTotal", mappedBy="powerForecast")
    *
    */
    private $businessDistrictTotals;

    /**
     * @ORM\ManyToOne(targetEntity="AdminPanelUser")
     * @ORM\JoinColumn(name="created_by_id", referencedColumnName="id")
     */
    private $createdBy;

     /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_datetime", type="datetime")
     */
    private $createdDatetime;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set powerPurchase
     *
     * @param PowerPurchase $powerPurchase
     *
     * @return PowerForecast
     */
    public function setPowerPurchase(PowerPurchase $powerPurchase)
    {
        $this->powerPurchase = $powerPurchase;

        return $this;
    }

    /**
     * Get powerPurchase
     *
     * @return PowerPurchase
     */
    public function getPowerPurchase()
    {
        return $this->powerPurchase;
    }

    /**
     * Set amountToForecast
     *
     * @param integer $amountToForecast
     *
     * @return PowerForecast
     */
    public function setAmountToForecast($amountToForecast)
    {
        $this->amountToForecast = $amountToForecast;

        return $this;
    }

    /**
     * Get amountToForecast
     *
     * @return int
     */
    public function getAmountToForecast()
    {
        return $this->amountToForecast;
    }

    /**
     * Set totalCustomersAffected
     *
     * @param integer $totalCustomersAffected
     *
     * @return PowerForecast
     */
    public function setTotalCustomersAffected($totalCustomersAffected)
    {
        $this->totalCustomersAffected = $totalCustomersAffected;

        return $this;
    }

    /**
     * Get totalCustomersAffected
     *
     * @return int
     */
    public function getTotalCustomersAffected()
    {
        return $this->totalCustomersAffected;
    }

    /**
     * Set totalTariffsAffected
     *
     * @param integer $totalTariffsAffected
     *
     * @return PowerForecast
     */
    public function setTotalTariffsAffected($totalTariffsAffected)
    {
        $this->totalTariffsAffected = $totalTariffsAffected;

        return $this;
    }

    /**
     * Get totalTariffsAffected
     *
     * @return int
     */
    public function getTotalTariffsAffected()
    {
        return $this->totalTariffsAffected;
    }

    /**
     * Set totalBusinessDistrictsAffected
     *
     * @param integer $totalBusinessDistrictsAffected
     *
     * @return PowerForecast
     */
    public function setTotalBusinessDistrictsAffected($totalBusinessDistrictsAffected)
    {
        $this->totalBusinessDistrictsAffected = $totalBusinessDistrictsAffected;

        return $this;
    }

    /**
     * Get totalBusinessDistrictsAffected
     *
     * @return int
     */
    public function getTotalBusinessDistrictsAffected()
    {
        return $this->totalBusinessDistrictsAffected;
    }

    /**
     * Set averageAllocationPerCustomer
     *
     * @param string $averageAllocationPerCustomer
     *
     * @return PowerForecast
     */
    public function setAverageAllocationPerCustomer($averageAllocationPerCustomer)
    {
        $this->averageAllocationPerCustomer = $averageAllocationPerCustomer;

        return $this;
    }

    /**
     * Get averageAllocationPerCustomer
     *
     * @return string
     */
    public function getAverageAllocationPerCustomer()
    {
        return $this->averageAllocationPerCustomer;
    }

    /**
     * Set powerType
     *
     * @param string $powerType
     *
     * @return PowerForecast
     */
    public function setPowerType($powerType)
    {
        $this->powerType = $powerType;

        return $this;
    }

    /**
     * Get powerType
     *
     * @return string
     */
    public function getPowerType()
    {
        return $this->powerType;
    }

    /**
     * Set highestAllocationBusinessDistrict
     *
     * @param string $highestAllocationBusinessDistrict
     *
     * @return PowerForecast
     */
    public function setHighestAllocationBusinessDistrict(BusinessDistrict $highestAllocationBusinessDistrict)
    {
        $this->highestAllocationBusinessDistrict = $highestAllocationBusinessDistrict;

        return $this;
    }

    /**
     * Get highestAllocationBusinessDistrict
     *
     * @return string
     */
    public function getHighestAllocationBusinessDistrict()
    {
        return $this->highestAllocationBusinessDistrict;
    }

    /**
     * Set highestBusinessDistrictAllocationValue
     *
     * @param string $highestBusinessDistrictAllocationValue
     *
     * @return PowerForecast
     */
    public function setHighestBusinessDistrictAllocationValue($highestBusinessDistrictAllocationValue)
    {
        $this->highestBusinessDistrictAllocationValue = $highestBusinessDistrictAllocationValue;

        return $this;
    }

    /**
     * Get highestBusinessDistrictAllocationValue
     *
     * @return string
     */
    public function getHighestBusinessDistrictAllocationValue()
    {
        return $this->highestBusinessDistrictAllocationValue;
    }

    /**
     * Set highestAllocationTariff
     *
     * @param integer $highestAllocationTariff
     *
     * @return PowerForecast
     */
    public function setHighestAllocationTariff(Tariff $highestAllocationTariff)
    {
        $this->highestAllocationTariff = $highestAllocationTariff;

        return $this;
    }

    /**
     * Get highestAllocationTariff
     *
     * @return int
     */
    public function getHighestAllocationTariff()
    {
        return $this->highestAllocationTariff;
    }

    /**
     * Set highestTariffAllocationValue
     *
     * @param string $highestTariffAllocationValue
     *
     * @return PowerForecast
     */
    public function setHighestTariffAllocationValue($highestTariffAllocationValue)
    {
        $this->highestTariffAllocationValue = $highestTariffAllocationValue;

        return $this;
    }

    /**
     * Get highestTariffAllocationValue
     *
     * @return string
     */
    public function getHighestTariffAllocationValue()
    {
        return $this->highestTariffAllocationValue;
    }


     /**
     * Add powerForecastTariffData
     *
     * @param PowerForecastTariffData $powerForecastTariffData
     * @return PowerForecast
     */
    public function addPowerForecastTariffData(PowerForecastTariffData $powerForecastTariffData)
    {
        $this->powerForecastTariffData[] = $powerForecastTariffData;

        return $this;
    }

    /**
     * Remove powerForecastTariffData
     *
     * @param PowerForecastTariffData $powerForecastTariffData
     */
    public function removePowerForecastTariffData(PowerForecastTariffData $powerForecastTariffData)
    {
        $this->powerForecastTariffData->removeElement($powerForecastTariffData);
    }

    /**
     * Get powerForecastTariffDatas
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPowerForecastTariffDatas()
    {
        return $this->powerForecastTariffData;
    }

     /**
     * Add PowerForecastBusinessDistrictTariffData
     *
     * @param PowerForecastBusinessDistrictTariffData $businessDistrictTariffsData
     * @return PowerForecast
     */
    public function addBusinessDistrictTariffsData(PowerForecastBusinessDistrictTariffData $businessDistrictTariffData)
    {
        $this->businessDistrictTariffsData[] = $businessDistrictTariffData;

        return $this;
    }

    /**
     * Remove businessDistrictTariffData
     *
     * @param PowerForecastBusinessDistrictTariffData $businessDistrictTariffData
     */
    public function removeBusinessDistrictTariffsData(PowerForecastBusinessDistrictTariffData $businessDistrictTariffData)
    {
        $this->businessDistrictTariffsData->removeElement($businessDistrictTariffData);
    }

    /**
     * Get businessDistrictTariffsDatas
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getBusinessDistrictTariffsDatas()
    {
        return $this->businessDistrictTariffsData;
    }


     /**
     * Add PowerForecastBusinessDistrictTariffsData
     *
     * @param PowerForecastBusinessDistrictTariffsData $businessDistrictTotals
     * @return PowerForecast
     */
    public function addBusinessDistrictTotal(PowerForecastBusinessDistrictTotal $businessDistrictTotal)
    {
        $this->businessDistrictTotals[] = $businessDistrictTotal;

        return $this;
    }

    /**
     * Remove businessDistrictTotals
     *
     * @param PowerForecastBusinessDistrictTotal $businessDistrictTotals
     */
    public function removeBusinessDistrictTotal(PowerForecastBusinessDistrictTotal $businessDistrictTotal)
    {
        $this->businessDistrictTotals->removeElement($businessDistrictTotal);
    }

    /**
     * Get businessDistrictTotalss
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getBusinessDistrictTotals()
    {
        return $this->businessDistrictTotals;
    }

    /**
     * Set createdBy
     *
     * @param AdminPanelUser $createdBy
     * @return Appliance
     */
    public function setCreatedBy(AdminPanelUser $createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return AdminPanelUser 
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

     /**
     * Set createdDatetime
     *
     * @param DateTime $createdDatetime
     * @return Appliance
     */
    public function setCreatedDatetime(\DateTime $createdDatetime)
    {
        $this->createdDatetime = $createdDatetime;

        return $this;
    }

    /**
     * Get createdDatetime
     *
     * @return \DateTime 
     */
    public function getCreatedDatetime()
    {
        return $this->createdDatetime;
    }
}

