<?php

namespace HTEC\SmartMeteringModelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * PowerTransformer
 *
 * @ORM\Table(name="power_transformers")
 * @ORM\Entity(repositoryClass="HTEC\SmartMeteringModelBundle\Repository\PowerTransformerRepository")
 */
class PowerTransformer
{

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->outputFeeders = new ArrayCollection();
        $this->distributionTransformers = new ArrayCollection();
        $this->powerSchedules = new ArrayCollection();
    }

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=64, unique=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=32)
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=16)
     */
    private $code;

     /**
     * @ORM\ManyToOne(targetEntity="InjectionSubstation", inversedBy="powerTransformers")
     * @ORM\JoinColumn(name="injection_substation_id", referencedColumnName="id")
     */
    private $injectionSubstation;

   /**
     * @ORM\ManyToMany(targetEntity="Feeder")
     * @ORM\JoinTable(name="power_transformer_output_feeders",
     *      joinColumns={@ORM\JoinColumn(name="power_transformer_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="feeder_id", referencedColumnName="id")}
     *      )
     */
    private $outputFeeders;

    /**
    *
    * @ORM\OneToMany(targetEntity="DistributionTransformer", mappedBy="powerTransformer")
    *
    */
    private $distributionTransformers;

    /**
    *
    * @ORM\OneToMany(targetEntity="PowerSchedule", mappedBy="powerTransformer")
    *
    */
    private $powerSchedules;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return PowerTransformer
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return PowerTransformer
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return PowerTransformer
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set injectionSubstation
     *
     * @param BusinessDistrict $injectionSubstation
     *
     * @return InjectionSubstation
     */
    public function setInjectionSubstation(InjectionSubstation $injectionSubstation = null)
    {
        $this->injectionSubstation = $injectionSubstation;

        return $this;
    }

    /**
     * Get injectionSubstation
     *
     * @return InjectionSubstation
     */
    public function getInjectionSubstation()
    {
        return $this->injectionSubstation;
    }

    /**
     * Add outputFeeders
     *
     * @param Feeder $feeder
     * @return PowerTransformer
     */
    public function addOutputFeeder(Feeder $feeder)
    {
        $this->outputFeeders[] = $feeder;

        return $this;
    }

    /**
     * Remove outputFeeder
     *
     * @param Feeder $feeder
     */
    public function removeOutputFeeder(Feeder $feeder)
    {
        $this->outputFeeders->removeElement($feeder);
    }

    /**
     * Get outputFeeders
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getOutputFeeders()
    {
       return $this->outputFeeders;
    }

    /**
     * Add distributionTransformer
     *
     * @param DistributionTransformer $distributionTransformer
     * @return DistributionTransformer
     */
    public function addDistributionTransformer(DistributionTransformer $distributionTransformer)
    {
        $this->distributionTransformers[] = $distributionTransformer;

        return $this;
    }

    /**
     * Remove distributionTransformer
     *
     * @param DistributionTransformer $distributionTransformer
     */
    public function removeDistributionTransformer(DistributionTransformer $distributionTransformer)
    {
        $this->distributionTransformers->removeElement($distributionTransformer);
    }

    /**
     * Get distributionTransformers
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDistributionTransformers()
    {
        return $this->distributionTransformers;
    }

    /**
     * Add powerSchedule
     *
     * @param PowerSchedule $powerSchedule
     * @return PowerTransformer
     */
    public function addPowerSchedule(PowerSchedule $powerSchedule)
    {
        $this->powerSchedules[] = $powerSchedule;

        return $this;
    }

    /**
     * Remove powerSchedule
     *
     * @param PowerSchedule $powerSchedule
     */
    public function removePowerSchedule(PowerSchedule $powerSchedule)
    {
        $this->powerSchedules->removeElement($powerSchedule);
    }

    /**
     * Get powerSchedules
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPowerSchedules()
    {
        return $this->powerSchedules;
    }
}
