<?php

namespace HTEC\SmartMeteringModelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PowerActivateDeactivate
 *
 * @ORM\Table(name="power_activate_deactivate")
 * @ORM\Entity(repositoryClass="HTEC\SmartMeteringModelBundle\Repository\PowerActivateDeactivateRepository")
 *
 */
class PowerActivateDeactivate
{
    const TYPE_POWER_ACTIVATE = 'power-activate';
    const TYPE_POWER_DEACTIVATE = 'power-deactivate';

    public function __construct()
    {
        $this->createdDatetime = new \DateTime();
        $this->setType(self::TYPE_POWER_ACTIVATE);
        $this->setDate(new \DateTime());
        $this->setTime(new \DateTime());
    }

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="BusinessDistrict")
     * @ORM\JoinColumn(name="business_district_id", referencedColumnName="id")
     */
    private $businessDistrict;

    /**
     * @ORM\ManyToOne(targetEntity="InjectionSubstation")
     * @ORM\JoinColumn(name="injection_substation_id", referencedColumnName="id")
     */
    private $injectionSubstation;

    /**
     * @ORM\ManyToOne(targetEntity="PowerTransformer")
     * @ORM\JoinColumn(name="power_transformer_id", referencedColumnName="id")
     */
    private $powerTransformer;

     /**
     * @ORM\ManyToOne(targetEntity="Feeder")
     * @ORM\JoinColumn(name="power_transformer_feeder_id", referencedColumnName="id")
     */
    private $powerTransformerFeeder;

    /**
     * @ORM\ManyToOne(targetEntity="DistributionTransformer")
     * @ORM\JoinColumn(name="distribution_transformer_id", referencedColumnName="id")
     */
    private $distributionTransformer;

    /**
     * @ORM\ManyToOne(targetEntity="Feeder")
     * @ORM\JoinColumn(name="distribution_transformer_feeder_id", referencedColumnName="id")
     */
    private $distributionTransformerFeeder;

    /**
     * @ORM\ManyToOne(targetEntity="Tariff")
     * @ORM\JoinColumn(name="tariff_id", referencedColumnName="id")
     */
    private $tariff;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date")
     */
    private $date;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="time", type="time")
     */
    private $time;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=16)
     */
    private $type;

    /**
     * @ORM\ManyToOne(targetEntity="AdminPanelUser")
     * @ORM\JoinColumn(name="created_by_id", referencedColumnName="id")
     */
    private $createdBy;

    /**
     * @ORM\ManyToOne(targetEntity="AdminPanelUser")
     * @ORM\JoinColumn(name="last_edited_by_id", referencedColumnName="id")
     */
    private $lastEditedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_datetime", type="datetime")
     */
    private $createdDatetime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_edited_datetime", type="datetime", nullable=true)
     */
    private $lastEditedDatetime;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set businessDistrict
     *
     * @param integer $businessDistrict
     *
     * @return ScheduledPower
     */
    public function setBusinessDistrict($businessDistrict)
    {
        $this->businessDistrict = $businessDistrict;

        return $this;
    }

    /**
     * Get businessDistrict
     *
     * @return int
     */
    public function getBusinessDistrict()
    {
        return $this->businessDistrict;
    }

    /**
     * Set injectionSubstation
     *
     * @param string $injectionSubstation
     *
     * @return ScheduledPower
     */
    public function setInjectionSubstation($injectionSubstation)
    {
        $this->injectionSubstation = $injectionSubstation;

        return $this;
    }

    /**
     * Get injectionSubstation
     *
     * @return string
     */
    public function getInjectionSubstation()
    {
        return $this->injectionSubstation;
    }

    /**
     * Set powerTransformer
     *
     * @param integer $powerTransformer
     *
     * @return ScheduledPower
     */
    public function setPowerTransformer($powerTransformer)
    {
        $this->powerTransformer = $powerTransformer;

        return $this;
    }

    /**
     * Get powerTransformer
     *
     * @return int
     */
    public function getPowerTransformer()
    {
        return $this->powerTransformer;
    }

    /**
     * Set powerTransformerFeeder
     *
     * @param integer $powerTransformerFeeder
     *
     * @return ScheduledPower
     */
    public function setPowerTransformerFeeder($powerTransformerFeeder)
    {
        $this->powerTransformerFeeder = $powerTransformerFeeder;

        return $this;
    }

    /**
     * Get powerTransformerFeeder
     *
     * @return int
     */
    public function getPowerTransformerFeeder()
    {
        return $this->powerTransformerFeeder;
    }

    /**
     * Set distributionTransformer
     *
     * @param integer $distributionTransformer
     *
     * @return ScheduledPower
     */
    public function setDistributionTransformer($distributionTransformer)
    {
        $this->distributionTransformer = $distributionTransformer;

        return $this;
    }

    /**
     * Get distributionTransformer
     *
     * @return int
     */
    public function getDistributionTransformer()
    {
        return $this->distributionTransformer;
    }

    /**
     * Set distributionTransformerFeeder
     *
     * @param integer $distributionTransformerFeeder
     *
     * @return ScheduledPower
     */
    public function setDistributionTransformerFeeder($distributionTransformerFeeder)
    {
        $this->distributionTransformerFeeder = $distributionTransformerFeeder;

        return $this;
    }

    /**
     * Get distributionTransformerFeeder
     *
     * @return int
     */
    public function getDistributionTransformerFeeder()
    {
        return $this->distributionTransformerFeeder;
    }

    /**
     * Set tariff
     *
     * @param integer $tariff
     *
     * @return ScheduledPower
     */
    public function setTariff($tariff)
    {
        $this->tariff = $tariff;

        return $this;
    }

    /**
     * Get tariff
     *
     * @return int
     */
    public function getTariff()
    {
        return $this->tariff;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return ScheduledPower
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set time
     *
     * @param \DateTime $time
     *
     * @return ScheduledPower
     */
    public function setTime($time)
    {
        $this->time = $time;

        return $this;
    }

    /**
     * Get time
     *
     * @return \DateTime
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return ScheduledPower
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

      /**
     * Set createdBy
     *
     * @param AdminPanelUser $createdBy
     * @return Appliance
     */
    public function setCreatedBy(AdminPanelUser $createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return AdminPanelUser 
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set lastEditedBy
     *
     * @param AdminPanelUser $lastEditedBy
     * @return Appliance
     */
    public function setLastEditedBy(AdminPanelUser $lastEditedBy)
    {
        $this->lastEditedBy = $lastEditedBy;

        return $this;
    }

    /**
     * Get lastEditedBy
     *
     * @return AdminPanelUser
     */
    public function getLastEditedBy()
    {
        return $this->lastEditedBy;
    }

    /**
     * Set createdDatetime
     *
     * @param \DateTime $createdDatetime
     * @return Appliance
     */
    public function setCreatedDatetime(\DateTime $createdDatetime)
    {
        $this->createdDatetime = $createdDatetime;

        return $this;
    }

    /**
     * Get createdDatetime
     *
     * @return \DateTime 
     */
    public function getCreatedDatetime()
    {
        return $this->createdDatetime;
    }

    /**
     * Set lastEditedDatetime
     *
     * @param \DateTime $lastEditedDatetime
     * @return Appliance
     */
    public function setLastEditedDatetime(\DateTime $lastEditedDatetime)
    {
        $this->lastEditedDatetime = $lastEditedDatetime;

        return $this;
    }

    /**
     * Get lastEditedDatetime
     *
     * @return \DateTime 
     */
    public function getLastEditedDatetime()
    {
        return $this->lastEditedDatetime;
    }
}

