<?php

namespace HTEC\SmartMeteringModelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PowerForecastBusinessDistrictTotal
 *
 * @ORM\Table(name="power_forecast_business_district_totals")
 * @ORM\Entity()
 */
class PowerForecastBusinessDistrictTotal
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

     /**
    * @ORM\ManyToOne(targetEntity="PowerForecast", inversedBy="businessDistrictTotals")
    * @ORM\JoinColumn(name="power_forecast_id", referencedColumnName="id")
    */
    private $powerForecast;

     /**
     * @ORM\ManyToOne(targetEntity="BusinessDistrict")
     * @ORM\JoinColumn(name="business_district_id", referencedColumnName="id")
     */
    private $businessDistrict;

    /**
     * @var int
     *
     * @ORM\Column(name="number_of_customers", type="integer")
     */
    private $numberOfCustomers;

    /**
     * @var string
     *
     * @ORM\Column(name="power_allocation_in_kilowatt_hour", type="decimal", precision=20, scale=4)
     */
    private $powerAllocationInKilowattHour;

    /**
     * @var string
     *
     * @ORM\Column(name="power_allocation_in_kilowatt_hour_per_day", type="decimal", precision=20, scale=4)
     */
    private $powerAllocationInKilowattHourPerDay;

    /**
     * @var string
     *
     * @ORM\Column(name="daily_charge", type="decimal", precision=20, scale=4)
     */
    private $dailyCharge;

    /**
     * @var string
     *
     * @ORM\Column(name="monthly_charge", type="decimal", precision=20, scale=4)
     */
    private $monthlyCharge;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set powerForecast
     *
     * @param integer $powerForecast
     *
     * @return PowerForecastBusinessDistrictTotal
     */
    public function setPowerForecast(PowerForecast $powerForecast)
    {
        $this->powerForecast = $powerForecast;

        return $this;
    }

    /**
     * Get powerForecast
     *
     * @return int
     */
    public function getPowerForecast()
    {
        return $this->powerForecast;
    }

    /**
     * Set businessDistrict
     *
     * @param integer $businessDistrict
     *
     * @return PowerForecastBusinessDistrictTotal
     */
    public function setBusinessDistrict(BusinessDistrict $businessDistrict)
    {
        $this->businessDistrict = $businessDistrict;

        return $this;
    }

    /**
     * Get businessDistrict
     *
     * @return int
     */
    public function getBusinessDistrict()
    {
        return $this->businessDistrict;
    }

    /**
     * Set numberOfCustomers
     *
     * @param integer $numberOfCustomers
     *
     * @return PowerForecastBusinessDistrictTotal
     */
    public function setNumberOfCustomers($numberOfCustomers)
    {
        $this->numberOfCustomers = $numberOfCustomers;

        return $this;
    }

    /**
     * Get numberOfCustomers
     *
     * @return int
     */
    public function getNumberOfCustomers()
    {
        return $this->numberOfCustomers;
    }

    /**
     * Set powerAllocationInKilowattHour
     *
     * @param string $powerAllocationInKilowattHour
     *
     * @return PowerForecastBusinessDistrictTotal
     */
    public function setPowerAllocationInKilowattHour($powerAllocationInKilowattHour)
    {
        $this->powerAllocationInKilowattHour = $powerAllocationInKilowattHour;

        return $this;
    }

    /**
     * Get powerAllocationInKilowattHour
     *
     * @return string
     */
    public function getPowerAllocationInKilowattHour()
    {
        return $this->powerAllocationInKilowattHour;
    }

    /**
     * Set powerAllocationInKilowattHourPerDay
     *
     * @param string $powerAllocationInKilowattHourPerDay
     *
     * @return PowerForecastBusinessDistrictTotal
     */
    public function setPowerAllocationInKilowattHourPerDay($powerAllocationInKilowattHourPerDay)
    {
        $this->powerAllocationInKilowattHourPerDay = $powerAllocationInKilowattHourPerDay;

        return $this;
    }

    /**
     * Get powerAllocationInKilowattHourPerDay
     *
     * @return string
     */
    public function getPowerAllocationInKilowattHourPerDay()
    {
        return $this->powerAllocationInKilowattHourPerDay;
    }

    /**
     * Set dailyCharge
     *
     * @param string $dailyCharge
     *
     * @return PowerForecastBusinessDistrictTotal
     */
    public function setDailyCharge($dailyCharge)
    {
        $this->dailyCharge = $dailyCharge;

        return $this;
    }

    /**
     * Get dailyCharge
     *
     * @return string
     */
    public function getDailyCharge()
    {
        return $this->dailyCharge;
    }

    /**
     * Set monthlyCharge
     *
     * @param string $monthlyCharge
     *
     * @return PowerForecastBusinessDistrictTotal
     */
    public function setMonthlyCharge($monthlyCharge)
    {
        $this->monthlyCharge = $monthlyCharge;

        return $this;
    }

    /**
     * Get monthlyCharge
     *
     * @return string
     */
    public function getMonthlyCharge()
    {
        return $this->monthlyCharge;
    }
}

