<?php

namespace HTEC\SmartMeteringModelBundle\Entity;   

use Doctrine\Common\Collections\ArrayCollection;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\EquatableInterface;

use HTEC\BaseModelBundle\Entity\User as BaseUser;

use Doctrine\ORM\Mapping as ORM;

 /**
  * 
  * @ORM\Entity(repositoryClass="HTEC\BaseModelBundle\Repository\UserRepository")
  *
  **/
class AdminPanelUser extends BaseUser
{

    public function __construct()
    {
        parent::__construct();

        $this->setPinCode(1234);

        $this->roles = new ArrayCollection();

        $this->approvedPowerPurchases = new ArrayCollection();
    }

    /**
     * @var smallint
     *
     * @ORM\Column(name="pin_code", type="smallint")
     */
    private $pinCode;

    /**
    * @ORM\ManyToMany(targetEntity="Role", inversedBy="users")
    * @ORM\JoinTable(name="user_roles",
    *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
    *      inverseJoinColumns={@ORM\JoinColumn(name="role_id", referencedColumnName="id")}
    *      )
    *
    */
    private $roles;

    /**
     *
     * @ORM\OneToMany(targetEntity="Role", mappedBy="owner")
     *
     */
    private $ownedRoles;

    /**
    *
    * @ORM\OneToMany(targetEntity="PowerPurchase", mappedBy="createdBy")
    *
    */
    private $createdPowerPurchase;

    /**
    *
    * @ORM\OneToMany(targetEntity="PowerPurchase", mappedBy="lastEditedBy")
    *
    */
    private $lastEditedPowerPurchase;

    /**
    *
    * @ORM\OneToMany(targetEntity="PowerPurchase", mappedBy="approvedBy")
    *
    */
    private $approvedPowerPurchases;


    private $permissions;

    public function getRoles()
    {
       return $this->roles->toArray();
    }

    public function getPermissions()
    {
        return $this->permissions;
    }

    public function setPermissions(array $permissions)
    {
        $this->permissions = $permissions;
    }


    /**
     * Set pinCode
     *
     * @param integer $pinCode
     * @return User
     */
    public function setPinCode($pinCode)
    {
        $this->pinCode = $pinCode;

        return $this;
    }

    /**
     * Get pinCode
     *
     * @return integer 
     */
    public function getPinCode()
    {
        return $this->pinCode;
    }

    /**
     * Add roles
     *
     * @param Role $role
     * @return SiteUser
     */
    public function addRole(Role $role)
    {
        $role->addUser($this);
        $this->roles[] = $role;

        return $this;
    }

    /**
     * Remove roles
     *
     * @param Role $role
     */
    public function removeRole(Role $role)
    {
        $role->removeUser($this);
        $this->roles->removeElement($role);
    }

    /**
     * Add ownedRoles
     *
     * @param Role $ownedRoles
     * @return User
     */
    public function addOwnedRole(Role $ownedRoles)
    {
        $this->ownedRoles[] = $ownedRoles;

        return $this;
    }

    /**
     * Remove ownedRoles
     *
     * @param Role $ownedRoles
     */
    public function removeOwnedRole(Role $ownedRoles)
    {
        $this->ownedRoles->removeElement($ownedRoles);
    }

    /**
     * Get ownedRoles
     *
     * @return ArrayCollection
     */
    public function getOwnedRoles()
    {
        return $this->ownedRoles;
    }

    /**
     * Add createdPowerPurchase
     *
     * @param PowerPurchase $createdPowerPurchase
     * @return User
     */
    public function addCreatedPowerPurchase(PowerPurchase $createdPowerPurchase)
    {
        $this->createdPowerPurchase[] = $createdPowerPurchase;

        return $this;
    }

    /**
     * Remove createdPowerPurchase
     *
     * @param PowerPurchase $createdPowerPurchase
     */
    public function removeCreatedPowerPurchase(PowerPurchase $createdPowerPurchase)
    {
        $this->createdPowerPurchase->removeElement($createdPowerPurchase);
    }

    /**
     * Get createdPowerPurchase
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCreatedPowerPurchase()
    {
        return $this->createdPowerPurchase;
    }

    /**
     * Add lastEditedPowerPurchase
     *
     * @param PowerPurchase $lastEditedPowerPurchase
     * @return User
     */
    public function addLastEditedPowerPurchase(PowerPurchase $lastEditedPowerPurchase)
    {
        $this->lastEditedPowerPurchase[] = $lastEditedPowerPurchase;

        return $this;
    }

    /**
     * Remove lastEditedPowerPurchase
     *
     * @param PowerPurchase $lastEditedPowerPurchase
     */
    public function removeLastEditedPowerPurchase(PowerPurchase $lastEditedPowerPurchase)
    {
        $this->lastEditedPowerPurchase->removeElement($lastEditedPowerPurchase);
    }

    /**
     * Get lastEditedPowerPurchase
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getLastEditedPowerPurchase()
    {
        return $this->lastEditedPowerPurchase;
    }

    /**
     * Add approvedPowerPurchase
     *
     * @param PowerPurchase $approvedPowerPurchase
     * @return User
     */
    public function addApprovedPowerPurchase(PowerPurchase $approvedPowerPurchase)
    {
        $this->approvedPowerPurchases[] = $approvedPowerPurchase;

        return $this;
    }

    /**
     * Remove approvedPowerPurchase
     *
     * @param PowerPurchase $approvedPowerPurchase
     */
    public function removeApprovedPowerPurchase(PowerPurchase $approvedPowerPurchase)
    {
        $this->approvedPowerPurchases->removeElement($approvedPowerPurchase);
    }

    /**
     * Get approvedPowerPurchase
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getApprovedPowerPurchase()
    {
        return $this->approvedPowerPurchases;
    }
}
