<?php

namespace HTEC\SmartMeteringModelBundle\Entity;

use Symfony\Component\Security\Core\Role\Role as BaseRole;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 *
 * @ORM\Table(name="roles", uniqueConstraints={@ORM\UniqueConstraint(name="role_name", columns={"name"}),
 *                                                  @ORM\UniqueConstraint(name="role", columns={"role"})})
 *
 * @UniqueEntity(fields="name", message="Role with that name already exists")
 * @UniqueEntity(fields="role", message="Role already exists")
 *
 */
class Role extends BaseRole
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=48)
     */
    private $name;

    /**
     * @ORM\Column(name="role", type="string", length=48, unique=true)
     */
    private $role;

    /**
     * 
     * @ORM\ManyToOne(targetEntity="AdminPanelUser", inversedBy="ownedRoles")
     * @ORM\JoinColumn(name="owner_id", referencedColumnName="id")
     *
     */
    private $owner;

    /**
     * @ORM\ManyToMany(targetEntity="AdminPanelUser", mappedBy="roles")
     * @ORM\JoinTable(name="user_roles",
     *      joinColumns={@ORM\JoinColumn(name="role_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")}
     *      )
     */
    private $users;

    private $permissions;

    public function getPermissions()
    {
        return $this->permissions;
    }

    public function setPermissions(array $permissions)
    {
        $this->permissions = $permissions;
    }

    public function __construct()
    {
        $this->users = new ArrayCollection();
    }

    /**
     * @see RoleInterface
     */
    public function getRole()
    {
        return $this->role;
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Role
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set role
     *
     * @param string $role
     * @return Role
     */
    public function setRole($role)
    {
        $this->role = $role;
    
        return $this;
    }

    /**
     * Add users
     *
     * @param AdminPanelUser $users
     * @return Role
     */
    public function addUser(AdminPanelUser $users)
    {
        $this->users[] = $users;
    
        return $this;
    }

    /**
     * Remove users
     *
     * @param AdminPanelUser $users
     */
    public function removeUser(AdminPanelUser $users)
    {
        $this->users->removeElement($users);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Set owner
     *
     * @param User $owner
     * @return Role
     */
    public function setOwner(AdminPanelUser $owner)
    {
        $this->owner = $owner;
    
        return $this;
    }

    /**
     * Get owner
     *
     * @return User 
     */
    public function getOwner()
    {
        return $this->owner;
    }
}
