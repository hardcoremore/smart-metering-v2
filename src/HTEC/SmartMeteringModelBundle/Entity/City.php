<?php

namespace HTEC\SmartMeteringModelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Doctrine\Common\Collections\ArrayCollection;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * City
 *
 * @ORM\Table(name="cities", uniqueConstraints={@ORM\UniqueConstraint(name="city_name", columns={"name"})})
 * @ORM\Entity
 *
 * @UniqueEntity(fields="name", message="City with that name already exists")
 * 
 */
class City
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=64)
     */
    private $name;

    /**
    *
    * @ORM\OneToMany(targetEntity="BusinessDistrict", mappedBy="city")
    *
    */
    private $businessDistricts;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->businessDistricts = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return City
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add businessDistrict
     *
     * @param BusinessDistrict $businessDistrict
     * @return City
     */
    public function addBusinessDistrict(BusinessDistrict $businessDistrict)
    {
        $this->businessDistricts[] = $businessDistrict;

        return $this;
    }

    /**
     * Remove businessDistrict
     *
     * @param BusinessDistrict $businessDistrict
     */
    public function removeBusinessDistrict(BusinessDistrict $businessDistrict)
    {
        $this->businessDistricts->removeElement($businessDistrict);
    }

    /**
     * Get businessDistricts
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getBusinessDistricts()
    {
        return $this->businessDistricts;
    }
}
