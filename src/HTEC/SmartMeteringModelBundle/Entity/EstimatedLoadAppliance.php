<?php

namespace HTEC\SmartMeteringModelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EstimatedLoadAppliance
 *
 * @ORM\Table(name="estimated_load_appliances")
 * @ORM\Entity()
 */
class EstimatedLoadAppliance
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="EstimatedLoad")
     * @ORM\JoinColumn(name="estimated_load_id", referencedColumnName="id")
     */
    private $estimatedLoad;

    /**
     * @ORM\ManyToOne(targetEntity="ApplianceType")
     * @ORM\JoinColumn(name="appliance_type_id", referencedColumnName="id")
     */
    private $applianceType;

     /**
     * @ORM\ManyToOne(targetEntity="Appliance")
     * @ORM\JoinColumn(name="appliance_id", referencedColumnName="id")
     */
    private $appliance;

    /**
     * @var int
     *
     * @ORM\Column(name="appliance_count", type="smallint")
     */
    private $applianceCount;

    /**
     * @var int
     *
     * @ORM\Column(name="total_consumption", type="integer")
     */
    private $totalConsumption;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set estimatedLoad
     *
     * @param EstimatedLoad $estimatedLoad
     *
     * @return EstimatedLoadAppliance
     */
    public function setEstimatedLoad(EstimatedLoad $estimatedLoad)
    {
        $this->estimatedLoad = $estimatedLoad;

        return $this;
    }

    /**
     * Get estimatedLoad
     *
     * @return EstimatedLoad
     */
    public function getEstimatedLoad()
    {
        return $this->estimatedLoad;
    }

    /**
     * Set applianceType
     *
     * @param ApplianceType $applianceType
     *
     * @return CustomerAppliance
     */
    public function setApplianceType(ApplianceType $applianceType)
    {
        $this->applianceType = $applianceType;

        return $this;
    }

    /**
     * Get applianceType
     *
     * @return ApplianceType
     */
    public function getApplianceType()
    {
        return $this->applianceType;
    }

    /**
     * Set appliance
     *
     * @param Appliance $appliance
     *
     * @return CustomerAppliance
     */
    public function setAppliance(Appliance $appliance)
    {
        $this->appliance = $appliance;

        return $this;
    }

    /**
     * Get appliance
     *
     * @return Appliance
     */
    public function getAppliance()
    {
        return $this->appliance;
    }

    /**
     * Set applianceCount
     *
     * @param integer $applianceCount
     *
     * @return CustomerAppliance
     */
    public function setApplianceCount($applianceCount)
    {
        $this->applianceCount = $applianceCount;

        return $this;
    }

    /**
     * Get applianceCount
     *
     * @return int
     */
    public function getApplianceCount()
    {
        return $this->applianceCount;
    }

    /**
     * Set totalConsumption
     *
     * @param integer $totalConsumption
     *
     * @return EstimatedLoadAppliance
     */
    public function setTotalConsumption($totalConsumption)
    {
        $this->totalConsumption = $totalConsumption;

        return $this;
    }

    /**
     * Get totalConsumption
     *
     * @return int
     */
    public function getTotalConsumption()
    {
        return $this->totalConsumption;
    }
}
