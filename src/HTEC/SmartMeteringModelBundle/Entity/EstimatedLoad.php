<?php

namespace HTEC\SmartMeteringModelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * EstimatedLoad
 *
 * @ORM\Table(name="estimated_loads", uniqueConstraints={@ORM\UniqueConstraint(name="customer", columns={"customer_id"})})
 * @ORM\Entity(repositoryClass="HTEC\SmartMeteringModelBundle\Repository\EstimatedLoadRepository")
 *
 * @UniqueEntity(fields="customer", message="This customer already have estimated load")
 */
class EstimatedLoad
{
    public function __construct()
    {
        $this->setCreatedDatetime(new \Datetime());
        $this->setTotalLoad(0);
        $this->setApplianceCount(0);
    }

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Customer")
     * @ORM\JoinColumn(name="customer_id", referencedColumnName="id")
     */
    private $customer;

     /**
     * @ORM\ManyToOne(targetEntity="CustomerCategory")
     * @ORM\JoinColumn(name="customer_category_id", referencedColumnName="id")
     */
    private $customerCategory;

    /**
     * @var int
     *
     * @ORM\Column(name="total_load", type="integer")
     */
    private $totalLoad;

    /**
     * @var int
     *
     * @ORM\Column(name="appliance_count", type="smallint")
     */
    private $applianceCount;

    /**
     * @ORM\ManyToOne(targetEntity="AdminPanelUser")
     * @ORM\JoinColumn(name="created_by_id", referencedColumnName="id")
     */
    private $createdBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_date_time", type="datetime")
     */
    private $createdDatetime;

    /**
     * @ORM\ManyToOne(targetEntity="AdminPanelUser")
     * @ORM\JoinColumn(name="last_edited_by_id", referencedColumnName="id")
     */
    private $lastEditedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_edited_datetime", type="datetime", nullable=true)
     */
    private $lastEditedDatetime;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set customer
     *
     * @param Customer $customer
     *
     * @return EstimatedLoad
     */
    public function setCustomer(Customer $customer)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * Get customer
     *
     * @return Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * Set customerCategory
     *
     * @param CustomerCategory $customerCategory
     *
     * @return EstimatedLoad
     */
    public function setCustomerCategory(CustomerCategory $customerCategory)
    {
        $this->customerCategory = $customerCategory;

        return $this;
    }

    /**
     * Get customerCategory
     *
     * @return CustomerCategory
     */
    public function getCustomerCategory()
    {
        return $this->customerCategory;
    }

    /**
     * Set totalLoad
     *
     * @param integer $totalLoad
     *
     * @return EstimatedLoad
     */
    public function setTotalLoad($totalLoad)
    {
        $this->totalLoad = $totalLoad;

        return $this;
    }

    /**
     * Get totalLoad
     *
     * @return integer
     */
    public function getTotalLoad()
    {
        return $this->totalLoad;
    }

    /**
     * Set applianceCount
     *
     * @param integer $applianceCount
     *
     * @return EstimatedLoad
     */
    public function setApplianceCount($applianceCount)
    {
        $this->applianceCount = $applianceCount;

        return $this;
    }

    /**
     * Get applianceCount
     *
     * @return integer
     */
    public function getApplianceCount()
    {
        return $this->applianceCount;
    }

    /**
     * Set createdBy
     *
     * @param AdminPanelUser $createdBy
     *
     * @return EstimatedLoad
     */
    public function setCreatedBy(AdminPanelUser $createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return AdminPanelUser
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set createdDatetime
     *
     * @param \DateTime $createdDatetime
     *
     * @return EstimatedLoad
     */
    public function setCreatedDatetime($createdDatetime)
    {
        $this->createdDatetime = $createdDatetime;

        return $this;
    }

    /**
     * Get createdDatetime
     *
     * @return \DateTime
     */
    public function getCreatedDatetime()
    {
        return $this->createdDatetime;
    }

    /**
     * Set lastEditedBy
     *
     * @param AdminPanelUser $lastEditedBy
     *
     * @return EstimatedLoad
     */
    public function setLastEditedBy(AdminPanelUser $lastEditedBy)
    {
        $this->lastEditedBy = $lastEditedBy;

        return $this;
    }

    /**
     * Get lastEditedBy
     *
     * @return AdminPanelUser
     */
    public function getLastEditedBy()
    {
        return $this->lastEditedBy;
    }

    /**
     * Set lastEditedDatetime
     *
     * @param \DateTime $lastEditedDatetime
     *
     * @return EstimatedLoad
     */
    public function setLastEditedDatetime($lastEditedDatetime)
    {
        $this->lastEditedDatetime = $lastEditedDatetime;

        return $this;
    }

    /**
     * Get lastEditedDatetime
     *
     * @return \DateTime
     */
    public function getLastEditedDatetime()
    {
        return $this->lastEditedDatetime;
    }
}

