<?php

namespace HTEC\SmartMeteringModelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Meter
 *
 * @ORM\Table(name="meters",  uniqueConstraints={@ORM\UniqueConstraint(name="customer", columns={"customer_id"})})
 * @ORM\Entity(repositoryClass="HTEC\SmartMeteringModelBundle\Repository\MeterRepository")
 *
 * @UniqueEntity(fields="customer", message="This customer already have a meter")
 *
 */
class Meter
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Customer", inversedBy="meters")
     * @ORM\JoinColumn(name="customer_id", referencedColumnName="id")
     */
    private $customer;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=32)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=32)
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=32, nullable=true)
     */
    private $code;

    /**
     * @ORM\ManyToOne(targetEntity="DistributionTransformer", inversedBy="meters")
     * @ORM\JoinColumn(name="distribution_transformer_id", referencedColumnName="id")
     */
    private $distributionTransformer;

    /**
     * @ORM\ManyToOne(targetEntity="Feeder", inversedBy="meters")
     * @ORM\JoinColumn(name="feeder_id", referencedColumnName="id")
     */
    private $feeder;

    /**
     * @var string
     *
     * @ORM\Column(name="serial_number", type="string", length=32, nullable=true, unique=true)
     */
    private $serialNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="manufacturer", type="string", length=128, nullable=true)
     */
    private $manufacturer;

     /**
     * @var int
     *
     * @ORM\Column(name="nodials", type="integer", nullable=true)
     *
     */
    private $nodials;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set customer
     *
     * @param Customer $customer
     *
     * @return Meter
     */
    public function setCustomer(Customer $customer)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * Get customer
     *
     * @return Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Meter
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return Meter
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }



    /**
     * Set status
     *
     * @param string $status
     *
     * @return Meter
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set distributionTransformer
     *
     * @param DistributionTransformer $distributionTransformer
     *
     * @return Meter
     */
    public function setDistributionTransformer(DistributionTransformer $distributionTransformer)
    {
        $this->distributionTransformer = $distributionTransformer;

        return $this;
    }

    /**
     * Get distributionTransformer
     *
     * @return DistributionTransformer
     */
    public function getDistributionTransformer()
    {
        return $this->distributionTransformer;
    }

    /**
     * Set feeder
     *
     * @param Feeder $feeder
     *
     * @return Meter
     */
    public function setFeeder(Feeder $feeder)
    {
        $this->feeder = $feeder;

        return $this;
    }

    /**
     * Get feeder
     *
     * @return feeder
     */
    public function getFeeder()
    {
        return $this->feeder;
    }

    /**
     * Set serialNumber
     *
     * @param string $serialNumber
     *
     * @return Meter
     */
    public function setSerialNumber($serialNumber)
    {
        $this->serialNumber = $serialNumber;

        return $this;
    }

    /**
     * Get serialNumber
     *
     * @return string
     */
    public function getSerialNumber()
    {
        return $this->serialNumber;
    }

    /**
     * Set manufacturer
     *
     * @param string $manufacturer
     *
     * @return Meter
     */
    public function setManufacturer($manufacturer)
    {
        $this->manufacturer = $manufacturer;

        return $this;
    }

    /**
     * Get manufacturer
     *
     * @return string
     */
    public function getManufacturer()
    {
        return $this->manufacturer;
    }

    /**
     * Get nodials
     *
     * @return integer
     */
    public function getNodials()
    {
        return $this->nodials;
    }

    /**
     * Set nodials
     *
     * @param integer $nodials
     *
     * @return Meter
     */
    public function setNodials($nodials)
    {
        $this->nodials = $nodials;

        return $this;
    }
}
