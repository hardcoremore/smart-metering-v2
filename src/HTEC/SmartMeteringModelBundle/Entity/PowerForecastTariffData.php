<?php

namespace HTEC\SmartMeteringModelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PowerForecastTariffData
 *
 * @ORM\Table(name="power_forecast_tariff_data")
 * @ORM\Entity()
 */
class PowerForecastTariffData
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

     /**
     * @ORM\ManyToOne(targetEntity="PowerForecast", inversedBy="powerForecastTariffData")
     * @ORM\JoinColumn(name="power_forecast_id", referencedColumnName="id")
     */
    private $powerForecast;

    /**
     * @ORM\ManyToOne(targetEntity="Tariff")
     * @ORM\JoinColumn(name="tariff_id", referencedColumnName="id")
     */
    private $tariff;

    /**
     * @var int
     *
     * @ORM\Column(name="number_of_customers", type="integer")
     */
    private $numberOfCustomers;

    /**
     * @var string
     *
     * @ORM\Column(name="forecast_rate", type="decimal", precision=20, scale=4)
     */
    private $forecastRate;

    /**
     * @var string
     *
     * @ORM\Column(name="share_of_power_allocation_on_grid", type="decimal", precision=20, scale=4)
     */
    private $shareOfPowerAllocationOnGrid;

    /**
     * @var string
     *
     * @ORM\Column(name="share_of_power_allocation_per_customer", type="decimal", precision=20, scale=4)
     */
    private $shareOfPowerAllocationPerCustomer;

    /**
     * @var string
     *
     * @ORM\Column(name="tariffs_code", type="string", length=8)
     */
    private $tariffsCode;

    /**
     * @var string
     *
     * @ORM\Column(name="tariffs_price", type="decimal", precision=20, scale=4)
     */
    private $tariffsPrice;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set powerForecast
     *
     * @param integer $powerForecast
     *
     * @return PowerForecastTariffData
     */
    public function setPowerForecast(PowerForecast $powerForecast)
    {
        $this->powerForecast = $powerForecast;

        return $this;
    }

    /**
     * Get powerForecast
     *
     * @return PowerForecast
     */
    public function getPowerForecast()
    {
        return $this->powerForecast;
    }

    /**
     * Set tariff
     *
     * @param Tariff $tariff
     *
     * @return PowerForecastTariffData
     */
    public function setTariff(Tariff $tariff)
    {
        $this->tariff = $tariff;

        return $this;
    }

    /**
     * Get tariff
     *
     * @return Tariff
     */
    public function getTariff()
    {
        return $this->tariff;
    }

    /**
     * Set numberOfCustomers
     *
     * @param integer $numberOfCustomers
     *
     * @return PowerForecastTariffData
     */
    public function setNumberOfCustomers($numberOfCustomers)
    {
        $this->numberOfCustomers = $numberOfCustomers;

        return $this;
    }

    /**
     * Get numberOfCustomers
     *
     * @return int
     */
    public function getNumberOfCustomers()
    {
        return $this->numberOfCustomers;
    }

    /**
     * Set forecastRate
     *
     * @param string $forecastRate
     *
     * @return PowerForecastTariffData
     */
    public function setForecastRate($forecastRate)
    {
        $this->forecastRate = $forecastRate;

        return $this;
    }

    /**
     * Get forecastRate
     *
     * @return string
     */
    public function getForecastRate()
    {
        return $this->forecastRate;
    }

    /**
     * Set shareOfPowerAllocationOnGrid
     *
     * @param string $shareOfPowerAllocationOnGrid
     *
     * @return PowerForecastTariffData
     */
    public function setShareOfPowerAllocationOnGrid($shareOfPowerAllocationOnGrid)
    {
        $this->shareOfPowerAllocationOnGrid = $shareOfPowerAllocationOnGrid;

        return $this;
    }

    /**
     * Get shareOfPowerAllocationOnGrid
     *
     * @return string
     */
    public function getShareOfPowerAllocationOnGrid()
    {
        return $this->shareOfPowerAllocationOnGrid;
    }

    /**
     * Set shareOfPowerAllocationPerCustomer
     *
     * @param string $shareOfPowerAllocationPerCustomer
     *
     * @return PowerForecastTariffData
     */
    public function setShareOfPowerAllocationPerCustomer($shareOfPowerAllocationPerCustomer)
    {
        $this->shareOfPowerAllocationPerCustomer = $shareOfPowerAllocationPerCustomer;

        return $this;
    }

    /**
     * Get shareOfPowerAllocationPerCustomer
     *
     * @return string
     */
    public function getShareOfPowerAllocationPerCustomer()
    {
        return $this->shareOfPowerAllocationPerCustomer;
    }

     /**
     * Set tariffsCode
     *
     * @param string $tariffsCode
     *
     * @return PowerForecastTariffData
     */
    public function setTariffsCode($tariffsCode)
    {
        $this->tariffsCode = $tariffsCode;

        return $this;
    }

    /**
     * Get tariffsCode
     *
     * @return string
     */
    public function getTariffsCode()
    {
        return $this->tariffsCode;
    }

    /**
     * Set tariffsPrice
     *
     * @param string $tariffsPrice
     *
     * @return PowerForecastTariffData
     */
    public function setTariffsPrice($tariffsPrice)
    {
        $this->tariffsPrice = $tariffsPrice;

        return $this;
    }

    /**
     * Get tariffsPrice
     *
     * @return string
     */
    public function getTariffsPrice()
    {
        return $this->tariffsPrice;
    }
}

