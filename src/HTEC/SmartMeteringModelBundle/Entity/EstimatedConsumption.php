<?php

namespace HTEC\SmartMeteringModelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EstimatedConsumption
 *
 * @ORM\Table(name="estimated_consumptions")
 * @ORM\Entity(repositoryClass="HTEC\SmartMeteringModelBundle\Repository\EstimatedConsumptionRepository")
 */
class EstimatedConsumption
{
    const TYPE_ESTIMATED = 'estimated';
    const TYPE_TARIFF_AVERAGE = 'tariff-average';

    public function __construct()
    {
        $this->setCreatedDatetime(new \DateTime());
    }

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Customer")
     * @ORM\JoinColumn(name="customer_id", referencedColumnName="id")
     */
    private $customer;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start", type="date")
     */
    private $start;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end", type="date")
     */
    private $end;

    /**
     * @var int
     *
     * @ORM\Column(name="actual_hours_scheduled", type="decimal", precision=20, scale=2)
     */
    private $actualHoursScheduled;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=32)
     */
    private $type;

    /**
     * @var int
     *
     * @ORM\Column(name="estimated_consumption", type="decimal", precision=20, scale=2)
     */
    private $estimatedConsumption;

    /**
     * @var string
     *
     * @ORM\Column(name="diversity_factor", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $diversityFactor;

   /**
     * @ORM\ManyToOne(targetEntity="EstimatedLoad")
     * @ORM\JoinColumn(name="estimated_load_id", referencedColumnName="id")
     */
    private $estimatedLoad;

    /**
     * @var int
     *
     * @ORM\Column(name="calculated_load", type="integer")
     */
    private $calculatedLoad;

    /**
     * @ORM\ManyToOne(targetEntity="AdminPanelUser")
     * @ORM\JoinColumn(name="created_by_id", referencedColumnName="id")
     */
    private $createdBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_datetime", type="datetime")
     */
    private $createdDatetime;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set customer
     *
     * @param Customer $customer
     *
     * @return EstimatedConsumption
     */
    public function setCustomer(Customer $customer)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * Get customer
     *
     * @return Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * Set start
     *
     * @param \DateTime $start
     *
     * @return EstimatedConsumption
     */
    public function setStart($start)
    {
        $this->start = $start;

        return $this;
    }

    /**
     * Get start
     *
     * @return \DateTime
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * Set end
     *
     * @param \DateTime $end
     *
     * @return EstimatedConsumption
     */
    public function setEnd($end)
    {
        $this->end = $end;

        return $this;
    }

    /**
     * Get end
     *
     * @return \DateTime
     */
    public function getEnd()
    {
        return $this->end;
    }

    /**
     * Set actualHoursScheduled
     *
     * @param decimal $actualHoursScheduled
     *
     * @return EstimatedConsumption
     */
    public function setActualHoursScheduled($actualHoursScheduled)
    {
        $this->actualHoursScheduled = $actualHoursScheduled;

        return $this;
    }

    /**
     * Get actualHoursScheduled
     *
     * @return decimal
     */
    public function getActualHoursScheduled()
    {
        return $this->actualHoursScheduled;
    }

    /**
     * Set estimatedConsumption
     *
     * @param decimal $estimatedConsumption
     *
     * @return EstimatedConsumption
     */
    public function setEstimatedConsumption($estimatedConsumption)
    {
        $this->estimatedConsumption = $estimatedConsumption;

        return $this;
    }

    /**
     * Get estimatedConsumption
     *
     * @return decimal
     */
    public function getEstimatedConsumption()
    {
        return $this->estimatedConsumption;
    }

    /**
     * Set diversityFactor
     *
     * @param string $diversityFactor
     *
     * @return EstimatedConsumption
     */
    public function setDiversityFactor($diversityFactor)
    {
        $this->diversityFactor = $diversityFactor;

        return $this;
    }

    /**
     * Get diversityFactor
     *
     * @return string
     */
    public function getDiversityFactor()
    {
        return $this->diversityFactor;
    }

    /**
     * Set estimatedLoad
     *
     * @param EstimatedLoad $estimatedLoad
     *
     * @return EstimatedConsumption
     */
    public function setEstimatedLoad(EstimatedLoad $estimatedLoad)
    {
        $this->estimatedLoad = $estimatedLoad;

        return $this;
    }

    /**
     * Get estimatedLoad
     *
     * @return EstimatedLoad
     */
    public function getEstimatedLoad()
    {
        return $this->estimatedLoad;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return EstimatedConsumption
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set calculatedLoad
     *
     * @param integer $calculatedLoad
     *
     * @return EstimatedConsumption
     */
    public function setCalculatedLoad($calculatedLoad)
    {
        $this->calculatedLoad = $calculatedLoad;

        return $this;
    }

    /**
     * Get calculatedLoad
     *
     * @return int
     */
    public function getCalculatedLoad()
    {
        return $this->calculatedLoad;
    }

    /**
     * Set createdBy
     *
     * @param AdminPanelUser $createdBy
     * @return Appliance
     */
    public function setCreatedBy(AdminPanelUser $createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return AdminPanelUser 
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set createdDatetime
     *
     * @param \DateTime $createdDatetime
     * @return Appliance
     */
    public function setCreatedDatetime($createdDatetime)
    {
        $this->createdDatetime = $createdDatetime;

        return $this;
    }

    /**
     * Get createdDatetime
     *
     * @return \DateTime 
     */
    public function getCreatedDatetime()
    {
        return $this->createdDatetime;
    }
}
