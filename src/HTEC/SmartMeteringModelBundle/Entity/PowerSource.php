<?php

namespace HTEC\SmartMeteringModelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * PowerSource
 *
 * @ORM\Table(name="power_sources")
 * @ORM\Entity(repositoryClass="HTEC\SmartMeteringModelBundle\Repository\PowerSourceRepository")
 */
class PowerSource
{
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->powerPurchases = new ArrayCollection();
        $this->outputFeeders = new ArrayCollection();
        $this->powerSchedules = new ArrayCollection();
    }

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=64, unique=true)
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="capacity", type="integer")
     */
    private $capacity;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255)
     */
    private $address;

    /**
    * @ORM\ManyToMany(targetEntity="Feeder")
    * @ORM\JoinTable(name="power_source_output_feeders",
    *      joinColumns={@ORM\JoinColumn(name="power_source_id", referencedColumnName="id")},
    *      inverseJoinColumns={@ORM\JoinColumn(name="feeder_id", referencedColumnName="id")}
    *      )
    *
    */
    private $outputFeeders;

    /**
    *
    * @ORM\OneToMany(targetEntity="PowerPurchase", mappedBy="powerSource")
    *
    */
    private $powerPurchases;

    /**
    *
    * @ORM\OneToMany(targetEntity="InjectionSubstation", mappedBy="powerSource")
    *
    */
    private $injectionSubstations;

    /**
    *
    * @ORM\OneToMany(targetEntity="PowerSchedule", mappedBy="powerSource")
    *
    */
    private $powerSchedules;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return PowerSource
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set capacity
     *
     * @param integer $capacity
     *
     * @return PowerSource
     */
    public function setCapacity($capacity)
    {
        $this->capacity = $capacity;

        return $this;
    }

    /**
     * Get capacity
     *
     * @return int
     */
    public function getCapacity()
    {
        return $this->capacity;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return PowerSource
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Add powerPurchase
     *
     * @param PowerPurchase $powerPurchase
     * @return City
     */
    public function addPowerPurchase(PowerPurchase $powerPurchase)
    {
        $this->powerPurchases[] = $powerPurchase;

        return $this;
    }

    /**
     * Remove powerPurchase
     *
     * @param PowerPurchase $powerPurchase
     */
    public function removePowerPurchase(PowerPurchase $powerPurchase)
    {
        $this->powerPurchases->removeElement($powerPurchase);
    }

    /**
     * Get powerPurchases
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPowerPurchases()
    {
        return $this->powerPurchases;
    }

    /**
     * Add outputFeeders
     *
     * @param Feeder $feeder
     * @return SiteUser
     */
    public function addOutputFeeder(Feeder $feeder)
    {
        $this->outputFeeders[] = $feeder;

        return $this;
    }

    /**
     * Remove outputFeeders
     *
     * @param Feeder $feeder
     */
    public function removeOutputFeeder(Feeder $feeder)
    {
        $this->outputFeeders->removeElement($feeder);
    }

    /**
     * Get outputFeeders
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getOutputFeeders()
    {
       return $this->outputFeeders;
    }

    /**
     * Add injectionSubstation
     *
     * @param InjectionSubstation $injectionSubstation
     * @return PowerSource
     */
    public function addInjectionSubstation(InjectionSubstation $injectionSubstation)
    {
        $this->injectionSubstations[] = $injectionSubstation;

        return $this;
    }

    /**
     * Remove injectionSubstation
     *
     * @param InjectionSubstation $injectionSubstation
     */
    public function removeInjectionSubstation(InjectionSubstation $injectionSubstation)
    {
        $this->injectionSubstations->removeElement($injectionSubstation);
    }

    /**
     * Get injectionSubstations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getInjectionSubstations()
    {
        return $this->injectionSubstations;
    }

    /**
     * Add powerSchedule
     *
     * @param PowerSchedule $powerSchedule
     * @return PowerSource
     */
    public function addPowerSchedule(PowerSchedule $powerSchedule)
    {
        $this->powerSchedules[] = $powerSchedule;

        return $this;
    }

    /**
     * Remove powerSchedule
     *
     * @param PowerSchedule $powerSchedule
     */
    public function removePowerSchedule(PowerSchedule $powerSchedule)
    {
        $this->powerSchedules->removeElement($powerSchedule);
    }

    /**
     * Get powerSchedules
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPowerSchedules()
    {
        return $this->powerSchedules;
    }
}
