<?php

namespace HTEC\SmartMeteringModelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * InjectionSubstation
 *
 * @ORM\Table(name="injection_substations", uniqueConstraints={@ORM\UniqueConstraint(name="injection_substation_name", columns={"name"})})
 * @ORM\Entity(repositoryClass="HTEC\SmartMeteringModelBundle\Repository\InjectionSubstationRepository")
 *
 * @UniqueEntity(fields="name", message="Injection Substation with this name already exists")
 *
 */
class InjectionSubstation
{

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->inputFeeders = new ArrayCollection();
        $this->powerTransformers = new ArrayCollection();
        $this->powerSchedules = new ArrayCollection();
    }

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=64, unique=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255, nullable=true)
     */
    private $address;

     /**
     * @ORM\ManyToOne(targetEntity="BusinessDistrict", inversedBy="injectionSubstations")
     * @ORM\JoinColumn(name="business_district_id", referencedColumnName="id")
     */
    private $businessDistrict;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=32)
     */
    private $code;

     /**
     * @ORM\ManyToOne(targetEntity="PowerSource", inversedBy="injectionSubstations")
     * @ORM\JoinColumn(name="power_source_id", referencedColumnName="id")
     */
    private $powerSource;

    /**
    * @ORM\ManyToMany(targetEntity="Feeder")
    * @ORM\JoinTable(name="injection_substation_input_feeders",
    *      joinColumns={@ORM\JoinColumn(name="injection_substation_id", referencedColumnName="id")},
    *      inverseJoinColumns={@ORM\JoinColumn(name="feeder_id", referencedColumnName="id")}
    *      )
    *
    */
    private $inputFeeders;

    /**
    *
    * @ORM\OneToMany(targetEntity="PowerTransformer", mappedBy="injectionSubstation")
    *
    */
    private $powerTransformers;

    /**
    *
    * @ORM\OneToMany(targetEntity="PowerSchedule", mappedBy="injectionSubstation")
    *
    */
    private $powerSchedules;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return InjectionSubstation
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return InjectionSubstation
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set businessDistrict
     *
     * @param BusinessDistrict $businessDistrict
     *
     * @return InjectionSubstation
     */
    public function setBusinessDistrict(BusinessDistrict $businessDistrict = null)
    {
        $this->businessDistrict = $businessDistrict;

        return $this;
    }

    /**
     * Get businessDistrict
     *
     * @return BusinessDistrict
     */
    public function getBusinessDistrict()
    {
        return $this->businessDistrict;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return InjectionSubstation
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

     /**
     * Set powerSource
     *
     * @param PowerSource $powerSource
     *
     * @return InjectionSubstation
     */
    public function setPowerSource(PowerSource $powerSource = null)
    {
        $this->powerSource = $powerSource;

        return $this;
    }

    /**
     * Get powerSource
     *
     * @return PowerSource
     */
    public function getPowerSource()
    {
        return $this->powerSource;
    }

    /**
     * Add powerSourceFeeder
     *
     * @param Feeder $feeder
     * @return InjectionSubstation
     */
    public function addInputFeeder(Feeder $feeder)
    {
        $this->inputFeeders[] = $feeder;

        return $this;
    }

    /**
     * Remove powerSourceFeeder
     *
     * @param Feeder $feeder
     */
    public function removeInputFeeder(Feeder $feeder)
    {
        $this->inputFeeders->removeElement($feeder);
    }

    /**
     * Get inputFeeders
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getInputFeeders()
    {
       return $this->inputFeeders;
    }

     /**
     * Add powerTransformer
     *
     * @param PowerTransformer $powerTransformer
     * @return City
     */
    public function addPowerTransformer(PowerTransformer $powerTransformer)
    {
        $this->powerTransformers[] = $powerTransformer;

        return $this;
    }

    /**
     * Remove powerTransformer
     *
     * @param PowerTransformer $powerTransformer
     */
    public function removePowerTransformer(PowerTransformer $powerTransformer)
    {
        $this->powerTransformers->removeElement($powerTransformer);
    }

    /**
     * Get injectionSubstations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPowerTransformers()
    {
        return $this->powerTransformers;
    }

    /**
     * Add powerSchedule
     *
     * @param PowerSchedule $powerSchedule
     * @return InjectionSubstation
     */
    public function addPowerSchedule(PowerSchedule $powerSchedule)
    {
        $this->powerSchedules[] = $powerSchedule;

        return $this;
    }

    /**
     * Remove powerSchedule
     *
     * @param PowerSchedule $powerSchedule
     */
    public function removePowerSchedule(PowerSchedule $powerSchedule)
    {
        $this->powerSchedules->removeElement($powerSchedule);
    }

    /**
     * Get powerSchedules
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPowerSchedules()
    {
        return $this->powerSchedules;
    }
}

