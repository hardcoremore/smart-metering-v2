<?php

namespace HTEC\SmartMeteringModelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * DistributionTransformer
 *
 * @ORM\Table(name="distribution_transformers")
 * @ORM\Entity(repositoryClass="HTEC\SmartMeteringModelBundle\Repository\DistributionTransformerRepository")
 */
class DistributionTransformer
{
    const TYPE_PUBLIC = 'PUBLIC';
    const TYPE_PRIVATE = 'PRIVATE';

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->outputFeeders = new ArrayCollection();
        $this->meters = new ArrayCollection();
        $this->powerSchedules = new ArrayCollection();
        $this->setType(self::TYPE_PUBLIC);
    }

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=64, unique=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=32)
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=32)
     */
    private $code;

    /**
     * @var decimal
     *
     * @ORM\Column(name="rating", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $rating;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=16)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="location", type="string", length=512, nullable=true)
     */
    private $location;

    /**
     * @ORM\ManyToOne(targetEntity="BusinessDistrict")
     * @ORM\JoinColumn(name="business_district_id", referencedColumnName="id")
     */
    private $businessDistrict;

    /**
     * @ORM\ManyToOne(targetEntity="InjectionSubstation")
     * @ORM\JoinColumn(name="injection_substation_id", referencedColumnName="id")
     */
    private $injectionSubstation;

    /**
     * @ORM\ManyToOne(targetEntity="PowerTransformer", inversedBy="distributionTransformers")
     * @ORM\JoinColumn(name="power_transformer_id", referencedColumnName="id")
     */
    private $powerTransformer;

     /**
     * @ORM\ManyToOne(targetEntity="Feeder")
     * @ORM\JoinColumn(name="feeder_id", referencedColumnName="id")
     */
    private $inputFeeder;

   /**
     * @ORM\ManyToMany(targetEntity="Feeder")
     * @ORM\JoinTable(name="distribution_transformer_output_feeders",
     *      joinColumns={@ORM\JoinColumn(name="distribution_transformer_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="feeder_id", referencedColumnName="id")}
     *      )
     */
    private $outputFeeders;

    /**
    *
    * @ORM\OneToMany(targetEntity="Meter", mappedBy="distributionTransformer")
    *
    */
    private $meters;

    /**
    *
    * @ORM\OneToMany(targetEntity="PowerSchedule", mappedBy="distributionTransformer")
    *
    */
    private $powerSchedules;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return DistributionTransformer
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return DistributionTransformer
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return DistributionTransformer
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set rating
     *
     * @param string $rating
     *
     * @return DistributionTransformer
     */
    public function setRating($rating)
    {
        $this->rating = $rating;

        return $this;
    }

    /**
     * Get rating
     *
     * @return string
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return DistributionTransformer
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set location
     *
     * @param string $location
     *
     * @return DistributionTransformer
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return string
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set businessDistrict
     *
     * @param BusinessDistrict $businessDistrict
     *
     * @return DistributionTransformer
     */
    public function setBusinessDistrict(BusinessDistrict $businessDistrict)
    {
        $this->businessDistrict = $businessDistrict;

        return $this;
    }

    /**
     * Get businessDistrict
     *
     * @return BusinessDistrict
     */
    public function getBusinessDistrict()
    {
        return $this->businessDistrict;
    }

    /**
     * Set injectionSubstation
     *
     * @param InjectionSubstation $injectionSubstation
     *
     * @return DistributionTransformer
     */
    public function setInjectionSubstation(InjectionSubstation $injectionSubstation)
    {
        $this->injectionSubstation = $injectionSubstation;

        return $this;
    }

    /**
     * Get injectionSubstation
     *
     * @return InjectionSubstation
     */
    public function getInjectionSubstation()
    {
        return $this->injectionSubstation;
    }

    /**
     * Set powerTransformer
     *
     * @param PowerTransformer $powerTransformer
     *
     * @return DistributionTransformer
     */
    public function setPowerTransformer(PowerTransformer $powerTransformer)
    {
        $this->powerTransformer = $powerTransformer;

        return $this;
    }

    /**
     * Get powerTransformer
     *
     * @return PowerTransformer
     */
    public function getPowerTransformer()
    {
        return $this->powerTransformer;
    }

     /**
     * Set inputFeeder
     *
     * @param Feeder $inputFeeder
     *
     * @return DistributionTransformer
     */
    public function setInputFeeder(Feeder $inputFeeder)
    {
        $this->inputFeeder = $inputFeeder;

        return $this;
    }

    /**
     * Get inputFeeder
     *
     * @return Feeder
     */
    public function getInputFeeder()
    {
        return $this->inputFeeder;
    }

    /**
     * Add outputFeeders
     *
     * @param Feeder $feeder
     * @return PowerTransformer
     */
    public function addOutputFeeder(Feeder $feeder)
    {
        $this->outputFeeders[] = $feeder;

        return $this;
    }

    /**
     * Remove outputFeeder
     *
     * @param Feeder $feeder
     */
    public function removeOutputFeeder(Feeder $feeder)
    {
        $this->outputFeeders->removeElement($feeder);
    }

    /**
     * Get outputFeeders
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getOutputFeeders()
    {
       return $this->outputFeeders;
    }

    /**
     * Add meter
     *
     * @param Meter $meter
     * @return DistributionTransformer
     */
    public function addMeter(Meter $meter)
    {
        $this->meters[] = $meter;

        return $this;
    }

    /**
     * Remove meter
     *
     * @param Meter $meter
     */
    public function removeMeter(Meter $meter)
    {
        $this->meters->removeElement($meter);
    }

    /**
     * Get meters
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMeters()
    {
        return $this->meters;
    }

    /**
     * Add powerSchedule
     *
     * @param PowerSchedule $powerSchedule
     * @return DistributionTransformer
     */
    public function addPowerSchedule(PowerSchedule $powerSchedule)
    {
        $this->powerSchedules[] = $powerSchedule;

        return $this;
    }

    /**
     * Remove powerSchedule
     *
     * @param PowerSchedule $powerSchedule
     */
    public function removePowerSchedule(PowerSchedule $powerSchedule)
    {
        $this->powerSchedules->removeElement($powerSchedule);
    }

    /**
     * Get powerSchedules
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPowerSchedules()
    {
        return $this->powerSchedules;
    }
}

