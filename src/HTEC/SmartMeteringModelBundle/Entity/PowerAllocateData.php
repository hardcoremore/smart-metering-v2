<?php

namespace HTEC\SmartMeteringModelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PowerAllocateData
 *
 * @ORM\Table(name="power_allocate_data")
 * @ORM\Entity(repositoryClass="HTEC\SmartMeteringModelBundle\Repository\PowerAllocateDataRepository")
 */
class PowerAllocateData
{

     /**
     * Constructor
     */
    public function __construct()
    {
        $this->createdDatetime = new \DateTime();
    }

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

   
    /**
     * @ORM\ManyToOne(targetEntity="PowerAllocate")
     * @ORM\JoinColumn(name="power_allocate_id", referencedColumnName="id")
     */
    private $powerAllocate;

     /**
     * @ORM\ManyToOne(targetEntity="BusinessDistrict")
     * @ORM\JoinColumn(name="business_district_id", referencedColumnName="id")
     */
    private $businessDistrict;

    /**
     * @var string
     *
     * @ORM\Column(name="power_allocated", type="decimal", precision=20, scale=4)
     */
    private $powerAllocated;

    /**
     * @var string
     *
     * @ORM\Column(name="power_forecasted", type="decimal", precision=20, scale=4)
     */
    private $powerForecasted;

    /**
     * @var string
     *
     * @ORM\Column(name="power_override", type="decimal", precision=20, scale=4, nullable=true)
     */
    private $powerOverride;

    /**
     * @ORM\ManyToOne(targetEntity="AdminPanelUser")
     * @ORM\JoinColumn(name="created_by_id", referencedColumnName="id")
     */
    private $createdBy;

    /**
     * @ORM\ManyToOne(targetEntity="AdminPanelUser")
     * @ORM\JoinColumn(name="last_edited_by_id", referencedColumnName="id")
     */
    private $lastEditedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_datetime", type="datetime")
     */
    private $createdDatetime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_edited_datetime", type="datetime", nullable=true)
     */
    private $lastEditedDatetime;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set powerAllocate
     *
     * @param PowerAllocate $powerAllocate
     *
     * @return PowerAllocateData
     */
    public function setPowerAllocate(PowerAllocate $powerAllocate)
    {
        $this->powerAllocate = $powerAllocate;

        return $this;
    }

    /**
     * Get powerAllocate
     *
     * @return PowerAllocate
     */
    public function getPowerAllocate()
    {
        return $this->powerAllocate;
    }

    /**
     * Set businessDistrict
     *
     * @param integer $businessDistrict
     *
     * @return PowerAllocateData
     */
    public function setBusinessDistrict($businessDistrict)
    {
        $this->businessDistrict = $businessDistrict;

        return $this;
    }

    /**
     * Get businessDistrict
     *
     * @return int
     */
    public function getBusinessDistrict()
    {
        return $this->businessDistrict;
    }

    /**
     * Set powerAllocated
     *
     * @param string $powerAllocated
     *
     * @return PowerAllocateData
     */
    public function setPowerAllocated($powerAllocated)
    {
        $this->powerAllocated = $powerAllocated;

        return $this;
    }

    /**
     * Get powerAllocated
     *
     * @return string
     */
    public function getPowerAllocated()
    {
        return $this->powerAllocated;
    }


    /**
     * Set powerOverride
     *
     * @param decimal $powerOverride
     *
     * @return PowerAllocateData
     */
    public function setPowerOverride($powerOverride)
    {
        $this->powerOverride = $powerOverride;

        return $this;
    }

    /**
     * Get powerOverride
     *
     * @return decimal
     */
    public function getPowerOverride()
    {
        return $this->powerOverride;
    }



    /**
     * Set powerForecasted
     *
     * @param string $powerForecasted
     *
     * @return PowerAllocateData
     */
    public function setPowerForecasted($powerForecasted)
    {
        $this->powerForecasted = $powerForecasted;

        return $this;
    }

    /**
     * Get powerForecasted
     *
     * @return string
     */
    public function getPowerForecasted()
    {
        return $this->powerForecasted;
    }

    /**
     * Set createdBy
     *
     * @param AdminPanelUser $createdBy
     * @return Appliance
     */
    public function setCreatedBy(AdminPanelUser $createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return AdminPanelUser 
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set lastEditedBy
     *
     * @param AdminPanelUser $lastEditedBy
     * @return Appliance
     */
    public function setLastEditedBy(AdminPanelUser $lastEditedBy)
    {
        $this->lastEditedBy = $lastEditedBy;

        return $this;
    }

    /**
     * Get lastEditedBy
     *
     * @return AdminPanelUser
     */
    public function getLastEditedBy()
    {
        return $this->lastEditedBy;
    }

    /**
     * Set createdDatetime
     *
     * @param \DateTime $createdDatetime
     * @return Appliance
     */
    public function setCreatedDatetime($createdDatetime)
    {
        $this->createdDatetime = $createdDatetime;

        return $this;
    }

    /**
     * Get createdDatetime
     *
     * @return \DateTime 
     */
    public function getCreatedDatetime()
    {
        return $this->createdDatetime;
    }

    /**
     * Set lastEditedDatetime
     *
     * @param \DateTime $lastEditedDatetime
     * @return Appliance
     */
    public function setLastEditedDatetime($lastEditedDatetime)
    {
        $this->lastEditedDatetime = $lastEditedDatetime;

        return $this;
    }

    /**
     * Get lastEditedDatetime
     *
     * @return \DateTime 
     */
    public function getLastEditedDatetime()
    {
        return $this->lastEditedDatetime;
    }
}

