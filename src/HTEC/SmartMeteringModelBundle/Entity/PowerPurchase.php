<?php

namespace HTEC\SmartMeteringModelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PowerPurchase
 *
 * @ORM\Table(name="power_purchases")
 * @ORM\Entity(repositoryClass="HTEC\SmartMeteringModelBundle\Repository\PowerPurchaseRepository")
 */
class PowerPurchase
{
    const POWER_TYPE_NATIONAL = "national";
    const POWER_TYPE_CAPTIVE = "captive";
    const POWER_TYPE_EMBEDDED = "embedded";

    const STATUS_WAITING_APPROVAL = 'waiting_approval';
    const STATUS_APPROVED = 'approved';

    public function __construct()
    {
        $this->createdDatetime = new \DateTime();
        $this->setStatus(self::STATUS_WAITING_APPROVAL);
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="power_type", type="string", length=32)
     */
    private $powerType;

     /**
     * @ORM\ManyToOne(targetEntity="PowerSource", inversedBy="powerPurchases")
     * @ORM\JoinColumn(name="power_source_id", referencedColumnName="id")
     */
    private $powerSource;

    /**
     * @var integer
     *
     * @ORM\Column(name="amount_purchased", type="bigint")
     */
    private $amountPurchased;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_purchased", type="date")
     */
    private $datePurchased;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=16)
     */
    private $status;

    /**
     * @ORM\ManyToOne(targetEntity="AdminPanelUser", inversedBy="approvedPowerPurchases")
     * @ORM\JoinColumn(name="approved_by_id", referencedColumnName="id")
     */
    private $approvedBy;

    /**
     * @ORM\ManyToOne(targetEntity="AdminPanelUser", inversedBy="createdPowerPurchase")
     * @ORM\JoinColumn(name="created_by_id", referencedColumnName="id")
     */
    private $createdBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_datetime", type="datetime")
     */
    private $createdDatetime;

    /**
     * @ORM\ManyToOne(targetEntity="AdminPanelUser", inversedBy="lastEditedPowerPurchase")
     * @ORM\JoinColumn(name="last_edited_by_id", referencedColumnName="id")
     */
    private $lastEditedBy;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_edited_datetime", type="datetime", nullable=true)
     */
    private $lastEditedDatetime;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set powerType
     *
     * @param integer $powerType
     * @return PowerPurchase
     */
    public function setPowerType($powerType)
    {
        $this->powerType = $powerType;

        return $this;
    }

    /**
     * Get powerType
     *
     * @return integer 
     */
    public function getPowerType()
    {
        return $this->powerType;
    }

     /**
     * Set powerSource
     *
     * @param PowerSource $powerSource
     * @return PowerPurchase
     */
    public function setPowerSource(PowerSource $powerSource = null)
    {
        $this->powerSource = $powerSource;

        return $this;
    }

    /**
     * Get powerSource
     *
     * @return PowerSource 
     */
    public function getPowerSource()
    {
        return $this->powerSource;
    }

    /**
     * Set amountPurchased
     *
     * @param integer $amountPurchased
     * @return PowerPurchase
     */
    public function setAmountPurchased($amountPurchased)
    {
        $this->amountPurchased = $amountPurchased;

        return $this;
    }

    /**
     * Get amountPurchased
     *
     * @return integer 
     */
    public function getAmountPurchased()
    {
        return $this->amountPurchased;
    }

    /**
     * Set datePurchased
     *
     * @param \DateTime $datePurchased
     * @return PowerPurchase
     */
    public function setDatePurchased($datePurchased)
    {
        $this->datePurchased = $datePurchased;

        return $this;
    }

    /**
     * Get datePurchased
     *
     * @return \DateTime 
     */
    public function getDatePurchased()
    {
        return $this->datePurchased;
    }

     /**
     * Set status
     *
     * @param boolean $status
     * @return PowerPurchase
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set approvedBy
     *
     * @param AdminPanelUser $approvedBy
     * @return PowerPurchase
     */
    public function setApprovedBy(AdminPanelUser $approvedBy = null)
    {
        $this->approvedBy = $approvedBy;

        return $this;
    }

    /**
     * Get approvedBy
     *
     * @return User 
     */
    public function getApprovedBy()
    {
        return $this->approvedBy;
    }
    
    /**
     * Set createdDatetime
     *
     * @param \DateTime $createdDatetime
     * @return PowerPurchase
     */
    public function setCreatedDatetime($createdDatetime)
    {
        $this->createdDatetime = $createdDatetime;

        return $this;
    }

    /**
     * Get createdDatetime
     *
     * @return \DateTime 
     */
    public function getCreatedDatetime()
    {
        return $this->createdDatetime;
    }

    /**
     * Set lastEditedDatetime
     *
     * @param \DateTime $lastEditedDatetime
     * @return PowerPurchase
     */
    public function setLastEditedDatetime($lastEditedDatetime)
    {
        $this->lastEditedDatetime = $lastEditedDatetime;

        return $this;
    }

    /**
     * Get lastEditedDatetime
     *
     * @return \DateTime 
     */
    public function getLastEditedDatetime()
    {
        return $this->lastEditedDatetime;
    }

    /**
     * Set createdBy
     *
     * @param AdminPanelUser $createdBy
     * @return PowerPurchase
     */
    public function setCreatedBy(AdminPanelUser $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return User 
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set lastEditedBy
     *
     * @param AdminPanelUser $lastEditedBy
     * @return PowerPurchase
     */
    public function setLastEditedBy(AdminPanelUser $lastEditedBy = null)
    {
        $this->lastEditedBy = $lastEditedBy;

        return $this;
    }

    /**
     * Get lastEditedBy
     *
     * @return User 
     */
    public function getLastEditedBy()
    {
        return $this->lastEditedBy;
    }
}
