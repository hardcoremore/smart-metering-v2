<?php

namespace HTEC\SmartMeteringModelBundle\Strategy\Search\Quick;

use HTEC\BaseModelBundle\Strategy\Search\BaseSearchStrategy;

use Doctrine\ORM\QueryBuilder;

class ApplianceQuickSearchStrategy extends BaseSearchStrategy
{
    public function bindParameters(QueryBuilder $queryBuilder)
    {
        $searchRule = $this->getQuickSearchRule();

        switch($searchRule->field)
        {
            case "applianceType":
            case "type":

                $queryBuilder->innerJoin("pr.applianceType", 'at', 'WITH', 'at = pr.applianceType');
                $this->bindSearchRule($queryBuilder, $searchRule, 'at.name');

            break;

            default:
                $searchRule->field = 'name';
                $this->bindSearchRule($queryBuilder, $searchRule, 'pr.' . $searchRule->field);
            break;
        }
    }
}