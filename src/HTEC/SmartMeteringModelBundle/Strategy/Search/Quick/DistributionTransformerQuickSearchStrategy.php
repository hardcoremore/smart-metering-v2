<?php

namespace HTEC\SmartMeteringModelBundle\Strategy\Search\Quick;

use HTEC\BaseModelBundle\Strategy\Search\BaseSearchStrategy;

use Doctrine\ORM\QueryBuilder;

class DistributionTransformerQuickSearchStrategy extends BaseSearchStrategy
{
    public function bindParameters(QueryBuilder $queryBuilder)
    {
        $searchRule = $this->getQuickSearchRule();

        switch($searchRule->field)
        {   
            case "transformer":
            case "power":
            case "powerTransformer":

                $queryBuilder->innerJoin("pr.powerTransformer", 'pt', 'WITH', 'c = pt.powerTransformer');
                $this->bindSearchRule($queryBuilder, $searchRule, 'pt.name');

            break;

            default:

                if(strlen($searchRule->field) < 1)
                {
                    $searchRule->field = 'name';    
                }
                
                $this->bindSearchRule($queryBuilder, $searchRule, 'pr.' . $searchRule->field);

            break;
        }
    }
}