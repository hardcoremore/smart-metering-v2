<?php

namespace HTEC\SmartMeteringModelBundle\Strategy\Search\Quick;

use HTEC\BaseModelBundle\Strategy\Search\BaseSearchStrategy;

use Doctrine\ORM\QueryBuilder;

class PowerPurchaseQuickSearchStrategy extends BaseSearchStrategy
{
    public function bindParameters(QueryBuilder $queryBuilder)
    {
        $searchRule = $this->getQuickSearchRule();

        switch($searchRule->field)
        {
            case "type":
            case "powerType":

                if($searchRule->data === 'national')
                {
                    $searchRule->data = 1;
                }
                else if($searchRule->data === 'captive') {
                    $searchRule->data = 2;
                }

                $this->bindSearchRule($queryBuilder, $searchRule, 'pr.powerType');

            break;

            case "createdBy":
            case "created":
            case "cby":
            case "by":

                $queryBuilder->innerJoin("pr.createdBy", 'cby', 'WITH', 'cby = pr.createdBy');

                $searchRule->groupOp = "or";
                
                $this->bindSearchRule($queryBuilder, $searchRule, 'cby.firstName');
                $this->bindSearchRule($queryBuilder, $searchRule, 'cby.lastName');
                $this->bindSearchRule($queryBuilder, $searchRule, 'cby.username');

            break;

            case "editedBy":
            case "edited":
            case "eby":

                $queryBuilder->innerJoin("pr.lastEditedBy", 'eby', 'WITH', 'eby = pr.lastEditedBy');

                $searchRule->groupOp = "or";
                
                $this->bindSearchRule($queryBuilder, $searchRule, 'eby.firstName');
                $this->bindSearchRule($queryBuilder, $searchRule, 'eby.lastName');
                $this->bindSearchRule($queryBuilder, $searchRule, 'eby.username');

            break;

            default:
                $this->bindSearchRule($queryBuilder, $searchRule, 'pr.amountPurchased');
            break;
        }
    }
}