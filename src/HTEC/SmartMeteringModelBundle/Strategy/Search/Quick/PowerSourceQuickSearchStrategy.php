<?php

namespace HTEC\SmartMeteringModelBundle\Strategy\Search\Quick;

use HTEC\BaseModelBundle\Strategy\Search\BaseSearchStrategy;

use Doctrine\ORM\QueryBuilder;

class PowerSourceQuickSearchStrategy extends BaseSearchStrategy
{
    public function bindParameters(QueryBuilder $queryBuilder)
    {
        $searchRule = $this->getQuickSearchRule();

        switch($searchRule->field)
        {
            default:
                $searchRule->field = 'name';
                $this->bindSearchRule($queryBuilder, $searchRule, 'pr.' . $searchRule->field);
            break;
        }
    }
}