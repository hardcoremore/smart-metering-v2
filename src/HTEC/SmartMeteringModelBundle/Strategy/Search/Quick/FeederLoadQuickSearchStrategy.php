<?php

namespace HTEC\SmartMeteringModelBundle\Strategy\Search\Quick;

use HTEC\BaseModelBundle\Strategy\Search\BaseSearchStrategy;

use Doctrine\ORM\QueryBuilder;

class FeederLoadQuickSearchStrategy extends BaseSearchStrategy
{
    public function bindParameters(QueryBuilder $queryBuilder)
    {
        $searchRule = $this->getQuickSearchRule();

        switch($searchRule->field)
        {
            default:
            case "powerTransformerFeeder":

                $queryBuilder->innerJoin("pr.powerTransformerFeeder", 'ptf', 'WITH', 'ptf = pr.powerTransformerFeeder');
                $this->bindSearchRule($queryBuilder, $searchRule, 'ptf.name');

            break;
        }
    }
}