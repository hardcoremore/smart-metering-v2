<?php

namespace HTEC\SmartMeteringModelBundle\Strategy\Search\Quick;

use HTEC\BaseModelBundle\Strategy\Search\BaseSearchStrategy;

use Doctrine\ORM\QueryBuilder;

class UserQuickSearchStrategy extends BaseSearchStrategy
{
    public function bindParameters(QueryBuilder $queryBuilder)
    {
        $searchRule = $this->getQuickSearchRule();

        switch($searchRule->field)
        {

            case "name":
            default:    
                $searchRule->groupOp = "or";

                $this->bindSearchRule($queryBuilder, $searchRule, 'pr.firstName');
                $this->bindSearchRule($queryBuilder, $searchRule, 'pr.lastName');

            break;

            case "first":    
                $this->bindSearchRule($queryBuilder, $searchRule, 'pr.firstName');
            break;

            case "last":
                $this->bindSearchRule($queryBuilder, $searchRule, 'pr.lastName');
            break;

            case "email":
            case "username":
            case "firstName":
            case "lastName":
                $this->bindSearchRule($queryBuilder, $searchRule, 'pr.' . $searchRule->field);
            break;
        }
    }
}