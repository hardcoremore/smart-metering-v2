<?php

namespace HTEC\SmartMeteringModelBundle\Strategy\Search\Quick;

use HTEC\BaseModelBundle\Strategy\Search\BaseSearchStrategy;

use Doctrine\ORM\QueryBuilder;

class ElectricPoleQuickSearchStrategy extends BaseSearchStrategy
{
    public function bindParameters(QueryBuilder $queryBuilder)
    {
        $searchRule = $this->getQuickSearchRule();

        switch($searchRule->field)
        {
            default:

                if(strlen($searchRule->field) < 1)
                {
                    $searchRule->field = 'code';    
                }
                
                $this->bindSearchRule($queryBuilder, $searchRule, 'pr.' . $searchRule->field);

            break;
        }
    }
}