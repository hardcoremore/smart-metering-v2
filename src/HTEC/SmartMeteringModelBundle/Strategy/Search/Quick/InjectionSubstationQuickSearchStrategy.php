<?php

namespace HTEC\SmartMeteringModelBundle\Strategy\Search\Quick;

use HTEC\BaseModelBundle\Strategy\Search\BaseSearchStrategy;

use Doctrine\ORM\QueryBuilder;

class InjectionSubstationQuickSearchStrategy extends BaseSearchStrategy
{
    public function bindParameters(QueryBuilder $queryBuilder)
    {
        $searchRule = $this->getQuickSearchRule();

        switch($searchRule->field)
        {
            case "district":
            case "businessDistrict":
            case "business":
                $queryBuilder->innerJoin("pr.businessDistrict", 'bd', 'WITH', 'bd = pr.businessDistrict');
                $this->bindSearchRule($queryBuilder, $searchRule, 'bd.name');

            break;

            default:

                if(strlen($searchRule->field) < 1)
                {
                    $searchRule->field = 'name';    
                }
                
                $this->bindSearchRule($queryBuilder, $searchRule, 'pr.' . $searchRule->field);

            break;
        }
    }
}