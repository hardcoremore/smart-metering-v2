<?php

namespace HTEC\SmartMeteringModelBundle\Strategy\Search\Quick;

use HTEC\BaseModelBundle\Strategy\Search\BaseSearchStrategy;

use Doctrine\ORM\QueryBuilder;

class CustomerQuickSearchStrategy extends BaseSearchStrategy
{
    public function bindParameters(QueryBuilder $queryBuilder)
    {
        $searchRule = $this->getQuickSearchRule();

        switch($searchRule->field)
        {
            case "tariff":

                $queryBuilder->innerJoin("pr.tariff", 't', 'WITH', 't = pr.tariff');
                $this->bindSearchRule($queryBuilder, $searchRule, 't.name');

            break;

            case "district":
            case "business":
            case "businessDistrict":

                $queryBuilder->innerJoin("pr.businessDistrict", 'bs', 'WITH', 'bs = pr.businessDistrict');
                $this->bindSearchRule($queryBuilder, $searchRule, 'bs.name');

            break;

            default:

                $searchRule->field = 'name';
                $searchRule->groupOp = "or";               

                $this->bindSearchRule($queryBuilder, $searchRule, 'pr.name');
                $this->bindSearchRule($queryBuilder, $searchRule, 'pr.accountNumber');

            break;
        }
    }
}