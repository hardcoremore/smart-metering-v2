<?php

namespace HTEC\SmartMeteringModelBundle\Strategy\Search\Quick;

use HTEC\BaseModelBundle\Strategy\Search\BaseSearchStrategy;

use Doctrine\ORM\QueryBuilder;

class FeederQuickSearchStrategy extends BaseSearchStrategy
{
    public function bindParameters(QueryBuilder $queryBuilder)
    {
        $searchRule = $this->getQuickSearchRule();

        switch($searchRule->field)
        {
            case 'type':

                if(strpos($searchRule->data, 'high') !== false)
                {
                    $searchRule->data = 'high-voltage';
                }
                else
                {
                    $searchRule->data = 'low-voltage';
                }
                
                $this->bindSearchRule($queryBuilder, $searchRule, 'pr.' . $searchRule->field);
            break;

            default:

                if(strlen($searchRule->field) < 1)
                {
                    $searchRule->field = 'name'; 
                }
                
                $this->bindSearchRule($queryBuilder, $searchRule, 'pr.' . $searchRule->field);

            break;
        }
    }
}