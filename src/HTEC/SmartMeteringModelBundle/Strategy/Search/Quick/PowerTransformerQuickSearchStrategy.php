<?php

namespace HTEC\SmartMeteringModelBundle\Strategy\Search\Quick;

use HTEC\BaseModelBundle\Strategy\Search\BaseSearchStrategy;

use Doctrine\ORM\QueryBuilder;

class PowerTransformerQuickSearchStrategy extends BaseSearchStrategy
{
    public function bindParameters(QueryBuilder $queryBuilder)
    {
        $searchRule = $this->getQuickSearchRule();

        switch($searchRule->field)
        {
            case "station":
            case "injection":
            case "injectionSubstation":

                $queryBuilder->innerJoin("pr.injectionSubstation", 'is', 'WITH', 'is = pr.injectionSubstation');
                $this->bindSearchRule($queryBuilder, $searchRule, 'is.name');

            break;

            default:

                if(strlen($searchRule->field) < 1)
                {
                    $searchRule->field = 'name';    
                }
                
                $this->bindSearchRule($queryBuilder, $searchRule, 'pr.' . $searchRule->field);

            break;
        }
    }
}