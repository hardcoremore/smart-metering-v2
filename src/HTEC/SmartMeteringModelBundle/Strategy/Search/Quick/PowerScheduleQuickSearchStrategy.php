<?php

namespace HTEC\SmartMeteringModelBundle\Strategy\Search\Quick;

use HTEC\BaseModelBundle\Strategy\Search\BaseSearchStrategy;

use Doctrine\ORM\QueryBuilder;

class PowerScheduleQuickSearchStrategy extends BaseSearchStrategy
{
    public function bindParameters(QueryBuilder $queryBuilder)
    {
        $searchRule = $this->getQuickSearchRule();

        switch($searchRule->field)
        { 
            case 'powerSource':

                $queryBuilder->innerJoin("pr.powerSource", 'ps', 'WITH', 'ps = pr.powerSource');
                $this->bindSearchRule($queryBuilder, $searchRule, 'ps.name');

            break;
            
            default:
            case 'bd':
            case 'district':
            case 'business':
            case 'businessDistrict':

                $searchRule->field = 'businessDistrict';

                $queryBuilder->innerJoin("pr.businessDistrict", 'bd', 'WITH', 'bd = pr.businessDistrict');
                $this->bindSearchRule($queryBuilder, $searchRule, 'bd.name');

            break;

            case "is":
            case "station":
            case "injection":
            case "injectionSubstation":

                $queryBuilder->innerJoin("pr.injectionSubstation", 'insub', 'WITH', 'insub = pr.injectionSubstation');
                $this->bindSearchRule($queryBuilder, $searchRule, 'insub.name');

            break;

            case "pt":
            case "pTransformer":
            case "powerTransformer":

                $queryBuilder->innerJoin("pr.powerTransformer", 'pt', 'WITH', 'pt = pr.powerTransformer');
                $this->bindSearchRule($queryBuilder, $searchRule, 'pt.name');

            break;

            case "ptf":
            case "ptFeeder":
            case "powerTransformerFeeder":

                $queryBuilder->innerJoin("pr.powerTransformerFeeder", 'ptf', 'WITH', 'ptf = pr.powerTransformerFeeder');
                $this->bindSearchRule($queryBuilder, $searchRule, 'ptf.name');

            break;

            case "dt":
            case "dTransformer":
            case "distributionTransformer":

                $queryBuilder->innerJoin("pr.distributionTransformer", 'dt', 'WITH', 'dt = pr.distributionTransformer');
                $this->bindSearchRule($queryBuilder, $searchRule, 'dt.name');

            break;

            case "dtf":
            case "dtFeeder":
            case "distributionTransformerFeeder":

                $queryBuilder->innerJoin("pr.distributionTransformerFeeder", 'dtf', 'WITH', 'dtf = pr.distributionTransformerFeeder');
                $this->bindSearchRule($queryBuilder, $searchRule, 'dtf.name');

            break;
        }
    }
}