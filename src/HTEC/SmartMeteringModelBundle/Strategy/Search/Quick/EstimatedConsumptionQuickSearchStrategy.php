<?php

namespace HTEC\SmartMeteringModelBundle\Strategy\Search\Quick;

use HTEC\BaseModelBundle\Strategy\Search\BaseSearchStrategy;

use Doctrine\ORM\QueryBuilder;

class EstimatedConsumptionQuickSearchStrategy extends BaseSearchStrategy
{
    public function bindParameters(QueryBuilder $queryBuilder)
    {
        $searchRule = $this->getQuickSearchRule();

        switch($searchRule->field)
        {
            case "category":
            case "customerCategory":

                $queryBuilder->innerJoin("pr.customerCategory", 'cr', 'WITH', 'cr = pr.customerCategory');
                $this->bindSearchRule($queryBuilder, $searchRule, 'cr.name');

            break;

            case "customer":

                $queryBuilder->innerJoin("pr.customer", 'c', 'WITH', 'c = pr.customer');
                $this->bindSearchRule($queryBuilder, $searchRule, 'c.name');

            break;

            case 'load':
            case 'estimatedLoad':
                
                $this->bindSearchRule($queryBuilder, $searchRule, 'pr.calculatedLoad');

            break;

            case 'consumption':
            case 'estimatedConsumption':
            default:

                if(strlen($searchRule->field) < 1)
                {
                    $searchRule->field = 'estimatedConsumption';
                }
                
                $this->bindSearchRule($queryBuilder, $searchRule, 'pr.' . $searchRule->field);

            break;
        }
    }
}