<?php

namespace HTEC\SmartMeteringModelBundle\Strategy\Search\Quick;

use HTEC\BaseModelBundle\Strategy\Search\BaseSearchStrategy;

use Doctrine\ORM\QueryBuilder;

class CustomerCategoryQuickSearchStrategy extends BaseSearchStrategy
{
    public function bindParameters(QueryBuilder $queryBuilder)
    {
        $searchRule = $this->getQuickSearchRule();

        if(strlen($searchRule->field) < 1)
        {
            $searchRule->field = 'name';
        }

        $this->bindSearchRule($queryBuilder, $searchRule, 'pr.' . $searchRule->field);
    }
}