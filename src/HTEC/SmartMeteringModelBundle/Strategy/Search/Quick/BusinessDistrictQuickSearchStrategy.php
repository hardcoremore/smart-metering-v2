<?php

namespace HTEC\SmartMeteringModelBundle\Strategy\Search\Quick;

use HTEC\BaseModelBundle\Strategy\Search\BaseSearchStrategy;

use Doctrine\ORM\QueryBuilder;

class BusinessDistrictQuickSearchStrategy extends BaseSearchStrategy
{
    public function bindParameters(QueryBuilder $queryBuilder)
    {
        $searchRule = $this->getQuickSearchRule();

        switch($searchRule->field)
        {
            case "city":

                $queryBuilder->innerJoin("pr.city", 'c', 'WITH', 'c = pr.city');
                $this->bindSearchRule($queryBuilder, $searchRule, 'c.name');

            break;

            default:
                $searchRule->field = 'name';
                $this->bindSearchRule($queryBuilder, $searchRule, 'pr.' . $searchRule->field);
            break;
        }
    }
}