<?php

namespace HTEC\SmartMeteringModelBundle\Strategy\Search\Quick;

use HTEC\BaseModelBundle\Strategy\Search\BaseSearchStrategy;

use Doctrine\ORM\QueryBuilder;

class MeterQuickSearchStrategy extends BaseSearchStrategy
{
    public function bindParameters(QueryBuilder $queryBuilder)
    {
        $searchRule = $this->getQuickSearchRule();

        switch($searchRule->field)
        {   
            case "transformer":
            case "distribution":
            case "distributionTransformer":

                $queryBuilder->innerJoin("pr.distributionTransformer", 'dt', 'WITH', 'dt = pr.distributionTransformer');
                $this->bindSearchRule($queryBuilder, $searchRule, 'dt.name');

            break;

            case "customer":

                $queryBuilder->innerJoin("pr.customer", 'c', 'WITH', 'c = pr.customer');
                $this->bindSearchRule($queryBuilder, $searchRule, 'c.name');

            break;

            case "serialNumber":
            case "number":
            default:

                $searchRule->field = 'serialNumber';
                $this->bindSearchRule($queryBuilder, $searchRule, 'pr.serialNumber');

            break;
        }
    }
}