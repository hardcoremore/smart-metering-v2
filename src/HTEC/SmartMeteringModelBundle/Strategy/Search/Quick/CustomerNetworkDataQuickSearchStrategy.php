<?php

namespace HTEC\SmartMeteringModelBundle\Strategy\Search\Quick;

use HTEC\BaseModelBundle\Strategy\Search\BaseSearchStrategy;

use Doctrine\ORM\QueryBuilder;

class CustomerNetworkDataQuickSearchStrategy extends BaseSearchStrategy
{
    public function bindParameters(QueryBuilder $queryBuilder)
    {
        $searchRule = $this->getQuickSearchRule();

        switch($searchRule->field)
        { 
            case 'customer':
            default:

                if(strlen($searchRule->field) < 1)
                {
                    $searchRule->field = 'customer';

                    $queryBuilder->innerJoin("pr.customer", 'c', 'WITH', 'c = pr.customer');

                    $searchRule->groupOp = "or";

                    $this->bindSearchRule($queryBuilder, $searchRule, 'c.name');
                    $this->bindSearchRule($queryBuilder, $searchRule, 'c.accountNumber');
                }
                else
                {
                    $this->bindSearchRule($queryBuilder, $searchRule, 'pr.accountNumber');
                }

            break;

            case "account":
            case "number":
            case "accountNumber":

                $queryBuilder->innerJoin("pr.customer", 'c', 'WITH', 'c = pr.customer');
                $this->bindSearchRule($queryBuilder, $searchRule, 'c.accountNumber');

            break;

            case "bd":
            case "district":
            case "businessDistrict":

                $queryBuilder->innerJoin("pr.customer", 'c', 'WITH', 'c = pr.customer');
                $queryBuilder->innerJoin("c.businessDistrict", 'bd', 'WITH', 'bd = c.businessDistrict');

                $this->bindSearchRule($queryBuilder, $searchRule, 'bd.name');

            break;

            case "station":
            case "injection":
            case "injectionSubstation":

                $queryBuilder->innerJoin("pr.injectionSubstation", 'injStat', 'WITH', 'injStat = pr.injectionSubstation');
                $this->bindSearchRule($queryBuilder, $searchRule, 'injStat.name');

            break;

            case "pt":
            case "pTransformer":
            case "powerTransformer":

                $queryBuilder->innerJoin("pr.powerTransformer", 'pt', 'WITH', 'pt = pr.powerTransformer');
                $this->bindSearchRule($queryBuilder, $searchRule, 'pt.name');

            break;

            case "ptf":
            case "ptFeeder":
            case "powerTransformerFeeder":

                $queryBuilder->innerJoin("pr.powerTransformerFeeder", 'ptf', 'WITH', 'ptf = pr.powerTransformerFeeder');
                $this->bindSearchRule($queryBuilder, $searchRule, 'ptf.name');

            break;

            case "htp":
            case "htPole":
            case "highTensionPole":

                $queryBuilder->innerJoin("pr.highTensionPole", 'htp', 'WITH', 'htp = pr.highTensionPole');
                $this->bindSearchRule($queryBuilder, $searchRule, 'htp.code');

            break;

            case "dt":
            case "dTransformer":
            case "distributionTransformer":

                $queryBuilder->innerJoin("pr.distributionTransformer", 'dt', 'WITH', 'dt = pr.distributionTransformer');
                $this->bindSearchRule($queryBuilder, $searchRule, 'dt.name');

            break;

            case "dtf":
            case "dtFeeder":
            case "distributionTransformerFeeder":

                $queryBuilder->innerJoin("pr.distributionTransformerFeeder", 'dtf', 'WITH', 'dtf = pr.distributionTransformerFeeder');
                $this->bindSearchRule($queryBuilder, $searchRule, 'dtf.name');

            break;

            case "ltp":
            case "ltPole":
            case "lowTensionPole":

                $queryBuilder->innerJoin("pr.lowTensionPole", 'ltp', 'WITH', 'ltp = pr.lowTensionPole');
                $this->bindSearchRule($queryBuilder, $searchRule, 'ltp.code');

            break;
        }
    }
}