<?php


namespace HTEC\SmartMeteringModelBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class OverrideServiceCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        $definition = $container->getDefinition('security.acl.permission.map');
        $definition->setClass('HTEC\SmartMeteringModelBundle\Security\Acl\Permission\SmartMeteringPermissionMap');
    }
}