<?php

namespace HTEC\SmartMeteringModelBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class SmartMeteringModelExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');
        $loader->load('services/models.yml');
        $loader->load('services/forms.yml');
        $loader->load('services/search_strategy.yml');

        $loader->load('entity_search_field_params.yml');

        $loader->load('forms/role.yml');
        $loader->load('forms/admin_panel_user.yml');
        $loader->load('forms/city.yml');
        $loader->load('forms/business_district.yml');
        $loader->load('forms/customer_category.yml');
        $loader->load('forms/customer_network_data.yml');
        $loader->load('forms/power_source.yml');
        $loader->load('forms/power_purchase.yml');
        $loader->load('forms/estimated_load.yml');
        $loader->load('forms/estimated_load_appliance.yml');
        $loader->load('forms/estimated_consumption.yml');
        $loader->load('forms/appliance_type.yml');
        $loader->load('forms/appliance.yml');
        $loader->load('forms/tariff.yml');
        $loader->load('forms/power_schedule.yml');
        $loader->load('forms/power_activate_deactivate.yml');
        $loader->load('forms/feeder_load.yml');

        $loader->load('forms/power_forecast.yml');
        $loader->load('forms/power_forecast_tariff_data.yml');
        $loader->load('forms/power_forecast_business_district_tariff_data.yml');
        $loader->load('forms/power_forecast_business_district_total.yml');

        $loader->load('forms/power_allocate.yml');
        $loader->load('forms/power_allocate_data.yml');

        $loader->load('forms/feeder.yml');
        $loader->load('forms/injection_substation.yml');
        $loader->load('forms/power_transformer.yml');
        $loader->load('forms/electric_pole.yml');
        $loader->load('forms/distribution_transformer.yml');
        $loader->load('forms/meter.yml');
    }
}

