<?php

namespace HTEC\SmartMeteringModelBundle\Security\Acl\Permission;

use Symfony\Component\Security\Acl\Permission\BasicPermissionMap;
use Symfony\Component\Security\Acl\Permission\MaskBuilder;

use HTEC\SmartMeteringModelBundle\Security\Acl\Permission\SmartMeteringMaskBuilder;

class SmartMeteringPermissionMap extends BasicPermissionMap
{
    const PERMISSION_APPROVE = 'approve';
    const PERMISSION_DECOMMISSION = 'decommission';
    const PERMISSION_CHANGE_PASSWORD = 'changePassword';


    public function __construct()
    {
        parent::__construct();

        $this->map[self::PERMISSION_APPROVE] = array(
            SmartMeteringMaskBuilder::MASK_APPROVE,
            MaskBuilder::MASK_OPERATOR,
            MaskBuilder::MASK_MASTER,
            MaskBuilder::MASK_OWNER,
        );
        
        $this->map[self::PERMISSION_DECOMMISSION] = array(
            SmartMeteringMaskBuilder::MASK_DECOMMISSION,
            MaskBuilder::MASK_OPERATOR,
            MaskBuilder::MASK_MASTER,
            MaskBuilder::MASK_OWNER,
        );
       
        $this->map[self::PERMISSION_CHANGE_PASSWORD] = array(
            SmartMeteringMaskBuilder::MASK_CHANGEPASSWORD,
            MaskBuilder::MASK_OPERATOR,
            MaskBuilder::MASK_MASTER,
            MaskBuilder::MASK_OWNER,
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getMaskBuilder()
    {
        return new SmartMeteringMaskBuilder();
    }
}