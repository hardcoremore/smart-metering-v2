<?php

namespace HTEC\SmartMeteringModelBundle\Security\Acl\Permission;

use Symfony\Component\Security\Acl\Permission\MaskBuilder;

class SmartMeteringMaskBuilder extends MaskBuilder
{
    const MASK_APPROVE          = 256;
    const MASK_DECOMMISSION     = 512;
    const MASK_CHANGEPASSWORD  = 1024;
}