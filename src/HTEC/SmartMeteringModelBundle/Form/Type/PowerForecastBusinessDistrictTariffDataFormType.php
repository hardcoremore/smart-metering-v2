<?php

namespace HTEC\SmartMeteringModelBundle\Form\Type;

use HTEC\BaseModelBundle\Form\Type\ConfigFormType;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class PowerForecastBusinessDistrictTariffDataFormType extends ConfigFormType
{
    public function __construct(ContainerInterface $container, RequestStack $requestStack, $formConfigName = 'power_forecast_business_district_tariff_data')
    {
        parent::__construct($container, $requestStack, $formConfigName);
    }
}