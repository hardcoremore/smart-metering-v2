<?php

namespace HTEC\SmartMeteringModelBundle\Form\Type;

use HTEC\BaseModelBundle\Form\Type\ConfigFormType;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class PowerSourceFormType extends ConfigFormType
{
    public function __construct(ContainerInterface $container, RequestStack $requestStack, $formConfigName = 'power_source')
    {
        parent::__construct($container, $requestStack, $formConfigName);
    }
}