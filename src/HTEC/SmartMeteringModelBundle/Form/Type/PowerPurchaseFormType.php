<?php

namespace HTEC\SmartMeteringModelBundle\Form\Type;

use HTEC\BaseModelBundle\Form\Type\ConfigFormType;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class PowerPurchaseFormType extends ConfigFormType
{
    public function __construct(ContainerInterface $container, RequestStack $requestStack, $formConfigName = 'power_purchase')
    {
        parent::__construct($container, $requestStack, $formConfigName);
    }
}