<?php

namespace HTEC\SmartMeteringModelBundle\Form\Type;

use HTEC\BaseModelBundle\Form\Type\UserFormType;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class AdminPanelUserFormType extends UserFormType
{
    public function __construct(ContainerInterface $container, RequestStack $requestStack, $formConfigName = 'admin_panel_user')
    {
        parent::__construct($container, $requestStack, $formConfigName);
    }
}