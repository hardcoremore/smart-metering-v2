<?php

namespace HTEC\SmartMeteringModelBundle;

use Symfony\Component\DependencyInjection\ContainerBuilder;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use HTEC\SmartMeteringModelBundle\DependencyInjection\Compiler\OverrideServiceCompilerPass;

class SmartMeteringModelBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new OverrideServiceCompilerPass());
    }
}
