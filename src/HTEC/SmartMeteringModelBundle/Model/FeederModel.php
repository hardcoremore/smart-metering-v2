<?php

namespace HTEC\SmartMeteringModelBundle\Model;

use HTEC\BaseModelBundle\Model\BaseModel;

class FeederModel extends BaseModel
{
    private $powerTransformerModel;
    private $distributionTransformerModel;

    public function setPowerTransformerModel(BaseModel $powerTransformerModel)
    {
        $this->powerTransformerModel = $powerTransformerModel;
    }

    public function getPowerTransformerModel()
    {
        return $this->powerTransformerModel;
    }

    public function setDistributionTransformerModel(BaseModel $distributionTransformerModel)
    {
        $this->distributionTransformerModel = $distributionTransformerModel;
    }

    public function getDistributionTransformerModel()
    {
        return $this->distributionTransformerModel;
    }

    public function readForSelect(array $queryParameters = null)
    {
        if($queryParameters !== null AND array_key_exists('outputFeedersForPowerTransformer', $queryParameters))
        {
            $powerTransformer = $this->getPowerTransformerModel()->get($queryParameters['outputFeedersForPowerTransformer']);

            if($powerTransformer)
            {
                return $powerTransformer->getOutputFeeders();
            }
            else
            {
                return [];
            }
        }
        else if($queryParameters !== null AND array_key_exists('outputFeedersForDistributionTransformer', $queryParameters))
        {
            $distributionTransformer = $this->getDistributionTransformerModel()->get($queryParameters['outputFeedersForDistributionTransformer']);

            if($distributionTransformer)
            {
                return $distributionTransformer->getOutputFeeders();
            }
            else
            {
                return [];
            }
        }
        else
        {
            return parent::readForSelect($queryParameters);
        }
    }
}