<?php

namespace HTEC\SmartMeteringModelBundle\Model;

use Symfony\Component\Security\Acl\Domain\RoleSecurityIdentity;
use Symfony\Component\Security\Acl\Model\AclProviderInterface;

use HTEC\SmartMeteringModelBundle\Model\PermissionModel;
use HTEC\BaseModelBundle\Model\UserModel;

use HTEC\SmartMeteringModelBundle\Security\RolePermissions;

class AdminPanelUserModel extends UserModel
{
    private $permissionModel;

    public function setPermissionModel(PermissionModel $permissionModel)
    {
        $this->permissionModel = $permissionModel;
    }

    public function getPermissionModel()
    {
        return $this->permissionModel;
    }

    public function get($id)
    {
        $entity = $this->entityManager->find($this->entityName, $id);

        if($this->getLoggedInUser()->getId() !== $id)
        {
            $this->checkViewPermission($this->entityManager->getClassMetadata($this->entityName)->getName());
        }

        $entity->setPermissions($this->getPermissionModel()->getUserPermissions($entity));

        return $entity;
    }

    protected function updateEntityBeforeCreate($entity, array $data)
    {
        parent::updateEntityBeforeCreate($entity, $data);
        $entity->setPermissions($this->getPermissionModel()->getUserPermissions($entity));
    }
}