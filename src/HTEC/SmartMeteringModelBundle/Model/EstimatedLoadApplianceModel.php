<?php

namespace HTEC\SmartMeteringModelBundle\Model;

use HTEC\BaseModelBundle\Model\BaseModel;
use HTEC\SmartMeteringModelBundle\Entity\EstimatedLoad;

class EstimatedLoadApplianceModel extends BaseModel
{
    private $estimatedLoadModel;

    public function setEstimatedLoadModel(BaseModel $estimatedLoadModel)
    {
        $this->estimatedLoadModel = $estimatedLoadModel;
    }

    public function getEstimatedLoadModel()
    {
        return $this->estimatedLoadModel;
    }

    public function create(array $data, bool $checkPermissions = true)
    {
        $entity = parent::create($data, $checkPermissions);

        $this->updateEstimatedLoadTotals($entity->getEstimatedLoad());

        return $entity;
    }
    
    protected function save($id, array $data, bool $isPatch = false, bool $checkPermissions = true)
    {
        $entity = parent::save($id, $data, $isPatch, $checkPermissions);

        $this->updateEstimatedLoadTotals($entity->getEstimatedLoad());

        return $entity;
    }

    public function delete($id)
    {
        $entity = parent::delete($id);

        $this->updateEstimatedLoadTotals($entity->getEstimatedLoad());
    }

    protected function updateEstimatedLoadTotals(EstimatedLoad $estimatedLoad)
    {
        $allEstimatedLoadAppliances = $this->readAll(['estimatedLoad' => $estimatedLoad->getId()]);

        $allEstimatedLoadAppliancesCount = count($allEstimatedLoadAppliances);
        $estimatedLoadTotalLoad = 0;

        foreach($allEstimatedLoadAppliances AS $estimatedLoadAppliance)
        {
            $estimatedLoadTotalLoad = $estimatedLoadTotalLoad + $estimatedLoadAppliance->getTotalConsumption();
        }

        $estimatedLoad->setTotalLoad($estimatedLoadTotalLoad);
        $estimatedLoad->setApplianceCount($allEstimatedLoadAppliancesCount);

        $this->entityManager->persist($estimatedLoad);
        $this->entityManager->flush();
    }
}