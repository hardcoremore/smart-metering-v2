<?php

namespace HTEC\SmartMeteringModelBundle\Model;

use HTEC\BaseModelBundle\Model\BaseModel;

class PowerAllocateDataModel extends BaseModel
{
    public function getDomain():string
    {
        return 'HTEC\SmartMeteringModelBundle\Entity\PowerAllocate';
    }
}