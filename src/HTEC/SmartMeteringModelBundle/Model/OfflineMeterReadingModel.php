<?php

namespace HTEC\SmartMeteringModelBundle\Model;

use HTEC\BaseModelBundle\Model\BaseModel;

use HTEC\BaseModelBundle\Strategy\Search\BaseSearchStrategy;
use HTEC\BaseModelBundle\Entity\PageParameters;

use HTEC\BaseAPIBundle\Interfaces\Requester;

class OfflineMeterReadingModel
{
    protected $requester;
    protected $baseUrl;

    public function setRequester(Requester $requester)
    {
        $this->requester = $requester;
    }

    public function getRequester():Requester
    {
        return $this->requester;
    }

    public function setBaseUrl(string $url)
    {
        $this->baseUrl = $url;
    }

    public function getBaseUrl():string
    {
        return $this->baseUrl;
    }

    public function getPagedData(array $parameters)
    {
        return $this->getRequester()->get($this->getBaseUrl() . '/readings/offline-meter-readings/list', $parameters);
    }

    public function getRecordAction($id)
    {
        return $this->getRequester()->get($this->getBaseUrl() . '/readings/offline-meter-readings/list/' . $id);
    }

    public function quickSearch(array $parameters)
    {
        return $this->getRequester()->get($this->getBaseUrl() . '/readings/quick-search', $parameters);
    }

    public function update($id, array $data)
    {
        if(empty($id) || is_numeric($id) === false)
        {
            throw new InvalidFormDataException("Reading id is invalid", 400);
        }
        else
        {
            return $this->getRequester()->put($this->getBaseUrl() . '/readings/id/' . $id . '/approve', $data);
        }
    }
}