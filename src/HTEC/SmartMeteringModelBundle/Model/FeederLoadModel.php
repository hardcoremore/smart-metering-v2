<?php

namespace HTEC\SmartMeteringModelBundle\Model;

use HTEC\BaseModelBundle\Model\BaseModel;

class FeederLoadModel extends BaseModel
{
    public function readAll(array $searchParameters = [], int $limit = -1)
    {
        if($searchParameters AND array_key_exists('date', $searchParameters))
        {
            $searchParameters['date'] = new \DateTime($searchParameters['date']);
        }
            
        return parent::readAll($searchParameters);
    }
}