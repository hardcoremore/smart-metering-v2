<?php

namespace HTEC\SmartMeteringModelBundle\Model;

use HTEC\BaseModelBundle\Model\BaseModel;

use HTEC\BaseModelBundle\Strategy\Search\BaseSearchStrategy;
use HTEC\BaseModelBundle\Entity\PageParameters;

use HTEC\BaseAPIBundle\Interfaces\Requester;

class OnlineMeterReadingModel
{
    protected $requester;
    protected $baseUrl;

    public function setRequester(Requester $requester)
    {
        $this->requester = $requester;
    }

    public function getRequester():Requester
    {
        return $this->requester;
    }

    public function setBaseUrl(string $url)
    {
        $this->baseUrl = $url;
    }

    public function getBaseUrl():string
    {
        return $this->baseUrl;
    }

    public function getPagedData(array $parameters)
    {
        return $this->getRequester()->get($this->getBaseUrl() . '/readings/online-meter-readings/list', $parameters);
    }

    public function getRecordAction($id)
    {
        return $this->getRequester()->get($this->getBaseUrl() . '/readings/online-meter-readings/list/' . $id);
    }

    public function quickSearch(array $parameters)
    {
        return $this->getRequester()->get($this->getBaseUrl() . '/readings/quick-search', $parameters);
    }
}