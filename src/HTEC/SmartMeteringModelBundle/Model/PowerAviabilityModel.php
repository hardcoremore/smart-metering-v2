<?php

namespace HTEC\SmartMeteringModelBundle\Model;

use HTEC\SmartMeteringModelBundle\Entity\PowerActivateDeactivate;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class PowerAviabilityModel extends PowerActivateDeactivateModel
{
    protected $feederModel;

    public function create(array $data, bool $checkPermissions = true)
    {
        throw new AccessDeniedException("Access denied", 403);
    }

    public function update($id, array $data, bool $checkPermissions = true)
    {
        throw new AccessDeniedException("Access denied", 403);   
    }

    public function patch($id, array $data, bool $checkPermissions = true)
    {
        throw new AccessDeniedException("Access denied", 403);
    }

    public function delete($id)
    {
        throw new AccessDeniedException("Access denied", 403);   
    }

    public function readAll(array $searchParameters = [], int $limit = -1)
    {
        if($searchParameters AND array_key_exists('date', $searchParameters))
        {
            $searchParameters['date'] = new \DateTime($searchParameters['date']);
        }
            
        if(array_key_exists('start', $searchParameters) && array_key_exists('end', $searchParameters))
        {
            $start = new \DateTime($searchParameters['start']);
            $end = new \DateTime($searchParameters['end']);

            unset($searchParameters['start']);
            unset($searchParameters['end']);

            $powerActivatesDeactivates = $this->entityManager->getRepository($this->entityName)
                                                  ->searchPowerActivateDeactivateForDateRange($start, $end, $searchParameters);

            return $powerActivatesDeactivates;
        }
        else
        {
            return parent::readAll($searchParameters);
        }
    }

    public function getDomain():string
    {
        return 'HTEC\SmartMeteringModelBundle\Entity\PowerAviability';
    }
}