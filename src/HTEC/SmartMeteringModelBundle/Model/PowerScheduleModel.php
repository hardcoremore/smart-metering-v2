<?php

namespace HTEC\SmartMeteringModelBundle\Model;

use HTEC\BaseModelBundle\Model\BaseModel;

use HTEC\SmartMeteringModelBundle\Entity\PowerSchedule;

class PowerScheduleModel extends BaseModel
{
    protected function updateEntityBeforeUpdate($entity, array $data)
    {        
        if(array_key_exists('status', $data) === false)
        {
            $entity->setStatus($entity::STATUS_WAITING_APPROVAL);
            $entity->setApprovedBy(null);
        }
        else
        {
            $token = $this->container->get('security.token_storage')->getToken();
            $entity->setApprovedBy($token->getUser());
        }

        parent::updateEntityBeforeUpdate($entity, $data);
    }

    protected function updateEntityBeforeCreate($entity, array $data)
    {
        $entity->setStatus($entity::STATUS_WAITING_APPROVAL);
    }

    public function readAll(array $searchParameters = [], int $limit = -1)
    {
        if($searchParameters AND array_key_exists('date', $searchParameters))
        {
            $searchParameters['date'] = new \DateTime($searchParameters['date']);
        }
            
        if(array_key_exists('start', $searchParameters) && array_key_exists('end', $searchParameters))
        {
            $start = new \DateTime($searchParameters['start']);
            $end = new \DateTime($searchParameters['end']);

            unset($searchParameters['start']);
            unset($searchParameters['end']);

            $powerSchedules = $this->entityManager->getRepository($this->entityName)
                                                  ->searchPowerSchedulesForDateRange($start, $end, $searchParameters);

            return $this->getRepeatedSchedulesForDateRange($powerSchedules, $start, $end);
        }
        else
        {
            return parent::readAll($searchParameters);
        }
    }

    public function getPowerSchedulesForEstimatedConsumption(\DateTime $start, \DateTime $end, array $searchParameters = [])
    {
        $powerSchedules = $this->entityManager->getRepository($this->entityName)
                                              ->searchPowerSchedulesForEstimatedConsumption($start, $end, $searchParameters);

            return $this->getRepeatedSchedulesForDateRange($powerSchedules, $start, $end);
    }

    public function getRepeatedSchedulesForDateRange(array $powerSchedules, \DateTime $start, \DateTime $end)
    {
        $ps = null;
        $repeatDate = null;
        $repeatedEvent = null;

        $allPowerSchedules = [];

        for($i = 0, $len = count($powerSchedules); $i < $len; $i++)
        {
            $ps = $powerSchedules[$i];

            if($ps->getRepeatType() AND $ps->getRepeatEnd() AND $ps->getRepeatEnd() instanceof \DateTime)
            {
                $repeatDate = clone $ps->getDate();

                while($this->isPowerScheduleRepeatDateInRange($ps, $repeatDate, $end))
                {
                    if($repeatDate->getTimestamp() < $start->getTimestamp())
                    {
                        $this->incrementRepeatDateForPowerSchedule($ps, $repeatDate);
                        continue;
                    }
                    else if($repeatDate->getTimestamp() === $start->getTimestamp() OR $repeatDate->getTimestamp() === $end->getTimestamp())
                    {
                        // if repeatDate was never incremented just return power schedule to array
                        if($repeatDate->getTimestamp() === $ps->getDate())
                        {
                            $allPowerSchedules[] = $ps;
                        }
                        else
                        {
                            // repeatDate is incremented and power schedule is repeated
                            $repeatedEvent = clone $ps;
                            $repeatedEvent->setRepeatDate(clone $repeatDate);

                            $allPowerSchedules[] = $repeatedEvent;
                        }
                    }
                    // start date is withing range, repeatDate is not incremented and schedule is not repeated
                    else if($repeatDate->getTimestamp() === $ps->getDate()->getTimestamp())
                    {
                        $allPowerSchedules[] = $ps;
                    }
                    else
                    {
                        // repeatDate is incremented and power schedule is repeated
                        $repeatedEvent = clone $ps;
                        $repeatedEvent->setRepeatDate(clone $repeatDate);

                        $allPowerSchedules[] = $repeatedEvent;
                    }

                    $this->incrementRepeatDateForPowerSchedule($ps, $repeatDate);
                }
            }
            else
            {
                $allPowerSchedules[] = $ps;
            }
        }

        return $allPowerSchedules;
    }

    protected function isPowerScheduleRepeatDateInRange(PowerSchedule $powerSchedule, \DateTime $repeatDate, \DateTime $end)
    {
        return $repeatDate->getTimestamp() <= $powerSchedule->getRepeatEnd()->getTimestamp() && $repeatDate->getTimestamp() <= $end->getTimestamp();
    }

    protected function incrementRepeatDateForPowerSchedule(PowerSchedule $powerSchedule, \DateTime $repeatDate)
    {
        switch($powerSchedule->getRepeatType())
        {
            case PowerSchedule::REPEAT_TYPE_DAILY:
                $repeatDate->modify('+1 day');
            break;

            case PowerSchedule::REPEAT_TYPE_WEEKLY:
                $repeatDate->modify('+1 week');
            break;

            case PowerSchedule::REPEAT_TYPE_MONTHLY:
                $repeatDate->modify('+1 month');
            break;
        }
    }
}