<?php

namespace HTEC\SmartMeteringModelBundle\Model;

use HTEC\BaseModelBundle\Model\BaseModel;

class PowerPurchaseModel extends BaseModel
{
    protected function updateEntityBeforeUpdate($entity, array $data)
    {
        if(array_key_exists('status', $data) === false)
        {
            $entity->setStatus($entity::STATUS_WAITING_APPROVAL);
            $entity->setApprovedBy(null);
        }
        else
        {
            $token = $this->container->get('security.token_storage')->getToken();
            $entity->setApprovedBy($token->getUser());
        }

        parent::updateEntityBeforeUpdate($entity, $data);
    }

    protected function updateEntityBeforeCreate($entity, array $data)
    {
        $entity->setStatus($entity::STATUS_WAITING_APPROVAL);
    }

    public function readAllForToday(array $parameters = [])
    {
        return $this->entityManager->getRepository($this->entityName)->searchByDate('createdDatetime', new \DateTime(), $parameters);
    }
}