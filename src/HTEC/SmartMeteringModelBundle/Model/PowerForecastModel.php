<?php

namespace HTEC\SmartMeteringModelBundle\Model;

use HTEC\BaseModelBundle\Model\BaseModel;

use HTEC\SmartMeteringModelBundle\Entity\PowerForecast;

class PowerForecastModel extends BaseModel
{

    private $tariffModel;
    private $businessDistrictModel;
    private $customerModel;

    private $powerForecastTariffDataModel;
    private $powerForecastBusinessDistrictTariffDataModel;
    private $powerForecastBusinessDistrictTotal;

    private $customerNetworkDataModel;

    public function setTariffModel(BaseModel $model)
    {
        $this->tariffModel = $model;
    }

    public function getTariffModel():BaseModel
    {
        return $this->tariffModel;
    }

    public function setBusinessDistrictModel(BaseModel $model)
    {
        $this->businessDistrictModel = $model;
    }

    public function getBusinessDistrictModel():BaseModel
    {
        return $this->businessDistrictModel;
    }

    public function setCustomerModel(BaseModel $model)
    {
        $this->customerModel = $model;
    }

    public function getCustomerModel():BaseModel
    {
        return $this->customerModel;
    }

    public function setCustomerNetworkDataModel(BaseModel $model)
    {
        $this->customerNetworkDataModel = $model;
    }

    public function getCustomerNetworkDataModel():BaseModel
    {
        return $this->customerNetworkDataModel;
    }




    public function setPowerForecastTariffDataModel(BaseModel $model)
    {
        $this->powerForecastTariffDataModel = $model;
    }

    public function getPowerForecastTariffDataModel():BaseModel
    {
        return $this->powerForecastTariffDataModel;
    }

    public function setPowerForecastBusinessDistrictTariffDataModel(BaseModel $model)
    {
        $this->powerForecastBusinessDistrictTariffDataModel = $model;
    }

    public function getPowerForecastBusinessDistrictTariffDataModel():BaseModel
    {
        return $this->powerForecastBusinessDistrictTariffDataModel;
    }

    public function setPowerForecastBusinessDistrictTotalModel(BaseModel $model)
    {
        $this->powerForecastBusinessDistrictTotal = $model;
    }

    public function getPowerForecastBusinessDistrictTotalModel():BaseModel
    {
        return $this->powerForecastBusinessDistrictTotal;
    }

    public function readAllForToday()
    {
        return $this->entityManager->getRepository($this->entityName)->searchByDate('createdDatetime', new \DateTime());
    }

    public function create(array $data, bool $checkPermissions = true)
    {
        $entity = parent::create($data, $checkPermissions);

        $amountToForecast = $entity->getAmountToForecast();

        $allTariffData = $this->calculatePowerForecastTariffData($amountToForecast);
        $allBusinessDistrictTariffData = $this->calculatePowerForecastBusinessDistrictTariffData($allTariffData);
        $allBusinessDistrictTotalData = $this->calculatePowerForecastBusinessDistrictTotal($allBusinessDistrictTariffData);


        $totalNumberOfCustomers = 0;
        $totalAllocationPerCustomer = 0;

        $highestAllocationTariff = null;
        $highestAllocationTariffValue = 0;

        $tariffData = null;
        $businessDistrictTariffData = null;
        $businessDistrictTotalData = null;

        for($i = 0, $len = count($allTariffData); $i < $len; $i++)
        {
            $tariffData = $allTariffData[$i];

            $totalNumberOfCustomers += $tariffData['numberOfCustomers'];
            $totalAllocationPerCustomer += $tariffData['shareOfPowerAllocationPerCustomer'];

            if($tariffData['shareOfPowerAllocationOnGrid'] > $highestAllocationTariffValue)
            {
                $highestAllocationTariff = $tariffData['tariff'];
                $highestAllocationTariffValue = $tariffData['shareOfPowerAllocationOnGrid'];
            }

            $tariffData['powerForecast'] = $entity->getId();
            $allTariffData[$i] = $tariffData;

            $this->getPowerForecastTariffDataModel()->create($tariffData, $checkPermissions = false);
        }

        for($i = 0, $len = count($allBusinessDistrictTariffData); $i < $len; $i++)
        {
            $businessDistrictTariffData = $allBusinessDistrictTariffData[$i];
            $businessDistrictTariffData['powerForecast'] = $entity->getId();
            $allBusinessDistrictTariffData[$i] = $businessDistrictTariffData;

            $this->getPowerForecastBusinessDistrictTariffDataModel()->create($businessDistrictTariffData, $checkPermissions = false);
        }

        $highestAllocationBusinessDistrict = null;
        $highestAllocationBusinessDistrictValue = 0;

        for($i = 0, $len = count($allBusinessDistrictTotalData); $i < $len; $i++)
        {
            $businessDistrictTotalData = $allBusinessDistrictTotalData[$i];

            if($businessDistrictTotalData['powerAllocationInKilowattHour'] > $highestAllocationBusinessDistrictValue)
            {
                $highestAllocationBusinessDistrict = $businessDistrictTotalData['businessDistrict'];
                $highestAllocationBusinessDistrictValue = $businessDistrictTotalData['powerAllocationInKilowattHour'];
            }

            $businessDistrictTotalData['powerForecast'] = $entity->getId();

            $allBusinessDistrictTotalData[$i] = $businessDistrictTotalData;

            $this->getPowerForecastBusinessDistrictTotalModel()->create($businessDistrictTotalData, $checkPermissions = false);
        }

        $totalTariffsAffected = count($allTariffData);

        $patchData = [
            'totalCustomersAffected' => $totalNumberOfCustomers,
            'totalBusinessDistrictsAffected' => count($allBusinessDistrictTotalData),
            'totalTariffsAffected' => $totalTariffsAffected,
            'averageAllocationPerCustomer' => $totalAllocationPerCustomer / $totalTariffsAffected,
            'highestAllocationBusinessDistrict' => $highestAllocationBusinessDistrict,
            'highestBusinessDistrictAllocationValue' => $highestAllocationBusinessDistrictValue,
            'highestAllocationTariff' => $highestAllocationTariff,
            'highestTariffAllocationValue' => $highestAllocationTariffValue
        ];

        // update totals and averages
        $this->patch($entity->getId(), $patchData, $checkPermissions = false);

        return $entity;
    }

    protected function updateEntityBeforeCreate($entity, array $data)
    {
        parent::updateEntityBeforeCreate($entity, $data);
        $entity->setPowerType($entity->getPowerPurchase()->getPowerType());
    }


    public function calculatePowerForecastBusinessDistrictTotal(array $businessDistrictTariffData):array
    {
        $businessDistrictTotalData = [];
        
        $numberOfCustomersTotal = 0;
        $powerAllocationInKilowattHourTotal = 0;
        $powerAllocationInKilowattHourPerDay = 0;
        $dailyChargeTotal = 0;
        $monthlyChargeTotal = 0;

        $currentBusinessDistrictId = null;

        $powerAllocationInKilowattHourTotal = 0;
        $powerAllocationInKilowattHourPerDayTotal = 0;

        foreach($businessDistrictTariffData AS $data)
        {
            if($currentBusinessDistrictId === null)
            {
                $currentBusinessDistrictId = $data['businessDistrict'];
            }

            if($currentBusinessDistrictId !== $data['businessDistrict'])
            {
                $businessDistrictTotalData[] = [
                    'businessDistrict' => $currentBusinessDistrictId,
                    'numberOfCustomers' => $numberOfCustomersTotal,
                    'powerAllocationInKilowattHour' => $powerAllocationInKilowattHourTotal,
                    'powerAllocationInKilowattHourPerDay' => $powerAllocationInKilowattHourPerDayTotal,
                    'dailyCharge' => $dailyChargeTotal,
                    'monthlyCharge' => $monthlyChargeTotal
                ];
            
                $currentBusinessDistrictId = $data['businessDistrict'];

                $numberOfCustomersTotal = $data['numberOfCustomers'];
                $powerAllocationInKilowattHourTotal = $data['powerAllocationInKilowattHour'];
                $powerAllocationInKilowattHourPerDayTotal = $data['powerAllocationInKilowattHourPerDay'];
                $dailyChargeTotal = $data['dailyCharge'];
                $monthlyChargeTotal = $data['monthlyCharge'];
            }
            else
            {
                $numberOfCustomersTotal += $data['numberOfCustomers'];
                $powerAllocationInKilowattHourTotal += $data['powerAllocationInKilowattHour'];
                $powerAllocationInKilowattHourPerDayTotal += $data['powerAllocationInKilowattHourPerDay'];
                $dailyChargeTotal += $data['dailyCharge'];
                $monthlyChargeTotal += $data['monthlyCharge'];    
            }
        }

        // insert last business district data
        $businessDistrictTotalData[] = [
            'businessDistrict' => $currentBusinessDistrictId,
            'numberOfCustomers' => $numberOfCustomersTotal,
            'powerAllocationInKilowattHour' => $powerAllocationInKilowattHourTotal,
            'powerAllocationInKilowattHourPerDay' => $powerAllocationInKilowattHourPerDayTotal,
            'dailyCharge' => $dailyChargeTotal,
            'monthlyCharge' => $monthlyChargeTotal
        ];

        return $businessDistrictTotalData;
    }

    public function calculatePowerForecastBusinessDistrictTariffData(array $powerForecastTariffData):array
    {
        $allBusinessDistricts = $this->getBusinessDistrictModel()->readAll();
        $businessDistrictTariffData = [];
        $data = null;

        foreach($allBusinessDistricts AS $businessDistrict)
        {
            foreach($powerForecastTariffData AS $tariffData)
            {
                $data = [];
                $data['numberOfCustomers'] = $this->getCustomerModel()->count([
                    'tariff' => $tariffData['tariff'],
                    'businessDistrict' => $businessDistrict->getId()
                ]);

                $data['powerAllocationInKilowattHour'] = $data['numberOfCustomers'] * $tariffData['shareOfPowerAllocationPerCustomer'];
                $data['powerAllocationInKilowattHourPerDay'] = $data['powerAllocationInKilowattHour'] * 24;
                $data['dailyCharge'] = $data['powerAllocationInKilowattHourPerDay'] * $tariffData['tariffsPrice'];
                $data['monthlyCharge'] = $data['dailyCharge'] * 30;

                $data['tariff'] = $tariffData['tariff'];
                $data['businessDistrict'] = $businessDistrict->getId();

                $businessDistrictTariffData[] = $data;
            }  
        }

        return $businessDistrictTariffData;
    }

    public function calculatePowerForecastTariffData(int $amountToForecast):array
    {
        $amountToForecastInKilowattHour = $amountToForecast * 1000;

        $allTariffs = $this->getTariffModel()->readAll();

        $totalTariffPrice = 0;
        $totalNumberOfTariffCustomers = 0;
        $totalForecastRate = 0;

        $powerForecastTariffData = [];
    
        $tariffData = null;

        foreach($allTariffs AS $tariff)
        {
            $tariffData = [];

            $tariffData['tariff'] = $tariff;
            $tariffData['numberOfCustomers'] = $this->getCustomerModel()->count(['tariff' => $tariff->getId()]);
            $tariffData['forecastRate'] = $tariff->getPrice() * $tariffData['numberOfCustomers'];

            $totalTariffPrice = $totalTariffPrice + $tariff->getPrice();
            $totalNumberOfTariffCustomers = $totalNumberOfTariffCustomers + $tariffData['numberOfCustomers'];
            $totalForecastRate = $totalForecastRate + $tariffData['forecastRate'];

            $powerForecastTariffData[] = $tariffData;
        }

        for($i = 0, $len = count($powerForecastTariffData); $i < $len; $i++)
        {
            $tariffData = $powerForecastTariffData[$i];

            $tariffData['shareOfPowerAllocationOnGrid'] = ($tariffData['forecastRate'] / $totalForecastRate) * $amountToForecastInKilowattHour;
            $tariffData['shareOfPowerAllocationPerCustomer'] = $tariffData['shareOfPowerAllocationOnGrid'] / $tariffData['numberOfCustomers'];

            $tariffData['tariffsCode'] = $tariffData['tariff']->getCode();
            $tariffData['tariffsPrice'] = $tariffData['tariff']->getPrice();

            // we don't need tariff object anymore
            $tariffData['tariff'] = $tariffData['tariff']->getId();

            $powerForecastTariffData[$i] = $tariffData;
        }

        return $powerForecastTariffData;
    }
}
