<?php

namespace HTEC\SmartMeteringModelBundle\Model;

use HTEC\BaseModelBundle\Model\BaseModel;

class ApplianceTypeModel extends BaseModel
{
    private $customerCategoryModel;

    public function setCustomerCategoryModel(BaseModel $customerCategoryModel)
    {
        $this->customerCategoryModel = $customerCategoryModel;
    }

    public function getCustomerCategoryModel()
    {
        return $this->customerCategoryModel;
    }

    public function readForSelect(array $queryParameters = null)
    {
        if($queryParameters !== null AND array_key_exists('customerCategory', $queryParameters))
        {
            $customerCategory = $this->getCustomerCategoryModel()->get($queryParameters['customerCategory']);

            if($customerCategory)
            {
                return $customerCategory->getApplianceTypes();
            }
            else
            {
                return [];
            }
        }
        else
        {
            return parent::readForSelect($queryParameters);
        }
    }
}