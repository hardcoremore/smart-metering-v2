<?php

namespace HTEC\SmartMeteringModelBundle\Model;

use Symfony\Component\Security\Acl\Domain\RoleSecurityIdentity;
use Symfony\Component\Security\Acl\Model\AclProviderInterface;
use Symfony\Component\Security\Acl\Domain\PermissionGrantingStrategy;
use Symfony\Component\Security\Acl\Permission\MaskBuilder;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;

use HTEC\BaseModelBundle\Model\BaseModel;

use HTEC\SmartMeteringModelBundle\Entity\Role;
use HTEC\SmartMeteringModelBundle\Entity\AdminPanelUser;

class PermissionModel extends BaseModel
{
    private $aclProvider;
    private $maskBuilder;

    public function setAclProvider(AclProviderInterface $aclProvider)
    {
        $this->aclProvider = $aclProvider;
    }

    public function getAclProvider()
    {
        return $this->aclProvider;
    }

    public function setMaskBuilder(MaskBuilder $maskBuilder)
    {
        $this->maskBuilder = $maskBuilder;
    }

    public function getMaskBuilder()
    {
        return $this->maskBuilder;
    }

    // everyone have access to this entity
    protected function checkViewPermission(string $domain):bool
    {
        return true;
    }

    public function getUserPermissions(AdminPanelUser $userEntity)
    {   
        $allPermissions = $this->readAll();
        $userRoles = $userEntity->getRoles();

        $rolePermissions = [];

        foreach($userRoles AS $role)
        {
            $rolePermissions[] = $this->getPermissionsGroupedByIdentifier($this->getRolePermissions($role, $allPermissions));
        }

        $userPermissions = [];

        // $value is integer index of each role
        foreach($rolePermissions AS $value)
        {
            // $key is permission identifier index of each permission in a role
            foreach($value AS $key => $permission)
            {
                if(array_key_exists($key, $userPermissions))
                {
                    $userPermissions[$key]['actions'] = array_unique(
                        array_merge($userPermissions[$key]['actions'], $permission['actions'])
                    );
                }
                else
                {
                    $userPermissions[$key] = $permission;
                }
            }
        }

        return $userPermissions;
    }

    public function getRolePermissions(Role $role, array $allPermissions)
    {
        $securityIdentity = new RoleSecurityIdentity($role->getRole());
        $classIdentity = null;
        $acl = null;

        $rolePermissions = [];

        foreach($allPermissions AS $permission)
        {
            $classIdentity = new ObjectIdentity('class', $permission->getDomain());

            try
            {
                $acl = $this->getAclProvider()->findAcl($classIdentity);
            }
            catch(\Exception $e)
            {
                continue;
            }

            $permissionsGrantingStrategy = new PermissionGrantingStrategy();

            foreach($acl->getClassAces() AS $ace)
            {
                if($ace->getSecurityIdentity()->equals($securityIdentity))
                {
                    foreach ($permission->getActions() AS $key => $value)
                    {
                        $this->getMaskBuilder()->reset();
                        $this->getMaskBuilder()->add($value);

                        try
                        {
                            if($permissionsGrantingStrategy->isGranted(
                                    $acl,
                                    array($this->getMaskBuilder()->get()),
                                    array($ace->getSecurityIdentity())
                                )
                            )
                            {
                                $rolePermissions[] = [
                                    "id"=> $permission->getIdentifier(),
                                    "name"=> $permission->getName(),
                                    "domain"=> $permission->getDomain(),
                                    "action" => $value
                                ];
                            }
                        }
                        catch(\Exception $e)
                        {
                            continue;
                        }
                    }
                }
            }
        }

        return $rolePermissions;    
    }

    public function getPermissionsGroupedByIdentifier($permissions)
    {
        $groupedPermissions = [];
        $groupedItem;

        foreach($permissions AS $value)
        {               
            if(array_key_exists($value['id'], $groupedPermissions))
            {
                $groupedPermissions[$value['id']]['actions'][] = $value['action'];
            }
            else
            {
                $groupedPermissions[$value['id']] = [
                    'identifier' => $value['id'],
                    'domain' => $value['domain'],
                    'name' => $value['name'],
                    'actions' => [$value['action']]
                ];
            }
        }

        return $groupedPermissions;
    }
}