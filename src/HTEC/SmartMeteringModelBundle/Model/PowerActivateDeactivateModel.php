<?php

namespace HTEC\SmartMeteringModelBundle\Model;

use HTEC\BaseModelBundle\Model\BaseModel;

use HTEC\SmartMeteringModelBundle\Entity\PowerActivateDeactivate;
use HTEC\SmartMeteringModelBundle\Entity\Feeder;
use HTEC\BaseModelBundle\Exception\Form\InvalidFormDataException;

class PowerActivateDeactivateModel extends BaseModel
{
    protected $feederModel;

    public function setFeederModel(BaseModel $model)
    {
        $this->feederModel = $model;
    }

    public function getFeederModel()
    {
        return $this->feederModel;
    }

    protected function updateEntityBeforeCreate($entity, array $data)
    {
        $isDateSent = array_key_exists('date', $data);

        if($isDateSent === false || ($isDateSent === true && strlen($data['date']) < 1))
        {
            $entity->setDate(new \DateTime());
        }

        $isTimeSent = array_key_exists('time', $data);

        if($isTimeSent === false || ($isTimeSent === true && strlen($data['time']) < 1))
        {
            $entity->setTime(new \DateTime());
        }

        if(PowerActivateDeactivate::TYPE_POWER_ACTIVATE === $data['type'])
        {
            $this->activatePowerTransformerFeeder($entity->getPowerTransformerFeeder());
        }
        else
        {
            $this->deactivatePowerTransformerFeeder($entity->getPowerTransformerFeeder());
        }

        parent::updateEntityBeforeCreate($entity, $data);
    }

    protected function updateEntityBeforeUpdate($entity, array $data)
    {
        if(isset($data['type']))
        {
            if(PowerActivateDeactivate::TYPE_POWER_ACTIVATE === $data['type'])
            {
                $this->activatePowerTransformerFeeder($entity->getPowerTransformerFeeder());
            }
            else
            {
                $this->deactivatePowerTransformerFeeder($entity->getPowerTransformerFeeder());
            }
        }

        parent::updateEntityBeforeUpdate($entity, $data);
    }

    protected function activatePowerTransformerFeeder(Feeder $feeder)
    {
        $feederId = $feeder->getId();

        if($feeder->getStatus() === Feeder::STATUS_OFFLINE)
        {
            $this->getFeederModel()->patch($feederId, ['status' => Feeder::STATUS_LIVE]);    
        }
        else
        {
             $exception = new InvalidFormDataException("Form data is invalid", 400);
             $exception->setFormErrorMessage('Power Transformer Feeder is already live and can not be activated.');

             throw $exception;
        }
    }

    protected function deactivatePowerTransformerFeeder(Feeder $feeder)
    {
        $feederId = $feeder->getId();

        if($feeder->getStatus() === Feeder::STATUS_LIVE)
        {
            $this->getFeederModel()->patch($feederId, ['status' => Feeder::STATUS_OFFLINE]);
        }
        else
        {
            $exception = new InvalidFormDataException("Form data is invalid", 400);
            $exception->setFormErrorMessage('Power Transformer Feeder is already offline and can not be deactivated.');

            throw $exception;
        }
    }

    public function readAll(array $searchParameters = [], int $limit = -1)
    {
        if(array_key_exists('start', $searchParameters) && array_key_exists('end', $searchParameters))
        {
            $start = new \DateTime($searchParameters['start']);
            $end = new \DateTime($searchParameters['end']);

            unset($searchParameters['start']);
            unset($searchParameters['end']);

            return $this->entityManager->getRepository($this->entityName)
                                       ->searchPowerActivateDeactivateForDateRange($start, $end, $searchParameters);
        }
        else
        {
            return parent::readAll($searchParameters);
        }
    }

    public function getPowerActivateDeactivateForEstimatedConsumption(\DateTime $start, \DateTime $end, array $searchParameters = [])
    {
        $powerActivatesAndDeactivates = $this->entityManager->getRepository($this->entityName)
                                              ->searchPowerActivateDeactivateForEstimatedConsumption($start, $end, $searchParameters);

        return $powerActivatesAndDeactivates;
    }
}