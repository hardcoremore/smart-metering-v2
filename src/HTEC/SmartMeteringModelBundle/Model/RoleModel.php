<?php

namespace HTEC\SmartMeteringModelBundle\Model;

use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Symfony\Component\Security\Acl\Domain\RoleSecurityIdentity;
use Symfony\Component\Security\Acl\Exception\AclNotFoundException;
use Symfony\Component\Security\Acl\Permission\MaskBuilder;

use HTEC\BaseModelBundle\Model\BaseModel;
use HTEC\BaseModelBundle\Strategy\Search\BaseSearchStrategy;
use HTEC\BaseModelBundle\Entity\PageParameters;

use HTEC\SmartMeteringModelBundle\Entity\Role;

class RoleModel extends BaseModel
{
    private $permissionModel;
    private $maskBuilder;

    private static $domainIdentities = [];
    private static $roleSecurityIdentities = [];

    public function setPermissionModel(PermissionModel $permissionModel)
    {
        $this->permissionModel = $permissionModel;
    }

    public function getPermissionModel()
    {
        return $this->permissionModel;
    }

    public function setMaskBuilder(MaskBuilder $maskBuilder)
    {
        $this->maskBuilder = $maskBuilder;
    }

    public function getMaskBuilder()
    {
        return $this->maskBuilder;
    }

    public function getFormattedRole(string $roleName)
    {
        return 'ROLE_' . strtoupper($roleName);
    }

    public function create(array $data, bool $checkPermissions = true)
    {
        $role = $this->getFormattedRole($data['name']);

        $data['name'] = $role;
        $data['role'] = $role;

        return parent::create($data, $checkPermissions);
    }

    public function update($id, array $data, bool $checkPermissions = true)
    {
        $role = $this->getFormattedRole($data['name']);

        $data['name'] = $role;
        $data['role'] = $role;

        return parent::update($id, $data, $checkPermissions);
    }

    public function get($id)
    {
        $role = parent::get($id);

        $allPermissions = $this->getPermissionModel()->readAll();
        $role->setPermissions($this->getPermissionModel()->getRolePermissions($role, $allPermissions));

        return $role;
    }

    public function getPagedData(PageParameters $pageData, BaseSearchStrategy $searchStrategy = NULL)
    {
        $pagedData = parent::getPagedData($pageData, $searchStrategy);
        $allPermissions = $this->getPermissionModel()->readAll();

        $rolePermissions;

        foreach($pagedData->listData AS $role)
        {
            $rolePermissions = $this->getPermissionModel()->getRolePermissions($role, $allPermissions);
            $role->setPermissions($this->getPermissionModel()->getPermissionsGroupedByIdentifier($rolePermissions));
        }

        return $pagedData;
    }

    protected function updateEntityBeforeCreate($entity, array $data)
    {
        parent::updateEntityBeforeCreate($entity, $data);

        $token = $this->container->get('security.token_storage')->getToken();
        $entity->setOwner($token->getUser());

        $this->saveRolePermissions($entity, $entity->getPermissions());
    }

    protected function updateEntityBeforeUpdate($entity, array $data)
    {
        parent::updateEntityBeforeUpdate($entity, $data);

        $this->saveRolePermissions($entity, $entity->getPermissions());
    }

    protected function saveRolePermissions(Role $entity, array $rolePermissionData)
    {
        $rolePermission;

        $aclProvider = $this->container->get('security.acl.provider');

        $allPermissions = $this->getPermissionModel()->readAll();
        
        foreach($allPermissions AS $value)
        {
            $rolePermission = [
                'id' => $value->getIdentifier(),
                'class' => $value->getDomain(),
            ];

            if(array_key_exists($value->getDomain(), $rolePermissionData))
            {
                $rolePermission['actions'] = $rolePermissionData[$value->getDomain()];
                $this->updateRolePermission($rolePermission, $entity, $aclProvider);
            }
            else
            {
                $this->removeRolePermission($rolePermission, $entity, $aclProvider);        
            }
        }

        $rolePermissions = $this->getPermissionModel()->getRolePermissions($entity, $allPermissions);
        $entity->setPermissions($this->getPermissionModel()->getPermissionsGroupedByIdentifier($rolePermissions));
    }

    /**
     *   Structure of $permission array is as follows:
     *    
     *   array(
     *       'id' => 'identifier',
     *       'class' => 'fully/qualified/classname',
     *       'actions' => array('view', 'create', 'update', 'delete')
     *   )
     *
     * @param array $permission Permission to update role with
     * @param Role $role 
     * @param $aclProvider
     */
    public function updateRolePermission(array $permission, Role $role, $aclProvider)
    {
        $roleIdentity = $this->getRoleSecurityIdentity($role);
        $domainIdentity = $this->getDomainIdentity($permission['class']);

        $acl = null;

        //finding the ACL FOR THE DOMAIN (class name)
        try
        {
            $acl = $aclProvider->findAcl($domainIdentity, array($roleIdentity));
        }
        catch(AclNotFoundException $e)
        {
            // acl is not found so create new acl for domain
            $acl = $aclProvider->createAcl($domainIdentity);
        }

        // reset mask builder
        $this->getMaskBuilder()->reset();

        // combine all actions in mask
        foreach($permission['actions'] AS $action)
        {
            $this->getMaskBuilder()->add($action);
        }

        $mask = $this->getMaskBuilder()->get();


        // find if ace already exists
        $aceExists = false;
        
        foreach($acl->getClassAces() AS $index => $ace)
        {
            if($ace->getSecurityIdentity()->equals($roleIdentity))
            {
                $aceExists = true;
                $acl->updateClassAce($index, $mask);
            }
        }

        // ace already exists so we do not create new
        if($aceExists === false) {
            $acl->insertClassAce($roleIdentity, $mask);
        }
        
        $aclProvider->updateAcl($acl);
    }

    public function removeRolePermission(array $permission, Role $role, $aclProvider)
    {
        $roleIdentity = $this->getRoleSecurityIdentity($role);
        $domainIdentity = $this->getDomainIdentity($permission['class']);

        //creating the ACL FOR THE DOMAIN (class name)
        try
        {
            $acl = $aclProvider->findAcl($domainIdentity, array($roleIdentity));

            //REMOVE ACL
            foreach($acl->getClassAces() AS $index => $ace)
            {
                if($ace->getSecurityIdentity()->equals($roleIdentity))
                {
                    $acl->deleteClassAce($index);
                }
            }

            $aclProvider->updateAcl($acl);
        }
        catch(AclNotFoundException $e)
        {
            // acl not found for the domain nothing to delete here
        }
    }

    protected function getRoleSecurityIdentity(Role $role)
    {
        if(array_key_exists($role->getRole(), self::$roleSecurityIdentities))
        {
            return self::$roleSecurityIdentities[$role->getRole()];
        }
        else
        {
            self::$roleSecurityIdentities[$role->getRole()] = new RoleSecurityIdentity($role->getRole());
        }

        return self::$roleSecurityIdentities[$role->getRole()];
    }

    protected function getDomainIdentity(string $domain)
    {
        if(array_key_exists($domain, self::$domainIdentities))
        {
            return self::$domainIdentities[$domain];
        }
        else
        {
            self::$domainIdentities[$domain] = new ObjectIdentity('class', $domain);
        }

        return self::$domainIdentities[$domain];
    }
}
