<?php

namespace HTEC\SmartMeteringModelBundle\Model;

use HTEC\BaseModelBundle\Model\BaseModel;
use HTEC\SmartMeteringModelBundle\Entity\Customer;
use HTEC\SmartMeteringModelBundle\Entity\EstimatedLoad;
use HTEC\SmartMeteringModelBundle\Entity\CustomerNetworkData;
use HTEC\SmartMeteringModelBundle\Entity\EstimatedConsumption;

class EstimatedConsumptionModel extends BaseModel
{
    private $customerModel;
    private $estimatedLoadModel;
    private $powerActivateDeactivateModel;
    private $powerScheduleModel;
    private $customerNetworkDataModel;

    public function setCustomerModel(BaseModel $customerModel)
    {
        $this->customerModel = $customerModel;
    }

    public function getCustomerModel()
    {
        return $this->customerModel;
    }

    public function setEstimatedLoadModel(BaseModel $estimatedLoadModel)
    {
        $this->estimatedLoadModel = $estimatedLoadModel;
    }

    public function getEstimatedLoadModel()
    {
        return $this->estimatedLoadModel;
    }

    public function setPowerActivateDeactivateModel(BaseModel $powerActivateDeactivateModel)
    {
        $this->powerActivateDeactivateModel = $powerActivateDeactivateModel;
    }

    public function getPowerActivateDeactivateModel()
    {
        return $this->powerActivateDeactivateModel;
    }

    public function setPowerScheduleModel(BaseModel $powerScheduleModel)
    {
        $this->powerScheduleModel = $powerScheduleModel;
    }

    public function getPowerScheduleModel()
    {
        return $this->powerScheduleModel;
    }

    public function setCustomerNetworkDataModel(BaseModel $customerNetworkDataModel)
    {
        $this->customerNetworkDataModel = $customerNetworkDataModel;
    }

    public function getCustomerNetworkDataModel()
    {
        return $this->customerNetworkDataModel;
    }

    protected function updateEntityBeforeCreate($entity, array $data)
    {
        parent::updateEntityBeforeCreate($entity, $data);
        $this->updateEstimatedConsumption($entity, $data);
    }

    protected function updateEntityBeforeUpdate($entity, array $data)
    {
        parent::updateEntityBeforeUpdate($entity, $data);
        $this->updateEstimatedConsumption($entity, $data);
    }

    protected function updateEstimatedConsumption($entity, array $data)
    {
        $start = new \DateTime($data['start']);
        $end = new \DateTime($data['end']);

        $actualHoursScheduled = $this->getActualHoursScheduled($entity->getCustomer(), $start, $end);

        $entity->setActualHoursScheduled($actualHoursScheduled);

        $estimatedLoad = $this->getEstimatedLoadModel()->readAll(['customer' => $entity->getCustomer()->getId()], 1);

        $totalLoad = $this->getCustomerTotalLoad($entity->getCustomer(), $estimatedLoad);

        $entity->setCalculatedLoad($totalLoad);

        $diversityFactor = 1;

        if($estimatedLoad)
        {
            $diversityFactor = $estimatedLoad->getCustomerCategory()->getDiversityFactor();
            $entity->setDiversityFactor($diversityFactor);
            $entity->setType(EstimatedConsumption::TYPE_ESTIMATED);
        }
        else
        {
            $entity->setType(EstimatedConsumption::TYPE_TARIFF_AVERAGE);   
        }

        $estimatedConsumption = $this->calculateEstimatedConsumption($actualHoursScheduled, $totalLoad, $diversityFactor);

        $entity->setEstimatedConsumption($estimatedConsumption);
    }

    public function getCustomersEstimatedConsumption(Customer $customer, \DateTime $start, \DateTime $end):float
    {
        $actualHoursScheduled = $this->getActualHoursScheduled($customer, $start, $end);
        $estimatedLoad = $this->getEstimatedLoadModel()->readAll(['customer' => $customer->getId()], 1);
        $totalLoad = $this->getCustomerTotalLoad($customer, $estimatedLoad);

        $diversityFactor = 1;

        if($estimatedLoad)
        {
            $diversityFactor = $estimatedLoad->getCustomerCategory()->getDiversityFactor();
        }

        $estimatedConsumption = $this->calculateEstimatedConsumption($actualHoursScheduled, $totalLoad, $diversityFactor);

        return $estimatedConsumption;
    }

    public function calculateEstimatedConsumption(float $actualHoursScheduled, float $totalLoad, float $diversityFactor):float
    {
        return $actualHoursScheduled * $totalLoad * $diversityFactor;
    }

    public function getCustomerTotalLoad(Customer $customer, EstimatedLoad $estimatedLoad = null):float
    {
        $totalLoadInKwH = 0;

        if($estimatedLoad)
        {
            // total load is in watts
            $totalLoadInKwH = $estimatedLoad->getTotalLoad() / 1000;
        }
        else
        {
            // average consumption per day is in kw/h
            $totalLoadInKwH = $customer->getTariff()->getAverageConsumptionPerDay() / 24;
        }

        return $totalLoadInKwH;
    }

    public function getActualHoursScheduled(Customer $customer, \DateTime $start, \DateTime $end):float
    {
        $customerNetworkData = $this->getCustomerNetworkDataModel()->readAll(['customer' => $customer->getId()], 1);

        $filters = [
            'businessDistrict' => $customer->getBusinessDistrict()->getId(),
            'status' => 'approved'
        ];

        $this->addNetworkDataToFilters($filters, $customerNetworkData);

        $powerSchedules = $this->getPowerScheduleModel()->getPowerSchedulesForEstimatedConsumption($start, $end, $filters);

        // if there are no power schedules for customer
        // calculate actual scheduled hours as if customer 
        // had electricity every day 24 hours
        if(count($powerSchedules) === 0)
        {
            // $numberOfDays = $start->diff($end)->days + 1; // 2 is to include start date as well

            return 0; // no power schedules found
        }

        unset($filters['status']);

        $powerActivatesAndDeactivates = $this->getPowerActivateDeactivateModel()
                                             ->getPowerActivateDeactivateForEstimatedConsumption($start, $end, $filters);

        $powerDeactivates = [];
        $powerActivates = [];

        if($powerActivatesAndDeactivates)
        {
            $ps = null;

            for($i = 0, $len = count($powerActivatesAndDeactivates); $i < $len; $i++)
            {
                $pad = $powerActivatesAndDeactivates[$i];

                if($pad->getType() === 'power-activate')
                {
                    $powerActivates[] = $pad;
                }
                else
                {
                    $powerDeactivates[] = $pad;   
                }
            }
        }

        $powerScheduleDate = null;
        $totalHoursScheduled = 0;               
        $interval = null;

        $activateMinutes = 0;
        $deactivateMinutes = 0;
        $powerScheduleEndMinutes = 0;

        for($i = 0, $len = count($powerSchedules); $i < $len; $i++)
        {
            $ps = $powerSchedules[$i];

            if($ps->getRepeatDate())
            {
                $powerScheduleDate = $ps->getRepeatDate();
            }
            else
            {
                $powerScheduleDate = $ps->getDate();
            }

            $powerScheduleDateFormatted = $powerScheduleDate->format('Y-m-d');

            $interval = $ps->getStartTime()->diff($ps->getEndTime(), true);

            $totalHoursScheduled += (($interval->format('%h') * 60) + $interval->format('%i')) / 60;

            foreach ($powerDeactivates AS $deactivatedPower)
            {
                if($powerScheduleDateFormatted === $deactivatedPower->getDate()->format('Y-m-d'))
                {
                    foreach ($powerActivates AS $activatedPower)
                    {
                        if($powerScheduleDateFormatted === $activatedPower->getDate()->format('Y-m-d'))
                        {
                            $powerScheduleEndMinutes = ($ps->getEndTime()->format('H') * 60) + $ps->getEndTime()->format('i');

                            $deactivateMinutes = ($deactivatedPower->getTime()->format('H') * 60) + $deactivatedPower->getTime()->format('i');
                            $activateMinutes = ($activatedPower->getTime()->format('H') * 60) + $activatedPower->getTime()->format('i');

                            // check to see if power is activated before the schedule is finished
                            // if power is activated after the schedule is finished than reduce to total hours scheduled
                            // only until the schedule lastet
                            if($activateMinutes < $powerScheduleEndMinutes)
                            {
                                $totalHoursScheduled -= ($activateMinutes - $deactivateMinutes) / 60;
                            }
                            else
                            {
                                $totalHoursScheduled -= ($activateMinutes - $powerScheduleEndMinutes) / 60;
                            }
                        }
                    }
                }
            }
        }        

        return $totalHoursScheduled;
    }

    protected function addNetworkDataToFilters(array &$filters, CustomerNetworkData $customerNetworkData = null):array
    {
        if($customerNetworkData)
        {
            if($customerNetworkData->getInjectionSubstation())
            {
                $filters['injectionSubstation'] = $customerNetworkData->getInjectionSubstation()->getId();
            }

            if($customerNetworkData->getPowerTransformer())
            {
                $filters['powerTransformer'] = $customerNetworkData->getPowerTransformer()->getId();
            }

            if($customerNetworkData->getPowerTransformerFeeder())
            {
                $filters['powerTransformerFeeder'] = $customerNetworkData->getPowerTransformerFeeder()->getId();
            }

            if($customerNetworkData->getDistributionTransformer())
            {
                $filters['distributionTransformer'] = $customerNetworkData->getDistributionTransformer()->getId();
            }

            if($customerNetworkData->getDistributionTransformerFeeder())
            {
                $filters['distributionTransformerFeeder'] = $customerNetworkData->getDistributionTransformerFeeder()->getId();
            }
        }

        return $filters;
    }
}