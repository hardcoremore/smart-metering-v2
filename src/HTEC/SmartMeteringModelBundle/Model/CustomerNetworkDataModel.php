<?php

namespace HTEC\SmartMeteringModelBundle\Model;

use HTEC\BaseModelBundle\Model\BaseModel;

use HTEC\BaseModelBundle\Strategy\Search\BaseSearchStrategy;
use HTEC\BaseModelBundle\Entity\PageParameters;

class CustomerNetworkDataModel extends BaseModel
{
    public function getPagedData(PageParameters $pageParameters, BaseSearchStrategy $searchStrategy = NULL)
    {
        return $this->getPageGenerator()->getPagedData(
            $this->entityManager->getRepository($this->entityName),
            $pageParameters,
            [
                'customer', 'injectionSubstation', 'powerTransformer', 'powerTransformerFeeder',
                'highTensionPole', 'distributionTransformer', 'lowTensionPole', 'distributionTransformerFeeder'
            ],
            $searchStrategy
        );
    }
}