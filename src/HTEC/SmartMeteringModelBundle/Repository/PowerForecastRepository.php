<?php

namespace HTEC\SmartMeteringModelBundle\Repository;

use HTEC\BaseModelBundle\Repository\BaseDoctrineRepository;

/**
 * PowerForecastRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class PowerForecastRepository extends BaseDoctrineRepository
{
}
