<?php

namespace HTEC\SmartMeteringModelBundle\Repository;

use HTEC\BaseModelBundle\Repository\BaseDoctrineRepository;

use Doctrine\ORM\QueryBuilder;

/**
 * CustomerRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class CustomerRepository extends BaseDoctrineRepository
{
    public function searchForAutoComplete(string $queryString, array $searchInColumns, array $filterParameters = null, $maxResults = 12)
    {
        return parent::searchForAutoComplete($queryString, ['name', 'accountNumber', 'address', 'email'], $filterParameters, $maxResults);
    }
}
