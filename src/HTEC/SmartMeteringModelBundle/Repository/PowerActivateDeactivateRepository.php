<?php

namespace HTEC\SmartMeteringModelBundle\Repository;

use Doctrine\ORM\QueryBuilder;

use HTEC\BaseModelBundle\Repository\BaseDoctrineRepository;

class PowerActivateDeactivateRepository extends BaseDoctrineRepository
{
    public function searchPowerActivateDeactivateForDateRange(\DateTime $start, \DateTime $end, array $searchParameters = [])
    {
        $qb = $this->createQueryBuilder('pr');

        $this->joinRelationsForSearch($qb);

        $this->addDateRangeFilters($qb, $start, $end);

        foreach($searchParameters AS $key => $val)
        {
            $qb->andWhere('pr.' . $key . ' = :' . $key)->setParameter($key, $val);
        }

        $qb->addOrderBy('pr.date', 'ASC');
        $qb->addOrderBy('pr.businessDistrict', 'ASC');
        $qb->addOrderBy('pr.injectionSubstation', 'ASC');
        $qb->addOrderBy('pr.powerTransformer', 'ASC');
        $qb->addOrderBy('pr.powerTransformerFeeder', 'ASC');

        return $qb->getQuery()->getResult();
    }

    public function searchPowerActivateDeactivateForEstimatedConsumption(\DateTime $start, \DateTime $end, array $searchParameters = [])
    {
        $qb = $this->createQueryBuilder('pr');

        $this->joinRelationsForSearch($qb);

        $this->addDateRangeFilters($qb, $start, $end);

        foreach($searchParameters AS $key => $val)
        {
            // business district is required 
            if($key === 'businessDistrict')
            {
                $qb->andWhere('pr.businessDistrict = :businessDistrict');
            }
            else
            {
                $qb->andWhere($qb->expr()->orx(
                    $qb->expr()->eq('pr.' . $key, ':' . $key),
                    $qb->expr()->isNull('pr.' . $key)
                ));
            }

            $qb->setParameter($key, $val);
        }

        $qb->orderBy('pr.date', 'ASC');

        return $qb->getQuery()->getResult();
    }

    protected function joinRelationsForSearch(QueryBuilder $queryBuilder)
    {
        // addSelect('ps') will select all columns from power soruces table
        // so that doctrine does not call extra queries for relation fields
        // http://www.doctrine-project.org/api/dbal/2.3/class-Doctrine.DBAL.Query.Expression.ExpressionBuilder.html

        $queryBuilder->leftJoin('pr.businessDistrict', 'bd')->addSelect('bd')
                     ->leftJoin('pr.injectionSubstation', 'injStat')->addSelect('injStat')
                     ->leftJoin('pr.powerTransformer', 'pt')->addSelect('pt')
                     ->leftJoin('pr.powerTransformerFeeder', 'ptf')->addSelect('ptf')
                     ->leftJoin('pr.distributionTransformer', 'dt')->addSelect('dt')
                     ->leftJoin('pr.distributionTransformerFeeder', 'dtf')->addSelect('dtf');
    }

    protected function addDateRangeFilters(QueryBuilder $queryBuilder, \DateTime $start, \DateTime $end)
    {
        $queryBuilder->andWhere($queryBuilder->expr()->gte('pr.date', ':start'))
                     ->andWhere($queryBuilder->expr()->lte('pr.date', ':end'))
                     ->setParameter('start', $start->format('Y-m-d'))
                     ->setParameter('end', $end->format('Y-m-d'));
    }
}
