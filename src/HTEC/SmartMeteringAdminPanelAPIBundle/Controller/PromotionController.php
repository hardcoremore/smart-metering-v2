<?php

namespace HTEC\SmartMeteringAdminPanelAPIBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use HTEC\BaseModelBundle\Entity\PageParameters;
use HTEC\SmartMeteringModelBundle\Model\PromotionModel;

class PromotionController extends Controller
{
    protected $promotionModel;

    public function setPromotionModel(PromotionModel $model)
    {
        $this->promotionModel = $model;
    }

    public function getPromotionModel()
    {
        return $this->promotionModel;
    }

    public function getRecordAction($id)
    {
        $pagedData = $this->promotionModel->getRecordAction($id);

        $response = new Response();

        $response->setContent($pagedData['body']);
        $response->setStatusCode(Response::HTTP_OK);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    public function listRecordsAction(Request $request)
    {
        $pagedData = $this->promotionModel->getPagedData($request->query->all());

        $response = new Response();

        $response->setContent($pagedData['body']);
        $response->setStatusCode(Response::HTTP_OK);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    public function quickSearchAction(Request $request)
    {
        $pagedData = $this->promotionModel->quickSearch($request->query->all());

        $response = new Response();

        $response->setContent($pagedData['body']);
        $response->setStatusCode(Response::HTTP_OK);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }
}