<?php

namespace HTEC\SmartMeteringAdminPanelAPIBundle\Controller;

use HTEC\BaseAPIBundle\Controller\BaseRestController;

use Symfony\Component\HttpFoundation\Request;

class PowerForecastController extends BaseRestController
{
    private $fullDetailsSerializationGroups = [
        'power_forecast', 'power_forecast_full_details', 'power_forecast_business_district_total',
        'power_forecast_tariff_data', 'power_forecast_business_district_tariff_data',
        'business_district_select', 'tariff_select', 'power_purchase_select'
    ];

    public function getFullDetailsRecordAction($id)
    {
        $record = $this->getModel()->get($id);

        if($record)
        {
            $serializationContext = $this->getSerializationContext();
            $serializationContext->setGroups($this->fullDetailsSerializationGroups);

            $view = $this->view($record, 200)
                         ->setContext($serializationContext);

            return $this->handleView($view);
        }
        else
        {
            return $this->handleView($this->view(null, 404));
        }
    }

    public function readAllForTodayWithDetailsAction(Request $request)
    {
        $allData = $this->getModel()->readAllForToday();

        $serializationContext = $this->getSerializationContext();
        $serializationContext->setGroups($this->fullDetailsSerializationGroups);

        $view = $this->view($allData, 200)
                     ->setContext($serializationContext);

        return $this->handleView($view);
    }
}