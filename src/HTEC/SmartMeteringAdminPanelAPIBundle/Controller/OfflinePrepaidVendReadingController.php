<?php

namespace HTEC\SmartMeteringAdminPanelAPIBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use HTEC\BaseModelBundle\Entity\PageParameters;
use HTEC\SmartMeteringModelBundle\Model\OfflineMeterReadingModel;

class OfflinePrepaidVendReadingController extends Controller
{
    protected $offlinePrepaidVendReadingModel;

    public function setOfflinePrepaidVendReadingModel(OfflinePrepaidVendReadingModel $model)
    {
        $this->offlinePrepaidVendReadingModel = $model;
    }

    public function getOfflinePrepaidVendReadingModel()
    {
        return $this->offlinePrepaidVendReadingModel;
    }

    public function getRecordAction($id)
    {
        $pagedData = $this->offlinePrepaidVendReadingModel->getRecordAction($id);

        $response = new Response();

        $response->setContent($pagedData['body']);
        $response->setStatusCode(Response::HTTP_OK);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    public function listRecordsAction(Request $request)
    {
        $pagedData = $this->offlinePrepaidVendReadingModel->getPagedData($request->query->all());

        $response = new Response();

        $response->setContent($pagedData['body']);
        $response->setStatusCode(Response::HTTP_OK);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    public function quickSearchAction(Request $request)
    {
        $pagedData = $this->offlinePrepaidVendReadingModel->quickSearch($request->query->all());

        $response = new Response();

        $response->setContent($pagedData['body']);
        $response->setStatusCode(Response::HTTP_OK);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }
}