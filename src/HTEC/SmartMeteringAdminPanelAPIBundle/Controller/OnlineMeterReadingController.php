<?php

namespace HTEC\SmartMeteringAdminPanelAPIBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use HTEC\BaseModelBundle\Entity\PageParameters;
use HTEC\SmartMeteringModelBundle\Model\OnlineMeterReadingModel;

class OnlineMeterReadingController extends Controller
{
    protected $onlineMeterReadingModel;

    public function setOnlineMeterReadingModel(OnlineMeterReadingModel $model)
    {
        $this->onlineMeterReadingModel = $model;
    }

    public function getOnlineMeterReadingModel()
    {
        return $this->onlineMeterReadingModel;
    }

    public function getRecordAction($id)
    {
        $pagedData = $this->onlineMeterReadingModel->getRecordAction($id);

        $response = new Response();

        $response->setContent($pagedData['body']);
        $response->setStatusCode(Response::HTTP_OK);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    public function listRecordsAction(Request $request)
    {
        $pagedData = $this->onlineMeterReadingModel->getPagedData($request->query->all());

        $response = new Response();

        $response->setContent($pagedData['body']);
        $response->setStatusCode(Response::HTTP_OK);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    public function quickSearchAction(Request $request)
    {
        $pagedData = $this->onlineMeterReadingModel->quickSearch($request->query->all());

        $response = new Response();

        $response->setContent($pagedData['body']);
        $response->setStatusCode(Response::HTTP_OK);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }
}