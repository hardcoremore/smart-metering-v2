<?php

namespace HTEC\SmartMeteringAdminPanelAPIBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use HTEC\BaseModelBundle\Entity\PageParameters;
use HTEC\SmartMeteringModelBundle\Model\OfflineMeterReadingModel;

class OfflineMeterReadingController extends Controller
{
    protected $offlineMeterReadingModel;

    public function setOfflineMeterReadingModel(OfflineMeterReadingModel $model)
    {
        $this->offlineMeterReadingModel = $model;
    }

    public function getOfflineMeterReadingModel()
    {
        return $this->offlineMeterReadingModel;
    }

    public function getRecordAction($id)
    {
        $pagedData = $this->offlineMeterReadingModel->getRecordAction($id);

        $response = new Response();

        $response->setContent($pagedData['body']);
        $response->setStatusCode(Response::HTTP_OK);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    public function listRecordsAction(Request $request)
    {
        $pagedData = $this->offlineMeterReadingModel->getPagedData($request->query->all());

        $response = new Response();

        $response->setContent($pagedData['body']);
        $response->setStatusCode(Response::HTTP_OK);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    public function quickSearchAction(Request $request)
    {
        $pagedData = $this->offlineMeterReadingModel->quickSearch($request->query->all());

        $response = new Response();

        $response->setContent($pagedData['body']);
        $response->setStatusCode(Response::HTTP_OK);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    public function patchAction(Request $request, $id)
    {
        $token = $this->container->get('security.token_storage')->getToken();

        $responseData = $this->offlineMeterReadingModel->update($id, ['approvedBy' => $token->getUser()->getId()]);

        $response = new Response();

        $response->setContent($responseData['body']);
        $response->setStatusCode(Response::HTTP_OK);
        $response->headers->set('Content-Type', 'application/json');

        return $response;   
    }
}