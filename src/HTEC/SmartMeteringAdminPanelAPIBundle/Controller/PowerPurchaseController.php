<?php

namespace HTEC\SmartMeteringAdminPanelAPIBundle\Controller;

use HTEC\BaseAPIBundle\Controller\BaseRestController;

use Symfony\Component\HttpFoundation\Request;

class PowerPurchaseController extends BaseRestController
{
    public function listAllForTodayAction(Request $request)
    {
        $queryParameters = $request->query->all();

        $allData = $this->getModel()->readAllForToday($queryParameters);

        $serializationContext = $this->getSerializationContext();
        $serializationContext->setGroups($this->getSerializationGroupsForSingleRecordData());

        $view = $this->view($allData, 200)
                     ->setContext($serializationContext);

        return $this->handleView($view);
    }
}