<?php

namespace HTEC\SmartMeteringUtilityAPIBundle\Controller;

use HTEC\BaseModelBundle\Model\BaseModel;
use HTEC\BaseAPIBundle\Controller\BaseRestController;
use HTEC\BaseModelBundle\Exception\Entity\EntityNotFoundException;
use HTEC\BaseModelBundle\Exception\Form\InvalidFormDataException;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class EstimateConsumptionController extends BaseRestController
{
    protected $customerModel;

    public function setCustomerModel(BaseModel $customer)
    {
        $this->customerModel = $customer;
    }

    public function getCustomerModel():BaseModel
    {
        return $this->customerModel;
    }

    public function getCustomersEstimatedConsumption(string $accountNumber, string $start, string $end)
    {
        $customer = $this->getCustomerModel()->readAll(['accountNumber' => $accountNumber], 1);

        if($customer)
        {
            $startDate = new \DateTime($start);
            $endDate = new \DateTime($end);

            if($startDate->getTimestamp() < $endDate->getTimestamp())
            {
                $estimatedConsumption = $this->getModel()->getCustomersEstimatedConsumption($customer, $startDate, $endDate);

                $response = new Response();

                $response->setContent(json_encode(['estimated_consumption' => $estimatedConsumption]));
                $response->setStatusCode(Response::HTTP_OK);
                $response->headers->set('Content-Type', 'application/json');

                return $response;
            }
            else
            {
                $exception = new InvalidFormDataException("End date must be greater than start date");
                $exception->setErrorMessages(['end' => 'End date is invalid. It must be greated than start date']);

                throw $exception;
            }
        }
        else
        {
            throw new EntityNotFoundException('Customer is not found for account number: ' . $accountNumber);
        }
    }
}