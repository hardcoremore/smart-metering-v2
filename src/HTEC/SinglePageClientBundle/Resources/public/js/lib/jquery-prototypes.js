/* extend jQuery prototype object with new fn for chaining */
$.fn.extend({

    equalHeight: function () {

        var tallest = $(this[0]).outerHeight() || 0;
        var allElementsEqual = true;

        this.each(function () {
            var el = $(this);

            //reset all sizes if needed
            if (el.prop('style').height !== '') {
                el.removeAttr('style');
            }

            var elHeight = el.outerHeight();

            //check if elements need resizing-equalization
            if (elHeight != tallest) {
                allElementsEqual = false;
            }

            //set tallest
            if (elHeight > tallest) {
                tallest = elHeight;
            }

        });

        if (allElementsEqual) {
            return;
        } else {
            return this.each(function () {
                $(this).height(tallest);
            });
        }
    }
});