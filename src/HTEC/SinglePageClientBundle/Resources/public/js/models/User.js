HCM.define({

    name: 'Model.User',

    extendFrom: 'Base.Model',

    construct: function() {

        this.callConstructor('Base.Model');

        var that = this;
        var urlPart = '/user';
        var isUserLoggedIn = false;
        var currentUser = null;

        var fields = [];
        var validations = [];


        this.USERNAME_LOGIN_POST_PROPERTY_NAME = '__elavon_creator_api_login_username__';
        this.PASSWORD_LOGIN_POST_PROPERTY_NAME = '__elavon_creator_api_login_password__';

        this.getIsUserLoggedIn = function() {
            return isUserLoggedIn;
        };

        this.setIsUserLoggedIn = function(loggedIn) {
            isUserLoggedIn = loggedIn;
        };

        this.getUrlPart = function() {
            return urlPart;
        };

        this.getCurrentUser = function() {
            return currentUser;
        };

        this.setCurrentUser = function(cu) {
            currentUser = cu;
        };

        this.getFields = function() {
            return fields;
        };

        this.getValidationRules = function() {
            return validations;
        };
    },

    methods: {

        events: HCM.getNamespaceValue('Events.Model.User'),

        login: function(username, password, options) {

            var that = this;

            var data = {};
                data[this.USERNAME_LOGIN_POST_PROPERTY_NAME] = username;
                data[this.PASSWORD_LOGIN_POST_PROPERTY_NAME] = password;

            $.extend(options, true, {

                data: data,

                url: this.getUrl('/login'),

                success: function(data){
                    that.setCurrentUser(data);
                    that.setIsUserLoggedIn(true);
                    that.dispatchSuccess(that.events.LOGIN_COMPLETE, data, options);
                },

                error: function(error) {
                    that.setIsUserLoggedIn(false);
                    that.dispatchError(that.events.LOGIN_ERROR, error, options);
                }
            });

            this.getRequester().POST(options);
        },

        loadCurrentUser: function(options) {

            var that = this;

            $.extend(options, true, {

                url: this.getUrl('/current-user'),

                success: function(data){
                    that.setCurrentUser(data);
                    that.dispatchSuccess(that.events.LOAD_CURRENT_USER_COMPLETE, data, options);
                },

                error: function(error) {
                    that.dispatchError(that.events.LOAD_CURRENT_USER_ERROR, error, options);
                }
            });

            this.getRequester().GET(options);
        },

        logout: function(options) {

            var that = this;

            this.setCurrentUser(null);

            this.getRequester().POST({

                url: this.getUrl('/logout'),
                dataType: 'text',

                success: function(data){
                    that.dispatchSuccess(that.events.LOGOUT_COMPLETE, data, options);
                },

                error: function(error) {
                    that.setIsUserLoggedIn(false);
                    that.dispatchError(that.events.LOGOUT_ERROR, error, options);
                }
            });
        }
    }
});
