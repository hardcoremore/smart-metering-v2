HCM.define({

    name: 'Model.Permission',

    extendFrom: 'Base.Model',

    extendPrototypeWith: {
        events: HCM.getNamespaceValue('Events.Model.Permission')
    },

    construct: function() {

        this.callConstructor('Base.Model');

        var urlPart = '/permission';

        this.getUrlPart = function() {
            return urlPart;
        };
    },

    methods: {

        convertResponseDataToTreeData: function(responseData) {

            var treeData = [];
            var item, children, index;
            var permissionGroups = {};

            for(var i = 0, len = responseData.length; i < len; i++) {

                item = responseData[i];

                children = [];

                for(index in item.actions) {
                    children.push({text: item.actions[index], permissionData: {action: item.actions[index], domain: item.domain}});
                }

                if(permissionGroups.hasOwnProperty(item.group_name) === false) {
                    permissionGroups[item.group_name] = [];
                }

                permissionGroups[item.group_name].push({text: item.name, children: children, id: item.identifier, domain: item.domain});
            }

            for(var groupIndex in permissionGroups) {
                treeData.push({
                    text: groupIndex,
                    children:  permissionGroups[groupIndex]
                })
            }

            return treeData;
        },

        convertTreeDataToUpdateData: function(treeData) {

            var updateData = {};
            var item;

            for(var i = 0, len = treeData.length; i < len; i++) {

                item = treeData[i];

                if(item.hasOwnProperty('permissionData')) {

                    if(updateData.hasOwnProperty(item.permissionData.domain) === false) {
                        updateData[item.permissionData.domain] = [];
                    }

                    updateData[item.permissionData.domain].push(item.permissionData.action);
                }
            }

            return updateData;
        }
    }
});