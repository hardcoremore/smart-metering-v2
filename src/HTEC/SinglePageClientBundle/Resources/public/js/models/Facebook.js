HCM.define({

    name: 'Model.Facebook',

    extendFrom: 'Base.Model',

    construct: function() {

        this.callConstructor('Base.Model');

        var that = this;

        var userFacebookId;
        var sessionData;

        var loadedAlbumPhotos = {};

        var currentAlbums;
        var currentAlbumPhotos;


        var albumsPaging;
        var photosPaging;

        var selectedPhoto;

        this.getSelectedPhoto = function() {
            return selectedPhoto;
        };

        this.setSelectedPhoto = function(photo) {

            selectedPhoto = photo;

            var e = $.Event(
                this.events.PHOTO_SELECTED,
                {
                    photo: selectedPhoto
                }
            );

            $(document).trigger(e);
        };

        this.resetSelectedPhoto = function() {
            selectedPhoto = null;
        };

        this.setCurrentAlbums = function(ca) {
            currentAlbums = ca;
        };

        this.getCurrentAlbums = function() {
            return currentAlbums;
        };

        this.setCurrentAlbumPhotos = function(cap) {
            currentAlbumPhotos = cap;
        };

        this.getCurrentAlbumPhotos = function() {
            return currentAlbumPhotos;
        };

        this.getAlbumsPaging = function() {
            return albumsPaging;
        };

        this.setAlbumsPaging = function(ap) {
            albumsPaging = ap;
        };

        this.setPhotosPaging = function(pp) {
            photosPaging = pp;
        };

        this.getPhotosPaging = function() {
            return photosPaging;
        };

        this.getUserFacebookId = function() {
            return userFacebookId;
        };

        this.getSessionData = function() {
            return sessionData;
        };

        this.userLoginCompleteEventHandler = function(response) {

            sessionData = response.authResponse;

            var e = $.Event(
                that.events.LOGIN_COMPLETE,
                {
                    response: response
                }
            );

            $(document).trigger(e);
        };

        this.albumsLoadCompleteEventHandler = function(response) {

            if(response && response.data && response.data.length > 0) {
                
                that.setCurrentAlbums(response.data);

                var albums = {};

                for(var i = 0; i < response.data.length; i++) {
                    albums[response.data[i].id] = response.data[i];
                }
            }

            that.setAlbumsPaging(response.paging);

            var e = $.Event(
                that.events.ALBUMS_LOAD_COMPLETE,
                {
                    albums: albums
                }
            );

            $(document).trigger(e);

        };
    },

    methods: {

        events: HCM.getNamespaceValue('Events.Model.Facebook'),

        login: function(fbScope) {

            var config = this.getConfig();

            FB.init({
                appId: config.getParameter('facebookAppId'),
                xfbml: false,
                version: config.getParameter('facebookAPIVersion')
            });

            FB.login(this.userLoginCompleteEventHandler, {
                scope: fbScope,
                return_scopes: true
            });
        },

        loadAlbums: function(nextPageLink) {

            if(nextPageLink) {
                FB.api(nextPageLink, this.albumsLoadCompleteEventHandler);
            }
            else {
                FB.api('me/albums', this.albumsLoadCompleteEventHandler);
            }
        },

        loadAlbumCoverPhoto: function(albumId) {

            var that = this;

            FB.api("/" + albumId + "/picture",
                function(response) {

                var e = $.Event(
                    that.events.ALBUM_COVER_PHOTO_LOAD_COMPLETE,
                    {
                        albumId: albumId,
                        response: response
                    }
                );

                $(document).trigger(e);
            });
        },

        loadAlbumPhotos: function(albumId, nextPageLink) {

            var that = this;

            var photosLoadedCallback = function(response) {

                that.setPhotosPaging(response.paging);
                that.setCurrentAlbumPhotos(response.data);

                var e = $.Event(
                    that.events.ALBUM_PHOTOS_LOAD_COMPLETE,
                    {
                        albumId: albumId,
                        photos: response.data,
                        paging: response.paging
                    }
                );

                $(document).trigger(e);
            };

            if(nextPageLink) {
               FB.api(nextPageLink, photosLoadedCallback);
            }
            else {
                FB.api("/"+albumId+"/photos", photosLoadedCallback);
            }
        },

        loadPhotoById: function(photoId) {
            
            var that = this;

            FB.api("/" + photoId + "?fields=id,images",
                function(response) {

                var e = $.Event(
                    that.events.PHOTO_LOAD_COMPLETE,
                    {
                        photoId: photoId,
                        response: response
                    }
                );

                $(document).trigger(e);
            });
        },
    }
});
