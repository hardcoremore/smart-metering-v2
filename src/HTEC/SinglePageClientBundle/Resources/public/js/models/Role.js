HCM.define({

    name: 'Model.Role',

    extendFrom: 'Base.Model',

    extendPrototypeWith: {
        events: HCM.getNamespaceValue('Events.Model.Role')
    },

    construct: function(permissionModel) {

        this.callConstructor('Base.Model');

        var urlPart = '/role';

        var fields =  [
            {name: 'id', type: 'integer'},
            {name: 'name', type: 'string'},
            {
                name: 'permissions',
                type: 'relation',
                relationType: 'manyToMany',
                relationModel: permissionModel,
                labelPropertyName: 'name',
                valuePropertyName: 'id'
            },
        ];

        var validationRules = [
            {field: 'name', type: 'presence'},
            {field: 'permissions', type: 'presence'}
        ];

        this.getUrlPart = function() {
            return urlPart;
        };

        this.getFields = function() {
            return fields;
        };

        this.getValidationRules = function() {
            return validationRules;
        };
    }
});
