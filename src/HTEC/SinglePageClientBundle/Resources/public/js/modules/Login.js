HCM.define({

    name: 'Modules.Login',

    extendFrom: 'Base.Module',

    construct: function(app, permissions) {
        
        this.callConstructor('Base.Module', [app, permissions]);

        var loginForm;
        var loginButton;
        var loginFormErrorMessageContainer;

        var authenticationController;
        var userModel;

        this.setLoginForm = function(form){
            loginForm = form;
        };

        this.getLoginForm = function(){
            return loginForm;
        };

        this.setLoginButton = function(button){
            loginButton = button;
        };

        this.getLoginButton = function(){
            return loginButton;
        };

        this.setLoginFormErrorMessageContainer = function(errorMessageCnt){
            loginFormErrorMessageContainer = errorMessageCnt;
        };

        this.getLoginFormErrorMessageContainer = function(){
            return loginFormErrorMessageContainer;
        };

        /*----------------  END OF MODELS get/set  ------------------- */


        /*-------------------------------------------
         *           CONTROLLERS get/set
         *------------------------------------------*/
        this.setAuthenticationController = function(authController) {
            authenticationController = authController;
        };

        this.getAuthenticationController = function() {
            return authenticationController;
        };

        this.getModuleName = function() {
            return 'Login';
        };
    },

    methods: {

        initModule: function() {

            /*------- CALL PARENT METHOD --------*/
            this.super('initModule');

            /*------- INITIALIZE CONTROLLERS --------*/
            this.setAuthenticationController(this.getApp().getController('Controllers.Authentication', [this]));
        },

        startModule: function() {
            this.super('startModule');
            this.getApp().startController(this.getAuthenticationController());
            this.getLoginFormErrorMessageContainer().hide();
        },

        stopModule: function() {
            this.getApp().stopController(this.getAuthenticationController());  
            this.super('stopModule');
        },

        setControls: function() {
            this.super('setControls');
            this.setLoginForm(this.getModuleElement().find('#login-form'));
            this.setLoginButton(this.getModuleElement().find('#login-button'));
            this.setLoginFormErrorMessageContainer(this.getModuleElement().find('#login-form-error-message-container'));
        },

        login: function(username, password) {

            this.getApp().getUserModel().login(
                username,
                password,
                {scope:this}
            );
        }
    }
});
