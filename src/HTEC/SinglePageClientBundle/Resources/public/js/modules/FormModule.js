HCM.define({

    name: 'Modules.FormModule',

    extendFrom: 'Base.Module',

    construct: function(app, permissions) {

        this.callConstructor('Base.Module', [app, permissions]);

        var model;
        var isFormChanged;
        var formContainer;
        var formSubmitButton;
        var formResetButton;
        var formResetPopup;
        var formClosePopup;
        var formCloseButton;
        var formController;
        var editingRecordData;
        var formMode;
        var showFormErrorTooltipTimeout;

        this.setModel = function(m) {
            model = m;
        };

        this.getModel = function(m) {
            return model;
        };

        this.setIsFormChanged = function(changed) {
            isFormChanged = changed;
        };

        this.getIsFormChanged = function() {
            return isFormChanged;
        };

        this.setFormContainer = function(form) {
            formContainer = form;
        };

        this.getFormContainer = function() {
            return formContainer;
        };

        this.setFormSubmitButton = function(fsb) {
            formSubmitButton = fsb;
        };

        this.getFormSubmitButton = function() {
            return formSubmitButton;
        };

        this.setFormResetButton = function(frb) {
            formResetButton = frb;
        };

        this.getFormResetButton = function() {
            return formResetButton;
        };

        this.setFormCloseButton = function(fcb) {
            formCloseButton = fcb;
        };

        this.getFormCloseButton = function() {
            return formCloseButton;
        };

        this.setFormResetPopup = function(frp) {
            formResetPopup = frp;
        };

        this.getFormResetPopup = function() {
            return formResetPopup;
        };

        this.setFormClosePopup = function(fcp) {
            formClosePopup = fcp;
        };

        this.getFormClosePopup = function() {
            return formClosePopup;
        };

        this.setFormController = function(fc) {
            formController = fc;
        };

        this.getFormController = function() {
            return formController;
        };

        this.setEditingRecordData = function(erd) {
            editingRecordData = erd;
        };

        this.getEditingRecordData = function() {
            return editingRecordData;
        };

        this.setFormMode = function(mode) {
            formMode = mode;
        };

        this.getFormMode = function() {
            return formMode;
        };

        this.setShowFormErrorTooltipTimeout = function(timeout) {
            showFormErrorTooltipTimeout = timeout;
        };

        this.getShowFormErrorTooltipTimeout = function() {
            return showFormErrorTooltipTimeout;
        };

        this.getModuleName = function() {
            return 'FormModule';
        };
    },

    methods: {

        initModule: function() {
            this.initModel();
            this.initController();
            this.super('initModule');
        },

        initModel: function() {},

        initController: function() {
            var formController = this.getApp().getController('Controllers.Form.BaseForm', [this, this.getModel()]);
            this.setFormController(formController);
        },

        setControls: function() {

            this.setFormContainer(this.getModuleElement().find('.module-content-container'));

            this.setFormSubmitButton(this.getModuleElement().find('.submit-button'));
            this.setFormResetButton(this.getModuleElement().find('.reset-button'));
            this.setFormCloseButton(this.getModuleElement().find('.close-button'));

            var resetFormPopup = this.getModuleElement().find("#reset-form-popup");

            if(resetFormPopup.length === 1) {

                resetFormPopup.popup({
                    compileTemplateFunction: this.getApp().compileTemplate,
                    compileTemplateData: {
                        title: 'Form is not saved. Are you sure you want to reset?',
                        okLabel: 'Yes',
                        cancelLabel: 'No'
                    }
                });

                this.setFormResetPopup(resetFormPopup);
            }

            var closeFormPopup = this.getModuleElement().find("#close-form-popup");

            if(closeFormPopup.length === 1) {

                closeFormPopup.popup({
                    compileTemplateFunction: this.getApp().compileTemplate,
                    compileTemplateData: {
                        title: 'Form is not saved. Are you sure you want to close?',
                        okLabel: 'Close',
                        cancelLabel: 'No'
                    }
                });

                this.setFormClosePopup(closeFormPopup);
            }
        },

        startModule: function() {
            this.getApp().startController(this.getFormController());
            this.super('startModule');
        },

        stopModule: function() {

            this.resetForm();
            this.setIsFormChanged(false);
            this.setFormMode(null);
            this.setEditingRecordData(null);

            this.getApp().stopController(this.getFormController());

            this.super('stopModule');
        },

        allowStopModule: function() {
            return this.getIsFormChanged() !== true;
        },

        askForModuleStop: function(moduleStopCallback) {

            this.super('askForModuleStop', [moduleStopCallback]);

            if(this.getFormClosePopup()) {
                this.getFormClosePopup().popup('open');
            }            
        },

        /**
         *
         * Display form errors for every form field.
         *
         * @param {Object} errors Object containing all field errors.
            Every key of errors object is form field name in model field definition.
            Every value of errors object is array containing error messages for that field.
         *
         */
        displayFormErrors: function(formErrors) {

            this.removeAllFormErrors();

            var self = this;
            var errorKeys = Object.keys(formErrors);
            var fieldName, formFieldErrors;

            for(var i = 0, len = errorKeys.length; i < len; i++) {

                fieldName = errorKeys[i];
                formFieldErrors = formErrors[fieldName];
                
                this.showFormFieldError(fieldName, formFieldErrors);
            }
        },

        showFormFieldError: function(fieldName, formFieldErrors) {

            var formFieldErrorHolder = this.getFormFieldErrorHolder(fieldName);

            var fieldInput = this.getFormFieldByName(fieldName);
                fieldInput.addClass('form-input-error');

            var errorsListElement = $('<ul/>', {
                class: 'form-field-errors-list'
            });

            formFieldErrorHolder.append(errorsListElement);

            if(this.isArray(formFieldErrors)) {

                for(var errorMessageIndex in formFieldErrors) {
                    errorsListElement.append($('<li/>', {
                        text: formFieldErrors[errorMessageIndex],
                        class: 'form-field-error-message'
                    }));
                }
            }
            else if(this.isString(formFieldErrors)) {
                errorsListElement.append($('<li/>', {
                    text: formFieldErrors,
                    class: 'form-field-error-message'
                }));
            }
            else {
                this.throwException('Form field "' + fieldName + '" message is invalid', 1500, 'kernel');
            }

            this.tooltipFormFieldError(fieldName);
        },

        tooltipFormFieldError: function(fieldName) {

            var tooltipTimeout = this.getShowFormErrorTooltipTimeout();

            var formFieldErrorHolder = this.getFormFieldErrorHolder(fieldName);
            var errorsListElement = formFieldErrorHolder.find('.form-field-errors-list');
            var tooltipContent = null;

            var errorTooltipPosition = 'right';

            if(this.getFormContainer().find('form').first().hasClass('form-errors-vertical')) {
                errorTooltipPosition = 'top';
            }

            if(formFieldErrorHolder.children().length) {

                if(errorsListElement.length === 1) {
                    tooltipContent = errorsListElement;
                }
                else {
                    tooltipContent = $(formFieldErrorHolder.html());
                }

                var fieldInput = this.getFormFieldByName(formFieldErrorHolder.data('field-name'));
                    fieldInput.tooltipster({
                        content: tooltipContent,
                        position: errorTooltipPosition,
                        debug: false,
                        animationDuration: 0,
                        theme: 'tooltipster-default tooltipster-error'
                    });

                    fieldInput.tooltipster('show');
            }
        },

        removeFormFieldErrorTooltip: function(fieldName) {
            this.getFormFieldByName(fieldName).tooltipster('destroy');
        },

        isFormFieldErrorTooltip: function(fieldName) {
            return this.getFormFieldByName(fieldName).hasClass('tooltipstered');
        },

        blockFormField: function(fieldName, message) {

            var fieldInput = this.getFormFieldByName(fieldName);
                fieldInput.addClass('form-field-disabled');

            if(message) {

                fieldInput.tooltipster({
                    content: message,
                    autoClose: false,
                    position: 'right',
                    debug: false,
                    theme: 'tooltipster-default tooltipster-blue'
                });

                fieldInput.tooltipster('show');
            }
        },

        unblockFormField: function(fieldName) {

            var fieldInput = this.getFormFieldByName(fieldName);
                fieldInput.removeClass('form-field-disabled');

            if(fieldInput.hasClass('tooltipstered')) {
                fieldInput.tooltipster('destroy');
            }
        },        

        /**
         *
         * Populate form with data.
         *
         * @param {Object} data Data to populate form with
         *
         */
        populateForm: function(data) {

            if(this.isObject(data)) {

                var self = this;

                var dataKeys = Object.keys(data);
                var fieldInputElement = null;

                var fieldName = '';

                for(var i = 0, len = dataKeys.length; i < len; i++) {

                    fieldName = dataKeys[i];

                    fieldInputElement = this.getFormFieldByName(fieldName);

                    this.populateFormField(fieldName, data[fieldName], fieldInputElement);
                }    
            }
        },

        populateFormField: function(fieldName, value, fieldInputElement) {
            this.setFormFieldValue(fieldName, value, fieldInputElement);
        },

        setFormFieldValue: function(fieldName, value, fieldInputElement) {

            if(value !== null) {
                value = value.toString();
            }

            var previousValue = this.getFormFieldValue('fieldName', fieldInputElement);
                fieldInputElement.data('prev-value', previousValue);

            fieldInputElement.val(value);
        },

        getFormFieldValue: function(fieldName, fieldInputElement) {

            fieldInputElement = fieldInputElement || this.getFormFieldByName(fieldName);

            return fieldInputElement.val();
        },

        getFormData: function(includeEmptyFields) {

            var formData = {};
            var self = this;

            //get all data from the HTML form inputs
            this.getFormContainer().find("input[name], textarea[name], select[name]").each(function(index) {

                input = $(this);
                fieldName = input.attr('name');

                if(fieldName) {

                    formData[fieldName] = self.getFormFieldValue(fieldName, input);

                    if(String(formData[fieldName]).length === 0 && includeEmptyFields !== true) {
                        delete formData[fieldName];
                    }
                }
            });

            return formData;
        },

        prepareDataForRead: function(data) {
            return data;
        },

        prepareDataForCreate: function(data) {
            return data;
        },

        prepareDataForUpdate: function(data, recordId) {
            return data;
        },

        getEditingRecordId: function() {

            var editingRecordData = this.getEditingRecordData();

            if(this.isObject(editingRecordData) && editingRecordData.hasOwnProperty('id')) {
                return editingRecordData.id;
            }

            return null;
        },

        submitFormRead: function(formData) {
            this.getModel().read({
                parameters: this.prepareDataForRead(formData),
                scope: this
            });
        },

        submitFormCreate: function(formData) {
            this.getModel().create(this.prepareDataForCreate(formData), {scope:this});
        },

        submitFormUpdate: function(recordId, formData) {
            this.getModel().update(recordId, this.prepareDataForUpdate(formData, recordId), {scope:this});
        },

        /**
         *
         * Reset all fields in the form.
         *
         *
         */
        resetForm: function() {

            var that = this;

            // reset form fields
            this.getAllFormFields()
                .each(function(index){

                    var fieldElement = $(this);
                    var fieldName = fieldElement.attr('name');

                    if(fieldName) {
                        that.resetFormField(fieldName, fieldElement);
                    }
            });

            this.setEditingRecordData(null);
            this.removeAllFormErrors();
            this.setIsFormChanged(false);

            var constants = this.getApp().getConstants();
            var e = $.Event(constants.FORM_RESETTED_EVENT, {scope: this});
            $(document).trigger(e);
        },

        getAllFormFields: function() {
            return this.getFormContainer().find("input[name], textarea[name], select[name]");
        },

        resetFormField: function(fieldName, inputElement) {

            if(inputElement.prop('disabled') === false) {

                if(inputElement.is(':checkbox')) {
                    inputElement.prop('checked', false);
                }
                else {
                    inputElement.val('');
                }
            }
            else {
                inputElement.val('');
            }
        },

        removeAllFormErrors: function() {

            var formFields = this.getAllFormFields();
            var self = this;

            formFields.each(function(index){

                var formFieldElement = $(this);
                var fieldName = self.getFormFieldNameFromFormFieldElement(formFieldElement);

                var formFieldErrorHolder = self.getFormFieldErrorHolder(fieldName);

                if(self.isFormFieldErrorTooltip(fieldName)) {
                    self.removeFormFieldErrorTooltip(fieldName);
                }

                formFieldElement.removeClass('form-input-error');
            });
        },

        getFormFieldByName: function(elementName) {

            var formElement = this.getFormContainer().find("[name='"+elementName+"']");

            if(formElement.length > 0) {
                return formElement.first();
            }

            return formElement;
        },

        getFormFieldNameFromFormFieldElement: function(formFieldElement) {
            if(formFieldElement) {
                return formFieldElement.attr('name');
            }
        },

        getFormFieldErrorHolder: function(fieldName) {
            return this.getFormContainer().find('.form-field-error-' + fieldName).first();
        },

        initializeDateField: function(fieldName, options) {

            var field = this.getFormFieldByName(fieldName);

            return field.datepick({ 
                dateFormat: 'yyyy-mm-dd',
                onSelect: function(dates) {

                    var format = 'YYYY-MM-DD';
                    var prevDate = field.data('prev-value');

                    if(prevDate) {
                        if(moment(prevDate).format(format) !== moment(dates[0]).format(format)) {
                            field.data('prev-value', moment(dates[0]).format(format));
                            field.trigger('input');
                        }
                    }
                    else {
                        field.data('prev-value', moment(dates[0]).format(format));
                        field.trigger('input');
                    }
                } 
            });
        },

        initializeTimeField: function(fieldName, options) {

            this.getFormFieldByName(fieldName).datetimepicker({
                dateFormat: "yy-mm-dd",
                timeOnly: true
            });
        },

        updateFormSubmitButtonLabel: function(formMode) {

            if(formMode === 'edit') {
                this.updateSubmitButtonLabel('Save');
            }
            else if(formMode === 'create') {
                this.updateSubmitButtonLabel('Create');
            }
            else {
                this.updateSubmitButtonLabel('Search');
            }
        },

        updateSubmitButtonLabel: function(label) {
            this.getFormSubmitButton().find('.button-label').text(label);
        }
    }
});