HCM.define({

    name: 'Modules.CrudModule',

    extendFrom: 'Base.Module',

    construct: function(app, permissions) {

        this.callConstructor('Base.Module', [app, permissions]);

        var that = this;

        var model;
        var formModule;
        var modelRelationFields;

        var selectedRecord;
        var selectedRecordIndex;
        var currentSortColumnName;
        var currentSortColumnOrder;
        var searchParameters;

        var moduleCrudControls;
        var mainViewComponent;
        var quickSearchComponent;
        var pagingComponent;
        var viewRecordDetailsContainer;
        var viewRecordDetailsControls;
        var recordDeletePopup;
        var gridEditNotCompletePopup;

        var crudController;

        var notAutoLoadingRelationFields = [];

        var relationFieldSelectData = {};

        this.setModel = function(m) {
            model = m;
        };

        this.getModel = function() {
            return model;
        };

        this.setFormModule = function(fm) {
            formModule = fm;
        };

        this.getFormModule = function() {
            return formModule;
        };

        this.setModelRelationFields = function(mrf) {
            modelRelationFields = mrf;
        };

        this.getModelRelationFields = function() {
            return modelRelationFields;
        };

        this.setSelectedRecord = function(record) {
            selectedRecord = record;
        };

        this.getSelectedRecord = function() {
            return selectedRecord;
        };

        this.setSelectedRecordIndex = function(index) {
            selectedRecordIndex = index;
        };

        this.getSelectedRecordIndex = function() {
            return selectedRecordIndex;
        };

        this.setCurrentSortColumnName = function(columnName) {
            currentSortColumnName = columnName;
        };

        this.getCurrentSortColumnName = function() {
            return currentSortColumnName;
        };

        this.setCurrentSortColumnOrder = function(columnOrder) {
            currentSortColumnOrder = columnOrder;
        };

        this.getCurrentSortColumnOrder = function() {
            return currentSortColumnOrder;
        };

        this.setSearchParameters = function(params) {
            searchParameters = params;
        };

        this.getSearchParameters = function() {
            return searchParameters;
        };

        this.setController = function(cc) {
            crudController = cc;
        };

        this.getController = function() {
            return crudController;
        };

        this.setModuleCrudControls = function(mcc) {
            moduleCrudControls = mcc;
        };

        this.getModuleCrudControls = function() {
            return moduleCrudControls;
        };

        this.setMainViewComponent = function(mvc) {
            mainViewComponent = mvc;
        };

        this.getMainViewComponent = function() {
            return mainViewComponent;
        };

        this.setQuickSearchComponent = function(qs) {
            quickSearchComponent = qs;
        };

        this.getQuickSearchComponent = function() {
            return quickSearchComponent;
        };

        this.setPagingComponent = function(pc) {
            pagingComponent = pc;
        };

        this.getPagingComponent = function() {
            return pagingComponent;
        };

        this.setViewRecordDetailsContainer = function(vrc) {
            viewRecordDetailsContainer = vrc;
        };

        this.getViewRecordDetailsContainer = function() {
            return viewRecordDetailsContainer;
        };

        this.setViewRecordDetailsControls = function(vrdc) {
            viewRecordDetailsControls = vrdc;
        };

        this.getViewRecordDetailsControls = function() {
            return viewRecordDetailsControls;
        };

        this.setNotAutoLoadingRelationFields =  function(narf) {
            notAutoLoadingRelationFields = narf;
        };

        this.getNotAutoLoadingRelationFields =  function() {
            return notAutoLoadingRelationFields;
        };

        this.setRecordDeletePopup = function(popup) {
            recordDeletePopup = popup;
        };

        this.getRecordDeletePopup = function() {
            return recordDeletePopup;
        };

        this.setGridEditNotCompletePopup = function(popup) {
            gridEditNotCompletePopup = popup;
        };

        this.getGridEditNotCompletePopup = function() {
            return gridEditNotCompletePopup;
        };

        this.setRelationFieldSelectData = function(fieldName, data) {
            relationFieldSelectData[fieldName] = data;
        };

        this.getRelationFieldSelectData = function(fieldName) {
            return relationFieldSelectData[fieldName];
        };

        this.getAllSelectdata = function() {
            return relationFieldSelectData;
        };

        this.getFormModuleClassname = function() {
            return 'Base.FormModule';
        };

        this.getModuleName = function() {
            return 'CrudModule';
        };
    },

    methods: {

        initModule: function() {

            this.initModel();
            this.initController();

            this.getModel().getRecordManager().registerForDataMonitor(this.getInstanceId());

            var relationFields = this.getModel().getRelationFields();

            this.setModelRelationFields(relationFields);

            if(relationFields && relationFields.length > 0) {

                for(var i = 0, len = relationFields.length; i < len; i++) {
                    relationFields[i].relationModel.getRecordManager().registerForDataMonitor(this.getInstanceId());
                }
            }

            this.super('initModule');
        },

        initModel: function() {},

        initController: function() {
            var crudController = this.getApp().getController('Controllers.Crud', [this, this.getModel()]);
            this.setController(crudController);
        },

        startModule: function() {
            this.super('startModule');
            this.getApp().startController(this.getController());
        },

        stopModule: function() {

            if(this.getFormModule()) {

                if(this.getFormModule().getIsModuleRunning()) {
                    this.getFormModule().stopModule();
                }

                if(this.getFormModule().isModuleVisible()) {
                    this.getFormModule().hideModule();    
                }           
            }

            this.getApp().stopController(this.getController());
            this.super('stopModule');
        },

        resolveModulePermissions: function(permissions) {

            if(permissions && this.isArray(permissions.actions)) {

                if(permissions.actions.indexOf('create') === -1) {
                    this.getModuleElement().find('[data-role="create-record"]').remove();
                }

                if(permissions.actions.indexOf('edit') === -1) {
                    this.getModuleElement().find('[data-role="edit-record"]').remove();
                }
            }
        },

        setControls: function() {

            this.setModuleCrudControls(this.getModuleElement().find('.module-crud-controls'));

            this.setMainViewComponent(this.getModuleElement().find('.main-view-component'));

            this.setViewRecordDetailsContainer(this.getModuleElement().find('.view-record-container'));

            this.setViewRecordDetailsControls(this.getModuleElement().find('.view-record-controls'));

            var quickSearch = this.getModuleElement().find('.module-quick-search').quicksearch({min:1});

            this.setQuickSearchComponent(quickSearch);

            var paginator = this.getModuleElement().find('.module-paging-cnt').paginator({
                rowsPerPage: this.getPagingParameters().rowsPerPage,
                pageNumber: 1
            });

            this.setPagingComponent(paginator);

            var recordDeletePopup = this.getModuleElement().find('#record-delete-popup').popup({
                compileTemplateFunction: this.getApp().compileTemplate,
                compileTemplateData: {
                    title: 'Are you sure you want to delete selected record?',
                    okLabel: 'Yes',
                    cancelLabel: 'No'
                }
            });

            this.setRecordDeletePopup(recordDeletePopup);

            var gridEditNotCompletePopup = this.getModuleElement().find('#grid-cell-edit-not-complete-popup').popup({
                compileTemplateFunction: this.getApp().compileTemplate,
                compileTemplateData: {
                    title: 'Grid cell edit is not complete. Are you sure you want to proceed?',
                    okLabel: 'Yes',
                    cancelLabel: 'No'
                }
            });

            this.setGridEditNotCompletePopup(gridEditNotCompletePopup);
        },

        allowStopModule: function() {

            if(this.getFormModule() && this.getFormModule().getIsModuleRunning()) {
                return this.getFormModule().getIsFormChanged() !== true;
            }
            else if(this.getMainViewComponent() && this.getMainViewComponent().grid('getIsTableCellEditing')) {
                return false;
            }
            else {
                return true;
            }
        },

        askForModuleStop: function(moduleStopCallback) {

            if(this.getFormModule() && this.getFormModule().getIsModuleRunning()) {
                this.getFormModule().askForModuleStop(moduleStopCallback);
            }
            else if(this.getMainViewComponent() && this.getMainViewComponent().grid('getIsTableCellEditing')) {

                this.super('askForModuleStop', [moduleStopCallback]);

                if(this.getGridEditNotCompletePopup()) {
                    this.getGridEditNotCompletePopup().popup('open');
                }
            }
            else {
                this.stopModule();
            }
        },

        updateModuleState: function(stateData) {

            if(stateData.options.length === 0 && this.getFormModule()) {
                this.closeFormModule(); 
            }
            else if(stateData.options.length > 0) {

                var option = stateData.options[0];

                if(option === 'create') {

                    if(this.getFormModule()) {
                        this.openCreateForm();
                    }
                    else {
                        this.loadFormModule('create');
                    }
                }
                else if(option === 'edit') {

                    if(this.getSelectedRecord()) {

                        if(this.getFormModule()) {
                            this.openEditForm(this.getSelectedRecord());
                        }
                        else {
                            this.loadFormModule('edit', this.getSelectedRecord());
                        }
                    }
                    else {
                        this.getApp().getHistoryManager().goToHashUrl(this.getModuleName());
                        this.getApp().showNotification('warning', 'Please select record first.');
                    }
                }
                else if(option === 'details' && this.getSelectedRecord()) {

                    if(this.getSelectedRecord()) {
                        // @to-do open details view
                    }
                    else {
                        this.getApp().getHistoryManager().goToHashUrl(this.getModuleName());
                        this.getApp().showNotification('warning', 'Please select record first.');
                    }
                }
            }
        },

        openCreateForm: function() {

            this.hideModule();

            this.getApp().getHistoryManager().goToHashUrl(this.getModuleName() + "/create");

            this.getFormModule().setFormMode('create');

            this.getApp().getModuleManager().startModule(this.getFormModule());

            this.getFormModule().showModule();
        },

        openEditForm: function(formData) {

            this.hideModule();

            this.getApp().getHistoryManager().goToHashUrl(this.getModuleName() + "/edit");

            this.getFormModule().setFormMode('edit');
            this.getFormModule().setEditingRecordData(formData);

            this.getApp().getModuleManager().startModule(this.getFormModule());

            this.getFormModule().showModule();
        },

        closeFormModule: function() {

            this.getApp().getHistoryManager().goToHashUrl(this.getModuleName());
            this.getFormModule().hideModule();
            this.showModule();
        },

        updateMainViewComponentFromMonitorData: function(monitorData) {

            if(monitorData) {

                var mainViewComponentData = this.getMainViewComponentData();

                if(monitorData.created && monitorData.created.length > 0) {
                    
                    var pagingParameters = this.getPagingComponentPageParameters();
                    var i, len;

                    if((mainViewComponentData.length + monitorData.created.length) <= pagingParameters.rowsPerPage) {

                        var combinedData = [];
                        monitorData.created = monitorData.created.reverse();

                        for(i = 0, len = monitorData.created.length; i < len; i++) {
                            combinedData.push(this.getModel().getRecordManager().getRecord(monitorData.created[i]));
                        }

                        combinedData = combinedData.concat(mainViewComponentData);

                        this.insertPagedDataIntoMainViewComponent(combinedData);
                    }
                    else {

                        pagingParameters = this.getPagingParameters(
                            pagingParameters.pageNumber,
                            pagingParameters.rowsPerPage,
                            this.getCurrentSortColumnName(),
                            this.getCurrentSortColumnOrder()
                        );

                        this.readRecords(pagingParameters);
                    }
                }
                else if(monitorData.updated) {

                    var record, updatedRecord, mergedRecord;
                    var newRecordKeys, newRecordProperty, newRecordPropertyValue;
                    var fieldModelData;

                    for(i = 0, len = mainViewComponentData.length; i < len; i++) {

                        record = mainViewComponentData[i];

                        if(monitorData.updated.hasOwnProperty(record.id)) {
                            
                            updatedRecord = this.getModel().getRecordManager().getRecord(record.id);
                            mergedRecord = $.extend({}, record, updatedRecord);

                            newRecordKeys = Object.keys(mergedRecord);

                            for(var k = 0, klen = newRecordKeys.length; k < klen; k++) {

                                newRecordProperty = newRecordKeys[k];
                                newRecordPropertyValue = mergedRecord[newRecordProperty];

                                fieldModelData = this.getModel().getFieldByName(newRecordProperty);
                                
                                if(fieldModelData && fieldModelData.type === 'relation') {

                                    if(this.isArray(newRecordPropertyValue)) {

                                        for(var c = 0, clen = newRecordPropertyValue.length; c < clen; c++) {

                                            if(this.isObject(newRecordPropertyValue[c]) === false) {
                                                newRecordPropertyValue[c] = this.getModel().findRecordByPropertyValue(
                                                    this.getRelationFieldSelectData(newRecordProperty),
                                                    'id',
                                                    newRecordPropertyValue[c]
                                                );
                                            }
                                        }

                                        mergedRecord[newRecordProperty] = newRecordPropertyValue;
                                    }

                                    this.setRelationFieldSelectData(newRecordProperty, mergedRecord[newRecordProperty]);
                                }
                            }

                            this.updateMainViewComponentRecord(i, mergedRecord);
                            break;
                        }
                    }
                }

                this.getModel().getRecordManager().removeMonitorData(this.getInstanceId());
            }
        },

        updateFieldFromMonitorData: function(fieldModelData) {

            var fieldRecordManager = fieldModelData.relationModel.getRecordManager();
            var fieldMonitorData = fieldRecordManager.getMonitorData(this.getInstanceId());
            var fieldSelectData = this.getRelationFieldSelectData(fieldModelData.name);

            if(fieldMonitorData) {

                var updatedSelectData = fieldRecordManager.getUpdatedFormFieldData(fieldMonitorData, fieldModelData, fieldSelectData);

                this.getMainViewComponent().grid(
                    'setColumnEditOptions',
                    fieldModelData.name,
                    {availableOptions: updatedSelectData}
                );

                this.setRelationFieldSelectData(fieldModelData.name, updatedSelectData);

                var mainViewComponentData = this.getMainViewComponentData();
                var record;

                for(var i = 0, len = mainViewComponentData.length; i < len; i++) {
                    
                    record = mainViewComponentData[i];

                    if(this.isArray(record[fieldModelData.name])) {

                        for(var c = 0, clen = record[fieldModelData.name].length; c < clen; c++) {
                            record[fieldModelData.name][c] = this.getModel().findRecordByPropertyValue(
                                this.getRelationFieldSelectData(fieldModelData.name),
                                'id',
                                record[fieldModelData.name][c].id
                            );
                        }
                    }
                    else {
                        record[fieldModelData.name] = this.getModel().findRecordByPropertyValue(
                            this.getRelationFieldSelectData(fieldModelData.name),
                            'id',
                            record[fieldModelData.name].id
                        );
                    }

                    this.updateMainViewComponentRecord(i, record);
                }

                fieldRecordManager.removeMonitorData(this.getInstanceId());
            }
        },

        openViewRecordDetails: function(recordData) {
            this.getViewRecordDetailsContainer().show();
            this.getViewRecordDetailsControls().show();
        },

        closeViewRecordDetails: function() {
            this.getViewRecordDetailsContainer().hide();
            this.getViewRecordDetailsControls().hide();
        },

        loadFormModule: function(operationType, formData) {

            var module = this.getApp().getModuleManager().initializeModule(this.getFormModuleClassname(), null, false);
                module.setModel(this.getModel());
                module.setEditingRecordData(formData);
                module.setFormMode(operationType);
                module.setModuleProperty('moduleHolder', this.getModuleProperty('container'));

            var selectData = this.getAllSelectdata();
            var fields = Object.keys(selectData);
            var fieldName;

            for(var i = 0, len = fields.length; i < len; i++) {
                
                fieldName = fields[i];

                module.setRelationFieldSelectData(fieldName, selectData[fieldName]);
            }

            this.setFormModule(module);

            this.getApp().getModuleManager().loadModule(this.getFormModule());

            return module;
        },

        getPagingComponentPageParameters: function() {

            var pagingParameters = {};

            pagingParameters.pageNumber = this.getPagingComponent().paginator('option', 'pageNumber');
            pagingParameters.rowsPerPage = this.getPagingComponent().paginator('option', 'rowsPerPage');

            return pagingParameters;
        },

        getPagingParameters: function(pageNumber, rowsPerPage, sortColumnName, sortColumnOrder) {

            var pagingParameters = {};
            var pageParameterProperties = this.getApp().getConfig().getParameter('defaultPagingParameters');

            if(pageParameterProperties.hasOwnProperty(this.getModuleName())) {
                pagingParameters = $.extend(pagingParameters, pageParameterProperties[this.getModuleName()]);
            }
            else {
                pagingParameters = $.extend(pagingParameters, pageParameterProperties['CrudModule']);
            }

            if(pageNumber) {
                pagingParameters.pageNumber = pageNumber;
            }

            if(rowsPerPage) {
                pagingParameters.rowsPerPage = rowsPerPage;
            }

            if(sortColumnName) {
                pagingParameters.sortColumnName = sortColumnName;
            }

            if(sortColumnOrder) {
                pagingParameters.sortColumnOrder = sortColumnOrder;
            }

            if(this.getCurrentSortColumnName()) {
                pagingParameters.sortColumnName = this.getCurrentSortColumnName();
            }

            if(this.getCurrentSortColumnOrder()) {
                pagingParameters.sortColumnOrder = this.getCurrentSortColumnOrder();
            }

            return pagingParameters;
        },

        readRecords: function(pagingParameters, searchParameters) {

            pagingParameters = pagingParameters || this.getPagingParameters();
            searchParameters = searchParameters || this.getSearchParameters();

            var parameters = $.extend({}, pagingParameters, searchParameters);

            var options = {
                scope: this,
                showLoader: false,
                parameters: parameters
            };

            if(searchParameters && searchParameters.quickSearch === true) {
                this.getModel().quickSearch(options);
                this.blockMainViewComponent();
            }
            else {
                this.getModel().read(options);
                this.blockMainViewComponent();
            }
        },

        applyPagedData: function(pagedData) {

            this.insertPagedDataIntoMainViewComponent(pagedData.listData);
            
            this.getPagingComponent().paginator({
                totalPageCount: pagedData.totalPageCount,
                totalRowsCount: pagedData.totalRowsCount,
                pageNumber: pagedData.pageNumber
            });
        },

        emptyMainViewComponent: function() {

            if(this.getMainViewComponent() && this.getMainViewComponent().grid) {
                this.getMainViewComponent().grid('removeAllRows');
            }
        },

        insertDataIntoMainViewComponent: function(data) {

            if(this.isArray(data) && this.getMainViewComponent() && this.getMainViewComponent().grid) {
                for(var i = 0, len = data.length; i < len; i++) {
                    this.getMainViewComponent().grid('addRow', data[i]);
                }
            }
        },

        getMainViewComponentData: function() {

            if(this.getMainViewComponent()) {
                return this.getMainViewComponent().grid('getAllData');
            }

            return null;
        },

        insertPagedDataIntoMainViewComponent: function(data) {
            this.emptyMainViewComponent();
            this.insertDataIntoMainViewComponent(data);
        },

        updateMainViewComponentRecord: function(recordIndex, recordData) {
            if(this.getMainViewComponent() && this.getMainViewComponent().grid) {
                this.getMainViewComponent().grid('updateRow', recordIndex, recordData);
            }  
        },

        blockMainViewComponent: function() {

            var showAnimation = true;
            var mainViewComponentData = this.getMainViewComponentData();

            // if main view component does not have any data empty 'no records found message'
            // will be enough height for animation to be displayed properly
            if(mainViewComponentData && this.isArray(mainViewComponentData)) {
                showAnimation = mainViewComponentData.length === 0 || mainViewComponentData.length > 2;
            }

            this.getMainViewComponent().grid('lockGrid', null, showAnimation);
        },

        unblockMainViewComponent: function() {
            this.getMainViewComponent().grid('unlockGrid');
        },

        parseQuickSearchQuery: function(query) {

            var colonIndex = query.indexOf(':');
            var propertyName = null;
            var searchQuery = null;

            if(colonIndex !== -1) {

                propertyName = query.substring(0, colonIndex);
                searchQuery = query.substring(colonIndex + 1, query.length);
            }
            else {
                searchQuery = query;
            }

            var searchParameters = {
                quickSearchQuery: searchQuery,
                quickSearchPropertyName: propertyName,
                quickSearch: true
            };

            return searchParameters;
        },

        loadRelationFieldData: function(fieldModelData, parameters) {

            var params = parameters || fieldModelData.relationDataLoadParams;

            var options = {
                scope: {
                    fieldModelData: fieldModelData,
                    module: this
                }
            };

            if(params) {
                options['parameters'] = params;
            }

            fieldModelData.relationModel.readForSelect(options);
        },

        patchRecordField: function(recordId, fieldName, patchData, scope) {

            var willItPatch = this.getModel().patch(recordId, patchData, {scope:scope, showLoader: false});
            
            if(willItPatch) {
                this.blockMainViewComponent();
            }
        },

        getPatchRecordScope: function(fieldName, editedFieldValue, editedfieldData) {

            var fieldModelData = this.getModel().getFieldByName(fieldName);

            var scope = {
                module: this,
                editedData: editedfieldData,
                editedValue: editedFieldValue,
                fieldModelData: fieldModelData
            };

            return scope;
        },

        getAutoCompleteGridCellEditOptions: function(grid, fieldModelData, ajaxQueryParams) {

            var autoCompleteOptions = $.extend({}, this.getNamespaceValue('Helpers.Grid.CellEdit.AutoComplete'));

            var scopeOptions = {
                grid: grid,
                fieldModelData: fieldModelData,
                ajaxQueryParams: ajaxQueryParams
            };

            autoCompleteOptions.valueProperty = fieldModelData.valuePropertyName;
            autoCompleteOptions.labelProperty = fieldModelData.labelPropertyName;
            autoCompleteOptions.initializeInputCallback = autoCompleteOptions.initializeInputCallback.bind(scopeOptions);

            return autoCompleteOptions;
        },

        getMultiAutoCompleteGridCellEditOptions: function(grid, fieldModelData, model, ajaxQueryParams) {

            var autoCompleteOptions = $.extend({}, this.getNamespaceValue('Helpers.Grid.CellEdit.MultiAutoComplete'));

            var scopeOptions = {
                grid: grid,
                fieldModelData: fieldModelData,
                ajaxQueryParams: ajaxQueryParams
            };

            autoCompleteOptions.valueProperty = fieldModelData.valuePropertyName;
            autoCompleteOptions.labelProperty = fieldModelData.labelPropertyName;

            autoCompleteOptions.initializeInputCallback = autoCompleteOptions.initializeInputCallback.bind(scopeOptions);
            autoCompleteOptions.checkIfCellDataIsChanged = model.isArrayCollectionChanged.bind(model);

            return autoCompleteOptions;
        },

        getCheckBoxGroupGridCellEditOptions: function(grid, fieldModelData, model) {

            var checkboxGroupOptions = $.extend({}, this.getNamespaceValue('Helpers.Grid.CellEdit.CheckBoxGroup'));
                checkboxGroupOptions['grid'] = grid;
                checkboxGroupOptions['fieldModelData'] = fieldModelData;
                checkboxGroupOptions['availableOptions'] = this.getRelationFieldSelectData(fieldModelData.name);

            checkboxGroupOptions.valueProperty = fieldModelData.valuePropertyName;
            checkboxGroupOptions.labelProperty = fieldModelData.labelPropertyName;

            checkboxGroupOptions.initializeInputCallback = checkboxGroupOptions.initializeInputCallback.bind(checkboxGroupOptions);
            checkboxGroupOptions.checkIfCellDataIsChanged = model.isArrayCollectionChanged.bind(model);

            return checkboxGroupOptions;
        }
    }
});