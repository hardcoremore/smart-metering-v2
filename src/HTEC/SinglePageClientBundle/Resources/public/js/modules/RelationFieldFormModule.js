HCM.define({

    name: 'Modules.RelationFieldFormModule',

    extendFrom: 'Modules.FormModule',

    construct: function(app, permissions) {

        this.callConstructor('Modules.FormModule', [app, permissions]);

        var modelRelationFields;
        var notAutoLoadingRelationFields = [];
        var relationFieldSelectData = {};

        this.setModelRelationFields = function(mrf) {
            modelRelationFields = mrf;
        };

        this.getModelRelationFields = function() {
            return modelRelationFields;
        };

        this.setNotAutoLoadingRelationFields =  function(narf) {
            notAutoLoadingRelationFields = narf;
        };

        this.getNotAutoLoadingRelationFields =  function() {
            return notAutoLoadingRelationFields;
        };

        this.setRelationFieldSelectData = function(fieldName, selectData) {
            relationFieldSelectData[fieldName] = selectData;
        };

        this.getRelationFieldSelectData = function(fieldName) {
            return relationFieldSelectData[fieldName];  
        };
    },

    methods: {

        initModule: function() {

            this.super('initModule');

            this.setModelRelationFields(this.getModel().getRelationFields());

            var relationFields = this.getModelRelationFields();

            for(var i = 0, len = relationFields.length; i < len; i++) {
                relationFields[i].relationModel.getRecordManager().registerForDataMonitor(this.getInstanceId());
            }
        },

        initController: function() {
            var formController = this.getApp().getController('Controllers.RelationFieldForm', [this, this.getModel()]);
            this.setFormController(formController);
        },

        populateRelationFormField: function(fieldModelData, data, relationFormField) {

            var that = this;

            if(this.isArray(data) === false) {
                data = [data];
            }

            $.each(data, function(index) {

                var option = $('<option/>', {
                    text: that.getRelationRecordLabel(fieldModelData, this),
                    value: that.getRelationRecordValue(fieldModelData, this)
                });

                relationFormField.append(option);
            });
        },

        populateFormField: function(fieldName, value, fieldInputElement) {

            var fieldModelData = this.getModel().getFieldByName(fieldName);

            if(fieldModelData && fieldModelData.type === 'relation') {
                
                var isRelationFieldInitialized = this.isRelationFormFieldInitialized(fieldModelData, fieldInputElement);

                if(isRelationFieldInitialized === true) {
                    this.super('populateFormField', [fieldName, value, fieldInputElement]);
                }
            }
            else {
                this.super('populateFormField', [fieldName, value, fieldInputElement]);
            }
        },

        setFormFieldValue: function(fieldName, value, formFieldInput) {

            var fieldModelData = this.getModel().getFieldByName(fieldName);

            if(fieldModelData && fieldModelData.type === 'relation') {

                var isRelationFieldInitialized = this.isRelationFormFieldInitialized(fieldModelData, formFieldInput);

                if(isRelationFieldInitialized) {
                    var selectedValue = this.getRelationRecordValue(fieldModelData, value);
                    formFieldInput.val(selectedValue);
                }
            }
            else {
                this.super('setFormFieldValue', [fieldName, value, formFieldInput]);
            }
        },

        getRelationFieldSelectedData: function(fieldName) {

            var fieldModelData = this.getModel().getFieldByName(fieldName);

            if(fieldModelData && fieldModelData.type === 'relation') {

                var relationModel = fieldModelData.relationModel;
                var fieldInput = this.getFormFieldByName(fieldName);
                var selectedData = null;
            
                selectedData = relationModel.findRecordByPropertyValue(
                    this.getRelationFieldSelectData(fieldName),
                    'id',
                    this.getFormFieldValue(fieldName, fieldInput)
                );

                return selectedData;
            }

            return null;
        },

        getRelationRecordLabel: function(fieldModelData, recordData) {

            if(this.isObject(recordData)) {

                if(fieldModelData.hasOwnProperty('labelPropertyName')) {

                    if(this.isString(fieldModelData.labelPropertyName)) {
                        return recordData[fieldModelData.labelPropertyName];
                    }
                    else if(this.isFunction(fieldModelData.labelPropertyName)) {
                        return fieldModelData.labelPropertyName(recordData);
                    }
                    else {
                        this.throwException('Invalid label property in field model data.', 1500, 'kernel');
                    }
                }
                else if(recordData.hasOwnProperty('label')) {
                    return recordData.label;
                }
                else if(recordData.hasOwnProperty('name')) {
                    return recordData.name;
                }
                else {
                    return '';
                }
            }
            else {
                return recordData;
            }
        },

        getRelationRecordValue: function(fieldModelData, recordData) {

            if(this.isObject(recordData)) {

                if(fieldModelData.valuePropertyName && recordData.hasOwnProperty(fieldModelData.valuePropertyName)) {
                    return recordData[fieldModelData.valuePropertyName];
                }
                else if(recordData.hasOwnProperty('value')) {
                    return recordData.label;
                }
                else if(recordData.hasOwnProperty('id')) {
                    return recordData.id;
                }
                else {
                    return '';
                }
            }
            else {
                return recordData;
            }
        },

        resetRelationFormField: function(fieldModelData, relationFormField) {

            if(relationFormField.is("select")) {
                relationFormField.find('option:not(:first)').remove();
            }

            this.setRelationFormFieldAsUninitialized(fieldModelData, relationFormField);
        },

        isRelationFormFieldInitialized: function(fieldModelData, relationFormField) {
            return relationFormField.data('initialized') === true;
        },

        initializeRelationFormField: function(fieldModelData, data, relationFormField) {
            this.populateRelationFormField(fieldModelData, data, relationFormField);
            this.setRelationFormFieldAsInitialized(fieldModelData, relationFormField);
        },

        setRelationFormFieldAsInitialized: function(fieldModelData, relationFormField) {
            relationFormField.data('initialized', true);
        },

        setRelationFormFieldAsUninitialized: function(fieldModelData, relationFormField) {
            relationFormField.data('initialized', false);
        },

        loadRelationFormFieldData: function(fieldModelData, parameters) {
            fieldModelData.relationModel.readForSelect(this.getRelationFieldLoadOptions(fieldModelData, parameters));
        },

        getRelationFieldLoadOptions: function(fieldModelData, parameters) {

            var params = parameters || fieldModelData.relationDataLoadParams;

            var options = {
                scope: {
                    fieldModelData: fieldModelData,
                    module: this
                }
            };

            if(params) {
                options['parameters'] = params;
            }

            return options;
        },

        getFormFieldByName: function(elementName) {

            var formElement = this.getFormContainer().find(".form-field-widget[data-field-name='"+elementName+"']");

            if(formElement.length > 0) {
                return formElement.first();
            }

            return this.super('getFormFieldByName', [elementName]);
        },

        getFormFieldNameFromFormFieldElement: function(formFieldElement) {

            if(formFieldElement.is('input') || formFieldElement.is('select') || formFieldElement.is('textarea')) {
                return this.super('getFormFieldNameFromFormFieldElement', [formFieldElement]);
            }
            else {
                return formFieldElement.data('field-name');
            }
        },

        getFormData: function(includeEmptyFields) {

            var formData = this.super('getFormData', [includeEmptyFields]);

            var self = this;

            this.getFormContainer().find('.form-field-widget').each(function(index) {

                var fieldInputElement = $(this);
                var fieldName = $(this).data('field-name');

                if(fieldName) {
                    formData[fieldName] = self.getFormFieldValue(fieldName, fieldInputElement);
                }
            });

            return formData;
        },

        resetForm: function() {
            
            var self = this;
            this.getFormContainer().find('.form-field-widget').each(function(index){

                var fieldName = $(this).data('field-name');
                self.resetFormField(fieldName);
            });

            this.super('resetForm');
        },

        getAllFormFields: function() {

            var formFields = this.super('getAllFormFields');
                formFields = formFields.add(this.getFormContainer().find('.form-field-widget'));

            return formFields;
        },

        updateFieldFromMonitorData: function(fieldModelData) {

            var fieldRecordManager = fieldModelData.relationModel.getRecordManager();
            var fieldMonitorData = fieldRecordManager.getMonitorData(this.getInstanceId());
            var fieldSelectData = this.getRelationFieldSelectData(fieldModelData.name);

            if(fieldMonitorData) {

                var updatedSelectData = fieldRecordManager.getUpdatedFormFieldData(fieldMonitorData, fieldModelData, fieldSelectData);

                inputField = this.getFormFieldByName(fieldModelData.name);

                this.setRelationFieldSelectData(fieldModelData.name, fieldSelectData);
                this.resetRelationFormField(fieldModelData, inputField);
                this.initializeRelationFormField(fieldModelData, updatedSelectData, inputField);

                fieldRecordManager.removeMonitorData(this.getInstanceId());
            }
        }
    }
});