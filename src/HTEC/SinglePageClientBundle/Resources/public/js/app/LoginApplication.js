HCM.define({

    name: 'App.LoginApplication',

    extendFrom: 'Base.Application',

    construct: function(c) {

        this.callConstructor('Base.Application', [c]);

        var self = this;

        var loginModuleHolder;
        var appContentHolder;

        var userModel;
        var authenticationController;
        var loaderNotifications = {};

        this.setLoginModuleHolder = function(holder) {
            loginModuleHolder = holder;
        };

        this.getLoginModuleHolder = function() {
            return loginModuleHolder;
        };

        this.setAppContentHolder = function(cel) {
            appContentHolder = cel;
        };

        this.getAppContentHolder = function() {
            return appContentHolder;
        };

        this.addLoaderNotification = function(loaderId, notificationObject) {
            loaderNotifications[loaderId] = notificationObject;
        };

        this.removeLoaderNotification = function(loaderId) {
            delete loaderNotifications[loaderId];
        };

        this.setAuthenticationController = function(lc) {
            authenticationController = lc;
        };

        this.getAuthenticationController = function() {
            return authenticationController;
        };

        this.setUserModel = function(um) {
            userModel = um;
        };

        this.getUserModel = function() {
            return userModel;
        };

        this.loadCurrentUserCompleteHandler = function(ev) {
            self.enterApplication();
        };

        this.loadCurrentUserErrorHandler = function(ev) {

            if(ev.error.code === 403) {
                self.getModuleManager().openPageModule('Modules.Login');
            }
            else {
                console.log('Failed to load user from session. Code: ' + ev.error.code);
            }
        };

        this.logoutCompleteEventHandler = function() {

            setTimeout(function(){
                self.showLoader();
                window.location.href = self.getConfig().getBaseUrl();    
            }, 150);
        };

        this.logoutErrorEventHandler = function() {

            setTimeout(function(){
                self.showLoader();
                window.location.href = self.getConfig().getBaseUrl();    
            }, 150);
        };

    },

    methods: {

        /**
        *
        * Initialize the application
        *
        */
        init: function() {

            this.super('init');

            $(document).on(this.getUserModel().events.LOAD_CURRENT_USER_COMPLETE, this.loadCurrentUserCompleteHandler);
            $(document).on(this.getUserModel().events.LOAD_CURRENT_USER_ERROR, this.loadCurrentUserErrorHandler);

            $(document).on(this.getUserModel().events.LOGOUT_COMPLETE, this.logoutCompleteEventHandler);
            $(document).on(this.getUserModel().events.LOGOUT_ERROR, this.logoutErrorEventHandler);
        },

        startApp: function() {

            this.super('startApp');

            // don't allow loading page modules through url history change before user is logged in
            this.stopController(this.getHistoryController());

            this.getAppContentHolder().hide();

            // try to load currently logged in user
            this.loadApplicationUser();
        },

        // call this function after user identity has been confirmed
        // either through successfull login or when current user is loaded
        enterApplication: function() {
            this.showApp();
            this.startEntryModule();
            this.startController(this.getHistoryController());
        },

        loadApplicationUser: function() {
            this.getUserModel().loadCurrentUser({scope:this});
        },

        startEntryModule: function() {

            var entryUrlData = this.getUrlData();
            var isValidEntryData = entryUrlData && entryUrlData.moduleName && entryUrlData.moduleName.length > 0;
            var entryModulePermissions = this.getModulePermissions(entryUrlData.moduleName);
            var isEntryModuleNotLogout = entryUrlData.moduleName !== 'Login';
            var defaultModulePermissions = this.getModulePermissions(this.getConfig().getParameter('defaultModule'));

            if(isValidEntryData && isEntryModuleNotLogout && entryModulePermissions) {
                this.loadModuleFromUrl(entryUrlData);
            }
            else if(defaultModulePermissions) {
                this.loadDefaultModule();
            }
            else {

                // start first module from permissions
                var userPermissions = this.getUserModel().getCurrentUser().permissions;
                    permissionKeys = Object.keys(userPermissions);

                var pageModulesNamespace = this.getConfig().getParameter('pageModulesNamespace');
                this.getModuleManager().openPageModule(pageModulesNamespace + '.' + permissionKeys[0]);
            }
        },

        showApp: function() {
            this.getLoginModuleHolder().hide();
            this.getAppContentHolder().show();
        },

        /**
         *
         * Get module permissions
         *
         * @param {String} moduleClassName
         *
         * @return {Array} Returns array with actions that module is allowed to
         *
         */
        getModulePermissions: function(moduleClassName) {

            if(!this.getUserModel().getCurrentUser()) {
                return null;
            }

            var permissions = this.getUserModel().getCurrentUser().permissions;

            if(permissions && permissions.hasOwnProperty(moduleClassName)) {
                return permissions[moduleClassName];
            }

            return null;
        }
    }
});