HCM.createNamespace('Services.Base.Application', {

    'single_page_client.config': {
        class: 'Base.Config'
    },

    'single_page_client.requester.rest': {
        class: 'Base.RESTRequest'
    },

    'single_page_client.model_record_manager': {
        class: 'Base.ModelRecordManager',
        singleton: false
    },

    'single_page_client.model_validator': {
        class: 'Base.Validator',
        singleton: false,
        arguments: ['$errorMessages']
    }
});