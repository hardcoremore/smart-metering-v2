HCM.createNamespace('Global.Parameters', {

    env: 'prod',

    appName: 'Base Application',
    pageModulesNamespace: 'Modules',

    errorNotificationDuration: 15000, // false is infinite
    successNotificationDuration: 5000, // 5 seconds
    warningNotificationDuration: 10000, // 10 seconds
    informationNotificationDuration: 5000, // 5 seconds

    fileUploadProgressInterval: 800, // 0.8 second

    pageParameterProperties: {
        rowsPerPage:  "rowsPerPage" ,
        pageNumber:  "pageNumber",
        sortColumnName:  "sortColumnName",
        sortColumnOrder: "sortColumnOrder"
    },

    defaultPagingParameters: {
        CrudModule: {
            rowsPerPage: 10,
            sortColumnName: 'id',
            sortColumnOrder: 'desc'
        }
    },

    monthNames: ['Јануар', 'Фебруар', 'Март', 'Април', 'Мај', 'Јун', 'Јул', 'Август', 'Септембар', 'Октобар', 'Новембар', 'Децембар'],

    errorCodes: {
        invalidResponseFormat: 1400
    },

    errorMessages: {

        invalidResponseFormatMsg: "Read Error occured. Invalid response format. Please contact support.",

        /****** VALIDATION ********/
        fieldIsRequired: "This field is required.",
        fieldInvalidFormat: "Value is not in valid format.",
        fieldLengthError: "Maximum or minimun characters exceeded.",
        fieldRangeError: "Value is greater or lower than allowed."
    }

});
