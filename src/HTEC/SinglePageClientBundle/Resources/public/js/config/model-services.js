HCM.createNamespace('Services.Base.Models', {

    'Base.Model': {
        calls: [
            { method: 'setConfig', arguments: ['@single_page_client.config'] },
            { method: 'setRequester', arguments: ['@single_page_client.requester.rest'] },
            { method: 'setValidator', arguments: ['@single_page_client.model_validator'] },
            { method: 'setRecordManager', arguments: ['@single_page_client.model_record_manager'] }
        ]
    }
});