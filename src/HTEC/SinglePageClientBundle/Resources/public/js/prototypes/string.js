String.prototype.firstCharacterToUpperCase = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
};
  
String.prototype.lastCharacterToUpperCase =  function() {
    return this.charAt(this.length - 1).toUpperCase() + this.slice(0, this.length - 1);
};

String.prototype.firstCharacterToLowerCase = function() {
    return this.charAt(0).toLowerCase() + this.slice(1);
};
  
String.prototype.lastCharacterToLowerCase =  function() {
    return this.charAt(this.length - 1).toLowerCase() + this.slice(0, this.length - 1);
};