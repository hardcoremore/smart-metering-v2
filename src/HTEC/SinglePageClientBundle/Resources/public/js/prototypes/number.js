Number.prototype.formatNumberThousands = function(tausandSeparator, decimalSeparator) {

    var stringNumber = this.toString();

    var numberParts = stringNumber.split(decimalSeparator || '.');

    var pattern = /\B(?=(\d{3})+(?!\d))/g;
    var formattedNumber;

    if(numberParts.length === 2) {
        formattedNumber = numberParts[0].replace(pattern, tausandSeparator || ",");
        formattedNumber += (decimalSeparator || '.') + numberParts[1];
    }
    else {
        formattedNumber = stringNumber.replace(pattern, tausandSeparator || ",");   
    }

    return formattedNumber;
};

Number.prototype.addLeadingZero = function() {

    var numberString = this.toString();

    if(numberString.length === 1) {
        return "0" + numberString;
    }
    else {
        return numberString;
    }
};