HCM.createNamespace('Helpers.Grid.CellEdit.AutoComplete', {

    type: 'text',

    initializeInputCallback: function(inputElement) {

        var autoCompleteWidget = inputElement.autocomplete({
            queryParamName: "q",
            valueParam: this.fieldModelData.valuePropertyName,
            labelParam: this.fieldModelData.labelPropertyName,
            source: this.fieldModelData.relationModel.getUrl('/list-auto-complete'),
            ajaxQueryParams: this.ajaxQueryParams
        });

        return autoCompleteWidget;
    },

    getInputData: function(initializedInput) {
        return initializedInput.autocomplete('getSelectedOriginalItem');
    },

    setInputData: function(initializedInput, cellData, rowData) {
        initializedInput.autocomplete('setSelectedOriginalItem', cellData);
    },

    destroyWidget: function(initializedInput) {
        initializedInput.autocomplete('destroy');
    }
});