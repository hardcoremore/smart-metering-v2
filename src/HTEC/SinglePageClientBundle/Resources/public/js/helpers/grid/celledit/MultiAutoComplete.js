HCM.createNamespace('Helpers.Grid.CellEdit.MultiAutoComplete', {

    type: 'widget',

    initializeInputCallback: function(inputElement) {

        var self = this;

        var multiAutoCompleteWidget = inputElement.multiAutocomplete({

            isPopup: true,
            allowNew: true,
            inputPlaceHolder: 'Search...',
            class: "multi-autocomplete-widget-grid",

            autoCompleteOptions: {
                source: this.fieldModelData.relationModel.getUrl('/list-auto-complete'),
                queryParamName: 'q',
                labelParam: this.labelPropertyName,
                valueParam: this.valuePropertyName,
                ajaxQueryParams: this.ajaxQueryParams
            },

            onCancel: function(ev, ui) {
                self.grid.grid('cancelCellEdit');
            },

            onSave: function(ev, ui) {
                self.grid.grid('finishCellEdit');
            }
        });

        multiAutoCompleteWidget = multiAutoCompleteWidget.multiAutocomplete('getRootElement');

        return multiAutoCompleteWidget;
    },

    getInputData: function(initializedInput) {
        return initializedInput.multiAutocomplete('getSelectedItems');
    },

    setInputData: function(initializedInput, cellData, rowData) {
        initializedInput.multiAutocomplete('option', 'data', cellData);
    },

    closeWidget: function(initializedInput) {
        initializedInput.multiAutocomplete('close');
    },

    openWidget: function(initializedInput) {
        initializedInput.multiAutocomplete('open');
    },

    destroyWidget: function(initializedInput) {
        initializedInput.multiAutocomplete('destroy');
    }
});