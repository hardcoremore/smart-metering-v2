HCM.createNamespace('Helpers.Grid.CellEdit.CheckBoxGroup', {

    type: 'widget',

    initializeInputCallback: function(inputElement) {

        var self = this;

        var checkBoxInputWidget = inputElement.checkboxgroup({

            class: "checkboxgroup-widget-grid",
            isPopup: true,
            data: this.availableOptions,

            onCancel: function(ev, ui) {
                self.grid.grid('cancelCellEdit');
            },

            onSave: function(ev, ui) {
                self.grid.grid('finishCellEdit');
            }
        });

        checkBoxInputWidget = checkBoxInputWidget.checkboxgroup('getRootElement');

        return checkBoxInputWidget;
    },

    getInputData: function(initializedInput) {
        return initializedInput.checkboxgroup('getSelectedItems');
    },

    setInputData: function(initializedInput, cellData, rowData) {
        initializedInput.checkboxgroup('setSelectedItems', cellData);
    },

    closeWidget: function(initializedInput) {
        initializedInput.checkboxgroup('close');
    },

    openWidget: function(initializedInput) {
        initializedInput.checkboxgroup('open');
    },

    destroyWidget: function(initializedInput) {
        initializedInput.checkboxgroup('destroy');
    }
});