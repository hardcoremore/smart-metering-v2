HCM.createNamespace('Helpers.DateTime',  {

    isValidDateString: function(dateString) {

        var d = new Date(dateString);

        return isNaN(d.getDate()) === false;
    },

    formatDateTimeToString: function(dateTime, showDate, showTime, removeTimezone) {

        var stringHelper = SM.helpers.String;

        var d = null;

        if(!dateTime) {
            return null;
        }

        if(typeof dateTime === 'string') {

            if(removeTimezone === true) {

                var tzPos = dateTime.indexOf("+");

                if(tzPos !== -1) {
                    dateTime = dateTime.substring(0, tzPos);
                }

                dateTime += "+0000"; // DONT USE TIMEZONE
            }

            if(dateTime.indexOf("T") === -1) {
                dateTime = dateTime.replace(" ", "T");
            }
        }

        if(SM.base.Class.isDate(dateTime) && dateTime.getTime() > 0) {

            d = new Date(dateTime.getTime());
        }
        else if(typeof dateTime === "string") {
            d = new Date(dateTime);
        }
        else {
            return '';
        }

        if(!d || isNaN(d.getTime())) {
            return '';
        }

        var month = stringHelper.addLeadingZero(d.getMonth() + 1);
        var date = stringHelper.addLeadingZero(d.getDate());

        var hours = stringHelper.addLeadingZero(d.getUTCHours());
        var minutes = stringHelper.addLeadingZero(d.getUTCMinutes());
        //var seconds = this.addLeadingZeroIfMissing(d.getSeconds());
        
        var dateString = "";

        if(showDate) {
            dateString = d.getUTCFullYear() + "-" + month + "-" + date + " ";
        }

        if(showTime) {
            dateString += hours + ":" + minutes;
        }
        else {
            dateString = dateString.trim();
        }

        return dateString;
    },

    removeTimeZoneFromString: function(dateTimeString) {
        
        var plusIndex = dateTimeString.indexOf('+');

        if(plusIndex !== -1) {
            dateTimeString = dateTimeString.substring(0, plusIndex);
            return dateTimeString;
        }

        return dateTimeString;
    },

    getWeekDayNameFromDate: function(date) {

        var weekDayName = '';

        var dayOfWeek = '';

        if(SM.base.Class.isDate(date) && date.getTime() > 0) {

            dayOfWeek = date.getDay();
        }
        else {

            var d = new Date(this.removeTimeZoneFromString(date));
                dayOfWeek = d.getDay();
        }

        if(isNaN(dayOfWeek)) {
            return '';
        }

        switch(dayOfWeek) {
            case 0:
                dayOfWeek = 'sunday';
            break;

            case 1:
                dayOfWeek = 'monday';
            break;

            case 2:
                dayOfWeek = 'tuesday';
            break;

            case 3:
                dayOfWeek = 'wednesday';
            break;

            case 4:
                dayOfWeek = 'thursday';
            break;

            case 5:
                dayOfWeek = 'friday';
            break;

            case 6:
                dayOfWeek = 'saturday';
            break;
        }

        return dayOfWeek;
    }
});
