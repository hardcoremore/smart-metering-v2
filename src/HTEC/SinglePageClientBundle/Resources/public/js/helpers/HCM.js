var HCM = (function(){

    var namespaceHolder = {};
    var classDefinitions = {};

    var fn = function() {

        var instanceId = this.namespace + '-' + Math.round(Math.random() * 1000000000);

        var superMethodParentPrototype = {};
        var superParentNamespace = {};

        this.getInstanceId = function() {
            return instanceId;
        }

        this.setNextSuperMethodParentPrototype = function(methodName, proto) {

            superMethodParentPrototype[methodName] = proto;

            if(proto) {
                this.setNextSuperMethodParentPrototypeNamespace(methodName, proto.getNamespace());
            }
            else {
                this.setNextSuperMethodParentPrototypeNamespace(methodName, null);
            }
        }

        this.getNextSuperMethodParentPrototype = function(methodName) {
            return superMethodParentPrototype[methodName];
        }

        this.setNextSuperMethodParentPrototypeNamespace = function(methodName, namespace) {
            superParentNamespace[methodName] = namespace;
        };

        this.getNextSuperMethodParentPrototypeNamespace = function(methodName) {
            return superParentNamespace[methodName];
        };
    };

    var basePrototypeMethods = {

        namespace: '',

        findClosestScopeThatHasMethod: function(methodName, scope) {

            var scopeIterator = null;

            while(scope && scope.hasOwnProperty('$parent')) {

                scope = scope.$parent.prototype;

                if(scope.hasOwnProperty(methodName)) {
                    scopeIterator = scope;
                    break;
                }
            }

            return scopeIterator;
        },

        super: function(methodName, arguments) {

            var baseProto = this.getNextSuperMethodParentPrototype(methodName) || this.constructor.prototype;

            var thisClassDefinition = this.getClassDefinition(baseProto.getNamespace());
            var thisClassHasMethods = thisClassDefinition.hasOwnProperty('methods');

            // if current class does not have super method
            // set baseProto to first class that has a method
            if(thisClassHasMethods === false || 
               (thisClassHasMethods === true && thisClassDefinition.methods.hasOwnProperty(methodName) === false)) {
                baseProto = this.findClosestScopeThatHasMethod(methodName, baseProto);                       
            }   

            // find next occurance of the method
            var closestScope = this.findClosestScopeThatHasMethod(methodName, baseProto);

            if(closestScope !== null) {
                this.setNextSuperMethodParentPrototype(methodName, closestScope);
            }
            else{
                this.setNextSuperMethodParentPrototype(methodName, null);
            }

            if(closestScope !== null) {

                var returnValue = closestScope[methodName].apply(this, arguments);

                if(closestScope.getNamespace() === this.getNextSuperMethodParentPrototypeNamespace(methodName)) {
                    this.setNextSuperMethodParentPrototype(methodName, null);
                }

                return returnValue;
            }
        },

        getCurrentClassDefinition: function() {
            return this.getClassDefinition(this.getNamespace());
        },

        setClassDefinition: function(className, classData) {
            classDefinitions[className] = classData;
        },

        getClassDefinition: function(className) {
            return classDefinitions[className];
        },

        setNamespace: function(ns) {
            this.namespace = ns;
        },

        getNamespace: function() {
            return this.namespace;
        },

        createNamespace: function (namespaceName, namespaceValue, namespaceObject) {

            var namespaceParts = namespaceName.split('.');

            var namespace = namespaceObject || namespaceHolder;

            for (var i = 0, len = namespaceParts.length, part; i < len; i += 1) {

                part = namespaceParts[i];

                // create a property if it doesn't exist
                if (typeof namespace[part] === "undefined") {

                    if(i === len-1) {
                        namespace[part] = namespaceValue;
                    }
                    else {
                        namespace[part] = {};
                        namespace = namespace[part];
                    }
                }
                else {
                    namespace = namespace[part];
                }
            }
        },

        getNamespaceValue: function(namespaceName, namespaceObject) {

            var namespaceParts = namespaceName.split('.');
            var namespace = namespaceObject || namespaceHolder;

            for (var i = 0, len = namespaceParts.length, part; i < len; i += 1) {

                part = namespaceParts[i];

                if(namespace[part]) {
                    namespace = namespace[part];
                }
                else {
                    console.log("Namespace part '" + part + "' does not exists for namespace: " + namespaceName);
                    this.throwException("Namespace part '" + part + "' does not exists", 1500, 'kernel');
                }
            }

            return namespace;
        },

        define: function(classData) {

            if(this.isString(classData.name) !== true || classData.name.length < 1) {
                this.throwException('Class must have a name.', 1500, 'kernel');
            }
            else if(this.isFunction(classData.construct) !== true) {
                this.throwException('Class must have a constructor definition.', 1500, 'kernel');
            }

            // set class'es constructor as namespace value
            this.createNamespace(classData.name, classData.construct);

            classData.configured = false;

            this.setClassDefinition(classData.name, classData);
        },

        configure: function(className) {

            var classData = this.getClassDefinition(className);

            this.extendClassPrototype(classData.construct, fn);

            if(classData.extendFrom) {
                this.extendClassPrototype(classData.construct, this.getClassConstructor(classData.extendFrom));
            }

            if(classData.methods && this.isObject(classData.methods)) {
                this.extendClassPrototypeWithObject(classData.construct, classData.methods);
            }

            if(classData.extendPrototypeWith && this.isObject(classData.extendPrototypeWith)) {
                this.extendClassPrototypeWithObject(classData.construct, classData.extendPrototypeWith);
            }

            classData.construct.prototype.setNamespace(classData.name);

            classData.configured = true;

            this.setClassDefinition(classData.name, classData);

            return classData;
        },

        getClassConstructor: function(className) {

            try {

                var classData = this.getClassDefinition(className);

                if(classData.configured === false) {
                    classData = this.configure(className);
                }

                // namespace value of class name is class'es constructor
                return this.getNamespaceValue(className);
            }
            catch(error) {

                console.log('Failed to return constructor of class: ' + className);

                if(error.hasOwnProperty('getMessage')) {
                    console.log(error.getMessage());
                }
                else {
                    console.log(error);
                }
            }
        },

        getInstance: function(className, instanceArguments) {

            var classConstructor = this.getClassConstructor(className);
            
            // set classConstructor definition function as first argument
            if(this.isArray(instanceArguments) && instanceArguments.length > 0) {
                instanceArguments.splice(0, 0, classConstructor);
            }

            try {
                var instance = new (Function.prototype.bind.apply(classConstructor, instanceArguments));
                fn.call(instance);
                return instance;
            }
            catch(error) {
                this.throwException("Instance of class '" + className + "' could not be created.", 1500, 'kernel');
            }
        },

        callConstructor: function(parentClass, classArguments) {

            try {

                var parentClassConstructor;

                if(this.isString(parentClass)) {
                    parentClassConstructor = this.getClassConstructor(parentClass);
                }
                else if(this.isFunction(parentClass)) {
                    parentClassConstructor = parentClass;
                }

                var constructorReturnValue =  parentClassConstructor.apply(this, classArguments);
                var parentClassDefinition = this.getClassDefinition(parentClass);

                if(!parentClassDefinition.extendFrom) {
                    fn.apply(this);
                }

                return constructorReturnValue;
            }
            catch(error) {
                console.log("Tried to extend from parent class: " + parentClass);
                console.log(error);
            }
        },

        callClassMethod: function(className, methodName, classArguments) {

            var classData = this.getClassDefinition(className);

            if(this.isObject(classData) && classData.hasOwnProperty('definition')) {

                if(classData.construct.prototype.hasOwnProperty(methodName)) {
                    return classData.construct.prototype[methodName].apply(this, classArguments);    
                }
                else {
                    this.throwException('Class method: "' + methodName + '" of class: "'+className+'" does not exists.', 1500, 'kernel');    
                }
            }
            else {
                this.throwException('Class: "'+className+'" does not exists.', 1500, 'kernel');
            }
        },

        throwException: function(message, code, type) {
            throw this.getInstance('Base.Exception', [message, code]);
        },

        extendClassPrototype: function(newClass, baseClass) {
            if(this.isFunction(newClass) && this.isFunction(baseClass)) {
                newClass.prototype = Object.create(baseClass.prototype);
                newClass.prototype.$parent = baseClass;
                newClass.prototype.constructor = newClass;
            }
            else {
                this.throwException('Invalid new or base class definition at CLASS::extendClassPrototype', 1500, 'kernel');
            }
        },

        extendClassPrototypeWithObject: function(classFunc, prototypeObject) {

            if(this.isFunction(classFunc) && this.isObject(prototypeObject)) {
                for(var p in prototypeObject) {
                    classFunc.prototype[p] = prototypeObject[p];
                }
            }
            else {
                this.throwException('Invalid class definition or prototypeObject at CLASS::extendClassPrototypeWithObject', 1500, 'kernel');
            }
        },

        isArray: function(arrayVar) {
            return Object.prototype.toString.call(arrayVar) === '[object Array]';
        },

        isDate: function(date) {
            return Object.prototype.toString.call(date) === '[object Date]';
        },

        isString: function(string) {
            return Object.prototype.toString.call(string) === '[object String]';
        },

        isObject: function(object) {
            return Object.prototype.toString.call(object) === '[object Object]';
        },

        isFunction: function(functionDefintion) {
            return Object.prototype.toString.call(functionDefintion) === '[object Function]';
        },

        sortByObjectPropertyASC: function(dataArray, propertyName) {

            var compareFunctionASC = function compare(a,b) {

                if (a[propertyName] < b[propertyName]) {
                    return -1;
                }
                
                if (a[propertyName] > b[propertyName])
                {
                    return 1;
                }
                    
                return 0;
            };

            dataArray.sort(compareFunctionASC);
        },

        sortByObjectPropertyDESC: function(dataArray, propertyName) {

            var compareFunctionDESC = function compare(a,b) {

                if (a[propertyName] > b[propertyName]) {
                    return -1;
                }
                
                if (a[propertyName] < b[propertyName])
                {
                    return 1;
                }
                    
                return 0;
            };

            dataArray.sort(compareFunctionDESC);
        },

        log: function() {

            if(window.console) {

                var logItem;
                var logText = this.getNowTimeString();

                for(var i = 0, len = arguments.length; i < len; i++) {

                    logItem = arguments[i];

                    if(this.isArray(logItem)) {
                        logText += ' [' + logItem.join(',') + ']';
                    }
                    else if(this.isObject(logItem)) {
                        logText += ' ' + JSON.stringify(logItem);
                    }
                    else {
                        logText +=  ' ' + logItem.toString();
                    }
                }

                console.log(logText);
            }
        },

        getNowTimeString: function() {

            var now = new Date();
            var hour = "0" + now.getHours();
            var minute = "0" + now.getMinutes();
            var second = "0" + now.getSeconds();

            hour = hour.substring(hour.length-2);
            minute = minute.substring(minute.length-2);
            second = second.substring(second.length-2);

            return hour + ":" + minute + ":" + second;
        }
    };

    fn.prototype = basePrototypeMethods;

    return basePrototypeMethods;

}());
