HCM.createNamespace('Events.Model.Role', {

    VALIDATION_ERROR: 'Events.Model.Role.validationError',

    CREATE_COMPLETE: 'Events.Model.Role.createComplete',
    CREATE_ERROR: 'Events.Model.Role.createError',

    READ_COMPLETE: 'Events.Model.Role.readComplete',
    READ_ERROR: 'Events.Model.Role.readError',

    READ_FOR_SELECT_COMPLETE: 'Events.Model.Role.readForSelectComplete',
    READ_FOR_SELECT_ERROR: 'Events.Model.Role.readForSelectError',

    READ_FOR_AUTOCOMPLETE_COMPLETE: 'Events.Model.Role.readForAutocompleteComplete',
    READ_FOR_AUTOCOMPLETE_ERROR: 'Events.Model.Role.readForAutocompleteError',

    UPDATE_COMPLETE: 'Events.Model.Role.updateComplete',
    UPDATE_ERROR: 'Events.Model.Role.updateError',

    PATCH_COMPLETE: 'Events.Model.Role.patchComplete',
    PATCH_ERROR: 'Events.Model.Role.patchError',

    DELETE_COMPLETE: 'Events.Model.Role.deleteComplete',
    DELETE_ERROR: 'Events.Model.Role.deleteError'

});