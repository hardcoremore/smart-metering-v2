HCM.createNamespace('Events.Model.Permission', {

    READ_ALL_COMPLETE: 'Events.Model.Permission.readAllComplete',
    READ_ALL_ERROR: 'Events.Model.Permission.readAllError'

});