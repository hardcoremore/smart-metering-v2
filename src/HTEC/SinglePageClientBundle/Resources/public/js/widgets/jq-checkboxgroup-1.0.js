/*
 * jQuery Checkboxgroup
 * Display simple list of checkboxes as a group (with the same name)
 *
 * @version v1.0 (6 Oct 2014)
 *
 * Copyright 2014, High Tech Engineering Center (HTEC).
 *
 * Homepage:
 * http://www.htec.rs
 *
 * Authors:
 *  Caslav Sabani
 *
 * Dependencies:
 *   jQuery v1.6+
 *    jQuery UI v1.8+
 */
;(function($) {

    $.widget( "ui.checkboxgroup", $.ui.popupWidget, {

        //default options
        options: {
            labelPropertyName: 'name',
            valuePropertyName: 'id',
            inputName: 'name',
            data: []
        },

        _itemsHolder: null,

        // the constructor
        _create: function() {

            if(this.element.is('input') || this.element.is('textarea') || this.element.is('select')) {

                var holder = $('<div />');
                var elementId = this.element.attr('id');

                holder.data(this.element.data());
                this.element.replaceWith(holder);
                this.element = holder;
                this.element.attr('id', elementId);
            }

            this.element.addClass('jq-checkbox-group');
            this.element.addClass(this.options.class);

            this._itemsHolder = $('<div/>', {
                'class': 'checkboxgroup-items-holder'
            });

            this.element.append(this._itemsHolder);

            if(Array.isArray(this.options.data) && this.options.data.length > 0) {
                
                var dataLen = this.options.data.length;
                var dataItem = null;

                for(var i = 0; i < dataLen; i++) {

                    dataItem = this.options.data[i];
                    this._renderItem(dataItem[this.options.valuePropertyName], dataItem);
                }
            }
            else {
                this.options.data = [];
            }

            this._createPopup('#checkbox-popup-holder');
        },

        _renderItem: function(id, data) {

            var checkbox = $('<input/>', {
                type: 'checkbox',
                value: data[this.options.valuePropertyName],
                'data-id': id,
                class: 'jq-checkboxgroup-item-' + id,
                checked: data.checked,
                id: 'checkbox-widget-' + this.options.inputName + '-item-' + id
            });

            var label = '';

            if(typeof this.options.labelPropertyName === 'string') {
                label = data[this.options.labelPropertyName];
            }
            else if(typeof this.options.labelPropertyName === 'function') {
                label = this.options.labelPropertyName(data);
            }
            else {
                label = data.toString();
            }

            var label = $('<label>', {
                text: label,
                for: 'checkbox-widget-' + this.options.inputName + '-item-' + id
            });

            var checkboxContainer = $('<div/>', {
                class: 'checkboxgroup-item'
            });

            checkboxContainer.append([checkbox, label]);
            this._itemsHolder.append(checkboxContainer);
        },

        _setOption: function(key, value, triggerEvent) {

            var that = this;
            this._super(key, value);

            if (key === 'data') {

                this.removeAllOptions();
                this.options.data = [];

                var dataItem;
                var dataLen = value.length;

                for(var i = 0; i < dataLen; i++) {

                    dataItem = value[i];

                    this.options.data.push(dataItem);
                    that._renderItem(dataItem[this.options.valuePropertyName], dataItem);
                }
            }
        },

        getRootElement: function() {
            return this.element;
        },

        removeAllOptions: function () {
            this.options.data = [];
            this._itemsHolder.empty();
        },

        // method for popup widget event
        getSelectedData: function() {
            return this.getSelectedItems();
        },

        setSelectedItems: function (selectedItems) {

            var item;

            for(var i = 0, len = selectedItems.length; i < len; i++) {
                item = selectedItems[i];
                this._itemsHolder.find('.jq-checkboxgroup-item-' + item[this.options.valuePropertyName]).prop("checked", true);
            }
        },

        setSelectedValues: function (selectedValues) {
            for(var i = 0, len = selectedValues.length; i < len; i++) {
                this._itemsHolder.find('.jq-checkboxgroup-item-' + selectedValues[i]).prop("checked", true);
            }
        },

        getSelectedItems: function() {

            var items = [];
            var self = this;

            this._itemsHolder.find('input:checked').each(function(){
                var itemId = $(this).data('id');
                items.push(self.getItemById(itemId));
            });

            return items;
        },

        getSelectedValues: function() {

            var values = [];
            var self = this;

            this._itemsHolder.find('input:checked').each(function(){
                values.push($(this).val());
            });

            return values;
        },

        getItemById: function(id) {

            var itemData = null;

            for(var i = 0, len = this.options.data.length; i < len; i++) {

                itemData = this.options.data[i];

                if(itemData[this.options.valuePropertyName].toString() === id.toString()) {
                    break;
                }
            }

            return itemData;
        },

        uncheckAll: function() {
            this._itemsHolder.find('input:checked').prop("checked", false);
        },

        resetAll: function() {
            this._itemsHolder.find('input').removeAttr('checked');
        }
    });

})(jQuery);