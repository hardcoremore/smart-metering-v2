/*
* jQuery Grid
*
* @version v1.0 (3 July 2015)
* @version v2.0 (May 14 2016)
* 
* Copyright 2015, HTEC.
*
*
* Homepage:
* http://www.htec.com
*
*
* Authors:
*   Caslav Sabani
*
*
* Dependencies:
*   jQuery v1.6+
*    jQuery UI core v1.8+
*/
;(function($) {

    $.widget('ui.grid', {
        options: {
            lockMessage: 'Loading...',
            autoLoad: true,
            multiselect: false,
            localSort: false,
            cellEdit: false,
            cellEditType: 'local', // how to save cell data. can be local, remote or callback
            selectedRows: [],
            model: [],

            defaultCellEditErrorMessage: 'This value is invalid.',
            lockGridTemplate: null, // function returns the template for content of lock grid div

            /***
            model:[
                {
                    header: 'Monday',
                    id: 'id', // this is requiered field
                    dataMap: 'id',
                    formatter: function(cellValue, rowData, rowIndex){},
                    hidden: true,
                    sortable: true,
                    sortName: 'id',
                    sortPropertyName: 'name',
                    editable: false,
                    editOptions: { 
                        type: 'text', // e.g. 'text', textarea, select, 'checkbox', 'widget'
                        availableOptions: [{value:1, label:'Options 1'}] // data to select from,
                        areAvailableOptionsStatic: true, // if available options will not change
                        valueProperty: 'id',
                        labelProperty: 'name',
                        validateCallback: function(editedValue, columnName, rowData) { return true} // function to valdiate value
                        validateRegex: '' // regular expression to validate value. If validateCallback is defined it takes presedence over validateRegex
                        validationErrorMessage: 'This value is not valid' // message to be displayed if value is not valid
                        initializeInputCallback: function(inputElement) { return initializedInput} // function to create widget or other components for cell editing
                        closeWidget: function(initializedInput){} close the widget
                        openWidget: function(initializedInput){} opens the widget
                        destroyWidget: function(initializedInput){} destroys widget after cell is edited or if cell editing is cancelled
                        getInputData: function(initializedInput) {} Get input data from cell edit input 
                        setInputData: function(initializedInput or regular input, cellData, rowData) {} Set value to cell edit input
                        checkIfCellDataIsChanged: function(cellName, oldCellData, editedCellData, rowData) // check if data has been changed while cell was edited
                    }
                }
            ],
            ***/

            url: '',

            ajaxOptions: {
                methodType: 'GET',
                parameters: null,
                getUrlCallback: null,
                dataListPropertyName: 'data'
            },

            cellEditAjaxOptions: {

                url: '',
                method: 'PATCH',
                dataType: 'text',

                // function (cellName, cellEditedValue, rowData)
                // if url is not the same for all cell updates
                // return url in callback
                getUrlCallback: null,

                // function (response, cellName, cellEditedValue) {} Get error message from response when error occurs after cell edit
                parseErrorMessageCallback: null,

                // function (cellName, cellEditedValue, rowData). format data sent to server when saving cell data
                formatDataCallback: null,
            },

            cellEditCallbackOptions: {

                callback: null, // function, callback to call to save cell data when cell is edited

                // callbackArgs array. Arguments to pass to callback function if arguments are static
                // note that cellName, cellEditedValue and rowData will always be first three arguments and
                // arguments defined in callbackArgs will be appended at the end.
                callbackArgs: null,

                // getCallbackArgsCallback(cellName, cellEditedValue, cellValue, rowData)
                // get arguments for callback if arguments are not static.
                // if getCallbackArgsCallback is defined it has precendence over callbackArgs 
                // and it must return array of arguments
                getCallbackArgsCallback: null,

                callbackScope: null, // scope in which to invoke callback
            },


            sortColumnName: 'id',
            sortColumnOrder: 'desc',

            reader: {
                repeatItems: false,
                dataProperty: null,
                primaryKey: null
            },

            hiddenColumns: [],
            data: [],
            selectedRowIndex: -1,
            selectedRowData: null,

            selectedRowEl: null,
            enableShortcuts: true,
            cellEdit: false,
        },

        _tableId: null,
        _tableState: 'normal', // can be 'normal', 'loading', 'locked', 'editing'
        _isTableLocked: false,

        _tHeadEl: null,
        _tBodyEl: null,
        _wrapperOut: null,
        _lockGridContainer: null,

        _currentCellEditRowIndex: -1,
        _currentCellEditColumnName: '',
        _currentCellTdEl: null,
        _currentCellEditInputEl: null,
        _currentCellEditInitalizedInputEl: null,
        _currentCellEditType: '',
        _cellMessageContainer: null,

        _currentSortColumnName: '',
        _currentSortColumnOrder: '',
        _maxTableHeight: null,
        _noRecordsFoundTREl: null,
        _scrollbarWidth: null,
        _resizeTimeout: 0,

        _exception: function(errorMessage, code) {

            this.getMessage = function() {
                return errorMessage
            };

            this.getCode = function() {
                return code;
            }
        },

        getTableState: function() {
            return this._tableState;
        },

        getIsTableCellEditing: function() {
            return this._currentCellEditRowIndex > -1;
        },

        getSelectedRowsIndexes: function() {
            return this.options.selectedRows;
        },

        getSelectedRowsData: function() {

            var selectedRowsData = [];

            var selectedRowsIndexes = this.getSelectedRowsIndexes();
            var selectedRowsLen = selectedRowsIndexes.length;

            for(var i = 0; i < selectedRowsLen; i++) {
                selectedRowsData.push($.extend({}, this.options.data[selectedRowsIndexes[i]]));
            }

            return selectedRowsData;
        },

        getCurrentSortColumnName: function() {
            return this._currentSortColumnName;
        },

        getCurrentSortColumnOrder: function() {
            return this._currentSortColumnOrder;
        },

        getCurrentCellEditName: function() {
            return this._currentCellEditColumnName;
        },

        _create: function() {

            this._tableId = Math.floor(Math.random() * 999999999999999) + 1;

            if(this.options.data.length === 0) {
                this.options.data = [];
            }

            this.options.hiddenColumns = [];

            if(this.element.is('table') === false) {
                throw new Error(
                    'Grid can only be instantiated on table tag',
                    1700
                );
            }

            if(this.element.attr('id').length < 1) {
                this.element.attr('id', 'jq-grid-' + this._tableId);
            }

            if(this.options.sortColumnName)
            {
                this._currentSortColumnName = this.options.sortColumnName;
            }

            if(this.options.sortColumnOrder)
            {
                this._currentSortColumnOrder = this.options.sortColumnOrder;
            }

            //wrap the table element
            this._wrapperOut =  $('<div/>', {
               class: 'table-wrapper-out'
            });

            var tableWrapper = $('<div/>', {
                class: 'table-wrapper'
            });

            this.element.after(this._wrapperOut);
            this._wrapperOut.append(tableWrapper);

            this._lockGridContainer = $('<div/>', {
                'class': 'jq-grid-lock-container'
            });

            if(this.options.lockGridTemplate && typeof this.options.lockGridTemplate === 'function') {
                this._lockGridContainer.html(this.options.lockGridTemplate());
            }
            else {

                this._lockGridContainer.append($('<div/>', {
                    'class': 'lock-grid-animation'
                }));

                this._lockGridContainer.append($('<div/>', {
                    'class': 'lock-grid-message',
                    text: this.options.lockMessage
                }));
            }

            this._wrapperOut.append(this._lockGridContainer);

            tableWrapper.append(this.element);

            this.element.addClass('jq-grid');
            this.element.attr('cellspacing', 0);
            this.element.attr('cellpadding', 0);
            this.element.attr('border', 0);
            this.element.attr('tabindex', 1);

            this._tHeadEl = $('<thead/>');
            this._tBodyEl = $('<tbody/>');


            var tr = $('<tr/>');

            this._tHeadEl.append(tr);

            var th = null;
            var tha = null;

            if(this.options.multiselect === true) {
                
                th = $('<th/>', {
                    class: 'jq-grid-header jq-grid-header-multiselect'
                });

                var multiSelectCheckBox = this._createCheckBoxForMultiSelect(
                    'jq-grid-multiselect-' + (window.performance.now() * 100000000000),
                    'jq-grid-select-all',
                    true
                );

                th.append(multiSelectCheckBox);
                tr.append(th);
            }

            var that = this;
            $.each(this.options.model, function(index) {

                // skip if column does not have id
                if(this.id && this.id.toString().length > 0) {

                    th = $('<th/>', {
                        class: 'jq-grid-header jq-grid-header-' + this.id,
                        text: this.header,
                        'data-id': this.id
                    });

                    if(that.options.sortColumnName === this.id) {
                        th.addClass('jq-grid-column-sort jq-grid-column-sort-' + that.options.sortColumnOrder);
                    }

                    if(this.sortable !== false) {
                        that._on(th, {
                            click: that._headerColumnClickHandler
                        });
                    }

                    tr.append(th);

                    if(this.hidden === true) {
                        that.options.hiddenColumns.push(this.id);
                        th.addClass('jq-grid-column-hidden');
                        th.hide();
                    }

                    tr.append(th);
                }
            });

            this.element.append(this._tHeadEl);
            this.element.append(this._tBodyEl);

            this._cellMessageContainer = this.element.closest('body').find('.jq-grid-cell-message-container');

            if(this._cellMessageContainer.length === 0) {

                this._cellMessageContainer = $('<div/>', {
                    'class': 'jq-grid-cell-message-container',
                });

                this.element.closest('body').append(this._cellMessageContainer);
            }

            if(this.options.data.length > 1) {
                for(var i = 0, len = this.options.data.length; i < len; i++) {
                    this._renderRow(this.options.data[i], i);
                }
            }

            this._checkRowNumbers();

            if(this.options.autoLoad === true) {
                this._loadData();
            }

            this.setTableScrollbarClass();
            this._setTableWrapperFadeRightPosition();

            if(this.options.enableShortcuts) {
                this._enableKeyboardShortcuts();
            };

            this._on( window, {
                resize: this.setTableScrollbarClass,
                orientationchange: this.setTableScrollbarClass
            });

            this._on(document, {
                click: this._documentClickHandler
            });
        },

        _getTableId: function() {

            var tableId = this.element.attr('id');

            if(tableId.length > 0) {
                return tableId;
            }
            else {
                return 
            }
        },

        // @to-do fix bug. this handler is called twice for widget popups
        _documentClickHandler: function(ev) {

            // try to cancel cell edit if user clicks outside of table and 
            // table is in editing mode
            if(this._tableState === 'editing') {

                var fieldModelData = this.getColumnModelData(this._currentCellEditColumnName);

                // if cancelEditOnClick is explicitly set to false skip canceling cell edit
                if(fieldModelData.editOptions && fieldModelData.editOptions.cancelEditOnClick === false) {
                    return;
                }
                // if cell is not edited with widget try to cancel cell edit
                // widgets should have their own cancel and save button controls
                else if(fieldModelData.editOptions && fieldModelData.editOptions.type !== 'widget') {

                    var closestTable = $(ev.target).closest('table.jq-grid');

                    if(closestTable[0] !== this.element[0]) {

                        if(this._currentCellEditInitalizedInputEl &&
                           $(ev.target).closest(this._currentCellEditInitalizedInputEl).length > 0) {
                            // do not close the grid
                            // user clicked on cell edit popup
                        }
                        else {
                            this.cancelCellEdit();
                        }
                    }
                }
            }
        },

        _createCheckBoxForMultiSelect: function(checkBoxId, checkBoxClass, isSelectAll, rowIndex) {

            var span = $('<span/>', {
                class: 'custom-checkbox-container centered-custom-checkbox'
            });

            var checkbox = $('<input/>',{
                type: 'checkbox',
                id: checkBoxId,
                class: checkBoxClass,
                "data-index": rowIndex
            });

            var label = $('<label/>', {
                for: checkBoxId,
                html: '&nbsp;'
            });

            if(isSelectAll) {
                this._on(checkbox, {
                    click: this._selectAllClickHandler
                });
            }
            else {
                this._on(checkbox, {
                    click: this._selectRowClickHandler
                });
            }

            span.append(checkbox);
            span.append(label);

            return span;
        },

        _selectAllClickHandler: function(event) {

            var checkBox = $(event.currentTarget);
            var isChecked = checkBox.prop('checked') === true;

            if(isChecked) {
                this.checkAllRows();
            }
            else {
                this.uncheckAllRows();   
            }
        },

        _selectRowClickHandler: function(event) {

            var checkBox = $(event.currentTarget);
            var isChecked = checkBox.prop('checked') === true;

            var rowIndex = checkBox.data('index');
            var selectedRowArrayIndex = this.options.selectedRows.indexOf(rowIndex);

            if(selectedRowArrayIndex !== -1 && isChecked === false) {
                this.options.selectedRows.splice(selectedRowArrayIndex, 1);
            }
            else {
                this.options.selectedRows.push(rowIndex);
            }
        },

        lockGrid: function(message, showAnimation) {

            if(message) {
                this._lockGridContainer.find('.lock-grid-message').empty().html(message);
            }
            else {
                this._lockGridContainer.find('.lock-grid-message').empty().text(this.options.lockMessage);
            }

            if(showAnimation !== false) {
                this._lockGridContainer.find('.lock-grid-animation').show();
            }
            else {
                this._lockGridContainer.find('.lock-grid-animation').hide();
            }

            this._wrapperOut.addClass('table-blocked');
            this._lockGridContainer.show();

            if(this._currentCellEditInputEl) {
                this._currentCellEditInputEl.blur();
            }

            this._isTableLocked = true;
            this._tableState = 'locked';
        },

        unlockGrid: function() {

            this._lockGridContainer.hide();
            this._wrapperOut.removeClass('table-blocked');

            if(this._currentCellEditInputEl) {
                this._currentCellEditInputEl.focus();
            }

            this._isTableLocked = false;
            this._tableState = 'normal';
        },

        checkAllRows: function() {

            this._tBodyEl.find('.jq-grid-select-row').each(function(){
                $(this).prop('checked', true);
            });

            this.options.selectedRows = [];
            var dataLength = this.options.data.length;

            for(var i = 0; i < dataLength; i++) {
                this.options.selectedRows.push(i);
            }
        },

        uncheckAllRows: function() {

            this._tBodyEl.find('.jq-grid-select-row').each(function(){
                $(this).prop('checked', false);
            });

            this.options.selectedRows = [];
        },

        openEditingCellWidget: function() {

            if(this._currentCellEditColumnName) {

                var columnModelData = this.getColumnModelData(this._currentCellEditColumnName);

                if(columnModelData.editOptions && this._isFunction(columnModelData.editOptions.openWidget)) {
                    columnModelData.editOptions.openWidget(this._currentCellEditInitalizedInputEl);
                }
            }
        },

        closeEditingCellWidget: function() {

            if(this._currentCellEditColumnName) {

                var columnModelData = this.getColumnModelData(this._currentCellEditColumnName);

                if(columnModelData.editOptions && this._isFunction(columnModelData.editOptions.closeWidget)) {
                    columnModelData.editOptions.closeWidget(this._currentCellEditInitalizedInputEl);
                }
            }
        },

        _enableKeyboardShortcuts: function () {
            this._on(document, {
                keydown: this._keydownEventHandler
            });
        },

        _disableKeyboardShortcuts: function () {
            this._off(document, 'keydown');
        },

        _keydownEventHandler: function (ev) {

            //if table is not visible skip everything
            if ( !(this.element.is(':visible')) ) { return; }

            var currentSelectedRowIndex = this.getSelectedRowIndex();

            var keyCode = ev.keyCode;

            if(this._tableState === 'normal' && this.options.data.length > 0) {

                switch (keyCode) {

                    case 38: /* arrow up */

                        ev.preventDefault();
                        ev.stopPropagation();

                        if(currentSelectedRowIndex > -1) {
                            currentSelectedRowIndex--;
                        }

                        this._selectRow(Math.max(currentSelectedRowIndex, 0));

                    break;
            
                    case 40: /* arrow down */

                        ev.preventDefault();
                        ev.stopPropagation();

                        currentSelectedRowIndex++;

                        this._selectRow(Math.min(currentSelectedRowIndex, this.options.data.length - 1));

                    break;
                }
            }
            else if(this._tableState === 'editing' && this._currentCellEditType !== 'widget') {

                if(keyCode === 13) { // enter
                    this.finishCellEdit();
                }
                else if(keyCode === 27) { // escape
                    this.cancelCellEdit();
                }
            }
        },

        _scrollStepHorizontal: 80,

        _scrollTableRight: function () {
            if(!(this._elementHasScrollbar(this.element))) { return; }

            var that = this;
            var tableWrap = this._wrapperOut.find('.table-wrapper')[0];
            var maxScroll = tableWrap.scrollLeftMax;
            if (tableWrap.scrollLeft === maxScroll) { return; }

            $(tableWrap)
                .stop(true)
                .animate({
                    scrollLeft: '+=' + that._scrollStepHorizontal
                }, 100)
        },

        _scrollTableLeft: function () {
            if(!(this._elementHasScrollbar(this.element))) { return; }

            var that = this;
            var tableWrap = this._wrapperOut.find('.table-wrapper')[0];
            if (tableWrap.scrollLeft === 0) { return; }

            $(tableWrap)
                .stop(true)
                .animate({
                    scrollLeft: '-=' + that._scrollStepHorizontal
                }, 100);
        },

        _simulateDoubleClickOnFocusedRow: function () {
            this._trigger('onRowDoubleClick', null, {
                rowData: this.options.selectedRowData,
                rowIndex: this.options.selectedRowIndex
            });
        },

        _selectRow: function (rowIndex, columnId, tdElement) {

            var rowData = this.options.data[rowIndex];
            var isCurrentCellAlreadyBeingEdited = this._currentCellEditColumnName === columnId && this._currentCellEditRowIndex === rowIndex;

            if(isCurrentCellAlreadyBeingEdited) {
                return;
            }
            else if(this._tableState === 'editing') {
                this.finishCellEdit();
                return;
            }

            this.options.selectedRowIndex = rowIndex;

            var selectedRowElement = this._getRowElementByIndex(rowIndex);

            if(this.selectedRowEl) {
                this.selectedRowEl.removeClass('jq-grid-selected-row');
            }

            this.selectedRowEl = selectedRowElement;
            this.selectedRowEl.addClass('jq-grid-selected-row');

            if(columnId) {
                this._trigger('onCellSelected', null, {
                    rowData: rowData,
                    rowIndex: rowIndex,
                    columnId: columnId,
                });    
            }

            this._trigger('onRowSelected', null, {
                rowData: rowData,
                rowIndex: rowIndex
            });

            if(columnId && tdElement && this.options.cellEdit) {

                var columnModelData = this.getColumnModelData(columnId);

                if(columnModelData.editable) {
                    this._createCellEditInput(tdElement, columnModelData, rowIndex, rowData);
                }
            }
        },

        deselectAllRows: function() {

            this.options.selectedRowIndex = -1;
            this.selectedRowEl = null;
            this.selectedRowData = null;
            this.selectedRowIndex = -1;
            this.selectedRowElement = null;
            this.selectedRowsData = [];
            this.selectedRowsIndexes = [];

            this._tBodyEl.find('.jq-grid-selected-row').removeClass('jq-grid-selected-row');

            this.uncheckAllRows();
        },

        setColumnEditOptions: function(columnName, options) {

            var columnModelData = this.getColumnModelData(columnName);

            if(columnModelData.editOptions) {
                $.extend(columnModelData.editOptions, options);
            }
            else {
                columnModelData.editOptions = options;
            }
        },

        getColumnEditOptions: function(columnName) {

            var columnModelData = this.getColumnModelData(columnName);

            return columnModelData.editOptions;
        },

        reloadGrid: function() {

            this.removeAllRows();
            this._loadData();
        },

        addRow: function(data, rowIndex) {

            if(typeof rowIndex === 'undefined' || rowIndex < 0) {
                rowIndex = this.options.data.length;
            }

            this.options.data.push(data);
            this._renderRow(data, rowIndex);

            if(rowIndex !== this.options.data.length) {
                this._refreshRowsIndexes();
            }

            //check row numbers and show message if 0
            this._checkRowNumbers();
        },

        _checkRowNumbers: function() {

            if(this.options.data.length === 0) {
                this.showEmptyRowsMessage();
            }
            else if(this._noRecordsFoundTREl) {
                this.hideEmptyRowsMessage();
            }
        },

        showEmptyRowsMessage: function(message) {

            this.hideEmptyRowsMessage();

            var thLength = this.options.model.length - this.options.hiddenColumns.length;

            if(this.options.multiselect === true) {
                thLength++;
            }

            var tbodyElement = this._tBodyEl;

            this._noRecordsFoundTREl = $('<tr/>', {
                class: 'jq-grid-row'
            });

            var td = $('<td/>', {
                class: 'jq-grid-cell jq-grid-cell-spanned-empty',
                colspan: thLength
            });

            var emptyRowsMessage = message || 'No records found';
            var textEl = $('<h2/>', {
                class: 'jq-grid-empty-title',
                html: emptyRowsMessage
            });

            td.append(textEl);
            this._noRecordsFoundTREl.append(td);

            tbodyElement.empty().append(this._noRecordsFoundTREl);
        },

        hideEmptyRowsMessage: function() {

            if(this._noRecordsFoundTREl) {
                this._noRecordsFoundTREl.remove();
            }
        },

        updateRow: function(rowIndex, rowData) {

            if(this.options.data[rowIndex]) {

                var columnModelData = null;
                var tr = this._getRowElementByIndex(rowIndex);

                this.options.data[rowIndex] = rowData;

                var columnIndex = null;

                for(columnIndex in rowData) {

                    columnModelData = this.getColumnModelData(columnIndex);

                    if(!columnModelData) {
                        continue;
                    }

                    this._updateCellData(rowData, columnModelData, tr);
                }
            }
        },

        removeRow: function(index) {

            if(this.options.data[index]) {

                var tr = this._getRowElementByIndex(index);
                var elIndex = null;

                if(tr.length === 1) {

                    elIndex = tr.data('index');

                    this._off(tr, 'click');
                    this._off(tr, 'dblclick');

                    if(this.options.multiselect) {
                        this._off(tr.find('.jq-grid-select-row'), 'click');
                    }

                    tr.remove();
                    this.options.data.splice(elIndex, 1);

                    this._refreshRowsIndexes();
                }
            }

            //check row numbers and show message if 0
            this._checkRowNumbers();
        },

        removeAllRows: function(removeData) {

            // remove all data by default
            if(removeData !== false) {
                this.options.data = [];
            }

            this.options.selectedRowIndex = -1;
            this.options.selectedRowData = null;

            this._off(this._tBodyEl.find('jq-grid-row'), 'click');
            this._off(this._tBodyEl.find('jq-grid-row'), 'dblclick');

            if(this.options.multiselect) {
                this._off(this._tBodyEl.find('.jq-grid-select-row'), 'click');
            }

            this._tBodyEl.empty();

            //check row numbers and show message if 0
            this._checkRowNumbers();
        },

        getSelectedRowData: function() {
            return this.options.selectedRowData;
        },

        getSelectedRowIndex: function() {
            return this.options.selectedRowIndex;
        },

        getRowDataByIndex: function(index) {
            if(this.options.data.length > index) {
                return this.options.data[index];
            }
            else {
                return null;
            }
        },

        getAllData: function() {
            return this.options.data;
        },

        showColumn: function(columnId) {
            this._tHeadEl.find('.jq-grid-header-' + columnId).show();
            this._tBodyEl.find('.jq-grid-cell-' + columnId).show();
        },

        hideColumn: function(columnId) {
            this._tHeadEl.find('.jq-grid-header-' + columnId).hide();
            this._tBodyEl.find('.jq-grid-cell-' + columnId).hide();
        },

        _rowDoubleClickEventHandler: function () {
            this._trigger('onRowDoubleClick', null, {
                rowData: this.options.selectedRowData,
                rowIndex: this.options.selectedRowIndex
            });
        },

        _getScrollbarWidth: function () {
            if (this._scrollbarWidth === null) {
                this._scrollbarWidth = this._calculateScrollbarWidth();
            }
            return this._scrollbarWidth;
        },

        _setTableWrapperFadeRightPosition: function () {
            this._wrapperOut.find('.table-wrapper-fade').css({
                right: this._getScrollbarWidth(),
                bottom: this._getScrollbarWidth()
            });
            this._wrapperOut.find('.table-wrapper-fade-left').css({
                bottom: this._getScrollbarWidth()
            });
        },

        setTableScrollbarClass: function () {

            if(this._resizeTimeout) {
                clearTimeout(this._resizeTimeout);
            }

            var that = this;
            this._resizeTimeout = setTimeout(function() {

                that._resizeTimeout = null;
                var tableWrap = that._wrapperOut.find('.table-wrapper');

                if (that._elementHasVerticalScrollbar(that.element)) {
                    that._wrapperOut.addClass('has_vertical_scrollbar');
                } else {
                    that._wrapperOut.removeClass('has_vertical_scrollbar');
                }

                if(that._elementHasScrollbar(that.element)) {

                    //set initially fade right and scroll table to the max left
                    tableWrap.scrollLeft(0);
                    that._wrapperOut.addClass('has_scrollbar show_right_fade');

                    //set scroll event listener
                    that._on(
                        tableWrap, {
                            scroll: that._tableScrollEventHandler
                        }
                    );
                } else {
                    that._wrapperOut.removeClass('has_scrollbar show_right_fade show_left_fade');
                    that._off( tableWrap, 'scroll' );
                }
            }, 200);
        },

        _setTableMaxHeight: function () {
            if (this._maxTableHeight === null) {
                this._maxTableHeight = this._wrapperOut.find( '.table-wrapper' ).height();
            }
            return this._maxTableHeight;
        },

        _tableScrollEventHandler: function(event) {
            this._setTableFadeElementsAccordingToScrollPosition();
        },

        _setTableFadeElementsAccordingToScrollPosition: function () {
            //show-hide table-fade element when table is scrolled to the max
            var tableWrap = this._wrapperOut.find('.table-wrapper');
            var that = this;

            var maxScrollLeft;
            if (typeof tableWrap[0].scrollLeftMax !== 'undefined') {
                //property supported only in Firefox
                maxScrollLeft = tableWrap[0].scrollLeftMax;
            } else {
                maxScrollLeft = tableWrap[0].scrollWidth - tableWrap[0].clientWidth;
            }

            if(maxScrollLeft === tableWrap[0].scrollLeft) {

                /* scrolled to the max right */
                if( !this._wrapperOut.hasClass('show_right_fade') ) { return; }

                this._wrapperOut.find('.table-wrapper-fade' )
                    .stop(true)
                    .fadeTo(200, 0, function () {
                        that._wrapperOut.removeClass('show_right_fade');
                    });

                this._wrapperOut.find('.table-wrapper-fade-left' )
                    .stop(true)
                    .fadeTo(200, 1, function () {
                        that._wrapperOut.addClass('show_left_fade');
                    });

            }
            else if(tableWrap[0].scrollLeft === 0) {

                /* scrolled to max left */
                if( !this._wrapperOut.hasClass('show_left_fade') ) { return; }

                this._wrapperOut.find('.table-wrapper-fade' )
                    .stop(true)
                    .fadeTo(200, 1, function () {
                        that._wrapperOut.addClass('show_right_fade');
                    });

                this._wrapperOut.find('.table-wrapper-fade-left' )
                    .stop(true)
                    .fadeTo(200, 0, function () {
                        that._wrapperOut.removeClass('show_left_fade');
                    });

            }
            else {

                /* scrolled somewhere between max left and max right */
                if(this._wrapperOut.hasClass('show_right_fade show_left_fade') ) { return; }

                this._wrapperOut.find('.table-wrapper-fade-left' )
                    .stop(true)
                    .fadeTo(200, 1, function () {
                        that._wrapperOut.addClass('show_left_fade');
                    });

                this._wrapperOut.find('.table-wrapper-fade' )
                    .stop(true)
                    .fadeTo(200, 1, function () {
                        that._wrapperOut.addClass('show_right_fade');
                    });
            }
        },

        _elementHasScrollbar: function (element) {
            return element.width() > element.parent().width();
        },

        _elementHasVerticalScrollbar: function (element) {
            return element.height() > element.parent().height();
        },

        _getRowElementByIndex: function(index) {
            return this._tBodyEl.find("tr:nth-child(" + (index + 1) + ")");
        },

        _refreshRowsIndexes: function() {

            var that = this;
            var rowEl = null;

            this._tBodyEl.find('.jq-grid-row').each(function(index){

                rowEl = $(this);
                rowEl.data('index', index);

                if(rowEl.hasClass('jq-grid-selected-row')) {
                    that.options.selectedRowIndex = index;
                }
            });
        },

        _loadData: function() {

            var that = this;

            var url = this.options.url;

            if(typeof this.options.ajaxOptions.getUrlCallback === 'function') {
                url = this.options.ajaxOptions.getUrlCallback();
            }

            this.lockGrid();

            $.ajax({

                url: this.options.url,
                dataType: "json",
                type: this.options.ajaxOptions.methodType,
                data: this.options.ajaxOptions.parameters,

                success: function(data, textStatus, jqXHR) {

                    var i;

                    if(data instanceof Array) {
                        for(i = 0, len = data.length; i < len; i++) {
                            that.addRow(data[i], i);
                        }
                    }
                    else if(typeof data === 'object') {

                        var propertyName = this.options.ajaxOptions.dataListPropertyName;

                        if(data.hasOwnProperty(propertyName) && data[propertyName] instanceof Array) {

                            var dataList = data[propertyName];

                            for(i = 0, len = dataList.length; i < len; i++) {
                                that.addRow(dataList[i], i);
                            }
                        }
                    }
                    else {
                        console.log("Unable to parse read data");
                    }
                },

                error: function(jqXHR, textStatus, errorThrown) {

                },

                complete: function(jqXHR, textStatus) {
                    that.unlockGrid();
                }
            });
        },

        _renderRow: function(rowData, rowIndex) {

            var tr = $("<tr/>", {
                class:'jq-grid-row',
                'data-index': rowIndex
            });

            if(rowIndex === 0) {
                this._tBodyEl.prepend(tr);
            }
            else {
                this._tBodyEl.find("tr:nth-child(" + rowIndex + ")").after(tr);
            }

            this._on(tr, {
                click: this._rowClickEventHandler,
                dblclick: this._rowDoubleClickEventHandler
            });

            var td;
            var columnModelData;

            if(this.options.multiselect === true) {

                var multiSelectCheckBox = this._createCheckBoxForMultiSelect(
                    'jq-grid-multiselect-' + (window.performance.now() * 100000000000),
                    'jq-grid-select-row',
                    false,
                    rowIndex
                );

                td = $('<td/>', {
                    class:  'jq-grid-cell',
                    'data-index': rowIndex,
                });

                td.append(multiSelectCheckBox);
                tr.append(td);
            }

            for(var columnIndex in this.options.model) {

                columnModelData = this.options.model[columnIndex];

                if(!columnModelData) { continue; }

                var cssDisplayString = '';

                if(columnModelData.hidden === true) {
                    cssDisplayString = 'display: none';
                }

                td = $('<td/>', {
                    class:  'jq-grid-cell jq-grid-cell-' + columnModelData.id,
                    'data-id': columnModelData.id,
                    'data-index': rowIndex,
                    'style': cssDisplayString
                });

                tr.append(td);

                this._updateCellData(rowData, columnModelData, tr);
            }

            this.setTableScrollbarClass();
        },

        _updateCellData: function(rowData, columnModelData, rowElement) {

            if(!rowData || !columnModelData || rowElement.length === 0) {
                return;
            }

            var td = rowElement.find('.jq-grid-cell-' + columnModelData.id);

            var cellData = '';
            var rowIndex = rowElement.data('index');

            if(typeof columnModelData.formatter === "function") {
                cellData = columnModelData.formatter(rowData[columnModelData.dataMap], rowData, rowIndex) || '';
            }
            else{
                cellData = rowData[columnModelData.dataMap] || '';
            }

            td.empty();

            if(typeof cellData === 'object') {
                td.append(cellData);
            }
            else {
                td.append($.parseHTML(cellData.toString()));
            }

            return cellData;
        },

        getAllColumnData: function() {
            return this.options.model;
        },

        getColumnModelData: function(columnId) {

            if(columnId && columnId.toString().length > 0) {

                var item;

                for(var i = 0, len = this.options.model.length; i < len; i++) {

                    item = this.options.model[i];

                    if(item.id.toString() === columnId.toString()) {
                        return item;
                    }
                }
            }

            return null;
        },

        _cellEditInputClickHandler: function(ev) {
            //console.log(ev)
        },

        _cellEditInputKeydownHandler: function(ev) {

            // check if table is locked
            if(this._isTableLocked) {
                return;
            }

            if(this._currentCellEditType !== 'widget') {

                if(ev.keyCode === 13) { // enter
                    this.finishCellEdit();
                }
                else  if(ev.keyCode === 27) { // escape
                    this.cancelCellEdit();
                }
            }
        },

        _cellEditInputKeyupHandler: function(ev) {
            //console.log(ev)
        },

        _editCellSelectInputChangeHandler: function(ev) {
            this.finishCellEdit();
        },

        _createCellEditInput: function(cellTdEl, columnModelData, rowIndex, rowData) {
            
            this._currentCellTdEl = cellTdEl;
            this._currentCellEditColumnName = columnModelData.id;
            this._currentCellEditRowIndex = rowIndex;

            var inputType = 'text';
            var editOptions = columnModelData.editOptions;

            if(editOptions && editOptions.type) {
                inputType = editOptions.type;
            }

            this._currentCellEditType = inputType;

            if(inputType === 'textarea') {
                this._currentCellEditInputEl = $('<textarea/>', {
                    'data-column-id': columnModelData.id,
                    'data-row-index': rowIndex,
                    'class': 'cell-edit-input'
                });
            }
            else if(inputType === 'select') {
                this._currentCellEditInputEl = $('<select/>', {
                    'data-column-id': columnModelData.id,
                    'data-row-index': rowIndex,
                    'class': 'cell-edit-input'
                });

            }
            else if(inputType === 'widget') {
                this._currentCellEditInputEl = $('<div/>', {
                    'data-column-id': columnModelData.id,
                    'data-row-index': rowIndex,
                    'class':'cell-edit-widget'
                });
            }
            else {
                // fallback to input as default
                this._currentCellEditInputEl = $('<input/>', {
                    type: inputType,
                    'data-column-id': columnModelData.id,
                    'data-row-index': rowIndex,
                    'class': 'cell-edit-input'
                });
            }


            var optionRowData;
            var optionEl;

            if( inputType === 'select' &&
                editOptions &&
                editOptions.availableOptions &&
                this._isArray(editOptions.availableOptions)) {

                var value;
                var label;
                var rowDataValue;

                var avialableOptions = columnModelData.editOptions.availableOptions;

                for(var i = 0, len = avialableOptions.length; i < len; i++) {

                    optionRowData = avialableOptions[i];

                    optionEl = $('<option/>');

                    value = this._extractCellEditValue(optionRowData, columnModelData);
                    optionEl.attr('value', value);

                    label = this._extractCellEditLabel(optionRowData, columnModelData);
                    optionEl.text(label);

                    rowDataValue = this._extractCellEditValue(rowData[columnModelData.id], columnModelData);

                    if(rowDataValue && rowDataValue.toString() ===  value.toString()) {
                        optionEl.attr('selected', true);
                    }

                    this._currentCellEditInputEl.append(optionEl);
                }

                this._on(this._currentCellEditInputEl, {
                    change: this._editCellSelectInputChangeHandler
                });
            }
            else if(inputType === 'text') {
                this._currentCellEditInputEl.val(rowData[columnModelData.dataMap]);
            }

            cellTdEl.addClass('cell-edit');

            if(inputType !== 'widget') {
                cellTdEl.empty();
            }

            cellTdEl.append(this._currentCellEditInputEl);

            this._currentCellEditInputEl.focus();

            if(editOptions && editOptions.initializeInputCallback && this._isFunction(editOptions.initializeInputCallback)) {
                this._currentCellEditInitalizedInputEl = editOptions.initializeInputCallback(this._currentCellEditInputEl);
            };

            if(editOptions && editOptions.setInputData) {
                var cellEditInput = this._currentCellEditInitalizedInputEl || this._currentCellEditInputEl;
                editOptions.setInputData(cellEditInput, rowData[columnModelData.id], rowData);
            };

            this._tableState = 'editing';

            this._on(this._currentCellEditInputEl, {
                click: this._cellEditInputClickHandler,
                keydown: this._cellEditInputKeydownHandler,
                keyup: this._cellEditInputKeyupHandler,
            });

            this._trigger('onCellEditStarted', null, {
                cellEditInput: this._cellEditInputClickHandler,
                rowIndex: rowIndex,
                rowData: rowData
            });
        },


        restorCellFromEdit: function(cellData, columnModelData, rowIndex, rowData) {

            this._currentCellTdEl.removeClass('cell-edit');

            var currentCellData = rowData[columnModelData.id];
            var areAvailableOptionsStatic = false;

            var editOptions = columnModelData.editOptions;

            if(editOptions && editOptions.areAvailableOptionsStatic) {
                areAvailableOptionsStatic = editOptions.areAvailableOptionsStatic;
            }

            // if newCellData is object override current cell data with newCellData
            if(this._isObject(cellData)) {

                if(areAvailableOptionsStatic) {
                    this.options.data[rowIndex][columnModelData.id] = this._extractCellEditValue(cellData, columnModelData);
                }
                else {
                    this.options.data[rowIndex][columnModelData.id] = $.extend({}, currentCellData, cellData);
                }
            }
            else {
                this.options.data[rowIndex][columnModelData.id] = cellData;    
            }

            // update selected row data with newly updated row data
            this.options.selectedRowData = this.options.data[rowIndex];

            var updatedCellData = this.options.data[rowIndex][columnModelData.id];
            var newCellLabel = '';

            if(this._isFunction(columnModelData.formatter)) {
                newCellLabel = columnModelData.formatter(updatedCellData, rowData, rowIndex);
            }
            else {
                newCellLabel = this._extractCellEditLabel(updatedCellData, columnModelData);
            }

            this._currentCellTdEl.empty().html(newCellLabel);

            this._off(this._currentCellEditInputEl, 'change');    
            this._off(this._currentCellEditInputEl, 'click');
            this._off(this._currentCellEditInputEl, 'keydown');
            this._off(this._currentCellEditInputEl, 'keyup');

            if(editOptions && editOptions.type === 'widget' && this._isFunction(editOptions.destroyWidget)) {
                editOptions.destroyWidget(this._currentCellEditInitalizedInputEl);
            };

            this._currentCellTdEl = null;
            this._currentCellEditColumnName = '';
            this._currentCellEditRowIndex = -1;
            this._currentCellEditInitalizedInputEl = null;
            this._currentCellEditType = '';

            this._tableState = 'normal';

            this.removeCellError();
        },

        _extractCellEditValue: function(cellData, columnModelData) {

            var value;
            var valueProperty;

            // find the property value
            if(columnModelData.editOptions && columnModelData.editOptions.valueProperty) {
                valueProperty = columnModelData.editOptions.valueProperty;
            }
            else {
                valueProperty = 'id'; // try to guess the value property
            }

            // extract the value from cell data
            if(this._isObject(cellData) && cellData.hasOwnProperty(valueProperty)) {
                value = cellData[valueProperty];
            }
            else if(this._isArray(cellData)) {

                var item;
                value = [];

                for(var i = 0, len = cellData.length; i < len; i++) {

                    item = cellData[i];

                    if(item.hasOwnProperty(valueProperty)) {
                        value.push(item[valueProperty]);
                    }
                }
            }
            else if(this._isString(cellData) || isNaN(cellData) === false) {
                value = cellData;
            }

            return value;
        },

        _extractCellEditLabel: function(cellData, columnModelData) {

            var label;
            var labelProperty;

            // find the property label
            if(columnModelData.editOptions && columnModelData.editOptions.labelProperty) {
                labelProperty = columnModelData.editOptions.labelProperty;
            }
            else {
                labelProperty = 'label'; // try to guess the label property
            }

            if(this._isObject(cellData) && cellData.hasOwnProperty(labelProperty)) {
                label = cellData[labelProperty];
            }
            else if(this._isString(cellData) || isNaN(cellData) === false) {
                label = cellData;
            }

            return label;
        },

        finishCellEdit: function() {

            if(this._currentCellEditRowIndex === -1) {
                return;
            }

            this.removeCellError();

            var columnModelData = this.getColumnModelData(this._currentCellEditColumnName);
            var rowData = this.options.data[this._currentCellEditRowIndex];
            var editOptions = columnModelData.editOptions;

            var oldCellData = rowData[this._currentCellEditColumnName] || '';
            var editedCellData;

            if(editOptions && editOptions.getInputData && this._isFunction(editOptions.getInputData)) {
                editedCellData = editOptions.getInputData(this._currentCellEditInitalizedInputEl);
            }
            else if(this._currentCellEditInputEl.is('input') ||
                this._currentCellEditInputEl.is('select') || 
                this._currentCellEditInputEl.is('textarea')) {

                editedCellData = this._currentCellEditInputEl.val();

                if(editOptions && editOptions.availableOptions) {
                    editedCellData = this.findRecordByPropertyValue(editOptions.availableOptions, editOptions.valueProperty, editedCellData);
                }
            }

            var isDataValid = true;
            var isDataChanged = false;

            if(editOptions && this._isFunction(editOptions.checkIfCellDataIsChanged)) {
                isDataChanged = editOptions.checkIfCellDataIsChanged(columnModelData.id, oldCellData, editedCellData, rowData);
            }
            else {

                if(this._isArray(oldCellData) && this._isArray(editedCellData) && oldCellData !== editedCellData) {
                    isDataChanged = true;
                }
                else if(this._isObject(oldCellData) && this._isObject(editedCellData)) {

                    var oldCellDataValue = this._extractCellEditValue(oldCellData, columnModelData);
                    var editedCellDataValue = this._extractCellEditValue(editedCellData, columnModelData);

                    isDataChanged = oldCellDataValue.toString() !== editedCellDataValue.toString();
                }
                else if(this._isObject(editedCellData) && (this._isString(oldCellData) || isNaN(oldCellData) === false)) {
                    var editedCellDataValue = this._extractCellEditValue(editedCellData, columnModelData);
                    isDataChanged = oldCellData.toString() !== editedCellDataValue.toString();
                }
                else {
                    isDataChanged = oldCellData.toString() !== editedCellData.toString();
                }
            }

            if(isDataChanged === false) {
                this.restorCellFromEdit(oldCellData, columnModelData, this._currentCellEditRowIndex, rowData);
                return true;
            }
            else if(editOptions) {

                if(editOptions.validateCallback && this._isFunction(editOptions.validateCallback)) {
                    isDataValid = editOptions.validateCallback(editedCellData, columnModelData.id, rowData);
                }
                else if(editOptions.validateRegex) {
                    var regexPattern = new RegExp(editOptions.validateRegex);
                    isDataValid = regexPattern.test(editedCellData);
                }
            }

            if(true === isDataValid) {

                var rowIndex = this._currentCellEditRowIndex;
                var cellEditColumnName = this._currentCellEditColumnName;

                if(this.options.cellEditType === 'local') {


                    this.options.data[this._currentCellEditRowIndex][columnModelData.id] = editedCellData;
                    this.restorCellFromEdit(editedCellData, columnModelData, rowIndex, rowData);

                    this._trigger('onCellEdited', null, {
                        newValue: editedCellData,
                        oldValue: oldCellData,
                        cellName: cellEditColumnName,
                        rowIndex: rowIndex
                    });

                    return true;
                }
                else if(this.options.cellEditType === 'remote') {

                    var ajaxData = null;
                    var formatDataCallback = this.options.cellEditAjaxOptions.formatDataCallback;

                    if(this._isFunction(formatDataCallback)) {
                        ajaxData = formatDataCallback(columnModelData.id, editedCellData, rowData);
                    }
                    else if(this._isObject(editedCellData)) {
                        ajaxData = {};
                        ajaxData[columnModelData.id] = this._extractCellEditValue(editedCellData, columnModelData);
                    }

                    if(editOptions && this._isFunction(editOptions.closeWidget)) {    
                        editOptions.closeWidget(this._currentCellEditInitalizedInputEl);
                    }

                    var that = this;
                    var ajaxParams = $.extend({}, this.options.cellEditAjaxOptions, {

                        success: function(data, textStatus, jqXHR) {

                            that.options.data[that._currentCellEditRowIndex][columnModelData.id] = editedCellData;

                            that._trigger('onCellEdited', null, {
                                editedCellData: editedCellData,
                                oldCellData: oldCellData,
                                cellName: that._currentCellEditColumnName,
                                rowIndex: that._currentCellEditRowIndex,
                                serverResponse: data
                            });

                            that.restorCellFromEdit(
                                editedCellData,
                                columnModelData,
                                that._currentCellEditRowIndex,
                                rowData
                            );
                        },

                        error: function(jqXHR, textStatus, errorThrown) {

                            if(editOptions && that._isFunction(editOptions.openWidget)) {
                                editOptions.openWidget(that._currentCellEditInitalizedInputEl);
                            }

                            var parseErrorCallback = that.options.cellEditAjaxOptions.parseErrorMessageCallback;

                            if(this._isFunction(parseErrorCallback)) {

                                var cellErrorMessage = parseErrorCallback(
                                    jqXHR.responseText,
                                    that._currentCellEditColumnName,
                                    editedCellData
                                );

                                that.showCellError(
                                    that._currentCellEditColumnName,
                                    that._currentCellEditRowIndex,
                                    cellErrorMessage || that.options.defaultCellEditErrorMessage,
                                    that._currentCellTdEl
                                );
                            }
                            else {

                                // try to digg the error message from response

                                try {

                                    var error = jqXHR.responseJSON;

                                    if(!error) {
                                        error = JSON.parse(jqXHR.responseText);
                                    }

                                    if(!error) {
                                        error = errorThrown;
                                    }
                                    
                                    var errorMessage = that._getErrorMessageFromAjaxResponse(error);

                                    that.showCellError(
                                        that._currentCellEditColumnName,
                                        that._currentCellEditRowIndex,
                                        errorMessage,
                                        that._currentCellTdEl
                                    );
                                }
                                catch(err) {
                                    
                                    // whatever is the error use default cell error message

                                    that.showCellError(
                                        that._currentCellEditColumnName,
                                        that._currentCellEditRowIndex,
                                        cellErrorMessage || that.options.defaultCellEditErrorMessage,
                                        that._currentCellTdEl
                                    );
                                }
                            }
                        },

                        complete: function() {
                            that.unlockGrid();
                        }
                    });
                        
                    var getUrlCallback = this.options.cellEditAjaxOptions.getUrlCallback;

                    if(getUrlCallback && this._isFunction(getUrlCallback)) {
                        ajaxParams.url = getUrlCallback(columnModelData.id, editedCellData, rowData);
                    }

                    ajaxParams.data = ajaxData;
                    ajaxParams.type = ajaxParams.method;

                    delete ajaxParams.method;

                    if(this._isString(ajaxParams.url) && ajaxParams.url.length > 0) {
                        this.removeCellError();
                        this.lockGrid();
                        $.ajax(ajaxParams);
                    }
                    else {
                        this.showCellError(
                            this._currentCellEditColumnName,
                            this._currentCellEditRowIndex,
                            'Invalid cell ajax edit url defined',
                            this._currentCellTdEl
                        );
                    }
                }
                else if(this.options.cellEditType === 'callback') {
                    
                    var cellEditCallbackOptions = this.options.cellEditCallbackOptions;

                    if(this._isFunction(cellEditCallbackOptions.callback)) {

                        var editedCellValue = this._extractCellEditValue(editedCellData, columnModelData);
                        var callbackArgs = [columnModelData.id, editedCellData, editedCellValue, rowData];

                        if(this._isFunction(cellEditCallbackOptions.getCallbackArgsCallback)) {
                            callbackArgs = cellEditCallbackOptions.getCallbackArgsCallback(
                                columnModelData.id,
                                editedCellData,
                                editedCellValue,
                                rowData
                            );
                        }
                        else if(this._isArray(cellEditCallbackOptions.callbackArgs)) {
                            callbackArgs = callbackArgs.concat(cellEditCallbackOptions.callbackArgs);
                        }

                        if(cellEditCallbackOptions.callbackScope) {
                            cellEditCallbackOptions.callback.apply(
                                cellEditCallbackOptions.callbackScope,
                                callbackArgs
                            );
                        }
                        else {
                            cellEditCallbackOptions.callback.apply(this, callbackArgs);
                        }
                    }
                    else {
                        this.showCellError(
                            this._currentCellEditColumnName,
                            this._currentCellEditRowIndex,
                            'Invalid cell edit callback defined',
                            this._currentCellTdEl
                        );
                    }
                }
                else {

                    this.showCellError(
                        this._currentCellEditColumnName,
                        this._currentCellEditRowIndex,
                        'Invalid cell edit type option defined',
                        this._currentCellTdEl
                    );
                }

                return false;
            }
            else {
               
                this.showCellError(
                    this._currentCellEditColumnName,
                    this._currentCellEditRowIndex,
                    columnModelData.editOptions.validationErrorMessage || this.options.defaultCellEditErrorMessage,
                    this._currentCellTdEl
                );

                return false;
            }
        },

        _getErrorMessageFromAjaxResponse: function(error) {

            var errorMessage;

            if(this._isObject(error)) {

                if(error.hasOwnProperty('message')) {
                    errorMessage = error.message;
                }
                else if(error.hasOwnProperty('error')) {
                    errorMessage = error.error;
                }
                else if(error.hasOwnProperty('form')) {
                    errorMessage = error.form;
                }
                else if(error.hasOwnProperty('errorMessage')) {
                    errorMessage = error.errorMessage;
                }
                else if(error.hasOwnProperty('text')) {
                    errorMessage = error.text;
                }

                if(this._isObject(errorMessage) && errorMessage.hasOwnProperty('children')) {
                    errorMessage = errorMessage.children[that._currentCellEditColumnName].errors;
                }
                else {
                    errorMessage = error.toString();   
                }
            }
            else if(this._isString(error) && error.length > 0) {
                errorMessage = error;
            }
            else {
                errorMessage = this.options.defaultCellEditErrorMessage;
            }

            return errorMessage;
        },

        removeAllCellMessages: function() {
            this._cellMessageContainer.empty();
        },

        removeCellError: function() {
            this._cellMessageContainer.find('.jq-grid-error-holder-' + this._tableId).remove();
        },

        showCellError: function(cellName, rowIndex, errorMessage, tdElement) {

            var cellErrorMessageHolder = $('<div/>', {
                'class': 'cell-error-holder jq-grid-error-holder-' + this._tableId
            });

            var errorMessageEl = $('<ul/>');    

            if(errorMessage instanceof Array && errorMessage.length > 1) {

                if(errorMessage.length > 1) {

                    var errorItem;

                    for(var i = 0, len = errorMessage.length; i < len; i++) {
                        errorItem = $('<li />', {
                            text: errorMessage[i]
                        });

                        errorMessageEl.append(errorItem);
                    }
                }
                else {
                    errorMessageEl.text(errorMessage[0]);
                }
            }
            else {
                errorMessageEl.text(errorMessage);
            }

            cellErrorMessageHolder.append(errorMessageEl);

            this._cellMessageContainer.append(cellErrorMessageHolder);

            var position = this._currentCellTdEl.offset();

            cellErrorMessageHolder.css('top', position.top + (this._currentCellTdEl.height() / 2) - cellErrorMessageHolder.height() / 2);

            var totalLeft = position.left + this._currentCellTdEl.width();
            var halfWindowWidth = window.innerWidth / 2;

            if(totalLeft > halfWindowWidth) {
                cellErrorMessageHolder.addClass('box-arrow-right');
                cellErrorMessageHolder.css('left', position.left - cellErrorMessageHolder.width() - 40);
            }
            else {
                cellErrorMessageHolder.addClass('box-arrow-left');
                cellErrorMessageHolder.css('left', totalLeft + 40);
            }
        },

        cancelCellEdit: function() {

            if(this._tableState === 'editing') {

                var rowData = this.options.data[this._currentCellEditRowIndex];
                var cellValue = this.options.data[this._currentCellEditRowIndex][this._currentCellEditColumnName];

                this.restorCellFromEdit(
                    cellValue,
                    this.getColumnModelData(this._currentCellEditColumnName),
                    this._currentCellEditRowIndex,
                    rowData
                );
            }
        },

        findRecordByPropertyValue: function(data, propertyName, propertyValue) {

            if(this._isArray(data) === false) {
                this._throwException('Data is not valid at jqGrid:findRecordByPropertyValue().', 1500, 'kernel');
            }

            if(this._isString(propertyName) === false || propertyName.length < 1) {
                this._throwException('Property name is not valid at jqGrid:findRecordByPropertyValue().', 1500, 'kernel');
            }

            if(this._isString(propertyValue) === false && isNaN(propertyValue) === false) {
                this._throwException('Property value is not valid at jqGrid:findRecordByPropertyValue().', 1500, 'kernel');
            }

            var record = null;

            for(var i = 0, len = data.length; i < len; i++) {

                record = data[i];

                if(record && record.hasOwnProperty(propertyName) && 
                    this._isObject(record[propertyName]) === false &&
                    record[propertyName].toString() === propertyValue.toString()) {
                    return record;
                }
            }

            return null;
        },

        _rowClickEventHandler: function(ev) {

            var selectedRowElement = $(ev.currentTarget);
            var tdElement = $(ev.target);

            if(tdElement.is('td') === false) {
                tdElement = tdElement.closest('.jq-grid-cell');
            }

            var columnId = tdElement.data('id');
            var rowIndex = selectedRowElement.data('index');

            var clickedTarget = $(ev.target);

            if(!columnId) {
                columnId = clickedTarget.closest('td').data('id');
            }

            this._selectRow(rowIndex, columnId, tdElement);
        },

        _headerColumnClickHandler: function(ev) {

            if(this._currentCellEditRowIndex >= 0) {
                this.cancelCellEdit();
            }

            var el = $(ev.target);

            var currentSortCol = this._tHeadEl.find('.jq-grid-column-sort');

            var columnModelData = this.getColumnModelData(el.data('id'));
            var sortColumnName = columnModelData.sortName || el.data('id');
            var sortPropertyName = columnModelData.sortPropertyName || null;

            if(currentSortCol.data('id') === el.data('id')) {
                el.toggleClass('jq-grid-column-sort-asc');
                el.toggleClass('jq-grid-column-sort-desc');
            }
            else {

                currentSortCol.removeClass('jq-grid-column-sort');
                currentSortCol.removeClass('jq-grid-column-sort-asc');
                currentSortCol.removeClass('jq-grid-column-sort-desc');

                el.addClass('jq-grid-column-sort');
                el.addClass('jq-grid-column-sort-asc');
            }


            var sortColumnOrder = el.hasClass('jq-grid-column-sort-asc') ? 'asc' : 'desc';

            this._currentSortColumnName = sortColumnName;
            this._currentSortColumnOrder = sortColumnOrder;

            if(this.options.localSort === true) {

                var compareFunctionASC = function compare(a,b) {

                    if(sortPropertyName) {

                        if (a[sortColumnName][sortPropertyName] < b[sortColumnName][sortPropertyName]) {
                            return -1;
                        }
                        
                        if (a[sortColumnName][sortPropertyName] > b[sortColumnName][sortPropertyName])
                        {
                            return 1;
                        }
                            
                        return 0;
                    }
                    else {

                        if (a[sortColumnName] < b[sortColumnName]) {
                            return -1;
                        }
                        
                        if (a[sortColumnName] > b[sortColumnName])
                        {
                            return 1;
                        }
                            
                        return 0;
                    }
                    
                };

                var compareFunctionDESC = function compare(a,b) {

                    if(sortPropertyName) {

                        if (a[sortColumnName][sortPropertyName] > b[sortColumnName][sortPropertyName]) {
                            return -1;
                        }
                        
                        if (a[sortColumnName][sortPropertyName] < b[sortColumnName][sortPropertyName])
                        {
                            return 1;
                        }

                        return 0;
                    }
                    else {

                        if (a[sortColumnName] > b[sortColumnName]) {
                            return -1;
                        }
                        
                        if (a[sortColumnName] < b[sortColumnName])
                        {
                            return 1;
                        }

                        return 0;
                    }
                    
                };

                if(sortColumnOrder === 'asc') {
                    this.options.data.sort(compareFunctionASC);
                }
                else {
                    this.options.data.sort(compareFunctionDESC);
                }

                this.removeAllRows(false);

                for(var i = 0, len = this.options.data.length; i < len; i++) {
                    this._renderRow(this.options.data[i], i);
                }
            }

            this._trigger('onSortColumnChange', null, {
                sortColumnName: sortColumnName,
                sortColumnOrder: sortColumnOrder
            });
        },

        _setOption: function(key, value, triggerEvent) {

            this._super(key, value);

            if (key === 'enableShortcuts') {
                if (value) {
                    this._enableKeyboardShortcuts();
                }
                else {
                    this._disableKeyboardShortcuts();
                }
            }
        },

        _isElementVisibleIn: function(element, parentEl) {

            var result = true;
            var delta = 0;

            var elementOffsetTop = element.offsetTop;
            var elementHeight = element.offsetHeight;
            var parentScrollTop = parentEl.scrollTop;
            var parentHeight = parentEl.offsetHeight;

            if(elementOffsetTop + elementHeight > parentScrollTop + parentHeight) {
                result = false;
                delta = (elementOffsetTop + elementHeight) - (parentScrollTop + parentHeight);
            }
            else if(elementOffsetTop < parentScrollTop) {
                result = false;
                delta = elementOffsetTop - parentScrollTop;
            }

            return {
                result: result,
                delta: delta
            };
        },

        _scrollTableWrapperToElement: function (delta, elementToScroll) {

            //add 10px for breathing
            var scrollBreathe = 20;
            var scrollHeightValue = delta;
            var scrollHeightString;

            if (delta > 0) {

                //element is below viewport
                scrollHeightValue += scrollBreathe;
                scrollHeightString = '+=' + scrollHeightValue;
            }
            else {

                //element is above viewport
                scrollHeightValue -= scrollBreathe;
                scrollHeightValue = Math.abs(scrollHeightValue);
                scrollHeightString = '-=' + scrollHeightValue;
            }

            //animate scroll
            elementToScroll.stop(true).animate({
                scrollTop: scrollHeightString
            }, 200);
        },

        _calculateScrollbarWidth: function() {

            var outer = document.createElement("div");
            outer.style.visibility = "hidden";
            outer.style.width = "100px";
            outer.style.msOverflowStyle = "scrollbar";

            document.body.appendChild(outer);

            var widthNoScroll = outer.offsetWidth;
            // force scrollbars
            outer.style.overflow = "scroll";

            // add innerdiv
            var inner = document.createElement("div");
            inner.style.width = "100%";
            outer.appendChild(inner);

            var widthWithScroll = inner.offsetWidth;

            // remove divs
            outer.parentNode.removeChild(outer);

            return widthNoScroll - widthWithScroll;
        },

        _isObject: function(object) {
            return Object.prototype.toString.call(object) === '[object Object]';
        },

        _isArray: function(arrayVar) {
            return Object.prototype.toString.call(arrayVar) === '[object Array]';
        },

        _isDate: function(date) {
            return Object.prototype.toString.call(date) === '[object Date]';
        },

        _isString: function(string) {
            return Object.prototype.toString.call(string) === '[object String]';
        },

        _isObject: function(object) {
            return Object.prototype.toString.call(object) === '[object Object]';
        },

        _isFunction: function(functionDefintion) {
            return Object.prototype.toString.call(functionDefintion) === '[object Function]';
        },

        _throwException: function(message, code) {
            throw new this._exception(message, code);
        }
    });

})(jQuery);