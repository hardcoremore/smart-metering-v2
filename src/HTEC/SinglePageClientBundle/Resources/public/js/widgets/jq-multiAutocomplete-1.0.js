/*
 * jQuery Multi Autocomplete
 *
 * @version v1.0 (24 Nov 2014)
 *
 * Copyright 2014, High Tech Engineering Center (HTEC).
 *
 * Homepage:
 * http://www.htec.rs
 *
 * Authors:
 *
 *  Caslav Sabani
 *
 * Dependencies:
 *   jQuery v1.6+
 *    jQuery UI v1.8+
 */
;(function($) {

    $.widget( "ui.multiAutocomplete", $.ui.popupWidget, {

        //default options
        options: {
            allowNew: false,
            maxItemsNumber: -1,
            inputPlaceHolder: 'Search...',
            data: [],
            autoCompleteOptions: {
                source: null,
                queryParamName: 'q',
                labelParam: 'name',
                valueParam: 'id'
            }
        },

        _listEl: null,
        _autoCompleteEl: null,
        _inputEl: null,
        _addButton: null,

        // the constructor
        _create: function() {

            if(this.element.is('input') || this.element.is('textarea') || this.element.is('select')) {

                var holder = $('<div />');
                var elementId = this.element.attr('id');

                holder.data(this.element.data());
                this.element.replaceWith(holder);
                this.element = holder;
                this.element.attr('id', elementId);
            }

            this.element.addClass('jq-multi-autocomplete');
            this.element.addClass(this.options.class);

            this._listEl = $('<ul/>', {
                'class': 'multi-autocomplete-items-holder'
            });

            this.element.append(this._listEl);

            if(Array.isArray(this.options.data) && this.options.data.length > 0) {

                var dataLen = this.options.data.length;
                var dataItem = null;

                for(var i = 0; i < dataLen; i++) {

                    dataItem = this.options.data[i];

                    if(dataItem.id) {
                        this._renderItem(dataItem.id, dataItem);
                    }
                    else {
                        this._renderItem(this.data.length + 1, dataItem);
                    }
                }
            }
            else {
                this.options.data = [];
            }

            if(this.options.allowNew) {

                this._inputEl = $('<input/>', {
                    type: 'text',
                    placeHolder: this.options.inputPlaceHolder,
                    class: 'jq-mac-search-input'
                });


                var inputItem = $('<li/>', {
                    class: 'jq-mac-input-item'
                });

                inputItem.append($('<span/>', {
                    class: 'jq-mac-item-number jq-mac-item-input-number',
                    text: '1'
                }));

                if(this.options.autoCompleteOptions.source) {

                    var self = this;

                    var autoCompleteOptions = $.extend(this.options.autoCompleteOptions, true, {

                        select: function(ev, ui) {

                            self.addItem(self.options.data.length + 1, ui.originalItem);
                            self._delay(self._resetInputEl, 25);
                        }
                    });

                    this._autoCompleteEl = $(this._inputEl).autocomplete(autoCompleteOptions);

                    inputItem.append(this._autoCompleteEl);
                }
                else {
                    inputItem.append(this._inputEl);

                    this._addButton = $('<a/>', {
                        class: 'jq-mac-item-add-btn',
                        text: '+'
                    });

                    this._on(this._addButton, {
                        click: this._addButtonClickHandler
                    });
                }

                this._on(this._inputEl, {
                    keydown: this._inputElKeyPress
                });

                inputItem.append(this._addButton);

                this._listEl.append(inputItem);
            }

            this._createPopup('#multi-autocomplete-popup-holder');
        },

        getRootElement: function() {
            return this.element;
        },

        _addButtonClickHandler: function(ev) {

            this.addItem(this.options.data.length + 1, this._inputEl.val());
            this._resetInputEl();
        },

        _inputElKeyPress: function(ev) {

            if($.ui.keyCode.ENTER === ev.keyCode && this._autoCompleteEl === null) {
                this.addItem(this.options.data.length + 1, this._inputEl.val());
                this._resetInputEl();
            }
        },

        addItem: function(id, data) {

            // can not add empty data
            if(!data || data.toString().length < 1) {
                return;
            }

            this.options.data.push(data);
            this._renderItem(id, data);

            if(this._addButton) {
                if(this.options.data.length >= this.options.maxItemsNumber) {
                    this._addButton.addClass('inactive');
                }
                else {
                    this._addButton.removeClass('inactive');
                }
            }

            this._trigger('onItemAdd', null, {
                item: data
            });
        },

        _resetInputEl: function() {

            if(this._inputEl.attr('role') === 'autocomplete') {
                this._inputEl.autocomplete('reset');
            }
            else {
                this._inputEl.val('');
            }
        },

        _renderItem: function(id, data) {

            var itemEl = $('<li/>', {
                class: 'jq-mac-item jq-mac-item-'+ id
            });

            itemEl.append($('<span/>', {
                text: this.options.data.length,
                class: 'jq-mac-item-number'
            }));


            var itemLabel = $('<span/>', {
                type: 'checkbox',
                text: this._getItemLabelFromData(data),
                'data-id': id,
                class: 'jq-mac-item-label'
            });

            itemEl.append(itemLabel);

            var itemRemoveBtn = $('<a/>', {
                class: 'jq-mac-remove-item-btn',
                text: '-'
            });

            itemEl.append(itemRemoveBtn);

            this._on(itemRemoveBtn, {
                click: this._removeItemClickHandler
            });

            this._listEl.find("li:nth-child(" + this._listEl.children().length + ")").before(itemEl);

            this._updateItemNumbers();
        },

        _removeItemClickHandler: function(ev) {

            var button = $(ev.currentTarget);
            var item = button.closest('.jq-mac-item');

            this._removeItem(item);
        },

        _updateItemNumbers: function() {
            this.element.find('.jq-mac-item-number').each(function(index){
                $(this).text(index + 1);
            });
        },

        _getItemLabelFromData: function(data) {

            if(data) {

                if(data.name) {
                    return data.name;
                }
                else if(data.serialNumber) {
                    return data.serialNumber;
                }
                else {
                    return data.toString();
                }
            }

            return '';
        },

        _removeItem: function(item) {

            var itemData = this.options.data[item.index()];

            this.options.data.splice(item.index(), 1);

            var removeButton = item.find('.jq-mac-remove-item-btn');
            this._off(removeButton, 'click');

            item.remove();

            this._updateItemNumbers();

            this._trigger('onItemRemove', null, {
                item: itemData
            });
        },

        _setOption: function(key, value, triggerEvent) {

            var that = this;

            if(key === 'data') {

                var copiedValue = null;

                if(value && Array.isArray(value)) {
                    copiedValue = value.slice();
                }

                this._super(key, copiedValue);

                this.removeAllItems();

                var dataItem;
                var dataLen = value.length;

                for(var i = 0; i < dataLen; i++) {

                    dataItem = value[i];

                    if(dataItem.id) {
                        that._renderItem(dataItem.id, dataItem);
                    }
                    else {
                        that._renderItem(this.options.data.length + 1, dataItem);
                    }
                }

                this.element.find('.jq-mac-item-input-number').text(this.options.data.length+1);
            }
            else {
                this._super(key, value);
            }
        },

        removeAllItems: function () {

            var that = this;

            this.element.find('.jq-mac-item').each(function(index){
                that._removeItem($(this));
            });
        },

        // method for popup widget event
        getSelectedData: function() {
            return this.getSelectedItems();
        },

        getSelectedValues: function() {

            var result = [];

            var valueParam = this.options.autoCompleteOptions.valueParam;

            for(var i = 0; i < this.options.data.length; i++) {

                if(this.options.data[i][valueParam]) {
                    result.push(
                        this.options.data[i][valueParam]
                    );
                }
                else {
                    result.push(this.options.data[i]);
                }
            }

            return result;
        },

        getSelectedItems: function() {
            return this.options.data.slice(0);
        },

        reset: function() {

            this.removeAllItems();
            this.element.find('.jq-mac-item-input-number').text(1);

            if(this._autoCompleteEl && this._autoCompleteEl.attr('role') === 'autocomplete') {
                this._autoCompleteEl.autocomplete('reset');
            }
            else {
                this._inputEl.val('');   
            }
        }
    });

})(jQuery);