/*
* jQuery Quick Search
*
* @version v1.0 (17 Oct 2014)
*
* Copyright 2014, High Tech Engineering Center (HTEC).
*
*
* Homepage:
* http://www.htec.rs
*
*
* Authors:
*   Časlav Šabani.
*
*
* Dependencies:
*   jQuery v1.6+
*    jQuery UI v1.8+
*/
;(function($) {

    $.widget('ui.quicksearch', {

        options: {
            min: 2
        },

        _searchInput: null,
        _resetButton: null,

        _create: function() {

            this.element.addClass('jq-quick-search');

            this._searchInput = $('<input/>', {
                type: 'text',
                placeHolder: 'Quick Search',
                'data-type': 'first',
                class: 'jq-quick-search-input'
            });

            this._resetButton = $('<span/>', {
                text: 'Reset',
                class: 'jq-quick-search-reset-button'
            });
            
            this._resetButton.hide();

            this.element.append(this._searchInput);
            this.element.append(this._resetButton);

            this._on(this._searchInput, {
                keydown: this._searchInputKeypressHandler
            });

            this._on(this._resetButton, {
                click: this._resetButtonClickHandler
            });
        },

        _searchInputKeypressHandler: function(ev) {

            if(ev.keyCode === $.ui.keyCode.ENTER && this._searchInput.val().length >= this.options.min) {
                this._resetButton.show();
                this._trigger('onQuickSearch', null, {
                    query: this._searchInput.val()
                });
            }
            else if(this._searchInput.val().length > 0 && ev.keyCode === $.ui.keyCode.ESCAPE) {
                this.resetQuickSearch();
            }
        },

        _resetButtonClickHandler: function(ev) {

            this.resetQuickSearch();
        },

        resetQuickSearch: function() {

            var that = this;

            setTimeout(function(){
                that._searchInput.val('');    
            }, 50);
            
            this._resetButton.hide();

            this._trigger('onQuickSearchReset', null);
        }
    });

})(jQuery);