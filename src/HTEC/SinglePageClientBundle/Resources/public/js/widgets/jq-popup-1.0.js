/*
 * jQuery Popup
 * Simple popup created from handlebar templates
 *
 * @version v1.0 (12 Nov 2015)
 *
 * Homepage:
 * http://www.htec.com
 *
 * Authors:
 * Caslav Sabani
 *
 * Dependencies:
 *   jQuery v1.6+
 *    jQuery UI v1.8+
 */
;(function($) {

    $.widget( "ui.popup", {

        //default options
        options: {
            popupTemplateId: '',
            popupContentTemplateId: '',
            triggerButton: null,
            compileTemplateFunction: null,
            compileTemplateData: null,
            destroyOnClose: false,
            popupContainer: '#popups-container'
        },

        _formEl: null,
        _triggerButton: null,
        _closeButton: null,
        _popupContainer: null,

        // the constructor
        _create: function() {

            if($(this.options.popupContainer).length === 0) {
                this._popupContainer = $('<div/>', {
                    'class' : 'popup-background-filter display-none',
                    'id' : 'popups-container'
                });
                $('body').append(this._popupContainer);
            }
            else{
                this._popupContainer = $('#popups-container');
            }

            this.element.addClass('jq-popup');

            this._popupContainer.append(this.element);

            var templateId = '';

            if(this.element.data('template')) {
                templateId = this.element.data('template');
            }
            else {
                templateId = this.options.popupTemplateId;
            }

            var contentTemplateId = '';

            if(this.element.data('content-template')) {
                contentTemplateId = this.element.data('content-template');
            }
            else {
                contentTemplateId = this.options.popupContentTemplateId;
            }

            if(templateId.length > 0 && typeof this.options.compileTemplateFunction === 'function') {

                var partial = {};

                if(contentTemplateId.length > 0) {
                    partial.dataName = 'content';
                    partial.templateId = contentTemplateId;
                }

                var html = this.options.compileTemplateFunction(templateId, this.options.compileTemplateData, [partial]);
                this.element.empty().append(html);
            }

            if($.hasData(this.element) && this.element.data('trigger-button') && this.element.data('trigger-button').length > 0) {
                this._triggerButton = $(this.element.data('trigger-button'));
            }
            if(typeof this.options.triggerButton === 'string') {
                this._triggerButton = $(this.options.triggerButton);
            }
            else if( typeof this.options.triggerButton === 'object') {
                this._triggerButton = this.options.triggerButton
            }
            
            this._formEl = this.element.find('.popup-form');

            if(this._formEl.length > 0) {
                this._on(this.element.find('[data-role="popup-form-button"]'),
                {
                    click: this._formButtonClickHandler,
                });

                this._on(this._formEl, {
                    submit: this._formSubmittedEventHandler
                });
            }

            if(this._triggerButton instanceof Array) {
                for(var i in this._triggerButton) {
                    this._on(this._triggerButton[i], {
                        click: this._triggerButtonClickHandler
                    });
                }
            }
            else {
                this._on(this._triggerButton, {
                    click: this._triggerButtonClickHandler
                });    
            }

            this._closeButton = this.element.find('.popup-close-button').first();

            this._on(this._closeButton, {
                click: this._closeButtonClickHandler
            });
        },

        _triggerButtonClickHandler: function() {
            this.open();
        },

        _closeButtonClickHandler: function() {
            this.close();
        },

        _formButtonClickHandler: function(ev) {

            var button = $(ev.currentTarget);

            if(button.data('intention') === 'submitForm') {
                this._triggerPopupFormSubmit();
            }
            else {
                this._trigger('onFormCanceled');
            }
        },

        _formSubmittedEventHandler: function() {
            this._triggerPopupFormSubmit();
            return false;
        },

        _triggerPopupFormSubmit: function() {

            var formData = {};

            this.element.find("input[name], textarea[name], select[name]").each(function(index){

                var input = $(this);
                var fieldName = input.attr('name');

                formData[fieldName] = input.val();
            });

            this._trigger('onFormSubmitted', null, {
                formData: formData
            });
        },

        open: function() {

            this._trigger('onPopupOpen', null, {
                popup: this,
                triggerButton: this._triggerButton
            });

            this._popupContainer.removeClass('display-none');
            this.element.removeClass('fadeOutUp display-none');
            this.element.addClass('fadeInDown');

            $('body').css({
                'height' : '100%',
                'overflow' : 'hidden'
            });
        },

        close: function() {

            this._trigger('onPopupClose', null, {
                popup: this,
                triggerButton: this._triggerButton
            });

            this.element.removeClass('fadeInDown');
            this.element.addClass('fadeOutUp');

            $('body').css({
                'height' : 'auto',
                'overflow' : 'visible'
            });

            var self = this;

            setTimeout(function() {

                self.element.addClass('display-none');
                self._popupContainer.addClass('display-none');

                if(self.options.destroyOnClose === true) {
                    self.element.remove();
                }

            }, 350);
        },

        _destroy: function() {

            this._off(this._closeButton, 'click');

            if(this._formEl.length > 0) {
                this._off(this._formEl, 'click');
                this._off(this._formEl, 'submit');
            }

            this.element.empty();
            this._super();
            this.element.removeData();
            this.element = null;
        }
    });

})(jQuery);