/*
* jQuery Paginator
*
* @version v1.0 (17 Oct 2014)
*
* Copyright 2014, High Tech Engineering Center (HTEC).
*
*
* Homepage:
* http://www.htec.rs
*
*
* Authors:
*   Časlav Šabani.
*
*
* Dependencies:
*   jQuery v1.6+
*    jQuery UI v1.8+
*/
;(function($) {

    $.widget('ui.paginator', {

        options: {
            pageNumber: 0,
            totalPageCount: 0,
            rowsPerPage: 10,
            totalRowsCount: 0,
            rowsCount: 0,
            firstPageBtn: null,
            lastPageBtn: null,
            previousPageBtn: null,
            nextPageBtn: null,
            pageNumberInputEl: null,
            totalPageCountLabel: null
        },

        _create: function() {

            this.element.addClass('jq-paginator');

            this.options.firstPageBtn = $('<span/>', {
                title: 'First page',
                html: '&laquo;',
                'data-type': 'first',
                class: 'jq-paginator-button jq-paginator-first-page button buttonWhite'
            });

            this.options.lastPageBtn = $('<span/>', {
                title: 'Last page',
                html: '&raquo;',
                'data-type': 'last',
                class: 'jq-paginator-button jq-paginator-last-page button buttonWhite'
            });

            this.options.previousPageBtn = $('<span/>', {
                title: 'Previous page',
                html: '&lsaquo;',
                'data-type': 'previous',
                class: 'jq-paginator-button jq-paginator-previous-page button buttonWhite'
            });

            this.options.nextPageBtn = $('<span/>', {
                title: 'Next page',
                html: '&rsaquo;',
                'data-type': 'next',
                class: 'jq-paginator-button jq-paginator-next-page button buttonWhite'
            });

            this.options.pageNumberWrapper = $('<div/>', { class: 'jq-paginator-page-number-wrapper' });
                this.options.pageNumberInputEl = $('<input/>', {
                    value: this.options.pageNumber,
                    'data-type': 'page',
                    class: 'jq-paginator-page-number',
                    type: 'text',
                    maxlength: 8
                });
                this.options.totalPageCountLabel = $('<label/>', {
                    text: ' of 1',
                    'data-type': 'label',
                    class: 'jq-paginator-page-label'
                });
                this.options.pageNumberWrapper.append(this.options.pageNumberInputEl);
                this.options.pageNumberWrapper.append(this.options.totalPageCountLabel);

            this.element.append(this.options.firstPageBtn);
            this.element.append(this.options.previousPageBtn);
            this.element.append(this.options.pageNumberWrapper);
            this.element.append(this.options.nextPageBtn);
            this.element.append(this.options.lastPageBtn);

            this._on(this.element, {
                click: this._paginatorClickHandler
            });

            this._on(this.options.pageNumberInputEl, {
                keydown: this._pageNumberInputChangeHandler
            });

            this._checkControls(1);
        },

        _setOption: function(key, value, triggerEvent) {

            this._super(key, value);

            if(key === 'totalPageCount') {
                this.options.totalPageCountLabel.text(' of ' + value);
                this._checkControls();
            }
            else if(key === 'pageNumber') {

                if(typeof value === 'object') {

                    this.options.pageNumberInputEl.val(value.pageNumber);

                    if(value.triggerEvent) {
                        this._submitNewPage(value.pageNumber);
                    }
                }
                else {
                    this.options.pageNumberInputEl.val(value);
                }

                this._checkControls();
            }
        },

        _paginatorClickHandler: function(ev) {

            var el = $(ev.target);

            if(el.hasClass('inactive') || el.hasClass('jq-paginator-page-label')) {
                return;
            }

            var pageNumberToGo = this.options.pageNumber;

            switch(el.data('type')) {

                case 'first':
                    pageNumberToGo = 1;
                break;

                case 'previous':
                    pageNumberToGo -= 1;
                break;

                case 'next':
                    pageNumberToGo += 1;
                break;

                case 'last':
                    pageNumberToGo = this.options.totalPageCount;
                break;

                case 'page':
                    return;
                break;
            }

            this._submitNewPage(pageNumberToGo);
        },

        _pageNumberInputChangeHandler: function(ev) {

            // return key
            if(ev.keyCode === 8) {
                return true;
            }

            var value = $(ev.target).val();
            var newCharacter = String.fromCharCode(ev.keyCode);
            var newPageNumber = Number(value + newCharacter);

            if(ev.keyCode === $.ui.keyCode.ENTER && newPageNumber !== this.options.pageNumber) {
                this._submitNewPage(newPageNumber);
            }
            else if(isNaN(newPageNumber)) {
                return false;
            }
        },

        _submitNewPage: function(pageNumber) {


            if(pageNumber > 0 && pageNumber <= this.options.totalPageCount) {

                this.options.pageNumber = pageNumber;
                this.options.pageNumberInputEl.val(pageNumber);

                this._trigger('onPageChange', null, {
                    pageNumber: pageNumber,
                    rowsPerPage: this.options.rowsPerPage
                });

                this._checkControls(pageNumber);
            }
        },

        _checkControls: function(pageNumber) {

            if(!pageNumber) {
                pageNumber = this.options.pageNumber;
            }

            if(pageNumber === this.options.totalPageCount || this.options.totalPageCount < 2) {
                this.options.nextPageBtn.addClass('inactive');
                this.options.lastPageBtn.addClass('inactive');
            }
            else {
                this.options.nextPageBtn.removeClass('inactive');
                this.options.lastPageBtn.removeClass('inactive');
            }

            if(pageNumber === 1 || this.options.totalPageCount < 1) {
                this.options.firstPageBtn.addClass('inactive');
                this.options.previousPageBtn.addClass('inactive');
            }
            else {
                this.options.firstPageBtn.removeClass('inactive');
                this.options.previousPageBtn.removeClass('inactive');
            }
        }
    });

})(jQuery);