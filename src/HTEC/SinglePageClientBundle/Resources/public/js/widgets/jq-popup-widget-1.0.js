/*
 * jQuery Popup widget
 *
 * @version v1.0 (15 May 2016)
 *
 * Copyright 2014, High Tech Engineering Center (HTEC).
 *
 * Homepage:
 * http://www.htec.rs
 *
 * Authors:
 *
 *  Caslav Sabani
 *
 * Dependencies:
 *   jQuery v1.6+
 *    jQuery UI v1.8+
 */
;(function($) {

    $.widget( "ui.popupWidget", {

        //default options
        options: {
            isPopup: false,
        },

        _popupHolder: null,
        _popupSaveButton: null,
        _popupCancelButton: null,
        _id: '',

        _createPopup: function(popupHolderSelector) {

            this._id = 'popup-widget-' + Math.round(Math.random() * 9999999999);
            
            var elementId = this.element.attr('id');

            if(elementId === 'undefined' || (elementId && elementId.length === 0)) {
                this.element.attr('id', this._id);
            }

            if(this.options.isPopup === true) {

                var position = this.element.offset();
                var popupHolder = $('body').first().find(popupHolderSelector).first();

                if(popupHolder.length === 1) {

                    this._popupHolder = popupHolder;
                    this._popupHolder.append(this.element);
                }
                else {

                    if(popupHolderSelector.indexOf('.') !== -1) {
                        this._popupHolder = $('<div/>', {
                            'class': popupHolderSelector.substr(1) + ' popup-widget-holder'
                        });
                    }
                    else {
                        this._popupHolder = $('<div/>', {
                            'id': popupHolderSelector.substr(1),
                            'class': 'popup-widget-holder'
                        });
                    }

                    $('body').first().append(this._popupHolder);

                    this._popupHolder.append(this.element);
                }

                this._popupHolder.css('left', position.left);
                this._popupHolder.css('top', position.top);

                this._createPopupControls();

                this.open();
            }
        },

        _createPopupControls: function() {

            var controlsHolder = $('<div/>', {
                'class': 'popup-contros-holder checkboxgroup-controls'
            });

            this._popupCancelButton = $('<div/>', {
                'class': 'popup-button popup-cancel-button',
                text: 'Cancel'
            });

            this._popupSaveButton = $('<div/>', {
                'class': 'popup-button popup-save-button',
                text: 'Save'
            });

            this._on(this._popupCancelButton, {
                click: this._cancelButtonClickHandler
            });

            this._on(this._popupSaveButton, {
                click: this._saveButtonClickHandler
            });

            controlsHolder.append(
                this._popupCancelButton,
                this._popupSaveButton
            );

            this.element.append(controlsHolder);
        },

        _cancelButtonClickHandler: function(ev) {

            this.close();

            this._trigger('onCancel', null, {
                selectedItems: this.getSelectedData()
            });
        },

        _saveButtonClickHandler: function(ev) {

            this.close();

            this._trigger('onSave', null, {
                selectedItems: this.getSelectedData()
            });
        },

         open: function() {
            if(this.options.isPopup) {
                this._popupHolder.show();
                this.element.show();
            }
        },

        close: function() {
            if(this.options.isPopup) {
                this._popupHolder.hide();
                this.element.hide();
            }
        },

        destroy: function () {

            this._super();

            if(this._popupCancelButton) {
                this._off(this._popupCancelButton);
            }

            if(this._popupSaveButton) {
                this._off(this._popupSaveButton);
            }

            if(this.options.isPopup) {
                this.element.remove();
            }

            this._popupHolder.hide();
        }

    });

})(jQuery);