/*
* jQuery Autocomplete
*
* @version v1.0 (17 Oct 2014)
*
* Copyright 2014, High Tech Engineering Center (HTEC).
*
*
* Homepage:
* http://www.htec.rs
*
*
* Authors:
*   Časlav Šabani.
*
*
* Dependencies:
*   jQuery v1.6+
*    jQuery UI v1.8+
*
*  
* This is MODIFIED version of jQuery UI automplete widget.
*
*/

$.widget( "ui.autocomplete", $.ui.autocomplete, {

    version: "1.0",

    options: {
        queryParamName: "term",
        valueParam: "value",
        labelParam: "label",
        ajaxQueryParams: null
    },

    requestIndex: 0,
    pending: 0,

    _loaderTimeout: 0,    

    setSelectedItem: function(item) {
        this.selectedItem = item;
        this.element.val(item.label);
    },

    setSelectedOriginalItem: function(item) {

        this.selectedOriginalItem = item;

        if(item) {
            this.element.addClass('item-selected');
            this.element.val(this._getItemLabel(item));
        }
        else {
            this.element.removeClass('item-selected');
            this.element.val('');
        }
    },

    getSelectedItem: function() {
        return this.selectedItem;
    },

    getSelectedOriginalItem: function() {
        return this.selectedOriginalItem;
    },

    reset: function() {
        this.selectedItem = null;
        this.selectedOriginalItem = null;
        this.element.removeClass('item-selected');
        this.element.val('');
        this._closeMenu();
    },

    _create: function() {
        // Some browsers only repeat keydown events, not keypress events,
        // so we use the suppressKeyPress flag to determine if we've already
        // handled the keydown event. #7269
        // Unfortunately the code for & in keypress is the same as the up arrow,
        // so we use the suppressKeyPressRepeat flag to avoid handling keypress
        // events when we know the keydown event was used to modify the
        // search term. #7799
        var suppressKeyPress, suppressKeyPressRepeat, suppressInput,
            nodeName = this.element[ 0 ].nodeName.toLowerCase(),
            isTextarea = nodeName === "textarea",
            isInput = nodeName === "input";

        this.isMultiLine =
            // Textareas are always multi-line
            isTextarea ? true :
            // Inputs are always single-line, even if inside a contentEditable element
            // IE also treats inputs as contentEditable
            isInput ? false :
            // All other element types are determined by whether or not they're contentEditable
            this.element.prop( "isContentEditable" );

        this.valueMethod = this.element[ isTextarea || isInput ? "val" : "text" ];
        this.isNewMenu = true;

        this.element
            .addClass( "ui-autocomplete-input" )
            .attr( "autocomplete", "off" )
            .attr( "role", "autocomplete" );

        this._on( this.element, {
            keydown: function( event ) {
                if ( this.element.prop( "readOnly" ) ) {
                    suppressKeyPress = true;
                    suppressInput = true;
                    suppressKeyPressRepeat = true;
                    return;
                }

                suppressKeyPress = false;
                suppressInput = false;
                suppressKeyPressRepeat = false;
                var keyCode = $.ui.keyCode;
                switch ( event.keyCode ) {
                case keyCode.PAGE_UP:
                    suppressKeyPress = true;
                    this._move( "previousPage", event );
                    break;
                case keyCode.PAGE_DOWN:
                    suppressKeyPress = true;
                    this._move( "nextPage", event );
                    break;
                case keyCode.UP:
                    suppressKeyPress = true;
                    this._keyEvent( "previous", event );
                    break;
                case keyCode.DOWN:
                    suppressKeyPress = true;
                    this._keyEvent( "next", event );
                    break;
                case keyCode.ENTER:
                    // when menu is open and has focus
                    if ( this.menu.active ) {
                        // #6055 - Opera still allows the keypress to occur
                        // which causes forms to submit
                        suppressKeyPress = true;
                        event.preventDefault();
                        this.menu.select( event );
                    }
                    break;
                case keyCode.TAB:
                    if ( this.menu.active ) {
                        this.menu.select( event );
                    }
                    break;
                case keyCode.ESCAPE:
                    if ( this.menu.element.is( ":visible" ) ) {

                        if ( !this.isMultiLine ) {
                            this._value( this[this.options.queryParamName] );
                        }

                        this.close( event );
                        // Different browsers have different default behavior for escape
                        // Single press can mean undo or clear
                        // Double press in IE means clear the whole form
                        event.preventDefault();
                    }
                    break;
                default:

                    suppressKeyPressRepeat = true;

                    // skip search for ctrl and shift
                    if(event.keyCode !== 16 && event.keyCode !== 17 && event.ctrlKey === false) {
                        // search timeout should be triggered before the input value is changed
                        this._searchTimeout( event );    
                    }
                    
                    break;
                }
            },
            keypress: function( event ) {
                if ( suppressKeyPress ) {
                    suppressKeyPress = false;
                    if ( !this.isMultiLine || this.menu.element.is( ":visible" ) ) {
                        event.preventDefault();
                    }
                    return;
                }
                if ( suppressKeyPressRepeat ) {
                    return;
                }

                // replicate some key handlers to allow them to repeat in Firefox and Opera
                var keyCode = $.ui.keyCode;
                switch ( event.keyCode ) {
                case keyCode.PAGE_UP:
                    this._move( "previousPage", event );
                    break;
                case keyCode.PAGE_DOWN:
                    this._move( "nextPage", event );
                    break;
                case keyCode.UP:
                    this._keyEvent( "previous", event );
                    break;
                case keyCode.DOWN:
                    this._keyEvent( "next", event );
                    break;
                }
            },
            input: function( event ) {
                if ( suppressInput ) {
                    suppressInput = false;
                    event.preventDefault();
                    return;
                }
                this._searchTimeout( event );
            },

            blur: function( event ) {
               this._closeMenu();
            }
        });

        this._initSource();
        this.menu = $( "<ul>" )
            .addClass( "ui-autocomplete ui-front" )
            .appendTo( this._appendTo() )
            .menu({
                // disable ARIA support, the live region takes care of that
                role: null
            })
            .hide()
            .menu( "instance" );

        this._on( this.menu.element, {
            mousedown: function( event ) {
                // prevent moving focus out of the text field
                event.preventDefault();

                // IE doesn't prevent moving focus even with event.preventDefault()
                // so we set a flag to know when we should ignore the blur event
                this.cancelBlur = true;
                this._delay(function() {
                    delete this.cancelBlur;
                });

                // clicking on the scrollbar causes focus to shift to the body
                // but we can't detect a mouseup or a click immediately afterward
                // so we have to track the next mousedown and close the menu if
                // the user clicks somewhere outside of the autocomplete
                var menuElement = this.menu.element[ 0 ];
                if ( !$( event.target ).closest( ".ui-menu-item" ).length ) {
                    this._delay(function() {
                        var that = this;
                        this.document.one( "mousedown", function( event ) {
                            if ( event.target !== that.element[ 0 ] &&
                                    event.target !== menuElement &&
                                    !$.contains( menuElement, event.target ) ) {
                                that.close();
                            }
                        });
                    });
                }
            },
            menufocus: function( event, ui ) {
                var label, 
                    item,
                    originalItem;

                // support: Firefox
                // Prevent accidental activation of menu items in Firefox (#7024 #9118)
                if (this.isNewMenu) {
                    this.isNewMenu = false;
                    if(event.originalEvent && /^mouse/.test(event.originalEvent.type)) {
                        this.menu.blur();

                        this.document.one("mousemove", function() {
                            $(event.target).trigger(event.originalEvent);
                        });

                        return;
                    }
                }

                item = ui.item.data("ui-autocomplete-item");
                originalItem = ui.item.data("ui-autocomplete-original-item");

                if( false !== this._trigger( "focus", event, {
                            item: item,
                            originalItem: originalItem
                        })
                ) {
                    // use value to match what will end up in the input, if it was a key event
                    if(event.originalEvent && /^key/.test(event.originalEvent.type)) {
                        this._value(item.label);
                    }
                }

                // Announce the value in the liveRegion
                label = ui.item.attr( "aria-label" ) || item.value;

                if( label && $.trim( label ).length ) {

                    this.liveRegion.children().hide();
                    $("<div>").text(label).appendTo(this.liveRegion);
                }
            },

            menuselect: function( event, ui ) {

                var item = ui.item.data("ui-autocomplete-item"),
                    originalItem = ui.item.data("ui-autocomplete-original-item"),
                    previous = this.previous;

                // only trigger when focus was lost (click on menu)
                if(this.element[0] !== this.document[0].activeElement) {

                    this.element.focus();
                    this.previous = previous;

                    // #6109 - IE triggers two focus events and the second
                    // is asynchronous, so we need to reset the previous
                    // term synchronously and asynchronously :-(
                    this._delay(function() {
                        this.previous = previous;
                        this.selectedItem = item;
                        this.selectedOriginalItem = originalItem;
                    });
                }

                this.selectedItem = item;
                this.selectedOriginalItem = originalItem;

                var isItemSelected = this._trigger( "select", event, { 
                    item: item,
                    originalItem: originalItem
                });                

                if(isItemSelected !== false) {
                    this._value(item.label);
                    this.element.addClass('item-selected');
                }
                else {
                    this.selectedItem = null;
                    this.selectedOriginalItem = null;

                    this.element.removeClass('item-selected');
                }

                // reset the term after the select event
                // this allows custom select handling to work properly
                this[this.options.queryParamName] = this._value();
                this.close(event);
            }
        });

        this.liveRegion = $( "<span>", {
                role: "status",
                "aria-live": "assertive",
                "aria-relevant": "additions"
            })
            .addClass( "ui-helper-hidden-accessible" )
            .appendTo( this.document[ 0 ].body );

        // turning off autocomplete prevents the browser from remembering the
        // value when navigating through history, so we re-enable autocomplete
        // if the page is unloaded before the widget is destroyed. #7790
        this._on( this.window, {
            beforeunload: function() {
                this.element.removeAttr( "autocomplete" );
            }
        });
    },

    _ajaxUrl: '',
    _sourceData: [],

    _initSource: function() {

        var array, url,
            that = this;

        if($.isArray(this.options.source)) {

            this._sourceData = this.options.source;
            this.source = this._sourceArrayFunction;
        } 
        else if(typeof this.options.source === "string") {

            this._ajaxUrl = this.options.source;
            this.source = this._sourceAjaxFunction;
        }
        else {
            this.source = this.options.source;
        }
    },

    _sourceArrayFunction: function(request, response) {

        response( 
            this._filter(
                this._sourceData, 
                request[this.options.queryParamName]
            )
        );
    },

    _sourceAjaxFunction: function(request, response) {

        if(this.xhr) {
            this.xhr.abort();
        }

        this.xhr = $.ajax({

            url: this._ajaxUrl,
            data: request,
            dataType: "json",

            success: function(data) {
                response(data);
            },

            error: function() {
                response([]);
            }
        });
    },

    _searchTimeout: function( event ) {
        clearTimeout( this.searching );
        this.searching = this._delay(function() {

            // Search if the value has changed, or if the user retypes the same value (see #7434)
            var equalValues = this.options.queryParamName === this._value(),
                menuVisible = this.menu.element.is( ":visible" ),
                modifierKey = event.altKey || event.ctrlKey || event.metaKey || event.shiftKey;

            if ( !equalValues || ( equalValues && !menuVisible && !modifierKey ) ) {
                this.selectedItem = null;
                this.selectedOriginalItem = null;
                this.search( null, event );
            }
        }, this.options.delay );
    },

    search: function( value, event ) {
        value = value != null ? value : this._value();

        // always save the actual value, not the one passed as an argument
        this[this.options.queryParamName] = this._value();

        if ( value.length < this.options.minLength ) {
            return this.close( event );
        }

        if ( this._trigger( "search", event ) === false ) {
            return;
        }

        return this._search( value );
    },

    _search: function( value ) {

        this.pending++;

        clearTimeout(this._loaderTimeout);

        var self = this;

        this._loaderTimeout = setTimeout(function(){
            self.element.addClass("ui-autocomplete-loading");
        }, 300);
        
        this.cancelSearch = false;

        var data = this.options.ajaxQueryParams || {};
            data[this.options.queryParamName] = value;

        this.element.removeClass('item-selected');
        this.source(data, this._response());
    },

    _response: function() {

        var index = ++this.requestIndex;

        return $.proxy(function(content) {

            if(index === this.requestIndex) {
                this.__response(content);
            }

            this.pending--;

            if(!this.pending) {
                this.element.removeClass( "ui-autocomplete-loading" );
            }

        }, this);
    },

    __response: function(content) {

        clearTimeout(this._loaderTimeout);

        var normalizedContent = null;
        var originalContent = content;

        if(content) {
            normalizedContent = this._normalize(content);
        }

        this._trigger(
            "response",
            null,
            {
                normalizedContent: normalizedContent,
                originalContent: originalContent
            }
        );

        if( !this.options.disabled && originalContent && originalContent.length && !this.cancelSearch ) {

            this._suggest(normalizedContent, originalContent);
            this._trigger("open");
        } 
        else {
            // use ._close() instead of .close() so we don't cancel future searches
            this._close();
        }
    },

    _change: function( event ) {
        if(this.previous !== this._value()) {
            this._trigger("change", event, { item: this.selectedItem, originalItem: this.selectedOriginalItem } );
        }
    },

    _normalize: function( items ) {

        // check if default formatting is set and return if true
        if(items.length && items[0]["label"] && items[0]["value"]) {
            return items;
        }

        var returnObj = {};
        var that = this;

        return $.map( items, function( item ) {

            if(typeof item === "string") {

                returnObj.label = item;
                returnObj.value = item;

                return returnObj;
            }

            returnObj = {};

            returnObj.label = that._getItemLabel(item);
            returnObj.value = that._getItemValue(item);

            return returnObj;
        });
    },

    _getItemLabel: function(item) {
        
        var label = "";

        if(typeof this.options.labelParam === "function") {
            label = this.options.labelParam(item);
        }
        else {
            label = item[this.options.labelParam];
        }

        return label;
    },

    _getItemValue: function(item) {
        
        var value = "";

        if(typeof this.options.valueParam === "function") {
            value = this.options.valueParam(item);
        }
        else {
            value = item[this.options.valueParam];
        }

        return value;
    },

    _suggest: function(normalizedItems, originalItems) {

        var ul = this.menu.element.empty();
        this._renderMenu(ul, normalizedItems, originalItems);
        this.isNewMenu = true;
        this.menu.refresh();

        // size and position menu
        ul.show();
        this._resizeMenu();
        ul.position( $.extend({
            of: this.element
        }, this.options.position ) );

        if ( this.options.autoFocus ) {
            this.menu.next();
        }
    },

    _renderMenu: function(ul, normalizedItems, originalItems) {
        var that = this;
        $.each(normalizedItems, function(index, item) {
            that._renderItemData(ul, item, originalItems[index]);
        });
    },

    _closeMenu: function() {
        if(this.menu.element.is(":visible")) {
            this.menu.element.hide();
            this.menu.blur();
            this.isNewMenu = true;
        }
    },

    _renderItemData: function(ul, normalizedItem, originalItem) {

        var renderedItem = this._renderItem(ul, normalizedItem, originalItem);
            renderedItem.data("ui-autocomplete-item", normalizedItem);
            renderedItem.data("ui-autocomplete-original-item", originalItem);

        return renderedItem;
    },

    _renderItem: function( ul, item ) {
        return $( "<li>" ).text(item.label).appendTo(ul);
    },

    _move: function( direction, event ) {
        
        if ( !this.menu.element.is( ":visible" ) ) {
            this.search( null, event );
            return;
        }

        if ( this.menu.isFirstItem() && /^previous/.test( direction ) ||
                this.menu.isLastItem() && /^next/.test( direction ) ) {

            if ( !this.isMultiLine ) {
                this._value( this[this.options.queryParamName] );
            }

            this.menu.blur();
            return;
        }

        this.menu[ direction ]( event );
    },

    _keyEvent: function( keyEvent, event ) {
        if ( !this.isMultiLine || this.menu.element.is( ":visible" ) ) {
            this._move( keyEvent, event );

            // prevents moving cursor to beginning/end of the text field in some browsers
            event.preventDefault();
        }
    },

    _escapeRegex: function( value ) {
        return value.replace( /[\-\[\]{}()*+?.,\\\^$|#\s]/g, "\\$&" );
    },
    
    _filter: function( array, term ) {

        var matcher = new RegExp( this._escapeRegex(term), "i" );
        var that = this;

        return $.grep(array, function(value) {
            return matcher.test(that._getItemLabel(value) || that._getItemValue(value) || value);
        });
    }
});

// live region extension, adding a `messages` option
// NOTE: This is an experimental API. We are still investigating
// a full solution for string manipulation and internationalization.
$.widget( "ui.autocomplete", $.ui.autocomplete, {
    options: {
        messages: {
            noResults: "No search results.",
            results: function( amount ) {
                return amount + ( amount > 1 ? " results are" : " result is" ) +
                    " available, use up and down arrow keys to navigate.";
            }
        }
    },

    __response: function(content) {

        var message;

        this._superApply(arguments);

        if(this.options.disabled || this.cancelSearch) {
            return;
        }

        if(content && content.length) {
            message = this.options.messages.results(content.length);
        } 
        else {
            message = this.options.messages.noResults;
        }

        this.liveRegion.children().hide();

        $("<div>").text(message).appendTo(this.liveRegion);
    }
});

var autocomplete = $.ui.autocomplete;