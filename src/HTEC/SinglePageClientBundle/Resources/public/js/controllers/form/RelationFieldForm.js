HCM.define({

    name: 'Controllers.RelationFieldForm',

    extendFrom: 'Controllers.Form.BaseForm',

    construct: function(app, module) {

        this.callConstructor('Controllers.Form.BaseForm', [app, module]);

        var self = this;

        this.relationDataLoadCompleteEventHandler = function(ev) {

            var scope = ev.scope;

            if(scope.module === self.getModule() && scope.fieldModelData.name === ev.data.fieldModelData.name) {

                var relationFormFieldInput = self.getModule().getFormFieldByName(scope.fieldModelData.name);

                if(self.isArray(ev.responseData) && ev.responseData.length > 0) {

                    self.getModule().setRelationFieldSelectData(scope.fieldModelData.name, ev.responseData);
                    self.getModule().resetRelationFormField(scope.fieldModelData, relationFormFieldInput);
                    self.getModule().initializeRelationFormField(scope.fieldModelData, ev.responseData, relationFormFieldInput);

                    if(self.getModule().getFormMode() === 'edit') {

                        var editRecordData = self.getModule().getEditingRecordData();

                        if(editRecordData && editRecordData.hasOwnProperty(scope.fieldModelData.name)) {

                            self.getModule().setFormFieldValue(
                                scope.fieldModelData.name,
                                editRecordData[scope.fieldModelData.name],
                                relationFormFieldInput
                            );
                        }
                    }
                }
            }
        };

        this.relationDataLoadErrorEventHandler = function(ev) {
            self.getApp().showNotification('error', "Relational data load error. Editing or creating might not work properly");
        };
    },

    methods: {

        addEvents: function() {
            this.super('addEvents');
            this.addRelationFieldModelEvents();
        },

        removeEvents: function() {
            this.super('removeEvents');
            this.removeRelationFieldModelEvents();
        },

        startController: function() {

            this.super('startController');

            var relationFields = this.getModule().getModelRelationFields();
            var fieldModelData, relationFormField, isFieldInitialized, isNotAutoLoading, relationFieldSelectData;

            var editRecordData = this.getModule().getEditingRecordData();
            var isEditForm = this.getModule().getFormMode() === 'edit';

            for(var i = 0, len = relationFields.length; i < len; i++) {

                fieldModelData = relationFields[i];

                relationFormField = this.getModule().getFormFieldByName(fieldModelData.name);

                isFieldInitialized = this.getModule().isRelationFormFieldInitialized(fieldModelData, relationFormField);

                isRelationFormFieldDataAutoLoading = this.getModule().getNotAutoLoadingRelationFields().indexOf(fieldModelData.name) === -1;

                relationFieldSelectData = this.getModule().getRelationFieldSelectData(fieldModelData.name);
                
                if(isFieldInitialized === false) {

                    if(relationFieldSelectData) {

                        this.getModule().initializeRelationFormField(fieldModelData, relationFieldSelectData, relationFormField);

                        if(isEditForm) {

                            // set selected value
                            if(editRecordData && editRecordData.hasOwnProperty(fieldModelData.name)) {

                                this.getModule().setFormFieldValue(
                                    fieldModelData.name,
                                    editRecordData[fieldModelData.name],
                                    this.getModule().getFormFieldByName(fieldModelData.name)
                                );
                            }
                        }
                    }
                    // if select data does not exists and field is auto loading load select data
                    else if(isRelationFormFieldDataAutoLoading) {
                        this.getModule().loadRelationFormFieldData(fieldModelData);
                    }
                }
                // if field is already initialized and is autoloading check new data from model monitor data
                else if(isRelationFormFieldDataAutoLoading) {
                    this.getModule().updateFieldFromMonitorData(fieldModelData);
                }
            }
        },

        addRelationFieldModelEvents: function() {

            var relationFields = this.getModule().getModelRelationFields();
            var fieldModelData;

            for(var i = 0, len = relationFields.length; i < len; i++) {
                this.addRelationFieldEvent(relationFields[i]);
            }
        },

        removeRelationFieldModelEvents: function() {

            var relationFields = this.getModule().getModelRelationFields();
            var fieldModelData;

            for(var i = 0, len = relationFields.length; i < len; i++) {
                this.removeRelationFieldEvent(relationFields[i]);
            }
        },

        addRelationFieldEvent: function(fieldModelData) {

            if(this.getModule().getNotAutoLoadingRelationFields().indexOf(fieldModelData.name) === -1) {

                $(document).on(
                    fieldModelData.relationModel.events.READ_FOR_SELECT_COMPLETE,
                    {fieldModelData: fieldModelData},
                    this.relationDataLoadCompleteEventHandler
                );
                
                $(document).on(
                    fieldModelData.relationModel.events.READ_FOR_SELECT_ERROR,
                    {fieldModelData: fieldModelData},
                    this.relationDataLoadErrorEventHandler
                );

                $(document).on(
                    fieldModelData.relationModel.events.CREATE_COMPLETE,
                    {fieldModelData: fieldModelData},
                    this.relationDataCreateCompleteEventHandler
                );

                $(document).on(
                    fieldModelData.relationModel.events.UPDATE_COMPLETE,
                    {fieldModelData: fieldModelData},
                    this.relationDataUpdateCompleteEventHandler
                );
            }
        },

        removeRelationFieldEvent: function(fieldModelData) {

            if(this.getModule().getNotAutoLoadingRelationFields().indexOf(fieldModelData.name) === -1) {

                $(document).off(fieldModelData.relationModel.events.READ_FOR_SELECT_COMPLETE, this.relationDataLoadCompleteEventHandler);
                $(document).off(fieldModelData.relationModel.events.READ_FOR_SELECT_ERROR, this.relationDataLoadErrorEventHandler);

                $(document).off(fieldModelData.relationModel.events.CREATE_COMPLETE, this.relationDataCreateCompleteEventHandler);
                $(document).off(fieldModelData.relationModel.events.UPDATE_COMPLETE, this.relationDataUpdateCompleteEventHandler);
            }
        }
    }
});