HCM.define({

    name: 'Controllers.Form.BaseForm',

    extendFrom: 'Base.Controller',

    construct: function(app, module) {

		this.callConstructor('Base.Controller', [app, module]);

        var self = this;

    	this.formChangedEventHandler = function(ev) {

    		if(ev.notifyFormForChange !== false) {
                self.getModule().setIsFormChanged(true);
            }
            
            var constants = self.getNamespaceValue('Static.Constants');
            var formFieldIinput = $(ev.currentTarget);
            var formFieldName = formFieldIinput.attr('name');

            var e = $.Event(
                constants.FORM_INPUT_CHANGED_EVENT,
                {
                    module: self.getModule(),
                    formFieldName: formFieldName
                }
            );

            $(document).trigger(e);
    	};

    	this.formSubmittedEventHandler = function(ev) {

            var button = $(ev.currentTarget);
            var role = button.data('role');

            var formData = self.getModule().getFormData(true);
            var formMode = self.getModule().getFormMode();

            self.submitForm(formData, formMode, role);

            // stop native from submitting
            return false;
    	};

        this.formResetPopupSubmitHandler = function(ev) {

            self.getModule().getFormResetPopup().popup('close');
            self.getModule().resetForm();
        };

        this.formResetPopupCanceldHandler = function(ev) {

        };

        this.formResetButtonClickHandler = function() {

            if(self.getModule().getFormResetPopup() && self.getModule().getIsFormChanged()) {
                self.getModule().getFormResetPopup().popup('open');
            }
            else {
                self.getModule().resetForm();
            }
        };

        this.formClosePopupSubmitHandler = function(ev) {
            self.getModule().getFormClosePopup().popup('close');
            self.getModule().stopModule();
        };

        this.formClosePopupCanceldHandler = function(ev) {
            self.getModule().setModuleAskForStopCallback(null);
        };

        this.formCloseButtonClickHandler = function(ev) {

            if(self.getModule().getFormClosePopup() && self.getModule().getIsFormChanged()) {
                self.getModule().getFormClosePopup().popup('open');
            }
            else {
                self.getModule().stopModule();
            }
        };

        this.modelValidationErrorEventHandler = function(ev) {

            if(ev.scope === self.getModule()) {
                self.resolveModelValidationError(ev.validationErrors, ev.validationErrorMessage);
            }
        };

        this.recordCreateCompleteEventHandler = function(ev) {
            self.getModule().resetForm();
            self.getApp().showNotification('success', 'You have successfully added new record.');
        };

        this.recordCreateErrorEventHandler = function(ev) {

            if(ev.scope === self.getModule()) {

                self.getApp().showNotification('error', ev.error.message);

                if(ev.error.errors) {
                    self.getModule().displayFormErrors(ev.error.errors);    
                }
            }
        };

        this.recordUpdateCompleteEventHandler = function(ev) {

            if(ev.scope === self.getModule()) {
                self.getModule().setIsFormChanged(false);
                self.getModule().setEditingRecordData(ev.responseData);
                self.getApp().showNotification('success', 'You have successfully updated record.');
            }
        };

        this.recordUpdateErrorEventHandler = function(ev) {

            if(ev.scope === self.getModule()) {

                self.getApp().showNotification('error', ev.error.message);
                
                if(ev.error.errors) {
                    self.getModule().displayFormErrors(ev.error.errors);    
                }
            }
        };
    },

    methods: {

        addEvents: function() {

            this.addCrudEvents();

            this.getModule().getFormContainer().on('submit', 'form', this.formSubmittedEventHandler);

            this.getModule().getFormSubmitButton().on('click', this.formSubmittedEventHandler);
            this.getModule().getFormResetButton().on('click', this.formResetButtonClickHandler);
            this.getModule().getFormCloseButton().on('click', this.formCloseButtonClickHandler);

            if(this.getModule().getFormResetPopup()) {
                this.getModule().getFormResetPopup().popup('option', {
                    onFormSubmitted: this.formResetPopupSubmitHandler,
                    onFormCanceled: this.formResetPopupCanceldHandler
                });
            }

            if(this.getModule().getFormClosePopup()) {
                this.getModule().getFormClosePopup().popup('option', {
                    onFormSubmitted: this.formClosePopupSubmitHandler,
                    onFormCanceled: this.formClosePopupCanceldHandler
                });
            }

        	// listen when form inputs change
            this.getModule().getFormContainer().find(
                'input[type="text"], input[type="password"], input[type="hidden"], textarea'
            ).on('input', this.formChangedEventHandler);

            this.getModule().getFormContainer().find(
                'input[type="checkbox"], input[type="radio"], select'
            ).on('change', this.formChangedEventHandler);
        },

        removeEvents: function() {

            this.removeCrudEvents();            

            this.getModule().getFormContainer().off('submit', 'form', this.formSubmittedEventHandler);

            this.getModule().getFormSubmitButton().off('click', this.formSubmittedEventHandler);
            this.getModule().getFormResetButton().off('click', this.formResetButtonClickHandler);
            this.getModule().getFormCloseButton().off('click', this.formCloseButtonClickHandler);

            if(this.getModule().getFormResetPopup()) {
                this.getModule().getFormResetPopup().popup('option', {
                    onFormSubmitted: null,
                    onFormCanceled: null
                });
            }

            if(this.getModule().getFormClosePopup()){
                this.getModule().getFormClosePopup().popup('option', {
                    onFormSubmitted: null,
                    onFormCanceled: null
                });
            }

        	this.getModule().getFormContainer().find(
                'input[type="text"], input[type="password"], input[type="hidden"], textarea'
            ).off('input', this.formChangedEventHandler);

            this.getModule().getFormContainer().find(
                'input[type="checkbox"], input[type="radio"], select'
            ).off('change', this.formChangedEventHandler);
        },

        addCrudEvents: function() {

            var modelEvents = this.getModule().getModel().events;

            $(document).on(modelEvents.VALIDATION_ERROR, this.modelValidationErrorEventHandler);

            $(document).on(modelEvents.CREATE_COMPLETE, this.recordCreateCompleteEventHandler);
            $(document).on(modelEvents.CREATE_ERROR, this.recordCreateErrorEventHandler);

            $(document).on(modelEvents.UPDATE_COMPLETE, this.recordUpdateCompleteEventHandler);
            $(document).on(modelEvents.UPDATE_ERROR, this.recordUpdateErrorEventHandler);

            $(document).on(modelEvents.PATCH_COMPLETE, this.recordUpdateCompleteEventHandler);
            $(document).on(modelEvents.PATCH_ERROR, this.recordUpdateErrorEventHandler);
        },

        removeCrudEvents: function() {

            var modelEvents = this.getModule().getModel().events;

            $(document).off(modelEvents.VALIDATION_ERROR, this.modelValidationErrorEventHandler);

            $(document).off(modelEvents.CREATE_COMPLETE, this.recordCreateCompleteEventHandler);
            $(document).off(modelEvents.CREATE_ERROR, this.recordCreateErrorEventHandler);

            $(document).off(modelEvents.UPDATE_COMPLETE, this.recordUpdateCompleteEventHandler);
            $(document).off(modelEvents.UPDATE_ERROR, this.recordUpdateErrorEventHandler);

            $(document).off(modelEvents.PATCH_COMPLETE, this.recordUpdateCompleteEventHandler);
            $(document).off(modelEvents.PATCH_ERROR, this.recordUpdateErrorEventHandler);
        },

        startController: function() {

            if(this.getModule().getFormMode() === 'edit') {
                this.getModule().populateForm(this.getModule().getEditingRecordData());
            }

            this.getModule().updateFormSubmitButtonLabel(this.getModule().getFormMode());
        },

        submitForm: function(formData, formMode, role) {

            this.getModule().removeAllFormErrors();

            if(formMode === 'create') {
                this.getModule().submitFormCreate(formData);
            }
            else if(formMode === 'edit') {
                var recordId = this.getModule().getEditingRecordId();
                this.getModule().submitFormUpdate(recordId, formData);
            }
            else if(formMode === 'read') {
                this.getModule().submitFormRead(formData);   
            }
            else {
                this.throwException('Invalid form mode at Controllers.BaseForm', 1500, 'kernel');
            }
        },

        resolveModelValidationError: function(validationErrors, validationErrorMessage) {

            if(validationErrorMessage) {
                this.getApp().showNotification('error', validationErrorMessage);
            }
            else {
                this.getApp().showNotification('error', 'Form is invalid');
            }

            this.getModule().displayFormErrors(validationErrors);
        }
    }
});