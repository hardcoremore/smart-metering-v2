HCM.define({
    
    name: 'Controllers.Module',

    extendFrom: 'Base.Controller',

    construct: function(app) {

        this.callConstructor('Base.Controller', [app]);

        var self = this;

        this.moduleViewLoadCompleteEventHandler = function(ev) {

            var module = ev.module;
            var moduleManager = self.getApp().getModuleManager();

            moduleManager.setModuleView(ev.moduleClassName, ev.moduleView);
            moduleManager.initializeModuleView(module, ev.moduleView);

            var moduleContainer = moduleManager.getModuleContainer(module);

            moduleManager.addModuleViewToDOM(module, moduleContainer);

            self.getApp().hideLoader();

            var ev = $.Event(self.getApp().getConstants().MODULE_READY_EVENT, {module:module});
            $(document).trigger(ev);
        };

        this.moduleReinitializedEventHandler = function(ev) {

            if(ev.module.getIsPageModule() === true) {
                self.getApp().getModuleManager().activatePageModule(ev.module);
            }
        };

        //Hide loader on Module loading Error
        this.moduleLoadErrorEventHandler = function(ev) {

            self.getApp().hideLoader();
            self.getApp().resolveError({
                code: 1500,
                message: "Module could not be loaded. Please try again."
            });
        };

        this.moduleReadyEventHandler = function(ev) {

            if(ev.module.getIsPageModule() === true) {
                self.getApp().getModuleManager().activatePageModule(ev.module);
            }
        };

        this.moduleDestroyedEventHandler = function(ev) {
            console.log("Module destroyed: " + ev.module.getModuleName() + '. Module instance: ' + ev.module.getInstanceId());
        };

        this.resizeWindowHandler = function(ev) {

            var activeModule = self.getApp().getModuleManager().getActiveModule();

            if(activeModule) {
                activeModule.updateModuleDimensions();
            }
        };

        this.moduleButtonClickEventHandler = function(ev) {

            var pageModulesNamespace = self.getApp().getConfig().getParameter('pageModulesNamespace');

            var element = $(this);

            var module = element.data('module');
            var namespace = element.data('module-namespace') || pageModulesNamespace;
            var data = element.data('module-data') || null;
            var reinitialize = element.data('module-reinitialize') || false;
            var moduleClassName = namespace + '.' + module;

            if(module === 'Logout') {
                self.getApp().getUserModel().logout();
            }
            else if(element.data('page-module') !== false) {

                var openPageModuleCallback = self.getApp().getModuleManager().openPageModule;
                var activeModule = self.getApp().getModuleManager().getActiveModule();

                // check if requested page module is already opened
                if(activeModule.getNamespace() === moduleClassName) {
                    return;
                }

                if(activeModule && activeModule.allowStopModule() === false) {
                    activeModule.askForModuleStop(openPageModuleCallback.bind(
                        self.getApp().getModuleManager(),
                        moduleClassName,
                        data,
                        reinitialize
                    ));
                }
                else {

                    openPageModuleCallback.call(
                        self.getApp().getModuleManager(),
                        moduleClassName,
                        data,
                        reinitialize
                    );
                }
            }
        };

        this.moduleStartedEventHandler = function(ev) {};

        this.moduleStoppedEventHandler = function(ev) {

            var stopCallback = ev.module.getModuleAskForStopCallback();

            if(stopCallback) {
                stopCallback();
            }
        };
    },

    methods: {

        addEvents: function() {

            var constants = this.getApp().getConstants();

            $(document).on(constants.MODULE_VIEW_LOAD_COMPLETE_EVENT, this.moduleViewLoadCompleteEventHandler);
            $(document).on(constants.MODULE_VIEW_LOAD_ERROR_EVENT, this.moduleViewLoadErrorEventHandler);
            
            $(document).on(constants.MODULE_LOAD_COMPLETE_EVENT, this.moduleLoadCompleteEventHandler);
            $(document).on(constants.MODULE_LOAD_ERROR_EVENT, this.moduleLoadErrorEventHandler);

            $(document).on(constants.MODULE_REINITIALIZED_EVENT, this.moduleReinitializedEventHandler);
            
            $(document).on(constants.MODULE_READY_EVENT, this.moduleReadyEventHandler);

            $(document).on(constants.MODULE_STARTED_EVENT, this.moduleStartedEventHandler);
            $(document).on(constants.MODULE_STOPPED_EVENT, this.moduleStoppedEventHandler);

            $(document).on(constants.MODULE_DESTROYED_EVENT, this.moduleDestroyedEventHandler);

            $(window).on('resize orientationchange', this.resizeWindowHandler);

            //Start Page Module By Name on Click on Menu Item
            $("body").on('click', '[data-module]', this.moduleButtonClickEventHandler);
        },

        removeEvents: function() {

            var constants = this.getApp().getConstants();

            $(document).off(constants.MODULE_VIEW_LOAD_COMPLETE_EVENT, this.moduleViewLoadCompleteEventHandler);
            $(document).off(constants.MODULE_VIEW_LOAD_ERROR_EVENT, this.moduleViewLoadErrorEventHandler);
            
            $(document).off(constants.MODULE_LOAD_COMPLETE_EVENT, this.moduleLoadCompleteEventHandler);
            $(document).off(constants.MODULE_LOAD_ERROR_EVENT, this.moduleLoadErrorEventHandler);

            $(document).off(constants.MODULE_REINITIALIZED_EVENT, this.moduleReinitializedEventHandler);
            
            $(document).off(constants.MODULE_READY_EVENT, this.moduleReadyEventHandler);

            $(document).on(constants.MODULE_STARTED_EVENT, this.moduleStartedEventHandler);
            $(document).on(constants.MODULE_STOPPED_EVENT, this.moduleStoppedEventHandler);

            $(document).off(constants.MODULE_DESTROYED_EVENT, this.moduleDestroyedEventHandler);

            $(window).off('resize orientationchange', this.resizeWindowHandler);

            $("body").off('click', '[data-module]', this.moduleButtonClickEventHandler);
        },

        startDefaultModule: function() {
            var defaultModuleName = this.getApp().getConfig().getParameter('defaultModule');
            this.getApp().getModuleManager().openPageModule(defaultModuleName);
        }
    }
});