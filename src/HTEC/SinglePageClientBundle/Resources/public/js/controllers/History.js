HCM.define({

    name: 'Controllers.History',

    extendFrom: 'Base.Controller',

    construct: function(app) {

        this.callConstructor('Base.Controller', [app]);

        var that = this;

        this.historyChangedEventHandler = function(ev) {
            that.getApp().getHistoryManager().resolveUrl(ev.target.location.href);
        };
    },

    methods: {

        addEvents: function() {
            $(window).on('popstate', this.historyChangedEventHandler);
        },

        removeEvents: function() {
            $(window).off('popstate', this.historyChangedEventHandler);
        },

        startController: function() {
            var urlData = this.getApp().getHistoryManager().parseUrl(window.location.href);
            this.getApp().setUrlData(urlData);
        }
    }
});