HCM.define({
    
    name: 'Controllers.Authentication',

    extendFrom: 'Base.Controller',

    construct: function(app, module) {

        this.callConstructor('Base.Controller', [app, module]);

        var that = this;

        this.loginCompleteEventHandler = function(ev) {
            that.startApplication();
        };

        this.loginErrorEventHandler = function(ev) {

            if(ev.error.code === 400) {
                that.getModule().getLoginFormErrorMessageContainer().text('Invalid username or password').show();
            }
            else{
                that.getApp().throwException("Error ocurred while trying to log in user.", 1500, 'kernel');
            }
        };

        this.loginButtonClickHandler = function(ev) {

            that.getModule().getLoginFormErrorMessageContainer().hide();
            that.loginUser();
        };

        this.loginFormKeyDownHandler = function(ev) {
            
            // if enter is not pressed do not try to log in user
            if(ev && ev.type === 'keydown') {
                if(ev.keyCode !== $.ui.keyCode.ENTER) {
                    return;
                }
            }

            that.loginUser();
        }
    },

    methods: {

        addEvents: function() {

            var userModel = this.getApp().getUserModel();

            $(document).on(userModel.events.LOGIN_COMPLETE, this.loginCompleteEventHandler);
            $(document).on(userModel.events.LOGIN_ERROR, this.loginErrorEventHandler);

            this.getModule().getLoginButton().on('click', this.loginButtonClickHandler);
            this.getModule().getLoginButton().on('keydown', this.loginFormKeyDownHandler);
            this.getModule().getLoginForm().find('input').on('keydown', this.loginFormKeyDownHandler);
        },

        removeEvents: function() {

            var userModel = this.getApp().getUserModel();

            $(document).off(userModel.events.LOGIN_COMPLETE, this.loginCompleteEventHandler);
            $(document).off(userModel.events.LOGIN_ERROR, this.loginErrorEventHandler);

            this.getModule().getLoginButton().off('click', this.loginButtonClickHandler);
            this.getModule().getLoginButton().off('keydown', this.loginFormKeyDownHandler);
            this.getModule().getLoginForm().find('input').off('keydown', this.loginFormKeyDownHandler);
        },

        startApplication: function() {

            this.getApp().getModuleManager().destroyModule(this.getModule());
            this.getApp().enterApplication();
        },

        loginUser: function() {

            var username = this.getModule().getLoginForm().find('input[name="username"]').blur().val();
            var password = this.getModule().getLoginForm().find('input[name="password"]').blur().val();

            this.getModule().login(username, password);
        }
    }
});