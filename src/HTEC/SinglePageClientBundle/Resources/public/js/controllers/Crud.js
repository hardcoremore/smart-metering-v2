HCM.define({

    name: 'Controllers.Crud',

    extendFrom: 'Base.Controller',

    construct: function(app, module) {

        this.callConstructor('Base.Controller', [app, module]);

        var self = this;

        this.recordCreateCompleteEventHandler = function(ev) {'Crud recrod create complete'};

        this.readRecordsCompleteEventHandler = function(ev) {
            if(ev.scope === self.getModule()) {
                self.getModule().applyPagedData(ev.responseData);
                self.getModule().unblockMainViewComponent();
            }
        };

        this.readRecordsErrorEventHandler = function(ev) {

            if(self.isObject(ev.error) && ev.error.code === 1403)
            {
                self.getApp().showNotification('error', ev.error.message);
            }
            else
            {
                self.getApp().showNotification('error', 'Error ocurred while reading records');
            }

            self.getModule().unblockMainViewComponent();
        };

        this.recordUpdateCompleteEventHandler = function(ev) {'Crud recrod update complete handler'};

        this.recordPatchCompleteEventHandler = function(ev) {
            
            if(ev.scope.module === self.getModule()) {

                var mvc = self.getModule().getMainViewComponent();

                if(mvc.grid('getIsTableCellEditing')) {

                    var fieldModelData = ev.scope.fieldModelData;
                    var columnModelData = mvc.grid('getColumnModelData', fieldModelData.name);
                    var newCellData = ev.scope.editedData;

                    self.getModule().getMainViewComponent().grid(
                        'restorCellFromEdit',
                        newCellData,
                        columnModelData,
                        self.getModule().getSelectedRecordIndex(),
                        self.getModule().getSelectedRecord()
                    );

                    self.getModule().unblockMainViewComponent();
                    self.getApp().showNotification('success', 'Record updated successfully');

                    self.getModule().getModel().getRecordManager().removeMonitorData(self.getModule().getInstanceId());
                }
            }
        };

        this.recordPatchErrorEventHandler = function(ev) {

            if(ev.scope.module === self.getModule()) {

                if(self.getModule().getMainViewComponent().grid) {
                    self.getModule().getMainViewComponent().grid('openEditingCellWidget');
                }

                if(ev.error.message) {
                    self.getApp().showNotification('error', ev.error.message);
                }
                else {
                    self.getApp().showNotification('error', 'Cell value is not valid');
                }

                var cellName = self.getModule().getMainViewComponent().grid('getCurrentCellEditName');
                self.getModule().unblockMainViewComponent();

                var cellErrorMessage;

                if(ev.error.hasOwnProperty('errors') && ev.error.errors[cellName]) {
                    cellErrorMessage = ev.error.errors[cellName][0];
                }

                if(cellErrorMessage) {
                    self.getModule().getMainViewComponent().grid(
                        'showCellError',
                        cellName,
                        self.getModule().getSelectedRecordIndex(),
                        cellErrorMessage
                    );
                }
            }
        };

        this.recordDeleteEventHandler = function(ev) {

            self.getModule().getMainViewComponent().grid('removeRow', self.getModule().getSelectedRecordIndex());
            self.getModule().setSelectedRecord(null);
            self.getModule().setSelectedRecordIndex(-1);

            self.getApp().showNotification('success', 'You have successfully deleted record.');
        };

        this.recordDeleteErrorEventHandler = function(ev) {

        };

        this.crudControlButtonClickEventHandler = function(ev) {
            
            var button = $(ev.target).closest('.button');
            var actionType = button.data('role');

            self.handleModuleControlButtonClick(actionType);
        };

        this.paginatorPageChangeHandler = function(ev, ui) {

            self.getModule().readRecords(
                self.getModule().getPagingParameters(ui.pageNumber, ui.rowsPerPage),
                self.getModule().getSearchParameters()
            );
        };

        this.sortColumnChangeEventHandler = function(ev, ui) {

            if(self.getModule().getMainViewComponent().grid('option', 'localSort') !== true) {

                self.getModule().setCurrentSortColumnName(ui.sortColumnName);
                self.getModule().setCurrentSortColumnOrder(ui.sortColumnOrder);

                var rowsPerPage = self.getModule().getPagingComponent().paginator('option', 'rowsPerPage');

                self.getModule().readRecords(
                    self.getModule().getPagingParameters(1, ui.rowsPerPage, ui.sortColumnName, ui.sortColumnOrder),
                    self.getModule().getSearchParameters()
                );
            }
        };

        this.cellSelectedEventHandler = function(ev, ui) {
            //console.log("Cell selected: ", ui);
        };

        this.rowSelectedEventHandler = function(ev, ui) {
            self.getModule().setSelectedRecordIndex(ui.rowIndex);
            self.getModule().setSelectedRecord(ui.rowData);
        };

        this.rowDoubleClickEventHandler = function(ev, ui) {
            //console.log("Row double click event handler: ", ui);
        };

        this.quickSearchEventHandler = function(ev, ui) {

            var searchParameters = self.getModule().parseQuickSearchQuery(ui.query);
            self.getModule().setSearchParameters(searchParameters);

            var pageParameters = self.getApp().getConfig().getParameter('defaultPagingParameters');

            self.getModule().readRecords(pageParameters['CrudModule'], searchParameters);
        };

        this.quickSearchResetEventHandler = function(ev, ui) {

            self.getModule().setSearchParameters(null);

            var pageParameters = self.getApp().getConfig().getParameter('defaultPagingParameters');

            self.getModule().readRecords(pageParameters['CrudModule']);
        };

        this.relationDataLoadCompleteEventHandler = function(ev) {

            if(ev.scope.module === self.getModule()) {

                self.getModule().setRelationFieldSelectData(ev.scope.fieldModelData.name, ev.responseData);

                self.getModule().getMainViewComponent().grid(
                    'setColumnEditOptions',
                    ev.scope.fieldModelData.name,
                    {availableOptions: ev.responseData}
                );
            }
        };

        this.relationDataLoadErrorEventHandler = function(ev) {
            self.getApp().showNotification('error', "Relational data load error. Editing or creating might not work properly");
        };

        this.validationErrorEventHandler = function(ev) {

            if(self.isObject(ev.scope) && ev.scope.hasOwnProperty('fieldModelData')) {
                self.getModule().getMainViewComponent().grid(
                    'showCellError',
                    ev.scope.fieldModelData.name,
                    self.getModule().getSelectedRecordIndex(),
                    ev.validationErrors[ev.scope.fieldModelData.name]
                );
            }
        };

        this.moduleReadyEventHandler = function(ev) {

            if(ev.module === self.getModule().getFormModule()) {

                if(self.getModule().getFormModule().getFormMode() === 'create') {
                    self.getModule().openCreateForm();
                }
                else {
                    self.getModule().openEditForm(self.getModule().getSelectedRecord());
                }
            }
        };

        this.moduleStoppedEventHandler = function(ev) {

            if(self.getModule().getFormModule() && ev.module === self.getModule().getFormModule()) {

                self.getModule().closeFormModule();

                var monitorData = self.getModule().getModel().getRecordManager().getMonitorData(self.getModule().getInstanceId());
                self.getModule().updateMainViewComponentFromMonitorData(monitorData);
            }
        };

        this.moduleDestroyedEventHandler = function(ev) {

        };

        this.gridCellEditNotCompletePopupSubmitted = function(ev) {
            self.getModule().getGridEditNotCompletePopup().popup('close');
            self.getModule().getMainViewComponent().grid('cancelCellEdit');
            self.getModule().stopModule();
        };

        this.gridCellEditNotCompletePopupCanceled = function(ev) {
            self.getModule().setModuleAskForStopCallback(null);
        };

        this.recordDeletePopupSubmitted = function(ev) {
            
            self.getModule().getRecordDeletePopup().popup('close');

            var selectedRecord = self.getModule().getSelectedRecord();

            self.getModule().getModel().delete(selectedRecord.id, {scope:self.getModule()});
        };

        this.recordDeletePopupCanceled = function(ev) {

        };
    },

    methods: {

        startController: function() {

            this.getModule().closeViewRecordDetails();

            var mainViewComponentData = this.getModule().getMainViewComponentData();

            if(mainViewComponentData === null || (this.isArray(mainViewComponentData) && mainViewComponentData.length === 0)) {
                this.getModule().getMainViewComponent().grid('showEmptyRowsMessage', '&nbsp;');
                this.getModule().readRecords();
            }

            this.loadRelationData();
        },

        addEvents: function() {

            var modelEvents = this.getModule().getModel().events;

            $(document).on(modelEvents.READ_COMPLETE, this.readRecordsCompleteEventHandler);
            $(document).on(modelEvents.READ_ERROR, this.readRecordsErrorEventHandler);

            $(document).on(modelEvents.CREATE_COMPLETE, this.recordCreateCompleteEventHandler);
            $(document).on(modelEvents.UPDATE_COMPLETE, this.recordUpdateCompleteEventHandler);

            $(document).on(modelEvents.PATCH_COMPLETE, this.recordPatchCompleteEventHandler);
            $(document).on(modelEvents.PATCH_ERROR, this.recordPatchErrorEventHandler);

            $(document).on(modelEvents.DELETE_COMPLETE, this.recordDeleteEventHandler);
            $(document).on(modelEvents.DELETE_ERROR, this.recordDeleteErrorEventHandler);

            $(document).on(modelEvents.VALIDATION_ERROR, this.validationErrorEventHandler);

            var constants = this.getNamespaceValue('Static.Constants');

            $(document).on(constants.MODULE_READY_EVENT, this.moduleReadyEventHandler);
            $(document).on(constants.MODULE_STOPPED_EVENT, this.moduleStoppedEventHandler);
            $(document).on(constants.MODULE_DESTROYED_EVENT, this.moduleDestroyedEventHandler);

            this.getModule().getModuleCrudControls().on('click', '.button', this.crudControlButtonClickEventHandler);

            this.addPopupEvents();
            this.addMainViewComponentEvents();
            this.addPaginatorEvents();
            this.addQuickSearchEvents();
            this.addRelationFieldModelEvents();
        },

        removeEvents: function() {

            var modelEvents = this.getModule().getModel().events;

            $(document).off(modelEvents.READ_COMPLETE, this.readRecordsCompleteEventHandler);
            $(document).off(modelEvents.READ_ERROR, this.readRecordsErrorEventHandler);

            $(document).off(modelEvents.CREATE_COMPLETE, this.recordCreateCompleteEventHandler);
            $(document).off(modelEvents.UPDATE_COMPLETE, this.recordUpdateCompleteEventHandler);

            $(document).off(modelEvents.PATCH_COMPLETE, this.recordPatchCompleteEventHandler);
            $(document).off(modelEvents.PATCH_ERROR, this.recordPatchErrorEventHandler);

            $(document).off(modelEvents.DELETE_COMPLETE, this.recordDeleteEventHandler);
            $(document).off(modelEvents.DELETE_ERROR, this.recordDeleteErrorEventHandler);

            $(document).off(modelEvents.VALIDATION_ERROR, this.validationErrorEventHandler);

            var constants = this.getNamespaceValue('Static.Constants');

            $(document).off(constants.MODULE_READY_EVENT, this.moduleReadyEventHandler);
            $(document).off(constants.MODULE_STOPPED_EVENT, this.moduleStoppedEventHandler);
            $(document).off(constants.MODULE_DESTROYED_EVENT, this.moduleDestroyedEventHandler);


            this.getModule().getModuleCrudControls().off('click', '.button', this.crudControlButtonClickEventHandler);


            this.getModule().getGridEditNotCompletePopup().popup('option', {
                onFormSubmitted: null,
                onFormCanceled: null
            });

            this.removePopupEvents();
            this.removeMainViewComponentEvents();
            this.removePaginatorEvents();
            this.removeQiuckSearchEvents();
            this.removeRelationFieldModelEvents();
        },

        addPopupEvents: function() {

             this.getModule().getGridEditNotCompletePopup().popup('option', {
                onFormSubmitted: this.gridCellEditNotCompletePopupSubmitted,
                onFormCanceled: this.gridCellEditNotCompletePopupCanceled
            });

            this.getModule().getRecordDeletePopup().popup('option', {
                onFormSubmitted: this.recordDeletePopupSubmitted,
                onFormCanceled: this.recordDeletePopupCanceled
            });
        },

        removePopupEvents: function() {

            this.getModule().getGridEditNotCompletePopup().popup('option', {
                onFormSubmitted: null,
                onFormCanceled: null
            });

            this.getModule().getRecordDeletePopup().popup('option', {
                onFormSubmitted: null,
                onFormCanceled: null
            });
        },

        addMainViewComponentEvents: function() {
            if(this.getModule().getMainViewComponent()) {
                
                this.getModule().getMainViewComponent().grid('option', {

                    onCellSelected: this.cellSelectedEventHandler,
                    onSortColumnChange: this.sortColumnChangeEventHandler,
                    onRowSelected: this.rowSelectedEventHandler,                    
                    onRowDoubleClick: this.rowDoubleClickEventHandler,
                    cellEditCallbackOptions: {
                        callback: this.mainViewComponentCellEditCallback,
                        callbackScope: this
                    }
                });
            }
        },

        removeMainViewComponentEvents: function() {
            if(this.getModule().getMainViewComponent()) {
                
                this.getModule().getMainViewComponent().grid('option', {

                    onCellSelected: null,
                    onSortColumnChange: null,
                    onRowSelected: null,
                    onRowDoubleClick: null,
                    cellEditCallbackOptions: {}
                });
            }
        },

        addPaginatorEvents: function() {
            if(this.getModule().getPagingComponent()) {
                this.getModule().getPagingComponent().paginator('option', {
                    onPageChange: this.paginatorPageChangeHandler
                });
            }
        },

        removePaginatorEvents: function() {
            if(this.getModule().getPagingComponent()) {
                this.getModule().getPagingComponent().paginator('option', {
                    onPageChange: null
                });
            }
        },

        addQuickSearchEvents: function() {
            this.getModule().getQuickSearchComponent().quicksearch({
                onQuickSearch: this.quickSearchEventHandler,
                onQuickSearchReset: this.quickSearchResetEventHandler
            });
        },

        removeQiuckSearchEvents: function() {
            this.getModule().getQuickSearchComponent().quicksearch({
                onQuickSearch: null,
                onQuickSearchReset: null
            });
        },

        addRelationFieldModelEvents: function() {

            var relationFields = this.getModule().getModelRelationFields();
            var fieldModelData;

            for(var i = 0, len = relationFields.length; i < len; i++) {
                if(this.getModule().getNotAutoLoadingRelationFields().indexOf(relationFields[i]) === -1) {
                    this.addRelationFieldEvent(relationFields[i]);
                }
            }
        },

        removeRelationFieldModelEvents: function() {

            var relationFields = this.getModule().getModelRelationFields();
            var fieldModelData;

            for(var i = 0, len = relationFields.length; i < len; i++) {
                if(this.getModule().getNotAutoLoadingRelationFields().indexOf(relationFields[i]) === -1) {
                    this.removeRelationFieldEvent(relationFields[i]);
                }
            }
        },

        addRelationFieldEvent: function(fieldModelData) {
            
            $(document).on(fieldModelData.relationModel.events.READ_FOR_SELECT_COMPLETE, this.relationDataLoadCompleteEventHandler);
            $(document).on(fieldModelData.relationModel.events.READ_FOR_SELECT_ERROR, this.relationDataLoadErrorEventHandler);
        },

        removeRelationFieldEvent: function(fieldModelData) {
            $(document).off(fieldModelData.relationModel.events.READ_FOR_SELECT_COMPLETE, this.relationDataLoadCompleteEventHandler);
            $(document).off(fieldModelData.relationModel.events.READ_FOR_SELECT_ERROR, this.relationDataLoadErrorEventHandler);
        },

        // load relation select data to enable cell editing in grid
        loadRelationData: function() {

            var relationFields = this.getModule().getModelRelationFields();
            var fieldModelData, isFieldAutoLoading, relationFieldSelectData;

            for(var i = 0, len = relationFields.length; i < len; i++) {
                
                isFieldAutoLoading = this.getModule().getNotAutoLoadingRelationFields().indexOf(relationFields[i].name) === -1;

                if(isFieldAutoLoading) {

                    relationFieldSelectData = this.getModule().getRelationFieldSelectData(relationFields[i].name);

                    if(relationFieldSelectData === undefined || (this.isArray(relationFieldSelectData) && relationFieldSelectData.length === 0)) {
                        this.getModule().loadRelationFieldData(relationFields[i]);
                    }
                    else {
                        this.getModule().updateFieldFromMonitorData(relationFields[i]);
                    }
                }
            }
        },

        mainViewComponentCellEditCallback: function(cellName, cellData, cellValue, rowData) {

            var patchScope = this.getModule().getPatchRecordScope(cellName, cellValue, cellData);
            var patchData = {};
                patchData[cellName] = cellValue;

            this.getModule().patchRecordField(rowData.id, cellName, patchData, patchScope);

            if(this.getModule().getMainViewComponent().grid) {
                this.getModule().getMainViewComponent().grid('closeEditingCellWidget');
            }
        },

        handleModuleControlButtonClick: function(actionType) {

            if(this.getModule().getMainViewComponent().grid('getTableState') === 'editing') {
                this.getApp().showNotification('warning', 'Please finish the grid cell editing first.');
                return false;
            }

            switch(actionType) {

                case 'create-record':
                    if(this.getModule().getFormModule()) {
                        this.getModule().openCreateForm();
                    }
                    else {
                        this.getModule().loadFormModule('create');
                    }
                break;

                case 'edit-record':

                    var selectedRecord = this.getModule().getSelectedRecord();

                    if(selectedRecord) {
                        if(this.getModule().getFormModule()) {
                            this.getModule().openEditForm(selectedRecord);
                        }
                        else {
                            this.getModule().loadFormModule('edit', selectedRecord);
                        }
                    }
                    else {
                        this.getApp().showNotification('warning', 'Please select the record you want to edit.');
                    }

                break;

                case 'delete-record':

                    var selectedRecord = this.getModule().getSelectedRecord();

                    if(selectedRecord) {
                        this.getModule().getRecordDeletePopup().popup('open');
                    }
                    else {
                        this.getApp().showNotification('warning', 'Please select the record you want to delete.');
                    }

                break;

                case 'record-details':

                break;
            }

            return true;
        }
    }
});