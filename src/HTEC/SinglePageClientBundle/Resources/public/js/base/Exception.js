HCM.define({

    name: 'Base.Exception',

    construct: function(errorMessage, code) {

        this.getMessage = function() {
            return errorMessage
        };

        this.getCode = function() {
            return code;
        }
    }
});