HCM.define({
    
    name: 'Base.ServiceManager',

    construct: function(servicesConfiguration, config) {

        var servicesConfig = servicesConfiguration;

        var services = {};

        this.getServicesConfiguration = function() {
            return servicesConfig;
        };

        this.getConfig = function() {
            return config;
        };

        this.setServiceToCache = function(serviceName, service) {
            services[serviceName] = service;
        };

        this.getServiceFromCache = function(serviceName) {
            return services[serviceName];
        };

        this.getAllServices = function() {
            return services;
        };
    },

    methods: {

        serviceExists: function(serviceName) {
            return this.getServicesConfiguration().hasOwnProperty(serviceName);
        },

        getService: function(serviceName) {

            var config = this.getServicesConfiguration();

            var serviceConfig = config[serviceName];

            if(serviceConfig) {

                var serviceInstance = this.getServiceFromCache(serviceName);

                // services are singletons by default.
                // you can set that service is not singleton by setting singleton to false in service config
                if(serviceInstance && serviceConfig.singleton !== false) {
                    return serviceInstance;    
                }

                /***
                 *
                 * HERE WE BUILD SERVICE WITH ALL ITS DEPENDENCIES 
                 *
                 */ 
                var serviceClassArguments = serviceConfig.arguments || [];
                var serviceClassCalls = serviceConfig.calls || [];

                var parentServiceConfig = config[serviceConfig.parent];

                while(parentServiceConfig) {

                    /**
                     * Check to see if service is extending constructor arguments from parent service
                     * 
                     * This means that if parent service configuration has constructor arguments (@Base.Model, 'http://example.com')
                     * and child service configuration has constructor arguments (@Project.ConcreteImplemenation)
                     *
                     * The class will be instantiated with following arguments (@Base.Model, 'http://example.com', @Project.ConcreteImplemenation)
                     *
                     */
                    if(serviceConfig.extendArguments === true && this.isArray(parentServiceConfig.arguments)) {
                        serviceClassArguments = [].concat(parentServiceConfig.arguments, serviceClassArguments);
                    }

                    // extending method calls from parent service definition is true by default
                    if(serviceConfig.extendCalls !== false && this.isArray(parentServiceConfig.calls)) {
                        serviceClassCalls = [].concat(parentServiceConfig.calls, serviceClassCalls);
                    }

                    parentServiceConfig = config[parentServiceConfig.parent];
                }
                
                var classArgumentValue;
                var classArguments = [];

                for(var i = 0, len = serviceClassArguments.length; i < len; i++) {

                    classArgumentValue = serviceClassArguments[i];

                    classArguments.push(this.getArgument(classArgumentValue));
                }

                var className = null;

                if(serviceConfig.hasOwnProperty('class') && this.isString(serviceConfig.class) && serviceConfig.class.length > 0) {
                    className = serviceConfig.class;
                }
                else {
                    className = serviceName;
                }

                serviceInstance = this.getInstance(className, classArguments);

                var classCallValue;
                var classCalls = [];
                var classCallArguments;

                for(var c = 0, len = serviceClassCalls.length; c < len; c++) {

                    classCallValue = serviceClassCalls[c];

                    if(classCallValue.method && this.isString(classCallValue.method) && classCallValue.method.length > 0) {

                        // check if arguments exists for this class call
                        if(classCallValue.arguments) {

                            // check if arguments is array
                            if(this.isArray(classCallValue.arguments)) {

                                classCallArguments = [];;

                                for(var index in classCallValue.arguments) {
                                    classCallArguments.push(this.getArgument(classCallValue.arguments[index]));
                                }
                            }
                            else {
                                classCallArguments = this.getArgument(classCallValue.arguments);
                            }
                        }
                        else {
                            classCallArguments = null;    
                        }

                        serviceInstance[classCallValue.method].apply(serviceInstance, classCallArguments);
                    }
                    else {
                        this.throwException('Service call does not have a valid method. Service name: "' + serviceName + '".', 1500, 'kernel');
                    }
                }
            }
            else {
                this.throwException('You have requested a non existing service: "' + serviceName + '".', 1500, 'kernel');
            }

            this.setServiceToCache(serviceName, serviceInstance);

            return serviceInstance;
        },

        isArgumentService: function(argumentValue) {
            return this.isString(argumentValue) && argumentValue.indexOf('@') !== -1;
        },

        isArgumentConfigParameter: function(argumentValue) {
            return this.isString(argumentValue) && argumentValue.indexOf('$') !== -1;
        },

        getServiceNameFromArgument: function(argumentValue) {
            return argumentValue.slice(1);
        },

        getParameterNameFromArgument: function(argumentValue) {
            return argumentValue.slice(1);
        },

        getArgument: function(argumentValue) {

            if(this.isArgumentConfigParameter(argumentValue)) {
                return this.getConfig().getParameter(this.getParameterNameFromArgument(argumentValue));
            }
            else if(this.isArgumentService(argumentValue)) {
                return this.getService(this.getServiceNameFromArgument(argumentValue));
            }
            else if(this.getClassDefinition(argumentValue)) {
                return this.getInstance(argumentValue);
            }
            else {
                return argumentValue;
            }
        }
    }
});
