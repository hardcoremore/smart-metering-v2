HCM.define({

    name: 'Base.Model',

    construct: function() {

        var mainApp = null;
        var config = null;
        var requester = null;
        
        var modelValidator = null;
        var recordManager = null;     

        this.setConfig = function(conf) {
            config = conf;
        };

        this.getConfig = function() {
            return config;
        };

        this.setRequester = function(req) {
            requester = req;
        };

        this.getRequester = function() {
            return requester;
        };

        this.setValidator = function(validator) {
            modelValidator = validator;
        };

        this.getValidator = function() {
            return modelValidator;
        };

        this.getConfigParameter = function(name) {
            return this.getConfig().getParameter(name);
        };

        this.setRecordManager = function(manager) {
            recordManager = manager;
        };

        this.getRecordManager = function() {
            return recordManager;
        };
    },

    extendPrototypeWith: {
        constants: HCM.getNamespaceValue('Static.Constants')
    },

    methods: {

        getUrl: function(part) {
            return this.getConfig().getBaseAPIUrl() + this.getUrlPart() + (part || '');
        },

        checkIfCharCodeIsAlhpaNumberic: function(c) {
            return (c > 47 && c < 58) || (c > 64 && c < 90) || (c > 96 && c < 123);
        },

        generateUploadId: function() {

            var r = '';
            var c = 0;

            for(var i = 0; i < 32; i++) {

                // numbers and letters only
                while(this.checkIfCharCodeIsAlhpaNumberic(c) === false) {
                    c = Math.round(Math.random() * 123);
                }

                r += String.fromCharCode(c);

                c = 0;
            }

            return r;
        },

        dataURItoBlob: function(dataURI) {

            var byteString = atob(dataURI.split(',')[1]);

            var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

            var ab = new ArrayBuffer(byteString.length);
            var ia = new Uint8Array(ab);

            for (var i = 0; i < byteString.length; i++) {
                ia[i] = byteString.charCodeAt(i);
            }

            return new Blob([ab], { "type": mimeString });
        },

        getArrayCollectionValues: function(fieldName, arrayCollection) {

            var fieldModelData = this.getFieldByName(fieldName);

            if(fieldModelData && fieldModelData.type === 'relation') {

                this.sortByObjectPropertyASC(arrayCollection, fieldModelData.valuePropertyName);

                var items = [];
                var editedItems = [];

                for(var i = 0, len = arrayCollection.length; i < len; i++) {
                    items.push(arrayCollection[i][fieldModelData.valuePropertyName].toString());
                }

                return items;
            }
            else {
                this.throwException('Field data is invalid at Base.Model:getArrayCollectionValues.', 1500, 'kernel');
            }
        },

        // compare if two array collections are the same for field
        isArrayCollectionChanged: function(fieldName, oldData, editedData) {

            var oldValue = this.getArrayCollectionValues(fieldName, oldData);
            var editedValue = this.getArrayCollectionValues(fieldName, editedData);

            if(oldValue.length !== editedValue.length) {
                return true;
            }

            return oldValue.join(',') !== editedValue.join(',');
        },

        getRelationFields: function() {

            var modelFields = this.getFields();
            var relationFields = [];

            for(var i = 0, len = modelFields.length; i < len; i++) {

                fieldModelData = modelFields[i];

                if(fieldModelData.type === 'relation') {
                    relationFields.push(fieldModelData);
                }
            }

            return relationFields;
        },

         /**
         *
         * Returns the record in array data by value of a recrod property.
         *
         * @param {data} array Data to be search in
         * 
         * @param {propertyName} string Property name of a record that needs to have certain value.
         *
         * @param {propertyValue} mix Value that will be matched against the property value of a record in data array.
         *
         * @return {Object} Returns selected record from select data
         *
         */
        findRecordByPropertyValue: function(data, propertyName, propertyValue) {

            if(this.isArray(data) === false) {
                this.throwException('Data is not valid at Model:findRecordByPropertyValue().', 1500, 'kernel');
            }

            if(this.isString(propertyName) === false || propertyName.length < 1) {
                this.throwException('Property name is not valid at Model:findRecordByPropertyValue().', 1500, 'kernel');
            }

            if(this.isString(propertyValue) === false && isNaN(propertyValue) === true) {
                this.throwException('Property value is not valid at Model:findRecordByPropertyValue().', 1500, 'kernel');
            }

            var record = null;

            for(var i = 0, len = data.length; i < len; i++) {

                record = data[i];

                if(record && record.hasOwnProperty(propertyName) && 
                    this.isObject(record[propertyName]) === false &&
                    record[propertyName].toString() === propertyValue.toString()) {
                    return record;
                }
            }

            return null;
        },

        uploadFile: function(url, file, postData, options) {

            var uploadId = options.uploadId || this.generateUploadId();
            var that = this;

            $.extend(options, true, {

                url: url + '?X-Progress-ID=' + uploadId,

                success: function(responseData, textStatus, jqXHR) {
                    that.dispatchSuccess(that.events.UPLOAD_FILE_COMPLETE, responseData, options, jqXHR);
                },

                error: function(error, jqXHR, textStatus) {
                    that.dispatchError(that.events.UPLOAD_FILE_ERROR, error, options, jqXHR);
                }
            });

            var data = new FormData();
                data.append('file', file);

            if(postData && this.isObject(postData)) {

                var postKeys = Object.keys(postData);

                // add post parameters to FormData
                for(var i in postKeys) {
                    data.append(postKeys[i], postData[postKeys[i]]);
                }
            }

            options.processData = false;
            options.contentType = false;

            options.data = data;

            this.getRequester().POST(options);

            return uploadId;
        },

        readFileUploadProgress: function(url, uploadId, options) {

            var that = this;
            options.uploadId = uploadId;
            
            $.extend(options, true, {

                url: url + '?X-Progress-ID=' + uploadId,

                success: function(responseData, textStatus, jqXHR) {
                    that.dispatchSuccess(that.events.READ_FILE_UPLOAD_PROGRESS_COMPLETE, responseData, options, jqXHR);
                },

                error: function(error, jqXHR, textStatus) {
                    that.dispatchError(that.events.READ_FILE_UPLOAD_PROGRESS_ERROR, error, options, jqXHR);
                }
            });

            this.getRequester().GET(options);
        },

         /**
         *
         * Create new record in the database.
         *
         * @param {Object} data Data of the record to be saved
         * @param {Object} options Options for creating new record
            {
                data: @param {Object} Data for the record to be created
                scope: @param {Object} In what scope to invoke success or error callback function
                showLoader: @param {Boolean} If true the global loader will be display while loading
            }
         *
         */
        create: function(data, options) {

            var that = this;

            if(this.getValidator().validate(this.getValidationRules(), data, this.constants.CREATE_OPERATION)) {

                $.extend(options, true, {

                    url: this.getUrl(),

                    success: function(responseData, textStatus, jqXHR){

                        that.modelCreateComplete(that.events.CREATE_COMPLETE, responseData, options);
                        that.dispatchSuccess(that.events.CREATE_COMPLETE, responseData, options, jqXHR);
                    },

                    error: function(error, jqXHR, textStatus) {
                        that.dispatchError(that.events.CREATE_ERROR, error, options, jqXHR);
                    }
                });

                options.data = data;
                this.getRequester().POST(options);
            }
            else {

                var e = $.Event(
                    that.events.VALIDATION_ERROR,
                    {
                        validationErrors: this.getValidator().getValidationErrors(),
                        validationErrorMessage: this.getValidator().getValidationErrorMessage(),
                        scope: options.scope
                    }
                );

                $(document).trigger(e);
            }
        },

         /**
         *
         * Reads the records. This function is supposed to be called with paging parameters.
         *
         * @param {Object} options Options for creating new record.
            {
                scope: @param {Object} In what scope to invoke success or error callback function
                showLoader: @param {Boolean} If true the global loader will be display while loading
                parameters: @param {Object} What parameters to add on the url.
            }
         *
         */
        read: function(options) {

            var self = this;

            $.extend(options, true, {

                url: this.getUrl('/list'),

                success: function(responseData, textStatus, jqXHR) {

                    if(self.checkPageResponseFormat(responseData) === true) {
                        self.dispatchSuccess(self.events.READ_COMPLETE, responseData, options, jqXHR);
                        self.modelReadComplete(self.events.READ_COMPLETE, responseData, options);
                    }
                    else {

                        var error = null;

                        if(responseData.code === 1403)
                        {
                            error = responseData;
                        }
                        else
                        {
                            error = {
                                code: self.getConfigParameter('errorCodes').invalidResponseFormat,
                                message: self.getConfigParameter('errorCodes').invalidResponseFormatMsg
                            };    
                        }

                        self.dispatchError(self.events.READ_ERROR, error, options, jqXHR);
                    }
                },

                error: function(error, jqXHR, textStatus) {
                    self.dispatchError(self.events.READ_ERROR, error, options, jqXHR);
                }
            });

            this.getRequester().GET(options);
        },

        readPagedData: function(pageNumber, rowsPerPage, sortColumnName, sortColumnOrder, parameters, options) {

            var self = this;

            $.extend(options, true, {

                url: this.getUrl('/list-paged-data'),

                parameters: $.extend({
                    pageNumber: pageNumber,
                    rowsPerPage: rowsPerPage,
                    sortColumnName: sortColumnName,
                    sortColumnOrder: sortColumnOrder
                }, parameters),

                success: function(responseData, textStatus, jqXHR) {

                    if(self.checkPageResponseFormat(responseData) === true) {

                        self.setPagedData(responseData);

                        self.dispatchSuccess(self.events.READ_PAGED_DATA_COMPLETE, responseData, options, jqXHR);
                        self.modelReadComplete(self.events.READ_PAGED_DATA_COMPLETE, responseData, options);
                    }
                    else {

                        var error = {
                            code: self.getConfigParameter('errorCodes').invalidResponseFormat,
                            message: self.getConfigParameter('errorCodes').invalidResponseFormatMsg
                        };

                        self.dispatchError(self.events.READ_PAGED_DATA_ERROR, error, options, jqXHR);
                    }
                },

                error: function(error, jqXHR, textStatus) {
                    self.dispatchError(self.events.READ_PAGED_DATA_ERROR, error, options, jqXHR);
                }
            });

            this.getRequester().GET(options);
        },

        /**
          * Reads all the records. This function should not be called with paging parameters.
         *
         * @param {Object} options Options for reading through record.
            {
                scope: @param {Object} In what scope to invoke success or error callback function
                showLoader: @param {Boolean} If true the global loader will be display while loading
                parameters: @param {Object} What parameters to add on the url.
            }
         *
         *
        */
        readAll: function(options) {

            var self = this;

            $.extend(options, true, {

                url: this.getUrl('/list-all'),

                success: function(responseData, textStatus, jqXHR) {
                    self.dispatchSuccess(self.events.READ_ALL_COMPLETE, responseData, options, jqXHR);
                    self.modelReadComplete(self.events.READ_ALL_COMPLETE, responseData, options);
                },

                error: function(error, jqXHR, textStatus) {
                    self.dispatchError(self.events.READ_ALL_ERROR, error, options, jqXHR);
                }
            });

            this.getRequester().GET(options);
        },

         /**
         *
         * Search trough all the records. This function can be called with paging parameters.
         *
         * @param {Object} options Options for searching through records.
            {
                scope: @param {Object} In what scope to invoke success or error callback function
                showLoader: @param {Boolean} If true the global loader will be display while loading
                parameters: @param {Object} What parameters to add on the url.
            }
         *
         *
         */
        quickSearch: function(options) {

            var that = this;

            $.extend(options, true, {

                url: this.getUrl('/quick-search'),

                success: function(responseData, textStatus, jqXHR) {

                    if(that.checkPageResponseFormat(responseData) === true) {
                        that.dispatchSuccess(that.events.READ_COMPLETE, responseData, options, jqXHR);
                        that.modelReadComplete(that.events.READ_COMPLETE, responseData, options);
                    }
                    else {

                        var error = {
                            code: that.getConfigParameter('errorCodes').invalidResponseFormat,
                            message: that.getConfigParameter('errorCodes').invalidResponseFormatMsg
                        };

                        that.dispatchError(that.events.READ_ERROR, error, options, jqXHR);
                    }
                },

                error: function(error, jqXHR, textStatus) {
                    that.dispatchError(that.events.READ_ERROR, error, options, jqXHR);
                }
            });

            this.getRequester().GET(options);
        },

        /**
         *
         * Read all records for select data, i.e. for select combo box.
         *
         * @param {Object} options Options for reading all records for select data.
            {
                scope: @param {Object} In what scope to invoke success or error callback function
                showLoader: @param {Boolean} If true the global loader will be display while loading
                parameters: @param {Object} What parameters to add on the url.
            }
         *
         *
         */
        readForSelect: function(options) {

            var self = this;

            $.extend(options, true, {

                url: this.getUrl('/list-for-select'),

                success: function(responseData, textStatus, jqXHR){
                    self.dispatchSuccess(self.events.READ_FOR_SELECT_COMPLETE, responseData, options, jqXHR);
                    self.modelReadComplete(self.events.READ_FOR_SELECT_COMPLETE, responseData, options);
                },

                error: function(error, jqXHR, textStatus) {
                    self.dispatchError(self.events.READ_FOR_SELECT_ERROR, error, options, jqXHR);
                }
            });

            this.getRequester().GET(options);
        },

         /**
         *
         * Search all records for supplied query string for autocomplete.
         *
         * @param {Object} options Options for searching all records for autocomplete.
            {
                scope: @param {Object} In what scope to invoke success or error callback function
                showLoader: @param {Boolean} If true the global loader will be display while loading
                parameters: @param {Object} What parameters to add on the url.
            }
         *
         *
         */
        readForAutoComplete: function(options) {

            var that = this;

            $.extend(options, true, {

                url: this.getUrl('/list-auto-complete'),

                success: function(responseData, textStatus, jqXHR){
                    that.dispatchSuccess(that.events.READ_FOR_AUTOCOMPLETE_COMPLETE, responseData, options, jqXHR);
                    that.modelReadComplete(that.events.READ_FOR_AUTOCOMPLETE_COMPLETE, responseData, options);
                },

                error: function(error, jqXHR, textStatus) {
                    that.dispatchError(that.events.READ_FOR_AUTOCOMPLETE_ERROR, error, options, jqXHR);
                }
            });

            this.getRequester().GET(options);
        },

        /**
         *
         * Search all records for supplied date range if record has start and end properties.
         *
         * @param {Object} options Options for searching all records for supplied date range.
            {
                scope: @param {Object} In what scope to invoke success or error callback function
                showLoader: @param {Boolean} If true the global loader will be display while loading
                parameters: @param {Object} What parameters to add on the url.
            }
         * @param {String} start Start date in format yyyy-mm-dd, i.e. 1987-01-30
         * @param {String} end End date in format yyyy-mm-dd, i.e. 2115-01-30
         *
         */
        readForDateRange: function(options, start, end) {

            var that = this;

            $.extend(options, true, {

                url: this.getConfig().getBaseAPIUrl() + this.getUrl() + '/listForDateRange/start/' + start + '/end/' + end,

                success: function(responseData, textStatus, jqXHR){
                    that.dispatchSuccess(that.events.READ_FOR_DATE_RANGE_COMPLETE, responseData, options, jqXHR);
                    that.modelReadComplete(that.events.READ_FOR_DATE_RANGE_COMPLETE, responseData, options);
                },

                error: function(error, jqXHR, textStatus) {
                    that.dispatchError(that.events.READ_FOR_DATE_RANGE_ERROR, error, options, jqXHR);
                }
            });

            this.getRequester().GET(options);
        },

        /**
         *
         * Get record by id.
         *
         * @param {Object} options Options for getting record by id.
            {
                scope: @param {Object} In what scope to invoke success or error callback function
                showLoader: @param {Boolean} If true the global loader will be display while loading
            }
         * @param {Number} id Id of the record
         *
         */
        findById: function(id, options) {

            var that = this;

            $.extend(options, true, {

                url: this.getUrl('/' + id),
                showLoader: options.showLoader,

                success: function(responseData, textStatus, jqXHR){
                    that.dispatchSuccess(that.events.FIND_BY_ID_COMPLETE, responseData, options, jqXHR, jqXHR);
                    that.modelReadComplete(that.events.FIND_BY_ID_COMPLETE, responseData, options);
                },

                error: function(error, jqXHR, textStatus) {
                    that.dispatchError(that.events.FIND_BY_ID_ERROR, error, options, jqXHR, jqXHR);
                }
            });

            this.getRequester().GET(options);
        },

        readLastModified: function(options) {

            var that = this;

            $.extend(options, true, {

                url: this.getUrl('/get-last-modified'),
                showLoader: options.showLoader,

                success: function(responseData, textStatus, jqXHR){
                    that.dispatchSuccess(that.events.READ_LAST_MODIFIED_COMPLETE, responseData, options, jqXHR, jqXHR);
                    that.modelReadComplete(that.events.READ_LAST_MODIFIED_COMPLETE, responseData, options);
                },

                error: function(error, jqXHR, textStatus) {
                    that.dispatchError(that.events.READ_LAST_MODIFIED_ERROR, error, options, jqXHR, jqXHR);
                }
            });

            this.getRequester().GET(options);
        },

        /**
         *
         * Update the record with new data.
         *
         * @param {Number} id Id of the record to update
         * @param {Object} data New data of the record to be updated with
         * @param {Object} options Options for updating record.
            {
                scope: @param {Object} In what scope to invoke success or error callback function
                showLoader: @param {Boolean} If true the global loader will be display while loading
            }
         *
         */
        update: function(id, data, options) {

            var that = this;

            if(this.getValidator().validate(this.getValidationRules(), data, this.constants.UPDATE_OPERATION)) {

                $.extend(options, true, {
                    url: this.getUrl('/' + id),

                    success: function(responseData, textStatus, jqXHR){
                        selectData = null;
                        that.modelUpdateComplete(that.events.UPDATE_COMPLETE, id, responseData, options);
                        that.dispatchSuccess(that.events.UPDATE_COMPLETE, responseData, options, jqXHR, jqXHR);
                    },

                    error: function(error, jqXHR, textStatus) {
                        that.dispatchError(that.events.UPDATE_ERROR, error, options, jqXHR, jqXHR);
                    }
                });

                options.data = data;

                this.getRequester().PUT(options);
            }
            else {

                var e = $.Event(
                    that.events.VALIDATION_ERROR,
                    {
                        validationErrors: this.getValidator().getValidationErrors(),
                        validationErrorMessage: this.getValidator().getValidationErrorMessage(),
                        scope: options.scope
                    }
                );

                $(document).trigger(e);
            }
        },

        patch: function(id, data, options) {

            var that = this;

            var isDataValid = true;
            var dataKeys = Object.keys(data);

            for(var i = 0, len = dataKeys.length; i < len; i++) {
                isDataValid = this.getValidator().validateField(
                    this.getValidationRules(),
                    dataKeys[i],
                    data[dataKeys[i]],
                    this.constants.PATCH_OPERATION
                );
            }

            if(isDataValid) {

                $.extend(options, true, {

                    url: this.getUrl('/' + id),

                    success: function(responseData, textStatus, jqXHR){
                        that.modelPatchComplete(that.events.PATCH_COMPLETE, id, data, options);
                        that.dispatchSuccess(that.events.PATCH_COMPLETE, responseData, options, jqXHR, jqXHR);
                    },

                    error: function(error, jqXHR, textStatus) {
                        that.dispatchError(that.events.PATCH_ERROR, error, options, jqXHR, jqXHR);
                    }
                });

                options.data = data;

                this.getRequester().PATCH(options);

                return true;
            }
            else {

                var e = $.Event(
                    that.events.VALIDATION_ERROR,
                    {
                        validationErrors: this.getValidator().getValidationErrors(),
                        validationErrorMessage: this.getValidator().getValidationErrorMessage(),
                        scope: options.scope
                    }
                );

                $(document).trigger(e);

                return false;
            }
        },

        delete: function(id, options) {

            var that = this;

            $.extend(options, true, {

                url: this.getUrl('/' + id),
                showLoader: options.showLoader,

                success: function(responseData, textStatus, jqXHR) {

                    that.modelDeleteComplete(that.events.DELETE_COMPLETE, id, options);
                    that.dispatchSuccess(that.events.DELETE_COMPLETE, id, options, jqXHR);
                },

                error: function(error, jqXHR, textStatus) {
                    that.dispatchError(that.events.DELETE_ERROR, error, options, jqXHR);
                }
            });

            this.getRequester().DELETE(options);
        },

        addValidationError: function(fieldName, errorMessage) {

            var validationErrors = this.getValidator().getValidationErrors();
            var fieldErrors = validationErrors[fieldName];

            if(!fieldErrors) {
                fieldErrors = [];
            }

            fieldErrors.push(errorMessage);
            validationErrors[fieldName] = fieldErrors;

            this.setValidationErrors(validationErrors);
        },

        getFieldByName: function(fieldName) {

            if(fieldName) {
                var fields = this.getFields();

                var name = fieldName.toString();

                for(var f in fields) {
                    if(fields[f].name === name) {
                        return fields[f];
                    }
                }
            }

            return null;
        },


        dispatchSuccess: function(eventName, data, options, jqXHR) {

            if(this.isString(eventName) && eventName.length > 0) {

                var eventOptionsData = {
                    responseData:data,
                    model: this,
                    jqXHR:jqXHR
                };

                if(options) {
                    $.extend(eventOptionsData, options);
                }

                var e = $.Event(eventName, eventOptionsData);
                $(document).trigger(e);
            }
            else {
                this.throwException("Invalid event name supplied.", 1500, 'kernel');
            }
        },

        dispatchError: function(eventName, error, options, jqXHR) {

            var e = $.Event(eventName, {error:error, scope:options.scope, model:this, jqXHR:jqXHR});
            $(document).trigger(e);
        },

        modelCreateComplete: function(event, data, options) {

            if(this.getRecordManager()) {
                this.getRecordManager().recordCreate(data);
            }
        },

        modelReadComplete: function(event, data, options) {},

        modelUpdateComplete: function(event, id, data, options) {

            if(this.getRecordManager()) {
                this.getRecordManager().recordUpdate(id, data);
            }
        },

        modelPatchComplete: function(event, id, data, options) {
            if(this.getRecordManager()) {
                this.getRecordManager().recordUpdate(id, data);
            }
        },

        modelDeleteComplete: function(event, id, options) {
            if(this.getRecordManager()) {
                this.getRecordManager().recordDelete(id);
            }
        },

        getMergedPathedRecord: function(id, recordData, patchData) {
            return $.extend(recordData, patchData);
        },

        checkPageResponseFormat: function(serverResponse) {
             // check if returned value have valid properties
            if(serverResponse && serverResponse.hasOwnProperty('listData') && this.isArray(serverResponse.listData)) {
                return true;
            }
            else {
                return false;
            }
        }
    }
});
