HCM.define({

    name: 'Base.ModelRecordManager',

    construct: function() {

        var recordsData = {};
        var registeredClients = [];
        var monitorData = {};

        this.setRecord = function(id, data) {
            recordsData[id] = data;
        };

        this.removeRecord = function(id) {
            delete recordsData[id];
        };

        this.getRecord = function(id) {
            return recordsData[id];
        };

        this.getAllRecordsIds = function() {
            return Object.keys(recordsData);
        };

        this.setRegisteredClients = function(clients) {
            registeredClients = clients;
        };

        this.getRegisteredClients = function() {
            return registeredClients;
        };

        this.setMonitorData = function(id, data) {
            monitorData[id] = data;
        };

        this.getMonitorData = function(id) {
            return monitorData[id];
        };

        this.removeMonitorData = function(id) {
            delete monitorData[id];
        };
    },

    methods: {

        recordCreate: function(data) {

            this.setRecord(data.id, data);

            var clients = this.getRegisteredClients();
            var monitorData = null;

            for(var i = 0, len = clients.length; i < len; i++) {

                monitorData = this.getMonitorData(clients[i]);

                if(monitorData === undefined) {
                    monitorData = {
                        created: [data.id]
                    };
                }
                else {

                    if(monitorData.created) {
                        monitorData.created.push(data.id);
                    }
                    else {
                        monitorData.created = [data.id];
                    }
                }

                this.setMonitorData(clients[i], monitorData);
            }
        },

        recordUpdate: function(id, data) {

            var oldRecord = this.getRecord(id);
            var newRecord = data;

            var updatedPropertiesNames = [];

            if(oldRecord) {

                var keys = Object.keys(oldRecord);
                var propertyName;
                var propertyValue;
                var newRecordHasProperty;

                updatedPropertiesNames = [];

                for(var k = 0, len = keys.length; k < len; k++) {

                    propertyName = keys[k];
                    propertyValue = oldRecord[propertyName];
                    newRecordHasProperty = newRecord.hasOwnProperty(propertyName);

                    // if it is object it means that is relational record
                    if(newRecordHasProperty && this.isObject(propertyValue) && propertyValue.id.toString() !== newRecord[propertyName].id.toString()) {
                        updatedPropertiesNames.push(propertyName);
                    }
                    else if(newRecordHasProperty && propertyValue.toString() !== newRecord[propertyName].toString()) {
                        updatedPropertiesNames.push(propertyName);
                    }
                }

                this.setRecord(id, $.extend({}, oldRecord, newRecord));
            }
            else {
                updatedPropertiesNames = [Object.keys(newRecord)];
                this.setRecord(id, newRecord);
            }

            var clients = this.getRegisteredClients();
            var monitorData = null;

            for(var i = 0, len = clients.length; i < len; i++) {

                monitorData = this.getMonitorData(clients[i]);
                console.log(monitorData)
                if(monitorData === undefined) {
                    monitorData = {
                        updated: {}
                    };
                }
                console.log(monitorData)
                monitorData.updated[id] = {
                    id: id,
                    updatedProperties: updatedPropertiesNames
                };

                this.setMonitorData(clients[i], monitorData);
            }
        },

        recordDelete: function(id) {

            this.removeRecord(id);

            var clients = this.getRegisteredClients();
            var monitorData = null;

            for(var i = 0, len = clients.length; i < len; i++) {

                monitorData = this.getMonitorData(clients[i]);

                if(monitorData) {
                    
                    if(monitorData.updated) {
                        delete monitorData.updated[id];
                    }
                    
                    if(monitorData.created) {
                        monitorData.created.splice(monitorData.created.indexOf(id), 1);
                    }

                    this.setMonitorData(clients[i], monitorData);
                }
            }
        },

        registerForDataMonitor: function(id) {
            if(id && this.getRegisteredClients().indexOf(id) === -1) {
                this.getRegisteredClients().push(id);
            }
        },

        unregisterForDataMonitor: function(id) {

            var index = this.getRegisteredClients().indexOf(id);
            
            if(index !== -1) {
                this.getRegisteredClients().splice(index, 1);
                this.removeMonitorData(id);
            }  
        },

        getUpdatedFormFieldData: function(fieldMonitorData, fieldModelData, fieldSelectData) {

            var record, inputField, updatedRecordKeys, updatedRecordMonitorData;

            if(this.isArray(fieldSelectData) === false) {
                fieldSelectData = [];
            }
            else {
                fieldSelectData = fieldSelectData.slice(0);
            }

            if(fieldMonitorData) {
                
                if(fieldMonitorData.created) {

                   for(var i = 0, len = fieldMonitorData.created.length; i < len; i++) {

                        record = this.getRecord(fieldMonitorData.created[i]);

                        if(fieldMonitorData.updated && fieldMonitorData.updated.hasOwnProperty(record.id)) {

                            record = $.extend({}, record, fieldMonitorData.updated[record.id]);

                            delete fieldMonitorData.updated[id];
                        }

                        fieldSelectData.push(record);
                    }
                }
                else if(fieldMonitorData.updated) {

                    updatedRecordKeys = Object.keys(fieldMonitorData.updated);

                    for(var k = 0, klen = updatedRecordKeys.length; k < klen; k++) {

                        updatedRecordMonitorData = fieldMonitorData.updated[updatedRecordKeys[k]];

                        if(this.isRelationFieldRecordAllowed(fieldModelData, record)) {

                            if(fieldSelectData.length > 0) {

                                for(var l = 0, slen = fieldSelectData.length; l < slen; l++) {

                                    if(updatedRecordMonitorData.id.toString() === fieldSelectData[l].id.toString()) {
                                        fieldSelectData[l] = $.extend({}, fieldSelectData[l], this.getRecord(updatedRecordMonitorData.id));
                                    }
                                }
                            }
                            else {
                                fieldSelectData.push(record);
                            }
                        }
                    }
                }

                return fieldSelectData;
            }
        },

        isRelationFieldRecordAllowed: function(fieldModelData, recordData) {

            var paramKeys = fieldModelData.relationDataLoadParams;

            if(this.isArray(paramKeys)) {

                for(var i = 0, len = paramKeys.length; i < len; i++) {

                    if(fieldModelData.relationDataLoadParams[paramKeys[i]].toString() !== recordData[paramKeys[i]]) {
                        return false;
                    }
                }

                return true;
            }
            else {
                return true;
            }
        }
    }
});