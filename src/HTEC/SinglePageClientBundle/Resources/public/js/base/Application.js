HCM.define({

    name: 'Base.Application',

    construct: function(c) {

        var that = this;

        var config = c;
        var constants;

        var requester = null;
        var localStorage;
        var urlData;

        var historyController;
        var moduleController;

        var moduleContainer = null;
        var loaderElement = null;
        var moduleManager = null;
        var historyManager = null;
        var serviceManager = null;


        this.getConfig = function() {
            return config;
        };

        this.getRequester = function() {
            return requester;
        };

        this.setRequester = function(r) {
            requester = r;
        };

        this.setConstants = function(c) {
            constants = c;
        };

        this.getConstants = function() {
            return constants;
        };

        this.setLocalStorage = function(ls) {
            localStorage = ls;
        };

        this.getLocalStorage = function() {
            return localStorage;
        };

        this.setUrlData = function(data) {
            urlData = data;
        };

        this.getUrlData = function() {
            return urlData;
        };

        this.setModuleController = function(mc) {
            moduleController = mc;
        };

        this.getModuleController = function() {
            return moduleController;
        };

        this.setHistoryController = function(hc) {
            historyController = hc;
        };

        this.getHistoryController = function() {
            return historyController;
        };

        this.setLoaderElement = function(el) {
            loaderElement = el;
        };

        this.getLoaderElement = function() {
            return loaderElement;
        };

        this.getModuleContainer = function() {
            return moduleContainer;
        };

        this.setModuleContainer = function(mc) {
            moduleContainer = mc;
        };

        this.setModuleManager = function(mm) {
            moduleManager = mm;
        };

        this.getModuleManager = function() {
            return moduleManager;
        };

        this.setHistoryManager = function(hm) {
            historyManager = hm;
        };

        this.getHistoryManager = function() {
            return historyManager;
        };

        this.setServiceManager = function(sm) {
            serviceManager = sm;
        };

        this.getServiceManager = function() {
            return serviceManager;
        };

        this.globalErrorEventHandler = function(errorMsg, scriptName, lineNumber, colNumber, error) {

            var message = error.hasOwnProperty('getMessage') ? error.getMessage() : error;

            if(console) {
                console.log('Uncaught error handler.\nMessage: "' + message + '".\nAt script: ' + scriptName + '.\nOn line: ' + lineNumber + ", at col: " + colNumber);
            }

            if(error) {

                var code = error.hasOwnProperty('getCode') ? error.getCode() : 1500;

                that.resolveError({
                    code: code,
                    message: 'Message: ' + message
                });
            }

            return true;
        };
    },

    methods: {

        init: function() {

            window.onerror = this.globalErrorEventHandler;

            this.getRequester().setApp(this);
        },

        startApp: function() {
            this.startController(this.getHistoryController());
            this.startController(this.getModuleController());
        },

        loadDefaultModule: function() {

            var pageModulesNamespace = this.getConfig().getParameter('pageModulesNamespace');
            this.getModuleManager().openPageModule(pageModulesNamespace + '.' + this.getConfig().getParameter('defaultModule'));
        },

        loadModuleFromUrl: function(urlData) {

            var pageModulesNamespace = this.getConfig().getParameter('pageModulesNamespace');
            this.getModuleManager().openPageModule(pageModulesNamespace + '.' + urlData.moduleName);
        },

        compileTemplate: function(templateId, templateData, partials, callBack) {

            var source = $("#" + templateId).html();
            var template = Handlebars.compile(source);

            if(partials && partials.length > 0) {

                var part;

                for(var i = 0; i < partials.length; i++) {
                    part = partials[i];
                    Handlebars.registerPartial(part.dataName, $("#" + part.templateId).html());
                }
            }

            var html = template(templateData);

            if(callBack && this.isFunction(callBack)) {
                callBack.call(this, html);
            }

            return html;
        },

         /**
         *
         * Returns the model instance
         *
         * @param {String} modelClassName Class name of the model
         * @param {Boolean} singleton If singleton is false then new instance of model will be returned. Default value is true. 
         *
         * @return {Object} Returns model instance
         *
         */
        getModel: function(modelClassName, arguments) {

            try {

                var modelInstance = null;

                if(this.getServiceManager().serviceExists(modelClassName)) {
                    modelInstance = this.getServiceManager().getService(modelClassName);
                }
                else {
                    modelInstance = this.getInstance(modelClassName, arguments);
                }

                return modelInstance;
            }
            catch (error) {
                console.log(error);
                this.resolveError(error);
            }
        },

        getController: function(controllerClassName, arguments) {

            try {

                var args = [this];

                if(this.isArray(arguments)) {
                    args = args.concat(arguments);
                }

                var controllerInstance = this.getInstance(controllerClassName, args);
                    controllerInstance.init();

                return controllerInstance;
            }
            catch (error) {
                console.log(error);
                this.resolveError(error);
            }
        },

        startController: function(controller) {
            controller.addEvents();
            controller.startController();
        },

        stopController: function(controller) {
            controller.removeEvents();
            controller.stopController();
        },

        getNotificationHtml: function(type, notificationData) {
            return  this.compileTemplate("noty-" + type + "-notification", notificationData);
        },

        showNotification: function(type, message, loaderId, closeTime) {

            if(type === 'error' && !message) {
                message = "Unknown error ocurred"
            }

            var htmlTemplate = this.getNotificationHtml(
                type,
                {
                    loaderId: loaderId,
                    message: message
                }
            );

            var closeWidth = null;
            var speed = 100;

            if(!loaderId) {
                closeWidth = ['click'];
                speed = 500;
            }

            var timeBeforeClose = this.getConfig().getParameter(type + 'NotificationDuration');

            if(closeTime !== undefined) {
                timeBeforeClose = closeTime;
            }

            var notification = noty({
                text: htmlTemplate,
                type: type,
                dismissQueue: true,
                timeout: timeBeforeClose,
                layout: 'topRight',
                closeWith: closeWidth,
                theme: 'fanfarecation',
                maxVisible: 10,
                animation: {
                    open: 'animated bounceInRight',
                    close: 'animated bounceOutRight',
                    easing: 'swing',
                    speed: speed
                }
            });

            if(loaderId) {
                this.addLoaderNotification(loaderId, notification);
            }

            return notification;
        },

        updateLoaderNotificationProgress: function(loaderId, progressAmmount) {
            $("#loader-" + loaderId).find('.progress_bar').css('width', progressAmmount + "%");
        },

        showLoader: function() {
            this.getLoaderElement().show();
        },

        hideLoader: function() {

            var allRequests = this.getRequester().getAllRequestsOptions();
            var keys = Object.keys(allRequests);
            var doesRequestWithLoaderExists = false;

            if(keys && keys.length > 0) {
                for(var i = 0, len = keys.length; i < len; i++) {
                    if(allRequests[keys[i]].showLoader !== false) {
                        doesRequestWithLoaderExists = true;
                        break;
                    }
                }
            }

            if(doesRequestWithLoaderExists === false) {
                this.getLoaderElement().hide();
            }
        },

        resolveError: function(errorThrown, jqXHR, textStatus) {

            this.hideLoader();

            if(this.isObject(errorThrown)) {

                var code = 0;
                var message = 'Error ocurred';

                if(errorThrown.hasOwnProperty('getCode')) {
                    code = errorThrown.getCode();
                }
                else {
                    code = errorThrown.code;
                }

                if(errorThrown.hasOwnProperty('getMessage')) {
                    message = errorThrown.getMessage();
                }
                else {
                    message = errorThrown.message;
                }

                switch(code) {

                    case 1403:

                    break;

                    case 403:
                    case 500:
                        this.showNotification('error', message + " Code: " + code);
                    break;

                    case 1500:
                        this.showNotification('error', "Application error occurred. Error message: '" + message + ". Error Code: " + code + "'");
                    break;
                }
            }
        }
    }
});