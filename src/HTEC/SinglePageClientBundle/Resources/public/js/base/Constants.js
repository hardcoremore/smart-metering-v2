HCM.createNamespace('Static.Constants', {

    EMAIL_VALIDATION_REGEX: /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/,
    ROLE_NAME_VALIDATION_REGEX: /^[a-zA-Z_]+$/,

    MODULE_LOAD_COMPLETE_EVENT: 'Events.Module.moduleLoadComplete',
    MODULE_LOAD_ERROR_EVENT: 'Events.Module.moduleLoadError',

    MODULE_VIEW_LOAD_COMPLETE_EVENT: 'Events.Module.moduleViewLoadComplete',
    MODULE_VIEW_LOAD_ERROR_EVENT: 'Events.Module.moduleViewLoadError',

    MODULE_READY_EVENT: 'Events.Module.moduleReady',
    MODULE_STARTED_EVENT: 'Events.Module.moduleStarted',
    MODULE_STOPPED_EVENT: 'Events.Module.moduleStopped',
    MODULE_DESTROYED_EVENT: 'Events.Module.moduleDestroyed',
    MODULE_REINITIALIZED_EVENT: 'Events.Module.moduleReinitialized',
    MODULE_SHOWN_EVENT: 'Events.Module.moduleShown',
    MODULE_HIDDEN_EVENT: 'Events.Module.moduleHidden',

    FORM_RESETTED_EVENT: 'Events.FormModule.formResetted',
    FORM_INPUT_CHANGED_EVENT: 'Events.FormModule.formInputChanged',

    COLOR_PALLETE_COLOR_CHANGED_EVENT: 'Events.UI.ColorPallete.colorChanged',

    FABRIC_IMAGE_LOADED_EVENT: 'Events.UI.Fabric.imageLoaded',

    CREATE_OPERATION: 'create',
    UPDATE_OPERATION: 'update',
    PATCH_OPERATION: 'patch',
    DELETE_OPERATION: 'delete',

    UPDATE_USER_PASSWORD_OPERATION: 'updateUserPassword'
});
