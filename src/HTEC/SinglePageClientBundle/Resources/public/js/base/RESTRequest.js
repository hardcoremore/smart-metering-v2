HCM.define({

    name: 'Base.RESTRequest',

    construct: function() {

        var mainApp;
        var requestsOptions = {};
        var currentReadingRequests = {};
        var mainLoader;

        this.GET_REQUEST = 'GET';
        this.POST_REQUEST = 'POST';
        this.PUT_REQUEST = 'PUT';
        this.PATCH_REQUEST = 'PATCH';
        this.DELETE_REQUEST = 'DELETE';

        this.setApp = function(app) {
            mainApp = app;
        };

        this.getApp = function() {
            return mainApp;
        };

        this.setRequestsOptions = function(id, options) {
            requestsOptions[id] = options;
        };

        this.getRequestsOptions = function(id) {
            return requestsOptions[id];
        };

        this.removeRequestOptions = function(id) {
            delete requestsOptions[id];
        };

        this.getAllRequestsOptions = function() {
            return requestsOptions;
        };

        this.setReadingRequest = function(id, options) {
            currentReadingRequests[id] = options;
        };

        this.getReadingRequest = function(id) {

            if(currentReadingRequests.hasOwnProperty(id)) {
                return currentReadingRequests[id];
            }
            else {
                return null;
            }
        };

        this.removeReadingRequest = function(id) {
            delete currentReadingRequests[id];
        };

        this.getAllReadingRequests = function() {
            return currentReadingRequests;
        };

        this.getSerializedParameters = function(params) {
            return JSON.stringify(params);
        };

        this.getReadRequestIdentifier = function(requestOptions) {

            var id = requestOptions.url;

            if(requestOptions.parameters) {

                var keys = Object.keys(requestOptions.parameters);

                if(keys.length > 0) {

                    id = id + '?';

                    for(var i = 0, len = keys.length; i < len; i++) {
                        id += keys[i] + '=' + requestOptions.parameters[keys[i]] + '&';
                    }

                    id = id.slice(0, -1);
                }
            }

            return id;
        };


        this.validateRequestOptions = function(requestType, options) {

            var isOptionsValid = this.isObject(options);

            if(isOptionsValid) {

                isOptionsValid = options.hasOwnProperty('url') && this.isString(options.url) && options.url.length > 0;

                if(isOptionsValid !== true) {
                    this.throwException('Invalid url parameter for '+requestType+' request', 1500, 'kernel');
                }

                if(options.hasOwnProperty('parameters')) {

                    if(this.isObject(options.parameters) !== true) {
                        this.throwException('Invalid parameters for '+requestType+' request', 1500, 'kernel');
                    }
                }
            }
            else {
                this.throwException('Options is not a object. Invalid options argument for '+requestType+' request', 1500, 'kernel');
            }
        };
    },

    methods: {

        generateRequestId: function(requestType) {

            var time = new Date().getTime();
            var requestId = requestType + "-" + (Math.random() * 9999999999999999) + "-" + time;

            return requestId;
        },

        GET: function(options) {

            this.validateRequestOptions(this.GET_REQUEST, options);

            var readRequestIdentifier = this.getReadRequestIdentifier(options);
                options.readRequestIdentifier = readRequestIdentifier;

            // skip reading because the same resource is already being read
            if(this.getReadingRequest(readRequestIdentifier)) {
                console.log('Reading request already exists: ' + readRequestIdentifier);
                return false;
            }
            else {
                this.setReadingRequest(readRequestIdentifier, options);
            }

            var requestId = this.generateRequestId(this.GET_REQUEST);
                options.requestId = requestId;

            this.setRequestsOptions(requestId, options);

            if(options.showLoader !== false) {
                this.getApp().showLoader();
            }

            var that = this;

            $.ajax({
                url: options.url,
                dataType: "json",
                type: "GET",
                data: options.parameters,
                success: function(data, textStatus, jqXHR) {
                    that.requestSuccess(options, data, textStatus, jqXHR);
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    that.requestError(options, jqXHR, textStatus, errorThrown);
                },
                complete: function(jqXHR, textStatus) {
                    that.requestComplete(jqXHR, textStatus, options);
                }
            });
        },

        POST: function(options) {

            this.validateRequestOptions(this.POST_REQUEST, options);

            var requestId = this.generateRequestId('POST');
                options.requestId = requestId;

            this.setRequestsOptions(requestId, options);

            if(options.showLoader !== false) {
                this.getApp().showLoader();
            }

            var that = this;

            $.ajax({

                url: options.url,
                dataType: options.dataType || "json",
                type: "POST",
                data: options.data,
                processData: options.processData,
                contentType: options.contentType,

                success: function(data, textStatus, jqXHR) {
                    that.requestSuccess(options, data, textStatus, jqXHR);
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    that.requestError(options, jqXHR, textStatus, errorThrown);
                },
                complete: function(jqXHR, textStatus) {
                    that.requestComplete(jqXHR, textStatus, options);
                }
            });
        },

        PUT: function(options) {

            this.validateRequestOptions(this.PUT_REQUEST, options);

            var requestId = this.generateRequestId('PUT');
                options.requestId = requestId;

            this.setRequestsOptions(requestId, options);

            if(options.showLoader !== false) {
                this.getApp().showLoader();
            }

            var that = this;

            $.ajax({
                url: options.url,
                dataType: "json",
                type: "PUT",
                data: options.data,
                success: function(data, textStatus, jqXHR) {
                    that.requestSuccess(options, data, textStatus, jqXHR);
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    that.requestError(options, jqXHR, textStatus, errorThrown);
                },
                complete: function(jqXHR, textStatus) {
                    that.requestComplete(jqXHR, textStatus, options);
                }
            });
        },

        PATCH: function(options) {

            this.validateRequestOptions(this.PATCH_REQUEST, options);

            var requestId = this.generateRequestId('PATCH');
                options.requestId = requestId;

            this.setRequestsOptions(requestId, options);

            if(options.showLoader !== false) {
                this.getApp().showLoader();
            }

            var that = this;

            $.ajax({
                url: options.url,
                dataType: "json",
                type: "PATCH",
                data: options.data,
                success: function(data, textStatus, jqXHR) {
                    that.requestSuccess(options, data, textStatus, jqXHR);
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    that.requestError(options, jqXHR, textStatus, errorThrown);
                },
                complete: function(jqXHR, textStatus) {
                    that.requestComplete(jqXHR, textStatus, options);
                }
            });
        },

        DELETE: function(options) {
            
            this.validateRequestOptions(this.DELETE_REQUEST, options);

            var requestId = this.generateRequestId('DELETE');
                options.requestId = requestId;

            this.setRequestsOptions(requestId, options);

            if(options.showLoader !== false) {
                this.getApp().showLoader();
            }

            var that = this;

            $.ajax({
                url: options.url,
                type: "DELETE",
                success: function(data, textStatus, jqXHR) {
                    that.requestSuccess(options, data, textStatus, jqXHR);
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    that.requestError(options, jqXHR, textStatus, errorThrown);
                },
                complete: function(jqXHR, textStatus) {
                    that.requestComplete(jqXHR, textStatus, options);
                }
            });
        },


        requestSuccess: function(options, data, textStatus, jqXHR) {
            
            this.removeRequestOptions(options.requestId);

            if(this.isFunction(options.success)) {
                options.success.call(this, data, textStatus, jqXHR);
            }
        },

        requestError: function(options, jqXHR, textStatus, errorThrown) {

            this.removeRequestOptions(options.requestId);

            var error = jqXHR.responseJSON;

            try {

                error = jqXHR.responseJSON;

                if(!error) {
                    error = JSON.parse(jqXHR.responseText);
                }

                if(!error) {
                    error = errorThrown;
                }
            }
            catch(err) {
                error = err;
            }

            if(this.isString(error)) {

                error = {
                    message: error,
                    code: jqXHR.status
                }
            }
            else if(this.isObject(error) && error.hasOwnProperty('code') === false) {
                error.code = jqXHR.status;
            }

            if(this.isFunction(options.error)) {
                options.error.call(this, error, jqXHR, textStatus);
            }
            else {
                this.throwException('Request error', 1501, 'http');
            }
        },

        requestComplete: function(jqXHR, textStatus, options) {

            if(options.hasOwnProperty('readRequestIdentifier')) {
                this.removeReadingRequest(options.readRequestIdentifier);
            }

            this.getApp().hideLoader();
        }
    }
});