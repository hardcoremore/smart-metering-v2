HCM.define({

    name: 'Base.Module',

    construct: function(app, permissions) {

        var mainApp = app;
        var modulePermissions = permissions;
        var moduleElement;
        var moduleProperties = {
            allowMultiInstance: true
        };

        var moduleViewLoaded = false;
        var moduleLoaded = false;
        var operationType = null;

        var moduleData = null;

        var isPageModule = true;
        var initialized = false;
        var isModuleRunning = false;

        var moduleAskForStopCallback = null;

        this.setApp = function(app) {
            mainApp = app;
        };

        this.getApp = function() {
            return mainApp;
        };

        this.getPermissions = function() {
            return modulePermissions;
        };

        this.updatePermissions = function(permissions) {
            modulePermissions = permissions;
        };

        this.setModuleElement = function(mel) {
            moduleElement = mel;
        };

        this.getModuleElement = function() {
            return moduleElement;
        };

        this.setModuleLoaded = function(loaded) {
            moduleLoaded = loaded;
        };

        this.getModuleLoaded = function() {
            return moduleLoaded;
        };

        this.setModuleViewLoaded = function(loaded) {
            moduleViewLoaded = loaded;
        };

        this.getModuleViewLoaded = function() {
            return moduleViewLoaded;
        };

        this.setModuleData = function(data) {
            moduleData = data;
        };

        this.getModuleData = function() {
            return moduleData;
        };

        this.setInitialized = function(inited) {
            initialized = inited;
        };

        this.getInitialized = function() {
            return initialized;
        };

        this.setModuleProperty = function(key, value) {
            moduleProperties[key] = value;
        };

        this.setIsPageModule = function(pm) {
            isPageModule = pm;
        };

        this.getIsPageModule = function() {
            return isPageModule;
        };

        this.getModuleProperty = function(key) {

            if(moduleProperties.hasOwnProperty(key)) {
                return moduleProperties[key];
            }

            return null;
        };

        this.setIsModuleRunning = function(imr) {
            isModuleRunning = imr;
        };

        this.getIsModuleRunning = function() {
            return isModuleRunning;
        };

        this.setModuleAskForStopCallback = function(callback) {

            if(this.isFunction(callback)) {
                moduleAskForStopCallback = callback;
            }
        };

        this.getModuleAskForStopCallback = function() {
            return moduleAskForStopCallback;
        };        

        this.getModuleName = function() {
            return 'Module';
        };
    },

    methods: {

        initModule: function() {
            this.resolveModulePermissions(this.getPermissions());
            this.setControls();
            this.setInitialized(true);
        },

        startModule: function() {

            this.setIsModuleRunning(true);

            var ev = $.Event(this.getApp().getConstants().MODULE_STARTED_EVENT, {module:this});
            $(document).trigger(ev);
        },

        allowStopModule: function() {
            return true;
        },

        askForModuleStop: function(moduleStopCallback) {
            this.setModuleAskForStopCallback(moduleStopCallback);
        },

        stopModule: function() {

            this.setIsModuleRunning(false);

            var ev = $.Event(this.getApp().getConstants().MODULE_STOPPED_EVENT, {module:this});
            $(document).trigger(ev);
        },

        destroyModule: function() {

            this.hideModule();
            this.stopModule();
            this.removeControls();

            this.getModuleElement().remove();
            this.setModuleElement(null);

            this.setModuleData(null);

            var destroyedEvent = this.getApp().getConstants().MODULE_DESTROYED_EVENT;

            this.setApp(null);

            var ev = $.Event(destroyedEvent, {module:this});
            $(document).trigger(ev);
        },

        setControls: function(){},

        removeControls: function(){},

        resolveModulePermissions: function(permissions) {},

        reinitializeModule: function(data, permissions) {

            this.setModuleData(data);

            if(permissions) {
                this.updatePermissions(permissions);
            }

            var ev = $.Event(this.getApp().getConstants().MODULE_REINITIALIZED_EVENT, {module:this});
            $(document).trigger(ev);
        },

        showModule: function() {

            this.getModuleElement().show();

            var ev = $.Event(this.getApp().getConstants().MODULE_SHOWN_EVENT, {module:this});
            $(document).trigger(ev);
        },

        hideModule: function() {

            this.getModuleElement().hide();

            var ev = $.Event(this.getApp().getConstants().MODULE_HIDDEN_EVENT, {module:this});
            $(document).trigger(ev);
        },

        isModuleVisible: function() {

            if(this.getModuleElement()) {
                return this.getModuleElement().is(':visible');
            }

            return false;
        },

        /*
         * This method should be implemented in child class
         */
        hasHistory: function() {
            return false;
        },

        /*
         * This method should be implemented in child class
         */
        historyBack: function() {},

        /*
         * This method should be implemented in child class
         */
        updateModuleState: function(stateData) {},

        /*
         * This method should be implemented in child class
         */
        resetModule: function() {},

        /*
         * This method is fired on active module when window is resized
         */
        updateModuleDimensions: function() {}
    }
});
