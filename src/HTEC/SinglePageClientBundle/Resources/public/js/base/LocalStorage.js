HCM.define({
    
    name: 'Base.LocalStorage',

    construct: function() {},

    methods: {

        /**
         *
         * Get the value from local storage by key
         *
         * @param {String} key
         *
         * @return {Object|String|Number} Returns stored value
         *
         */
        get: function(key) {

            if(window.localStorage.hasOwnProperty(key)) {

                var item = JSON.parse(window.localStorage[key]);
                
                if(item && typeof item === "object") {

                    var timestamp = new Date().getTime();
                    
                    if(item.hasOwnProperty("expiresInTimestamp")) {
                        
                        if(item.expiresInTimestamp > timestamp) {
                            return item.value;
                        }
                        else {
                            window.localStorage[key] = null;
                            return null;
                        }   
                    }

                    return item;
                }
                else {
                    
                    item = window.localStorage[key];
                    
                    return (item === "null") ? null : item;
                }
            } 
            else {
                return null;
            }
        },

        /**
         *
         * Set the value to local storage
         *
         * @param {String} key A key that references the value
         * @param {Object|String|Number} value A Value to store in local storage
         * @param {Number} expiresIn Amount of time in microseconds for how long the value will be valid. 
         *                           If times expires the get method will return null.
         *
         */
        set: function(key, value, expiresIn) {
            
            var item = null;

            if(expiresIn > 0) {

                window.localStorage[key] = JSON.stringify({
                    expiresInTimestamp: new Date().getTime() + expiresIn,
                    value: value
                });
            }
            else {

                if(typeof value === "object") {
                    value = JSON.stringify(value);
                }

                window.localStorage[key] = value;
            }
        }
    }
});
