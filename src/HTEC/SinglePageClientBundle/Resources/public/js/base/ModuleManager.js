HCM.define({
    
    name: 'Base.ModuleManager',

    construct: function(app) {

        var application = app;

        var pageModules = {};

        var activeModule = null;
        var moduleViews = {};

        var previousModule = null;

        this.setApp = function(app) {
            application = app;
        };

        this.getApp = function() {
            return application;
        };

        this.setModuleView = function(moduleClassName, view) {
            moduleViews[moduleClassName] = view;
        };

        this.getModuleView = function(moduleClassName) {
            return moduleViews[moduleClassName];
        };

        this.getPageModules = function() {
            return pageModules;
        };

        this.getActiveModule = function() {
            return activeModule;
        };

        this.setActiveModule = function(m) {

            if(activeModule) {
                previousModule = activeModule;
            }

            activeModule = m;
        };

        this.getPreviousModule = function() {
            return previousModule;
        };

        this.registerPageModule = function(module) {
            pageModules[module.getNamespace()] = module;
        };

        this.getPageModule = function(moduleNamespace) {

            if(pageModules.hasOwnProperty(moduleNamespace)) {
                return pageModules[moduleNamespace];
            }

            return null;
        };

        this.unregisterModule = function(moduleNamespace) {
            delete pageModules[moduleNamespace];
        };
    },

    methods: {
        /**
         *
         *  This function is used to display module as if module is whole page
         *  
         *  If module with suplied class name is already loaded it will not be loaded again.
         *
         *  Currently loaded module will be hidden and suplied module will be shown.
         * 
         */
        openPageModule: function(moduleClassName, moduleData, reinitialize, updateUrl) {

            try {

                var module = this.getPageModule(moduleClassName);

                //if module is loaded already
                if(module) {

                    if(reinitialize === true) {
                        module.reinitializeModule(moduleData);
                    }
                    else {

                        if(this.getActiveModule() && this.getActiveModule().getNamespace() !== moduleClassName) {

                            if(this.getActiveModule().getIsModuleRunning()) {
                                this.getActiveModule().stopModule();    
                            }                            

                            this.getActiveModule().hideModule();
                        }

                        this.setActiveModule(module);

                        module.startModule();
                        module.showModule();
                    }

                    if(updateUrl !== false) {
                        this.getApp().getHistoryManager().goToHashUrl(this.getActiveModule().getModuleName());
                    }
                }
                else {
                    module = this.initializeModule(moduleClassName, moduleData, true);
                    this.loadModule(module);
                }

                return module;
            }
            catch(error) {                

                if(error.hasOwnProperty('getMessage')) {
                    console.log(error.getMessage());
                }
                else {
                    console.log(error);
                }
            }
        },

        /**
         *
         * Initialize module. Get permissions, get instalnce, inject permissions and load module view
         * 
         */
        initializeModule: function(moduleClassName, moduleData, isPageModule, autoStart) {

            // sets autoStart default value to true
            autoStart = autoStart !== false;

            var definitionParts = moduleClassName.split('.');

            // pass only module name without namespace
            var permissions = this.getApp().getModulePermissions(definitionParts[definitionParts.length - 1]);

            var module = this.getInstance(moduleClassName, [this.getApp(), permissions]);
                module.setModuleProperty('autoStartOnLoad', autoStart);

                module.setIsPageModule(isPageModule);
                module.setModuleData(moduleData);

            return module;
        },

        loadModule: function(module) {

            var moduleView = this.getModuleView(module.getNamespace());

            if(moduleView) {

                this.initializeModuleView(module, moduleView);
                
                var moduleContainer = this.getModuleContainer(module);
                    
                    this.addModuleViewToDOM(module, moduleContainer);

                var ev = $.Event(this.getApp().getConstants().MODULE_READY_EVENT, {module:module});
                $(document).trigger(ev);
            }
            else {
                this.loadModuleView(module);
            }
        },

        loadModuleView: function(module, options) {

            var that = this;
            var constants = this.getNamespaceValue('Static.Constants');

            var requestId = this.getApp().getRequester().generateRequestId();
            options = options || {};
            options.showLoader = true;

            this.getApp().getRequester().setRequestsOptions(requestId, options);
            that.getApp().showLoader();

            $.ajax({

                url: this.getApp().getConfig().getModuleViewBaseUrl() + '/' + module.getModuleName() + '/view',
                type: 'GET',
                dataType: 'html',

                error: function(jqXHR, textStatus, errorThrown) {

                    that.getApp().getRequester().removeRequestOptions(requestId);

                    var eventData = {
                        module:module,
                        error:errorThrown
                    };

                    var ev = $.Event(constants.MODULE_VIEW_LOAD_ERROR_EVENT, eventData);
                    $(document).trigger(ev);
                },

                success: function(data, textStatus, jqXHR) {

                    that.getApp().getRequester().removeRequestOptions(requestId);

                    var eventData = {
                        module:module,
                        moduleView: data,
                        jqXHR:jqXHR
                    };

                    var ev = $.Event(constants.MODULE_VIEW_LOAD_COMPLETE_EVENT, eventData);
                    $(document).trigger(ev);
                }
            });
        },

        initializeModuleView: function(module, moduleView) {

            var moduleElement = $(moduleView).first();
                moduleElement.attr('data-module-namespace', module.namespace);

            module.setModuleElement(moduleElement);
            module.setModuleViewLoaded(true);

            var self = this;

            // check if module element holds some other modules to be loaded
            moduleElement.find('[data-module]').each(function(index) {

                var moduleHolder = $(this);

                if(moduleHolder.data('module-holder')) {
                    moduleHolder = $(moduleHolder.data('module-holder'));
                }

                moduleHolder.data('page-module', false);

                var moduleClassName = moduleHolder.data('module');
                var moduleData = moduleHolder.data('module-data');

                if(moduleClassName && moduleClassName.length > 0) {

                    var module = self.initializeModule(moduleClassName, moduleData, false);
                        module.setModuleProperty('moduleHolder', moduleHolder);

                    self.loadModule(module);
                }
            });
        },


         /**
         *
         * Returns the module container element.
         *
         * @param {module} Object Module instance.
         * 
         * @param {ev} Object Event object that triggered loading module.
         *
         * @return {Object} Returns module container element.
         *
         */

        getModuleContainer: function(module) {

             // default module container is in application instance
            var moduleContainer = this.getApp().getModuleContainer();

            // search for module container in module view or module properties
            var moduleHolder = module.getModuleElement().data('module-holder') || module.getModuleProperty('moduleHolder');

            if(moduleHolder) {

                if(this.isString(moduleHolder)) {
                    moduleContainer = $(moduleHolder);
                }
                else if(this.isObject(moduleHolder) && moduleHolder.length > 0) {
                    moduleContainer = moduleHolder;   
                }
                else {
                    this.throwException('Module holder is invalid', 1500, 'kernel');
                }
            }

            return moduleContainer;
        },

        addModuleViewToDOM: function(module, moduleContainer) {

            if(!moduleContainer || moduleContainer.length < 1) {
                that.throwException('Module container invalid at Controllers.Modudle:moduleViewLoadCompleteEventHandler');
            }

            module.setModuleProperty('container', moduleContainer);

             // this will only delete module from module's container
            if(module.getModuleProperty('allowMultiInstance') === false) {

                var currentModuleView = moduleContainer.find('[data-module-namespace="'+module.getNamespace()+'"]');

                // remove the old view
                if(currentModuleView) {
                    currentModuleView.remove();
                }
            }

            moduleContainer.append(module.getModuleElement());
        },

        startModule: function(module) {

            if(module.getInitialized() === false) {
                module.initModule();
            }

            if(module.getIsModuleRunning() !== true) {
                module.startModule();
            }
        },

        stopModule: function(module) {

            if(module.getIsModuleRunning() === true) {
                module.stopModule();
            }
        },

        /*
         * hide currently active module, register new module, set new module as active,
         * and show module
         *  we can have only one module active at the time
         */
        activatePageModule: function(module) {

            if(this.getActiveModule()) {

                if(this.getActiveModule().getIsModuleRunning()) {
                    this.getActiveModule().stopModule();
                }

                this.getActiveModule().hideModule();
            }

            this.registerPageModule(module);
            this.setActiveModule(module);

            this.getApp().getHistoryManager().goToHashUrl(module.getModuleName());

            var urlData = this.getApp().getHistoryManager().parseUrl(window.location.href);

            module.updateModuleState(urlData);

            if(module.getModuleProperty('autoStartOnLoad') === true) {
                this.startModule(module);
                module.showModule();
            }
        },

        destroyModule: function(module) {

            module.setModuleProperty('container', null);

            if(module.getIsPageModule() === true) {

                var activeModule = this.getActiveModule();

                if(activeModule === module) {
                    this.setActiveModule(null);
                }

                this.unregisterModule(module.getNamespace());
            }

            module.destroyModule();
        }
    }
});