HCM.define({
    
    name: 'Base.HistoryManager',

    construct: function(app) {

        var application = app;

        this.setApp = function(app) {
            application = app;
        };

        this.getApp = function() {
            return application;
        };
    },

    methods: {

        goToHashUrl: function(hashValue) {

            var urlData = this.parseUrl(window.location.href);

            if(hashValue !== urlData.hashValue) {

                var url = urlData.baseUrl + "#" + hashValue;

                var appName = this.getApp().getConfig().getParameter('appName');

                history.pushState(urlData.moduleName, appName + " - " + hashValue, url);
            }
        },

        parseUrl: function(url) {

            var hashIndex = url.indexOf("#");
            var moduleName = '';
            var baseUrl = '';
            var options = [];

            if(hashIndex !== -1) {

                baseUrl = url.substring(0, hashIndex);

                var hashValue = url.substring(hashIndex + 1, url.length);
                var matches = hashValue.match(/\/(\w+)/g);

                if(matches) {
                    for(var i = 0, len = matches.length; i < len; i++) {
                        options.push(matches[i].substring(1));
                    }    
                }

                var moduleNameLengthIndex = hashValue.indexOf('/');

                if( moduleNameLengthIndex === -1) {
                    moduleNameLengthIndex = url.length;
                }

                moduleName = hashValue.substring(0, moduleNameLengthIndex);
            }

            return {
                baseUrl: baseUrl,
                moduleName: moduleName,
                hashValue: hashValue,
                options: options
            }
        },

        resolveUrl: function(url) {

            var urlData = this.parseUrl(url);

            var moduleManager = this.getApp().getModuleManager();

            if(moduleManager.getActiveModule() && urlData.moduleName === moduleManager.getActiveModule().getModuleName()) {

                var currentModuleName = moduleManager.getActiveModule().getModuleName();

                moduleManager.getActiveModule().updateModuleState(urlData);
            }
            else {

                // @to-do implement loading same module from another namespace programatically
                var pageModulesNamespace = this.getApp().getConfig().getParameter('pageModulesNamespace');

                moduleManager.openPageModule(pageModulesNamespace + '.' + urlData.moduleName, null, false, false);
            }
        }
    }
});