HCM.define({

    name: 'Base.Bootstrap',

    construct: function() {

        var parameters;
        var config;

        var requester;
        var application;
        var app;

        var serviceManagerConfig;
        var serviceManager;

        this.setParameters = function(params) {
            parameters = params;
        };

        this.getParameters = function() {
            return parameters;
        };

        this.setConfig = function(c) {
            config = c;
        };

        this.getConfig = function() {
            return config;
        };

        this.setRequester = function(r) {
            requester = r;
        };

        this.getRequester = function() {
            return requester;
        };

        this.setApp = function(app) {
            application = app;
        };

        this.getApp = function() {
            return application;
        };

        this.setServiceManagerConfig = function(c) {
            serviceManagerConfig = c;
        };

        this.getServiceManagerConfig = function() {
            return serviceManagerConfig;
        };

        this.setServiceManager = function(manager) {
            serviceManager = manager;
        };

        this.getServiceManager = function() {
            return serviceManager;
        };
    },

    methods: {

        initParameters: function() {

            var globalParameters = HCM.getNamespaceValue('Global.Parameters');

            this.setParameters(globalParameters);

            return globalParameters;
        },

        initConfig: function() {

            var config = HCM.getInstance('Base.Config', [this.getParameters()]);

            this.setConfig(config);

            return config;
        },

        initRequester: function() {

            var requester = HCM.getInstance('Base.RESTRequest');

            this.setRequester(requester);
            
            return requester;
        },

        initServiceManagerConfig: function() {

            var baseApplicationServices = HCM.getNamespaceValue('Services.Base.Application');
            var baseModelServices = HCM.getNamespaceValue('Services.Base.Models');

            var combinedParams = $.extend({}, baseApplicationServices, baseModelServices);

            this.setServiceManagerConfig(combinedParams);

            return combinedParams;
        },

        initServiceManager: function() {

            var serviceManager = HCM.getInstance('Base.ServiceManager', [this.getServiceManagerConfig(), this.getConfig()]);
                serviceManager.setServiceToCache('single_page_client.config', this.getConfig());
                serviceManager.setServiceToCache('single_page_client.requester.rest', this.getRequester());

            this.setServiceManager(serviceManager);

            return serviceManager;
        },

        initApp: function() {

        },

        start: function() {
            
            this.initParameters();
            this.initConfig();
            this.initRequester();
            this.initServiceManagerConfig();
            this.initServiceManager();
            this.initApp();

            this.getApp().init();
            this.getApp().startApp();
        }
    }
});