<?php

namespace HTEC\SinglePageClientBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Question\ConfirmationQuestion;

class ConfigureParametersCommand extends ContainerAwareCommand
{
    private $allParameters;
    private $requiredParameters;

    private $fileRelativePath;
    private $fileContent;

    private $bundle;

    public function setFileRelativePath($path)
    {
        $this->fileRelativePath = $path;
    }

    public function getFileRelativePath()
    {
        return $this->fileRelativePath;
    }

    public function setBundle($bundle)
    {
        $this->bundle = $bundle;
    }

    public function getBundle()
    {
        return $this->bundle;
    }

    public function setFileContent($content)
    {
        $this->fileContent = $content;
    }

    public function getFileContent()
    {
        return $this->fileContent;
    }

    public function setRequiredParameters(array $params)
    {
        return $this->requiredParameters = $params;
    }

    public function getRequiredParameters()
    {
        return $this->requiredParameters;
    }

    protected function configure()
    {
        $this->setName('site:configure:parameters')
             ->setDescription('Configure Java Script parameters for single page site application.')
             ->addOption('bundle', null, InputOption::VALUE_REQUIRED, 'The bundle name where class will be generated/loaded')
             ->addOption(
                'path',
                '-p',
                InputOption::VALUE_REQUIRED,
                'Path of the parameters file'
            );

        $this->allParameters = [];

        $this->setFileRelativePath('/Resources/public/js/config/parameters-local.js');
        $this->setRequiredParameters(['env', 'baseUrl', 'apiBaseUrl', 'moduleViewBaseUrl', 'defaultModule']);
    }

    protected function interact(InputInterface $input, OutputInterface $output)
    {
        $questionHelper = $this->getHelperSet()->get('question');

        $bundleNames = array_keys($this->getContainer()->get('kernel')->getBundles());

        $bundleQuestionText = 'Please enter bundle name where file will be generated/loaded from';

        if($input->getOption('bundle'))
        {
            $bundleQuestionText = $bundleQuestionText . ' <comment>(' . $input->getOption('bundle') . ')</comment>: ';
        }
        else
        {
            $bundleQuestionText = $bundleQuestionText . ': ';
        }

        $output->writeln('');

        $bundleNameQuestion = new Question($bundleQuestionText, $input->getOption('bundle'));
        $bundleNameQuestion->setAutocompleterValues($bundleNames);

        $fileContents = '';
        $bundle = null;

        while (true)
        {
            $bundleName = $questionHelper->ask($input, $output, $bundleNameQuestion);

            try
            {
                $bundle = $this->getContainer()->get('kernel')->getBundle($bundleName);
                break;
            }
            catch(\Exception $e)
            {
                $output->writeln(sprintf('<bg=red>The following error occurred: "%s".</>', $e->getMessage()));
            }
        }

        $this->setBundle($bundle);

        if(is_string($this->getFileContent()) === false || strlen($this->getFileContent()) === 0)
        {
            $fileFullPath = $bundle->getPath() . $this->getFileRelativePath();

            if(file_exists($fileFullPath) === true)
            {
                $output->writeln("<info>Configuration file already exists in bundle:</info> <comment>$bundleName</comment>");
                $this->setFileContent(file_get_contents($fileFullPath));
            }
            else
            {
                $output->writeln("Configuration file does not exists in bundle <comment>$bundleName</comment>.");
                $output->writeln("New configuration file will be created...");
            }    
        }

        if(is_string($this->getFileContent()) === true AND strlen($this->getFileContent()) > 0)
        {
            $this->parseParametersFromFile($this->getFileContent(), $input, $output);
        }
        else
        {
            foreach($this->getRequiredParameters() AS $parameter)
            {
                $this->getParameterFromUser($input, $output, $parameter);
            }
        }


        $question = new ConfirmationQuestion(
            'Do you want to add new parameters to the config n/Y? ',
            false,
            '/^Y$/'
        );

        $answer = $questionHelper->ask($input, $output, $question);
        $isNewParameterAdded;

        if($answer === true)
        {
            while(true)
            {
                $isNewParameterAdded = $this->getParameterFromUser($input, $output, $parameterKey = '');

                if($isNewParameterAdded === false)
                {
                    $quitAddingParamsQuestion = new ConfirmationQuestion(
                        'Do you want to stop adding new parameters to the config n/Y? ',
                        false,
                        '/^Y$/'
                    );

                    $answer = $questionHelper->ask($input, $output, $quitAddingParamsQuestion);

                    if($answer === true)
                    {
                        break;
                    }
                    else
                    {
                        continue;
                    }
                }
            }
        }
    }

    protected function parseParametersFromFile(string $fileContents, InputInterface $input, OutputInterface $output)
    {
        $lines = explode("\n", $fileContents);

        $paramKey = '';
        $paramValue = '';

        $delimiterIndex = 0;
        $isNewParameterAdded;

        foreach ($lines AS $value)
        {
            $delimiterIndex = strpos($value, ':');

            if($delimiterIndex > 0)
            {
                $paramKey = trim(substr($value, 0, $delimiterIndex));
                $paramValue = substr($value, $delimiterIndex + 1);

                $this->getParameterFromUser($input, $output, $paramKey, $paramValue);
            }
        }
    }

    protected function getParameterFromUser(InputInterface $input, OutputInterface $output, string $parameterKey = '', string $parameterValue = '')
    {
        $questionHelper = $this->getHelperSet()->get('question');

        $parameterKeyQuestion = new Question(
            "Please enter name for new parameter: "
        );

        $parameterValue = trim($parameterValue);
        $parameterValue = trim($parameterValue, ",");
        $parameterValue = trim($parameterValue, '"');
        $parameterValue = trim($parameterValue, "'");

        $parameterValueQuestionText = 'Please enter new value for parameter <info>:parameterKey</info>: ';

        if(strlen($parameterValue) > 0)
        {
            $parameterValueQuestionText = rtrim(rtrim($parameterValueQuestionText), ':');
            $parameterValueQuestionText = str_replace(':parameterKey', $parameterKey, $parameterValueQuestionText);
            $parameterValueQuestionText = $parameterValueQuestionText . " <comment>($parameterValue):</comment> ";

            $valueQuestion = new Question($parameterValueQuestionText, $parameterValue);
        }

        $parameterType = '';

        $parameterTypeQuestion = null;

        $parameterTypeQuestionText = 'Please enter type for parameter <info>:parameterKey</info> <comment>(string):</comment> ';
        $parameterTypes = ['string', 'integer', 'boolean', 'array'];

        while (true)
        {
            // if key is empty ask for parameter key
            if(strlen($parameterKey) === 0)
            {
                $parameterKey = $questionHelper->ask($input, $output, $parameterKeyQuestion);

                // if parameter key is empty return false to ask user if he wants to stop entering parameters
                if(strlen($parameterKey) === 0)
                {
                   return false;
                }
            }

            // ask for parameter value
            if(strlen($parameterValue) > 0)
            {
                $newParameterValue = $questionHelper->ask($input, $output, $valueQuestion);    
            }
            else
            {
                $parameterValueQuestionText = str_replace(':parameterKey', $parameterKey, $parameterValueQuestionText);

                $valueQuestion = new Question($parameterValueQuestionText, $parameterValue);
                $newParameterValue = $questionHelper->ask($input, $output, $valueQuestion);
            }
            
            // validate parameter value. Value can not be empty
            if(strlen($parameterValue) === 0 && strlen($newParameterValue) === 0)
            {
                $output->writeln('<error>Value can not be empty</error>');
                continue;
            }
            else if(strlen($newParameterValue) === 0)
            {
                $newParameterValue = $parameterValue;
            }

            $newParameterValue = trim($newParameterValue);

            // if new value is different from old value ask for parameter type
            // else try to fiture parameter type from value
            if($newParameterValue !== $parameterValue)
            {
                $parameterTypeQuestion = new Question(
                    str_replace(':parameterKey', $parameterKey, $parameterTypeQuestionText),
                    'string'
                );

                $parameterTypeQuestion->setAutocompleterValues($parameterTypes);

                while(true)
                {
                    $parameterType = $questionHelper->ask($input, $output, $parameterTypeQuestion);

                    if(in_array($parameterType, $parameterTypes) !== true)
                    {
                        $output->writeln(
                            '<error>Parameter type ' . $parameterType . ' is invalid.</error> ' . 
                            'Available parameter types are: <comment>' . implode(', ', $parameterTypes) . '</comment>'
                        );
                        continue;
                    }
                    else
                    {
                        break;
                    }    
                }
                
            }
            else if(strpos($parameterValue, '[') === 0)
            {
                $parameterType = 'array';   
            }
            else if(is_numeric($parameterValue))
            {
                $parameterType = 'integer';   
            }
            else if($parameterValue === 'true' || $parameterValue === 'false')
            {
                $parameterType = 'boolean';   
            }
            else
            {
                $parameterType = 'string';
            }

            // save formated parameter value
            $this->allParameters[$parameterKey] = $this->getFormattedParameterValue($parameterType, $newParameterValue);

            break;
        }

        return true;
    }

    protected function getFormattedParameterValue(string $parameterType, string $parameterValue)
    {
        switch($parameterType)
        {
            case 'string':
                return '"' . $parameterValue . '"';
            break;

            case 'array':
                return '[' . $parameterValue . ']';
            break;

            case 'integer':
            case 'boolean':
            default:
                return $parameterValue;
            break;
        }
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $compiledParameters = '';

        foreach($this->allParameters AS $key => $value)
        {
            $compiledParameters .= "    " . $key . ': ' . $value . ",\n";
        }

        $compiledParameters = rtrim($compiledParameters, "\n");
        $compiledParameters = rtrim($compiledParameters, ',');

        $parametersWithTemplate = $this->getParametersWithTemplate($compiledParameters);

        $fileFullPath = $this->getBundle()->getPath() . $this->getFileRelativePath();

        try
        {
            file_put_contents($fileFullPath , $parametersWithTemplate);

            $output->writeln('');
            $output->writeln('<info>Configuring parameters complete</info>');
            $output->writeln(str_repeat('=', 32));
            $output->writeln('');

            $output->writeln('File is saved to following path: <comment>'.$fileFullPath.'</comment>');
            $output->writeln('');
        }
        catch(\Exception $error)
        {
            $output->writeln('<error>Writing file failed!. Path: ' . $fileFullPath . '</error>');
            $output->writeln('<error>Error message: ' . $error->getMessage() . '</error>');
        }
    }

    protected function getParametersWithTemplate(string $compiledParamters)
    {
        $template = <<<EOT
// This is AUTO generated file. Please do not modify, instead use command for generating configuration file
HCM.createNamespace('Local.Parameters', {
{{:parameters}}
});
EOT;
        $template = str_replace('{{:parameters}}', $compiledParamters, $template);

        return $template;
    }
}
