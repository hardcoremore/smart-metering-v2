<?php

namespace HTEC\BaseAPIBundle\Helpers;

use HTEC\BaseModelBundle\Exception\Form\InvalidFormDataException;

class ExtractFormErrorsHelper
{
    public function getErrorMessages(InvalidFormDataException $exception)
    {
        $formErrors = [];

        $form = $exception->getForm();
        $errorMessages = $exception->getErrorMessages();

        if(strlen($exception->getFormErrorMessage()) > 0 )
        {
            $formErrors['message'] = $exception->getFormErrorMessage();
        }
        else if($form && $form->getErrors() && $form->getErrors()->current())
        {
            $formErrors['message'] = $form->getErrors()->current()->getMessage();
        }
        else if($exception->getMessage())
        {
            $formErrors['message'] = $exception->getMessage();
        }
        else
        {
            $formErrors['message'] = "Form is invalid";
        }

        if($form)
        {
            $formErrors['message'] .= ' Form name: ' .  implode(' ', explode('_', $form->getName()));
        }

        if($errorMessages)
        {
            $formErrors['errors'] = [];

            foreach ($errorMessages AS $key => $value)
            {
                if(is_object($errorMessages[$key]))
                {
                    if($errorMessages[$key]->getErrors()->current())
                    {
                        $formErrors['errors'][$key] = [];

                        foreach($errorMessages[$key]->getErrors() AS $error)
                        {
                            $formErrors['errors'][$key][] = $error->getMessage();
                        }
                    }
                }
                else if(is_string($errorMessages[$key]))
                {
                    if($formErrors['errors'][$key])
                    {
                        $formErrors['errors'][$key][] = $errorMessages[$key];
                    }
                    else
                    {
                        $formErrors['errors'][$key] = [$errorMessages[$key]];
                    }
                }
            }
        }

        return $formErrors;
    }
}