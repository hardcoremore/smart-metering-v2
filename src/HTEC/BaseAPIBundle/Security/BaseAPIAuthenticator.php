<?php

namespace HTEC\BaseAPIBundle\Security;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationFailureHandlerInterface;
use Symfony\Component\Security\Http\Logout\LogoutSuccessHandlerInterface;
use Symfony\Component\Security\Http\Authentication\SimpleFormAuthenticatorInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

use FOS\RestBundle\View\View;

use HTEC\BaseAPIBundle\Exception\Security\FailedLoginsException;

class BaseAPIAuthenticator implements SimpleFormAuthenticatorInterface,
                                      AuthenticationSuccessHandlerInterface,
                                      AuthenticationFailureHandlerInterface,
                                      LogoutSuccessHandlerInterface
{
    const FAILED_LOGINS_NUMBER = '_security.failed_logins_number';
    const FAILED_LOGINS_WAIT_TIME = '_security.failed_logins_wait_time';
    const FAILED_LOGINS_TIME_MSG = "You have failed to login more than 5 times. You have to wait 1 minute in order to try to login again.";

    protected $container;
    protected $encoderFactory;
    protected $currentRequest;

    protected $userModel;
    protected $permissionModel;

    public function __construct(ContainerInterface $container,
                                EncoderFactoryInterface $encoderFactory,
                                RequestStack $requestStack)
    {
        $this->container = $container;
        $this->encoderFactory = $encoderFactory;
        $this->currentRequest = $requestStack->getCurrentRequest();
    }

    public function setUserModel($userModel)
    {
        $this->userModel = $userModel;
    }

    public function getUserModel()
    {
        return $this->userModel;   
    }

    public function setPermissionModel($permissionModel)
    {
        $this->permissionModel = $permissionModel;
    }

    public function getPermissionModel()
    {
        return $this->permissionModel;   
    }

    public function authenticateToken(TokenInterface $token, UserProviderInterface $userProvider, $providerKey)
    {
        $session = $this->currentRequest->getSession();

        if($session)
        {
            $failedLoginsTime = $session->get(self::FAILED_LOGINS_WAIT_TIME);
        
            // check if user exceeds number of login attemps
            if($failedLoginsTime) {

                // check if user can try to login again
                if(($failedLoginsTime + 60) < time()) {
                    $session->set(self::FAILED_LOGINS_WAIT_TIME, NULL);
                    $session->set(self::FAILED_LOGINS_NUMBER, NULL);
                }
                else {
                    throw new FailedLoginsException(self::FAILED_LOGINS_TIME_MSG);
                }
            }
        }

        try 
        {
            $user = $userProvider->loadUserByUsername($token->getUsername());
        } 
        catch (UsernameNotFoundException $e)
        {
            $this->updateFailedLoginsNumber();
            throw new AuthenticationException('Invalid username or password');
        }

        if($user->getStatus() !== 'active')
        {
            throw new AuthenticationException('Account is not active');
        }

        $encoder = $this->encoderFactory->getEncoder($user);
        $passwordValid = $encoder->isPasswordValid(
            $user->getPassword(),
            $token->getCredentials(),
            $user->getSalt()
        );

        if($passwordValid)
        {
            if($this->getUserModel())
            {
                $user->setPermissions($this->getPermissionModel()->getUserPermissions($user));

                // store user permissions in session because it just wont serialize :(
                // symfony is calling 2 unserialize method on two user instances and I can not get my head around it.
                // it is stupid
                $this->container->get('session')->set('user_permissions', $user->getPermissions());
            }

            return new UsernamePasswordToken(
                $user,
                $user->getPassword(),
                $providerKey,
                $user->getRoles()
            );
        }
        else{
            $this->updateFailedLoginsNumber();
        }

        throw new AuthenticationException('Invalid username or password');
    }

    public function supportsToken(TokenInterface $token, $providerKey)
    {
        return $token instanceof UsernamePasswordToken && 
               $token->getProviderKey() === $providerKey;
    }

    public function createToken(Request $request, $username, $password, $providerKey)
    {
        return new UsernamePasswordToken($username, $password, $providerKey);
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {
        $view = View::create($token->getUser(), 200);

        return $this->container->get('fos_rest.view_handler')->handle($view);
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        $view = View::create(array('error' => $exception->getMessage()), 400);

        return $this->container->get('fos_rest.view_handler')->handle($view);
    }

    public function onLogoutSuccess(Request $request)
    {
        $view = View::create("You have successfully logged out.", 200);

        return $this->container->get('fos_rest.view_handler')->handle($view);
    }

    private function updateFailedLoginsNumber() 
    {
        $failedLoginsNumber = $this->currentRequest->getSession()->get(self::FAILED_LOGINS_NUMBER);

        if($failedLoginsNumber === NULL)
        {
            $failedLoginsNumber = 1;
        }
        else
        {
            $failedLoginsNumber++;
        }

        if($failedLoginsNumber > 5)
        {
            $this->currentRequest->getSession()->set(self::FAILED_LOGINS_WAIT_TIME, time());
            throw new FailedLoginsException(self::FAILED_LOGINS_TIME_MSG);
        }
        else
        {
            $this->currentRequest->getSession()->set(self::FAILED_LOGINS_NUMBER, $failedLoginsNumber);
        }
    }
}
