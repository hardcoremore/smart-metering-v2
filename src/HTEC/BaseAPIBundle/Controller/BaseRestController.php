<?php

namespace HTEC\BaseAPIBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Context\Context;
use Symfony\Component\HttpFoundation\Request;

use HTEC\BaseModelBundle\Entity\PageParameters;
use HTEC\BaseModelBundle\Model\BaseModel;

class BaseRestController extends FOSRestController
{
    private $model;
    private $recordGetRouteName;
    private $serializationContext;

    private $serializationGroupsForSingleRecordData;
    private $serializationGroupsForPagedData;
    private $serializationGroupsForSelectData;
    private $serializationGroupsForQuickSearchData;

    public function setModel(BaseModel $model)
    {
        $this->model = $model;
    }

    public function getModel()
    {
        return $this->model;
    }

    public function setRecordGetRouteName(string $routeName)
    {
        $this->recordGetRouteName = $routeName;
    }

    public function getRecordGetRouteName():string
    {
        return $this->recordGetRouteName;
    }

    public function setSerializationContext(Context $serializationContext)
    {
        $this->serializationContext = $serializationContext;
    }

    public function getSerializationContext():Context
    {
        return $this->serializationContext;
    }

    public function setSerializationGroupsForSingleRecordData(array $serializationGroups)
    {
        $this->serializationGroupsForSingleRecordData = $serializationGroups;
    }

    public function getSerializationGroupsForSingleRecordData():array
    {
        return $this->serializationGroupsForSingleRecordData;
    }

    public function setSerializationGroupsForPagedData(array $serializationGroups)
    {
        $this->serializationGroupsForPagedData = $serializationGroups;
    }

    public function getSerializationGroupsForPagedData():array
    {
        return $this->serializationGroupsForPagedData;
    }

    public function setSerializationGroupsForSelectData(array $serializationGroups)
    {
        $this->serializationGroupsForSelectData = $serializationGroups;
    }

    public function getSerializationGroupsForSelectData():array
    {
        return $this->serializationGroupsForSelectData;
    }

    public function setSerializationGroupsForQuickSearchData(array $serializationGroups)
    {
        $this->serializationGroupsForQuickSearchData = $serializationGroups;
    }

    public function getSerializationGroupsForQuickSearchData():array
    {
        return $this->serializationGroupsForQuickSearchData;
    }


    public function createAction(Request $request)
    {
        try
        {
            $createdEntity = $this->getModel()->create($this->prepareDataForCreate($request->request->all()));

            return $this->dispatchResourceCreatedResponse($createdEntity);
        }
        catch(InvalidFormDataException $error)
        {
            return $this->handleView($this->view($error->getErrorMessages(), 400));
        }
    }

    public function dispatchResourceCreatedResponse($createdEntity)
    {
        $url = $this->container->get('router')->generate(
            $this->getRecordGetRouteName(),
            array(
                'id' => $createdEntity->getId()
            ),
            true // absolute
        );

        $serializationContext = $this->getSerializationContext();
        $serializationContext->setGroups($this->getSerializationGroupsForSingleRecordData());

        $view = $this->view(
            $createdEntity,
            201,
            array(
                "Location" => $url
            )
        )->setContext($serializationContext);

        return $this->handleView($view);
    }

    public function getRecordAction($id)
    {
        $record = $this->getModel()->get($id);

        if($record)
        {
            $serializationContext = $this->getSerializationContext();
            $serializationContext->setGroups($this->getSerializationGroupsForSingleRecordData());

            $view = $this->view($record, 200)
                         ->setContext($serializationContext);

            return $this->handleView($view);
        }
        else
        {
            return $this->handleView($this->view(null, 404));
        }
    }

    public function prepareDataForCreate($data)
    {
        return $data;
    }

    public function listRecordsAction(Request $request)
    {
        $pageParameters = $this->getPageParameters($request);

        $pageData = $this->getModel()->getPagedData($pageParameters);

        $serializationContext = $this->getSerializationContext();
        $serializationContext->setGroups($this->getSerializationGroupsForPagedData());

        $view = $this->view($pageData, 200)
                     ->setContext($serializationContext);
    
        return $this->handleView($view);
    }

    public function listAllRecordsAction(Request $request)
    {
        $queryParameters = $request->query->all();

        $allData = $this->getModel()->readAll($queryParameters);

        $serializationContext = $this->getSerializationContext();
        $serializationContext->setGroups($this->getSerializationGroupsForSingleRecordData());

        $view = $this->view($allData, 200)
                     ->setContext($serializationContext);
    
        return $this->handleView($view);
    }

    public function listForSelectAction(Request $request)
    {
        $queryParameters = $request->query->all();

        $selectData = $this->getModel()->readForSelect($queryParameters);

        $serializationContext = $this->getSerializationContext();
        $serializationContext->setGroups($this->getSerializationGroupsForSelectData());

        $view = $this->view($selectData, 200)
                     ->setContext($serializationContext);
    
        return $this->handleView($view);
    }

    public function listForAutoCompleteAction(Request $request)
    {
        $queryParameters = $request->query->all();
        $queryString = '';

        if(array_key_exists('q', $queryParameters))
        {
            $queryString = $queryParameters['q'];
            unset($queryParameters['q']);
        }


        $selectData = $this->getModel()->readForAutoComplete($queryString, $queryParameters);

        $serializationContext = $this->getSerializationContext();
        $serializationContext->setGroups($this->getSerializationGroupsForSelectData());

        $view = $this->view($selectData, 200)
                     ->setContext($serializationContext);
    
        return $this->handleView($view);
    }

    public function updateAction(Request $request, $id)
    {
        try
        {
            $updatedRecord = $this->getModel()->update($id, $request->request->all());
            $serializationContext = $this->getSerializationContext();
            $serializationContext->setGroups($this->getSerializationGroupsForSingleRecordData());

            $view = $this->view($updatedRecord, 200)
                         ->setContext($serializationContext);

            return $view;
        }
        catch(InvalidFormDataException $error)
        {
            return $this->handleView($this->view($error->getErrorMessages(), 400));
        }
    }

    public function patchAction(Request $request, $id)
    {
        try
        {
            $this->getModel()->patch($id, $request->request->all());

            return $this->handleView($this->view(null, 204));
        }
        catch(InvalidFormDataException $error)
        {
            return $this->handleView($this->view($error->getErrorMessages(), 400));
        }
    }

    public function deleteAction(Request $request, $id)
    {
        try
        {
            $this->getModel()->delete($id);

            return $this->handleView($this->view(null, 204));
        }
        catch(InvalidFormDataException $error)
        {
            return $this->handleView($this->view($error->getErrorMessages(), 400));
        }
    }

    public function quickSearchAction(Request $request)
    {
        $pageParameters = $this->getPageParameters($request);

        $pageData = $this->getModel()->quickSearch($pageParameters);

        $serializationContext = $this->getSerializationContext();
        $serializationContext->setGroups($this->getSerializationGroupsForPagedData());

        $view = $this->view($pageData, 200)
                     ->setContext($serializationContext);
    
        return $this->handleView($view);
    }

    protected function getPageParameters(Request $request)
    {
        $pageParameters = new PageParameters();
        $pageParameters->pageNumber = $request->query->get('pageNumber', 1);
        $pageParameters->rowsPerPage = $request->query->get('rowsPerPage', 10);
        $pageParameters->sortColumnName = $request->query->get('sortColumnName', 'id');
        $pageParameters->sortColumnOrder = $request->query->get('sortColumnOrder', 'desc');

        return $pageParameters;
    }
}
