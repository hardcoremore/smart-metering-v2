<?php

namespace HTEC\BaseAPIBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContext;

use FOS\RestBundle\View\View;

use HTEC\BaseAPIBundle\ValueObject\BadInputError;
use HTEC\BaseAPIBundle\Error\ErrorCodes;

class UserController extends BaseRestController
{
    public function getCurrentUserAction()
    {
        $token = $this->container->get('security.token_storage')->getToken();

        return parent::getRecordAction($token->getUser()->getId());
    }

    public function logoutSuccessAction()
    {
        return $this->handleView(
            $this->view("You have logged out successfully.", 200)
        );
    }
}
