<?php

namespace HTEC\BaseAPIBundle\Listener;

use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;

use FOS\RestBundle\View\View;

use HTEC\BaseAPIBundle\Exception\Security\FailedLoginsException;
use HTEC\BaseModelBundle\Exception\Form\InvalidFormDataException;
use HTEC\BaseAPIBundle\Helpers\ExtractFormErrorsHelper;
use HTEC\BaseModelBundle\Exception\Entity\EntityNotFoundException;
use HTEC\BaseModelBundle\Exception\Security\InsufficientPermissionsException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class KernelExceptionListener
{

    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * Handles security related exceptions.
     *
     * @param GetResponseForExceptionEvent $event An GetResponseForExceptionEvent instance
     */
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $exception = $event->getException();
        $response = null;
        $environment = $this->container->get('kernel')->getEnvironment();

        if($exception instanceof AccessDeniedException)
        {
            $response = new Response(json_encode("You have to log in to access the API"), 403);
        }
        else if($exception instanceof InsufficientPermissionsException)
        {
            $response = new Response(json_encode([ 'code' => 1403, 'message' => $exception->getMessage()]), 403);
        }
        else if($exception instanceof FailedLoginsException)
        {
            $response = new Response(json_encode(array('message' => $exception->getMessage(), 'code' => 1403)), 403);
        }
        else if($exception instanceof InvalidFormDataException)
        {
            $formErrorHelper = new ExtractFormErrorsHelper();
            $formErrors = $formErrorHelper->getErrorMessages($exception);
            $response = new Response(json_encode($formErrors), 400);
        }
        else if($exception instanceof NotFoundHttpException)
        {
            $response = new Response(json_encode(array('message' => $exception->getMessage(), 'code' => 1404)), 404);
        }
        else if($exception instanceof EntityNotFoundException)
        {
            $response = new Response(json_encode($exception->getMessage()), 400);
        }
        else if($environment === 'dev' || $environment === 'stag' || $environment === 'test')
        {
            $response = new Response("Unknown error ocurred. Message: " . $exception->getMessage(), 500);
        }

        $response->headers->set('Content-Type', 'application/json');
        $event->setResponse($response);
    }
}