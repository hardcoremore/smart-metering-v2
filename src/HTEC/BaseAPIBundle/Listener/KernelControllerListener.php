<?php

namespace HTEC\BaseAPIBundle\Listener;

use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Bundle\TwigBundle\Controller\ExceptionController;

class KernelControllerListener
{
    protected $container;
    protected $requestStack;
    protected $currentRequest;

    public function __construct(ContainerInterface $container, RequestStack $requestStack)
    {
        $this->container = $container;
        $this->requestStack = $requestStack;
        $this->currentRequest = $requestStack->getCurrentRequest();
    }

    public function onKernelController(FilterControllerEvent $event)
    {
        $firewallUserControllers = $this->container->getParameter('firewall_user_controllers');
        $matchedController = $event->getController();

        if(is_array($matchedController) && $matchedController[0] instanceof ExceptionController)
        {
            return;
        }

        $parsedPattern = '';
        $isRouteMatched = false;
        $isHostMatched = false;

        foreach ($firewallUserControllers as $key => $val)
        {
            $parsedPattern = '/' . str_replace('/', '\/', $val['pattern']) . '/';
            $isRouteMatched = preg_match( $parsedPattern, $this->currentRequest->getPathInfo());

            if(isset($val['host']))
            {
                $isHostMatched = $val['host'] === $this->currentRequest->getHost();
            }
            else
            {
                $isHostMatched = true;
            }

            if($isRouteMatched && $isHostMatched)
            {
                $matchedController[0] = $this->container->get($val['controller']);
                $event->setController($matchedController);
                break;
            }
        }
    }
}