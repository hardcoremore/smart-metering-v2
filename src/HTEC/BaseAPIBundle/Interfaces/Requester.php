<?php

namespace HTEC\BaseAPIBundle\Interfaces;

interface Requester
{
    const GET_METHOD = 'GET';
    const POST_METHOD = 'POST';
    const PUT_METHOD = 'PUT';
    const DELETE_METHOD = 'DELETE';

    function post(string $url, array $params, array $headers = null);
    function get(string $url, array $params, array $headers = null);
    function put(string $url, array $params, array $headers = null);
    function delete(string $url, array $headers = null);
}