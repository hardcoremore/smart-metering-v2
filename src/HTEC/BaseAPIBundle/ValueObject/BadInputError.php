<?php

namespace HTEC\BaseAPIBundle\ValueObject;

class BadInputError
{
    public $message;
    public $code;
}