<?php

namespace HTEC\BaseAPIBundle\Services;

use HTEC\BaseAPIBundle\Interfaces\Requester;

class CurlRequest implements Requester
{
    private $options;

    public function __construct(array $options = null)
    {
        $this->options = $options;
    }

    public function post(string $url, array $params, array $headers = null)
    {
        return $this->request(self::POST_METHOD, $url, $params, $headers);
    }

    public function get(string $url, array $params, array $headers = null)
    {
        return $this->request(self::GET_METHOD, $url, $params, $headers);
    }

    public function put(string $url, array $params, array $headers = null)
    {
        return $this->request(self::PUT_METHOD, $url, $params, $headers);
    }

    public function delete(string $url, array $headers = null)
    {
        return $this->request(self::DELETE_METHOD, $url, $params, $headers);
    }

    protected function request(string $method, string $url, array $params, array $headers = null)
    {
        $ch = null;

        if(is_string($url) && strlen($url) > 0)
        {
            $ch = curl_init();

            if($params AND count($params) > 0)
            {
                if($method === self::GET_METHOD)
                {
                    $url = $url  . '?' . http_build_query($params);
                }
                else
                {
                    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
                }
            }

            curl_setopt($ch, CURLOPT_URL, $url);
        }
        else
        {
            throw new \Exception("Url is invalid.");
        }


        if(isset($this->options['httpauth_username']) && isset($this->options['httpauth_password']))
        {
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($ch, CURLOPT_USERPWD, $this->options['httpauth_username'] . ':' . $this->options['httpauth_password']);
        }

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_ENCODING, "");

        if(isset($this->options['follow_location']))
        {
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        }

        if(isset($this->options['max_redirections']) && is_numeric($this->options['max_redirections']))
        {
            curl_setopt($ch, CURLOPT_MAXREDIRS, $this->options['max_redirections']);
        }
        else
        {
            curl_setopt($ch, CURLOPT_MAXREDIRS, 3);
        }

        if(is_array($headers) && count($headers) > 0)
        {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        }

        if(isset($this->options['user_agent']))
        {
            curl_setopt($ch, CURLOPT_USERAGENT, $this->options['user_agent']);
        }

        switch($method)
        {
            case self::POST_METHOD:
                curl_setopt($ch, CURLOPT_POST, true);
            break;

            case self::PUT_METHOD:
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
            break;

            case self::DELETE_METHOD:
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
            break;
        }

        $responseString = curl_exec($ch);

        $headerLength = curl_getinfo($ch, CURLINFO_HEADER_SIZE);

        $headerContent = substr($responseString, 0, $headerLength);
        $bodyContent = substr($responseString, $headerLength);

        curl_close($ch);

        $response = [];
        $response["header"] = $headerContent;
        $response["body"] = $bodyContent;
        $response["headerItems"] = $this->parseHeaders($headerContent);

        return $response;
    }

    protected function parseHeaders($headersString)
    {
        $headers = [];

        $headerItem;

        foreach (explode("\r\n", $headersString) AS $i => $line)
        {
            if ($i === 0)
            {
                $headers['http_code'] = $line;
            }
            else
            {
                $headerItem = explode(': ', $line);

                if(count($headerItem) >= 2)
                {
                    $headers[$headerItem[0]] = $headerItem[1];
                }
            }
        }

        return $headers;
    }
}
