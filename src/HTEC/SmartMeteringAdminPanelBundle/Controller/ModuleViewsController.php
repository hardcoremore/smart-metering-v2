<?php

namespace HTEC\SmartMeteringAdminPanelBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ModuleViewsController extends Controller
{
    public function testViewAction()
    {
        return $this->render('SmartMeteringAdminPanelBundle:ModuleViews:test.html.twig');
    }

    public function homeViewAction()
    {
        return $this->render('SmartMeteringAdminPanelBundle:ModuleViews:home.html.twig');
    }

    public function loginViewAction()
    {
        return $this->render('SmartMeteringAdminPanelBundle:ModuleViews:login.html.twig');
    }

    public function dashboardViewAction()
    {
        return $this->render('SmartMeteringAdminPanelBundle:ModuleViews:dashboard.html.twig');
    }

    public function cityViewAction()
    {
        return $this->render('SmartMeteringAdminPanelBundle:ModuleViews:city.html.twig');
    }

    public function businessDistrictViewAction()
    {
        return $this->render('SmartMeteringAdminPanelBundle:ModuleViews:businessDistrict.html.twig');
    }

    public function powerPurchaseViewAction()
    {
        return $this->render('SmartMeteringAdminPanelBundle:ModuleViews:powerPurchase.html.twig');
    }

    public function powerSourceViewAction()
    {
        return $this->render('SmartMeteringAdminPanelBundle:ModuleViews:powerSource.html.twig');
    }

    public function customerViewAction()
    {
        return $this->render('SmartMeteringAdminPanelBundle:ModuleViews:customer.html.twig');
    }

    public function offlineMeterReadingViewAction()
    {
        return $this->render('SmartMeteringAdminPanelBundle:ModuleViews:offlineMeterReading.html.twig');
    }

    public function onlineMeterReadingViewAction()
    {
        return $this->render('SmartMeteringAdminPanelBundle:ModuleViews:onlineMeterReading.html.twig');
    }
    
    public function offlinePrepaidVendReadingViewAction()
    {
        return $this->render('SmartMeteringAdminPanelBundle:ModuleViews:offlinePrepaidVendReading.html.twig');
    }
    
    public function gridMeterReadingViewAction()
    {
        return $this->render('SmartMeteringAdminPanelBundle:ModuleViews:gridMeterReading.html.twig');
    }
    
    public function promotionViewAction()
    {
        return $this->render('SmartMeteringAdminPanelBundle:ModuleViews:promotion.html.twig');
    }

    public function customerNetworkDataViewAction()
    {
        return $this->render('SmartMeteringAdminPanelBundle:ModuleViews:customerNetworkData.html.twig');
    }

    public function customerCategoryViewAction()
    {
        return $this->render('SmartMeteringAdminPanelBundle:ModuleViews:customerCategory.html.twig');
    }

    public function applianceTypeViewAction()
    {
        return $this->render('SmartMeteringAdminPanelBundle:ModuleViews:applianceType.html.twig');
    }

    public function powerActivateDeactivateViewAction()
    {
        return $this->render('SmartMeteringAdminPanelBundle:ModuleViews:powerActivateDeactivate.html.twig');
    }

    public function estimatedLoadViewAction()
    {
        return $this->render('SmartMeteringAdminPanelBundle:ModuleViews:estimatedLoad.html.twig');
    }
    
    public function estimatedConsumptionViewAction()
    {
        return $this->render('SmartMeteringAdminPanelBundle:ModuleViews:estimatedConsumption.html.twig');
    }

    public function estimatedLoadApplianceViewAction()
    {
        return $this->render('SmartMeteringAdminPanelBundle:ModuleViews:estimatedLoadAppliance.html.twig');
    }

    public function powerForecastViewAction()
    {
        return $this->render('SmartMeteringAdminPanelBundle:ModuleViews:powerForecast.html.twig');
    }

    public function powerAllocateViewAction()
    {
        return $this->render('SmartMeteringAdminPanelBundle:ModuleViews:powerAllocate.html.twig');
    }

    public function tariffViewAction()
    {
        return $this->render('SmartMeteringAdminPanelBundle:ModuleViews:tariff.html.twig');
    }

    public function powerScheduleViewAction()
    {
        return $this->render('SmartMeteringAdminPanelBundle:ModuleViews:powerSchedule.html.twig');
    }

    public function transformerViewAction()
    {
        return $this->render('SmartMeteringAdminPanelBundle:ModuleViews:transformer.html.twig');
    }

    public function transformerInjectionSubstationViewAction()
    {
        return $this->render('SmartMeteringAdminPanelBundle:ModuleViews:transformerInjectionSubstation.html.twig');
    }

    public function concentratorViewAction()
    {
        return $this->render('SmartMeteringAdminPanelBundle:ModuleViews:concentrator.html.twig');
    }

    public function feederViewAction()
    {
        return $this->render('SmartMeteringAdminPanelBundle:ModuleViews:feeder.html.twig');
    }

    public function smartMeterViewAction()
    {
        return $this->render('SmartMeteringAdminPanelBundle:ModuleViews:smartMeter.html.twig');
    }

    public function smartMeterCommunicationViewAction()
    {
        return $this->render('SmartMeteringAdminPanelBundle:ModuleViews:smartMeterCommunication.html.twig');
    }

    public function repeaterViewAction()
    {
        return $this->render('SmartMeteringAdminPanelBundle:ModuleViews:repeater.html.twig');
    }

    public function adminPanelUserViewAction()
    {
        return $this->render('SmartMeteringAdminPanelBundle:ModuleViews:adminPanelUser.html.twig');
    }

    public function roleViewAction()
    {
        return $this->render('SmartMeteringAdminPanelBundle:ModuleViews:role.html.twig');
    }
    
    public function applianceViewAction()
    {
        return $this->render('SmartMeteringAdminPanelBundle:ModuleViews:appliance.html.twig');
    }

    public function injectionSubstationViewAction()
    {
        return $this->render('SmartMeteringAdminPanelBundle:ModuleViews:injectionSubstation.html.twig');
    }

    public function powerTransformerViewAction()
    {
        return $this->render('SmartMeteringAdminPanelBundle:ModuleViews:powerTransformer.html.twig');
    }

    public function electricPoleViewAction()
    {
        return $this->render('SmartMeteringAdminPanelBundle:ModuleViews:electricPole.html.twig');
    }

    public function distributionTransformerViewAction()
    {
        return $this->render('SmartMeteringAdminPanelBundle:ModuleViews:distributionTransformer.html.twig');
    }

    public function meterViewAction()
    {
        return $this->render('SmartMeteringAdminPanelBundle:ModuleViews:meter.html.twig');
    }

    public function powerAviabilityViewAction()
    {
        return $this->render('SmartMeteringAdminPanelBundle:ModuleViews:powerAviability.html.twig');
    }

    public function feederLoadViewAction()
    {
        return $this->render('SmartMeteringAdminPanelBundle:ModuleViews:feederLoad.html.twig');
    }
}
