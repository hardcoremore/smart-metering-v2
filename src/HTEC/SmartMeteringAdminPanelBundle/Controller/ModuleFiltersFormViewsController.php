<?php

namespace HTEC\SmartMeteringAdminPanelBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ModuleFiltersFormViewsController extends Controller
{
    public function powerScheduleFiltersFormViewAction()
    {
        return $this->render('SmartMeteringAdminPanelBundle:ModuleFiltersFormViews:powerScheduleFiltersForm.html.twig');
    }
}
