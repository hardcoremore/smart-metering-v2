<?php

namespace HTEC\SmartMeteringAdminPanelBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ModuleFormViewsController extends Controller
{
    public function roleFormViewAction()
    {
        return $this->render('SmartMeteringAdminPanelBundle:ModuleFormViews:roleForm.html.twig');
    }

    public function adminPanelUserFormViewAction()
    {
        return $this->render('SmartMeteringAdminPanelBundle:ModuleFormViews:adminPanelUserForm.html.twig');
    }

    public function cityFormViewAction()
    {
        return $this->render('SmartMeteringAdminPanelBundle:ModuleFormViews:cityForm.html.twig');
    }

    public function businessDistrictFormViewAction()
    {
        return $this->render('SmartMeteringAdminPanelBundle:ModuleFormViews:businessDistrictForm.html.twig');
    }

    public function powerPurchaseFormViewAction()
    {
        return $this->render('SmartMeteringAdminPanelBundle:ModuleFormViews:powerPurchaseForm.html.twig');
    }

    public function powerSourceFormViewAction()
    {
        return $this->render('SmartMeteringAdminPanelBundle:ModuleFormViews:powerSourceForm.html.twig');
    }

    public function customerCategoryFormViewAction()
    {
        return $this->render('SmartMeteringAdminPanelBundle:ModuleFormViews:customerCategoryForm.html.twig');
    }

    public function applianceTypeFormViewAction()
    {
        return $this->render('SmartMeteringAdminPanelBundle:ModuleFormViews:applianceTypeForm.html.twig');
    }

    public function applianceFormViewAction()
    {
        return $this->render('SmartMeteringAdminPanelBundle:ModuleFormViews:applianceForm.html.twig');
    }

    public function estimatedLoadFormViewAction()
    {
        return $this->render('SmartMeteringAdminPanelBundle:ModuleFormViews:estimatedLoadForm.html.twig');
    }

    public function estimatedLoadApplianceFormViewAction()
    {
        return $this->render('SmartMeteringAdminPanelBundle:ModuleFormViews:estimatedLoadApplianceForm.html.twig');
    }

    public function estimatedConsumptionFormViewAction()
    {
        return $this->render('SmartMeteringAdminPanelBundle:ModuleFormViews:estimatedConsumptionForm.html.twig');
    }

    public function powerForecastFormViewAction()
    {
        return $this->render('SmartMeteringAdminPanelBundle:ModuleFormViews:powerForecastForm.html.twig');
    }

    public function powerAllocateFormViewAction()
    {
        return $this->render('SmartMeteringAdminPanelBundle:ModuleFormViews:powerAllocateForm.html.twig');
    }

    public function tariffFormViewAction()
    {
        return $this->render('SmartMeteringAdminPanelBundle:ModuleFormViews:tariffForm.html.twig');
    }

    public function powerScheduleFormViewAction()
    {
        return $this->render('SmartMeteringAdminPanelBundle:ModuleFormViews:powerScheduleForm.html.twig');
    }

    public function powerActivateDeactivateFormViewAction()
    {
        return $this->render('SmartMeteringAdminPanelBundle:ModuleFormViews:powerActivateDeactivateForm.html.twig');
    }

    public function feederFormViewAction()
    {
        return $this->render('SmartMeteringAdminPanelBundle:ModuleFormViews:feederForm.html.twig');
    }

    public function injectionSubstationFormViewAction()
    {
        return $this->render('SmartMeteringAdminPanelBundle:ModuleFormViews:injectionSubstationForm.html.twig');
    }

    public function powerTransformerFormViewAction()
    {
        return $this->render('SmartMeteringAdminPanelBundle:ModuleFormViews:powerTransformerForm.html.twig');
    }

    public function electricPoleFormViewAction()
    {
        return $this->render('SmartMeteringAdminPanelBundle:ModuleFormViews:electricPoleForm.html.twig');
    }

    public function distributionTransformerFormViewAction()
    {
        return $this->render('SmartMeteringAdminPanelBundle:ModuleFormViews:distributionTransformerForm.html.twig');
    }

    public function meterFormViewAction()
    {
        return $this->render('SmartMeteringAdminPanelBundle:ModuleFormViews:meterForm.html.twig');
    }

    public function customerNetworkDataFormViewAction()
    {
        return $this->render('SmartMeteringAdminPanelBundle:ModuleFormViews:customerNetworkDataForm.html.twig');
    }

    public function feederLoadFormViewAction()
    {
        return $this->render('SmartMeteringAdminPanelBundle:ModuleFormViews:feederLoadForm.html.twig');
    }
}
