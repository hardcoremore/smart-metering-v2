<?php

namespace HTEC\SmartMeteringAdminPanelBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('SmartMeteringAdminPanelBundle:Default:index.html.twig');
    }

    public function notFoundAction()
    {
        return $this->redirectToRoute('elavon_creator_login_check');
    }
}
