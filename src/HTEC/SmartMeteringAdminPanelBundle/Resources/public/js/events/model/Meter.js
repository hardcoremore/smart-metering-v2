HCM.createNamespace('Events.Model.Meter', {

    VALIDATION_ERROR: 'Events.Model.Meter.validationError',

    CREATE_COMPLETE: 'Events.Model.Meter.createComplete',
    CREATE_ERROR: 'Events.Model.Meter.createError',

    READ_COMPLETE: 'Events.Model.Meter.readComplete',
    READ_ERROR: 'Events.Model.Meter.readError',
    
    READ_FOR_SELECT_COMPLETE: 'Events.Model.Meter.readForSelectComplete',
    READ_FOR_SELECT_ERROR: 'Events.Model.Meter.readForSelectError',

    READ_FOR_AUTOCOMPLETE_COMPLETE: 'Events.Model.Meter.readForAutocompleteComplete',
    READ_FOR_AUTOCOMPLETE_ERROR: 'Events.Model.Meter.readForAutocompleteError',

    UPDATE_COMPLETE: 'Events.Model.Meter.updateComplete',
    UPDATE_ERROR: 'Events.Model.Meter.updateError',

    PATCH_COMPLETE: 'Events.Model.Meter.patchComplete',
    PATCH_ERROR: 'Events.Model.Meter.patchError',

    DELETE_COMPLETE: 'Events.Model.Meter.deleteComplete',
    DELETE_ERROR: 'Events.Model.Meter.deleteError'
});