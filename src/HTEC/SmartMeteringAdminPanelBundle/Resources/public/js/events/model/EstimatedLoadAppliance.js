HCM.createNamespace('Events.Model.EstimatedLoadAppliance', {

    VALIDATION_ERROR: 'Events.Model.EstimatedLoadAppliance.validationError',

    ESTIMATED_LOAD_CHANGED: 'Events.Model.EstimatedLoadAppliance.estimatedLoadChanged',

    CREATE_COMPLETE: 'Events.Model.EstimatedLoadAppliance.createComplete',
    CREATE_ERROR: 'Events.Model.EstimatedLoadAppliance.createError',

    READ_COMPLETE: 'Events.Model.EstimatedLoadAppliance.readComplete',
    READ_ERROR: 'Events.Model.EstimatedLoadAppliance.readError',

    READ_ALL_COMPLETE: 'Events.Model.EstimatedLoadAppliance.readAllComplete',
    READ_ALL_ERROR: 'Events.Model.EstimatedLoadAppliance.readAllError',
    
    READ_FOR_SELECT_COMPLETE: 'Events.Model.EstimatedLoadAppliance.readForSelectComplete',
    READ_FOR_SELECT_ERROR: 'Events.Model.EstimatedLoadAppliance.readForSelectError',

    READ_FOR_AUTOCOMPLETE_COMPLETE: 'Events.Model.EstimatedLoadAppliance.readForAutocompleteComplete',
    READ_FOR_AUTOCOMPLETE_ERROR: 'Events.Model.EstimatedLoadAppliance.readForAutocompleteError',

    UPDATE_COMPLETE: 'Events.Model.EstimatedLoadAppliance.updateComplete',
    UPDATE_ERROR: 'Events.Model.EstimatedLoadAppliance.updateError',

    PATCH_COMPLETE: 'Events.Model.EstimatedLoadAppliance.patchComplete',
    PATCH_ERROR: 'Events.Model.EstimatedLoadAppliance.patchError',

    DELETE_COMPLETE: 'Events.Model.EstimatedLoadAppliance.deleteComplete',
    DELETE_ERROR: 'Events.Model.EstimatedLoadAppliance.deleteError'
});