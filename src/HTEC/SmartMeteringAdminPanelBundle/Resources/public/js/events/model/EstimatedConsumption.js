HCM.createNamespace('Events.Model.EstimatedConsumption', {

    VALIDATION_ERROR: 'Events.Model.EstimatedConsumption.validationError',

    CREATE_COMPLETE: 'Events.Model.EstimatedConsumption.createComplete',
    CREATE_ERROR: 'Events.Model.EstimatedConsumption.createError',

    READ_COMPLETE: 'Events.Model.EstimatedConsumption.readComplete',
    READ_ERROR: 'Events.Model.EstimatedConsumption.readError',
    
    READ_FOR_SELECT_COMPLETE: 'Events.Model.EstimatedConsumption.readForSelectComplete',
    READ_FOR_SELECT_ERROR: 'Events.Model.EstimatedConsumption.readForSelectError',

    READ_FOR_AUTOCOMPLETE_COMPLETE: 'Events.Model.EstimatedConsumption.readForAutocompleteComplete',
    READ_FOR_AUTOCOMPLETE_ERROR: 'Events.Model.EstimatedConsumption.readForAutocompleteError',

    UPDATE_COMPLETE: 'Events.Model.EstimatedConsumption.updateComplete',
    UPDATE_ERROR: 'Events.Model.EstimatedConsumption.updateError',

    PATCH_COMPLETE: 'Events.Model.EstimatedConsumption.patchComplete',
    PATCH_ERROR: 'Events.Model.EstimatedConsumption.patchError',

    DELETE_COMPLETE: 'Events.Model.EstimatedConsumption.deleteComplete',
    DELETE_ERROR: 'Events.Model.EstimatedConsumption.deleteError'
});