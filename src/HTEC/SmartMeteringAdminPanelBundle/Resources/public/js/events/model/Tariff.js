HCM.createNamespace('Events.Model.Tariff', {

    VALIDATION_ERROR: 'Events.Model.Tariff.validationError',

    CREATE_COMPLETE: 'Events.Model.Tariff.createComplete',
    CREATE_ERROR: 'Events.Model.Tariff.createError',

    READ_COMPLETE: 'Events.Model.Tariff.readComplete',
    READ_ERROR: 'Events.Model.Tariff.readError',

    READ_ALL_COMPLETE: 'Events.Model.Tariff.readAllComplete',
    READ_ALL_ERROR: 'Events.Model.Tariff.readAllError',

    READ_FOR_SELECT_COMPLETE: 'Events.Model.Tariff.readForSelectComplete',
    READ_FOR_SELECT_ERROR: 'Events.Model.Tariff.readForSelectError',

    READ_FOR_SELECT_FOR_TRANSFORMER_COMPLETE: 'Events.Model.Transformer.readForSelectForTransformerComplete',
    READ_FOR_SELECT_FOR_TRANSFORMER_ERROR: 'Events.Model.Transformer.readForSelectForTransformerError',

    READ_FOR_AUTOCOMPLETE_COMPLETE: 'Events.Model.Tariff.readForAutocompleteComplete',
    READ_FOR_AUTOCOMPLETE_ERROR: 'Events.Model.Tariff.readForAutocompleteError',

    UPDATE_COMPLETE: 'Events.Model.Tariff.updateComplete',
    UPDATE_ERROR: 'Events.Model.Tariff.updateError',

    PATCH_COMPLETE: 'Events.Model.Tariff.patchComplete',
    PATCH_ERROR: 'Events.Model.Tariff.patchError',

    DELETE_COMPLETE: 'Events.Model.Tariff.deleteComplete',
    DELETE_ERROR: 'Events.Model.Tariff.deleteError'
});