HCM.createNamespace('Events.Model.PowerSource', {

    VALIDATION_ERROR: 'Events.Model.PowerSource.validationError',

    CREATE_COMPLETE: 'Events.Model.PowerSource.createComplete',
    CREATE_ERROR: 'Events.Model.PowerSource.createError',

    READ_COMPLETE: 'Events.Model.PowerSource.readComplete',
    READ_ERROR: 'Events.Model.PowerSource.readError',
    
    READ_FOR_SELECT_COMPLETE: 'Events.Model.PowerSource.readForSelectComplete',
    READ_FOR_SELECT_ERROR: 'Events.Model.PowerSource.readForSelectError',

    READ_FOR_AUTOCOMPLETE_COMPLETE: 'Events.Model.PowerSource.readForAutocompleteComplete',
    READ_FOR_AUTOCOMPLETE_ERROR: 'Events.Model.PowerSource.readForAutocompleteError',

    UPDATE_COMPLETE: 'Events.Model.PowerSource.updateComplete',
    UPDATE_ERROR: 'Events.Model.PowerSource.updateError',

    PATCH_COMPLETE: 'Events.Model.PowerSource.patchComplete',
    PATCH_ERROR: 'Events.Model.PowerSource.patchError',

    DELETE_COMPLETE: 'Events.Model.PowerSource.deleteComplete',
    DELETE_ERROR: 'Events.Model.PowerSource.deleteError'
});