HCM.createNamespace('Events.Model.OfflineMeterReading', {

    READ_COMPLETE: 'Events.Model.OfflineMeterReading.readComplete',
    READ_ERROR: 'Events.Model.OfflineMeterReading.readError',
    
    READ_FOR_SELECT_COMPLETE: 'Events.Model.OfflineMeterReading.readForSelectComplete',
    READ_FOR_SELECT_ERROR: 'Events.Model.OfflineMeterReading.readForSelectError',

    READ_FOR_AUTOCOMPLETE_COMPLETE: 'Events.Model.OfflineMeterReading.readForAutocompleteComplete',
    READ_FOR_AUTOCOMPLETE_ERROR: 'Events.Model.OfflineMeterReading.readForAutocompleteError',

    PATCH_COMPLETE: 'Events.Model.OfflineMeterReading.patchComplete',
    PATCH_ERROR: 'Events.Model.OfflineMeterReading.patchError'
});