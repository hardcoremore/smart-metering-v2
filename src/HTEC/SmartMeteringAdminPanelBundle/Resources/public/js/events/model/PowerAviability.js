HCM.createNamespace('Events.Model.PowerAviability', {

    READ_COMPLETE: 'Events.Model.PowerAviability.readComplete',
    READ_ERROR: 'Events.Model.PowerAviability.readError',

    READ_ALL_COMPLETE: 'Events.Model.PowerAviability.readAllComplete',
    READ_ALL_ERROR: 'Events.Model.PowerAviability.readAllError',

});