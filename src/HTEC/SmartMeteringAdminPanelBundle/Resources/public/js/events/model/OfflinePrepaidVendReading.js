HCM.createNamespace('Events.Model.OfflinePrepaidVendReading', {

    READ_COMPLETE: 'Events.Model.OfflinePrepaidVendReading.readComplete',
    READ_ERROR: 'Events.Model.OfflinePrepaidVendReading.readError',
    
    READ_FOR_SELECT_COMPLETE: 'Events.Model.OfflinePrepaidVendReading.readForSelectComplete',
    READ_FOR_SELECT_ERROR: 'Events.Model.OfflinePrepaidVendReading.readForSelectError',

    READ_FOR_AUTOCOMPLETE_COMPLETE: 'Events.Model.OfflinePrepaidVendReading.readForAutocompleteComplete',
    READ_FOR_AUTOCOMPLETE_ERROR: 'Events.Model.OfflinePrepaidVendReading.readForAutocompleteError'
   
});