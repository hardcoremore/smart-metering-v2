HCM.createNamespace('Events.Model.CustomerNetworkData', {

    VALIDATION_ERROR: 'Events.Model.CustomerNetworkData.validationError',

    CREATE_COMPLETE: 'Events.Model.CustomerNetworkData.createComplete',
    CREATE_ERROR: 'Events.Model.CustomerNetworkData.createError',

    READ_COMPLETE: 'Events.Model.CustomerNetworkData.readComplete',
    READ_ERROR: 'Events.Model.CustomerNetworkData.readError',
    
    READ_FOR_SELECT_COMPLETE: 'Events.Model.CustomerNetworkData.readForSelectComplete',
    READ_FOR_SELECT_ERROR: 'Events.Model.CustomerNetworkData.readForSelectError',

    READ_FOR_AUTOCOMPLETE_COMPLETE: 'Events.Model.CustomerNetworkData.readForAutocompleteComplete',
    READ_FOR_AUTOCOMPLETE_ERROR: 'Events.Model.CustomerNetworkData.readForAutocompleteError',

    UPDATE_COMPLETE: 'Events.Model.CustomerNetworkData.updateComplete',
    UPDATE_ERROR: 'Events.Model.CustomerNetworkData.updateError',

    PATCH_COMPLETE: 'Events.Model.CustomerNetworkData.patchComplete',
    PATCH_ERROR: 'Events.Model.CustomerNetworkData.patchError',

    DELETE_COMPLETE: 'Events.Model.CustomerNetworkData.deleteComplete',
    DELETE_ERROR: 'Events.Model.CustomerNetworkData.deleteError'
});