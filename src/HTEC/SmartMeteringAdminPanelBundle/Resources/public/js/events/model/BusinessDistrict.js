HCM.createNamespace('Events.Model.BusinessDistrict', {

    VALIDATION_ERROR: 'Events.Model.BusinessDistrict.validationError',

    CREATE_COMPLETE: 'Events.Model.BusinessDistrict.createComplete',
    CREATE_ERROR: 'Events.Model.BusinessDistrict.createError',

    READ_COMPLETE: 'Events.Model.BusinessDistrict.readComplete',
    READ_ERROR: 'Events.Model.BusinessDistrict.readError',

    FIND_BY_ID_COMPLETE: 'Events.Model.BusinessDistrict.findByIdComplete',
    FIND_BY_ID_ERROR: 'Events.Model.BusinessDistrict.findByIdError',
    
    READ_FOR_SELECT_COMPLETE: 'Events.Model.BusinessDistrict.readForSelectComplete',
    READ_FOR_SELECT_ERROR: 'Events.Model.BusinessDistrict.readForSelectError',

    READ_FOR_AUTOCOMPLETE_COMPLETE: 'Events.Model.BusinessDistrict.readForAutocompleteComplete',
    READ_FOR_AUTOCOMPLETE_ERROR: 'Events.Model.BusinessDistrict.readForAutocompleteError',

    READ_FOR_SELECT_FOR_CITY_COMPLETE: 'Events.Model.BusinessDistrict.readForSelectForCityComplete',
    READ_FOR_SELECT_FOR_CITY_ERROR: 'Events.Model.BusinessDistrict.readForSelectForCityError',

    UPDATE_COMPLETE: 'Events.Model.BusinessDistrict.updateComplete',
    UPDATE_ERROR: 'Events.Model.BusinessDistrict.updateError',

    PATCH_COMPLETE: 'Events.Model.BusinessDistrict.patchComplete',
    PATCH_ERROR: 'Events.Model.BusinessDistrict.patchError',

    DELETE_COMPLETE: 'Events.Model.BusinessDistrict.deleteComplete',
    DELETE_ERROR: 'Events.Model.BusinessDistrict.deleteError'
});