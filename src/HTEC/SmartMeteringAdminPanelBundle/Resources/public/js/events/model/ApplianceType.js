HCM.createNamespace('Events.Model.ApplianceType', {

    VALIDATION_ERROR: 'Events.Model.ApplianceType.validationError',

    CREATE_COMPLETE: 'Events.Model.ApplianceType.createComplete',
    CREATE_ERROR: 'Events.Model.ApplianceType.createError',

    READ_COMPLETE: 'Events.Model.ApplianceType.readComplete',
    READ_ERROR: 'Events.Model.ApplianceType.readError',

    READ_FOR_SELECT_COMPLETE: 'Events.Model.ApplianceType.readForSelectComplete',
    READ_FOR_SELECT_ERROR: 'Events.Model.ApplianceType.readForSelectError',

    READ_FOR_AUTOCOMPLETE_COMPLETE: 'Events.Model.ApplianceType.readForAutocompleteComplete',
    READ_FOR_AUTOCOMPLETE_ERROR: 'Events.Model.ApplianceType.readForAutocompleteError',

    UPDATE_COMPLETE: 'Events.Model.ApplianceType.updateComplete',
    UPDATE_ERROR: 'Events.Model.ApplianceType.updateError',

    PATCH_COMPLETE: 'Events.Model.ApplianceType.patchComplete',
    PATCH_ERROR: 'Events.Model.ApplianceType.patchError',

    DELETE_COMPLETE: 'Events.Model.ApplianceType.deleteComplete',
    DELETE_ERROR: 'Events.Model.ApplianceType.deleteError'
});