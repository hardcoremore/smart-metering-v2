HCM.createNamespace('Events.Model.GridMeterReading', {

    READ_COMPLETE: 'Events.Model.GridMeterReading.readComplete',
    READ_ERROR: 'Events.Model.GridMeterReading.readError',
    
    READ_FOR_SELECT_COMPLETE: 'Events.Model.GridMeterReading.readForSelectComplete',
    READ_FOR_SELECT_ERROR: 'Events.Model.GridMeterReading.readForSelectError',

    READ_FOR_AUTOCOMPLETE_COMPLETE: 'Events.Model.GridMeterReading.readForAutocompleteComplete',
    READ_FOR_AUTOCOMPLETE_ERROR: 'Events.Model.GridMeterReading.readForAutocompleteError'
   
});