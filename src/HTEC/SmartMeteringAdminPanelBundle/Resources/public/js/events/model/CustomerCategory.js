HCM.createNamespace('Events.Model.CustomerCategory', {

    VALIDATION_ERROR: 'Events.Model.CustomerCategory.validationError',

    CREATE_COMPLETE: 'Events.Model.CustomerCategory.createComplete',
    CREATE_ERROR: 'Events.Model.CustomerCategory.createError',

    READ_COMPLETE: 'Events.Model.CustomerCategory.readComplete',
    READ_ERROR: 'Events.Model.CustomerCategory.readError',

    READ_FOR_SELECT_COMPLETE: 'Events.Model.CustomerCategory.readForSelectComplete',
    READ_FOR_SELECT_ERROR: 'Events.Model.CustomerCategory.readForSelectError',

    READ_FOR_AUTOCOMPLETE_COMPLETE: 'Events.Model.CustomerCategory.readForAutocompleteComplete',
    READ_FOR_AUTOCOMPLETE_ERROR: 'Events.Model.CustomerCategory.readForAutocompleteError',

    UPDATE_COMPLETE: 'Events.Model.CustomerCategory.updateComplete',
    UPDATE_ERROR: 'Events.Model.CustomerCategory.updateError',

    PATCH_COMPLETE: 'Events.Model.CustomerCategory.patchComplete',
    PATCH_ERROR: 'Events.Model.CustomerCategory.patchError',

    DELETE_COMPLETE: 'Events.Model.CustomerCategory.deleteComplete',
    DELETE_ERROR: 'Events.Model.CustomerCategory.deleteError'
});