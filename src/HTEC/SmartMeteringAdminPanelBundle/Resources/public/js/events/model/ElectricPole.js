HCM.createNamespace('Events.Model.ElectricPole', {

    VALIDATION_ERROR: 'Events.Model.ElectricPole.validationError',

    CREATE_COMPLETE: 'Events.Model.ElectricPole.createComplete',
    CREATE_ERROR: 'Events.Model.ElectricPole.createError',

    READ_COMPLETE: 'Events.Model.ElectricPole.readComplete',
    READ_ERROR: 'Events.Model.ElectricPole.readError',
    
    READ_FOR_SELECT_COMPLETE: 'Events.Model.ElectricPole.readForSelectComplete',
    READ_FOR_SELECT_ERROR: 'Events.Model.ElectricPole.readForSelectError',

    READ_FOR_AUTOCOMPLETE_COMPLETE: 'Events.Model.ElectricPole.readForAutocompleteComplete',
    READ_FOR_AUTOCOMPLETE_ERROR: 'Events.Model.ElectricPole.readForAutocompleteError',

    UPDATE_COMPLETE: 'Events.Model.ElectricPole.updateComplete',
    UPDATE_ERROR: 'Events.Model.ElectricPole.updateError',

    PATCH_COMPLETE: 'Events.Model.ElectricPole.patchComplete',
    PATCH_ERROR: 'Events.Model.ElectricPole.patchError',

    DELETE_COMPLETE: 'Events.Model.ElectricPole.deleteComplete',
    DELETE_ERROR: 'Events.Model.ElectricPole.deleteError'
});