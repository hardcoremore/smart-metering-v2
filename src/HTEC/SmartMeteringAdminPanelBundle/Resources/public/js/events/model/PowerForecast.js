HCM.createNamespace('Events.Model.PowerForecast', {

    VALIDATION_ERROR: 'Events.Model.PowerForecast.validationError',

    CREATE_COMPLETE: 'Events.Model.PowerForecast.createComplete',
    CREATE_ERROR: 'Events.Model.PowerForecast.createError',

    READ_COMPLETE: 'Events.Model.PowerForecast.readComplete',
    READ_ERROR: 'Events.Model.PowerForecast.readError',

    READ_FOR_SELECT_COMPLETE: 'Events.Model.PowerForecast.readForSelectComplete',
    READ_FOR_SELECT_ERROR: 'Events.Model.PowerForecast.readForSelectError',

    READ_ALL_FOR_TODAY_WITH_DETAILS_COMPLETE: 'Events.Model.PowerForecast.readAllForTodayWithDetailsComplete',
    READ_ALL_FOR_TODAY_WITH_DETAILS_ERROR: 'Events.Model.PowerForecast.readAllForTodayWithDetailsError',

    READ_FOR_AUTOCOMPLETE_COMPLETE: 'Events.Model.PowerForecast.readForAutocompleteComplete',
    READ_FOR_AUTOCOMPLETE_ERROR: 'Events.Model.PowerForecast.readForAutocompleteError',

    READ_FULL_DETAILS_COMPLETE: 'Events.Model.PowerForecast.readFullDetailsComplete',
    READ_FULL_DETAILS_ERROR: 'Events.Model.PowerForecast.readFullDetailsError'
});