HCM.createNamespace('Events.Model.Promotion', {

    READ_COMPLETE: 'Events.Model.Promotion.readComplete',
    READ_ERROR: 'Events.Model.Promotion.readError',
    
    READ_FOR_SELECT_COMPLETE: 'Events.Model.Promotion.readForSelectComplete',
    READ_FOR_SELECT_ERROR: 'Events.Model.Promotion.readForSelectError',

    READ_FOR_AUTOCOMPLETE_COMPLETE: 'Events.Model.Promotion.readForAutocompleteComplete',
    READ_FOR_AUTOCOMPLETE_ERROR: 'Events.Model.Promotion.readForAutocompleteError'
   
});