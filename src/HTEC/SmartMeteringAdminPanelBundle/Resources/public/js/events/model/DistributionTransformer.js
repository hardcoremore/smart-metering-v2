HCM.createNamespace('Events.Model.DistributionTransformer', {

    VALIDATION_ERROR: 'Events.Model.DistributionTransformer.validationError',

    CREATE_COMPLETE: 'Events.Model.DistributionTransformer.createComplete',
    CREATE_ERROR: 'Events.Model.DistributionTransformer.createError',

    READ_COMPLETE: 'Events.Model.DistributionTransformer.readComplete',
    READ_ERROR: 'Events.Model.DistributionTransformer.readError',
    
    READ_FOR_SELECT_COMPLETE: 'Events.Model.DistributionTransformer.readForSelectComplete',
    READ_FOR_SELECT_ERROR: 'Events.Model.DistributionTransformer.readForSelectError',

    READ_FOR_AUTOCOMPLETE_COMPLETE: 'Events.Model.DistributionTransformer.readForAutocompleteComplete',
    READ_FOR_AUTOCOMPLETE_ERROR: 'Events.Model.DistributionTransformer.readForAutocompleteError',

    UPDATE_COMPLETE: 'Events.Model.DistributionTransformer.updateComplete',
    UPDATE_ERROR: 'Events.Model.DistributionTransformer.updateError',

    PATCH_COMPLETE: 'Events.Model.DistributionTransformer.patchComplete',
    PATCH_ERROR: 'Events.Model.DistributionTransformer.patchError',

    DELETE_COMPLETE: 'Events.Model.DistributionTransformer.deleteComplete',
    DELETE_ERROR: 'Events.Model.DistributionTransformer.deleteError'
});