HCM.createNamespace('Events.Model.Feeder', {

    VALIDATION_ERROR: 'Events.Model.Feeder.validationError',

    CREATE_COMPLETE: 'Events.Model.Feeder.createComplete',
    CREATE_ERROR: 'Events.Model.Feeder.createError',

    READ_COMPLETE: 'Events.Model.Feeder.readComplete',
    READ_ERROR: 'Events.Model.Feeder.readError',

    READ_FOR_SELECT_COMPLETE: 'Events.Model.Feeder.readForSelectComplete',
    READ_FOR_SELECT_ERROR: 'Events.Model.Feeder.readForSelectError',

    READ_FOR_AUTOCOMPLETE_COMPLETE: 'Events.Model.Feeder.readForAutocompleteComplete',
    READ_FOR_AUTOCOMPLETE_ERROR: 'Events.Model.Feeder.readForAutocompleteError',

    UPDATE_COMPLETE: 'Events.Model.Feeder.updateComplete',
    UPDATE_ERROR: 'Events.Model.Feeder.updateError',

    PATCH_COMPLETE: 'Events.Model.Feeder.patchComplete',
    PATCH_ERROR: 'Events.Model.Feeder.patchError',

    DELETE_COMPLETE: 'Events.Model.Feeder.deleteComplete',
    DELETE_ERROR: 'Events.Model.Feeder.deleteError'
});