HCM.createNamespace('Events.Model.PowerPurchase', {

    VALIDATION_ERROR: 'Events.Model.PowerPurchase.validationError',

    CREATE_COMPLETE: 'Events.Model.PowerPurchase.createComplete',
    CREATE_ERROR: 'Events.Model.PowerPurchase.createError',

    READ_COMPLETE: 'Events.Model.PowerPurchase.readComplete',
    READ_ERROR: 'Events.Model.PowerPurchase.readError',

    READ_FOR_SELECT_COMPLETE: 'Events.Model.PowerPurchase.readForSelectComplete',
    READ_FOR_SELECT_ERROR: 'Events.Model.PowerPurchase.readForSelectError',

    READ_FOR_AUTOCOMPLETE_COMPLETE: 'Events.Model.PowerPurchase.readForAutocompleteComplete',
    READ_FOR_AUTOCOMPLETE_ERROR: 'Events.Model.PowerPurchase.readForAutocompleteError',

    READ_ALL_FOR_TODAY_COMPLETE: 'Events.Model.PowerForecast.readAllForTodayComplete',
    READ_ALL_FOR_TODAY_ERROR: 'Events.Model.PowerForecast.readAllForTodayError',

    UPDATE_COMPLETE: 'Events.Model.PowerPurchase.updateComplete',
    UPDATE_ERROR: 'Events.Model.PowerPurchase.updateError',

    APPROVE_COMPLETE: 'Events.Model.PowerPurchase.approveComplete',
    APPROVE_ERROR: 'Events.Model.PowerPurchase.approveError',

    PATCH_COMPLETE: 'Events.Model.PowerPurchase.patchComplete',
    PATCH_ERROR: 'Events.Model.PowerPurchase.patchError',

    DELETE_COMPLETE: 'Events.Model.PowerPurchase.deleteComplete',
    DELETE_ERROR: 'Events.Model.PowerPurchase.deleteError'
});