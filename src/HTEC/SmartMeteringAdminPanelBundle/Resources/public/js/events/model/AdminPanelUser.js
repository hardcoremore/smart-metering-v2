HCM.createNamespace('Events.Model.AdminPanelUser', $.extend({}, HCM.getNamespaceValue('Events.Model.User'), {

        ADMIN_UPDATE_USER_PASSWORD_COMPLETE: 'Model.AdminPanelUser.adminUpdateUserPasswordComplete',
        ADMIN_UPDATE_USER_PASSWORD_ERROR: 'Model.AdminPanelUser.adminUpdateUserPasswordError',

        VALIDATE_PIN_CODE_COMPLETE: 'Model.AdminPanelUser.validatePinCodeComplete',
        VALIDATE_PIN_CODE_ERROR: 'Model.AdminPanelUser.validatePinCodeError'
    })
);