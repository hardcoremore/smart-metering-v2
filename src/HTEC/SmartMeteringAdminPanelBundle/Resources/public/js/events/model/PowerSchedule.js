HCM.createNamespace('Events.Model.PowerSchedule', {

    VALIDATION_ERROR: 'Events.Model.PowerSchedule.validationError',

    CREATE_COMPLETE: 'Events.Model.PowerSchedule.createComplete',
    CREATE_ERROR: 'Events.Model.PowerSchedule.createError',

    READ_COMPLETE: 'Events.Model.PowerSchedule.readComplete',
    READ_ERROR: 'Events.Model.PowerSchedule.readError',

    READ_ALL_COMPLETE: 'Events.Model.PowerSchedule.readAllComplete',
    READ_ALL_ERROR: 'Events.Model.PowerSchedule.readAllError',
    
    READ_FOR_SELECT_COMPLETE: 'Events.Model.PowerSchedule.readForSelectComplete',
    READ_FOR_SELECT_ERROR: 'Events.Model.PowerSchedule.readForSelectError',

    READ_FOR_AUTOCOMPLETE_COMPLETE: 'Events.Model.PowerSchedule.readForAutocompleteComplete',
    READ_FOR_AUTOCOMPLETE_ERROR: 'Events.Model.PowerSchedule.readForAutocompleteError',

    UPDATE_COMPLETE: 'Events.Model.PowerSchedule.updateComplete',
    UPDATE_ERROR: 'Events.Model.PowerSchedule.updateError',

    PATCH_COMPLETE: 'Events.Model.PowerSchedule.patchComplete',
    PATCH_ERROR: 'Events.Model.PowerSchedule.patchError',

    DELETE_COMPLETE: 'Events.Model.PowerSchedule.deleteComplete',
    DELETE_ERROR: 'Events.Model.PowerSchedule.deleteError'
});