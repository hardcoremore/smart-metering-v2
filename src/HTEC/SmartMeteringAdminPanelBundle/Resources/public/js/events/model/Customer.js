HCM.createNamespace('Events.Model.Customer', {

    READ_COMPLETE: 'Events.Model.Customer.readComplete',
    READ_ERROR: 'Events.Model.Customer.readError',
    
    READ_FOR_SELECT_COMPLETE: 'Events.Model.Customer.readForSelectComplete',
    READ_FOR_SELECT_ERROR: 'Events.Model.Customer.readForSelectError',

    READ_FOR_AUTOCOMPLETE_COMPLETE: 'Events.Model.Customer.readForAutocompleteComplete',
    READ_FOR_AUTOCOMPLETE_ERROR: 'Events.Model.Customer.readForAutocompleteError',
});