HCM.createNamespace('Events.Model.PowerAllocate', {

    VALIDATION_ERROR: 'Events.Model.PowerAllocate.validationError',

    CREATE_COMPLETE: 'Events.Model.PowerAllocate.createComplete',
    CREATE_ERROR: 'Events.Model.PowerAllocate.createError',

    READ_COMPLETE: 'Events.Model.PowerAllocate.readComplete',
    READ_ERROR: 'Events.Model.PowerAllocate.readError',

    READ_FOR_SELECT_COMPLETE: 'Events.Model.PowerAllocate.readForSelectComplete',
    READ_FOR_SELECT_ERROR: 'Events.Model.PowerAllocate.readForSelectError',

    UPDATE_COMPLETE: 'Events.Model.PowerAllocate.updateComplete',
    UPDATE_ERROR: 'Events.Model.PowerAllocate.updateError',

    PATCH_COMPLETE: 'Events.Model.Appliance.patchComplete',
    PATCH_ERROR: 'Events.Model.Appliance.patchError',

    DELETE_COMPLETE: 'Events.Model.PowerAllocate.deleteComplete',
    DELETE_ERROR: 'Events.Model.PowerAllocate.deleteError'
});