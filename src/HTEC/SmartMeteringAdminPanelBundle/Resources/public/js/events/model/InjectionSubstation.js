HCM.createNamespace('Events.Model.InjectionSubstation', {

    VALIDATION_ERROR: 'Events.Model.InjectionSubstation.validationError',

    CREATE_COMPLETE: 'Events.Model.InjectionSubstation.createComplete',
    CREATE_ERROR: 'Events.Model.InjectionSubstation.createError',

    READ_COMPLETE: 'Events.Model.InjectionSubstation.readComplete',
    READ_ERROR: 'Events.Model.InjectionSubstation.readError',
    
    READ_FOR_SELECT_COMPLETE: 'Events.Model.InjectionSubstation.readForSelectComplete',
    READ_FOR_SELECT_ERROR: 'Events.Model.InjectionSubstation.readForSelectError',

    READ_FOR_AUTOCOMPLETE_COMPLETE: 'Events.Model.InjectionSubstation.readForAutocompleteComplete',
    READ_FOR_AUTOCOMPLETE_ERROR: 'Events.Model.InjectionSubstation.readForAutocompleteError',

    UPDATE_COMPLETE: 'Events.Model.InjectionSubstation.updateComplete',
    UPDATE_ERROR: 'Events.Model.InjectionSubstation.updateError',

    PATCH_COMPLETE: 'Events.Model.InjectionSubstation.patchComplete',
    PATCH_ERROR: 'Events.Model.InjectionSubstation.patchError',

    DELETE_COMPLETE: 'Events.Model.InjectionSubstation.deleteComplete',
    DELETE_ERROR: 'Events.Model.InjectionSubstation.deleteError'
});