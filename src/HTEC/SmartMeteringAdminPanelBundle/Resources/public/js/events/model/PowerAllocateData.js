HCM.createNamespace('Events.Model.PowerAllocateData', {

    VALIDATION_ERROR: 'Events.Model.PowerAllocateData.validationError',

    CREATE_COMPLETE: 'Events.Model.PowerAllocateData.createComplete',
    CREATE_ERROR: 'Events.Model.PowerAllocateData.createError',

    READ_COMPLETE: 'Events.Model.PowerAllocateData.readComplete',
    READ_ERROR: 'Events.Model.PowerAllocateData.readError',

    READ_ALL_COMPLETE: 'Events.Model.PowerAllocateData.readAllComplete',
    READ_ALL_ERROR: 'Events.Model.PowerAllocateData.readAllError',

    UPDATE_COMPLETE: 'Events.Model.PowerAllocateData.updateComplete',
    UPDATE_ERROR: 'Events.Model.PowerAllocateData.updateError',

    PATCH_COMPLETE: 'Events.Model.Appliance.patchComplete',
    PATCH_ERROR: 'Events.Model.Appliance.patchError',

    DELETE_COMPLETE: 'Events.Model.PowerAllocateData.deleteComplete',
    DELETE_ERROR: 'Events.Model.PowerAllocateData.deleteError'
});