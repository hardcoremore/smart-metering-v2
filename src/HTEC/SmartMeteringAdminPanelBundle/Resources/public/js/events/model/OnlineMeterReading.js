HCM.createNamespace('Events.Model.OnlineMeterReading', {

    READ_COMPLETE: 'Events.Model.OnlineMeterReading.readComplete',
    READ_ERROR: 'Events.Model.OnlineMeterReading.readError',
    
    READ_FOR_SELECT_COMPLETE: 'Events.Model.OnlineMeterReading.readForSelectComplete',
    READ_FOR_SELECT_ERROR: 'Events.Model.OnlineMeterReading.readForSelectError',

    READ_FOR_AUTOCOMPLETE_COMPLETE: 'Events.Model.OnlineMeterReading.readForAutocompleteComplete',
    READ_FOR_AUTOCOMPLETE_ERROR: 'Events.Model.OnlineMeterReading.readForAutocompleteError',
});