HCM.createNamespace('Events.Model.FeederLoad', {

    VALIDATION_ERROR: 'Events.Model.FeederLoad.validationError',

    CREATE_COMPLETE: 'Events.Model.FeederLoad.createComplete',
    CREATE_ERROR: 'Events.Model.FeederLoad.createError',

    READ_COMPLETE: 'Events.Model.FeederLoad.readComplete',
    READ_ERROR: 'Events.Model.FeederLoad.readError',
    
    READ_ALL_COMPLETE: 'Events.Model.FeederLoad.readAllComplete',
    READ_ALL_ERROR: 'Events.Model.FeederLoad.readAllError',
    
    READ_FOR_SELECT_COMPLETE: 'Events.Model.FeederLoad.readForSelectComplete',
    READ_FOR_SELECT_ERROR: 'Events.Model.FeederLoad.readForSelectError',

    READ_FOR_AUTOCOMPLETE_COMPLETE: 'Events.Model.FeederLoad.readForAutocompleteComplete',
    READ_FOR_AUTOCOMPLETE_ERROR: 'Events.Model.FeederLoad.readForAutocompleteError',

    UPDATE_COMPLETE: 'Events.Model.FeederLoad.updateComplete',
    UPDATE_ERROR: 'Events.Model.FeederLoad.updateError',

    PATCH_COMPLETE: 'Events.Model.FeederLoad.patchComplete',
    PATCH_ERROR: 'Events.Model.FeederLoad.patchError',

    DELETE_COMPLETE: 'Events.Model.FeederLoad.deleteComplete',
    DELETE_ERROR: 'Events.Model.FeederLoad.deleteError'
});