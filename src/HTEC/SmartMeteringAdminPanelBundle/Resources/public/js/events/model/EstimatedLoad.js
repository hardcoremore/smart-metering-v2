HCM.createNamespace('Events.Model.EstimatedLoad', {

    VALIDATION_ERROR: 'Events.Model.EstimatedLoad.validationError',

    CREATE_COMPLETE: 'Events.Model.EstimatedLoad.createComplete',
    CREATE_ERROR: 'Events.Model.EstimatedLoad.createError',

    READ_COMPLETE: 'Events.Model.EstimatedLoad.readComplete',
    READ_ERROR: 'Events.Model.EstimatedLoad.readError',
    
    READ_FOR_SELECT_COMPLETE: 'Events.Model.EstimatedLoad.readForSelectComplete',
    READ_FOR_SELECT_ERROR: 'Events.Model.EstimatedLoad.readForSelectError',

    READ_FOR_AUTOCOMPLETE_COMPLETE: 'Events.Model.EstimatedLoad.readForAutocompleteComplete',
    READ_FOR_AUTOCOMPLETE_ERROR: 'Events.Model.EstimatedLoad.readForAutocompleteError',

    UPDATE_COMPLETE: 'Events.Model.EstimatedLoad.updateComplete',
    UPDATE_ERROR: 'Events.Model.EstimatedLoad.updateError',

    PATCH_COMPLETE: 'Events.Model.EstimatedLoad.patchComplete',
    PATCH_ERROR: 'Events.Model.EstimatedLoad.patchError',

    DELETE_COMPLETE: 'Events.Model.EstimatedLoad.deleteComplete',
    DELETE_ERROR: 'Events.Model.EstimatedLoad.deleteError'
});