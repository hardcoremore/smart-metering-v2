HCM.createNamespace('Events.Model.City', {

    VALIDATION_ERROR: 'Events.Model.City.validationError',

    CREATE_COMPLETE: 'Events.Model.City.createComplete',
    CREATE_ERROR: 'Events.Model.City.createError',

    READ_COMPLETE: 'Events.Model.City.readComplete',
    READ_ERROR: 'Events.Model.City.readError',

    READ_FOR_SELECT_COMPLETE: 'Events.Model.City.readForSelectComplete',
    READ_FOR_SELECT_ERROR: 'Events.Model.City.readForSelectError',

    READ_FOR_AUTOCOMPLETE_COMPLETE: 'Events.Model.City.readForAutocompleteComplete',
    READ_FOR_AUTOCOMPLETE_ERROR: 'Events.Model.City.readForAutocompleteError',

    UPDATE_COMPLETE: 'Events.Model.City.updateComplete',
    UPDATE_ERROR: 'Events.Model.City.updateError',

    PATCH_COMPLETE: 'Events.Model.City.patchComplete',
    PATCH_ERROR: 'Events.Model.City.patchError',

    DELETE_COMPLETE: 'Events.Model.City.deleteComplete',
    DELETE_ERROR: 'Events.Model.City.deleteError'
});