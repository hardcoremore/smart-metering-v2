HCM.createNamespace('Events.Model.PowerTransformer', {

    VALIDATION_ERROR: 'Events.Model.PowerTransformer.validationError',

    CREATE_COMPLETE: 'Events.Model.PowerTransformer.createComplete',
    CREATE_ERROR: 'Events.Model.PowerTransformer.createError',

    READ_COMPLETE: 'Events.Model.PowerTransformer.readComplete',
    READ_ERROR: 'Events.Model.PowerTransformer.readError',
    
    READ_FOR_SELECT_COMPLETE: 'Events.Model.PowerTransformer.readForSelectComplete',
    READ_FOR_SELECT_ERROR: 'Events.Model.PowerTransformer.readForSelectError',

    READ_FOR_AUTOCOMPLETE_COMPLETE: 'Events.Model.PowerTransformer.readForAutocompleteComplete',
    READ_FOR_AUTOCOMPLETE_ERROR: 'Events.Model.PowerTransformer.readForAutocompleteError',

    UPDATE_COMPLETE: 'Events.Model.PowerTransformer.updateComplete',
    UPDATE_ERROR: 'Events.Model.PowerTransformer.updateError',

    PATCH_COMPLETE: 'Events.Model.PowerTransformer.patchComplete',
    PATCH_ERROR: 'Events.Model.PowerTransformer.patchError',

    DELETE_COMPLETE: 'Events.Model.PowerTransformer.deleteComplete',
    DELETE_ERROR: 'Events.Model.PowerTransformer.deleteError'
});