HCM.createNamespace('Events.Model.Appliance', {

    VALIDATION_ERROR: 'Events.Model.Appliance.validationError',

    CREATE_COMPLETE: 'Events.Model.Appliance.createComplete',
    CREATE_ERROR: 'Events.Model.Appliance.createError',

    READ_COMPLETE: 'Events.Model.Appliance.readComplete',
    READ_ERROR: 'Events.Model.Appliance.readError',

    READ_FOR_SELECT_COMPLETE: 'Events.Model.Appliance.readForSelectComplete',
    READ_FOR_SELECT_ERROR: 'Events.Model.Appliance.readForSelectError',

    READ_FOR_AUTOCOMPLETE_COMPLETE: 'Events.Model.Appliance.readForAutocompleteComplete',
    READ_FOR_AUTOCOMPLETE_ERROR: 'Events.Model.Appliance.readForAutocompleteError',

    UPDATE_COMPLETE: 'Events.Model.Appliance.updateComplete',
    UPDATE_ERROR: 'Events.Model.Appliance.updateError',

    PATCH_COMPLETE: 'Events.Model.Appliance.patchComplete',
    PATCH_ERROR: 'Events.Model.Appliance.patchError',

    DELETE_COMPLETE: 'Events.Model.Appliance.deleteComplete',
    DELETE_ERROR: 'Events.Model.Appliance.deleteError'
});