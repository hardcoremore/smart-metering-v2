HCM.createNamespace('Events.Model.PowerActivateDeactivate', {

    VALIDATION_ERROR: 'Events.Model.PowerActivateDeactivate.validationError',

    CREATE_COMPLETE: 'Events.Model.PowerActivateDeactivate.createComplete',
    CREATE_ERROR: 'Events.Model.PowerActivateDeactivate.createError',

    READ_COMPLETE: 'Events.Model.PowerActivateDeactivate.readComplete',
    READ_ERROR: 'Events.Model.PowerActivateDeactivate.readError',
    
    READ_FOR_SELECT_COMPLETE: 'Events.Model.PowerActivateDeactivate.readForSelectComplete',
    READ_FOR_SELECT_ERROR: 'Events.Model.PowerActivateDeactivate.readForSelectError',

    READ_FOR_AUTOCOMPLETE_COMPLETE: 'Events.Model.PowerActivateDeactivate.readForAutocompleteComplete',
    READ_FOR_AUTOCOMPLETE_ERROR: 'Events.Model.PowerActivateDeactivate.readForAutocompleteError',

    UPDATE_COMPLETE: 'Events.Model.PowerActivateDeactivate.updateComplete',
    UPDATE_ERROR: 'Events.Model.PowerActivateDeactivate.updateError',

    PATCH_COMPLETE: 'Events.Model.PowerActivateDeactivate.patchComplete',
    PATCH_ERROR: 'Events.Model.PowerActivateDeactivate.patchError',

    DELETE_COMPLETE: 'Events.Model.PowerActivateDeactivate.deleteComplete',
    DELETE_ERROR: 'Events.Model.PowerActivateDeactivate.deleteError'
});