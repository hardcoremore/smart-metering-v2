HCM.define({

    name: 'Modules.CustomerNetworkData',

    extendFrom: 'Modules.CrudModule',

    construct: function(app, permissions) {

        this.callConstructor('Modules.CrudModule', [app, permissions]);

        this.getFormModuleClassname = function() {
            return 'Modules.Form.CustomerNetworkDataForm';
        };

        this.getModuleName = function() {
            return 'CustomerNetworkData';
        };
    },

    methods: {

        initModule: function() {

            this.setNotAutoLoadingRelationFields([
                'customer',
                'businessDistrict',
                'injectionSubstation',
                'powerTransformer',
                'powerTransformerFeeder',
                'highTensionPole',
                'distributionTransformer',
                'distributionTransformerFeeder',
                'lowTensionPole'
            ]);

            this.super('initModule');
        },

        initModel: function() {
            this.setModel(this.getApp().getModel('Model.CustomerNetworkData'));
        },

        setControls: function() {

            this.super('setControls');

            var customerFieldModelData = this.getModel().getFieldByName('customer');
            var injectionSubstationFieldModelData = this.getModel().getFieldByName('injectionSubstation');
            var powerTransformerFieldModelData = this.getModel().getFieldByName('powerTransformer');
            var powerTransformerFeederFieldModelData = this.getModel().getFieldByName('powerTransformerFeeder');
            var highTensionPoleFieldModelData = this.getModel().getFieldByName('highTensionPole');
            var distributionTransformerFieldModelData = this.getModel().getFieldByName('distributionTransformer');
            var distributionTransformerFeederFieldModelData = this.getModel().getFieldByName('distributionTransformerFeeder');
            var lowTensionPoleFieldModelData = this.getModel().getFieldByName('lowTensionPole');

            var self = this;

            var customersNetworkDataGrid = this.getModuleElement().find("#customersNetworkData-CustomerNetworkData-grid").grid({

                autoLoad: false,
                cellEditType: 'callback',
                model: [
                    {
                        header: 'ID',
                        id: 'id',
                        dataMap: 'id'
                    },
                    {
                        header: 'Customer',
                        id: 'customer',
                        dataMap: 'customer',
                        formatter: function(cellValue) {
                            if(cellValue && self.isObject(cellValue)) {
                                return cellValue[customerFieldModelData.labelPropertyName];
                            }
                        }
                    },
                    {
                        header: 'Account Number',
                        id: 'accountNumber',
                        dataMap: 'customer',
                        formatter: function(cellValue) {
                            if(cellValue && self.isObject(cellValue)) {
                                return cellValue.accountNumber;
                            }
                        }
                    },
                    {
                        header: 'Tariff',
                        id: 'tariff',
                        dataMap: 'customer',
                        formatter: function(cellValue) {
                            if(cellValue && self.isObject(cellValue) && cellValue.tariff) {
                                return cellValue.tariff.name;
                            }
                        }
                    },
                    {
                        header: 'Business District',
                        id: 'businessDistrict',
                        dataMap: 'customer',
                        formatter: function(cellValue) {
                            if(cellValue && self.isObject(cellValue) && cellValue.businessDistrict) {
                                return cellValue.businessDistrict.name;
                            }
                        }
                    },
                    {
                        header: 'Injection Substation',
                        id: 'injectionSubstation',
                        dataMap: 'injectionSubstation',
                        formatter: function(cellValue) {
                            if(cellValue && self.isObject(cellValue)) {
                                return cellValue[injectionSubstationFieldModelData.labelPropertyName];
                            }
                        }
                    },
                    {
                        header: 'Power Transformer',
                        id: 'powerTransformer',
                        dataMap: 'powerTransformer',
                        formatter: function(cellValue) {
                            if(cellValue && self.isObject(cellValue)) {
                                return cellValue[powerTransformerFieldModelData.labelPropertyName];
                            }
                        }
                    },
                    {
                        header: 'Power Transformer Feeder',
                        id: 'powerTransformerFeeder',
                        dataMap: 'powerTransformerFeeder',
                        formatter: function(cellValue) {
                            if(cellValue && self.isObject(cellValue)) {
                                return cellValue[powerTransformerFeederFieldModelData.labelPropertyName];
                            }
                        }
                    },
                    {
                        header: 'High Tension Pole',
                        id: 'highTensionPole',
                        dataMap: 'highTensionPole',
                        formatter: function(cellValue) {
                            if(cellValue && self.isObject(cellValue)) {
                                return cellValue[highTensionPoleFieldModelData.labelPropertyName];
                            }
                        }
                    },
                    {
                        header: 'Distribution Transformer',
                        id: 'distributionTransformer',
                        dataMap: 'distributionTransformer',
                        formatter: function(cellValue) {
                            if(cellValue && self.isObject(cellValue)) {
                                return cellValue[distributionTransformerFieldModelData.labelPropertyName];
                            }
                        }
                    },
                    {
                        header: 'Distribution Transformer Feeder',
                        id: 'distributionTransformerFeeder',
                        dataMap: 'distributionTransformerFeeder',
                        formatter: function(cellValue) {
                            if(cellValue && self.isObject(cellValue)) {
                                return cellValue[distributionTransformerFeederFieldModelData.labelPropertyName];
                            }
                        }
                    },
                    {
                        header: 'Low Tension Pole',
                        id: 'lowTensionPole',
                        dataMap: 'lowTensionPole',
                        formatter: function(cellValue) {
                            if(cellValue && self.isObject(cellValue)) {
                                return cellValue[lowTensionPoleFieldModelData.labelPropertyName];
                            }
                        }
                    }
                ]
            });
    
            this.setMainViewComponent(customersNetworkDataGrid);
        }
    }
});