HCM.define({

    name: 'Modules.Meter',

    extendFrom: 'Modules.CrudModule',

    construct: function(app, permissions) {

        this.callConstructor('Modules.CrudModule', [app, permissions]);

        this.getFormModuleClassname = function() {
            return 'Modules.Form.Meter';
        };

        this.getModuleName = function() {
            return 'Meter';
        };
    },

    methods: {

        initModule: function() {
            this.setNotAutoLoadingRelationFields(['customer', 'distributionTransformer', 'feeder']);
            this.super('initModule');
        },

        initModel: function() {
            this.setModel(this.getApp().getModel('Model.Meter'));
        },

        setControls: function() {

            this.super('setControls');

            var customerFieldModelData = this.getModel().getFieldByName('customer');
            var distributionTransformerFieldModelData = this.getModel().getFieldByName('distributionTransformer');
            var feederFieldModelData = this.getModel().getFieldByName('feeder');

            var self = this;

            var metersGrid = this.getModuleElement().find("#meters-Meter-grid").grid({

                autoLoad: false,
                cellEdit: true,
                cellEditType: 'callback',
                model: [
                    {
                        header: 'ID',
                        id: 'id',
                        dataMap: 'id'
                    },
                    {
                        header: 'Type',
                        id: 'type',
                        dataMap: 'type',
                        editable: true,
                        editOptions: {
                            type: 'select',
                            valueProperty: 'value',
                            labelProperty: 'label',
                            availableOptions:[
                                {value: 'live', label: 'Live'},
                                {value: 'offline', label: 'Offline'}
                            ],
                            areAvailableOptionsStatic: true
                        },
                        formatter: function(cellValue) {
                            if(cellValue) {
                                return cellValue.charAt(0).toUpperCase() + cellValue.slice(1);
                            }
                        }
                    },
                    {
                        header: 'Status',
                        id: 'status',
                        dataMap: 'status',
                        editable: true,
                        editOptions: {
                            type: 'select',
                            valueProperty: 'value',
                            labelProperty: 'label',
                            availableOptions:[
                                {value: 'live', label: 'Live'},
                                {value: 'offline', label: 'Offline'},
                                {value: 'repairing', label: 'Repairing'}
                            ],
                            areAvailableOptionsStatic: true
                        },
                        formatter: function(cellValue) {
                            if(cellValue) {
                                return cellValue.charAt(0).toUpperCase() + cellValue.slice(1);
                            }
                        }
                    },
                    {
                        header: 'Serial Number',
                        id: 'serialNumber',
                        dataMap: 'serialNumber',
                        editable: true
                    },
                    {
                        header: 'Customer',
                        id: 'customer',
                        dataMap: 'customer',
                        editable: true,
                        formatter: function(cellValue) {
                            if(cellValue && self.isObject(cellValue)) {
                                return cellValue[customerFieldModelData.labelPropertyName];
                            }
                        }
                    },
                    {
                        header: 'Distribution Transformer',
                        id: 'distributionTransformer',
                        dataMap: 'distributionTransformer',
                        editable: false,
                        formatter: function(cellValue) {
                            if(cellValue && self.isObject(cellValue)) {
                                return cellValue[distributionTransformerFieldModelData.labelPropertyName];
                            }
                        }
                    },
                    {
                        header: 'Feeder',
                        id: 'feeder',
                        dataMap: 'feeder',
                        editable: false,
                        formatter: function(cellValue) {
                            if(cellValue && self.isObject(cellValue)) {
                                return cellValue[feederFieldModelData.labelPropertyName];
                            }
                        }
                    }
                ]
            });
        
            var customerGridCellEditOptions = this.getAutoCompleteGridCellEditOptions(
                    metersGrid,
                    customerFieldModelData
            );

            metersGrid.grid(
                'setColumnEditOptions',
                'customer',
                customerGridCellEditOptions
            );

            var distributionTransformerGridCellEditOptions = this.getAutoCompleteGridCellEditOptions(
                    metersGrid,
                    distributionTransformerFieldModelData
            );

            metersGrid.grid(
                'setColumnEditOptions',
                'distributionTransformer',
                distributionTransformerGridCellEditOptions
            );

            this.setMainViewComponent(metersGrid);
        }
    }
});