HCM.define({

    name: 'Modules.PowerForecast',

    extendFrom: 'Modules.CrudModule',

    construct: function(app, permissions) {

        this.callConstructor('Modules.CrudModule', [app, permissions]);

        var forecastDetailsTableHolder;
        var closeForecastDetailsViewButton;
        var currentPowerForecastDetailsLoaded;

        this.setForecastDetailsTableHolder = function(holder) {
            forecastDetailsTableHolder = holder;
        };

        this.getForecastDetailsTableHolder = function() {
            return forecastDetailsTableHolder;
        };

        this.setCloseForecastDetailsViewButton = function(button) {
            closeForecastDetailsViewButton = button;
        };

        this.getCloseForecastDetailsViewButton = function() {
            return closeForecastDetailsViewButton;
        };

        this.setCurrentPowerForecastDetailsLoaded = function(powerForecast) {
            currentPowerForecastDetailsLoaded = powerForecast;
        };

        this.getCurrentPowerForecastDetailsLoaded = function() {
            return currentPowerForecastDetailsLoaded;
        };

        this.getFormModuleClassname = function() {
            return 'Modules.Form.PowerForecast';
        };

        this.getModuleName = function() {
            return 'PowerForecast';
        };
    },

    methods: {

        initModel: function() {

            this.setNotAutoLoadingRelationFields([
                'powerPurchase',
            ]);

            this.setModel(this.getApp().getModel('Model.PowerForecast'));
        },

        initController: function() {
            var crudController = this.getApp().getController('Controllers.Module.PowerForecast', [this, this.getModel()]);
            this.setController(crudController);
        },

        setControls: function() {

            this.super('setControls');

            var powerPurchaseModelData = this.getModel().getFieldByName('powerPurchase');

            var powerForecastGrid = $("#powerForecasts-PowerForecast-grid").grid({

                autoLoad: false,
                cellEdit: false,
                cellEditType: 'callback',
                model: [
                    {
                        header: 'ID',
                        id: 'id',
                        dataMap: 'id'
                    },
                    {
                        header: 'Power Purchase',
                        id: 'powerPurchase',
                        dataMap: 'powerPurchase',
                        formatter: powerPurchaseModelData.labelPropertyName
                    },
                    {
                        header: 'Power Type',
                        id: 'powerType',
                        dataMap: 'powerType'
                    },
                    {
                        header: 'Amount Forecasted',
                        id: 'amountToForecast',
                        dataMap: 'amountToForecast'
                    },
                    {
                        header: 'Date time',
                        id: 'createdDatetime',
                        dataMap: 'createdDatetime',
                        formatter: function(cellValue) {
                            return moment(cellValue).format('YYYY-MM-DD HH:mm:ss');
                        }
                    },
                    {
                        header: 'Customers Affected',
                        id: 'totalCustomersAffected',
                        dataMap: 'totalCustomersAffected',
                        formatter: function(cellValue) {
                            return Number(cellValue).formatNumberThousands();
                        }
                    },
                    {
                        header: 'Average Allocation Per Customer',
                        id: 'averageAllocationPerCustomer',
                        dataMap: 'averageAllocationPerCustomer'
                    },
                    {
                        header: 'Highest Allocation in Business District',
                        id: 'highestAllocationBusinessDistrict',
                        dataMap: 'highestAllocationBusinessDistrict',
                        formatter: function(cellValue) {
                            if(cellValue) {
                                return cellValue.name;
                            }
                        }
                    },
                    {
                        header: 'Highest Allocation in Business District Value',
                        id: 'highestBusinessDistrictAllocationValue',
                        dataMap: 'highestBusinessDistrictAllocationValue',
                        formatter: function(cellValue) {
                            return Number(cellValue).formatNumberThousands();
                        }
                    },
                    {
                        header: 'Highest Allocation in Tariff',
                        id: 'highestAllocationTariff',
                        dataMap: 'highestAllocationTariff',
                        formatter: function(cellValue) {
                            if(cellValue) {
                                return cellValue.name;
                            }
                        }
                    },
                    {
                        header: 'Highest Allocation in Tariff Value',
                        id: 'highestTariffAllocationValue',
                        dataMap: 'highestTariffAllocationValue',
                        formatter: function(cellValue) {
                            return Number(cellValue).formatNumberThousands();
                        }
                    }
                ]
            });

            this.setMainViewComponent(powerForecastGrid);

            this.setForecastDetailsTableHolder(this.getModuleElement().find("#forecast-details-table-holder"));
            this.getForecastDetailsTableHolder().hide();

            this.setCloseForecastDetailsViewButton(this.getModuleElement().find("#close-forecast-details-view-button"));
            this.getCloseForecastDetailsViewButton().hide();
        },

        closePowerForecastDetailsView: function() {

            this.getMainViewComponent().show();
            this.getPagingComponent().show();
            this.getQuickSearchComponent().show();

            this.getModuleCrudControls().show();

            this.getForecastDetailsTableHolder().hide();

            this.getCloseForecastDetailsViewButton().hide();
        },

        viewPowerForecastDetails: function(powerForecastData) {

            this.getMainViewComponent().hide();
            this.getPagingComponent().hide();
            this.getQuickSearchComponent().hide();

            this.getModuleCrudControls().hide();

            this.getCloseForecastDetailsViewButton().show();

            this.getForecastDetailsTableHolder().empty();

            var tariffCodes = this.getModel().extractDistinctTariffCodes(powerForecastData.powerForecastTariffData);

            var tariffDataHtml = this.getApp().compileTemplate(
                "power-forecast-tariff-data-template",
                {
                    data: powerForecastData.powerForecastTariffData,
                    tariffCodes: tariffCodes
                }
            );
            this.getForecastDetailsTableHolder().append(tariffDataHtml);



            var businessDistrictTariffDataHtml = this.getApp().compileTemplate(
                "power-forecast-business-district-tariff-data-template",
                {
                    groupedData: this.getModel().groupByBusinessDistrict(powerForecastData.businessDistrictTariffsData),
                    tariffCodes: tariffCodes
                }
            );
            this.getForecastDetailsTableHolder().append(businessDistrictTariffDataHtml);


            var businessDistrictTotalDataHtml = this.getApp().compileTemplate(
                "power-forecast-business-district-totals-data-template",
                {data:powerForecastData.businessDistrictTotals}
            );
            this.getForecastDetailsTableHolder().append(businessDistrictTotalDataHtml);

            this.getForecastDetailsTableHolder().show();
        },

        loadPowerForecastDetails: function(powerForecastId) {
            this.getModel().readPowerForecastFullDetails(powerForecastId, {scope:this});
        }
    }
});