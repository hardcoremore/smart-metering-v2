HCM.define({

    name: 'Modules.Tariff',

    extendFrom: 'Modules.CrudModule',

    construct: function(app, permissions) {

        this.callConstructor('Modules.CrudModule', [app, permissions]);

        this.getFormModuleClassname = function() {
            return 'Modules.Form.Tariff';
        };

        this.getModuleName = function() {
            return 'Tariff';
        };
    },

    methods: {

        initModel: function() {
            this.setModel(this.getApp().getModel('Model.Tariff'));
        },

        setControls: function() {

            this.super('setControls');

            var tariffsGrid = $("#tariffs-Tariff-grid").grid({

                autoLoad: false,
                cellEdit: true,
                cellEditType: 'callback',
                model: [
                    {
                        header: 'ID',
                        id: 'id',
                        dataMap: 'id'
                    },
                    {
                        header: 'Name',
                        id: 'name',
                        dataMap: 'name',
                        editable: true,
                    },
                    {
                        header: 'Code',
                        id: 'code',
                        dataMap: 'code',
                        editable: true,
                    },
                    {
                        header: 'Price',
                        id: 'price',
                        dataMap: 'price',
                        editable: true
                    },
                    {
                        header: 'Average Consumption Per Day kW/h',
                        id: 'averageConsumptionPerDay',
                        dataMap: 'averageConsumptionPerDay',
                        editable: true
                    }
                ]
            });

            this.setMainViewComponent(tariffsGrid);
        }
    }
});
