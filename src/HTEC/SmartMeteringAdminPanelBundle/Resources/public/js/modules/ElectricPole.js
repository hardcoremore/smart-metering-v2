HCM.define({

    name: 'Modules.ElectricPole',

    extendFrom: 'Modules.CrudModule',

    construct: function(app, permissions) {

        this.callConstructor('Modules.CrudModule', [app, permissions]);

        this.getFormModuleClassname = function() {
            return 'Modules.Form.ElectricPole';
        };

        this.getModuleName = function() {
            return 'ElectricPole';
        };
    },

    methods: {

        initModule: function() {

            this.setNotAutoLoadingRelationFields(['feeder']);
            this.super('initModule');
        },

        initModel: function() {
            this.setModel(this.getApp().getModel('Model.ElectricPole'));
        },

        setControls: function() {

            this.super('setControls');

            var feederFieldModelData = this.getModel().getFieldByName('feeder');

            var electricPolesGrid = $("#electricPoles-ElectricPole-grid").grid({

                autoLoad: false,
                cellEdit: true,
                cellEditType: 'callback',
                model: [
                    {
                        header: 'ID',
                        id: 'id',
                        dataMap: 'id'
                    },
                    {
                        header: 'Type',
                        id: 'type',
                        dataMap: 'type',
                        editable: true,
                        editOptions: {
                            type: 'select',
                            valueProperty: 'value',
                            labelProperty: 'label',
                            availableOptions:[
                                {value: 'low-tension', label: 'Low Tension'},
                                {value: 'high-tension', label: 'High Tension'}
                            ],
                            areAvailableOptionsStatic: true
                        },
                        formatter: function(cellValue) {

                            if(cellValue) {

                                var words = cellValue.split('-');

                                var firstWord = words[0].charAt(0).toUpperCase() + words[0].slice(1);
                                var secondWord = words[1].charAt(0).toUpperCase() + words[1].slice(1);

                                return firstWord + ' ' + secondWord;
                            }

                            return '';
                        }
                    },
                    {
                        header: 'Status',
                        id: 'status',
                        dataMap: 'status',
                        editable: true,
                        editOptions: {
                            type: 'select',
                            valueProperty: 'value',
                            labelProperty: 'label',
                            availableOptions:[
                                {value: 'live', label: 'Live'},
                                {value: 'offline', label: 'Offline'},
                                {value: 'repairing', label: 'Repairing'}
                            ],
                            areAvailableOptionsStatic: true
                        },
                        formatter: function(cellValue) {
                            if(cellValue) {
                                return cellValue.charAt(0).toUpperCase() + cellValue.slice(1);
                            }
                        }
                    },
                    {
                        header: 'Code',
                        id: 'code',
                        dataMap: 'code',
                        editable: true
                    },
                    {
                        header: 'Address',
                        id: 'address',
                        dataMap: 'address',
                        editable: true
                    },
                    {
                        header: 'Feeder',
                        id: 'feeder',
                        dataMap: 'feeder',
                        editable: true,
                        formatter: function(cellValue) {

                            if(cellValue) {

                                return cellValue[feederFieldModelData.labelPropertyName];
                            }

                            return 'N/A';
                        }
                    }
                ]
            });

            var feederGridCellEditOptions = this.getAutoCompleteGridCellEditOptions(
                electricPolesGrid,
                feederFieldModelData
            );

            electricPolesGrid.grid(
                'setColumnEditOptions',
                'feeder',
                feederGridCellEditOptions
            );

            this.setMainViewComponent(electricPolesGrid);
        }
    }
});
