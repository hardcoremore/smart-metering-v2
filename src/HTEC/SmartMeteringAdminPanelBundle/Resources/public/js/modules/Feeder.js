HCM.define({

    name: 'Modules.Feeder',

    extendFrom: 'Modules.CrudModule',

    construct: function(app, permissions) {

        this.callConstructor('Modules.CrudModule', [app, permissions]);

        this.getFormModuleClassname = function() {
            return 'Modules.Form.Feeder';
        };

        this.getModuleName = function() {
            return 'Feeder';
        };
    },

    methods: {

        initModel: function() {
            this.setModel(this.getApp().getModel('Model.Feeder'));
        },

        setControls: function() {

            this.super('setControls');

            var feedersGrid = $("#feeders-Feeder-grid").grid({

                autoLoad: false,
                cellEdit: true,
                cellEditType: 'callback',
                model: [
                    {
                        header: 'ID',
                        id: 'id',
                        dataMap: 'id'
                    },
                    {
                        header: 'Name',
                        id: 'name',
                        dataMap: 'name',
                        editable: true,
                    },
                    {
                        header: 'Status',
                        id: 'status',
                        dataMap: 'status',
                        editable: true,
                        editOptions: {
                            type: 'select',
                            valueProperty: 'value',
                            labelProperty: 'label',
                            availableOptions:[
                                {value: 'live', label: 'Live'},
                                {value: 'offline', label: 'Offline'},
                                {value: 'repairing', label: 'Repairing'}
                            ],
                            areAvailableOptionsStatic: true
                        },
                        formatter: function(cellValue) {
                            if(cellValue) {
                                return cellValue.charAt(0).toUpperCase() + cellValue.slice(1);
                            }
                        }
                    },
                    {
                        header: 'Type',
                        id: 'type',
                        dataMap: 'type',
                        editable: true,
                        editOptions: {
                            type: 'select',
                            valueProperty: 'value',
                            labelProperty: 'label',
                            availableOptions:[
                                {value: 'low-voltage', label: 'Low Voltage'},
                                {value: 'high-voltage', label: 'High Voltage'}
                            ],
                            areAvailableOptionsStatic: true
                        },
                        formatter: function(cellValue) {

                            if(cellValue) {

                                var words = cellValue.split('-');

                                var firstWord = words[0].charAt(0).toUpperCase() + words[0].slice(1);
                                var secondWord = words[1].charAt(0).toUpperCase() + words[1].slice(1);

                                return firstWord + ' ' + secondWord;
                            }

                            return '';
                        }
                    },
                    {
                        header: 'Formation',
                        id: 'formation',
                        dataMap: 'formation',
                        editable: true,
                        editOptions: {
                            type: 'select',
                            valueProperty: 'value',
                            labelProperty: 'label',
                            availableOptions:[
                                {value: 'overhead', label: 'Overhead'},
                                {value: 'underground', label: 'Underground'},
                                {value: 'overhead-underground', label: 'Overhead and Underground'}
                            ],
                            areAvailableOptionsStatic: true
                        },
                        formatter: function(cellValue) {

                            if(cellValue === 'overhead-underground') {
                                return 'Overhead and Underground';
                            }
                            else if(cellValue) {
                                return cellValue.charAt(0).toUpperCase() + cellValue.slice(1);
                            }
                        }
                    },
                    {
                        header: 'Length',
                        id: 'length',
                        dataMap: 'length',
                        editable: true,
                    },
                    {
                        header: 'Street coverage',
                        id: 'streetCoverage',
                        dataMap: 'streetCoverage',
                        editable: false,
                        formatter: function(cellValue) {

                            if(cellValue) {

                                var label = '';

                                for(var i = 0, len = cellValue.length; i < len; i++) {
                                    label += cellValue[i] + ', ';
                                }

                                return label.substring(0, label.length - 2);
                            }

                            return 'N/A';
                        }
                    },
                    {
                        header: 'Code',
                        id: 'code',
                        dataMap: 'code',
                        editable: true
                    },
                    {
                        header: 'Kilovolts (kV)',
                        id: 'kilovolts',
                        dataMap: 'kilovolts',
                        editable: true
                    }
                ]
            });

            this.setMainViewComponent(feedersGrid);
        }
    }
});
