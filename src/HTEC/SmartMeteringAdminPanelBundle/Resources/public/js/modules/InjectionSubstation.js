HCM.define({

    name: 'Modules.InjectionSubstation',

    extendFrom: 'Modules.CrudModule',

    construct: function(app, permissions) {

        this.callConstructor('Modules.CrudModule', [app, permissions]);

        this.getFormModuleClassname = function() {
            return 'Modules.Form.InjectionSubstation';
        };

        this.getModuleName = function() {
            return 'InjectionSubstation';
        };
    },

    methods: {

        initModule: function() {

            this.setNotAutoLoadingRelationFields(['inputFeeders']);
            this.super('initModule');
        },

        initModel: function() {
            this.setModel(this.getApp().getModel('Model.InjectionSubstation'));
        },

        setControls: function() {

            this.super('setControls');

            var businessDistrictModelData = this.getModel().getFieldByName('businessDistrict');
            var powerSourceFieldModelData = this.getModel().getFieldByName('powerSource');
            var inputFeedersFieldModelData = this.getModel().getFieldByName('inputFeeders');

            var self = this;

            var injectionSubstationsGrid = this.getModuleElement().find("#injectionSubstations-InjectionSubstation-grid").grid({

                autoLoad: false,
                cellEdit: true,
                cellEditType: 'callback',
                model: [
                    {
                        header: 'ID',
                        id: 'id',
                        dataMap: 'id'
                    },
                    {
                        header: 'Name',
                        id: 'name',
                        dataMap: 'name',
                        editable: true
                    },
                    {
                        header: 'Business District',
                        id: 'businessDistrict',
                        dataMap: 'businessDistrict',
                        editable: true,
                        editOptions: {
                            type: 'select',
                            valueProperty: businessDistrictModelData.valuePropertyName,
                            labelProperty: businessDistrictModelData.labelPropertyName,
                        },
                        formatter: function(cellValue) {
                            if(cellValue && self.isObject(cellValue)) {
                                return cellValue[businessDistrictModelData.labelPropertyName];
                            }
                        }
                    },
                    {
                        header: 'Address',
                        id: 'address',
                        dataMap: 'address',
                        editable: true
                    },
                    {
                        header: 'Code',
                        id: 'code',
                        dataMap: 'code',
                        editable: true
                    },
                    {
                        header: 'Power Source',
                        id: 'powerSource',
                        dataMap: 'powerSource',
                        editable: true,
                        editOptions: {
                            type: 'select',
                            valueProperty: powerSourceFieldModelData.valuePropertyName,
                            labelProperty: powerSourceFieldModelData.labelPropertyName,
                        },
                        formatter: function(cellValue) {

                            if(cellValue && self.isObject(cellValue)) {
                                return cellValue[powerSourceFieldModelData.labelPropertyName];
                            }

                            return 'N/A';
                        }
                    },
                    {
                        header: 'Power Source Feeders',
                        id: 'inputFeeders',
                        dataMap: 'inputFeeders',
                        editable: true,
                        formatter: function(cellValue) {

                            if(cellValue) {

                                var label = '';

                                for(var i = 0, len = cellValue.length; i < len; i++) {
                                    label += cellValue[i][inputFeedersFieldModelData.labelPropertyName] + ', ';
                                }

                                return label.substring(0, label.length - 2);
                            }

                            return 'N/A';
                        }
                    },
                ]
            });

            // set input feeders grid cell edit widget
            var inputFeedersGridCellEditOptions = this.getMultiAutoCompleteGridCellEditOptions(
                injectionSubstationsGrid,
                inputFeedersFieldModelData,
                this.getModel(),
                {type: 'high-voltage'}
            );

            injectionSubstationsGrid.grid(
                'setColumnEditOptions',
                'inputFeeders',
                inputFeedersGridCellEditOptions
            );

            this.setMainViewComponent(injectionSubstationsGrid);
        }
    }
});