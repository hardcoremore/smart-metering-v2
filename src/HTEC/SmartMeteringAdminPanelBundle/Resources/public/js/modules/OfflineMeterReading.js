HCM.define({

    name: 'Modules.OfflineMeterReading',

    extendFrom: 'Modules.CrudModule',

    construct: function(app, permissions) {

        this.callConstructor('Modules.CrudModule', [app, permissions]);

        var offlineMeterReadingApprovePopup;

        this.setOfflineMeterReadingApprovePopup = function(psap) {
            offlineMeterReadingApprovePopup = psap;
        };

        this.getOfflineMeterReadingApprovePopup = function() {
            return offlineMeterReadingApprovePopup;
        };

        this.getModuleName = function() {
            return 'OfflineMeterReading';
        };
    },

    methods: {

        initModel: function() {
            this.setModel(this.getApp().getModel('Model.OfflineMeterReading'));
        },

        initController: function() {
            var crudController = this.getApp().getController('Controllers.Module.OfflineMeterReading', [this, this.getModel()]);
            this.setController(crudController);
        },

        setControls: function() {

            this.super('setControls');

            var self = this;

            var offlineReadingsGrid = this.getModuleElement().find("#offlineMeterReadings-OfflineMeterReading-grid").grid({

                autoLoad: false,
                cellEdit: false,
                cellEditType: 'callback',
                model: [
                    {
                        header: 'ID',
                        id: 'id',
                        dataMap: 'id'
                    },
                    {
                        header: 'Account Number',
                        id: 'accountNumber',
                        dataMap: 'accountNumber'
                    },
                    {
                        header: 'Meter Number',
                        id: 'meterNumber',
                        dataMap: 'meterNumber'
                    },
                    {
                        header: 'Date Read',
                        id: 'dateRead',
                        dataMap: 'dateRead',
                        formatter: function(cellValue) {
                            if(cellValue) {
                                return moment(cellValue).format('YYYY-MM-DD HH:mm:ss');
                            }
                        }
                    },
                    {
                        header: 'Previous Meter Reading',
                        id: 'previousMeterReading',
                        dataMap: 'previousMeterReading'
                    },
                    {
                        header: 'Current Meter Reading',
                        id: 'currentMeterReading',
                        dataMap: 'currentMeterReading'
                    },
                    {
                        header: 'Consumption',
                        id: 'consumption',
                        dataMap: 'consumption'
                    },
                    {
                        header: 'Reader',
                        id: 'reader',
                        dataMap: 'reader'
                    },
                    {
                        header: 'Suspect',
                        id: 'suspect',
                        dataMap: 'suspect',
                        formatter: function(cellValue) {

                            if(cellValue === true) {
                                return '<span class="grid-red-text">Yes</span>';
                            }
                            else {
                                return '<span class="grid-green-text">No</span>';
                            }
                        }
                    },
                    {
                        header: 'Previous Consumption',
                        id: 'previousConsumption',
                        dataMap: 'previousConsumption'
                    },
                    {
                        header: 'Previous Date Read',
                        id: 'previousDateRead',
                        dataMap: 'previousDateRead',
                        formatter: function(cellValue) {
                            if(cellValue) {
                                return moment(cellValue).format('YYYY-MM-DD HH:mm:ss');
                            }
                        }
                    },
                    {
                        header: 'Average Daily Consumption',
                        id: 'averageDailyConsumption',
                        dataMap: 'averageDailyConsumption',
                        formatter: function(cellValue) {
                            if(cellValue) {
                                return Number(cellValue).toFixed(2);
                            }
                        }
                    }
                ]
            });

            this.setMainViewComponent(offlineReadingsGrid);

            var powerPurchaseApprovePopup = this.getModuleElement().find("#approve-offline-meter-reading-popup").popup({
                    compileTemplateFunction: this.getApp().compileTemplate,
                    compileTemplateData: {
                        title: 'Are you sure you want to approve this suspect offline meter reading?',
                        okLabel: 'Yes',
                        cancelLabel: 'No'
                    }
                });

            this.setOfflineMeterReadingApprovePopup(powerPurchaseApprovePopup);
        },

        displayOfflineMeterReadingImage: function(offlineReading) {

            var domain = this.getApp().getConfig().getParameter('offlineReadingDomain');
            var popupElement = this.getModuleElement().find('#offline-meter-reading-image-popup').clone();

            var popup = popupElement.popup({
                destroyOnClose: true,
                compileTemplateFunction: this.getApp().compileTemplate,
                compileTemplateData: {
                    title: 'Offline reading image for: ' + offlineReading.accountNumber,
                    imageSrc: domain + '/' + offlineReading.image
                }
            });

            popup.popup('open');
        },

        approveMeterReading: function(meterReading) {

            this.getModel().patch(meterReading.id, {suspect: 'false'}, {
                scope: {
                    module: this,
                    fieldModelData: this.getModel().getFieldByName('suspect')
                }
            });
        }
    }
});