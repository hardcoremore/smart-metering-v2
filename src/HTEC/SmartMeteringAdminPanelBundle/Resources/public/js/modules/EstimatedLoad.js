HCM.define({

    name: 'Modules.EstimatedLoad',

    extendFrom: 'Modules.CrudModule',

    construct: function(app, permissions) {

        this.callConstructor('Modules.CrudModule', [app, permissions]);

        this.getFormModuleClassname = function() {
            return 'Modules.Form.EstimatedLoadForm';
        };

        this.getModuleName = function() {
            return 'EstimatedLoad';
        };
    },

    methods: {

        initModule: function() {
            this.setNotAutoLoadingRelationFields(['customer', 'customerCategory']);
            this.super('initModule');
        },

        initModel: function() {
            this.setModel(this.getApp().getModel('Model.EstimatedLoad'));
        },

        setControls: function() {

            this.super('setControls');

            var customerFieldModelData = this.getModel().getFieldByName('customer');
            var customerCategoryFieldModelData = this.getModel().getFieldByName('customerCategory');

            var self = this;

            var estimatedLoadsGrid = this.getModuleElement().find("#estimatedLoads-EstimatedLoad-grid").grid({

                autoLoad: false,
                cellEdit: true,
                cellEditType: 'callback',
                model: [
                    {
                        header: 'ID',
                        id: 'id',
                        dataMap: 'id'
                    },
                    {
                        header: 'Customer',
                        id: 'customer',
                        dataMap: 'customer',
                        editable: true,
                        formatter: function(cellValue) {
                            if(cellValue && self.isObject(cellValue)) {
                                return cellValue[customerFieldModelData.labelPropertyName];
                            }
                        }
                    },
                    {
                        header: 'Customer Category',
                        id: 'customerCategory',
                        dataMap: 'customerCategory',
                        editable: false,
                        formatter: function(cellValue) {
                            if(cellValue && self.isObject(cellValue)) {
                                return cellValue[customerCategoryFieldModelData.labelPropertyName];
                            }
                        }
                    },
                    {
                        header: 'Total Load',
                        id: 'totalLoad',
                        dataMap: 'totalLoad',
                        editable: false,
                        formatter: function(cellValue) {
                            return Number(cellValue).formatNumberThousands();
                        }
                    },
                    {
                        header: 'Created By',
                        id: 'createdBy',
                        dataMap: 'createdBy',
                        editable: false,
                        formatter: function(cellValue) {
                            if(cellValue && self.isObject(cellValue)) {
                                return cellValue.firstName +   ' ' + cellValue.lastName;
                            }
                        }
                    },
                    {
                        header: 'Created Datetime',
                        id: 'createdDatetime',
                        dataMap: 'createdDatetime',
                        editable: false,
                        formatter: function(cellValue) {
                            return moment(cellValue).format('YYYY-MM-DD HH:mm:ss');
                        }
                    }
                ]
            });
        
            var customerGridCellEditOptions = this.getAutoCompleteGridCellEditOptions(
                estimatedLoadsGrid,
                customerFieldModelData
            );

            estimatedLoadsGrid.grid(
                'setColumnEditOptions',
                'customer',
                customerGridCellEditOptions
            );

            this.setMainViewComponent(estimatedLoadsGrid);
        },

        updateMainViewComponentFromMonitorData: function(monitorData) {

            var applianceModule = this.getFormModule().getEstimatedLoadApplianceModule();
            var applianceMonitorData = applianceModule.getModel().getRecordManager().getMonitorData(applianceModule.getInstanceId());

            if(applianceMonitorData && (applianceMonitorData.created || applianceMonitorData.updated)) {

                applianceModule.getModel().getRecordManager().removeMonitorData(applianceModule.getInstanceId());
                this.getModel().getRecordManager().removeMonitorData(this.getInstanceId());

                var pagingParameters = this.getPagingComponentPageParameters();

                pagingParameters = this.getPagingParameters(
                    pagingParameters.pageNumber,
                    pagingParameters.rowsPerPage,
                    this.getCurrentSortColumnName(),
                    this.getCurrentSortColumnOrder()
                );

                this.readRecords(pagingParameters);
            }
            else {
                this.super('updateFromMonitorData', [monitorData]);
            }
        }
    }
});