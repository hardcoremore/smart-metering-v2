HCM.define({

    name: 'Modules.EstimatedLoadAppliance',

    extendFrom: 'Modules.CrudModule',

    construct: function(app, permissions) {

        this.callConstructor('Modules.CrudModule', [app, permissions]);

        var totalLoadHolder;

        this.setTotalLoadHolder = function(holder) {
            totalLoadHolder = holder;
        };

        this.getTotalLoadHolder = function() {
            return totalLoadHolder;
        };

        this.getModuleName = function() {
            return 'EstimatedLoadAppliance';
        };
    },

    methods: {

        initModule: function() {
            this.setNotAutoLoadingRelationFields(['estimatedLoad', 'applianceType', 'appliance']);
            this.super('initModule');
        },

        initModel: function() {
            this.setModel(this.getApp().getModel('Model.EstimatedLoadAppliance'));
        },

        initController: function() {
            var crudController = this.getApp().getController('Controllers.Module.EstimatedLoadAppliance', [this, this.getModel()]);
            this.setController(crudController);
        },
        
        readRecords: function(pagingParameters, searchParameters) {},

        readAllAppliances: function() {

            var options = {
                scope: this,
                showLoader: false,
                parameters: {
                    estimatedLoad: this.getModel().getCurrentEstimatedLoad().id
                }
            };

            this.blockMainViewComponent();
            this.getModel().readAll(options);
        },

        applyPagedData: function(pagedData) {
            this.insertPagedDataIntoMainViewComponent(pagedData);
        },

        setControls: function() {

            this.super('setControls');

            var self = this;

            this.setTotalLoadHolder(this.getModuleElement().find('#total-load-holder'));

            var applianceTypeFieldModelData = this.getModel().getFieldByName('applianceType');
            var applianceFieldModelData = this.getModel().getFieldByName('appliance');

            var estimatedLoadAppliancesGrid = this.getModuleElement().find("#estimatedLoadAppliances-EstimatedLoadAppliance-grid").grid({

                autoLoad: false,
                cellEdit: true,
                localSort: true,
                cellEditType: 'callback',
                model: [
                    {
                        header: 'ID',
                        id: 'id',
                        dataMap: 'id'
                    },
                    {
                        header: 'Appliance Type',
                        id: 'applianceType',
                        dataMap: 'applianceType',
                        editable: false,
                        sortPropertyName: 'name',
                        formatter: function(cellValue) {
                            if(cellValue && self.isObject(cellValue)) {
                                return cellValue[applianceTypeFieldModelData.labelPropertyName];
                            }
                        }
                    },
                    {
                        header: 'Appliance',
                        id: 'appliance',
                        dataMap: 'appliance',
                        editable: false,
                        sortPropertyName: 'make',
                        formatter: function(cellValue) {
                            if(cellValue && self.isObject(cellValue)) {
                                return cellValue[applianceFieldModelData.labelPropertyName];
                            }
                        }
                    },
                    {
                        header: 'Count',
                        id: 'applianceCount',
                        dataMap: 'applianceCount',
                        editable: true
                    },
                    {
                        header: 'Total Consumption (Watts)',
                        id: 'totalConsumption',
                        dataMap: 'totalConsumption',
                        editable: true,
                        formatter: function(cellValue) {
                            if(cellValue) {
                                return Number(cellValue).formatNumberThousands();
                            }
                        }
                    }
                ]
            });

            this.setMainViewComponent(estimatedLoadAppliancesGrid);
        },

        updateCurrentTotalLoad: function(totalLoad) {

            this.getModel().setCurrentTotalLoad(totalLoad);

            this.getTotalLoadHolder().find('.total-load-value').text(Number(totalLoad).formatNumberThousands());
        },

        calculateCurrentTotalLoad: function() {

            var applianceData = this.getMainViewComponentData();

            var totalLoad = 0;

            for(var i = 0, len = applianceData.length; i < len; i++) {
                totalLoad = totalLoad + applianceData[i].totalConsumption;
            }

            return totalLoad;            
        }
    }
});