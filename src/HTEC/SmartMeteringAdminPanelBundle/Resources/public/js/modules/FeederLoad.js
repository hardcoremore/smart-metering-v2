HCM.define({

    name: 'Modules.FeederLoad',

    extendFrom: 'Base.Module',

    construct: function(app, permissions) {

        this.callConstructor('Base.Module', [app, permissions]);

        var feederLoadModel;
        var feederLoadController;
        var feederLoadScheduler;
        var schedulerHelper;

        var powerSchedulesDateRangeHolder;
        var currentStartDateForDateRangeSearch;

        var previousDayButton;
        var nextDayButton;
        var feederLoadFormModule;

        this.setFeederLoadModel = function(model) {
            feederLoadModel = model;
        };

        this.getFeederLoadModel = function() {
            return feederLoadModel;
        };

        this.setFeederLoadController = function(controller) {
            feederLoadController = controller;
        };

        this.getFeederLoadController = function() {
            return feederLoadController;
        };

        this.setFeederLoadScheduler = function(grid) {
            feederLoadScheduler = grid;
        };

        this.getFeederLoadScheduler = function() {
            return feederLoadScheduler;
        };

        this.setSchedulerHelper = function(helper) {
            schedulerHelper = helper;
        };

        this.getSchedulerHelper = function() {
            return schedulerHelper;
        };

        this.setFeederLoadDateRangeHolder = function(holder) {
            powerSchedulesDateRangeHolder = holder;
        };

        this.getFeederLoadDateRangeHolder = function() {
            return powerSchedulesDateRangeHolder;
        };

        this.setCurrentStartDateForDateRangeSearch = function(date){
            currentStartDateForDateRangeSearch = date;
        };

        this.getCurrentStartDateForDateRangeSearch = function() {
            return currentStartDateForDateRangeSearch;
        };

        this.setPreviousDayButton = function(button) {
            previousDayButton = button;
        };

        this.getPreviousDayButton = function() {
            return previousDayButton;
        };

        this.setNextDayButton = function(button) {
            nextDayButton = button;
        };

        this.getNextDayButton = function() {
            return nextDayButton;
        };

        this.setFeederLoadFormModule = function(module)  {
            feederLoadFormModule = module;
        };

        this.getFeederLoadFormModule = function() {
            return feederLoadFormModule;
        };

        this.getModuleName = function() {
            return 'FeederLoad';
        };
    },

    methods: {

        initModule: function() {

            this.setSchedulerHelper(this.getInstance('Helpers.FeederLoadScheduler', [this.getApp()]));

            this.initModel();
            this.initController();

            this.super('initModule');
        },

        initModel: function() {
            this.setFeederLoadModel(this.getApp().getModel('Model.FeederLoad'));
            this.getSchedulerHelper().setModel(this.getFeederLoadModel());
        },

        initController: function() {
            var controller = this.getApp().getController('Controllers.Module.FeederLoad', [this, this.getFeederLoadModel()]);
            this.setFeederLoadController(controller);
        },

        startModule: function() {
            this.super('startModule');
            this.getApp().startController(this.getFeederLoadController());
        },

        stopModule: function() {
            this.super('stopModule');
        },

        setControls: function() {

            this.super('setControls');

            var rowId = this.getSchedulerHelper().getDayNameFromDate(moment());

            var newModel = {};
                newModel.id = rowId;
                newModel.title = rowId.toString().firstCharacterToUpperCase();

            var feederLoadScheduler = this.getModuleElement().find("#feederLoads-FeederLoad-scheduler").scheduler({
                allowIntersectingEvents: true,
                createEventTooltipCallback: this.getSchedulerHelper().createTooltipForSchedulerEvent.bind(this.getSchedulerHelper()),
                rowsModel: [newModel]
            });

            this.setFeederLoadScheduler(feederLoadScheduler);

            this.setFeederLoadDateRangeHolder(this.getModuleElement().find('.date-range-display'));

            this.setPreviousDayButton(this.getModuleElement().find('.previous-day-button'));
            this.setNextDayButton(this.getModuleElement().find('.next-day-button'));
        },

        getDateForFeederLoad: function(isSlider, isReadingPrevious) {

            var currentStartDate = this.getCurrentStartDateForDateRangeSearch();

            if(currentStartDate && isSlider) {

                if(isReadingPrevious) {
                    currentStartDate.subtract(1, 'days');
                }
                else {
                    currentStartDate.add(1, 'days');
                }
            }
            else {
                currentStartDate = moment();
            }

            return currentStartDate;
        },

        readFeederLoadForDateRange: function(feeder, date, searchParameters) {

            this.getFeederLoadModel().readAll({

                parameters: $.extend({}, {
                    powerTransformerFeeder: feeder,
                    date: date
                }, searchParameters),

                scope: this
            });
        },

        previewFeederLoad: function(feederLoad) {

            this.getSchedulerHelper().addRecordToScheduler(this.getFeederLoadScheduler(), feederLoad);
        },

        setCurrentDateLabel: function(date) {
            this.getFeederLoadDateRangeHolder().find('.current-date').text(date.format('MMMM Do, dddd YYYY'));
        }
    }
});