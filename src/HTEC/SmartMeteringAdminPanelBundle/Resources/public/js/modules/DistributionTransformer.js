HCM.define({

    name: 'Modules.DistributionTransformer',

    extendFrom: 'Modules.CrudModule',

    construct: function(app, permissions) {

        this.callConstructor('Modules.CrudModule', [app, permissions]);

        this.getFormModuleClassname = function() {
            return 'Modules.Form.DistributionTransformer';
        };

        this.getModuleName = function() {
            return 'DistributionTransformer';
        };
    },

    methods: {

        initModule: function() {

            this.setNotAutoLoadingRelationFields(['businessDistrict', 'injectionSubstation', 'powerTransformer', 'inputFeeder', 'outputFeeders']);
            this.super('initModule');
        },

        initModel: function() {
            this.setModel(this.getApp().getModel('Model.DistributionTransformer'));
        },

        setControls: function() {

            this.super('setControls');

            var businessDistrictFieldModelData = this.getModel().getFieldByName('businessDistrict');
            var injectionSubstationFieldModelData = this.getModel().getFieldByName('injectionSubstation');
            var powerTransformerFieldModelData = this.getModel().getFieldByName('powerTransformer');
            var inputFeederFieldModelData = this.getModel().getFieldByName('inputFeeder');
            var outputFeedersFieldModelData = this.getModel().getFieldByName('outputFeeders');

            var self = this;

            var distributionTransformersGrid = this.getModuleElement().find("#distributionTransformers-DistributionTransformer-grid").grid({

                autoLoad: false,
                cellEdit: true,
                cellEditType: 'callback',
                model: [
                    {
                        header: 'ID',
                        id: 'id',
                        dataMap: 'id'
                    },
                    {
                        header: 'Name',
                        id: 'name',
                        dataMap: 'name',
                        editable: true
                    },
                    {
                        header: 'Status',
                        id: 'status',
                        dataMap: 'status',
                        editable: true,
                        editOptions: {
                            type: 'select',
                            valueProperty: 'value',
                            labelProperty: 'label',
                            availableOptions:[
                                {value: 'live', label: 'Live'},
                                {value: 'offline', label: 'Offline'},
                                {value: 'repairing', label: 'Repairing'}
                            ],
                            areAvailableOptionsStatic: true
                        },
                        formatter: function(cellValue) {
                            if(cellValue) {
                                return cellValue.charAt(0).toUpperCase() + cellValue.slice(1);
                            }
                        }
                    },
                    {
                        header: 'Type',
                        id: 'type',
                        dataMap: 'type',
                        editable: true,
                        editOptions: {
                            type: 'select',
                            valueProperty: 'value',
                            labelProperty: 'label',
                            availableOptions:[
                                {value: 'PUBLIC', label: 'Public'},
                                {value: 'PRIVATE', label: 'Private'}
                            ],
                            areAvailableOptionsStatic: true
                        },
                        formatter: function(cellValue) {
                            if(cellValue) {
                                return cellValue.charAt(0) + cellValue.slice(1).toLowerCase();
                            }
                        }
                    },
                    {
                        header: 'Business District',
                        id: 'businessDistrict',
                        dataMap: 'businessDistrict',
                        editable: false,
                        formatter: function(cellValue) {
                            if(cellValue && self.isObject(cellValue)) {
                                return cellValue[businessDistrictFieldModelData.labelPropertyName];
                            }
                        }
                    },
                    {
                        header: 'Injection Substation',
                        id: 'injectionSubstation',
                        dataMap: 'injectionSubstation',
                        editable: false,
                        formatter: function(cellValue) {
                            if(cellValue && self.isObject(cellValue)) {
                                return cellValue[injectionSubstationFieldModelData.labelPropertyName];
                            }
                        }
                    },
                    {
                        header: 'Power Transformer',
                        id: 'powerTransformer',
                        dataMap: 'powerTransformer',
                        editable: false,
                        formatter: function(cellValue) {
                            if(cellValue && self.isObject(cellValue)) {
                                return cellValue[powerTransformerFieldModelData.labelPropertyName];
                            }
                        }
                    },
                    {
                        header: 'Code',
                        id: 'code',
                        dataMap: 'code',
                        editable: true
                    },
                    {
                        header: 'Power Transformer Feeder',
                        id: 'inputFeeder',
                        dataMap: 'inputFeeder',
                        editable: false,
                        formatter: function(cellValue) {
                            if(cellValue && self.isObject(cellValue)) {
                                return cellValue[inputFeederFieldModelData.labelPropertyName];
                            }
                        }
                    },
                    {
                        header: 'Output Feeders',
                        id: 'outputFeeders',
                        dataMap: 'outputFeeders',
                        editable: true,
                        formatter: function(cellValue) {

                            if(cellValue) {

                                var label = '';

                                for(var i = 0, len = cellValue.length; i < len; i++) {
                                    label += cellValue[i].name + ', ';
                                }

                                return label.substring(0, label.length - 2);
                            }

                            return 'N/A';
                        }
                    }
                ]
            });
    
            var powerTransformerGridCellEditOptions = this.getAutoCompleteGridCellEditOptions(
                    distributionTransformersGrid,
                    powerTransformerFieldModelData
            );

            distributionTransformersGrid.grid(
                'setColumnEditOptions',
                'powerTransformer',
                powerTransformerGridCellEditOptions
            );


            var outputFeedersGridCellEditOptions = this.getMultiAutoCompleteGridCellEditOptions(
                distributionTransformersGrid,
                outputFeedersFieldModelData,
                this.getModel(),
                {type: 'low-voltage'}
            );

            distributionTransformersGrid.grid(
                'setColumnEditOptions',
                'outputFeeders',
                outputFeedersGridCellEditOptions
            );

            this.setMainViewComponent(distributionTransformersGrid);
        }
    }
});