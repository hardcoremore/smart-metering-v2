HCM.define({

    name: 'Modules.ApplianceType',

    extendFrom: 'Modules.CrudModule',

    construct: function(app, permissions) {

        this.callConstructor('Modules.CrudModule', [app, permissions]);

        this.getFormModuleClassname = function() {
            return 'Modules.Form.ApplianceTypeForm';
        };

        this.getModuleName = function() {
            return 'ApplianceType';
        };
    },

    methods: {

        initModel: function() {
            this.setModel(this.getApp().getModel('Model.ApplianceType'));
        },

        setControls: function() {

            this.super('setControls');

            var self = this;
            var customerCategoriesFieldModelData = this.getModel().getFieldByName('customerCategories');

            var applianceTypesGrid = $("#applianceTypes-ApplianceType-grid").grid({

                autoLoad: false,
                cellEdit: true,
                cellEditType: 'callback',
                model: [
                    {
                        header: 'ID',
                        id: 'id',
                        dataMap: 'id'
                    },
                    {
                        header: 'Name',
                        id: 'name',
                        dataMap: 'name',
                        editable: true
                    },
                    {
                        header: 'Customer Categories',
                        id: 'customerCategories',
                        dataMap: 'customerCategories',
                        editable: true,
                        formatter: function(cellValue) {

                            if(cellValue) {

                                var label = '';

                                for(var i = 0, len = cellValue.length; i < len; i++) {
                                    label += cellValue[i].name + ',';
                                }

                                return label.substring(0, label.length - 1);
                            }

                            return 'N/A';
                        }
                    }
                ]
            });

            var customerCategoriesGridCellEditOptions = this.getCheckBoxGroupGridCellEditOptions(
                applianceTypesGrid,
                customerCategoriesFieldModelData,
                this.getModel()
            );

            customerCategoriesGridCellEditOptions['checkIfCellDataIsChanged'] = this.getModel().isArrayCollectionChanged.bind(this.getModel());

            applianceTypesGrid.grid(
                'setColumnEditOptions',
                'customerCategories',
                customerCategoriesGridCellEditOptions
            );

            this.setMainViewComponent(applianceTypesGrid);
        }
    }
});
