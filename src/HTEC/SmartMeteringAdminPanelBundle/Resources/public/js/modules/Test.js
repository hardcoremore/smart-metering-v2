HCM.define({

    name: 'Modules.Test',

    extendFrom: 'Base.Module',

    construct: function(app, permissions) {

        this.callConstructor('Base.Module', [app, permissions]);
    },

    methods: {
        getModuleName: function() {
            return 'Test';
        }
    }
});