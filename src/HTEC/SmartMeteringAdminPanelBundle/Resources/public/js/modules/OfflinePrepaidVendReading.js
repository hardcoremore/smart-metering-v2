HCM.define({

    name: 'Modules.OfflinePrepaidVendReading',

    extendFrom: 'Modules.CrudModule',

    construct: function(app, permissions) {

        this.callConstructor('Modules.CrudModule', [app, permissions]);

        this.getModuleName = function() {
            return 'OfflinePrepaidVendReading';
        };
    },

    methods: {

        initModel: function() {
            this.setModel(this.getApp().getModel('Model.OfflinePrepaidVendReading'));
        },

        setControls: function() {

            this.super('setControls');

            var self = this;

            var offlinePrepaidVendReadingGrid = this.getModuleElement().find("#offlinePrepaidVendReadings-OfflinePrepaidVendReading-grid").grid({

                autoLoad: false,
                model: [
                    {
                        header: 'ID',
                        id: 'id',
                        dataMap: 'id'
                    },
                    {
                        header: 'Customer',
                        id: 'customer',
                        dataMap: 'customer',
                 
                    },
                    {
                        header: 'Account Number',
                        id: 'accountNumber',
                        dataMap: 'customer',
                 
                    },
                    {
                        header: 'Business District',
                        id: 'businessDistrict',
                        dataMap: 'customer',
                       
                    }
                ]
            });
    
            this.setMainViewComponent(offlinePrepaidVendReadingGrid);
        }
    }
});