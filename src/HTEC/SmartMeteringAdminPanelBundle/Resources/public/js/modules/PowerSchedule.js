HCM.define({

    name: 'Modules.PowerSchedule',

    extendFrom: 'Modules.CrudModule',

    construct: function(app, permissions) {

        this.callConstructor('Modules.CrudModule', [app, permissions]);

        var powerScheduleApprovePopup;
        var powerSchedulePreviewScheduler;
        var powerSchedulesWeeklyPreviewScheduler;

        var powerSchedulesViewTab;

        var currentStartDateForDateRangeSearch;

        var powerScheduleFiltersFormModule;

        var previousWeekButton;
        var nextWeekButton;

        var powerSchedulesDateRangeHolder;

        var schedulerHelper;

        this.setPowerSchedulesViewTab = function(psvt) {
            powerSchedulesViewTab = psvt;
        };

        this.getPowerSchedulesViewTab = function() {
            return powerSchedulesViewTab;
        };

        this.setPowerScheduleApprovePopup = function(psap) {
            powerScheduleApprovePopup = psap;
        };

        this.getPowerScheduleApprovePopup = function() {
            return powerScheduleApprovePopup;
        };

        this.setPowerSchedulePreviewScheduler = function(psps) {
            powerSchedulePreviewScheduler = psps;
        };

        this.getPowerSchedulePreviewScheduler = function() {
            return powerSchedulePreviewScheduler;
        };

        this.setPowerSchedulesWeeklyPreviewScheduler = function(pswps) {
            powerSchedulesWeeklyPreviewScheduler = pswps;
        };

        this.getPowerSchedulesWeeklyPreviewScheduler = function() {
            return powerSchedulesWeeklyPreviewScheduler;
        };

        this.setPowerScheduleFiltersFormModule = function(module) {
            powerScheduleFiltersFormModule = module;
        };

        this.getPowerScheduleFiltersFormModule = function(filterHolder) {
            return powerScheduleFiltersFormModule;
        };

        this.setPreviousWeekButton = function(button) {
            previousWeekButton = button;
        };

        this.getPreviousWeekButton = function() {
            return previousWeekButton;
        };

        this.setNextWeekButton = function(button) {
            nextWeekButton = button;
        };

        this.getNextWeekButton = function() {
            return nextWeekButton;
        };

        this.setPowerSchedulesDateRangeHolder = function(holder) {
            powerSchedulesDateRangeHolder = holder;
        };

        this.getPowerSchedulesDateRangeHolder = function() {
            return powerSchedulesDateRangeHolder;
        };

        this.setCurrentStartDateForDateRangeSearch = function(date){
            currentStartDateForDateRangeSearch = date;
        };

        this.getCurrentStartDateForDateRangeSearch = function() {
            return currentStartDateForDateRangeSearch;
        };

        this.setSchedulerHelper = function(helper) {
            schedulerHelper = helper;
        };

        this.getSchedulerHelper = function() {
            return schedulerHelper;
        };

        this.getFormModuleClassname = function() {
            return 'Modules.Form.PowerScheduleForm';
        };

        this.getModuleName = function() {
            return 'PowerSchedule';
        };
    },

    methods: {

        initModule: function() {

            this.setNotAutoLoadingRelationFields([
                'businessDistrict',
                'powerSource',
                'injectionSubstation',
                'powerTransformer',
                'powerTransformerFeeder',
                'distributionTransformer',
                'distributionTransformerFeeder'
            ]);

            this.setSchedulerHelper(this.getInstance('Helpers.PowerScheduleScheduler', [this.getApp()]));

            this.super('initModule');
        },

        initModel: function() {
            this.setModel(this.getApp().getModel('Model.PowerSchedule'));
            this.getSchedulerHelper().setModel(this.getModel());
        },

        initController: function() {
            var crudController = this.getApp().getController('Controllers.Module.PowerSchedule', [this, this.getModel()]);
            this.setController(crudController);
        },

        resolveModulePermissions: function(permissions) {

            this.super('resolveModulePermissions', [permissions]);

            if(permissions && this.isArray(permissions.actions)) {

                if(permissions.actions.indexOf('approve') === -1) {
                    this.getModuleElement().find('[data-role="approve-record"]').remove();
                }
            }
        },

        startModule: function() {

            this.super('startModule');

            if(this.getPowerScheduleFiltersFormModule()) {
                this.getPowerScheduleFiltersFormModule().startModule();
            }
        },

        stopModule: function() {

            this.super('stopModule');

            if(this.getPowerScheduleFiltersFormModule()) {
                this.getPowerScheduleFiltersFormModule().stopModule();
            }
        },

        openCreateForm: function() {

            this.super('openCreateForm');

            if(this.getPowerScheduleFiltersFormModule()) {
                this.getPowerScheduleFiltersFormModule().stopModule();
            }
        },

        openEditForm: function(formData) {

            this.super('openEditForm', [formData]);

            if(this.getPowerScheduleFiltersFormModule()) {
                this.getPowerScheduleFiltersFormModule().stopModule();
            }
        },

        closeFormModule: function() {

            this.super('closeFormModule');

            if(this.getPowerScheduleFiltersFormModule()) {
                this.getPowerScheduleFiltersFormModule().startModule();
            }
        },

        setControls: function() {

            this.super('setControls');

            var powerSchedulesViewTab = this.getModuleElement().find("#power-schedule-view-tab").tab({
                contentHolder: "#power-schedules-view-tab-content-holder",
                defaultTab: 0,
            });


            this.setPowerSchedulesViewTab(powerSchedulesViewTab);

            var powerSourceFieldModelData = this.getModel().getFieldByName('powerSource');
            var businessDistrictFieldModelData = this.getModel().getFieldByName('businessDistrict');
            var injectionSubstationFieldModelData = this.getModel().getFieldByName('injectionSubstation');
            var powerTransformerFieldModelData = this.getModel().getFieldByName('powerTransformer');
            var powerTransformerFeederFieldModelData = this.getModel().getFieldByName('powerTransformerFeeder');
            var distributionTransformerFieldModelData = this.getModel().getFieldByName('distributionTransformer');
            var distributionTransformerFeederFieldModelData = this.getModel().getFieldByName('distributionTransformerFeeder');

            var self = this;

            var powerSchedulesGrid = this.getModuleElement().find("#powerSchedules-PowerSchedule-grid").grid({

                autoLoad: false,
                cellEditType: 'callback',
                model: [
                    {
                        header: 'ID',
                        id: 'id',
                        dataMap: 'id'
                    },
                    {
                        header: 'Power Type',
                        id: 'powerType',
                        dataMap: 'powerType',
                        editable: true,
                        editOptions: {
                            type: 'select',
                            valueProperty: 'value',
                            labelProperty: 'label',
                            availableOptions:[
                                {value: 'national', label: 'National'},
                                {value: 'captive', label: 'Captive'},
                                {value: 'embedded', label: 'Embedded'},
                            ],
                            areAvailableOptionsStatic: true
                        },
                        formatter: function(cellValue) {
                            if(cellValue) {
                                return cellValue.charAt(0).toUpperCase() + cellValue.slice(1);
                            }
                        }
                    },
                    {
                        header: 'Power Source',
                        id: 'powerSource',
                        dataMap: 'powerSource',
                        formatter: function(cellValue) {
                            if(cellValue && self.isObject(cellValue)) {
                                return cellValue[powerSourceFieldModelData.labelPropertyName];
                            }
                        }
                    },
                    {
                        header: 'Business District',
                        id: 'businessDistrict',
                        dataMap: 'businessDistrict',
                        formatter: function(cellValue) {
                            if(cellValue && self.isObject(cellValue)) {
                                return cellValue[businessDistrictFieldModelData.labelPropertyName];
                            }
                        }
                    },
                    {
                        header: 'Injection Substation',
                        id: 'injectionSubstation',
                        dataMap: 'injectionSubstation',
                        formatter: function(cellValue) {
                            if(cellValue && self.isObject(cellValue)) {
                                return cellValue[injectionSubstationFieldModelData.labelPropertyName];
                            }
                        }
                    },
                    {
                        header: 'Power Transformer',
                        id: 'powerTransformer',
                        dataMap: 'powerTransformer',
                        formatter: function(cellValue) {
                            if(cellValue && self.isObject(cellValue)) {
                                return cellValue[powerTransformerFieldModelData.labelPropertyName];
                            }
                        }
                    },
                    {
                        header: 'Power Transformer Feeder',
                        id: 'powerTransformerFeeder',
                        dataMap: 'powerTransformerFeeder',
                        formatter: function(cellValue) {
                            if(cellValue && self.isObject(cellValue)) {
                                return cellValue[powerTransformerFeederFieldModelData.labelPropertyName];
                            }
                        }
                    },
                    {
                        header: 'Distribution Transformer',
                        id: 'distributionTransformer',
                        dataMap: 'distributionTransformer',
                        hidden: true,
                        formatter: function(cellValue) {
                            if(cellValue && self.isObject(cellValue)) {
                                return cellValue[distributionTransformerFieldModelData.labelPropertyName];
                            }
                        }
                    },
                    {
                        header: 'Distribution Transformer Feeder',
                        id: 'distributionTransformerFeeder',
                        dataMap: 'distributionTransformerFeeder',
                        hidden: true,
                        formatter: function(cellValue) {
                            if(cellValue && self.isObject(cellValue)) {
                                return cellValue[distributionTransformerFeederFieldModelData.labelPropertyName];
                            }
                        }
                    },
                    {
                        header: 'Tariff',
                        id: 'tariff',
                        dataMap: 'tariff',
                        hidden: true,
                        formatter: function(cellValue) {
                            if(cellValue && self.isObject(cellValue)) {
                                return cellValue.name;
                            }
                        }
                    },
                    {
                        header: 'Status',
                        id: 'status',
                        dataMap: 'status',
                        editable: false,
                        formatter: function(cellValue) {
                            if(cellValue === 'approved') {
                                return 'Approved';
                            }
                            else {
                                return 'Waiting for approval';
                            }
                        }
                    },
                    {
                        header: 'Date',
                        id: 'date',
                        dataMap: 'date',
                        editable: false,
                        formatter: function(cellValue) {
                            return moment(cellValue).format('YYYY-MM-DD');
                        }
                    },
                    {
                        header: 'Start Time',
                        id: 'startTime',
                        dataMap: 'startTime',
                        editable: false,
                        formatter: function(cellValue) {
                            return moment(cellValue).format('HH:mm');
                        }
                    },
                    {
                        header: 'End Time',
                        id: 'endTime',
                        dataMap: 'endTime',
                        editable: false,
                        formatter: function(cellValue) {
                            return moment(cellValue).format('HH:mm');
                        }
                    },
                    {
                        header: 'Repeat',
                        id: 'repeatType',
                        dataMap: 'repeatType',
                        editable: false
                    },
                    {
                        header: 'Repeat End',
                        id: 'repeatEnd',
                        dataMap: 'repeatEnd',
                        editable: false,
                        formatter: function(cellValue) {
                            if(cellValue) {
                                return moment(cellValue).format('YYYY-MM-DD');
                            }
                        }
                    }
                ]
            });
    
            this.setMainViewComponent(powerSchedulesGrid);


            var powerSchedulePreviewScheduler = $("#powerSchedulePreview-PowerSchedule-scheduler").scheduler({
                rowsModel:[
                    {
                        title: 'Monday',
                        id: 'monday'
                    }
                ],

                createEventTooltipCallback: this.getSchedulerHelper().createTooltipForSchedulerEvent.bind(this.getSchedulerHelper())
            });

            this.setPowerSchedulePreviewScheduler(powerSchedulePreviewScheduler);

            var powerScheduleApprovePopup = this.getModuleElement().find("#approve-power-schedule-popup").popup({
                    compileTemplateFunction: this.getApp().compileTemplate,
                    compileTemplateData: {
                        title: 'Are you sure you want to approve this power schedule?',
                        okLabel: 'Yes',
                        cancelLabel: 'No'
                    }
                });

            this.setPowerScheduleApprovePopup(powerScheduleApprovePopup);

            var powerSchedulesWeeklyPreviewScheduler = this.getModuleElement().find('#powerSchedulesWeeklyPreview-PowerSchedule-scheduler').scheduler({
                allowIntersectingEvents: true,
                createEventTooltipCallback: this.getSchedulerHelper().createTooltipForSchedulerEvent.bind(this.getSchedulerHelper())
            });

            this.setPowerSchedulesWeeklyPreviewScheduler(powerSchedulesWeeklyPreviewScheduler);

            this.setPreviousWeekButton(this.getModuleElement().find('.previous-week-button'));
            this.setNextWeekButton(this.getModuleElement().find('.next-week-button'));

            this.setPowerSchedulesDateRangeHolder(this.getModuleElement().find('.date-range-display'));
        },

        approvePowerSchedule: function(powerSchedule) {

            this.getModel().patch(powerSchedule.id, {status: 'approved'}, {
                scope: {
                    module: this,
                    fieldModelData: this.getModel().getFieldByName('status')
                }
            });
        },

        readPowerSchedulesForDateRange: function(start, end, searchParameters) {

            if(searchParameters && searchParameters.hasOwnProperty('date')) {
                this.getModel().readAll({
                    parameters: searchParameters,
                    scope: this.getPowerScheduleFiltersFormModule()
                });
            }
            else {

                this.getModel().readAll({

                    parameters: $.extend({}, {
                        start: start,
                        end: end
                    }, searchParameters),

                    scope: this.getPowerScheduleFiltersFormModule()
                });
            }
        },

        previewPowerScheduleEvent: function(powerSchedule) {

            var rowId = this.getSchedulerHelper().getSchedulerRowIdFromRecord(powerSchedule);

            var newModel = {};
                newModel.id = rowId;
                newModel.title = rowId.toString().firstCharacterToUpperCase();

            this.getPowerSchedulePreviewScheduler().scheduler('reinitializeWithNewModel', [newModel]);

            this.getSchedulerHelper().addRecordToScheduler(this.getPowerSchedulePreviewScheduler(), powerSchedule);
        },

        getStartDateForLoadingPowerSchedules: function(isReadingPrevious, isFilters) {

            var currentStartDate = this.getCurrentStartDateForDateRangeSearch();

            if(currentStartDate) {

                if(isFilters !== true) {

                    if(isReadingPrevious === true) {
                        currentStartDate.subtract(7, 'days');
                    }
                    else {
                        currentStartDate.add(7, 'days');
                    }    
                }
            }
            else {

                currentStartDate = moment();
                currentStartDate.subtract(currentStartDate.day() - 1, 'days');
            }

            return currentStartDate;
        },

        getEndDateForLoadingPowerSchedules: function(startDate) {
            return startDate.clone().add(6, 'days');
        }
    }
});
