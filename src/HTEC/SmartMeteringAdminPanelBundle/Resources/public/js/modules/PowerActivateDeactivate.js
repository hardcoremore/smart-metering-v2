HCM.define({

    name: 'Modules.PowerActivateDeactivate',

    extendFrom: 'Modules.CrudModule',

    construct: function(app, permissions) {

        this.callConstructor('Modules.CrudModule', [app, permissions]);

        this.getFormModuleClassname = function() {
            return 'Modules.Form.PowerActivateDeactivateForm';
        };

        this.getModuleName = function() {
            return 'PowerActivateDeactivate';
        };
    },

    methods: {

        initModule: function() {

            this.setNotAutoLoadingRelationFields([
                'businessDistrict',
                'injectionSubstation',
                'powerTransformer',
                'powerTransformerFeeder',
                'distributionTransformer',
                'distributionTransformerFeeder'
            ]);

            this.super('initModule');
        },

        initModel: function() {
            this.setModel(this.getApp().getModel('Model.PowerActivateDeactivate'));
        },

        setControls: function() {

            this.super('setControls');

            var businessDistrictFieldModelData = this.getModel().getFieldByName('businessDistrict');
            var injectionSubstationFieldModelData = this.getModel().getFieldByName('injectionSubstation');
            var powerTransformerFieldModelData = this.getModel().getFieldByName('powerTransformer');
            var powerTransformerFeederFieldModelData = this.getModel().getFieldByName('powerTransformerFeeder');
            var distributionTransformerFieldModelData = this.getModel().getFieldByName('distributionTransformer');
            var distributionTransformerFeederFieldModelData = this.getModel().getFieldByName('distributionTransformerFeeder');

            var self = this;

            var powerActivateDeactivateGrid = this.getModuleElement().find("#powerActivatesDeactivates-PowerActivateDeactivate-grid").grid({

                autoLoad: false,
                cellEditType: 'callback',
                model: [
                    {
                        header: 'ID',
                        id: 'id',
                        dataMap: 'id'
                    },
                    {
                        header: 'Business District',
                        id: 'businessDistrict',
                        dataMap: 'businessDistrict',
                        formatter: function(cellValue) {
                            if(cellValue && self.isObject(cellValue)) {
                                return cellValue[businessDistrictFieldModelData.labelPropertyName];
                            }
                        }
                    },
                    {
                        header: 'Injection Substation',
                        id: 'injectionSubstation',
                        dataMap: 'injectionSubstation',
                        formatter: function(cellValue) {
                            if(cellValue && self.isObject(cellValue)) {
                                return cellValue[injectionSubstationFieldModelData.labelPropertyName];
                            }
                        }
                    },
                    {
                        header: 'Power Transformer',
                        id: 'powerTransformer',
                        dataMap: 'powerTransformer',
                        formatter: function(cellValue) {
                            if(cellValue && self.isObject(cellValue)) {
                                return cellValue[powerTransformerFieldModelData.labelPropertyName];
                            }
                        }
                    },
                    {
                        header: 'Power Transformer Feeder',
                        id: 'powerTransformerFeeder',
                        dataMap: 'powerTransformerFeeder',
                        formatter: function(cellValue) {
                            if(cellValue && self.isObject(cellValue)) {
                                return cellValue[powerTransformerFeederFieldModelData.labelPropertyName];
                            }
                        }
                    },
                    {
                        header: 'Status',
                        id: 'type',
                        dataMap: 'type',
                        editable: false,
                        formatter: function(cellValue) {
                            if(cellValue === 'power-activate') {
                                return 'Power Activated';
                            }
                            else {
                                return 'Power Deactivated';
                            }
                        }
                    },
                    {
                        header: 'Date',
                        id: 'date',
                        dataMap: 'date',
                        editable: false,
                        formatter: function(cellValue) {
                            return moment(cellValue).format('YYYY-MM-DD');
                        }
                    },
                    {
                        header: 'Time',
                        id: 'time',
                        dataMap: 'time',
                        editable: false,
                        formatter: function(cellValue) {
                            return moment(cellValue).format('HH:mm');
                        }
                    }
                ]
            });
    
            this.setMainViewComponent(powerActivateDeactivateGrid);
        }
    }
});