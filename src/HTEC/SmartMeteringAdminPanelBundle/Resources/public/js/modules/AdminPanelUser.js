HCM.define({

    name: 'Modules.AdminPanelUser',

    extendFrom: 'Modules.CrudModule',

    construct: function(app, permissions) {

        this.callConstructor('Modules.CrudModule', [app, permissions]);

        this.getFormModuleClassname = function() {
            return 'Modules.Form.AdminPanelUserForm';
        };

        this.getModuleName = function() {
            return 'AdminPanelUser';
        };
    },

    methods: {

        initModule: function() {
            this.setModel(this.getApp().getUserModel());
            this.super('initModule');
        },

        setControls: function() {

            this.super('setControls');

            var adminPanelUsersGrid = $("#users-AdminPanelUser-grid").grid({

                autoLoad: false,
                cellEdit: true,
                cellEditType: 'callback',
                model: [
                    {
                        header: 'ID',
                        id: 'id',
                        dataMap: 'id'
                    },
                    {
                        header: 'First Name',
                        id: 'firstName',
                        dataMap: 'firstName',
                        editable: true
                    },
                    {
                        header: 'Last Name',
                        id: 'lastName',
                        dataMap: 'lastName',
                        editable: true,
                    },
                    {
                        header: 'Email',
                        id: 'email',
                        dataMap: 'email',
                        editable: true
                    },
                    {
                        header: 'Username',
                        id: 'username',
                        dataMap: 'username',
                        editable: true
                    },
                    {
                        header: 'Status',
                        id: 'status',
                        dataMap: 'status',
                        editable: true,
                        editOptions: {
                            type: 'select',
                            valueProperty: 'value',
                            labelProperty: 'label',
                            availableOptions:[
                                {value: 'active', label: 'Active'},
                                {value: 'inactive', label: 'Inactive'}
                            ],
                            areAvailableOptionsStatic: true
                        },
                        formatter: function(cellValue) {

                            if(cellValue) {
                                return cellValue.charAt(0).toUpperCase() + cellValue.slice(1);
                            }

                            return '';
                        }
                    },
                    {
                        header: 'Roles',
                        id: 'roles',
                        dataMap: 'roles',
                        editable: true,
                        formatter: function(cellValue) {

                            if(cellValue) {

                                var label = '';

                                for(var i = 0, len = cellValue.length; i < len; i++) {
                                    label += cellValue[i].name + ', <br />';
                                }

                                return label.substring(0, label.length - 8);
                            }

                            return 'N/A';
                        }
                    },
	                {
		                header: 'Created Datetime',
		                id: 'createdDatetime',
		                dataMap: 'createdDatetime',
		                formatter: function(cellValue) {
			                return moment(cellValue).format('YYYY-MM-DD hh:mm:ss');
		                }
	                }
                ]
            });
            
            var rolesFieldModelData = this.getModel().getFieldByName('roles');

            var rolesGridCellEditOptions = this.getCheckBoxGroupGridCellEditOptions(
                    adminPanelUsersGrid,
                    rolesFieldModelData,
                    this.getModel()
            );

            rolesGridCellEditOptions['checkIfCellDataIsChanged'] = this.getModel().isArrayCollectionChanged.bind(this.getModel());

            adminPanelUsersGrid.grid(
                'setColumnEditOptions',
                'roles',
                rolesGridCellEditOptions
            );

            this.setMainViewComponent(adminPanelUsersGrid);
        }
    }
});
