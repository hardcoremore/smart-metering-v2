HCM.define({

    name: 'Modules.City',

    extendFrom: 'Modules.CrudModule',

    construct: function(app, permissions) {

        this.callConstructor('Modules.CrudModule', [app, permissions]);

        this.getFormModuleClassname = function() {
            return 'Modules.Form.City';
        };

        this.getModuleName = function() {
            return 'City';
        };
    },

    methods: {

        initModel: function() {
            this.setModel(this.getApp().getModel('Model.City'));
        },

        setControls: function() {

            this.super('setControls');

            var citiesGrid = $("#cities-City-grid").grid({

                autoLoad: false,
                cellEdit: true,
                cellEditType: 'callback',
                model: [
                    {
                        header: 'ID',
                        id: 'id',
                        dataMap: 'id'
                    },
                    {
                        header: 'Name',
                        id: 'name',
                        dataMap: 'name',
                        editable: true,
                    }
                ]
            });

            this.setMainViewComponent(citiesGrid);
        }
    }
});
