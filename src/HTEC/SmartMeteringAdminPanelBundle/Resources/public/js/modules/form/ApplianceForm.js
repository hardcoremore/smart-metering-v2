HCM.define({

    name: 'Modules.Form.Appliance',

    extendFrom: 'Modules.RelationFieldFormModule',

    construct: function(app, permissions) {

        this.callConstructor('Modules.RelationFieldFormModule', [app, permissions]);

        this.getModuleName = function() {
            return 'ApplianceForm';
        };
    }
});