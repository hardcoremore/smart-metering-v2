HCM.define({

    name: 'Modules.Form.PowerForecast',

    extendFrom: 'Modules.RelationFieldFormModule',

    construct: function(app, permissions) {

        this.callConstructor('Modules.RelationFieldFormModule', [app, permissions]);

        this.getModuleName = function() {
            return 'PowerForecastForm';
        };
    },

    methods: {

        initController: function() {
            var formController = this.getApp().getController('Controllers.Form.PowerForecast', [this, this.getModel()]);
            this.setFormController(formController);
        },

        setControls: function() {

            this.super('setControls');

            this.blockFormField('amountToForecast');
        },

        submitFormCreate: function(formData) {

            var powerPurchase = this.getRelationFieldSelectedData('powerPurchase');

            var amountToForecastInput = this.getFormFieldByName('amountToForecast');
            var amountToForecast = this.getFormFieldValue('amountToForecast', amountToForecastInput);

            if(powerPurchase && formData.amountToForecast > powerPurchase.amountPurchased) {
                this.getApp().showNotification('error', 'You can not forecast more than it is purchased.');
                this.showFormFieldError('amountToForecast', 'This field is invalid');
            }
            else {
                this.super('submitFormCreate', [formData]);
            }
        },

        loadRelationFormFieldData: function(fieldModelData, parameters) {

            if(fieldModelData.name === 'powerPurchase') {
                fieldModelData.relationModel.readAllForToday(this.getRelationFieldLoadOptions(fieldModelData, parameters));
            }
            else {
                this.super('loadRelationFormFieldData', [fieldModelData, parameters]);
            }
        }
    }
});