HCM.define({

    name: 'Modules.Form.RoleForm',

    extendFrom: 'Modules.RelationFieldFormModule',

    construct: function(app, permissions) {

        this.callConstructor('Modules.RelationFieldFormModule', [app, permissions]);

        var permissionsInputWidget;

        this.setPermissionsInputWidget = function(piw) {
            permissionsInputWidget = piw;
        };

        this.getPermissionsInputWidget = function() {
            return permissionsInputWidget;
        };

        this.getModuleName = function() {
            return 'RoleForm';
        };
    },

    methods: {

        initModel: function() {
            this.setModel(this.getApp().getRoleModel());
        },

        initController: function() {
            var formController = this.getApp().getController('Controllers.Form.Role', [this, this.getModel()]);
            this.setFormController(formController);
        },

        setControls: function() {

            this.super('setControls');

            var permissionInput = new InspireTree({
                target: '#permissions-Role-widget',
                data: [{text: 'Loading....'}],
                selection: {
                    mode: 'checkbox',
                    multiple: true
                }
            });

            this.setPermissionsInputWidget(permissionInput);
        },

        loadRelationFormFieldData: function(fieldModelData) {

            if(fieldModelData.name === 'permissions') {

                fieldModelData.relationModel.readAll({
                    showLoader: false,
                    scope: {
                        fieldModelData: fieldModelData,
                        module: this
                    }
                });
            }
            else {
                this.super('loadRelationFormFieldData', [fieldModelData]);
            }
        },

        emptyRelationField: function(fieldModelData) {

            if(fieldModelData.name === 'permissions') {
                this.getPermissionsInputWidget().removeAll();
            }
            else {
                this.super('populateRelationField', [fieldModelData, data]);
            }
        },

        populateRelationFormField: function(fieldModelData, data, relationFormField) {
            
            if(fieldModelData.name === 'permissions') {
                var treeData = fieldModelData.relationModel.convertResponseDataToTreeData(data);
                this.getPermissionsInputWidget().load(treeData);
            }
            else {
                this.super('populateRelationFormField', [fieldModelData, data, relationFormField]);
            }
        },

        setFormFieldValue: function(fieldName, value, fieldInput) {

            if(fieldName === 'permissions') {

                var fieldModelData = this.getModel().getFieldByName(fieldName);

                if(this.isRelationFormFieldInitialized(fieldModelData, this.getFormFieldByName('permissions'))) {

                    var treeNodes = this.getPermissionsInputWidget().nodes(Object.keys(value));

                    var node;

                    for(var i = 0, len = treeNodes.length; i < len; i++) {

                        node = treeNodes[i];

                        var valuePermissionActions;

                        var valueActions = value[node.id].actions;

                        for(var c = 0, clen = node.children.length; c < clen; c++) {

                            var actionNode = node.children[c];

                            if(valueActions.indexOf(actionNode.permissionData.action) !== -1) {
                                actionNode.select();
                            }
                        }
                    }
                }
            }
            else if(fieldName === 'name') {
                // remove ROLE_ from value before populating role name field
                this.super('setFormFieldValue', [fieldName, value.split("ROLE_").pop(), fieldInput]);
            }
            else {
                this.super('setFormFieldValue', [fieldName, value, fieldInput]);
            }
        },

        getFormFieldValue: function(fieldName, fieldInputElement) {

            if(fieldName === 'permissions') {
                
                var selectedNodes = this.getPermissionsInputWidget().selected();
                var fieldModelData = this.getModel().getFieldByName(fieldName);

                var updateData = fieldModelData.relationModel.convertTreeDataToUpdateData(selectedNodes);

                return updateData;
            }
            else {
                return this.super('getFormFieldValue', [fieldName, fieldInputElement]);
            }
        },

        resetFormField: function(fieldName, inputElement) {
            if(fieldName === 'permissions') {
                this.getPermissionsInputWidget().selected().deselect();
            }
            else {
                this.super('resetFormField', [fieldName, inputElement]);   
            }
        }
    }
});