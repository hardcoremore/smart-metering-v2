HCM.define({

    name: 'Modules.Form.ApplianceTypeForm',

    extendFrom: 'Modules.RelationFieldFormModule',

    construct: function(app, permissions) {

        this.callConstructor('Modules.RelationFieldFormModule', [app, permissions]);

        var customerCategoriesWidget;

        this.setCustomerCategoriesInputWidget = function(ciw) {
            customerCategoriesWidget = ciw;
        };

        this.getCustomerCategoriesInputWidget = function() {
            return customerCategoriesWidget;
        };

        this.getModuleName = function() {
            return 'ApplianceTypeForm';
        };
    },

    methods: {

        setControls: function() {

            this.super('setControls');

            var customerCategoriesFieldModelData = this.getModel().getFieldByName('customerCategories');

            var customerCategoriesCheckBoxGroupInput = $("#customerCategories-ApplianceType-widget").checkboxgroup({
                labelParam: 'name',
                inputName: "customerCategories-autocomplete",
                class: "customerCategories-checkbox-item",
                data: this.getRelationFieldSelectData(customerCategoriesFieldModelData.name)
            });

            this.setCustomerCategoriesInputWidget(customerCategoriesCheckBoxGroupInput);
            this.setRelationFormFieldAsInitialized(customerCategoriesFieldModelData, customerCategoriesCheckBoxGroupInput);
        },

        getFormFieldByName: function(elementName) {

            if(elementName === 'customerCategories') {
                return this.getCustomerCategoriesInputWidget();
            }
            else {
                return this.super('getFormFieldByName', [elementName]);
            }
        },

        setFormFieldValue: function(fieldName, value, fieldInput) {

            if(fieldName === 'customerCategories') {
                // here value is expected to be array of objects
                this.getCustomerCategoriesInputWidget().checkboxgroup('setSelectedItems', value);
            }
            else {
                this.super('setFormFieldValue', [fieldName, value, fieldInput]);
            }
        },

        getFormFieldValue: function(fieldName, fieldInputElement) {

            if(fieldName === 'customerCategories') {
                // here it will return array of values not objects
                return this.getCustomerCategoriesInputWidget().checkboxgroup('getSelectedValues');
            }
            else {
                return this.super('getFormFieldValue', [fieldName, fieldInputElement]);
            }
        },

        getRelationFieldSelectedData: function(fieldName) {

            if(fieldName === 'customerCategories') {
                console.log(this.getCustomerCategoriesInputWidget())
                return this.getCustomerCategoriesInputWidget().checkboxgroup('getSelectedItems');
            }
            else {
                return this.super('getRelationFieldSelectedData', [fieldName]);
            }
        },

        populateRelationFormField: function(fieldModelData, data, relationFormField) {

            if(fieldModelData.name === 'customerCategories') {
                this.getCustomerCategoriesInputWidget().checkboxgroup('option', {data:data});
            }
            else {
                this.super('populateRelationFormField', [fieldModelData, data, relationFormField]);
            }
        },

        resetFormField: function(fieldName, inputElement) {
            if(fieldName === 'customerCategories') {
                this.getCustomerCategoriesInputWidget().checkboxgroup('resetAll');
            }
            else {
                this.super('resetFormField', [fieldName, inputElement]);   
            }
        }
    }
});