HCM.define({

    name: 'Modules.Form.DistributionTransformer',

    extendFrom: 'Modules.RelationFieldFormModule',

    construct: function(app, permissions) {

        this.callConstructor('Modules.RelationFieldFormModule', [app, permissions]);

        var outputFeedersInputWidget;

        this.setOutputFeedersInputWidget = function(ofw) {
            outputFeedersInputWidget = ofw;
        };

        this.getOutputFeedersInputWidget = function() {
            return outputFeedersInputWidget;
        };

        this.getModuleName = function() {
            return 'DistributionTransformerForm';
        };
    },

    methods: {

        initModule: function() {
            this.setNotAutoLoadingRelationFields(['injectionSubstation', 'powerTransformer', 'inputFeeder', 'outputFeeders']);
            this.super('initModule');
        },

        initController: function() {
            var formController = this.getApp().getController('Controllers.Form.DistributionTransformer', [this, this.getModel()]);
            this.setFormController(formController);
        },

        setControls: function() {

            this.super('setControls');

            var self = this;

            var outputFeedersFieldModelData = this.getModel().getFieldByName('outputFeeders');

            var outputFeedersInputWidget = this.getModuleElement().find("#outputFeeders-DistributionTransformer-widget").multiAutocomplete({

                inputName: 'outputFeeders-autocomplete',
                allowNew: true,
                inputPlaceHolder: 'Type Feeder',
                autoCompleteOptions: {
                    source: outputFeedersFieldModelData.relationModel.getUrl('/list-auto-complete'),
                    ajaxQueryParams: {type: 'low-voltage'}
                },

                onItemAdd: function(ev, ui) {
                    self.setIsFormChanged(true);
                },

                onItemRemove: function(ev, ui) {
                    self.setIsFormChanged(true);
                }
            });

            this.setOutputFeedersInputWidget(outputFeedersInputWidget);
            this.setRelationFormFieldAsInitialized(outputFeedersFieldModelData, outputFeedersInputWidget);
        },

        isRelationFormFieldInitialized: function(fieldModelData, relationFormField) {

            var isInitialized = this.super('isRelationFormFieldInitialized', [fieldModelData, relationFormField]);

            if(isInitialized && this.getFormMode() === 'edit') {

                var editRecordData = this.getEditingRecordData();

                switch(fieldModelData.name) {

                    case 'injectionSubstation':

                        var dataLoadedForBusinessDistrictId = Number(relationFormField.data('business-district'));
                        var editRecordDataBusinessDistrictId = this.getRelationRecordValue(
                            this.getModel().getFieldByName('businessDistrict'),
                            editRecordData['businessDistrict']
                        );

                        return dataLoadedForBusinessDistrictId === editRecordDataBusinessDistrictId;

                    break;

                    case 'powerTransformer':

                        var dataLoadedForInjectionSubstationId = Number(relationFormField.data('injection-substation'));
                        var editRecordDataInjectionSubstationId = this.getRelationRecordValue(
                            this.getModel().getFieldByName('injectionSubstation'),
                            editRecordData['injectionSubstation']
                        );
                        
                        return dataLoadedForInjectionSubstationId === editRecordDataInjectionSubstationId;

                    break;

                    case 'inputFeeder':

                        var dataLoadedForPowerTransformerId = Number(relationFormField.data('power-transformer'));
                        var editRecordDataPowerTransformerId = this.getRelationRecordValue(
                            this.getModel().getFieldByName('powerTransformer'),
                            editRecordData['powerTransformer']
                        );

                        return dataLoadedForPowerTransformerId === editRecordDataPowerTransformerId;

                    break;

                    default:
                        return isInitialized;
                    break;
                }
            }
            else {
                return isInitialized;
            }
        },

        setFormFieldValue: function(fieldName, value, fieldInputElement) {

            switch(fieldName)
            {
                case 'outputFeeders':
                    this.getOutputFeedersInputWidget().multiAutocomplete('option','data', value);
                break;

                case 'businessDistrict':
                case 'injectionSubstation':
                case 'powerTransformer':
                case 'inputFeeder':
                default:
                    this.super('setFormFieldValue', [fieldName, value, fieldInputElement]);
                break;
            }

            switch(fieldName)
            {
                case 'businessDistrict':

                    var injectionSubstationFormField = this.getFormFieldByName('injectionSubstation');
                    var businessDistrictId = this.getRelationRecordValue(
                        this.getModel().getFieldByName('businessDistrict'),
                        value
                    );

                    if(Number(injectionSubstationFormField.data('business-district')) !== businessDistrictId) {
                        this.loadBusinessDistrictInjectionSubstations(businessDistrictId);
                    }

                break;

                case 'injectionSubstation':

                    var powerTransformerFormField = this.getFormFieldByName('powerTransformer');

                    if(value !== null && value !== undefined) {
                        var injectionSubstationId = this.getRelationRecordValue(
                            this.getModel().getFieldByName('injectionSubstation'),
                            value
                        );

                        if(Number(powerTransformerFormField.data('injection-substation')) !== injectionSubstationId) {
                            this.loadInjectionSubstationPowerTransformers(injectionSubstationId);
                        }
                    }
                    else {
                        this.resetRelationFormField(
                            this.getModel().getFieldByName('powerTransformer'),
                            powerTransformerFormField
                        );
                    }

                break;

                case 'powerTransformer':

                    var inputFeederFormField = this.getFormFieldByName('inputFeeder');

                    if(value !== null && value !== undefined) {

                        var powerTransformerId = this.getRelationRecordValue(
                            this.getModel().getFieldByName('powerTransformer'),
                            value
                        );

                        if(Number(inputFeederFormField.data('power-transformer')) !== powerTransformerId) {
                            this.loadPowerTransformerOutputFeeders(powerTransformerId);
                        }
                    }
                    else {
                        this.resetRelationFormField(
                            this.getModel().getFieldByName('inputFeeder'),
                            inputFeederFormField
                        );
                    }

                break;
            }
        },

        getFormFieldValue: function(fieldName, fieldInputElement) {

            switch(fieldName)
            {
                case 'outputFeeders':
                    return this.getOutputFeedersInputWidget().multiAutocomplete('getSelectedValues');
                break;

                default:
                    return this.super('getFormFieldValue', [fieldName, fieldInputElement]);
                break;
            }
        },

        resetRelationFormField: function(fieldModelData, relationFormField) {

            this.super('resetRelationFormField', [fieldModelData, relationFormField]);

            switch(fieldModelData.name)
            {
                case 'injectionSubstation':
                    relationFormField.data('business-district', null);
                break;

                case 'powerTransformer':
                    relationFormField.data('injection-substation', null);
                break;

                case 'inputFeeder':
                    relationFormField.data('power-transformer', null);
                break;
            }
        },

        resetFormField: function(fieldName, inputElement) {

            switch(fieldName)
            {
                case 'businessDistrict':
                    this.super('resetFormField', [fieldName, inputElement]);
                    this.resetRelationFormField(
                        this.getModel().getFieldByName('injectionSubstation'),
                        this.getFormFieldByName('injectionSubstation')
                    );
                break;

                case 'injectionSubstation':
                    this.super('resetFormField', [fieldName, inputElement]);
                    this.resetRelationFormField(
                        this.getModel().getFieldByName('powerTransformer'),
                        this.getFormFieldByName('powerTransformer')
                    );
                break;

                case 'powerTransformer':
                    this.super('resetFormField', [fieldName, inputElement]);
                    this.resetRelationFormField(
                        this.getModel().getFieldByName('inputFeeder'),
                        this.getFormFieldByName('inputFeeder')
                    );
                break;

                case 'outputFeeders':
                    this.getOutputFeedersInputWidget().multiAutocomplete('removeAllItems');
                break;

                default:
                    this.super('resetFormField', [fieldName, inputElement]);
                break;
            }
        },

        loadBusinessDistrictInjectionSubstations: function(businessDistrictId) {

            if(businessDistrictId) {

                var fieldModelData = this.getModel().getFieldByName('injectionSubstation');

                var parameters = {
                    businessDistrict: businessDistrictId
                };

                this.loadRelationFormFieldData(fieldModelData, parameters);
            }
        },

        loadInjectionSubstationPowerTransformers: function(injectionSubstationId) {

            if(injectionSubstationId) {

                var fieldModelData = this.getModel().getFieldByName('powerTransformer');

                var parameters = {
                    injectionSubstation: injectionSubstationId
                };

                this.loadRelationFormFieldData(fieldModelData, parameters);
            }
        },

        loadPowerTransformerOutputFeeders: function(powerTransformerId) {

            var fieldModelData = this.getModel().getFieldByName('inputFeeder');

            var parameters = {
                outputFeedersForPowerTransformer: powerTransformerId
            };

            this.loadRelationFormFieldData(fieldModelData, parameters);
        },

        populateRelationFormField: function(fieldModelData, data, relationFormField) {

            this.super('populateRelationFormField', [fieldModelData, data, relationFormField]);

            var powerTransformerValue = this.getFormFieldValue('powerTransformer', this.getFormFieldByName('powerTransformer'));

            if(fieldModelData.name === 'injectionSubstation') {

                var businessDistrictValue = this.getFormFieldValue('businessDistrict', this.getFormFieldByName('businessDistrict'));

                if(businessDistrictValue) {
                    relationFormField.data('business-district', businessDistrictValue);
                }
            }
            else if(fieldModelData.name === 'powerTransformer') {

                var injectionSubstationValue = this.getFormFieldValue('injectionSubstation', this.getFormFieldByName('injectionSubstation'));

                if(injectionSubstationValue) {
                    relationFormField.data('injection-substation', injectionSubstationValue);    
                }
            }
            else if(fieldModelData.name === 'inputFeeder') {

                var powerTransformerValue = this.getFormFieldValue('powerTransformer', this.getFormFieldByName('powerTransformer'));

                if(powerTransformerValue) {
                    this.getFormFieldByName(fieldModelData.name).data('power-transformer', powerTransformerValue);
                }
            }
        }
    }
});