HCM.define({

    name: 'Modules.Form.ElectricPole',

    extendFrom: 'Modules.RelationFieldFormModule',

    construct: function(app, permissions) {

        this.callConstructor('Modules.RelationFieldFormModule', [app, permissions]);

        var feederInputWidget;

        this.setFeederAutoComplete = function(fiw) {
            feederInputWidget = fiw;
        };

        this.getFeederAutocomplete = function() {
            return feederInputWidget;
        };

        this.getModuleName = function() {
            return 'ElectricPoleForm';
        };
    },

    methods: {

        initModule: function() {
            this.setNotAutoLoadingRelationFields(['feeder']);
            this.super('initModule');
        },

        setControls: function() {

            this.super('setControls');

            var self = this;

            var feederFieldModelData = this.getModel().getFieldByName('feeder');

            var feederAutoComplete = this.getFormFieldByName('feeder').autocomplete({

                queryParamName: "q",
                valueParam: "id",
                labelParam: 'name',

                source: feederFieldModelData.relationModel.getUrl('/list-auto-complete')
            });

            this.setFeederAutoComplete(feederAutoComplete);
            this.setRelationFormFieldAsInitialized(feederFieldModelData, feederAutoComplete);
        },

        setFormFieldValue: function(fieldName, value, fieldInput) {

            switch(fieldName)
            {
                case 'feeder':
                    this.getFeederAutocomplete().autocomplete('setSelectedOriginalItem', value);
                break;

                default:
                    this.super('setFormFieldValue', [fieldName, value, fieldInput]);
                break;
            }
        },

        getFormFieldValue: function(fieldName, fieldInputElement) {

            switch(fieldName)
            {
                case 'feeder':
                    return this.getRelationRecordValue(
                        this.getModel().getFieldByName('feeder'),
                        this.getFeederAutocomplete().autocomplete('getSelectedOriginalItem')
                    );
                break;

                default:
                    return this.super('getFormFieldValue', [fieldName, fieldInputElement]);
                break;
            }
        },

        resetFormField: function(fieldName, inputElement) {

            switch(fieldName)
            {
                case 'feeder':
                    this.getFeederAutocomplete().autocomplete('reset');
                break;

                default:
                    this.super('resetFormField', [fieldName, inputElement]);
                break;
            }
        }
    }
});