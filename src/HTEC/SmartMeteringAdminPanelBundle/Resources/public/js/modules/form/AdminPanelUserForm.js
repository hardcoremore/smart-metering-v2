HCM.define({

    name: 'Modules.Form.AdminPanelUserForm',

    extendFrom: 'Modules.RelationFieldFormModule',

    construct: function(app, permissions) {

        this.callConstructor('Modules.RelationFieldFormModule', [app, permissions]);

        var rolesInputWidget;

        this.setRolesInputWidget = function(riw) {
            rolesInputWidget = riw;
        };

        this.getRolesInputWidget = function() {
            return rolesInputWidget;
        };

        this.getModuleName = function() {
            return 'AdminPanelUserForm';
        };
    },

    methods: {

        setControls: function() {

            this.super('setControls');

            var rolesFieldModelData = this.getModel().getFieldByName('roles');

            var rolesInput = this.getModuleElement().find("#roles-AdminPanelUser-widget").checkboxgroup({
                labelParam: 'role',
                inputName: "roles-autocomplete",
                class: "roles-checkbox-item",
                data: this.getRelationFieldSelectData(rolesFieldModelData.name)
            });

            this.setRolesInputWidget(rolesInput);
            this.setRelationFormFieldAsInitialized(rolesFieldModelData, rolesInput);
        },

        startModule: function() {

            this.super('startModule');

            if(this.getFormMode() === 'edit') {
                this.getFormFieldByName('password').closest('.form-field-set').hide();
                this.getFormFieldByName('repeatPassword').closest('.form-field-set').hide();
            }
            else {
                this.getFormFieldByName('password').closest('.form-field-set').show();
                this.getFormFieldByName('repeatPassword').closest('.form-field-set').show();
            }
        },

        getFormFieldByName: function(fieldName) {

            if(fieldName === 'roles') {
                return this.getRolesInputWidget();
            }
            else {
                return this.super('getFormFieldByName', [fieldName]);
            }
        },

        setFormFieldValue: function(fieldName, value, fieldInput) {

            if(fieldName === 'roles') {
                // here value is expected to be array of objects
                this.getRolesInputWidget().checkboxgroup('setSelectedItems', value);
            }
            else {
                this.super('setFormFieldValue', [fieldName, value, fieldInput]);
            }
        },

        getFormFieldValue: function(fieldName, fieldInputElement) {

            if(fieldName === 'roles') {
                // here it will return array of values not objects
                return this.getRolesInputWidget().checkboxgroup('getSelectedValues');
            }
            else {
                return this.super('getFormFieldValue', [fieldName, fieldInputElement]);
            }
        },

        getRelationFieldSelectedData: function(fieldName) {

            if(fieldName === 'roles') {
                return this.getRolesInputWidget().checkboxgroup('getSelectedItems');
            }
            else {
                return this.super('getRelationFieldSelectedData', [fieldName]);
            }
        },

        populateRelationFormField: function(fieldModelData, data, relationFormField) {

            if(fieldModelData.name === 'roles') {
                this.getRolesInputWidget().checkboxgroup('option', {data:data});
            }
            else {
                this.super('populateRelationFormField', [fieldModelData, data, relationFormField]);
            }
        },

        resetFormField: function(fieldName, inputElement) {
            if(fieldName === 'roles') {
                this.getRolesInputWidget().checkboxgroup('resetAll');
            }
            else {
                this.super('resetFormField', [fieldName, inputElement]);   
            }
        },

        submitFormCreate: function(formData) {

            if(formData.password !== formData.repeatPassword) {

                this.resetFormField('password', this.getFormFieldByName('password'));
                this.resetFormField('repeatPassword', this.getFormFieldByName('repeatPassword'));

                this.getApp().showNotification('error', 'Passwords did not match');
                this.showFormFieldError('repeatPassword', 'This value does not match with password field');
            }
            else {
                delete formData.repeatPassword;
                this.super('submitFormCreate', [formData]);
            }
        },

        submitFormUpdate: function(recordId, formData) {

            delete formData.password;
            delete formData.repeatPassword;

            this.getModel().patch(recordId, this.prepareDataForUpdate(formData, recordId), {scope:this});
        }
    }
});