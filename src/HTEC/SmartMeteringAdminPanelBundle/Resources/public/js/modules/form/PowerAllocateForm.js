HCM.define({

    name: 'Modules.Form.PowerAllocate',

    extendFrom: 'Modules.RelationFieldFormModule',

    construct: function(app, permissions) {

        this.callConstructor('Modules.RelationFieldFormModule', [app, permissions]);

        var powerAllocateDataModel;

        var businessDistrictAllocationGrid;
        var totalRemainingAvailablePowerAllocation = 0;
        var confirmPowerAllocationPopup;
        var editingPowerAllocationsData;

        this.setPowerAllocateDataModel = function(model) {
            powerAllocateDataModel = model;
        };

        this.getPowerAllocateDataModel = function() {
            return powerAllocateDataModel;
        };

        this.setBusinessDistrictAllocationGrid = function(grid) {
            businessDistrictAllocationGrid = grid;
        };

        this.getBusinessDistrictAllocationGrid = function() {
            return  businessDistrictAllocationGrid;
        };

        this.setTotalRemainingAvailablePowerAllocation = function(value) {
            totalRemainingAvailablePowerAllocation = value;
        };

        this.getTotalRemainingAvailablePowerAllocation = function() {
            return totalRemainingAvailablePowerAllocation;
        };

        this.setConfirmPowerAllocationPopup = function(popup) {
            confirmPowerAllocationPopup = popup;
        };

        this.getConfirmPowerAllocationPopup = function() {
            return confirmPowerAllocationPopup;
        };

        this.setEditingPowerAllocationsData = function(data) {
            editingPowerAllocationsData = data;
        };

        this.getEditingPowerAllocationsData = function() {
            return editingPowerAllocationsData;
        };

        this.getModuleName = function() {
            return 'PowerAllocateForm';
        };
    },

    methods: {

        initModel: function() {
            this.super('initModel');
            this.setPowerAllocateDataModel(this.getApp().getModel('Model.PowerAllocateData'));
        },

        initController: function() {
            var formController = this.getApp().getController('Controllers.Form.PowerAllocate', [this, this.getModel()]);
            this.setFormController(formController);
        },

        setControls: function() {

            this.super('setControls');

            this.blockFormField('totalPowerAllocated');
            
            var self = this;

            var businessDistrictAllocationsGrid = this.getModuleElement().find("#businessDistrictAllocations-PowerAllocateForm-grid").grid({

                autoLoad: false,
                cellEdit: true,
                localSort: true,

                model: [
                    {
                        header: 'ID',
                        id: 'id',
                        dataMap: 'id',
                        hidden: true
                    },
                    {
                        header: 'Business District',
                        id: 'businessDistrict',
                        dataMap: 'businessDistrict',
                        sortPropertyName: 'name',
                        formatter: function(cellValue) {
                            if(cellValue) {
                                return cellValue.name;
                            }
                        }
                    },
                    {
                        header: 'Number of Customers',
                        id: 'numberOfCustomers',
                        dataMap: 'numberOfCustomers',
                        formatter: function(cellValue) {
                            if(cellValue) {
                                return Number(cellValue).formatNumberThousands();
                            }
                        }
                    },
                    {
                        header: 'Amount Forecasted MW',
                        id: 'powerForecasted',
                        dataMap: 'powerForecasted',
                        formatter: function(cellValue) {
                            if(cellValue) {
                                return Number(cellValue).toFixed(4);
                            }
                        }
                    },
                    {
                        header: 'Amount To Allocate MW',
                        id: 'powerAllocated',
                        dataMap: 'powerAllocated',
                        formatter: function(cellValue) {
                            if(cellValue) {
                                return Number(cellValue).toFixed(4);
                            }
                        }
                    },
                    {
                        header: 'Power Override MW',
                        id: 'powerOverride',
                        dataMap: 'powerOverride',
                        editable: true,
                        editOptions: {
                            validateCallback: function(editedValue, columnName, rowData) {

                                var powerOverride = Number(editedValue);

                                if(powerOverride >= 0 && powerOverride <= self.getTotalRemainingAvailablePowerAllocation()) {
                                    return true;
                                }
                                else {
                                    return false;
                                }
                            }
                        }
                    },
                    {
                        header: 'Is Allocated',
                        id: 'isAllocated',
                        dataMap: 'isAllocated',
                        formatter: function(cellValue, rowData, rowIndex) {

                            if(cellValue === true) {
                                return '<span class="grid-green-text">Allocated</span>';
                            }
                            else {
                                return '<span class="grid-red-text">Not Allocated</span>';   
                            }
                        }
                    },
                ]
            });

            this.setBusinessDistrictAllocationGrid(businessDistrictAllocationsGrid);
        },

        displayOverridePowerPopup: function(overrideBusinessDistricts) {

            var popupElement = this.getModuleElement().find('#confirm-power-allocate-popup').clone();
            
            var businessDistrictNames = '';
            var totalOverride = 0;

            for(var i in overrideBusinessDistricts) {
                businessDistrictNames += overrideBusinessDistricts[i].businessDistrict.name + ', ';
                totalOverride += Number(overrideBusinessDistricts[i].powerOverride);
            }

            businessDistrictNames = businessDistrictNames.replace(/,\s+$/, '');

            var confirmPowerAllocatePopup = popupElement.popup({
                compileTemplateFunction: this.getApp().compileTemplate,
                compileTemplateData: {
                    title: 'Are you sure you want to override total ' + totalOverride + 'MW power to these Business Districts: ' + businessDistrictNames + ' ?',
                    okLabel: 'Yes',
                    cancelLabel: 'No'
                }
            });

            this.setConfirmPowerAllocationPopup(confirmPowerAllocatePopup);
            confirmPowerAllocatePopup.popup('open');
        },

        updateMaxOverrideValue: function(maxOverride) {

            this.setTotalRemainingAvailablePowerAllocation(Math.round(maxOverride));
            this.getBusinessDistrictAllocationGrid().grid('setColumnEditOptions', 'powerOverride', {
                validationErrorMessage: 'Value must be greater or equal than 0 and lower or equal than ' + Math.round(maxOverride)
            });
        },

        resetFormField: function(fieldName, inputElement) {

            if(fieldName === 'businessDistrictAllocations') {

                this.getBusinessDistrictAllocationGrid().grid('removeAllRows');
            }
            else {
                this.super('resetFormField', [fieldName, inputElement]);
            }
        },

        loadRelationFormFieldData: function(fieldModelData, parameters) {

            if(fieldModelData.name === 'powerForecast') {
                fieldModelData.relationModel.readAllForTodayWithDetails(this.getRelationFieldLoadOptions(fieldModelData, parameters));
            }
            else {
                this.super('loadRelationFormFieldData', [fieldModelData, parameters]);
            }
        },

        getOverridenBusinessDistrictData: function() {

            var businessDistrictData = this.getBusinessDistrictAllocationGrid().grid('getAllData');
            var overridenBusinessDistricts = [];

            for(var i = 0, len = businessDistrictData.length; i < len; i++) {
                if(businessDistrictData[i].powerOverride > 0) {
                    overridenBusinessDistricts.push(businessDistrictData[i]);
                }
            }

            return overridenBusinessDistricts;
        },

        updatePowerAllocationsGrid: function(recordData) {

            var businessDistrictTotals = recordData.businessDistrictTotals;
            var powerAllocateData = null;
            var totalPowerForecasted = 0;
            
            this.getBusinessDistrictAllocationGrid().grid('removeAllRows');

            var powerInMW = 0;

            for(var i = 0, len = businessDistrictTotals.length; i < len; i++) {

                powerInMW = businessDistrictTotals[i].powerAllocationInKilowattHour / 1000;

                powerAllocateData = {
                    businessDistrict: businessDistrictTotals[i].businessDistrict,
                    numberOfCustomers: businessDistrictTotals[i].numberOfCustomers,
                    powerForecasted: powerInMW,
                    powerAllocated: powerInMW,
                    powerOverride: businessDistrictTotals[i].powerOverride
                };

                if(this.getFormMode() === 'edit') {

                    if(recordData.id === this.getEditingRecordData().powerForecast.id) {
                        powerAllocateData.isAllocated = true;
                    }
                    else {
                        powerAllocateData.isAllocated = false;
                    }
                }
                else {
                    powerAllocateData.isAllocated = false;
                }


                totalPowerForecasted += powerAllocateData.powerForecasted;

                this.getBusinessDistrictAllocationGrid().grid('addRow', powerAllocateData);
            }

            this.setFormFieldValue(
                'totalPowerAllocated',
                Math.round(totalPowerForecasted),
                this.getFormFieldByName('totalPowerAllocated')
            );

            this.updateMaxOverrideValue(totalPowerForecasted);
        },

        setFormFieldValue: function(fieldName, value, formFieldInput) {

            this.super('setFormFieldValue', [fieldName, value, formFieldInput]);

            if(this.getFormMode() === 'edit' && fieldName === 'powerForecast') {

                var selectedData = this.getRelationFieldSelectedData(fieldName);
                
                if(selectedData) {

                    this.getBusinessDistrictAllocationGrid().grid('removeAllRows');

                    var editingPowerAllocationsData = this.getEditingPowerAllocationsData();
                    var recrodData;
                    var mergedData = [];

                    for(var i = 0, len = selectedData.businessDistrictTotals.length; i < len; i++) {

                        recordData = selectedData.businessDistrictTotals[i];

                        if(editingPowerAllocationsData) {
                            for(var c = 0, clen = editingPowerAllocationsData.length; c < clen; c++) {
                                if(editingPowerAllocationsData[c].businessDistrict.id === recordData.businessDistrict.id) {
                                    recordData = $.extend(recordData, editingPowerAllocationsData[c]);
                                }
                            }
                        }

                        mergedData.push(recordData);

                        this.getBusinessDistrictAllocationGrid().grid('addRow', recordData);
                    }
                    
                    this.setEditingPowerAllocationsData(mergedData);
                }
            }
        },

        updateFormSubmitButtonLabel: function(formMode) {

            if(formMode === 'edit') {
                this.updateSubmitButtonLabel('Update Power Allocation');
            }
            else {
                this.updateSubmitButtonLabel('Allocate Power');
            }
        },
    }
});