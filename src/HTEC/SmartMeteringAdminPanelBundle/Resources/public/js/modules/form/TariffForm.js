HCM.define({

    name: 'Modules.Form.Tariff',

    extendFrom: 'Modules.FormModule',

    construct: function(app, permissions) {

        this.callConstructor('Modules.FormModule', [app, permissions]);

        this.getModuleName = function() {
            return 'TariffForm';
        };
    }
});