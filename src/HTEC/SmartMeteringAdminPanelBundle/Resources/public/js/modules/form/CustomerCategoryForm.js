HCM.define({

    name: 'Modules.Form.CustomerCategory',

    extendFrom: 'Modules.FormModule',

    construct: function(app, permissions) {

        this.callConstructor('Modules.FormModule', [app, permissions]);

        this.getModuleName = function() {
            return 'CustomerCategoryForm';
        };
    }
});