HCM.define({

    name: 'Modules.Form.EstimatedConsumptionForm',

    extendFrom: 'Modules.RelationFieldFormModule',

    construct: function(app, permissions) {

        this.callConstructor('Modules.RelationFieldFormModule', [app, permissions]);

        var customerAutoComplete;
        var estimatedLoadAppliaceModule = null;

        this.setCustomerAutoComplete = function(cac) {
            customerAutoComplete = cac;
        };

        this.getCustomerAutoComplete = function() {
            return customerAutoComplete;
        };

        this.getModuleName = function() {
            return 'EstimatedConsumptionForm';
        };
    },

    methods: {

        initModule: function() {

            this.setNotAutoLoadingRelationFields(['customer']);
            this.super('initModule');
        },

        setControls: function() {

            this.super('setControls');

            var self = this;

            var customerFieldModelData = this.getModel().getFieldByName('customer');

            var customerAutoComplete = this.getFormFieldByName('customer').autocomplete({

                queryParamName: "q",
                valueParam: "id",
                labelParam: function(value) {
                    if(value) {
                        
                        var label = value.name + " (" + value.accountNumber + ")";

                        if(value.businessDistrict) {
                            label = label + ' - ' + value.businessDistrict.name                             
                        }

                        return label;
                    }
                },

                source: customerFieldModelData.relationModel.getUrl('/list-auto-complete')
            });

            this.setCustomerAutoComplete(customerAutoComplete);
            this.setRelationFormFieldAsInitialized(customerFieldModelData, customerAutoComplete);

            this.initializeDateField('start');
            this.initializeDateField('end');
        },

        setFormFieldValue: function(fieldName, value, fieldInputElement) {

            switch(fieldName)
            {
                case 'customer':
                    this.getCustomerAutoComplete().autocomplete('setSelectedOriginalItem', value);
                break;

                default:
                    this.super('setFormFieldValue', [fieldName, value, fieldInputElement]);
                break;
            }
        },

        getFormFieldValue: function(fieldName, fieldInputElement) {

            switch(fieldName)
            {
                case 'customer':
                    return this.getRelationRecordValue(
                        this.getModel().getFieldByName('customer'),
                        this.getCustomerAutoComplete().autocomplete('getSelectedOriginalItem')
                    );
                break;

                default:
                    return this.super('getFormFieldValue', [fieldName, fieldInputElement]);
                break;
            }
        },

        resetFormField: function(fieldName, inputElement) {

            switch(fieldName)
            {
                case 'customer':
                    this.getCustomerAutoComplete().autocomplete('reset');
                break;

                default:
                    this.super('resetFormField', [fieldName, inputElement]);
                break;
            }
        },

        getRelationFieldSelectedData: function(fieldName) {

            switch(fieldName)
            {
                case 'customer':
                    return this.getCustomerAutoComplete().autocomplete('getSelectedOriginalItem');
                break;

                default:
                    return this.super('getRelationFieldSelectedData', [fieldName]);
                break;
            }
        }
    }
});