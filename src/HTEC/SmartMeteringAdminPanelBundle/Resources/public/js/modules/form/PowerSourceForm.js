HCM.define({

    name: 'Modules.Form.PowerSource',

    extendFrom: 'Modules.RelationFieldFormModule',

    construct: function(app, permissions) {

        this.callConstructor('Modules.RelationFieldFormModule', [app, permissions]);

        var outputFeedersInputWidget;

        this.setOutputFeedersInputWidget = function(scw) {
            outputFeedersInputWidget = scw;
        };

        this.getOutputFeedersInputWidget = function() {
            return outputFeedersInputWidget;
        };

        this.getModuleName = function() {
            return 'PowerSourceForm';
        };
    },

    methods: {

        initModule: function() {
            this.setNotAutoLoadingRelationFields(['outputFeeders']);
            this.super('initModule');
        },

        setControls: function() {

            this.super('setControls');

            var self = this;

            var outputFeedersFieldModelData = this.getModel().getFieldByName('outputFeeders');

            var outputFeedersInputWidget = this.getModuleElement().find("#outputFeeders-PowerSource-widget").multiAutocomplete({

                inputName: 'outputFeeders-autocomplete',
                allowNew: true,
                inputPlaceHolder: 'Type Feeder',
                autoCompleteOptions: {
                    source: outputFeedersFieldModelData.relationModel.getUrl('/list-auto-complete'),
                    ajaxQueryParams: {type: 'high-voltage'}
                },

                onItemAdd: function(ev, ui) {
                    self.setIsFormChanged(true);
                },

                onItemRemove: function(ev, ui) {
                    self.setIsFormChanged(true);
                }
            });

            this.setOutputFeedersInputWidget(outputFeedersInputWidget);
            this.setRelationFormFieldAsInitialized(outputFeedersFieldModelData, outputFeedersInputWidget);
        },

        setFormFieldValue: function(fieldName, value, fieldInput) {

            if(fieldName === 'outputFeeders') {
                this.getOutputFeedersInputWidget().multiAutocomplete('option','data', value);
            }
            else {
                this.super('setFormFieldValue', [fieldName, value, fieldInput]);
            }
        },

        getFormFieldValue: function(fieldName, fieldInputElement) {

            if(fieldName === 'outputFeeders') {
                return this.getOutputFeedersInputWidget().multiAutocomplete('getSelectedValues');
            }
            else {
                return this.super('getFormFieldValue', [fieldName, fieldInputElement]);
            }
        },

        resetFormField: function(fieldName, inputElement) {

            if(fieldName === 'outputFeeders') {
                this.getOutputFeedersInputWidget().multiAutocomplete('removeAllItems');
            }
            else {
                this.super('resetFormField', [fieldName, inputElement]);
            }
        }
    }
});