HCM.define({

    name: 'Modules.Form.CustomerNetworkDataForm',

    extendFrom: 'Modules.RelationFieldFormModule',

    construct: function(app, permissions) {

        this.callConstructor('Modules.RelationFieldFormModule', [app, permissions]);

        var customerAutoComplete;
        var highTensionPoleAutoComplete;
        var distributionTransformerAutoComplete;
        var lowTensionPoleAutoComplete;

        this.setCustomerAutoComplete = function(cac) {
            customerAutoComplete = cac;
        };

        this.getCustomerAutoComplete = function() {
            return customerAutoComplete;
        };

        this.setHighTensionPoleAutoComplete = function(htpac) {
            highTensionPoleAutoComplete = htpac;
        };

        this.getHighTensionPoleAutoComplete = function() {
            return highTensionPoleAutoComplete;
        };

        this.setDistributionTransformerAutoComplete = function(dtac) {
            distributionTransformerAutoComplete = dtac;
        };

        this.getDistributionTransformerAutoComplete = function() {
            return distributionTransformerAutoComplete;
        };

        this.setLowTensionPoleAutoComplete = function(ltpac) {
            lowTensionPoleAutoComplete = ltpac;
        };

        this.getLowTensionPoleAutoComplete = function() {
            return lowTensionPoleAutoComplete;
        };

        this.getModuleName = function() {
            return 'CustomerNetworkDataForm';
        };
    },

    methods: {

        initModule: function() {

            this.setNotAutoLoadingRelationFields([
                'customer',
                'injectionSubstation',
                'powerTransformer',
                'powerTransformerFeeder',
                'highTensionPole',
                'distributionTransformer',
                'distributionTransformerFeeder',
                'lowTensionPole'
            ]);

            this.super('initModule');
        },

        initController: function() {
            var formController = this.getApp().getController('Controllers.Form.CustomerNetworkData', [this, this.getModel()]);
            this.setFormController(formController);
        },

        setControls: function() {

            this.super('setControls');

            var self = this;

            var customerFieldModelData = this.getModel().getFieldByName('customer');
            var highTensionPoleFieldModelData = this.getModel().getFieldByName('highTensionPole');
            var distributionTransformerFieldModelData = this.getModel().getFieldByName('distributionTransformer');
            var lowTensionPoleFieldModelData = this.getModel().getFieldByName('lowTensionPole');

            var customerAutoComplete = this.getFormFieldByName('customer').autocomplete({

                queryParamName: "q",
                valueParam: "id",
                labelParam: function(value) {
                    if(value) {
                        return value.name + " (" + value.accountNumber + ")";
                    }
                },

                source: customerFieldModelData.relationModel.getUrl('/list-auto-complete')
            });

            this.setCustomerAutoComplete(customerAutoComplete);
            this.setRelationFormFieldAsInitialized(customerFieldModelData, customerAutoComplete);

            var highTensionPoleAutoComplete = this.getFormFieldByName('highTensionPole').autocomplete({

                queryParamName: "q",
                valueParam: "id",
                labelParam: function(value) {
                    if(value) {

                        var label = value.code;

                        if(value.address) {
                            label += " (" + value.address + ")"
                        }

                        return label;
                    }
                },

                source: highTensionPoleFieldModelData.relationModel.getUrl('/list-auto-complete'),
                ajaxQueryParams: {type: 'high-tension'}
            });

            this.setHighTensionPoleAutoComplete(highTensionPoleAutoComplete);
            this.setRelationFormFieldAsInitialized(highTensionPoleFieldModelData, highTensionPoleAutoComplete);

            var distributionTransformerAutoComplete = this.getFormFieldByName('distributionTransformer').autocomplete({

                queryParamName: "q",
                valueParam: "id",
                labelParam: function(value) {
                    if(value) {

                        var label = value.name;

                        if(value.code) {
                            label += " (" + value.code + ")"
                        }

                        return label;
                    }
                },

                source: distributionTransformerFieldModelData.relationModel.getUrl('/list-auto-complete')
            });

            this.setDistributionTransformerAutoComplete(distributionTransformerAutoComplete);
            this.setRelationFormFieldAsInitialized(distributionTransformerFieldModelData, distributionTransformerAutoComplete);

            var lowTensionPoleAutoComplete = this.getFormFieldByName('lowTensionPole').autocomplete({

                queryParamName: "q",
                valueParam: "id",
                labelParam: function(value) {
                    if(value) {

                        var label = value.code;

                        if(value.address) {
                            label += " (" + value.address + ")"
                        }

                        return label;
                    }
                },

                source: lowTensionPoleFieldModelData.relationModel.getUrl('/list-auto-complete'),
                ajaxQueryParams: {type: 'low-tension'}
            });

            this.setLowTensionPoleAutoComplete(lowTensionPoleAutoComplete);
            this.setRelationFormFieldAsInitialized(lowTensionPoleFieldModelData, lowTensionPoleAutoComplete);
        },

        isRelationFormFieldInitialized: function(fieldModelData, relationFormField) {

            var isInitialized = this.super('isRelationFormFieldInitialized', [fieldModelData, relationFormField]);

            if(isInitialized && this.getFormMode() === 'edit') {

                var editRecordData = this.getEditingRecordData();

                switch(fieldModelData.name) {

                    case 'injectionSubstation':

                        var dataLoadedForBusinessDistrictId = Number(relationFormField.data('business-district'));

                        return dataLoadedForBusinessDistrictId === editRecordData.customer.businessDistrict.id;

                    break;

                    case 'powerTransformer':

                        var dataLoadedForInjectionSubstationId = Number(relationFormField.data('injection-substation'));
                        var editRecordDataInjectionSubstationId = this.getRelationRecordValue(
                            this.getModel().getFieldByName('injectionSubstation'),
                            editRecordData['injectionSubstation']
                        );
                        
                        return dataLoadedForInjectionSubstationId === editRecordDataInjectionSubstationId;

                    break;

                    case 'powerTransformerFeeder':

                        var dataLoadedForPowerTransformerId = Number(relationFormField.data('power-transformer'));
                        var editRecordDataPowerTransformerId = this.getRelationRecordValue(
                            this.getModel().getFieldByName('powerTransformer'),
                            editRecordData['powerTransformer']
                        );

                        return dataLoadedForPowerTransformerId === editRecordDataPowerTransformerId;

                    break;

                    case 'distributionTransformerFeeder':

                        var dataLoadedForDistributionTransformerId = Number(relationFormField.data('distribution-transformer'));
                        var editRecordDataDistributionTransformerId = this.getRelationRecordValue(
                            this.getModel().getFieldByName('distributionTransformer'),
                            editRecordData['distributionTransformer']
                        );

                        return dataLoadedForDistributionTransformerId === editRecordDataDistributionTransformerId;

                    break;

                    default:
                        return isInitialized;
                    break;
                }
            }
            else {
                return isInitialized;
            }
        },

        setFormFieldValue: function(fieldName, value, fieldInputElement) {

            switch(fieldName)
            {
                case 'injectionSubstation':
                case 'powerTransformer':
                case 'powerTransformerFeeder':
                case 'distributionTransformerFeeder':
                    this.super('setFormFieldValue', [fieldName, value, fieldInputElement]);
                break;
            }

            switch(fieldName)
            {
                case 'customer':

                    this.getCustomerAutoComplete().autocomplete('setSelectedOriginalItem', value);

                    if(value.businessDistrict) {

                        var injectionSubstationFormField = this.getFormFieldByName('injectionSubstation');
                        var businessDistrictId = value.businessDistrict.id;

                        if(Number(injectionSubstationFormField.data('business-district')) !== businessDistrictId) {
                            this.loadBusinessDistrictInjectionSubstations(businessDistrictId);
                        }
                    }

                break;

                case 'injectionSubstation':

                    var powerTransformerFormField = this.getFormFieldByName('powerTransformer');

                    if(value !== null && value !== undefined) {

                        var injectionSubstationId = this.getRelationRecordValue(
                            this.getModel().getFieldByName('injectionSubstation'),
                            value
                        );

                        if(Number(powerTransformerFormField.data('injection-substation')) !== injectionSubstationId) {
                            this.loadInjectionSubstationPowerTransformers(injectionSubstationId);
                        }
                    }
                    else {
                        this.resetRelationFormField(
                            this.getModel().getFieldByName('powerTransformer'),
                            powerTransformerFormField
                        );
                    }

                break;

                case 'powerTransformer':

                    var powerTransformerFeederFormField = this.getFormFieldByName('powerTransformerFeeder');

                    if(value !== null && value !== undefined) {

                        var powerTransformerId = this.getRelationRecordValue(
                            this.getModel().getFieldByName('powerTransformer'),
                            value
                        );

                        if(Number(powerTransformerFeederFormField.data('power-transformer')) !== powerTransformerId) {
                            this.loadPowerTransformerOutputFeeders(powerTransformerId);
                        }
                    }
                    else {
                        this.resetRelationFormField(
                            this.getModel().getFieldByName('powerTransformerFeeder'),
                            powerTransformerFeederFormField
                        );
                    }

                break;

                case 'distributionTransformer':

                    this.getDistributionTransformerAutoComplete().autocomplete('setSelectedOriginalItem', value);

                    var distributionTransformerFeederFormField = this.getFormFieldByName('distributionTransformerFeeder');

                    if(value !== null && value !== undefined) {

                        var distributionTransformerId = this.getRelationRecordValue(
                            this.getModel().getFieldByName('distributionTransformer'),
                            value
                        );

                        if(Number(distributionTransformerFeederFormField.data('distribution-transformer')) !== distributionTransformerId) {
                            this.loadDistributionTransformerOutputFeeders(distributionTransformerId);
                        }
                    }
                    else {
                        this.resetRelationFormField(
                            this.getModel().getFieldByName('distributionTransformerFeeder'),
                            distributionTransformerFeederFormField
                        );
                    }

                break;

                case 'highTensionPole':
                    this.getHighTensionPoleAutoComplete().autocomplete('setSelectedOriginalItem', value);
                break;

                case 'lowTensionPole':
                    this.getLowTensionPoleAutoComplete().autocomplete('setSelectedOriginalItem', value);
                break;
            }
        },

        getFormFieldValue: function(fieldName, fieldInputElement) {

            switch(fieldName)
            {
                case 'customer':
                    return this.getRelationRecordValue(
                        this.getModel().getFieldByName('customer'),
                        this.getCustomerAutoComplete().autocomplete('getSelectedOriginalItem')
                    );
                break;

                case 'highTensionPole':
                    return this.getRelationRecordValue(
                        this.getModel().getFieldByName('highTensionPole'),
                        this.getHighTensionPoleAutoComplete().autocomplete('getSelectedOriginalItem')
                    );
                break;

                case 'distributionTransformer':
                    return this.getRelationRecordValue(
                        this.getModel().getFieldByName('distributionTransformer'),
                        this.getDistributionTransformerAutoComplete().autocomplete('getSelectedOriginalItem')
                    );
                break;

                case 'lowTensionPole':
                    return this.getRelationRecordValue(
                        this.getModel().getFieldByName('lowTensionPole'),
                        this.getLowTensionPoleAutoComplete().autocomplete('getSelectedOriginalItem')
                    );
                break;

                default:
                    return this.super('getFormFieldValue', [fieldName, fieldInputElement]);
                break;
            }
        },

        resetRelationFormField: function(fieldModelData, relationFormField) {

            this.super('resetRelationFormField', [fieldModelData, relationFormField]);

            switch(fieldModelData.name)
            {
                case 'powerTransformer':
                    relationFormField.data('injection-substation', null);
                break;

                case 'powerTransformerFeeder':
                    relationFormField.data('power-transformer', null);
                break;

                case 'distributionTransformerFeeder':
                    relationFormField.data('distribution-transformer', null);
                break;
            }
        },

        resetFormField: function(fieldName, inputElement) {

            switch(fieldName)
            {
                case 'customer':
                    this.getCustomerAutoComplete().autocomplete('reset');
                    this.super('resetFormField', ['injectionSubstation', inputElement]);
                break;

                case 'highTensionPole':
                    this.getHighTensionPoleAutoComplete().autocomplete('reset');
                break;

                case 'powerTransformer':
                    this.super('resetFormField', [fieldName, inputElement]);
                    this.super('resetFormField', ['powerTransformerFeeder', inputElement]);
                break;

                case 'distributionTransformer':
                    this.getDistributionTransformerAutoComplete().autocomplete('reset');
                    this.super('resetFormField', ['distributionTransformerFeeder', inputElement]);
                break;

                case 'lowTensionPole':
                    this.getLowTensionPoleAutoComplete().autocomplete('reset');
                break;

                default:
                    this.super('resetFormField', [fieldName, inputElement]);
                break;
            }
        },

        getRelationFieldSelectedData: function(fieldName) {

            switch(fieldName)
            {
                case 'customer':
                    return this.getCustomerAutoComplete().autocomplete('getSelectedOriginalItem');
                break;

                case 'highTensionPole':
                    return this.getHighTensionPoleAutoComplete().autocomplete('getSelectedOriginalItem');
                break;

                case 'distributionTransformer':
                    return this.getDistributionTransformerAutoComplete().autocomplete('getSelectedOriginalItem');
                break;

                case 'lowTensionPole':
                    return this.getLowTensionPoleAutoComplete().autocomplete('getSelectedOriginalItem');
                break;

                default:
                    return this.super('getFormFieldValue', [fieldName, fieldInputElement]);
                break;
            }
        },

        loadBusinessDistrictInjectionSubstations: function(businessDistrictId) {

            if(businessDistrictId) {

                var fieldModelData = this.getModel().getFieldByName('injectionSubstation');

                var parameters = {
                    businessDistrict: businessDistrictId
                };

                this.loadRelationFormFieldData(fieldModelData, parameters);
            }
        },

        loadInjectionSubstationPowerTransformers: function(injectionSubstationId) {

            if(injectionSubstationId) {

                var fieldModelData = this.getModel().getFieldByName('powerTransformer');

                var parameters = {
                    injectionSubstation: injectionSubstationId
                };

                this.loadRelationFormFieldData(fieldModelData, parameters);
            }
        },

        loadPowerTransformerOutputFeeders: function(powerTransformerId) {

            if(powerTransformerId) {

                var fieldModelData = this.getModel().getFieldByName('powerTransformerFeeder');

                var parameters = {
                    outputFeedersForPowerTransformer: powerTransformerId
                };

                this.loadRelationFormFieldData(fieldModelData, parameters);
            }
        },

        loadDistributionTransformerOutputFeeders: function(distributionTransformerId) {

            if(distributionTransformerId) {

                var fieldModelData = this.getModel().getFieldByName('distributionTransformerFeeder');

                var parameters = {
                    outputFeedersForDistributionTransformer: distributionTransformerId
                };

                this.loadRelationFormFieldData(fieldModelData, parameters);
            }
        },

        populateRelationFormField: function(fieldModelData, data, relationFormField) {

            this.super('populateRelationFormField', [fieldModelData, data, relationFormField]);

            if(fieldModelData.name === 'injectionSubstation') {

                var businessDistrictValue = this.getRelationFieldSelectedData('customer').businessDistrict.id;

                if(businessDistrictValue) {
                    this.getFormFieldByName(fieldModelData.name).data('business-district', businessDistrictValue);    
                }
            }
            else if(fieldModelData.name === 'powerTransformer') {

                var injectionSubstationValue = this.getFormFieldValue('injectionSubstation', this.getFormFieldByName('injectionSubstation'));

                if(injectionSubstationValue) {
                    this.getFormFieldByName(fieldModelData.name).data('injection-substation', injectionSubstationValue);    
                }
            }
            else if(fieldModelData.name === 'powerTransformerFeeder') {

                var powerTransformerValue = this.getFormFieldValue('powerTransformer', this.getFormFieldByName('powerTransformer'));

                if(powerTransformerValue) {
                    this.getFormFieldByName(fieldModelData.name).data('power-transformer', powerTransformerValue);    
                }
            }
            else if(fieldModelData.name === 'distributionTransformerFeeder') {

                var distributionTransformerValue = this.getFormFieldValue('distributionTransformer', this.getFormFieldByName('distributionTransformer'));

                this.getFormFieldByName(fieldModelData.name).data('distribution-transformer', distributionTransformerValue);
            }
        }
    }
});