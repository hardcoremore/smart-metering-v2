HCM.define({

    name: 'Modules.Form.EstimatedLoadForm',

    extendFrom: 'Modules.RelationFieldFormModule',

    construct: function(app, permissions) {

        this.callConstructor('Modules.RelationFieldFormModule', [app, permissions]);

        var customerAutoComplete;
        var estimatedLoadAppliaceModule = null;

        this.setCustomerAutoComplete = function(cac) {
            customerAutoComplete = cac;
        };

        this.getCustomerAutoComplete = function() {
            return customerAutoComplete;
        };

        this.setEstimatedLoadApplianceModule = function(module) {
            estimatedLoadAppliaceModule = module;
        };

        this.getEstimatedLoadApplianceModule = function() {
            return estimatedLoadAppliaceModule;
        };

        this.getModuleName = function() {
            return 'EstimatedLoadForm';
        };
    },

    methods: {

        initModule: function() {

            this.setNotAutoLoadingRelationFields(['customer']);
            this.super('initModule');
        },

        initController: function() {
            var formController = this.getApp().getController('Controllers.Form.EstimatedLoad', [this, this.getModel()]);
            this.setFormController(formController);
        },

        setControls: function() {

            this.super('setControls');

            var self = this;

            var customerFieldModelData = this.getModel().getFieldByName('customer');

            var customerAutoComplete = this.getFormFieldByName('customer').autocomplete({

                queryParamName: "q",
                valueParam: "id",
                labelParam: function(value) {
                    if(value) {
                        
                        var label = value.name + " (" + value.accountNumber + ")";

                        if(value.businessDistrict) {
                            label = label + ' - ' + value.businessDistrict.name                             
                        }

                        return label;
                    }
                },

                source: customerFieldModelData.relationModel.getUrl('/list-auto-complete')
            });

            this.setCustomerAutoComplete(customerAutoComplete);
            this.setRelationFormFieldAsInitialized(customerFieldModelData, customerAutoComplete);
        },

        stopModule: function() {

            if(this.getEstimatedLoadApplianceModule() && this.isEstimatedLoadApplianceModuleBlocked()) {
                this.unblockEstimatedLoadApplianceModule();
            } 

            this.unblockFormField('customerCategory');

            this.super('stopModule');
        },

        setFormFieldValue: function(fieldName, value, fieldInputElement) {

            switch(fieldName)
            {
                case 'customer':
                    this.getCustomerAutoComplete().autocomplete('setSelectedOriginalItem', value);
                break;

                default:
                    this.super('setFormFieldValue', [fieldName, value, fieldInputElement]);
                break;
            }
        },

        getFormFieldValue: function(fieldName, fieldInputElement) {

            switch(fieldName)
            {
                case 'customer':
                    return this.getRelationRecordValue(
                        this.getModel().getFieldByName('customer'),
                        this.getCustomerAutoComplete().autocomplete('getSelectedOriginalItem')
                    );
                break;

                default:
                    return this.super('getFormFieldValue', [fieldName, fieldInputElement]);
                break;
            }
        },

        resetFormField: function(fieldName, inputElement) {

            switch(fieldName)
            {
                case 'customer':
                    this.getCustomerAutoComplete().autocomplete('reset');
                break;

                default:
                    this.super('resetFormField', [fieldName, inputElement]);
                break;
            }
        },

        getRelationFieldSelectedData: function(fieldName) {

            switch(fieldName)
            {
                case 'customer':
                    return this.getCustomerAutoComplete().autocomplete('getSelectedOriginalItem');
                break;

                default:
                    return this.super('getRelationFieldSelectedData', [fieldName]);
                break;
            }
        },

        blockEstimatedLoadApplianceModule: function(message) {

            var moduleElement = this.getEstimatedLoadApplianceModule().getModuleElement();

            moduleElement.addClass('module-disabled');

            moduleElement.parent().tooltipster({
                content: message || "Please, first create the Estimated Load before adding appliances.",
                debug: false,
                position: 'top',
                autoClose: false,
                theme: 'tooltipster-default tooltipster-green tooltipster-big'
            });

            setTimeout(function(){
                moduleElement.parent().tooltipster('show');    
            }, 400);
            
        },

        unblockEstimatedLoadApplianceModule: function() {

            var moduleElement = this.getEstimatedLoadApplianceModule().getModuleElement();

            moduleElement.removeClass('module-disabled');

            if(moduleElement.parent().hasClass('tooltipstered')) {
                moduleElement.parent().tooltipster('destroy');
            }
        },

        isEstimatedLoadApplianceModuleBlocked: function() {
            return this.getEstimatedLoadApplianceModule().getModuleElement().hasClass('module-disabled');
        },

        blockCustomerCategoryField: function() {

            var self = this;

            setTimeout(function(){
                self.blockFormField('customerCategory', 'Please delete all appliances before changing customer category');
            }, 250);
        }
    }
});