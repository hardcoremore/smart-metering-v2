HCM.define({

    name: 'Modules.Form.PowerTransformer',

    extendFrom: 'Modules.RelationFieldFormModule',

    construct: function(app, permissions) {

        this.callConstructor('Modules.RelationFieldFormModule', [app, permissions]);

        var injectionSubstationAutoComplete;
        var outputFeedersInputWidget;


        this.setInjectionSubstationAutoComplete = function(isac) {
            injectionSubstationAutoComplete = isac;
        };

        this.getInjectionSubstationAutoComplete = function() {
            return injectionSubstationAutoComplete;
        };

        this.setOutputFeedersInputWidget = function(ofw) {
            outputFeedersInputWidget = ofw;
        };

        this.getOutputFeedersInputWidget = function() {
            return outputFeedersInputWidget;
        };

        this.getModuleName = function() {
            return 'PowerTransformerForm';
        };
    },

    methods: {

        initModule: function() {
            this.setNotAutoLoadingRelationFields(['inputFeeders', 'outputFeeders', 'injectionSubstation']);
            this.super('initModule');
        },

        setControls: function() {

            this.super('setControls');

            var self = this;

            var injectionSubstationFieldModelData = this.getModel().getFieldByName('injectionSubstation');

            var injectionSubstationAutoComplete = this.getFormFieldByName('injectionSubstation').autocomplete({

                queryParamName: "q",
                valueParam: "id",
                labelParam: "name",

                source: injectionSubstationFieldModelData.relationModel.getUrl('/list-auto-complete'),

                select: function(ev, ui) {
                    self.setIsFormChanged(true);
                }
            });

            this.setInjectionSubstationAutoComplete(injectionSubstationAutoComplete);
            this.setRelationFormFieldAsInitialized(injectionSubstationFieldModelData, injectionSubstationAutoComplete);

            var outputFeedersFieldModelData = this.getModel().getFieldByName('outputFeeders');

            var outputFeedersInputWidget = this.getModuleElement().find("#outputFeeders-PowerTransformer-widget").multiAutocomplete({

                allowNew: true,
                inputPlaceHolder: 'Type Feeder',
                autoCompleteOptions: {
                    source: outputFeedersFieldModelData.relationModel.getUrl('/list-auto-complete'),
                    ajaxQueryParams: {type: 'high-voltage'}
                },

                onItemAdd: function(ev, ui) {
                    self.setIsFormChanged(true);
                },

                onItemRemove: function(ev, ui) {
                    self.setIsFormChanged(true);
                }
            });

            this.setOutputFeedersInputWidget(outputFeedersInputWidget);
            this.setRelationFormFieldAsInitialized(outputFeedersFieldModelData, outputFeedersInputWidget);
        },

        setFormFieldValue: function(fieldName, value, fieldInput) {

            switch(fieldName)
            {
                case 'injectionSubstation':
                    this.getInjectionSubstationAutoComplete().autocomplete('setSelectedOriginalItem', value);
                break;

                case 'outputFeeders':
                    this.getOutputFeedersInputWidget().multiAutocomplete('option','data', value);
                break;

                default:
                    this.super('setFormFieldValue', [fieldName, value, fieldInput]);
                break;
            }
        },

        getFormFieldValue: function(fieldName, fieldInputElement) {

            switch(fieldName)
            {
                case 'injectionSubstation':
                    return this.getRelationRecordValue(
                        this.getModel().getFieldByName('injectionSubstation'),
                        this.getInjectionSubstationAutoComplete().autocomplete('getSelectedOriginalItem')
                    );
                break;

                case 'outputFeeders':
                    return this.getOutputFeedersInputWidget().multiAutocomplete('getSelectedValues');
                break;

                default:
                    return this.super('getFormFieldValue', [fieldName, fieldInputElement]);
                break;
            }
        },

        resetFormField: function(fieldName, inputElement) {

            switch(fieldName)
            {
                case 'injectionSubstation':
                    this.getInjectionSubstationAutoComplete().autocomplete('reset');
                break;

                case 'outputFeeders':
                    this.getOutputFeedersInputWidget().multiAutocomplete('removeAllItems');
                break;

                default:
                    this.super('resetFormField', [fieldName, inputElement]);
                break;
            }
        }
    }
});