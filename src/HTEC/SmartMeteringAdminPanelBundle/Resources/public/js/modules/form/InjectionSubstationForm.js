HCM.define({

    name: 'Modules.Form.InjectionSubstation',

    extendFrom: 'Modules.RelationFieldFormModule',

    construct: function(app, permissions) {

        this.callConstructor('Modules.RelationFieldFormModule', [app, permissions]);

        var inputFeedersInputWidget;

        this.setInputFeedersInputWidget = function(ifw) {
            inputFeedersInputWidget = ifw;
        };

        this.getInputFeedersInputWidget = function() {
            return inputFeedersInputWidget;
        };

        this.getModuleName = function() {
            return 'InjectionSubstationForm';
        };
    },

    methods: {

        initModule: function() {
            this.setNotAutoLoadingRelationFields(['inputFeeders']);
            this.super('initModule');
        },

        setControls: function() {

            this.super('setControls');

            var self = this;

            var inputFeedersFieldModelData = this.getModel().getFieldByName('inputFeeders');

            var inputFeedersInputWidget = this.getModuleElement().find("#inputFeeders-InjectionSubstation-widget").multiAutocomplete({

                inputName: 'inputFeeders',
                allowNew: true,
                inputPlaceHolder: 'Search Power Source Feeder...',
                autoCompleteOptions: {
                    source: inputFeedersFieldModelData.relationModel.getUrl('/list-auto-complete'),
                    ajaxQueryParams: {type: 'high-voltage'}
                },

                onItemAdd: function(ev, ui) {
                    self.setIsFormChanged(true);
                },

                onItemRemove: function(ev, ui) {
                    self.setIsFormChanged(true);
                }
            });

            this.setInputFeedersInputWidget(inputFeedersInputWidget);
            this.setRelationFormFieldAsInitialized(inputFeedersFieldModelData, inputFeedersInputWidget);
        },

        setFormFieldValue: function(fieldName, value, fieldInput) {

            switch(fieldName)
            {
                case 'inputFeeders':
                    this.getInputFeedersInputWidget().multiAutocomplete('option','data', value);
                break;

                default:
                    this.super('setFormFieldValue', [fieldName, value, fieldInput]);
                break;
            }
        },

        getFormFieldValue: function(fieldName, fieldInputElement) {

            switch(fieldName)
            {
                case 'inputFeeders':
                    return this.getInputFeedersInputWidget().multiAutocomplete('getSelectedValues');
                break;

                default:
                    return this.super('getFormFieldValue', [fieldName, fieldInputElement]);
                break;
            }
        },

        resetFormField: function(fieldName, inputElement) {

            switch(fieldName)
            {
                case 'inputFeeders':
                    this.getInputFeedersInputWidget().multiAutocomplete('removeAllItems');
                break;

                default:
                    this.super('resetFormField', [fieldName, inputElement]);
                break;
            }
        }
    }
});