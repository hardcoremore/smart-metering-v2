HCM.define({

    name: 'Modules.Form.Feeder',

    extendFrom: 'Modules.RelationFieldFormModule',

    construct: function(app, permissions) {

        // we are extending RelationFieldModule because of street coverage field
        // that might become tied to street
        this.callConstructor('Modules.RelationFieldFormModule', [app, permissions]);

        var streetCoverageInputWidget;

        this.setStreetCoverageInputWidget = function(scw) {
            streetCoverageInputWidget = scw;
        };

        this.getStreetCoverageInputWidget = function() {
            return streetCoverageInputWidget;
        };

        this.getModuleName = function() {
            return 'FeederForm';
        };
    },

    methods: {

        setControls: function() {

            this.super('setControls');

            var self = this;

            var streetCoverageInput = this.getModuleElement().find("#streetCoverage-Feeder-widget").multiAutocomplete({

                inputName: 'streetCoverage-autocomplete',
                allowNew: true,
                inputPlaceHolder: 'Type Address',

                onItemAdd: function(ev, ui) {
                    self.setIsFormChanged(true);
                },

                onItemRemove: function(ev, ui) {
                    self.setIsFormChanged(true);
                }
            });

            this.setStreetCoverageInputWidget(streetCoverageInput);
        },

        setFormFieldValue: function(fieldName, value, fieldInput) {
            if(fieldName === 'streetCoverage') {
                this.getStreetCoverageInputWidget().multiAutocomplete('option','data', value);
            }
            else {
                this.super('setFormFieldValue', [fieldName, value, fieldInput]);
            }
        },

        getFormFieldValue: function(fieldName, fieldInputElement) {

            if(fieldName === 'streetCoverage') {
                return this.getStreetCoverageInputWidget().multiAutocomplete('getSelectedItems');
            }
            else {
                return this.super('getFormFieldValue', [fieldName, fieldInputElement]);
            }
        },

        resetFormField: function(fieldName, inputElement) {

            if(fieldName === 'streetCoverage') {
                this.getStreetCoverageInputWidget().multiAutocomplete('removeAllItems');
            }
            else {
                this.super('resetFormField', [fieldName, inputElement]);
            }
        }
    }
});