HCM.define({

    name: 'Modules.Form.PowerPurchase',

    extendFrom: 'Modules.RelationFieldFormModule',

    construct: function(app, permissions) {

        this.callConstructor('Modules.RelationFieldFormModule', [app, permissions]);

        this.getModuleName = function() {
            return 'PowerPurchaseForm';
        };
    },

    methods: {

        initController: function() {
            var formController = this.getApp().getController('Controllers.Form.PowerPurchase', [this, this.getModel()]);
            this.setFormController(formController);
        },

        setControls: function() {

            this.super('setControls');

            this.initializeDateField('datePurchased');
        },

        setFormFieldValue: function(fieldName, value, fieldInputElement) {

            switch(fieldName)
            {
                case 'datePurchased':
                    fieldInputElement.datepick('setDate', moment(value.toString()).format('YYYY-MM-DD'));
                break;

                default:
                    this.super('setFormFieldValue', [fieldName, value, fieldInputElement]);
                break;
            }
        }
    }
});