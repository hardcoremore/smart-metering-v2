HCM.define({

    name: 'Modules.Form.City',

    extendFrom: 'Modules.FormModule',

    construct: function(app, permissions) {

        this.callConstructor('Modules.FormModule', [app, permissions]);

        this.getModuleName = function() {
            return 'CityForm';
        };
    }
});