HCM.define({

    name: 'Modules.Form.Filters.PowerScheduleFiltersForm',

    extendFrom: 'Modules.RelationFieldFormModule',

    construct: function(app, permissions) {

        this.callConstructor('Modules.RelationFieldFormModule', [app, permissions]);

        var powerScheduleModule;

        this.setPowerScheduleModule = function(module) {
            powerScheduleModule = module;
        };

        this.getPowerScheduleModule = function() {
            return powerScheduleModule;
        };

        this.getModuleName = function() {
            return 'PowerScheduleFiltersForm';
        };
    },

    methods: {

        initModule: function() {

            this.setNotAutoLoadingRelationFields([
                'injectionSubstation',
                'powerTransformer',
                'powerTransformerFeeder',
                'distributionTransformer',
                'distributionTransformerFeeder'
            ]);

            this.super('initModule');
        },

        initController: function() {
            var formController = this.getApp().getController('Controllers.Form.Filters.PowerSchedule', [this, this.getModel()]);
            this.setFormController(formController);
        },

        setControls: function() {

            this.super('setControls');
            this.initializeDateField('date');
        },

        getFormData: function(includeEmptyFields) {
            return this.super('getFormData', [false]);
        },

        submitFormRead: function(formData) {

            var startDate = this.getPowerScheduleModule().getStartDateForLoadingPowerSchedules(false, true);
            var endDate = this.getPowerScheduleModule().getEndDateForLoadingPowerSchedules(startDate);

            this.getPowerScheduleModule().readPowerSchedulesForDateRange(
                startDate.format('YYYY-MM-DD'),
                endDate.format('YYYY-MM-DD'),
                formData
            );
        },

        resetRelationFormField: function(fieldModelData, relationFormField) {

            this.super('resetRelationFormField', [fieldModelData, relationFormField]);

            switch(fieldModelData.name)
            {
                case 'injectionSubstation':
                    relationFormField.data('business-district', null);
                break;

                case 'powerTransformer':
                    relationFormField.data('injection-substation', null);
                break;

                case 'powerTransformerFeeder':
                    relationFormField.data('power-transformer', null);
                break;
            }
        },

        loadBusinessDistrictInjectionSubstations: function(businessDistrictId) {

            if(businessDistrictId) {

                var fieldModelData = this.getModel().getFieldByName('injectionSubstation');

                var parameters = {
                    businessDistrict: businessDistrictId
                };

                this.loadRelationFormFieldData(fieldModelData, parameters);
            }
        },

        loadInjectionSubstationPowerTransformers: function(injectionSubstationId) {

            if(injectionSubstationId) {

                var fieldModelData = this.getModel().getFieldByName('powerTransformer');

                var parameters = {
                    injectionSubstation: injectionSubstationId
                };

                this.loadRelationFormFieldData(fieldModelData, parameters);
            }
        },

        loadPowerTransformerOutputFeeders: function(powerTransformerId) {

            if(powerTransformerId) {

                var fieldModelData = this.getModel().getFieldByName('powerTransformerFeeder');

                var parameters = {
                    outputFeedersForPowerTransformer: powerTransformerId
                };

                this.loadRelationFormFieldData(fieldModelData, parameters);
            }
        },

        updateFormSubmitButtonLabel: function(formMode) {

        }
    }
});