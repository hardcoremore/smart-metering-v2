HCM.define({

    name: 'Modules.Form.EstimatedLoadApplianceForm',

    extendFrom: 'Modules.RelationFieldFormModule',

    construct: function(app, permissions) {

        this.callConstructor('Modules.RelationFieldFormModule', [app, permissions]);

        this.getModuleName = function() {
            return 'EstimatedLoadApplianceForm';
        };
    },

    methods: {

        initModule: function() {
            this.setNotAutoLoadingRelationFields(['estimatedLoad', 'appliance']);
            this.super('initModule');
        },

        initController: function() {
            var formController = this.getApp().getController('Controllers.Form.EstimatedLoadAppliance', [this, this.getModel()]);
            this.setFormController(formController);
        },

        setControls: function() {

            this.super('setControls');

            this.blockFormField('totalConsumption');
        },

        loadRelationFormFieldData: function(fieldModelData, parameters) {

            if(fieldModelData.name === 'applianceType') {

                var currentEstimatedLoad = this.getModel().getCurrentEstimatedLoad();

                if(currentEstimatedLoad) {

                    var customerCategory = currentEstimatedLoad.customerCategory;

                    this.super('loadRelationFormFieldData', [fieldModelData, {customerCategory:customerCategory.id}]);
                }
            }
            else {
                this.super('loadRelationFormFieldData', [fieldModelData, parameters]);
            }
        },

        isRelationFormFieldInitialized: function(fieldModelData, relationFormField) {

            var isInitialized = this.super('isRelationFormFieldInitialized', [fieldModelData, relationFormField]);

            if(isInitialized && this.getFormMode() === 'edit') {

                var editRecordData = this.getEditingRecordData();

                switch(fieldModelData.name) {

                     case 'applianceType':

                        if(this.getModel().getCurrentEstimatedLoad()) {
                            var dataLoadedForCustomerCategoryId = Number(relationFormField.data('customer-category'));
                            var currentCustomerCategoryId = this.getModel().getCurrentEstimatedLoad().customerCategory.id;
                            
                            return dataLoadedForCustomerCategoryId === currentCustomerCategoryId;    
                        }
                        else {
                            return false;
                        }

                    break;

                    case 'appliance':

                        var dataLoadedForApplianceTypeId = Number(relationFormField.data('appliance-type'));
                        var editRecordDataApplianceTypeId = this.getRelationRecordValue(
                            this.getModel().getFieldByName('applianceType'),
                            editRecordData['applianceType']
                        );
                        
                        return dataLoadedForApplianceTypeId === editRecordDataApplianceTypeId;

                    break;

                    default:
                        return isInitialized;
                    break;
                }
            }
            else {
                return isInitialized;
            }
        },

        setFormFieldValue: function(fieldName, value, fieldInputElement) {

            this.super('setFormFieldValue', [fieldName, value, fieldInputElement]);

            switch(fieldName)
            {
                case 'applianceType':

                    var applianceFormField = this.getFormFieldByName('powerTransformer');

                    if(value !== null && value !== undefined) {

                        var applianceTypeId = this.getRelationRecordValue(
                            this.getModel().getFieldByName('applianceType'),
                            value
                        );

                        if(Number(applianceFormField.data('appliance-type')) !== applianceTypeId) {
                            this.loadAppliancesForApplianceType(applianceTypeId);
                        }
                    }
                    else {
                        this.resetRelationFormField(
                            this.getModel().getFieldByName('appliance'),
                            applianceFormField
                        );
                    }

                break;
            }
        },

        resetRelationFormField: function(fieldModelData, relationFormField) {

            this.super('resetRelationFormField', [fieldModelData, relationFormField]);

            switch(fieldModelData.name)
            {
                case 'applianceType':
                    relationFormField.data('customer-category', null);
                break;

                case 'appliance':
                    relationFormField.data('appliance-type', null);
                break;
            }
        },

        resetFormField: function(fieldName, inputElement) {

            switch(fieldName)
            {
                case 'applianceType':
                    this.super('resetFormField', ['applianceType', inputElement]);
                break;

                default:
                    this.super('resetFormField', [fieldName, inputElement]);
                break;
            }
        },

        loadApplianceTypesForCustomerCategory: function(customerCategoryId) {

            if(customerCategoryId) {

                var fieldModelData = this.getModel().getFieldByName('applianceType');

                var parameters = {
                    customerCategory: customerCategoryId
                };

                this.loadRelationFormFieldData(fieldModelData, parameters);
            }
        },

        loadAppliancesForApplianceType: function(applianceTypeId) {

            if(applianceTypeId) {

                var fieldModelData = this.getModel().getFieldByName('appliance');

                var parameters = {
                    applianceType: applianceTypeId
                };

                this.loadRelationFormFieldData(fieldModelData, parameters);
            }
        },

        populateRelationFormField: function(fieldModelData, data, relationFormField) {

            this.super('populateRelationFormField', [fieldModelData, data, relationFormField]);

            if(fieldModelData.name === 'applianceType') {

                if(this.getModel().getCurrentEstimatedLoad()) {

                    var customerCategoryId = this.getModel().getCurrentEstimatedLoad().customerCategory.id;

                    if(customerCategoryId) {
                        this.getFormFieldByName(fieldModelData.name).data('customer-category', customerCategoryId);    
                    }
                }
            }
            else if(fieldModelData.name === 'appliance') {

                var applianceTypeValue = this.getFormFieldValue('applianceType', this.getFormFieldByName('applianceType'));

                if(applianceTypeValue) {
                    this.getFormFieldByName(fieldModelData.name).data('appliance-type', applianceTypeValue);    
                }
            }
        },

        submitFormCreate: function(formData) {
            formData.estimatedLoad = this.getModel().getCurrentEstimatedLoad().id;
            this.getModel().create(this.prepareDataForCreate(formData), {scope:this});
        },

        submitFormUpdate: function(recordId, formData) {
            formData.estimatedLoad = this.getModel().getCurrentEstimatedLoad().id;
            this.getModel().update(recordId, this.prepareDataForUpdate(formData, recordId), {scope:this});
        }
    }
});