HCM.define({

    name: 'Modules.Form.Meter',

    extendFrom: 'Modules.RelationFieldFormModule',

    construct: function(app, permissions) {

        this.callConstructor('Modules.RelationFieldFormModule', [app, permissions]);

        var customerAutoComplete;
        var distributionTransformerAutoComplete;

        this.setCustomerAutoComplete = function(cac) {
            customerAutoComplete = cac;
        };

        this.getCustomerAutoComplete = function() {
            return customerAutoComplete;
        };

        this.setDistributionTransformerAutoComplete = function(dtac) {
            distributionTransformerAutoComplete = dtac;
        };

        this.getDistributionTransformerAutoComplete = function() {
            return distributionTransformerAutoComplete;
        };

        this.getModuleName = function() {
            return 'MeterForm';
        };
    },

    methods: {

        initModule: function() {
            this.setNotAutoLoadingRelationFields(['customer', 'distributionTransformer', 'feeder']);
            this.super('initModule');
        },

        initController: function() {
            var formController = this.getApp().getController('Controllers.Form.Meter', [this, this.getModel()]);
            this.setFormController(formController);
        },

        setControls: function() {

            this.super('setControls');

            var self = this;

            var customerFieldModelData = this.getModel().getFieldByName('customer');

            var customerAutoComplete = this.getFormFieldByName('customer').autocomplete({

                queryParamName: "q",
                valueParam: "id",
                labelParam: "name",

                source: customerFieldModelData.relationModel.getUrl('/list-auto-complete')
            });

            this.setCustomerAutoComplete(customerAutoComplete);
            this.setRelationFormFieldAsInitialized(customerFieldModelData, customerAutoComplete);
            
            var distributionTransformerFieldModelData = this.getModel().getFieldByName('distributionTransformer');

            var distributionTransformerAutoComplete = this.getFormFieldByName('distributionTransformer').autocomplete({

                queryParamName: "q",
                valueParam: "id",
                labelParam: "name",

                source: distributionTransformerFieldModelData.relationModel.getUrl('/list-auto-complete')
            });

            this.setDistributionTransformerAutoComplete(distributionTransformerAutoComplete);
            this.setRelationFormFieldAsInitialized(distributionTransformerFieldModelData, distributionTransformerAutoComplete);
        },

        setFormFieldValue: function(fieldName, value, fieldInputElement) {

            switch(fieldName)
            {
                case 'customer':
                    this.getCustomerAutoComplete().autocomplete('setSelectedOriginalItem', value);
                break;

                case 'distributionTransformer':

                    this.getDistributionTransformerAutoComplete().autocomplete('setSelectedOriginalItem', value);

                    var feederFormField = this.getFormFieldByName('feeder');

                    if(value !== null && value !== undefined) {

                        var distributionTransformerId = this.getRelationRecordValue(
                            this.getModel().getFieldByName('distributionTransformer'),
                            value
                        );

                        if(Number(feederFormField.data('distribution-transformer')) !== distributionTransformerId) {
                            this.loadDistributionTransformerOutputFeeders(distributionTransformerId);
                        }
                    }
                    else {
                        this.resetRelationFormField(
                            this.getModel().getFieldByName('feeder'),
                            feederFormField
                        );
                    }
                    
                break;

                case 'feeder':
                default:
                    this.super('setFormFieldValue', [fieldName, value, fieldInputElement]);
                break;
            }
        },

        getFormFieldValue: function(fieldName, fieldInputElement) {

            switch(fieldName)
            {
                case 'customer':
                    
                    return this.getRelationRecordValue(
                        this.getModel().getFieldByName('customer'),
                        this.getCustomerAutoComplete().autocomplete('getSelectedOriginalItem')
                    );

                break;

                case 'distributionTransformer':

                    return this.getRelationRecordValue(
                        this.getModel().getFieldByName('distributionTransformer'),
                        this.getDistributionTransformerAutoComplete().autocomplete('getSelectedOriginalItem')
                    );

                break;

                case 'feeder':
                default:
                    return this.super('getFormFieldValue', [fieldName, fieldInputElement]);
                break;
            }
        },

        resetRelationFormField: function(fieldModelData, relationFormField) {

            this.super('resetRelationFormField', [fieldModelData, relationFormField]);

            switch(fieldModelData.name)
            {
                case 'feeder':
                    relationFormField.data('distribution-transformer', null);
                break;
            }
        },

        resetFormField: function(fieldName, inputElement) {

            switch(fieldName)
            {
                case 'customer':
                    this.getCustomerAutoComplete().autocomplete('reset');
                break;

                case 'distributionTransformer':
                    this.getDistributionTransformerAutoComplete().autocomplete('reset');
                    this.super('resetFormField', ['feeder', inputElement]);
                break;

                default:
                    this.super('resetFormField', [fieldName, inputElement]);
                break;
            }
        },

        loadDistributionTransformerOutputFeeders: function(distributionTransformerId) {

            var fieldModelData = this.getModel().getFieldByName('feeder');

            var parameters = {
                outputFeedersForDistributionTransformer: distributionTransformerId
            };

            this.loadRelationFormFieldData(fieldModelData, parameters);
        },

        populateRelationFormField: function(fieldModelData, data, relationFormField) {

            this.super('populateRelationFormField', [fieldModelData, data, relationFormField]);

            var distributionTransformerValue = this.getFormFieldValue('distributionTransformer', this.getFormFieldByName('distributionTransformer'));

            if(fieldModelData.name === 'feeder' && distributionTransformerValue) {
                this.getFormFieldByName(fieldModelData.name).data('distribution-transformer', distributionTransformerValue);
            }
        }
    }
});