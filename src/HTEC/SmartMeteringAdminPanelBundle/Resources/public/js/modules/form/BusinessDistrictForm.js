HCM.define({

    name: 'Modules.Form.BusinessDistrict',

    extendFrom: 'Modules.RelationFieldFormModule',

    construct: function(app, permissions) {

        this.callConstructor('Modules.RelationFieldFormModule', [app, permissions]);

        this.getModuleName = function() {
            return 'BusinessDistrictForm';
        };
    }
});