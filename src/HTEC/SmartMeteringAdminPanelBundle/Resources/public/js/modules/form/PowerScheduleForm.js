HCM.define({

    name: 'Modules.Form.PowerScheduleForm',

    extendFrom: 'Modules.RelationFieldFormModule',

    construct: function(app, permissions) {

        this.callConstructor('Modules.RelationFieldFormModule', [app, permissions]);

        var distributionTransformerAutoComplete;
        var powerScheduleForWeekScheduler;

        var schedulerHelper;

        this.setDistributionTransformerAutoComplete = function(dtac) {
            distributionTransformerAutoComplete = dtac;
        };

        this.getDistributionTransformerAutoComplete = function() {
            return distributionTransformerAutoComplete;
        };

        this.setPowerScheduleForWeekScheduler = function(pfws) {
            powerScheduleForWeekScheduler = pfws;
        };

        this.getPowerScheduleForWeekScheduler = function() {
            return powerScheduleForWeekScheduler;
        };

        this.setSchedulerHelper = function(helper) {
            schedulerHelper = helper;
        };

        this.getSchedulerHelper = function() {
            return schedulerHelper;
        };

        this.getModuleName = function() {
            return 'PowerScheduleForm';
        };
    },

    methods: {

        initModule: function() {

            this.setNotAutoLoadingRelationFields([
                'injectionSubstation',
                'powerTransformer',
                'powerTransformerFeeder',
                'distributionTransformer',
                'distributionTransformerFeeder'
            ]);

            this.setSchedulerHelper(this.getInstance('Helpers.PowerScheduleScheduler', [this.getApp()]));
            this.getSchedulerHelper().setModel(this.getModel());

            this.super('initModule');
        },

        initController: function() {
            var formController = this.getApp().getController('Controllers.Form.PowerSchedule', [this, this.getModel()]);
            this.setFormController(formController);
        },

        setControls: function() {

            this.super('setControls');

            var self = this;

            var distributionTransformerFieldModelData = this.getModel().getFieldByName('distributionTransformer');
            var distributionTransformerAutoComplete = this.getFormFieldByName('distributionTransformer').autocomplete({

                queryParamName: "q",
                valueParam: "id",
                labelParam: function(value) {
                    if(value) {

                        var label = value.name;

                        if(value.code) {
                            label += " (" + value.code + ")"
                        }

                        return label;
                    }
                },

                source: distributionTransformerFieldModelData.relationModel.getUrl('/list-auto-complete')
            });

            this.setDistributionTransformerAutoComplete(distributionTransformerAutoComplete);
            this.setRelationFormFieldAsInitialized(distributionTransformerFieldModelData, distributionTransformerAutoComplete);

            this.initializeDateField('date');
            this.initializeTimeField('startTime');
            this.initializeTimeField('endTime');
            this.initializeDateField('repeatEnd');

            var powerScheduleForWeekScheduler = this.getModuleElement().find('#powerSchedules-PowerScheduleForm-scheduler').scheduler({
                allowIntersectingEvents: true,
                createEventTooltipCallback: this.getSchedulerHelper().createTooltipForSchedulerEvent.bind(this.getSchedulerHelper())
            });

            this.setPowerScheduleForWeekScheduler(powerScheduleForWeekScheduler);
        },

        stopModule: function() {
            this.getPowerScheduleForWeekScheduler().scheduler('removeAllEvents');
            this.super('stopModule');
        },

        getStartDateForLoadingPowerSchedules: function(date) {

            var currentStartDate = date || this.getFormFieldValue('date');

            if(currentStartDate) {

                currentStartDate = moment(currentStartDate);
                currentStartDate.subtract(currentStartDate.day() - 1, 'days');

                return currentStartDate;
            }
            else {
                return null;
            }
        },

        getEndDateForLoadingPowerSchedules: function(startDate) {
            return startDate.clone().add(6, 'days');
        },

        getFiltersForLoadingPowerSchedules: function(recrodData) {

            var filters = recrodData || this.getFormData();

            delete filters.powerType;
            delete filters.powerSource;
            delete filters.repeatType;
            delete filters.repeatEnd;
            delete filters.date;
            delete filters.startTime;
            delete filters.endTime;

            var filterKeys = Object.keys(filters);

            var filterKey;

            for(var i in filterKeys) {

                filterKey = filterKeys[i];

                if(filters[filterKey] === null || filters[filterKey].toString().length === 0) {
                    delete filters[filterKey];
                }
            }

            return filters;
        },

        getFiltersFromPowerSchedule: function(powerSchedule, filters) {

            var filters = filters || {};

            if(powerSchedule.businessDistrict) {
                filters.businessDistrict = powerSchedule.businessDistrict.id;
            }

            if(powerSchedule.injectionSubstation) {
                filters.injectionSubstation = powerSchedule.injectionSubstation.id;
            }

            if(powerSchedule.powerTransformer) {
                filters.powerTransformer = powerSchedule.powerTransformer.id;
            }

            if(powerSchedule.powerTransformerFeeder) {
                filters.powerTransformerFeeder = powerSchedule.powerTransformerFeeder.id;
            }

            if(powerSchedule.distributionTransformer) {
                filters.distributionTransformer = powerSchedule.distributionTransformer.id;
            }

            if(powerSchedule.distributionTransformerFeeder) {
                filters.distributionTransformerFeeder = powerSchedule.distributionTransformerFeeder.id;
            }

            return filters;
        },

        readPowerSchedulesForDateRange: function(start, end, searchParameters) {

            this.getModel().readAll({

                parameters: $.extend({}, {
                    start: start,
                    end: end
                }, searchParameters),

                scope: this
            });
        },

        isRelationFormFieldInitialized: function(fieldModelData, relationFormField) {

            var isInitialized = this.super('isRelationFormFieldInitialized', [fieldModelData, relationFormField]);

            if(isInitialized && this.getFormMode() === 'edit') {

                var editRecordData = this.getEditingRecordData();

                switch(fieldModelData.name) {

                    case 'injectionSubstation':

                        var dataLoadedForBusinessDistrictId = Number(relationFormField.data('business-district'));
                        var editRecordDataBusinessDistrictId = this.getRelationRecordValue(
                            this.getModel().getFieldByName('businessDistrict'),
                            editRecordData['businessDistrict']
                        );

                        return dataLoadedForBusinessDistrictId === editRecordDataBusinessDistrictId;

                    break;

                    case 'powerTransformer':

                        var dataLoadedForInjectionSubstationId = Number(relationFormField.data('injection-substation'));
                        var editRecordDataInjectionSubstationId = this.getRelationRecordValue(
                            this.getModel().getFieldByName('injectionSubstation'),
                            editRecordData['injectionSubstation']
                        );
                        
                        return dataLoadedForInjectionSubstationId === editRecordDataInjectionSubstationId;

                    break;

                    case 'powerTransformerFeeder':

                        var dataLoadedForPowerTransformerId = Number(relationFormField.data('power-transformer'));
                        var editRecordDataPowerTransformerId = this.getRelationRecordValue(
                            this.getModel().getFieldByName('powerTransformer'),
                            editRecordData['powerTransformer']
                        );

                        return dataLoadedForPowerTransformerId === editRecordDataPowerTransformerId;

                    break;

                    case 'distributionTransformerFeeder':

                        var dataLoadedForDistributionTransformerId = Number(relationFormField.data('distribution-transformer'));
                        var editRecordDataDistributionTransformerId = this.getRelationRecordValue(
                            this.getModel().getFieldByName('distributionTransformer'),
                            editRecordData['distributionTransformer']
                        );

                        return dataLoadedForDistributionTransformerId === editRecordDataDistributionTransformerId;

                    break;

                    default:
                        return isInitialized;
                    break;
                }
            }
            else {
                return isInitialized;
            }
        },

        setFormFieldValue: function(fieldName, value, fieldInputElement) {

            switch(fieldName)
            {
                case 'repeatEnd':
                case 'date':

                    if(value) {
                        value = value.toString();
                        fieldInputElement.datepick('setDate', moment(value).format('YYYY-MM-DD'));
                    }
                    else {
                        fieldInputElement.val('');
                    }

                break;

                case 'startTime':
                    this.super('setFormFieldValue', [fieldName, moment(value.toString()).format('HH:mm'), fieldInputElement])
                break;

                case 'endTime':
                    this.super('setFormFieldValue', [fieldName, moment(value.toString()).format('HH:mm'), fieldInputElement])
                break;

                case 'powerType':
                case 'powerSource':
                case 'businessDistrict':
                case 'injectionSubstation':
                case 'powerTransformer':
                case 'distributionTransformerFeeder':
                case 'repeatType':
                default:
                    this.super('setFormFieldValue', [fieldName, value, fieldInputElement]);
                break;

            }

            switch(fieldName)
            {
                case 'businessDistrict':

                    var injectionSubstationFormField = this.getFormFieldByName('injectionSubstation');
                    var businessDistrictId = this.getRelationRecordValue(
                        this.getModel().getFieldByName('businessDistrict'),
                        value
                    );

                    if(Number(injectionSubstationFormField.data('business-district')) !== businessDistrictId) {
                        this.loadBusinessDistrictInjectionSubstations(businessDistrictId);
                    }

                break;

                case 'injectionSubstation':

                    var powerTransformerFormField = this.getFormFieldByName('powerTransformer');

                    if(value !== null && value !== undefined) {
                        var injectionSubstationId = this.getRelationRecordValue(
                            this.getModel().getFieldByName('injectionSubstation'),
                            value
                        );

                        if(Number(powerTransformerFormField.data('injection-substation')) !== injectionSubstationId) {
                            this.loadInjectionSubstationPowerTransformers(injectionSubstationId);
                        }
                    }
                    else {
                        this.resetRelationFormField(
                            this.getModel().getFieldByName('powerTransformer'),
                            powerTransformerFormField
                        );
                    }

                break;

                case 'powerTransformer':

                    var powerTransformerFeederFormField = this.getFormFieldByName('powerTransformerFeeder');

                    if(value !== null && value !== undefined) {

                        var powerTransformerId = this.getRelationRecordValue(
                            this.getModel().getFieldByName('powerTransformer'),
                            value
                        );

                        if(Number(powerTransformerFeederFormField.data('power-transformer')) !== powerTransformerId) {
                            this.loadPowerTransformerOutputFeeders(powerTransformerId);
                        }
                    }
                    else {
                        this.resetRelationFormField(
                            this.getModel().getFieldByName('powerTransformerFeeder'),
                            powerTransformerFeederFormField
                        );
                    }

                break;

                case 'distributionTransformer':

                    this.getDistributionTransformerAutoComplete().autocomplete('setSelectedOriginalItem', value);

                    var distributionTransformerFeederFormField = this.getFormFieldByName('distributionTransformerFeeder');

                    if(value !== null && value !== undefined) {
                        
                        var distributionTransformerId = this.getRelationRecordValue(
                            this.getModel().getFieldByName('distributionTransformer'),
                            value
                        );

                        if(Number(distributionTransformerFeederFormField.data('distribution-transformer')) !== distributionTransformerId) {
                            this.loadDistributionTransformerOutputFeeders(distributionTransformerId);
                        }
                    }
                    else  {
                        this.resetRelationFormField(
                            this.getModel().getFieldByName('distributionTransformerFeeder'),
                            distributionTransformerFeederFormField
                        );
                    }
                    
                break;
            }
        },

        getFormFieldValue: function(fieldName, fieldInputElement) {

            switch(fieldName)
            {
                case 'distributionTransformer':
                    return this.getRelationRecordValue(
                        this.getModel().getFieldByName('distributionTransformer'),
                        this.getDistributionTransformerAutoComplete().autocomplete('getSelectedOriginalItem')
                    );
                break;

                default:
                    return this.super('getFormFieldValue', [fieldName, fieldInputElement]);
                break;
            }
        },

        resetRelationFormField: function(fieldModelData, relationFormField) {

            this.super('resetRelationFormField', [fieldModelData, relationFormField]);

            switch(fieldModelData.name)
            {
                case 'injectionSubstation':
                    relationFormField.data('business-district', null);
                break;

                case 'powerTransformer':
                    relationFormField.data('injection-substation', null);
                break;

                case 'powerTransformerFeeder':
                    relationFormField.data('power-transformer', null);
                break;

                case 'distributionTransformerFeeder':
                    relationFormField.data('distribution-transformer', null);
                break;
            }
        },

        resetFormField: function(fieldName, inputElement) {

            switch(fieldName)
            {
                case 'businessDistrict':
                    this.super('resetFormField', [fieldName, inputElement]);
                    this.resetRelationFormField(
                        this.getModel().getFieldByName('injectionSubstation'),
                        this.getFormFieldByName('injectionSubstation')
                    );
                break;

                case 'injectionSubstation':
                    this.super('resetFormField', [fieldName, inputElement]);
                    this.resetRelationFormField(
                        this.getModel().getFieldByName('powerTransformer'),
                        this.getFormFieldByName('powerTransformer')
                    );
                break;

                case 'powerTransformer':
                    this.super('resetFormField', [fieldName, inputElement]);
                    this.resetRelationFormField(
                        this.getModel().getFieldByName('powerTransformerFeeder'),
                        this.getFormFieldByName('powerTransformerFeeder')
                    );
                break;

                case 'distributionTransformer':
                    this.getDistributionTransformerAutoComplete().autocomplete('reset');
                    this.super('resetFormField', ['distributionTransformerFeeder', inputElement]);
                break;

                default:
                    this.super('resetFormField', [fieldName, inputElement]);
                break;
            }
        },

        getRelationFieldSelectedData: function(fieldName) {

            switch(fieldName)
            {
                case 'distributionTransformer':
                    return this.getDistributionTransformerAutoComplete().autocomplete('getSelectedOriginalItem');
                break;

                default:
                    return this.super('getFormFieldValue', [fieldName, fieldInputElement]);
                break;
            }
        },

        loadBusinessDistrictInjectionSubstations: function(businessDistrictId) {

            if(businessDistrictId) {

                var fieldModelData = this.getModel().getFieldByName('injectionSubstation');

                var parameters = {
                    businessDistrict: businessDistrictId
                };

                this.loadRelationFormFieldData(fieldModelData, parameters);
            }
        },

        loadInjectionSubstationPowerTransformers: function(injectionSubstationId) {

            if(injectionSubstationId) {

                var fieldModelData = this.getModel().getFieldByName('powerTransformer');

                var parameters = {
                    injectionSubstation: injectionSubstationId
                };

                this.loadRelationFormFieldData(fieldModelData, parameters);
            }
        },

        loadPowerTransformerOutputFeeders: function(powerTransformerId) {

            if(powerTransformerId) {

                var fieldModelData = this.getModel().getFieldByName('powerTransformerFeeder');

                var parameters = {
                    outputFeedersForPowerTransformer: powerTransformerId
                };

                this.loadRelationFormFieldData(fieldModelData, parameters);
            }
        },

        loadDistributionTransformerOutputFeeders: function(distributionTransformerId) {

            if(distributionTransformerId) {

                var fieldModelData = this.getModel().getFieldByName('distributionTransformerFeeder');

                var parameters = {
                    outputFeedersForDistributionTransformer: distributionTransformerId
                };

                this.loadRelationFormFieldData(fieldModelData, parameters);
            }
        },

        populateRelationFormField: function(fieldModelData, data, relationFormField) {

            this.super('populateRelationFormField', [fieldModelData, data, relationFormField]);

            if(fieldModelData.name === 'injectionSubstation') {

                var businessDistrictValue = this.getFormFieldValue('businessDistrict', this.getFormFieldByName('businessDistrict'));

                if(businessDistrictValue) {
                    relationFormField.data('business-district', businessDistrictValue);
                }
            }
            else if(fieldModelData.name === 'powerTransformer') {

                var injectionSubstationValue = this.getFormFieldValue('injectionSubstation', this.getFormFieldByName('injectionSubstation'));

                if(injectionSubstationValue) {
                    relationFormField.data('injection-substation', injectionSubstationValue);    
                }
            }
            else if(fieldModelData.name === 'powerTransformerFeeder') {

                var powerTransformerValue = this.getFormFieldValue('powerTransformer', this.getFormFieldByName('powerTransformer'));

                if(powerTransformerValue) {
                    relationFormField.data('power-transformer', powerTransformerValue);
                }
            }
            else if(fieldModelData.name === 'distributionTransformerFeeder') {

                var distributionTransformerValue = this.getFormFieldValue('distributionTransformer', this.getFormFieldByName('distributionTransformer'));

                relationFormField.data('distribution-transformer', distributionTransformerValue);
            }
        }
    }
});