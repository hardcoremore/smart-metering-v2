HCM.define({

    name: 'Modules.Form.FeederLoadForm',

    extendFrom: 'Modules.RelationFieldFormModule',

    construct: function(app, permissions) {

        this.callConstructor('Modules.RelationFieldFormModule', [app, permissions]);

        this.getModuleName = function() {
            return 'FeederLoadForm';
        };
    },

    methods: {

        initModule: function() {

            this.setNotAutoLoadingRelationFields([
                'injectionSubstation',
                'powerTransformer',
                'powerTransformerFeeder'
            ]);

            this.super('initModule');
        },

        initController: function() {
            var controller = this.getApp().getController('Controllers.Form.FeederLoad', [this, this.getModel()]);
            this.setFormController(controller);
        },

        setControls: function() {

            this.super('setControls');

            var self = this;

            this.initializeDateField('date').datepick('setDate', moment().format('YYYY-MM-DD'));
            
            var field = this.getFormFieldByName('time').datetimepicker({
                dateFormat: 'yyyy-mm-dd',
                timeOnly: true,
                showMinute: false,
                onSelect: function() {
                    field.trigger('input')
                }
            });
        },

        getRelationRecordLabel: function(fieldModelData, recordData) {

            var label = this.super('getRelationRecordLabel', [fieldModelData, recordData]);

            if(fieldModelData.name === 'powerTransformerFeeder') {
                label = label + ' (' + recordData.status + ')';
            }

            return label;
        },

        submitFormCreate: function(formData) {

            delete formData.businessDistrict;
            delete formData.injectionSubstation;
            delete formData.powerTransformer;

            this.getModel().create(this.prepareDataForCreate(formData), {scope:this});
        },

        submitFormUpdate: function(recordId, formData) {
            
            delete formData.businessDistrict;
            delete formData.injectionSubstation;
            delete formData.powerTransformer;

            this.getModel().update(recordId, this.prepareDataForUpdate(formData, recordId), {scope:this});
        },

        isRelationFormFieldInitialized: function(fieldModelData, relationFormField) {

            var isInitialized = this.super('isRelationFormFieldInitialized', [fieldModelData, relationFormField]);

            if(isInitialized && this.getFormMode() === 'edit') {

                var editRecordData = this.getEditingRecordData();

                switch(fieldModelData.name) {

                    case 'injectionSubstation':

                        var dataLoadedForBusinessDistrictId = Number(relationFormField.data('business-district'));
                        var editRecordDataBusinessDistrictId = this.getRelationRecordValue(
                            this.getModel().getFieldByName('businessDistrict'),
                            editRecordData['businessDistrict']
                        );

                        return dataLoadedForBusinessDistrictId === editRecordDataBusinessDistrictId;

                    break;

                    case 'powerTransformer':

                        var dataLoadedForInjectionSubstationId = Number(relationFormField.data('injection-substation'));
                        var editRecordDataInjectionSubstationId = this.getRelationRecordValue(
                            this.getModel().getFieldByName('injectionSubstation'),
                            editRecordData['injectionSubstation']
                        );
                        
                        return dataLoadedForInjectionSubstationId === editRecordDataInjectionSubstationId;

                    break;

                    default:
                        return isInitialized;
                    break;
                }
            }
            else {
                return isInitialized;
            }
        },

        setFormFieldValue: function(fieldName, value, fieldInputElement) {

            switch(fieldName)
            {
                case 'type':
                case 'businessDistrict':
                case 'injectionSubstation':
                case 'powerTransformer':
                case 'feederLoad':
                    this.super('setFormFieldValue', [fieldName, value, fieldInputElement]);
                break;

                case 'date':
                    fieldInputElement.datepick('setDate', moment(value.toString()).format('YYYY-MM-DD'));
                break;

                case 'time':
                    this.super('setFormFieldValue', [fieldName, moment(value.toString()).format('HH:mm'), fieldInputElement])
                break;
            }

            switch(fieldName)
            {
                case 'businessDistrict':

                    var injectionSubstationFormField = this.getFormFieldByName('injectionSubstation');
                    var businessDistrictId = this.getRelationRecordValue(
                        this.getModel().getFieldByName('businessDistrict'),
                        value
                    );

                    if(injectionSubstationFormField.data('business-district') !== businessDistrictId) {
                        this.loadBusinessDistrictInjectionSubstations(businessDistrictId);
                    }

                break;

                case 'injectionSubstation':

                    var powerTransformerFormField = this.getFormFieldByName('powerTransformer');

                    if(value !== null && value !== undefined) {
                        var injectionSubstationId = this.getRelationRecordValue(
                            this.getModel().getFieldByName('injectionSubstation'),
                            value
                        );

                        if(Number(powerTransformerFormField.data('injection-substation')) !== injectionSubstationId) {
                            this.loadInjectionSubstationPowerTransformers(injectionSubstationId);
                        }
                    }
                    else {
                        this.resetRelationFormField(
                            this.getModel().getFieldByName('powerTransformer'),
                            powerTransformerFormField
                        );
                    }

                break;

                case 'powerTransformer':

                    var powerTransformerFeederFormField = this.getFormFieldByName('powerTransformerFeeder');

                    if(value !== null && value !== undefined) {

                        var powerTransformerId = this.getRelationRecordValue(
                            this.getModel().getFieldByName('powerTransformer'),
                            value
                        );

                        if(Number(powerTransformerFeederFormField.data('power-transformer')) !== powerTransformerId) {
                            this.loadPowerTransformerOutputFeeders(powerTransformerId);
                        }
                    }
                    else {
                        this.resetRelationFormField(
                            this.getModel().getFieldByName('powerTransformerFeeder'),
                            powerTransformerFeederFormField
                        );
                    }

                break;
            }
        },

        resetRelationFormField: function(fieldModelData, relationFormField) {

            this.super('resetRelationFormField', [fieldModelData, relationFormField]);

            switch(fieldModelData.name)
            {
                case 'powerTransformer':
                    relationFormField.data('injection-substation', null);
                break;
            }
        },

        resetFormField: function(fieldName, inputElement) {

            switch(fieldName)
            {
                case 'businessDistrict':
                    this.super('resetFormField', [fieldName, inputElement]);
                    this.super('resetFormField', ['injectionSubstation', inputElement]);
                break;

                case 'powerTransformer':
                    this.super('resetFormField', [fieldName, inputElement]);
                    this.super('resetFormField', ['powerTransformerFeeder', inputElement]);
                break;

                case 'time':
                    this.unblockFormField(fieldName);
                    this.super('resetFormField', [fieldName, inputElement]);
                break;
                default:
                    this.super('resetFormField', [fieldName, inputElement]);
                break;
            }
        },

        loadBusinessDistrictInjectionSubstations: function(businessDistrictId) {

            if(businessDistrictId) {

                var fieldModelData = this.getModel().getFieldByName('injectionSubstation');

                var parameters = {
                    businessDistrict: businessDistrictId
                };

                this.loadRelationFormFieldData(fieldModelData, parameters);
            }
        },

        loadInjectionSubstationPowerTransformers: function(injectionSubstationId) {

            if(injectionSubstationId) {

                var fieldModelData = this.getModel().getFieldByName('powerTransformer');

                var parameters = {
                    injectionSubstation: injectionSubstationId
                };

                this.loadRelationFormFieldData(fieldModelData, parameters);
            }
        },


        loadPowerTransformerOutputFeeders: function(powerTransformerId) {

            if(powerTransformerId) {

                var fieldModelData = this.getModel().getFieldByName('powerTransformerFeeder');

                var parameters = {
                    outputFeedersForPowerTransformer: powerTransformerId
                };

                this.loadRelationFormFieldData(fieldModelData, parameters);
            }
        },

        populateRelationFormField: function(fieldModelData, data, relationFormField) {

            this.super('populateRelationFormField', [fieldModelData, data, relationFormField]);

            if(fieldModelData.name === 'injectionSubstation') {

                var businessDistrictValue = this.getFormFieldValue('businessDistrict', this.getFormFieldByName('businessDistrict'));

                if(businessDistrictValue) {
                    relationFormField.data('business-district', businessDistrictValue);    
                }
            }
            else if(fieldModelData.name === 'powerTransformer') {

                var injectionSubstationValue = this.getFormFieldValue('injectionSubstation', this.getFormFieldByName('injectionSubstation'));

                if(injectionSubstationValue) {
                    relationFormField.data('injection-substation', injectionSubstationValue);    
                }
            }
        }
    }
});