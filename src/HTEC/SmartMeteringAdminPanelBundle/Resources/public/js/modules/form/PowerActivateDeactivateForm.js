HCM.define({

    name: 'Modules.Form.PowerActivateDeactivateForm',

    extendFrom: 'Modules.RelationFieldFormModule',

    construct: function(app, permissions) {

        this.callConstructor('Modules.RelationFieldFormModule', [app, permissions]);

        var powerTransformerFeedersGrid;
        var searchFeedersButton;

        this.setPowerTransformerFeedersGrid = function(grid) {
            powerTransformerFeedersGrid = grid;            
        };

        this.getPowerTransformerFeedersGrid = function() {
            return powerTransformerFeedersGrid;
        };

        this.setSearchFeedersButton = function(button) {
            searchFeedersButton = button;
        };

        this.getSearchFeedersButton = function() {
            return searchFeedersButton;
        };

        this.getModuleName = function() {
            return 'PowerActivateDeactivateForm';
        };
    },

    methods: {

        initModule: function() {

            this.setNotAutoLoadingRelationFields([
                'injectionSubstation',
                'powerTransformer',
                'powerTransformerFeeder',
                'distributionTransformer',
                'distributionTransformerFeeder'
            ]);

            this.super('initModule');
        },

        initController: function() {
            var formController = this.getApp().getController('Controllers.Form.PowerActivateDeactivate', [this, this.getModel()]);
            this.setFormController(formController);
        },

        setControls: function() {

            this.super('setControls');

            var self = this;

            var powerTransformerFeedersGrid = this.getModuleElement().find("#powerTransformerFeeders-PowerActivateDeactivateForm-grid").grid({

                autoLoad: false,
                cellEdit: false,
                localSort: true,
                multiselect: true,
                model: [
                    {
                        header: 'ID',
                        id: 'id',
                        dataMap: 'id'
                    },
                    {
                        header: 'Feeder',
                        id: 'name',
                        dataMap: 'name'
                    },
                    {
                        header: 'Status',
                        id: 'status',
                        dataMap: 'status',
                        formatter: function(cellValue) {
                            if(cellValue === 'live') {
                                return '<span class="feeder-live">Live</span>';
                            }
                            else {
                                return '<span class="feeder-offline">Offline</span>';   
                            }
                        }
                    }
                ]
            });

            this.setPowerTransformerFeedersGrid(powerTransformerFeedersGrid);

            this.setSearchFeedersButton(this.getModuleElement().find('.search-feeders-button'));
        },

        getRelationRecordLabel: function(fieldModelData, recordData) {

            var label = this.super('getRelationRecordLabel', [fieldModelData, recordData]);

            if(fieldModelData.name === 'powerTransformerFeeder') {
                label = label + ' (' + recordData.status + ')';
            }

            return label;
        },

        isRelationFormFieldInitialized: function(fieldModelData, relationFormField) {

            var isInitialized = this.super('isRelationFormFieldInitialized', [fieldModelData, relationFormField]);

            if(isInitialized && this.getFormMode() === 'edit') {

                var editRecordData = this.getEditingRecordData();

                switch(fieldModelData.name) {

                    case 'injectionSubstation':

                        var dataLoadedForBusinessDistrictId = Number(relationFormField.data('business-district'));
                        var editRecordDataBusinessDistrictId = this.getRelationRecordValue(
                            this.getModel().getFieldByName('businessDistrict'),
                            editRecordData['businessDistrict']
                        );

                        return dataLoadedForBusinessDistrictId === editRecordDataBusinessDistrictId;

                    break;

                    case 'powerTransformer':

                        var dataLoadedForInjectionSubstationId = Number(relationFormField.data('injection-substation'));
                        var editRecordDataInjectionSubstationId = this.getRelationRecordValue(
                            this.getModel().getFieldByName('injectionSubstation'),
                            editRecordData['injectionSubstation']
                        );
                        
                        return dataLoadedForInjectionSubstationId === editRecordDataInjectionSubstationId;

                    break;

                    default:
                        return isInitialized;
                    break;
                }
            }
            else {
                return isInitialized;
            }
        },

        setFormFieldValue: function(fieldName, value, fieldInputElement) {

            switch(fieldName)
            {
                case 'type':
                case 'businessDistrict':
                case 'injectionSubstation':
                case 'powerTransformer':
                    this.super('setFormFieldValue', [fieldName, value, fieldInputElement]);
                break;

                case 'date':
                    fieldInputElement.datepick('setDate', moment(value.toString()).format('YYYY-MM-DD'));
                break;

                case 'time':
                    this.super('setFormFieldValue', [fieldName, moment(value.toString()).format('HH:mm'), fieldInputElement])
                break;
            }

            switch(fieldName)
            {
                case 'businessDistrict':

                    var injectionSubstationFormField = this.getFormFieldByName('injectionSubstation');
                    var businessDistrictId = this.getRelationRecordValue(
                        this.getModel().getFieldByName('businessDistrict'),
                        value
                    );

                    if(injectionSubstationFormField.data('business-district') !== businessDistrictId) {
                        this.loadBusinessDistrictInjectionSubstations(businessDistrictId);
                    }

                break;

                case 'injectionSubstation':

                    var powerTransformerFormField = this.getFormFieldByName('powerTransformer');

                    if(value !== null && value !== undefined) {
                        var injectionSubstationId = this.getRelationRecordValue(
                            this.getModel().getFieldByName('injectionSubstation'),
                            value
                        );

                        if(Number(powerTransformerFormField.data('injection-substation')) !== injectionSubstationId) {
                            this.loadInjectionSubstationPowerTransformers(injectionSubstationId);
                        }
                    }
                    else {
                        this.resetRelationFormField(
                            this.getModel().getFieldByName('powerTransformer'),
                            powerTransformerFormField
                        );
                    }

                break;

                case 'powerTransformer':

                    var powerTransformerFeederFormField = this.getFormFieldByName('powerTransformerFeeder');

                    if(value !== null && value !== undefined) {

                        var powerTransformerId = this.getRelationRecordValue(
                            this.getModel().getFieldByName('powerTransformer'),
                            value
                        );

                        if(Number(powerTransformerFeederFormField.data('power-transformer')) !== powerTransformerId) {
                            this.loadPowerTransformerOutputFeeders(powerTransformerId);
                        }
                    }
                    else {
                        this.resetRelationFormField(
                            this.getModel().getFieldByName('powerTransformerFeeder'),
                            powerTransformerFeederFormField
                        );
                    }

                break;
            }
        },

        resetRelationFormField: function(fieldModelData, relationFormField) {

            this.super('resetRelationFormField', [fieldModelData, relationFormField]);

            switch(fieldModelData.name)
            {
                case 'powerTransformer':
                    relationFormField.data('injection-substation', null);
                break;
            }
        },

        resetFormField: function(fieldName, inputElement) {

            switch(fieldName)
            {
                case 'businessDistrict':
                    this.super('resetFormField', [fieldName, inputElement]);
                    this.super('resetFormField', ['injectionSubstation', inputElement]);
                break;

                case 'powerTransformer':
                    this.super('resetFormField', [fieldName, inputElement]);
                    this.super('resetFormField', ['powerTransformerFeeder', inputElement]);
                break;

                default:
                    this.super('resetFormField', [fieldName, inputElement]);
                break;
            }
        },

        loadBusinessDistrictInjectionSubstations: function(businessDistrictId) {

            if(businessDistrictId) {

                var fieldModelData = this.getModel().getFieldByName('injectionSubstation');

                var parameters = {
                    businessDistrict: businessDistrictId
                };

                this.loadRelationFormFieldData(fieldModelData, parameters);
            }
        },

        loadInjectionSubstationPowerTransformers: function(injectionSubstationId) {

            if(injectionSubstationId) {

                var fieldModelData = this.getModel().getFieldByName('powerTransformer');

                var parameters = {
                    injectionSubstation: injectionSubstationId
                };

                this.loadRelationFormFieldData(fieldModelData, parameters);
            }
        },


        loadPowerTransformerOutputFeeders: function(powerTransformerId) {

            if(powerTransformerId) {

                var fieldModelData = this.getModel().getFieldByName('powerTransformerFeeder');

                var parameters = {
                    outputFeedersForPowerTransformer: powerTransformerId
                };

                this.loadRelationFormFieldData(fieldModelData, parameters);
            }
        },

        populateRelationFormField: function(fieldModelData, data, relationFormField) {

            this.super('populateRelationFormField', [fieldModelData, data, relationFormField]);

            if(fieldModelData.name === 'injectionSubstation') {

                var businessDistrictValue = this.getFormFieldValue('businessDistrict', this.getFormFieldByName('businessDistrict'));

                if(businessDistrictValue) {
                    relationFormField.data('business-district', businessDistrictValue);    
                }
            }
            else if(fieldModelData.name === 'powerTransformer') {

                var injectionSubstationValue = this.getFormFieldValue('injectionSubstation', this.getFormFieldByName('injectionSubstation'));

                if(injectionSubstationValue) {
                    relationFormField.data('injection-substation', injectionSubstationValue);    
                }
            }
        },

        updateFormSubmitButtonLabel: function(formMode) {},
    }
});