HCM.define({

    name: 'Modules.Appliance',

    extendFrom: 'Modules.CrudModule',

    construct: function(app, permissions) {

        this.callConstructor('Modules.CrudModule', [app, permissions]);

        this.getFormModuleClassname = function() {
            return 'Modules.Form.Appliance';
        };

        this.getModuleName = function() {
            return 'Appliance';
        };
    },

    methods: {

        initModel: function() {
            this.setModel(this.getApp().getModel('Model.Appliance'));
        },

        setControls: function() {

            this.super('setControls');

            var self = this;
            var applianceTypeFieldModelData = this.getModel().getFieldByName('applianceType');

            var appliancesGrid = $("#appliances-Appliance-grid").grid({

                autoLoad: false,
                cellEdit: true,
                cellEditType: 'callback',
                model: [
                    {
                        header: 'ID',
                        id: 'id',
                        dataMap: 'id'
                    },
                    {
                        header: 'Type',
                        id: 'applianceType',
                        dataMap: 'applianceType',
                        editable: true,
                        editOptions: {
                            type: 'select',
                            valueProperty: applianceTypeFieldModelData.valuePropertyName,
                            labelProperty: applianceTypeFieldModelData.labelPropertyName,
                        },
                        formatter: function(cellValue) {
                            if(cellValue && self.isObject(cellValue)) {
                                return cellValue[applianceTypeFieldModelData.labelPropertyName];
                            }
                        }
                    },
                    {
                        header: 'Make',
                        id: 'make',
                        dataMap: 'make',
                        editable: true
                    },
                    {
                        header: 'Consumption (Watts)',
                        id: 'consumption',
                        dataMap: 'consumption',
                        editable: true
                    }
                ]
            });

            this.setMainViewComponent(appliancesGrid);
        }
    }
});
