HCM.define({

    name: 'Modules.PowerPurchase',

    extendFrom: 'Modules.CrudModule',

    construct: function(app, permissions) {

        this.callConstructor('Modules.CrudModule', [app, permissions]);

        var powerPurchaseApprovePopup;

        this.setPowerPurchaseApprovePopup = function(psap) {
            powerPurchaseApprovePopup = psap;
        };

        this.getPowerPurchaseApprovePopup = function() {
            return powerPurchaseApprovePopup;
        };

        this.getFormModuleClassname = function() {
            return 'Modules.Form.PowerPurchase';
        };

        this.getModuleName = function() {
            return 'PowerPurchase';
        };
    },

    methods: {

        initModel: function() {
            this.setModel(this.getApp().getModel('Model.PowerPurchase'));
        },

        initController: function() {
            var crudController = this.getApp().getController('Controllers.Module.PowerPurchase', [this, this.getModel()]);
            this.setController(crudController);
        },        

        resolveModulePermissions: function(permissions) {

            this.super('resolveModulePermissions', [permissions]);

            if(permissions && this.isArray(permissions.actions)) {

                if(permissions.actions.indexOf('approve') === -1) {
                    this.getModuleElement().find('[data-role="approve-record"]').remove();
                }
            }
        },

        setControls: function() {

            this.super('setControls');

            var self = this;
            var powerSourceModelData = this.getModel().getFieldByName('powerSource');

            var powerSourcesGrid = $("#powerPurchases-PowerPurchase-grid").grid({

                autoLoad: false,
                cellEdit: true,
                cellEditType: 'callback',
                model: [
                    {
                        header: 'ID',
                        id: 'id',
                        dataMap: 'id'
                    },
                    {
                        header: 'Power Type',
                        id: 'powerType',
                        dataMap: 'powerType',
                        editable: true,
                        editOptions: {
                            type: 'select',
                            valueProperty: 'value',
                            labelProperty: 'label',
                            availableOptions:[
                                {value: 'national', label: 'National'},
                                {value: 'captive', label: 'Captive'},
                                {value: 'embedded', label: 'Embedded'},
                            ],
                            areAvailableOptionsStatic: true
                        },
                        formatter: function(cellValue) {

                            if(cellValue) {
                                return cellValue.charAt(0).toUpperCase() + cellValue.slice(1);
                            }

                            return '';
                        }
                    },
                    {
                        header: 'Power Source',
                        id: 'powerSource',
                        dataMap: 'powerSource',
                        editable: true,
                        editOptions: {
                            type: 'select',
                            valueProperty: powerSourceModelData.valuePropertyName,
                            labelProperty: powerSourceModelData.labelPropertyName,
                        },
                        formatter: function(cellValue) {
                            if(cellValue && self.isObject(cellValue)) {
                                return cellValue[powerSourceModelData.labelPropertyName];
                            }
                        }
                    },
                    {
                        header: 'Amount',
                        id: 'amountPurchased',
                        dataMap: 'amountPurchased',
                        editable: true
                    },
                    {
                        header: 'Date Purchased',
                        id: 'datePurchased',
                        dataMap: 'datePurchased',
                        editable: true,
                        formatter: function(cellValue) {
                            return moment(cellValue).format('YYYY-MM-DD');
                        },
                        editOptions: {

                            type: 'text',
                            cancelEditOnClick: false,

                            initializeInputCallback: function(inputElement) {

                                var datePicker = inputElement.datepick({ 
                                    dateFormat: 'yyyy-mm-dd'
                                });

                                datePicker.focus();
                                return datePicker;
                            },

                            getInputData: function(initializedInput) {
                                return moment(initializedInput.datepick('getDate')).format('YYYY-MM-DD');
                            },

                            setInputData: function(initializedInput, cellData, rowData) {
                                initializedInput.datepick('setDate', moment(cellData).format('YYYY-MM-DD'));
                            },

                            checkIfCellDataIsChanged: function(cellName, oldCellData, editedCellData, rowData) {

                                var oldDate = moment(oldCellData).format('YYYY-MM-DD');
                                var newDate = moment(editedCellData).format('YYYY-MM-DD');

                                return oldDate !== newDate;
                            }
                        }
                    },
                    {
                        header: 'Status',
                        id: 'status',
                        dataMap: 'status',
                        editable: false,
                        formatter: function(cellValue) {
                            if(cellValue === 'approved') {
                                return 'Approved';
                            }
                            else {
                                return 'Waiting for approval';
                            }
                        }
                    }
                ]
            });

            this.setMainViewComponent(powerSourcesGrid);

            var powerPurchaseApprovePopup = this.getModuleElement().find("#approve-power-purchase-popup").popup({
                    compileTemplateFunction: this.getApp().compileTemplate,
                    compileTemplateData: {
                        title: 'Are you sure you want to approve this power purchase?',
                        okLabel: 'Yes',
                        cancelLabel: 'No'
                    }
                });

            this.setPowerPurchaseApprovePopup(powerPurchaseApprovePopup);
        },

        approvePowerPurchase: function(powerPurchase) {

            this.getModel().patch(powerPurchase.id, {status: 'approved'}, {
                scope: {
                    module: this,
                    fieldModelData: this.getModel().getFieldByName('status')
                }
            });
        }
    }
});