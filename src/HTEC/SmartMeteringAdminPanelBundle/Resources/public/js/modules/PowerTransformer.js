HCM.define({

    name: 'Modules.PowerTransformer',

    extendFrom: 'Modules.CrudModule',

    construct: function(app, permissions) {

        this.callConstructor('Modules.CrudModule', [app, permissions]);

        this.getFormModuleClassname = function() {
            return 'Modules.Form.PowerTransformer';
        };

        this.getModuleName = function() {
            return 'PowerTransformer';
        };
    },

    methods: {

        initModule: function() {

            this.setNotAutoLoadingRelationFields(['inputFeeder', 'outputFeeders', 'injectionSubstation']);
            this.super('initModule');
        },

        initModel: function() {
            this.setModel(this.getApp().getModel('Model.PowerTransformer'));
        },

        setControls: function() {

            this.super('setControls');

            var injectionSubstationFieldModelData = this.getModel().getFieldByName('injectionSubstation');
            var outputFeedersFieldModelData = this.getModel().getFieldByName('outputFeeders');

            var self = this;

            var powerTransformersGrid = this.getModuleElement().find("#powerTransformers-PowerTransformer-grid").grid({

                autoLoad: false,
                cellEdit: true,
                cellEditType: 'callback',
                model: [
                    {
                        header: 'ID',
                        id: 'id',
                        dataMap: 'id'
                    },
                    {
                        header: 'Name',
                        id: 'name',
                        dataMap: 'name',
                        editable: true
                    },
                    {
                        header: 'Status',
                        id: 'status',
                        dataMap: 'status',
                        editable: true,
                        editOptions: {
                            type: 'select',
                            valueProperty: 'value',
                            labelProperty: 'label',
                            availableOptions:[
                                {value: 'live', label: 'Live'},
                                {value: 'offline', label: 'Offline'},
                                {value: 'repairing', label: 'Repairing'}
                            ],
                            areAvailableOptionsStatic: true
                        },
                        formatter: function(cellValue) {
                            if(cellValue) {
                                return cellValue.charAt(0).toUpperCase() + cellValue.slice(1);
                            }
                        }
                    },
                    {
                        header: 'Injection Substation',
                        id: 'injectionSubstation',
                        dataMap: 'injectionSubstation',
                        editable: true,
                        formatter: function(cellValue) {
                            if(cellValue && self.isObject(cellValue)) {
                                return cellValue[injectionSubstationFieldModelData.labelPropertyName];
                            }
                        }
                    },
                    {
                        header: 'Code',
                        id: 'code',
                        dataMap: 'code',
                        editable: true
                    },
                    {
                        header: 'Output Feeders',
                        id: 'outputFeeders',
                        dataMap: 'outputFeeders',
                        editable: true,
                        formatter: function(cellValue) {

                            if(cellValue) {

                                var label = '';

                                for(var i = 0, len = cellValue.length; i < len; i++) {
                                    label += cellValue[i][outputFeedersFieldModelData.labelPropertyName] + ', ';
                                }

                                return label.substring(0, label.length - 2);
                            }

                            return 'N/A';
                        }
                    }
                ]
            });
    
            var injectionSubstationGridCellEditOptions = this.getAutoCompleteGridCellEditOptions(
                    powerTransformersGrid,
                    injectionSubstationFieldModelData
            );

            powerTransformersGrid.grid(
                'setColumnEditOptions',
                'injectionSubstation',
                injectionSubstationGridCellEditOptions
            );

            var outputFeedersGridCellEditOptions = this.getMultiAutoCompleteGridCellEditOptions(
                    powerTransformersGrid,
                    outputFeedersFieldModelData,
                    this.getModel(),
                    {type: 'high-voltage'}
            );

            powerTransformersGrid.grid(
                'setColumnEditOptions',
                'outputFeeders',
                outputFeedersGridCellEditOptions
            );

            this.setMainViewComponent(powerTransformersGrid);
        }
    }
});