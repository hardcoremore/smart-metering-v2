HCM.define({

    name: 'Modules.CustomerCategory',

    extendFrom: 'Modules.CrudModule',

    construct: function(app, permissions) {

        this.callConstructor('Modules.CrudModule', [app, permissions]);

        this.getFormModuleClassname = function() {
            return 'Modules.Form.CustomerCategory';
        };

        this.getModuleName = function() {
            return 'CustomerCategory';
        };
    },

    methods: {

        initModel: function() {
            this.setModel(this.getApp().getModel('Model.CustomerCategory'));
        },

        setControls: function() {

            this.super('setControls');

            var customerCategoriesGrid = $("#customerCategories-CustomerCategory-grid").grid({

                autoLoad: false,
                cellEdit: true,
                cellEditType: 'callback',
                model: [
                    {
                        header: 'ID',
                        id: 'id',
                        dataMap: 'id'
                    },
                    {
                        header: 'Name',
                        id: 'name',
                        dataMap: 'name',
                        editable: true
                    },
                    {
                        header: 'Diversity Factor',
                        id: 'diversityFactor',
                        dataMap: 'diversityFactor',
                        editable: true
                    }
                ]
            });

            this.setMainViewComponent(customerCategoriesGrid);
        }
    }
});
