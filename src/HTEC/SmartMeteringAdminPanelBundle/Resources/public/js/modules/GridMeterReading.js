HCM.define({

    name: 'Modules.GridMeterReading',

    extendFrom: 'Modules.CrudModule',

    construct: function(app, permissions) {

        this.callConstructor('Modules.CrudModule', [app, permissions]);

        this.getModuleName = function() {
            return 'GridMeterReading';
        };
    },

    methods: {

        initModel: function() {
            this.setModel(this.getApp().getModel('Model.GridMeterReading'));
        },

        setControls: function() {

            this.super('setControls');

            var self = this;

            var gridMeterReadingsGrid = this.getModuleElement().find("#gridMeterReadings-GridMeterReading-grid").grid({

                autoLoad: false,
                model: [
                    {
                        header: 'ID',
                        id: 'id',
                        dataMap: 'id'
                    },
                    {
                        header: 'Customer',
                        id: 'customer',
                        dataMap: 'customer',
                 
                    },
                    {
                        header: 'Account Number',
                        id: 'accountNumber',
                        dataMap: 'customer',
                 
                    },
                    {
                        header: 'Business District',
                        id: 'businessDistrict',
                        dataMap: 'customer',
                       
                    }
                ]
            });
    
            this.setMainViewComponent(gridMeterReadingsGrid);
        }
    }
});