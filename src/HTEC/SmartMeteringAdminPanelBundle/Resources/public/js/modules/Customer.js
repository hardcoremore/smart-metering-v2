HCM.define({

    name: 'Modules.Customer',

    extendFrom: 'Modules.CrudModule',

    construct: function(app, permissions) {

        this.callConstructor('Modules.CrudModule', [app, permissions]);

        this.getModuleName = function() {
            return 'Customer';
        };
    },

    methods: {

        initModel: function() {
            this.setModel(this.getApp().getModel('Model.Customer'));
        },

        setControls: function() {

            this.super('setControls');

            var businessDistrictFieldModelData = this.getModel().getFieldByName('businessDistrict');
            var tariffFieldModelData = this.getModel().getFieldByName('tariff');

            var self = this;

            var customersGrid = this.getModuleElement().find("#customers-Customer-grid").grid({

                autoLoad: false,
                cellEdit: false,
                cellEditType: 'callback',
                model: [
                    {
                        header: 'ID',
                        id: 'id',
                        dataMap: 'id'
                    },
                    {
                        header: 'Name',
                        id: 'name',
                        dataMap: 'name'
                    },
                    {
                        header: 'Account Number',
                        id: 'accountNumber',
                        dataMap: 'accountNumber'
                    },
                    {
                        header: 'Account Type',
                        id: 'accountType',
                        dataMap: 'accountType'
                    },
                    {
                        header: 'Customer Status',
                        id: 'customerStatus',
                        dataMap: 'customerStatus',
                        formatter: function(cellValue) {
                            if(cellValue) {
                                return cellValue.charAt(0).toUpperCase() + cellValue.slice(1);
                            }
                        }
                    },
                    {
                        header: 'Customer Type',
                        id: 'customerType',
                        dataMap: 'customerType',
                        formatter: function(cellValue) {
                            if(cellValue) {
                                return cellValue.charAt(0).toUpperCase() + cellValue.slice(1);
                            }
                        }
                    },
                    {
                        header: 'Tariff',
                        id: 'tariff',
                        dataMap: 'tariff',
                        formatter: function(cellValue) {
                            if(cellValue && self.isObject(cellValue)) {
                                return cellValue[businessDistrictFieldModelData.labelPropertyName];
                            }
                        }
                    },
                    {
                        header: 'Business District',
                        id: 'businessDistrict',
                        dataMap: 'businessDistrict',
                        formatter: function(cellValue) {
                            if(cellValue && self.isObject(cellValue)) {
                                return cellValue[tariffFieldModelData.labelPropertyName];
                            }
                        }
                    },
                    {
                        header: 'Address',
                        id: 'address',
                        dataMap: 'address'
                    },
                   
                    {
                        header: 'Email',
                        id: 'email',
                        dataMap: 'email',
                        editable: true
                    },
                    {
                        header: 'Phone Number',
                        id: 'phoneNumber',
                        dataMap: 'phoneNumber',
                        editable: true
                    }
                ]
            });

            this.setMainViewComponent(customersGrid);
        },

        // disable loading relation data since customers are not editable
        loadRelationFieldData: function(fieldModelData) {}
    }
});