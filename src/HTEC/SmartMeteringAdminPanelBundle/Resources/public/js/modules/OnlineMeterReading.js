HCM.define({

    name: 'Modules.OnlineMeterReading',

    extendFrom: 'Modules.CrudModule',

    construct: function(app, permissions) {

        this.callConstructor('Modules.CrudModule', [app, permissions]);

        this.getModuleName = function() {
            return 'OnlineMeterReading';
        };
    },

    methods: {

        initModel: function() {
            this.setModel(this.getApp().getModel('Model.OnlineMeterReading'));
        },

        initController: function() {
            var crudController = this.getApp().getController('Controllers.Module.OnlineMeterReading', [this, this.getModel()]);
            this.setController(crudController);
        },

        setControls: function() {

            this.super('setControls');

            var self = this;

            var onlineReadingsGrid = this.getModuleElement().find("#onlineMeterReadings-OnlineMeterReading-grid").grid({

                autoLoad: false,
                cellEdit: false,
                cellEditType: 'callback',
                model: [
                    {
                        header: 'ID',
                        id: 'id',
                        dataMap: 'id'
                    },
                    {
                        header: 'Account Number',
                        id: 'accountNumber',
                        dataMap: 'accountNumber'
                    },
                    {
                        header: 'Meter Number',
                        id: 'meterNumber',
                        dataMap: 'meterNumber'
                    },
                    {
                        header: 'Date Read',
                        id: 'dateRead',
                        dataMap: 'dateRead',
                        formatter: function(cellValue) {
                            if(cellValue) {
                                return moment(cellValue).format('YYYY-MM-DD HH:mm:ss');
                            }
                        }
                    },
                    {
                        header: 'Previous Meter Reading',
                        id: 'previousMeterReading',
                        dataMap: 'previousMeterReading'
                    },
                    {
                        header: 'Current Meter Reading',
                        id: 'currentMeterReading',
                        dataMap: 'currentMeterReading'
                    },
                    {
                        header: 'Consumption',
                        id: 'consumption',
                        dataMap: 'consumption'
                    },
                    {
                        header: 'Reader',
                        id: 'reader',
                        dataMap: 'reader'
                    },
                    {
                        header: 'Suspect',
                        id: 'suspect',
                        dataMap: 'suspect'
                    },
                    {
                        header: 'Previous Consumption',
                        id: 'previousConsumption',
                        dataMap: 'previousConsumption'
                    },
                    {
                        header: 'Previous Date Read',
                        id: 'previousDateRead',
                        dataMap: 'previousDateRead',
                        formatter: function(cellValue) {
                            if(cellValue) {
                                return moment(cellValue).format('YYYY-MM-DD HH:mm:ss');
                            }
                        }
                    },
                    {
                        header: 'Average Daily Consumption',
                        id: 'averageDailyConsumption',
                        dataMap: 'averageDailyConsumption',
                        formatter: function(cellValue) {
                            if(cellValue) {
                                return Number(cellValue).toFixed(2);
                            }
                        }
                    }
                ]
            });

            this.setMainViewComponent(onlineReadingsGrid);
        }
    }
});