HCM.define({

    name: 'Modules.PowerAllocate',

    extendFrom: 'Modules.CrudModule',

    construct: function(app, permissions) {

        this.callConstructor('Modules.CrudModule', [app, permissions]);

        this.getFormModuleClassname = function() {
            return 'Modules.Form.PowerAllocate';
        };

        this.getModuleName = function() {
            return 'PowerAllocate';
        };
    },

    methods: {

        initModule: function() {

            this.setNotAutoLoadingRelationFields([
                'powerForecast'
            ]);

            this.super('initModule');
        },

        initModel: function() {
            this.setModel(this.getApp().getModel('Model.PowerAllocate'));
        },

        setControls: function() {

            this.super('setControls');

            var self = this;
            var cityFieldModelData = this.getModel().getFieldByName('city');

            var powerAllocatesGrid = $("#powerAllocates-PowerAllocate-grid").grid({

                autoLoad: false,
                cellEdit: true,
                cellEditType: 'callback',
                model: [
                    {
                        header: 'ID',
                        id: 'id',
                        dataMap: 'id'
                    },
                    {
                        header: 'Power Forecast',
                        id: 'powerForecast',
                        dataMap: 'powerForecast',
                        formatter: function(cellValue) {

                            if(cellValue) {
                                var label = cellValue.powerType;
                                    label += ' - ' + cellValue.powerPurchase.powerSource.name;
                                    label += ' - ' + cellValue.powerPurchase.amountPurchased + 'MW';

                                return label;
                            }
                        }
                    },
                    {
                        header: 'Highest Allocation in Business District',
                        id: 'highestAllocationBusinessDistrict',
                        dataMap: 'highestAllocationBusinessDistrict',
                        formatter: function(cellValue) {
                            if(cellValue) {
                                return cellValue.name;
                            }
                        }
                    },
                    {
                        header: 'Highest Allocation in Business District Value',
                        id: 'highestBusinessDistrictAllocationValue',
                        dataMap: 'highestBusinessDistrictAllocationValue',
                        formatter: function(cellValue) {
                            return Number(cellValue).formatNumberThousands();
                        }
                    },
                    {
                        header: 'Created By',
                        id: 'createdDatetime',
                        dataMap: 'createdDatetime',
                        formatter: function(cellValue) {
                            return cellValue.firstName + ' ' + cellValue.lastName;
                        }
                    },
                    {
                        header: 'Created Datetime',
                        id: 'createdDatetime',
                        dataMap: 'createdDatetime',
                        formatter: function(cellValue) {
                            return moment(cellValue).format('YYYY-MM-DD HH:mm:ss');
                        }
                    },
                    {
                        header: 'Last Edited Datetime',
                        id: 'lastEditedDatetime',
                        dataMap: 'lastEditedDatetime',
                        formatter: function(cellValue) {
                            if(cellValue) {
                                return moment(cellValue).format('YYYY-MM-DD HH:mm:ss');    
                            }
                            else {
                                return 'n/a';
                            }
                        }
                    },
                    {
                        header: 'Last Edited By',
                        id: 'lastEditedBy',
                        dataMap: 'lastEditedBy',
                        formatter: function(cellValue) {

                            if(cellValue) {
                                return cellValue.firstName + ' ' + cellValue.lastName;    
                            }
                            else {
                                return 'n/a';
                            }
                            
                        }
                    },
                ]
            });

            this.setMainViewComponent(powerAllocatesGrid);
        }
    }
});