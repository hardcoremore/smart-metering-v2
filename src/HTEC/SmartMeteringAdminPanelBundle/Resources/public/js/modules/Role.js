HCM.define({

    name: 'Modules.Role',

    extendFrom: 'Modules.CrudModule',

    construct: function(app, permissions) {

        this.callConstructor('Modules.CrudModule', [app, permissions]);

        this.getFormModuleClassname = function() {
            return 'Modules.Form.RoleForm';
        };

        this.getModuleName = function() {
            return 'Role';
        };
    },

    methods: {

        initModel: function() {
            this.setModel(this.getApp().getRoleModel());
        },

        setControls: function() {

            this.super('setControls');

            var rolesGrid = this.getModuleElement().find("#roles-Role-grid").grid({

                autoLoad: false,
                cellEdit: true,
                cellEditType: 'callback',
                model: [
                    {
                        header: 'ID',
                        id: 'id',
                        dataMap: 'id'
                    },
                    {
                        header: 'Owner',
                        id: 'owner',
                        dataMap: 'owner',
                        editable: false,
                        formatter: function(cellValue) {
                            if(cellValue) {
                                return cellValue.firstName + ' ' + cellValue.lastName;
                            }
                        }
                    },
                    {
                        header: 'Name',
                        id: 'name',
                        dataMap: 'name',
                        editable: true
                    },
                    {
                        header: 'Role',
                        id: 'role',
                        dataMap: 'role',
                        editable: false
                    }
                ]
            });

            this.setMainViewComponent(rolesGrid);        
        },

        loadRelationFieldData: function(fieldModelData) {
            // override this method to skip loading permissions relation data
            // because role permissions will not be edited through grid
        },
    }
});