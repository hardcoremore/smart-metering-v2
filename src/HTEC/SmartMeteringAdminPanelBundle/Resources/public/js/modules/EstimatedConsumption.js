HCM.define({

    name: 'Modules.EstimatedConsumption',

    extendFrom: 'Modules.CrudModule',

    construct: function(app, permissions) {

        this.callConstructor('Modules.CrudModule', [app, permissions]);

        this.getFormModuleClassname = function() {
            return 'Modules.Form.EstimatedConsumptionForm';
        };

        this.getModuleName = function() {
            return 'EstimatedConsumption';
        };
    },

    methods: {

        initModule: function() {

            this.setNotAutoLoadingRelationFields(['customer']);

            this.super('initModule');
        },

        initModel: function() {
            this.setModel(this.getApp().getModel('Model.EstimatedConsumption'));
        },

        setControls: function() {

            this.super('setControls');

            var customerFieldModelData = this.getModel().getFieldByName('customer');

            var self = this;

            var estimatedConsumptionsGrid = this.getModuleElement().find("#estimatedConsumptions-EstimatedConsumption-grid").grid({

                autoLoad: false,
                cellEditType: 'callback',
                model: [
                    {
                        header: 'ID',
                        id: 'id',
                        dataMap: 'id'
                    },
                    {
                        header: 'Customer',
                        id: 'customer',
                        dataMap: 'customer',
                        formatter: function(cellValue) {
                            if(cellValue && self.isObject(cellValue)) {
                                return cellValue[customerFieldModelData.labelPropertyName];
                            }
                        }
                    },
                    {
                        header: 'Account Number',
                        id: 'accountNumber',
                        dataMap: 'customer',
                        formatter: function(cellValue) {
                            if(cellValue && self.isObject(cellValue)) {
                                return cellValue.accountNumber;
                            }
                        }
                    },
                    {
                        header: 'Business District',
                        id: 'businessDistrict',
                        dataMap: 'customer',
                        formatter: function(cellValue) {
                            if(cellValue && self.isObject(cellValue) && cellValue.businessDistrict) {
                                return cellValue.businessDistrict.name;
                            }
                        }
                    },
                    {
                        header: 'Start',
                        id: 'start',
                        dataMap: 'start',
                        formatter: function(cellValue) {
                            if(cellValue) {
                                return moment(cellValue).format('YYYY-MM-DD');
                            }
                        }
                    },
                    {
                        header: 'End',
                        id: 'end',
                        dataMap: 'end',
                        formatter: function(cellValue) {
                            if(cellValue) {
                                return moment(cellValue).format('YYYY-MM-DD');
                            }
                        }
                    },
                    {
                        header: 'Hours Scheduled',
                        id: 'actualHoursScheduled',
                        dataMap: 'actualHoursScheduled'
                    },
                    {
                        header: 'Consumption',
                        id: 'estimatedConsumption',
                        dataMap: 'estimatedConsumption'
                    },
                    {
                        header: 'Is Estimated Load',
                        id: 'estimatedLoad',
                        dataMap: 'estimatedLoad',
                        formatter: function(cellValue) {
                            if(cellValue) {
                                return 'Yes';
                            }
                            else {
                                return 'No';
                            }
                        }
                    },
                    {
                        header: 'Estimated Load',
                        id: 'calculatedLoad',
                        dataMap: 'calculatedLoad',
                        formatter: function(cellValue) {
                            if(cellValue) {
                                return cellValue.totalLoad
                            }
                        }
                    },
                    {
                        header: 'Type',
                        id: 'type',
                        dataMap: 'type',
                        formatter: function(cellValue) {
                            if(cellValue === 'estimated') {
                                return 'Estimated';
                            }
                            else {
                                return "Tariff's average";
                            }
                        }
                    },
                    {
                        header: 'Diversity Factor',
                        id: 'diversityFactor',
                        dataMap: 'diversityFactor'
                    }
                ]
            });
    
            this.setMainViewComponent(estimatedConsumptionsGrid);
        }
    }
});