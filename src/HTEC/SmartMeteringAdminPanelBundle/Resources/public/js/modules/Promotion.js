HCM.define({

    name: 'Modules.Promotion',

    extendFrom: 'Modules.CrudModule',

    construct: function(app, permissions) {

        this.callConstructor('Modules.CrudModule', [app, permissions]);

        this.getModuleName = function() {
            return 'Promotion';
        };
    },

    methods: {

        initModel: function() {
            this.setModel(this.getApp().getModel('Model.Promotion'));
        },

        setControls: function() {

            this.super('setControls');

            var self = this;

            var promotionGrid = this.getModuleElement().find("#promotion-Promotion-grid").grid({

                autoLoad: false,
                model: [
                    {
                        header: 'ID',
                        id: 'id',
                        dataMap: 'id'
                    },
                    {
                        header: 'Customer',
                        id: 'customer',
                        dataMap: 'customer',
                 
                    },
                    {
                        header: 'Account Number',
                        id: 'accountNumber',
                        dataMap: 'customer',
                 
                    },
                    {
                        header: 'Business District',
                        id: 'businessDistrict',
                        dataMap: 'customer',
                       
                    }
                ]
            });
    
            this.setMainViewComponent(promotionGrid);
        }
    }
});