HCM.define({

    name: 'Modules.PowerSource',

    extendFrom: 'Modules.CrudModule',

    construct: function(app, permissions) {

        this.callConstructor('Modules.CrudModule', [app, permissions]);

        this.getFormModuleClassname = function() {
            return 'Modules.Form.PowerSource';
        };

        this.getModuleName = function() {
            return 'PowerSource';
        };
    },

    methods: {

        initModule: function() {

            this.setNotAutoLoadingRelationFields(['outputFeeders']);
            this.super('initModule');
        },

        initModel: function() {
            this.setModel(this.getApp().getModel('Model.PowerSource'));
        },

        setControls: function() {

            this.super('setControls');

            var that = this;

            var powerSourcesGrid = $("#powerSources-PowerSource-grid").grid({

                autoLoad: false,
                cellEdit: true,
                cellEditType: 'callback',
                model: [
                    {
                        header: 'ID',
                        id: 'id',
                        dataMap: 'id'
                    },
                    {
                        header: 'Name',
                        id: 'name',
                        dataMap: 'name',
                        editable: true
                    },
                    {
                        header: 'Capacity',
                        id: 'capacity',
                        dataMap: 'capacity',
                        editable: true
                    },
                    {
                        header: 'Address',
                        id: 'address',
                        dataMap: 'address',
                        editable: true
                    },
                    {
                        header: 'Output Feeders',
                        id: 'outputFeeders',
                        dataMap: 'outputFeeders',
                        editable: true,
                        formatter: function(cellValue) {

                            if(cellValue) {

                                var label = '';

                                for(var i = 0, len = cellValue.length; i < len; i++) {
                                    label += cellValue[i].name + ', ';
                                }

                                return label.substring(0, label.length - 2);
                            }

                            return 'N/A';
                        }
                    }
                ]
            });

            var putputFeedersFieldModelData = this.getModel().getFieldByName('outputFeeders');

            var outputFeedersGridCellEditOptions = this.getMultiAutoCompleteGridCellEditOptions(
                    powerSourcesGrid,
                    putputFeedersFieldModelData,
                    this.getModel()
            );

            powerSourcesGrid.grid(
                'setColumnEditOptions',
                'outputFeeders',
                outputFeedersGridCellEditOptions
            );

            this.setMainViewComponent(powerSourcesGrid);
        }
    }
});