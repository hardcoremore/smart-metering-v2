HCM.define({

    name: 'Modules.PowerAviability',

    extendFrom: 'Modules.RelationFieldFormModule',

    construct: function(app, permissions) {

        this.callConstructor('Modules.RelationFieldFormModule', [app, permissions]);

        var powerAviabilityScheduler;
        var searchFeedersButton;
        var schedulerHelper;

        var powerSchedulesDateRangeHolder;
        var currentStartDateForDateRangeSearch;

        var previousWeekButton;
        var nextWeekButton;

        this.setPowerAviabilityScheduler = function(grid) {
            powerAviabilityScheduler = grid;            
        };

        this.getPowerAviabilityScheduler = function() {
            return powerAviabilityScheduler;
        };

        this.setSearchFeedersButton = function(button) {
            searchFeedersButton = button;
        };

        this.getSearchFeedersButton = function() {
            return searchFeedersButton;
        };

        this.setSchedulerHelper = function(helper) {
            schedulerHelper = helper;
        };

        this.getSchedulerHelper = function() {
            return schedulerHelper;
        };

        this.setPowerAviabilityDateRangeHolder = function(holder) {
            powerSchedulesDateRangeHolder = holder;
        };

        this.getPowerAviabilityDateRangeHolder = function() {
            return powerSchedulesDateRangeHolder;
        };

        this.setCurrentStartDateForDateRangeSearch = function(date){
            currentStartDateForDateRangeSearch = date;
        };

        this.getCurrentStartDateForDateRangeSearch = function() {
            return currentStartDateForDateRangeSearch;
        };

        this.setPreviousWeekButton = function(button) {
            previousWeekButton = button;
        };

        this.getPreviousWeekButton = function() {
            return previousWeekButton;
        };

        this.setNextWeekButton = function(button) {
            nextWeekButton = button;
        };

        this.getNextWeekButton = function() {
            return nextWeekButton;
        };

        this.getModuleName = function() {
            return 'PowerAviability';
        };
    },

    methods: {

        initModule: function() {

            this.setNotAutoLoadingRelationFields([
                'injectionSubstation',
                'powerTransformer',
                'powerTransformerFeeder'
            ]);

            this.setSchedulerHelper(this.getInstance('Helpers.PowerAviabilityScheduler', [this.getApp()]));
            this.super('initModule');
        },

        initModel: function() {
            this.setModel(this.getApp().getModel('Model.PowerAviability'));
            this.getSchedulerHelper().setModel(this.getModel());
        },

        initController: function() {
            var controller = this.getApp().getController('Controllers.Module.PowerAviability', [this, this.getModel()]);
            this.setFormController(controller);
        },

        setControls: function() {

            this.super('setControls');

            var self = this;

            var powerAviabilityScheduler = this.getModuleElement().find("#feederActivatesDeactivates-PowerAviability-scheduler").scheduler({
                allowIntersectingEvents: true,
                createEventTooltipCallback: this.getSchedulerHelper().createTooltipForSchedulerEvent.bind(this.getSchedulerHelper())
            });

            this.setPowerAviabilityScheduler(powerAviabilityScheduler);

            this.setSearchFeedersButton(this.getModuleElement().find('.search-feeders-button'));

            this.setPowerAviabilityDateRangeHolder(this.getModuleElement().find('.date-range-display'));

            this.setPreviousWeekButton(this.getModuleElement().find('.previous-week-button'));
            this.setNextWeekButton(this.getModuleElement().find('.next-week-button'));
        },

        getRelationRecordLabel: function(fieldModelData, recordData) {

            var label = this.super('getRelationRecordLabel', [fieldModelData, recordData]);

            if(fieldModelData.name === 'powerTransformerFeeder') {
                label = label + ' (' + recordData.status + ')';
            }

            return label;
        },

        isRelationFormFieldInitialized: function(fieldModelData, relationFormField) {

            var isInitialized = this.super('isRelationFormFieldInitialized', [fieldModelData, relationFormField]);

            if(isInitialized && this.getFormMode() === 'edit') {

                var editRecordData = this.getEditingRecordData();

                switch(fieldModelData.name) {

                    case 'injectionSubstation':

                        var dataLoadedForBusinessDistrictId = Number(relationFormField.data('business-district'));
                        var editRecordDataBusinessDistrictId = this.getRelationRecordValue(
                            this.getModel().getFieldByName('businessDistrict'),
                            editRecordData['businessDistrict']
                        );

                        return dataLoadedForBusinessDistrictId === editRecordDataBusinessDistrictId;

                    break;

                    case 'powerTransformer':

                        var dataLoadedForInjectionSubstationId = Number(relationFormField.data('injection-substation'));
                        var editRecordDataInjectionSubstationId = this.getRelationRecordValue(
                            this.getModel().getFieldByName('injectionSubstation'),
                            editRecordData['injectionSubstation']
                        );
                        
                        return dataLoadedForInjectionSubstationId === editRecordDataInjectionSubstationId;

                    break;

                    default:
                        return isInitialized;
                    break;
                }
            }
            else {
                return isInitialized;
            }
        },

        setFormFieldValue: function(fieldName, value, fieldInputElement) {

            switch(fieldName)
            {
                case 'type':
                case 'businessDistrict':
                case 'injectionSubstation':
                case 'powerTransformer':
                    this.super('setFormFieldValue', [fieldName, value, fieldInputElement]);
                break;

                case 'date':
                    fieldInputElement.datepick('setDate', moment(value.toString()).format('YYYY-MM-DD'));
                break;

                case 'time':
                    this.super('setFormFieldValue', [fieldName, moment(value.toString()).format('HH:mm'), fieldInputElement])
                break;
            }

            switch(fieldName)
            {
                case 'businessDistrict':

                    var injectionSubstationFormField = this.getFormFieldByName('injectionSubstation');
                    var businessDistrictId = this.getRelationRecordValue(
                        this.getModel().getFieldByName('businessDistrict'),
                        value
                    );

                    if(injectionSubstationFormField.data('business-district') !== businessDistrictId) {
                        this.loadBusinessDistrictInjectionSubstations(businessDistrictId);
                    }

                break;

                case 'injectionSubstation':

                    var powerTransformerFormField = this.getFormFieldByName('powerTransformer');

                    if(value !== null && value !== undefined) {
                        var injectionSubstationId = this.getRelationRecordValue(
                            this.getModel().getFieldByName('injectionSubstation'),
                            value
                        );

                        if(Number(powerTransformerFormField.data('injection-substation')) !== injectionSubstationId) {
                            this.loadInjectionSubstationPowerTransformers(injectionSubstationId);
                        }
                    }
                    else {
                        this.resetRelationFormField(
                            this.getModel().getFieldByName('powerTransformer'),
                            powerTransformerFormField
                        );
                    }

                break;

                case 'powerTransformer':

                    var powerTransformerFeederFormField = this.getFormFieldByName('powerTransformerFeeder');

                    if(value !== null && value !== undefined) {

                        var powerTransformerId = this.getRelationRecordValue(
                            this.getModel().getFieldByName('powerTransformer'),
                            value
                        );

                        if(Number(powerTransformerFeederFormField.data('power-transformer')) !== powerTransformerId) {
                            this.loadPowerTransformerOutputFeeders(powerTransformerId);
                        }
                    }
                    else {
                        this.resetRelationFormField(
                            this.getModel().getFieldByName('powerTransformerFeeder'),
                            powerTransformerFeederFormField
                        );
                    }

                break;
            }
        },

        resetRelationFormField: function(fieldModelData, relationFormField) {

            this.super('resetRelationFormField', [fieldModelData, relationFormField]);

            switch(fieldModelData.name)
            {
                case 'powerTransformer':
                    relationFormField.data('injection-substation', null);
                break;
            }
        },

        resetFormField: function(fieldName, inputElement) {

            switch(fieldName)
            {
                case 'businessDistrict':
                    this.super('resetFormField', [fieldName, inputElement]);
                    this.super('resetFormField', ['injectionSubstation', inputElement]);
                break;

                case 'powerTransformer':
                    this.super('resetFormField', [fieldName, inputElement]);
                    this.super('resetFormField', ['powerTransformerFeeder', inputElement]);
                break;

                default:
                    this.super('resetFormField', [fieldName, inputElement]);
                break;
            }
        },

        loadBusinessDistrictInjectionSubstations: function(businessDistrictId) {

            if(businessDistrictId) {

                var fieldModelData = this.getModel().getFieldByName('injectionSubstation');

                var parameters = {
                    businessDistrict: businessDistrictId
                };

                this.loadRelationFormFieldData(fieldModelData, parameters);
            }
        },

        loadInjectionSubstationPowerTransformers: function(injectionSubstationId) {

            if(injectionSubstationId) {

                var fieldModelData = this.getModel().getFieldByName('powerTransformer');

                var parameters = {
                    injectionSubstation: injectionSubstationId
                };

                this.loadRelationFormFieldData(fieldModelData, parameters);
            }
        },


        loadPowerTransformerOutputFeeders: function(powerTransformerId) {

            if(powerTransformerId) {

                var fieldModelData = this.getModel().getFieldByName('powerTransformerFeeder');

                var parameters = {
                    outputFeedersForPowerTransformer: powerTransformerId
                };

                this.loadRelationFormFieldData(fieldModelData, parameters);
            }
        },

        populateRelationFormField: function(fieldModelData, data, relationFormField) {

            this.super('populateRelationFormField', [fieldModelData, data, relationFormField]);

            if(fieldModelData.name === 'injectionSubstation') {

                var businessDistrictValue = this.getFormFieldValue('businessDistrict', this.getFormFieldByName('businessDistrict'));

                if(businessDistrictValue) {
                    relationFormField.data('business-district', businessDistrictValue);    
                }
            }
            else if(fieldModelData.name === 'powerTransformer') {

                var injectionSubstationValue = this.getFormFieldValue('injectionSubstation', this.getFormFieldByName('injectionSubstation'));

                if(injectionSubstationValue) {
                    relationFormField.data('injection-substation', injectionSubstationValue);    
                }
            }
        },

        updateFormSubmitButtonLabel: function(formMode) {},

        getStartDateForLoadingPoverAviability: function(isSlider, isReadingPrevious) {

            var currentStartDate = this.getCurrentStartDateForDateRangeSearch();

            if(currentStartDate && isSlider) {

                if(isReadingPrevious) {
                    currentStartDate.subtract(7, 'days');
                }
                else {
                    currentStartDate.add(7, 'days');
                }
            }
            else {

                currentStartDate = moment();
                currentStartDate.subtract(currentStartDate.day() - 1, 'days');
            }

            return currentStartDate;
        },

        getEndDateForLoadingPowerAviability: function(startDate) {
            return startDate.clone().add(6, 'days');
        },

        readPowerAviabilityForDateRange: function(start, end, searchParameters) {

            this.getModel().readAll({

                parameters: $.extend({}, {
                    start: start,
                    end: end
                }, searchParameters),

                scope: this
            });
        },

    }
});