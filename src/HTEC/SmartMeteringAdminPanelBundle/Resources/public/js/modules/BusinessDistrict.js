HCM.define({

    name: 'Modules.BusinessDistrict',

    extendFrom: 'Modules.CrudModule',

    construct: function(app, permissions) {

        this.callConstructor('Modules.CrudModule', [app, permissions]);

        this.getFormModuleClassname = function() {
            return 'Modules.Form.BusinessDistrict';
        };

        this.getModuleName = function() {
            return 'BusinessDistrict';
        };
    },

    methods: {

        initModel: function() {
            this.setModel(this.getApp().getModel('Model.BusinessDistrict'));
        },

        setControls: function() {

            this.super('setControls');

            var self = this;
            var cityFieldModelData = this.getModel().getFieldByName('city');

            var businessDistrictsGrid = $("#businessDistricts-BusinessDistrict-grid").grid({

                autoLoad: false,
                cellEdit: true,
                cellEditType: 'callback',
                model: [
                    {
                        header: 'ID',
                        id: 'id',
                        dataMap: 'id'
                    },
                    {
                        header: 'Name',
                        id: 'name',
                        dataMap: 'name',
                        editable: true
                    },
                    {
                        header: 'City',
                        id: 'city',
                        dataMap: 'city',
                        editable: true,
                        editOptions: {
                            type: 'select',
                            valueProperty: cityFieldModelData.valuePropertyName,
                            labelProperty: cityFieldModelData.labelPropertyName,
                        },
                        formatter: function(cellValue) {
                            if(cellValue && self.isObject(cellValue)) {
                                var fieldModelData = self.getModel().getFieldByName('city');
                                return cellValue[fieldModelData.labelPropertyName];
                            }
                        }
                    },
                    {
                        header: 'Manager',
                        id: 'manager',
                        dataMap: 'manager',
                        editable: true
                    },
                    {
                        header: 'Phone Number',
                        id: 'phoneNumber',
                        dataMap: 'phoneNumber',
                        editable: true
                    },
                    {
                        header: 'Address',
                        id: 'address',
                        dataMap: 'address',
                        editable: true
                    },
                    {
                        header: 'Code',
                        id: 'code',
                        dataMap: 'code',
                        editable: true
                    }
                ]
            });

            this.setMainViewComponent(businessDistrictsGrid);
        }
    }
});