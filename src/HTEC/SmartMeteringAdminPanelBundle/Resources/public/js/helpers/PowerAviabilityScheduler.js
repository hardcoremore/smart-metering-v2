HCM.define({

    name: 'Helpers.PowerAviabilityScheduler',

    extendFrom: 'Helpers.Scheduler',

    construct: function(app) {
        this.callConstructor('Helpers.Scheduler', [app]);
    },

    methods: {

        getSchedulerRowIdFromRecord: function(record) {
            var date = moment(record.powerActivate.date);
            return this.getDayNameFromDate(date);
        },

        createSchedulerEventDataFromRecord: function(record) {

            var date = moment(record.powerActivate.date);
            var startTime = moment(record.powerActivate.time);
            var endTime = moment(record.powerDeactivate.time);

            var start = date.clone().set({
                'hour': startTime.hour(),
                'minute': startTime.minute()
            });

            var end = date.clone().set({
                'hour': endTime.hour(),
                'minute': endTime.minute()
            });

            var themeName = '';
            var status = 'offline';
            var title = this.getModel().getPowerAviabilityTitle(record.powerActivate);

            if(record.powerActivate.type === 'power-activate' && record.powerDeactivate.type === 'power-deactivate') {
                themeName = 'jq-scheduler-event-blue';
            }
            else {
                themeName = 'jq-scheduler-event-green';
                status = 'online';
                title = 'Feeder: ' + record.powerDeactivate.powerTransformerFeeder.name + ' is still online or someone has forgot to turn it off';
            }


            var schedulerData = {

                title: title,
                start: start.format(),
                end: end.format(),
                id: record.powerActivate.id + '-'  + record.powerDeactivate.id,
                status: status,
                recordData: record,
                theme: themeName
            };

            return schedulerData;
        },

        createTooltipForSchedulerEvent: function(eventEl, eventData) {

            var tooltipContent = this.getTooltipContent(eventData.recordData.powerActivate.id + '-'  + eventData.recordData.powerDeactivate.id);

            if(tooltipContent === undefined) {

                var templateData = {
                    powerActivateTimeLabel: moment(eventData.start).format('HH:mm'),
                    powerDeactivateTimeLabel: moment(eventData.end).format('HH:mm'),
                    powerActivate: eventData.recordData.powerActivate,
                    powerActivate: eventData.recordData.powerDeactivate
                };

                tooltipContent = this.getApp().compileTemplate(
                    'power-aviability-tooltip-template',
                    $.extend(templateData, eventData.recordData)
                );

                tooltipContent = $(tooltipContent);

                this.setTooltipContent(eventData.recordData.id, tooltipContent);
            }
             
            var numberOfItems = tooltipContent.find('.scheduler-event-tooltip-item').length;

            var themeName = '';

            if(eventData.status === 'offline') {
                themeName = 'tooltipster-scheduler-blue';
            }
            else {
                themeName = 'tooltipster-scheduler-green';
            }

            eventEl.tooltipster({
                content: tooltipContent,
                postion: 'top',
                anchor: 'top-center',
                offset: [0, numberOfItems * 40],
                minWidth: 550,
                maxWidth: 800,
                plugins: ['laa.follower'],
                animationDuration: 0,
                theme: 'tooltipster-default ' + themeName
            });
        }
    }
});