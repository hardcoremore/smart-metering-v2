HCM.define({

    name: 'Helpers.PowerScheduleScheduler',

    extendFrom: 'Helpers.Scheduler',

    construct: function(app) {
        this.callConstructor('Helpers.Scheduler', [app]);
    },

    methods: {

        getSchedulerRowIdFromRecord: function(record) {
            if(record.repeatType && record.repeatDate) {
                return this.getDayNameFromDate(moment(record.repeatDate));
            }
            else {
                return this.getDayNameFromDate(moment(record.date));
            }
        },

        createSchedulerEventDataFromRecord: function(record) {

            var date = moment(record.date);
            var startTime = moment(record.startTime);
            var endTime = moment(record.endTime);

            if(record.repeatType && record.repeatDate) {
                date = moment(record.repeatDate);
            }

            var start = date.clone().set({
                'hour': startTime.hour(),
                'minute': startTime.minute()
            });

            var end = date.clone().set({
                'hour': endTime.hour(),
                'minute': endTime.minute()
            });

            var themeName = '';

            if(record.powerType === 'captive') {
                themeName = 'jq-scheduler-event-yellow';
            }
            else if(record.powerType === 'national') {
                themeName = 'jq-scheduler-event-green';
            }
            else {
                themeName = 'jq-scheduler-event-blue';
            }

            if(record.status !== 'approved') {
                themeName = themeName + '-waiting';
            }

            var schedulerData = {

                title: this.getModel().getPowerScheduleTitle(record),
                start: start.format(),
                end: end.format(),
                id: record.id,
                powerType: record.powerType,
                status: record.status,
                recordData: record,
                theme: themeName
            };

            return schedulerData;
        },

        createTooltipForSchedulerEvent: function(eventEl, eventData) {

            var tooltipContent = this.getTooltipContent(eventData.recordData.id);

            if(tooltipContent === undefined) {

                var templateData = {
                    startTimeLabel: moment(eventData.recordData.startTime).format('HH:mm'),
                    endTimeLabel: moment(eventData.recordData.endTime).format('HH:mm')
                };

                if(eventData.recordData.repeatEnd) {
                    templateData.repeatEndLabel = moment(eventData.recordData.repeatEnd).format('YYYY-MM-DD');
                }

                tooltipContent = this.getApp().compileTemplate(
                    'scheduler-event-tooltip-template',
                    $.extend(templateData, eventData.recordData)
                );

                tooltipContent = $(tooltipContent);

                this.setTooltipContent(eventData.recordData.id, tooltipContent);
            }
             
            var numberOfItems = tooltipContent.find('.scheduler-event-tooltip-item').length;

            var themeName = '';

            if(eventData.recordData.powerType === 'captive') {
                themeName = 'tooltipster-scheduler-yellow';
            }
            else if(eventData.recordData.powerType === 'national') {
                themeName = 'tooltipster-scheduler-green';
            }
            else {
                themeName = 'tooltipster-scheduler-blue';   
            }

            if(eventData.recordData.status !== 'approved') {

                themeName = themeName + '-waiting';
            }

            eventEl.tooltipster({
                content: tooltipContent,
                postion: 'top',
                anchor: 'top-center',
                offset: [0, numberOfItems * 40],
                minWidth: 550,
                maxWidth: 800,
                plugins: ['laa.follower'],
                animationDuration: 0,
                theme: 'tooltipster-default ' + themeName
            });
        }
    }
});