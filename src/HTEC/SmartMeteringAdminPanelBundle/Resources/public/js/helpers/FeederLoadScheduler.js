HCM.define({

    name: 'Helpers.FeederLoadScheduler',

    extendFrom: 'Helpers.Scheduler',

    construct: function(app) {
        this.callConstructor('Helpers.Scheduler', [app]);
    },

    methods: {

        getSchedulerRowIdFromRecord: function(record) {
            return this.getDayNameFromDate(moment(record.date));
        },

        createSchedulerEventDataFromRecord: function(record) {

            var date = moment(record.date);
            var startTime = moment(record.time);
            var endTime = startTime.clone().add(1, 'hour');

            var start = date.clone().set({
                'hour': startTime.hour(),
                'minute': startTime.minute()
            });

            var end = date.clone().set({
                'hour': endTime.hour(),
                'minute': endTime.minute()
            });


            var themeName = '';
            var status = 'offline';

            if(record.feederLoad) {
                themeName = 'jq-scheduler-event-blue';
            }
            else {
                themeName = 'jq-scheduler-event-green';
            }


            var schedulerData = {

                title: record.feederLoad,
                start: start.format(),
                end: end.format(),
                id: record.id,
                recordData: record,
                theme: themeName
            };

            return schedulerData;
        },

        createTooltipForSchedulerEvent: function(eventEl, eventData) {

            var tooltipContent = this.getTooltipContent(eventData.id);

            if(tooltipContent === undefined) {

                var templateData = {
                    date: moment(eventData.recordData.date).format('MMMM Do, dddd YYYY'),
                    start: moment(eventData.start).format('HH:mm'),
                    end: moment(eventData.end).format('HH:mm'),
                    feeder: eventData.recordData.powerTransformerFeeder,
                    load: eventData.recordData.feederLoad,
                    createdBy: eventData.recordData.createdBy
                };

                tooltipContent = this.getApp().compileTemplate(
                    'feeder-load-tooltip-template',
                    templateData
                );

                tooltipContent = $(tooltipContent);

                this.setTooltipContent(eventData.recordData.id, tooltipContent);
            }
             
            var numberOfItems = tooltipContent.find('.scheduler-event-tooltip-item').length;

            var themeName = '';

            if(eventData.recordData.feederLoad) {
                themeName = 'tooltipster-scheduler-blue';
            }
            else {
                themeName = 'tooltipster-scheduler-blue-waiting';
            }

            eventEl.tooltipster({
                content: tooltipContent,
                postion: 'top',
                anchor: 'top-center',
                offset: [0, numberOfItems * 40],
                minWidth: 550,
                maxWidth: 800,
                plugins: ['laa.follower'],
                animationDuration: 0,
                theme: 'tooltipster-default ' + themeName
            });
        }
    }
});