HCM.define({

    name: 'Helpers.Scheduler',

    construct: function(app) {

        var mainApp = app;
        var model;
        var tooltipContent = {};

        this.getApp = function() {
            return mainApp;
        };

        this.setModel = function(m) {
            model = m;
        };

        this.getModel = function() {
            return model;
        };

        this.setTooltipContent = function(id, content) {
            tooltipContent[id] = content;
        };

        this.getTooltipContent = function(id) {
            return tooltipContent[id];
        };

        this.removeTooltipContent = function(id) {
            delete tooltipContent[id];
        };

        this.resetTooltipContent = function() {
            tooltipContent = {};
        };        

        this.getPowerSchedulesTooltipContent = function() {
            return tooltipContent;
        };
    },

     methods: {

        addRecordToScheduler: function(scheduler, record) {

            try {
                var rowId = this.getSchedulerRowIdFromRecord(record);
                var schedulerData = this.createSchedulerEventDataFromRecord(record);

                scheduler.scheduler('createEvent', rowId, schedulerData);
            }
            catch(error) {
                console.log(error);
            }
        },

        getSchedulerRowIdFromRecord: function(record) {
            var date = moment(record.date);
            return this.getDayNameFromDate(date);
        },

        getDayNameFromDate: function(date) {

            var dayName = date.format('dddd');

            return dayName.toString().firstCharacterToLowerCase();
        }
    }
});
