HCM.define({
    
    name: 'App.SmartMeteringAdminPanelClient',

    extendFrom: 'App.LoginApplication',

    construct: function(c) {

        this.callConstructor('App.LoginApplication', [c]);

        var permissionModel;
        var roleModel;
        var mainMenu;

        this.setPermissionModel = function(model) {
            permissionModel = model;
        };

        this.getPermissionModel = function() {
            return permissionModel;
        };

        this.setRoleModel = function(model) {
            roleModel = model;
        };

        this.getRoleModel = function() {
            return roleModel;
        };

        this.setMainMenu = function(menuEl) {
            mainMenu = menuEl;
        };

        this.getMainMenu = function() {
            return mainMenu;
        };
    },

    methods: {

        startEntryModule: function() {
            this.super('startEntryModule');
            this.showNotification('information', 'Welcome to smart metering.');
        },

        enterApplication: function() {

            // remove buttons that open modules which use does not have access to
            var permissionKeys = Object.keys(this.getUserModel().getCurrentUser().permissions);

            this.getMainMenu().find('[data-module]').each(function(index){

                var element = $(this);
                var parent = element.parent();
                var moduleName = element.data('module'); 

                if(moduleName === 'Logout') {
                    return;
                }

                if(permissionKeys.indexOf(moduleName) === -1) {
                    
                    if(parent.parent().children().length === 1) {
                        parent.parent().parent().remove();
                    }
                    else {
                        element.parent().remove();  
                    }
                }
            });

            this.super('enterApplication');
        }
    }
});
