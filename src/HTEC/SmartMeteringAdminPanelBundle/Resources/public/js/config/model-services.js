HCM.createNamespace('Services.SmartMetering.Models', {

    'Model.Permission': {
        parent: 'Base.Model',
    },

    'Model.City': {
        parent: 'Base.Model'
    },

    'Model.BusinessDistrict': {
        parent: 'Base.Model',
        arguments: ['@Model.City']
    },



    'Model.Feeder': {
        parent: 'Base.Model'  
    },

    'Model.Tariff': {
        parent: 'Base.Model'
    },

    'Model.Customer': {
        parent: 'Base.Model',
        arguments: ['@Model.Tariff', '@Model.BusinessDistrict']
    },

    'Model.CustomerNetworkData': {
        parent: 'Base.Model',
        arguments: [
            '@Model.Customer',
            '@Model.InjectionSubstation',
            '@Model.PowerTransformer',
            '@Model.Feeder',
            '@Model.ElectricPole',
            '@Model.DistributionTransformer'
        ]
    },

    'Model.CustomerCategory': {
        parent: 'Base.Model'
    },

    'Model.PowerSchedule': {
        parent: 'Base.Model',
        arguments: [
            '@Model.PowerSource',
            '@Model.BusinessDistrict',
            '@Model.InjectionSubstation',
            '@Model.PowerTransformer',
            '@Model.Feeder',
            '@Model.DistributionTransformer'
        ]
    },

    'Model.PowerActivateDeactivate': {
        parent: 'Base.Model',
        arguments: [
            '@Model.BusinessDistrict',
            '@Model.InjectionSubstation',
            '@Model.PowerTransformer',
            '@Model.Feeder'
        ]
    },

    'Model.FeederLoad': {
        parent: 'Base.Model',
        arguments: [
            '@Model.BusinessDistrict',
            '@Model.InjectionSubstation',
            '@Model.PowerTransformer',
            '@Model.Feeder'
        ]
    },



    'Model.EstimatedLoad': {
        parent: 'Base.Model',
        arguments: ['@Model.Customer', '@Model.CustomerCategory']
    },

    'Model.EstimatedLoadAppliance': {
        parent: 'Base.Model',
        arguments: ['@Model.EstimatedLoad', '@Model.ApplianceType', '@Model.Appliance']
    },

    'Model.EstimatedConsumption': {
        parent: 'Base.Model',
        arguments: ['@Model.Customer']
    },

    'Model.PowerForecast': {
        parent: 'Base.Model',
        arguments: ['@Model.PowerPurchase']
    },

    'Model.PowerAllocate': {
        parent: 'Base.Model',
        arguments: ['@Model.PowerForecast']
    },

    'Model.PowerAllocateData': {
        parent: 'Base.Model',
        arguments: ['@Model.PowerAllocate', '@Model.BusinessDistrict']
    },



    'Model.Role': {
        parent: 'Base.Model',
        arguments: ['@Model.Permission']
    },

    'Model.AdminPanelUser': {
        parent: 'Base.Model',
        arguments: ['@Model.Role']
    },

    'Model.PowerTransformer': {
        parent: 'Base.Model',
        arguments: ['@Model.InjectionSubstation', '@Model.Feeder']
    },

    'Model.ElectricPole': {
        parent: 'Base.Model',
        arguments: ['@Model.Feeder']
    },

    'Model.DistributionTransformer': {
        parent: 'Base.Model',
        arguments: [
            '@Model.BusinessDistrict',
            '@Model.InjectionSubstation',
            '@Model.PowerTransformer',
            '@Model.Feeder'
        ]
    },

    'Model.Meter': {
        parent: 'Base.Model',
        arguments: ['@Model.Customer', '@Model.DistributionTransformer', '@Model.Feeder']
    },


    'Model.PowerSource': {
        parent: 'Base.Model',
        arguments: ['@Model.Feeder']
    },

    'Model.InjectionSubstation': {
        parent: 'Base.Model',
        arguments: ['@Model.BusinessDistrict', '@Model.PowerSource', '@Model.Feeder']
    },

    'Model.PowerPurchase': {
        parent: 'Base.Model',
        arguments: ['@Model.PowerSource']
    },

    'Model.PowerAviability': {
        parent: 'Base.Model',
        arguments: ['@Model.BusinessDistrict', '@Model.InjectionSubstation', '@Model.PowerTransformer', '@Model.Feeder']
    },

   

    'Model.ApplianceType': {
        parent: 'Base.Model',
        arguments: ['@Model.CustomerCategory']
    },

    'Model.Appliance': {
        parent: 'Base.Model',
        arguments: ['@Model.ApplianceType']
    },

    'Model.OfflineMeterReading': {
        parent: 'Base.Model'
    },

    'Model.OnlineMeterReading': {
        parent: 'Base.Model'
    },
    
    'Model.OfflinePrepaidVendReading': {
        parent: 'Base.Model'
    },
    
    'Model.GridMeterReading': {
        parent: 'Base.Model'
    },
    
    'Model.Promotion': {
        parent: 'Base.Model'
    }

});
