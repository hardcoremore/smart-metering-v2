HCM.define({

    name: 'SmartMeteringAdminPanel.Base.Bootstrap',

    extendFrom: 'Base.Bootstrap',

    construct: function() {
        this.callConstructor('Base.Bootstrap');
    },

    methods: {

        initParameters: function() {

            var params = this.super('initParameters');
            
            var smartMeteringGlobalParameters = HCM.getNamespaceValue('Global.SmartMetering.Parameters');
            var smartMeteringLocalParameters = HCM.getNamespaceValue('Local.Parameters');

            var combinedParameters = $.extend({}, params, smartMeteringGlobalParameters, smartMeteringLocalParameters);

            this.setParameters(combinedParameters);

            return combinedParameters;
        },

        initServiceManagerConfig: function() {

            var config = this.super('initServiceManagerConfig');

            var smartMeteringModelServices = HCM.getNamespaceValue('Services.SmartMetering.Models');

            var combinedParams = $.extend({}, config, smartMeteringModelServices);

            this.setServiceManagerConfig(combinedParams);

            return combinedParams;
        },

        initApp: function() {

            var app = HCM.getInstance('App.SmartMeteringAdminPanelClient', [this.getConfig()]);

            var constants = HCM.getNamespaceValue('Static.Constants');
            var localStorage = HCM.getInstance('Base.LocalStorage');

            var moduleManager = HCM.getInstance('Base.ModuleManager', [app]);
            var historyManager = HCM.getInstance('Base.HistoryManager', [app]);

            var historyController = HCM.getInstance('Controllers.History', [app]);
            var moduleController = HCM.getInstance('Controllers.Module', [app]);

            var permissionModel = this.getServiceManager().getService('Model.Permission');
            var roleModel = this.getServiceManager().getService('Model.Role');
            var adminPanelUserModel = this.getServiceManager().getService('Model.AdminPanelUser');

            var moduleContainer = $("#main-module-container");
            var loaderElement = $("#main-loader");
            var loginModuleHolder = $("#login-module-holder")
            var appContentHolder = $("#app-content-all-holder");
            var mainMenu = $('#main-menu').superfish({
                speed: 'fast',
                speedOut: 'fast',
                delay: 250
            });

             /**
             *
             * INJECT APPLIACTION DEPENDENCIES
             *
             **/

            // APP BASE
            app.setConstants(constants);
            app.setLocalStorage(localStorage);
            app.setRequester(this.getRequester());
            
            // MANAGERS
            app.setServiceManager(this.getServiceManager());
            app.setModuleManager(moduleManager);
            app.setHistoryManager(historyManager);

            // CONTROLLERS
            app.setHistoryController(historyController);
            app.setModuleController(moduleController);

            // MODELS
            app.setPermissionModel(permissionModel);
            app.setRoleModel(roleModel);
            app.setUserModel(adminPanelUserModel);

            // DOM ELEMENTS
            app.setModuleContainer(moduleContainer);
            app.setLoaderElement(loaderElement);
            app.setLoginModuleHolder(loginModuleHolder);
            app.setAppContentHolder(appContentHolder);
            app.setMainMenu(mainMenu);

            this.setApp(app);

            return app;
        }
    }
});