/*
* jQuery Scheduler
*
* @version v1.0 (29 Aug 2014)
*
* Copyright 2014, High Tech Engineering Center (HTEC).
* 
*
* Homepage:
* http://www.htec.rs
* 
*
* Authors:
*   ÄŒaslav Å abani.
*
*
* Dependencies:
*   jQuery v1.6+
*    jQuery UI v1.8+
*/
;(function($) {
    
    $.widget('ui.scheduler', {
        options: {

            onEventClick: null,
            rowsModel: null,

            // time interval in minutes of how much each colon will be apart from other
            timeInterval: 60,
            allowIntersectingEvents: false,
            eventWidth: 0,
            events: null,
            currentlySelectedEvent: null,
            createEventTooltipCallback: null, // function(eventEl, eventData)
            removeEventTooltipCallback: null // function(eventEl)
        },
        
        _rowsModel:[
            {
                title: 'Monday',
                id: 'monday'
            },
            {
                title: 'Tuesday',
                id: 'tuesday'
            },
            {
                title: 'Wednesday',
                id: 'wednesday'
            },
            {
                title: 'Thursday',
                id: 'thursday'
            },
            {
                title: 'Friday',
                id: 'friday'
            },
            {
                title: 'Saturday',
                id: 'saturday'
            },
            {
                title: 'Sunday',
                id: 'sunday'
            }
        ],

        _events: null,
        _resizeTimeout: null,
        _timeLabelsNumber: 0,
        _resizeSensor: null,
        _schedulerWidth: 0,

        _create: function() {

            var that = this;

            this._rowsModel = this.options.rowsModel || this._rowsModel;

            this._events = this.options.events || {};

            this.element.addClass('jq-scheduler');

            this.leftTopSpacer = $("<div/>", {
                class: "jq-scheduler-left-top-spacer"
            });

            this.element.append(this.leftTopSpacer);

            this.timeLabelsCnt = $("<div/>", {
                class: "jq-scheduler-time-labels"
            });

            this.element.append(this.timeLabelsCnt);

            this._on(window, {
                resize: this._windowResizeHandler
            });

            this._timeLabelsNumber = 1440 / this.options.timeInterval;

            var timeLabel;
            var timeInMinues = 0;
            var timeString = "";

            var hoursCalculated = 0;
            var minutesCalculated = 0;


            this.rowLabelsCnt = $("<div/>", {
                class: "jq-scheduler-rowlabels-cnt"
            });

            this.element.append(this.rowLabelsCnt);


            this.eventsContainer = $("<div/>", {
                class: "jq-scheduler-events-cnt"
            });

            this.element.append(this.eventsContainer);

            this.controlsCnt = $("<div/>", {
                class: "jq-scheduler-controls-cnt"
            });

            this.element.append(this.controlsCnt);

            this.options.eventWidth = 100 / this._timeLabelsNumber;

            for(var ti = this._timeLabelsNumber; ti > 0; ti--) {
                
                if(timeInMinues === 0) {
                    timeString = "00:00";
                }
                else {

                    hoursCalculated = timeInMinues / 60;
                    minutesCalculated = timeInMinues % 60;

                    // check if hoursCalculated number is not a whole number
                    if(hoursCalculated  % 1 !==  0) {
                        hoursCalculated = Math.floor(hoursCalculated);
                    }

                    if(hoursCalculated.toString().length === 1) {
                        hoursCalculated = "0" + hoursCalculated.toString();
                    }

                    if(minutesCalculated.toString().length === 1) {
                        minutesCalculated = "0" + minutesCalculated.toString();
                    }

                    timeString = hoursCalculated + ":" + minutesCalculated;

                }

                timeLabel = $("<div/>", {
                    class: 'jq-scheduler-time-label',
                    'data-time': timeString
                }).append($("<span/>", {
                    html: timeString
                }));

                timeLabel.css('width', that.options.eventWidth + '%');

                this.timeLabelsCnt.append(timeLabel);

                timeInMinues += this.options.timeInterval;
            }

            this._createRows();

            this._resizeSensor = new ResizeSensor(this.element[0], this._schedulerResizeEvent.bind(this));

            var self = this;

            setTimeout(function(){
                self._schedulerWidth = self.element.outerWidth();
            }, 300);
        },

        _schedulerResizeEvent: function() {
            clearTimeout(this._resizeTimeout);
            this._resizeTimeout = this._delay(this._repositionElementsOnTimeScale, 500);
        },

        _windowResizeHandler: function(ev) {

            clearTimeout(this._resizeTimeout);
            this._resizeTimeout = this._delay(this._repositionElementsOnTimeScale, 500);
        },

        _redrawScheduler: function() {

            var keys = Object.keys(this._events);
            var rowId, rowEvents;

            // remove all rendered events
            for(var i = 0, len = keys.length; i < len; i++) {
                
                rowId = keys[i];

                rowEvents = this._events[rowId];

                if(rowEvents) {
                    for(var s = 0, slen = rowEvents.length; s < slen; s++) {
                        this.removeEventFromRow(rowId, rowEvents[s].id);
                    }
                }
            }

            // render all events again
            for(var i = 0, len = keys.length; i < len; i++) {

                rowId = keys[i];

                rowEvents = this._events[rowId];

                if(rowEvents) {

                    for(var s = 0, slen = rowEvents.length; s < slen; s++) {
                        this._renderEvent(rowId, rowEvents[s]);
                    }
                }
            }
        },

        _createRows: function() {

            this.rowLabelsCnt.empty();
            this.eventsContainer.empty();

            var rowLabel;
            var elementRow;

            var element;

            for(var index in this._rowsModel) {
                    
                element = this._rowsModel[index];

                rowLabel = $('<div/>', {
                    class: 'jq-scheduler-row-label',
                    'data-label-row-id': element.id.toString().toLowerCase()
                }).text(element.title);

                this.rowLabelsCnt.append(rowLabel);

                elementRow = $('<div/>', {
                    class: 'jq-scheduler-event-row jq-scheduler-event-row-' + index,
                    'data-row-id': element.id
                });

                this.eventsContainer.append(elementRow);
            }
        },

        reinitializeWithNewModel: function(rowsModel) {

            this.removeAllEvents();
            this._rowsModel = rowsModel;
            this._createRows();
        },

        isEventIntersecting: function(rowId, eventData) {

            var isIntersecting = false;
            

            var newEventStart = new Date(eventData.start);
                newEventStart.setMilliseconds(0);
                newEventStart.setSeconds(0);

            var newEventEnd = new Date(eventData.end);
                newEventEnd.setMilliseconds(0);
                newEventEnd.setSeconds(0);

            var start = null;
            var end = null;

            var currentEvent = null;

            var rowEvents = this._events[rowId];

            for(var eventIndex in rowEvents) {
                
                currentEvent = rowEvents[eventIndex];

                start = new Date(currentEvent.start);
                start.setMilliseconds(0);
                start.setSeconds(0);

                end = new Date(currentEvent.end);
                end.setMilliseconds(0);
                end.setSeconds(0);

                // check if start date is intersecting
                if(newEventStart.getTime() === start.getTime() || 
                    (newEventStart.getTime() > start.getTime() && newEventStart.getTime() < end.getTime())
                ) {
                    isIntersecting =  true;
                    break;
                }
                // check if end date is intersecting
                else if(newEventEnd.getTime() === end.getTime() || 
                    (newEventEnd.getTime() > start.getTime() && newEventEnd.getTime() < end.getTime())
                ) {
                    isIntersecting =  true;
                    break;
                }
                // check if start and end date are wider
                else if(newEventStart.getTime() < start.getTime() && newEventEnd.getTime() > end.getTime()) {
                    isIntersecting =  true;
                    break;
                }
            }

            return isIntersecting;
        },

        createEvent: function(rowId, eventData) {

            var isEventIntersecting = this.isEventIntersecting(rowId, eventData);

            if(this.options.allowIntersectingEvents === false && isEventIntersecting === true) {

                throw new Error(
                    "Event on a row id: " + rowId + " is intersecting with other events.",
                    1400
                );
            }

            if(typeof eventData.start === "string") {
                eventData.start = new Date(eventData.start);
            }

            if(typeof eventData.end === "string") {
                eventData.end = new Date(eventData.end);
            }

            if(eventData.start.getTime() > eventData.end.getTime()) {
                
                throw new Error(
                    "End time must be greater than start time.",
                    1400
                );
            }

            var rowIdEvents = this._events[rowId];

            if(typeof rowIdEvents === 'undefined') {
                rowIdEvents = [];
            }

            eventData.rowId = rowId;

            rowIdEvents.push(eventData);

            this._events[rowId] = rowIdEvents;

            this._renderEvent(rowId, eventData)
        },
        
        getAllEvents: function() {
            return this._events;
        },

        getRowsModel: function() {
            return this._rowsModel;
        },

        _renderEvent: function(rowId, eventData) {

            var newEventStart = new Date(eventData.start);
            var newEventEnd = new Date(eventData.end);

            var classes = eventData.theme || '';

            if(newEventStart.getDate() === newEventEnd.getDate()) {

                classes = classes + ' jq-scheduler-event-single';

                this._createEventElement(rowId, newEventStart, newEventEnd, eventData, classes);
            }
            else {

                newEventEnd.setHours(0);

                var eventEnd = new Date(eventData.end);

                var classRight = classes + ' jq-scheduler-event-split-right';

                if(eventData.class) {
                    classRight += ' ' + eventData.class;
                }

                var classLeft = classes + ' jq-scheduler-event-split-left';

                if(eventData.class) {
                    classLeft += ' ' + eventData.class;
                }

                this._createEventElement(rowId, newEventStart, newEventEnd, eventData, classLeft);
                this._createEventElement(rowId, newEventEnd, eventEnd, eventData, classRight, true);
            }
        },

        _createEventElement: function(rowId, start, end, eventData, additionalClass, spawnInNext) {

            var eventId = 'scheduler-event-' + rowId + '-' + (Math.random() * 99999999999999999);

            var eventEl = $("<div/>", {
                class: 'jq-scheduler-event ' + additionalClass,
                text: eventData.title,
                title: eventData.title,
                'data-id': eventData.id,
                'data-row-id': rowId,
                'id': eventId
            });

            this._on(eventEl, {
                click: this._eventClickEventHandler
            });

            var startHoursString = start.getHours().toString();

            if(start.getHours() < 10) {
                startHoursString = "0" + startHoursString.toString();
            }

            var timeLabelEl = this._getTimeLabelElFromHours(startHoursString);
            var timeLabelWidth = timeLabelEl.outerWidth();
            
            var timeLabelQuarterWidth = timeLabelWidth / 4;

            var startQuarterLeftModifier = this._getQuarterModifier(start.getMinutes()) * timeLabelQuarterWidth;
            var endQuarterLeftModifier = this._getQuarterModifier(end.getMinutes()) * timeLabelQuarterWidth;

            var endHours = end.getHours();

            if(endHours === 0) {
                endHours = 24;
            }
            
            var hoursDifference = Math.abs(endHours - start.getHours());

            eventEl.data('timeData', {
                'hoursDifference': hoursDifference,
                'startHours': startHoursString,
                'startMinutes': start.getMinutes(),
                'endHours': end.getHours(),
                'endMinutes': end.getMinutes()
            });

            var rowEl = this._getRowElById(rowId);
                rowEl.append(eventEl);

            var eventWidth = timeLabelWidth * hoursDifference;

            if(eventWidth === 0) {
                eventWidth = timeLabelWidth;
            }

            eventWidth = eventWidth - startQuarterLeftModifier + endQuarterLeftModifier;

            var newElementStartPosition = startQuarterLeftModifier + (timeLabelWidth *  start.getHours());

            var newElementDisplayData = {
                width: eventWidth,
                left: newElementStartPosition,
                height: eventEl.outerHeight()
            };
            
            eventEl.css('width', eventWidth);
            eventEl.css('left', newElementStartPosition);

            if(spawnInNext === true) {
                timeLabelEl = timeLabelEl.next();
            }

            if(spawnInNext === true) {
                rowEl = rowEl.next();
            }

            var newElementTopLeft = newElementStartPosition + newElementDisplayData.width;
            newElementDisplayData.topLeft = newElementTopLeft;

            var topMargin = 8;
            var topPosition = topMargin;

            var intersectingPositions = [];
            var elementLevelPosition = 0;
            var displayData = null;

            rowEl.children().each(function(index){

                var element = $(this);

                // skip element that we are adding
                if(element.attr('id') === eventId) {
                    return;
                }

                displayData = element.data('displayData');

                var isNotIntersecting = (displayData.left >= newElementTopLeft) || (newElementStartPosition >= displayData.topLeft);

                if(isNotIntersecting === false) {

                    if(displayData.top === topPosition) {
                        topPosition = displayData.top + displayData.height + topMargin;
                        elementLevelPosition++;
                    }
                    else {
                        // add only intersecting events that not caused level elevation
                        intersectingPositions.push(displayData);
                    }
                }
            });


            if(intersectingPositions.length > 0) {

                // loop through events that were intersecting on a different level and elevate position if necessary 
                for(var i = 0, len = intersectingPositions.length; i < len; i++) {

                    displayData = intersectingPositions[i];

                    if(displayData.level === elementLevelPosition) {
                        topPosition = displayData.top + newElementDisplayData.height + topMargin;
                        elementLevelPosition++;  
                    }
                }
            }

            newElementDisplayData.level = elementLevelPosition;

            newElementDisplayData.top = topPosition;

            eventEl.css('top', topPosition);
            eventEl.data('displayData', newElementDisplayData);

            // rowEl.append(eventEl);

            var parentHeight = 0;

            rowEl.children().each(function(index){

                var element = $(this);
                var displayData = element.data('displayData');

                parentHeight = Math.max(parentHeight, displayData.top + displayData.height + topMargin);
            });

            rowEl.css('height', parentHeight);

            this._setRowLabelHeight(rowId, parentHeight);

            if(this.options.createEventTooltipCallback) {
                this.options.createEventTooltipCallback(eventEl, eventData);
            }

            var schedulerWidth = this.element.outerWidth();

            if(this._schedulerWidth !== schedulerWidth) {
                this._schedulerWidth = schedulerWidth;
                this._repositionElementsOnTimeScale();
            }
        },

        _repositionElementsOnTimeScale: function() {

            var self = this;

            this.element.find('.jq-scheduler-event').each(function(){

                var element = $(this);
                displayData = element.data('displayData');
                timeData = element.data('timeData');

                var timeLabelEl = self._getTimeLabelElFromHours(timeData.startHours);
                var timeLabelWidth = timeLabelEl.outerWidth();
                
                var timeLabelQuarterWidth = timeLabelWidth / 4;

                var startQuarterLeftModifier = self._getQuarterModifier(timeData.startMinutes) * timeLabelQuarterWidth;
                var endQuarterLeftModifier = self._getQuarterModifier(timeData.endMinutes) * timeLabelQuarterWidth;

                var eventWidth = timeLabelWidth * timeData.hoursDifference;

                if(eventWidth === 0) {
                    eventWidth = timeLabelWidth;
                }

                eventWidth = eventWidth - startQuarterLeftModifier + endQuarterLeftModifier;

                var newElementStartPosition = startQuarterLeftModifier + (timeLabelWidth *  timeData.startHours);

                displayData.left = newElementStartPosition;
                element.css('left', newElementStartPosition);
                element.data('displayData', displayData);
            });
        },

        _setRowLabelHeight: function(rowId, height) {
            this.rowLabelsCnt.find('.jq-scheduler-row-label[data-label-row-id="'+rowId+'"]').css('height', height);
        },

        _eventClickEventHandler: function(ev) {

            if(this.selectedEvent) {
                this.selectedEvent.removeClass('jq-scheduler-selected-event');
            }

            this.selectedEvent = $(ev.currentTarget);

            this.selectedEvent.addClass('jq-scheduler-selected-event');

            var id = this.selectedEvent.data('id');
            var rowEvents = this._events[this.selectedEvent.data('row-id')];

            var clickedEventData = null;

            for(var eventIndex in rowEvents) {

                if(rowEvents[eventIndex].id === id) {
                    clickedEventData = rowEvents[eventIndex];
                }
            }

            if(clickedEventData) {

                this._trigger('onEventClick', null, {
                    event: clickedEventData
                });
            }
        },

        removeAllEvents: function() {

            var that = this;
            var scheduledEventIndex = null;
            var scheduledEvent = null;

            var keys = Object.keys(this._events);
            var rowId;

            for(var i = 0, len = keys.length; i < len; i++) {
                this._removeAllRowEvents(keys[i]);
            }

            this._events = {};
        },

        _removeAllRowEvents: function(rowId) {

            var rowEvents = this._events[rowId];

            if(rowEvents) {

                for(var i = 0, len = rowEvents.length; i < len; i++) {
                    this.removeEventFromRow(rowId, rowEvents[i].id);
                }
            }

            this._setRowLabelHeight(rowId, 48);
            this._getRowElById(rowId).css('height', 48);

            this._events[rowId] = null;

            delete this._events[rowId];
        },

        removeEventFromRow: function(rowId, eventId) {

            var eventEl = this._getRowEventElById(rowId, eventId);

            if(eventEl.length > 0) {

                this._off(eventEl, 'click');

                eventEl.remove();

                if(this.options.removeEventTooltipCallback) {
                    this.options.removeEventTooltipCallback(eventEl);
                }
            }
        },

        _getRowElById: function(rowId) {
            return this.element.find(".jq-scheduler-event-row[data-row-id='"+rowId+"']");
        },

        _getEventElById: function(eventId) {
            return this.element.find(".jq-scheduler-event[data-id='"+eventId+"']");
        },

        _getRowEventElById: function(rowId, eventId) {
            return this._getRowElById(rowId).find(".jq-scheduler-event[data-id='"+eventId+"']");
        },

        _getTimeLabelElFromHours: function(hours) {
            return this.element.find(".jq-scheduler-time-label[data-time='" + hours + ":00']").first();
        },

        _getQuarterModifier: function(minutes) {

            var quarterLeftModifier = 0;

            if(minutes >= 15 && minutes < 30) {
                quarterLeftModifier = 1;
            }
            else if(minutes >= 30 && minutes < 45) {
                quarterLeftModifier = 2;
            }
            else if(minutes >= 45) {
                quarterLeftModifier = 3;
            }

            return quarterLeftModifier;
        }
    });

})(jQuery);