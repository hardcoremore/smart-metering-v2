HCM.define({

    name: 'Controllers.Module.PowerPurchase',

    extendFrom: 'Controllers.Crud',

    construct: function(app, module) {

        this.callConstructor('Controllers.Crud', [app, module]);

        var self = this;

        this.approvePowerPurchasePopupSubmitHandler = function(ev) {
            self.getModule().approvePowerPurchase(self.getModule().getSelectedRecord());
            self.getModule().getPowerPurchaseApprovePopup().popup('close');
        };

        var superRecordPatchCompleteEventHandler = this.recordPatchCompleteEventHandler;

        this.recordPatchCompleteEventHandler = function(ev) {
            
            superRecordPatchCompleteEventHandler(ev);

            if(ev.scope.fieldModelData.name === 'status') {

                var selectedData = self.getModule().getSelectedRecord();
                    selectedData['status'] = 'approved';

                self.getModule().setSelectedRecord(selectedData);

                self.getModule().getMainViewComponent().grid(
                    'updateRow',
                    self.getModule().getSelectedRecordIndex(), self.getModule().getSelectedRecord()
                );

                self.getApp().showNotification('success', 'You have successfully approved power purchase');
            }
        };
    },

    methods: {

        addEvents: function() {

            this.super('addEvents');

            this.getModule().getPowerPurchaseApprovePopup().popup('option', {
                onFormSubmitted: this.approvePowerPurchasePopupSubmitHandler,
            });
        },

        removeEvents: function() {

            this.super('removeEvents');

            this.getModule().getPowerPurchaseApprovePopup().popup('option', {
                onFormSubmitted: null,
            });
        },

        handleModuleControlButtonClick: function(actionType) {

            if(this.getModule().getMainViewComponent().grid('getTableState') === 'editing') {
                this.getApp().showNotification('warning', 'Please finish the grid cell editing first.');
                return;
            }

            if(actionType === 'approve-record') {

                if(this.getModule().getSelectedRecord()) {

                    if(this.getModule().getSelectedRecord().status !== 'approved') {
                        this.getModule().getPowerPurchaseApprovePopup().popup('open');
                    }
                    else {
                        this.getApp().showNotification('information', 'Selected power purchase is already approved');
                    }
                }
                else {
                    this.getApp().showNotification('warning', 'Please select power purchase to be approved');
                }
                
            }
            else {
                this.super('handleModuleControlButtonClick', [actionType]);
            }
       }
    }
});
