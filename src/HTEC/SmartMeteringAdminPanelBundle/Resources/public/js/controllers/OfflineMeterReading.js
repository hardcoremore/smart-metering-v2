HCM.define({

    name: 'Controllers.Module.OfflineMeterReading',

    extendFrom: 'Controllers.Crud',

    construct: function(app, module) {
        this.callConstructor('Controllers.Crud', [app, module]);
        var self = this;

        this.approveOfflineMeterReadingPopupSubmitHandler = function(ev) {
            self.getModule().getOfflineMeterReadingApprovePopup().popup('close');
            self.getModule().approveMeterReading(self.getModule().getSelectedRecord());  
        };

        var superRecordPatchCompleteEventHandler = this.recordPatchCompleteEventHandler;

        this.recordPatchCompleteEventHandler = function(ev) {
            
            superRecordPatchCompleteEventHandler(ev);

            if(ev.scope.fieldModelData.name === 'suspect') {

                var selectedData = self.getModule().getSelectedRecord();
                    selectedData['suspect'] = 'false';

                self.getModule().setSelectedRecord(selectedData);

                self.getModule().getMainViewComponent().grid(
                    'updateRow',
                    self.getModule().getSelectedRecordIndex(), self.getModule().getSelectedRecord()
                );

                self.getApp().showNotification('success', 'You have successfully approved offline meter reading.');
            }
        };
    },

    methods: {

        addEvents: function() {

            this.super('addEvents');

            this.getModule().getOfflineMeterReadingApprovePopup().popup('option', {
                onFormSubmitted: this.approveOfflineMeterReadingPopupSubmitHandler,
            });
        },

        removeEvents: function() {

            this.super('removeEvents');
            
            this.getModule().getOfflineMeterReadingApprovePopup().popup('option', {
                onFormSubmitted: null,
            });
        },

        handleModuleControlButtonClick: function(actionType) {

            if(actionType === 'view-image' || actionType === 'approve-record') {

                if(this.getModule().getSelectedRecord()) {

                    if(actionType === 'view-image') {
                        this.getModule().displayOfflineMeterReadingImage(this.getModule().getSelectedRecord());
                    }
                    else {

                        if(this.getModule().getSelectedRecord().suspect === true) {
                            this.getModule().getOfflineMeterReadingApprovePopup().popup('open');
                        }
                        else {
                            this.getApp().showNotification('information', 'You can only approve meter readings that are suspect.');        
                        }
                    }
                }
                else {
                    this.getApp().showNotification('warning', 'Please select the offline meter reading first.');
                }
            }
            else {
                this.super('handleModuleControlButtonClick', [actionType]);                
            }
        }
    }
});
