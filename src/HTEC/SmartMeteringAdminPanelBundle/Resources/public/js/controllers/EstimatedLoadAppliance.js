HCM.define({

    name: 'Controllers.Module.EstimatedLoadAppliance',

    extendFrom: 'Controllers.Crud',

    construct: function(app, module) {

        this.callConstructor('Controllers.Crud', [app, module]);

        var self = this;

        this.moduleReadyEventHandler = function(ev) {

            if(ev.module.getNamespace() === 'Modules.Form.EstimatedLoadApplianceForm') {

                self.getModule().setFormModule(ev.module);
                self.getModule().getFormModule().setModel(self.getModule().getModel());
                self.getModule().getFormModule().setFormMode('create');

                self.getApp().getModuleManager().startModule(ev.module);
            }
        };

        this.currentEstimatedLoadChangedEventHandler = function(ev) {

            if(self.getModule().getModel().getCurrentEstimatedLoad()) {
                self.getModule().readAllAppliances();    
            }
            else {
                self.getModule().getMainViewComponent().grid('removeAllRows');
            }
        };

        var superReadRecordsCompleteEventHandler = this.readRecordsCompleteEventHandler;
        this.readRecordsCompleteEventHandler = function(ev) {

            superReadRecordsCompleteEventHandler(ev);

            var totalLoad = self.getModule().calculateCurrentTotalLoad();
            self.getModule().updateCurrentTotalLoad(totalLoad);
        };

        var superRowSelectedEventHandler = this.rowSelectedEventHandler;
        this.rowSelectedEventHandler = function(ev, ui) {

            if(self.getModule().getSelectedRecordIndex() !== ui.rowIndex) {

                self.getModule().getFormModule().setEditingRecordData(ui.rowData);

                self.getModule().getFormModule().setFormMode('edit');
                self.getModule().getFormModule().getFormSubmitButton().find('.button-label').text('Save');

                self.getModule().getFormModule().populateForm(ui.rowData);
            }

            superRowSelectedEventHandler(ev, ui);
        };

        this.recordCreateCompleteEventHandler = function(ev) {

            self.getModule().getMainViewComponent().grid('addRow', ev.responseData);

            self.getModule().setSelectedRecord(null);
            self.getModule().setSelectedRecordIndex(-1);

            var totalLoad = self.getModule().calculateCurrentTotalLoad();
            self.getModule().updateCurrentTotalLoad(totalLoad);
        };

        var superRecordUpdateCompleteEventHandler = this.recordUpdateCompleteEventHandler;
        this.recordUpdateCompleteEventHandler = function(ev) {

            superRecordUpdateCompleteEventHandler(ev);

            var totalLoad = self.getModule().calculateCurrentTotalLoad();
            self.getModule().updateCurrentTotalLoad(totalLoad);
        };

        var superRecordPatchCompleteEventHandler = this.recordPatchCompleteEventHandler;
        this.recordPatchCompleteEventHandler = function(ev) {

            superRecordPatchCompleteEventHandler(ev);

            var totalLoad = self.getModule().calculateCurrentTotalLoad();
            self.getModule().updateCurrentTotalLoad(totalLoad);
        }

        var superRecordDeleteEventHandler = this.recordDeleteEventHandler;
        this.recordDeleteEventHandler = function(ev) {

            superRecordDeleteEventHandler(ev);

            if(self.getModule().getFormModule()) {
                self.getModule().getFormModule().resetForm();
                self.getModule().getFormModule().setFormMode('create');
                self.getModule().getFormModule().getFormSubmitButton().find('.button-label').text('Create');
            }

            self.getModule().setSelectedRecord(null);
            self.getModule().setSelectedRecordIndex(-1);

            var totalLoad = self.getModule().calculateCurrentTotalLoad();
            self.getModule().updateCurrentTotalLoad(totalLoad);
        };

        this.formResettedEventHandler = function(ev) {
            if(ev.scope === self.getModule().getFormModule()) {
                self.getModule().getMainViewComponent().grid('deselectAllRows');
            }
        };
    },

    methods: {

        startController: function() {

            this.super('startController');

            if(this.getModule().getModel().getCurrentEstimatedLoad()) {
                this.getModule().readAllAppliances();
            }
        },

        addEvents: function() {

            this.super('addEvents');

            var modelEvents = this.getModule().getModel().events;
            var constants = this.getApp().getConstants();

            $(document).on(modelEvents.READ_ALL_COMPLETE, this.readRecordsCompleteEventHandler);
            $(document).on(modelEvents.READ_ALL_ERROR, this.readRecordsErrorEventHandler);

            $(document).on(modelEvents.ESTIMATED_LOAD_CHANGED, this.currentEstimatedLoadChangedEventHandler);

            $(document).on(constants.MODULE_READY_EVENT, this.moduleReadyEventHandler);
            $(document).on(constants.FORM_RESETTED_EVENT, this.formResettedEventHandler);
        },

        removeEvents: function() {

            this.super('removeEvents');
            
            var modelEvents = this.getModule().getModel().events;
            var constants = this.getApp().getConstants();

            $(document).off(modelEvents.READ_ALL_COMPLETE, this.readRecordsCompleteEventHandler);
            $(document).off(modelEvents.READ_ALL_ERROR, this.readRecordsErrorEventHandler);
                
            $(document).off(modelEvents.ESTIMATED_LOAD_CHANGED, this.currentEstimatedLoadChangedEventHandler);

            $(document).off(constants.MODULE_READY_EVENT, this.moduleReadyEventHandler);
            $(document).off(constants.FORM_RESETTED_EVENT, this.formResettedEventHandler);
        }
    }
});
