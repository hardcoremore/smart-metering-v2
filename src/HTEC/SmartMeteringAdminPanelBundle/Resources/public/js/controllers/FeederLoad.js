HCM.define({

    name: 'Controllers.Module.FeederLoad',

    extendFrom: 'Base.Controller',

    construct: function(app, module) {

        this.callConstructor('Base.Controller', [app, module]);

        var self = this;

        this.previousDayButtonClickedHandler = function(ev) {

            var startDate = self.getModule().getDateForFeederLoad(true, true);
            
            self.getModule().setCurrentStartDateForDateRangeSearch(startDate);

            self.loadFeederLoadForDate(
                self.getModule().getFeederLoadFormModule().getFormFieldValue('powerTransformerFeeder'),
                startDate
            );
        };

        this.nextDayButtonClickedHandler = function(ev) {

            var startDate = self.getModule().getDateForFeederLoad(true);

            self.getModule().setCurrentStartDateForDateRangeSearch(startDate);

            self.loadFeederLoadForDate(
                self.getModule().getFeederLoadFormModule().getFormFieldValue('powerTransformerFeeder'),
                startDate
            );
        };

        this.moduleReadyEventHandler = function(ev) {

            if(ev.module.getNamespace() === 'Modules.Form.FeederLoadForm') {

                ev.module.setFormMode('create');
                ev.module.setModel(self.getModule().getFeederLoadModel());

                self.getModule().setFeederLoadFormModule(ev.module);
                self.getApp().getModuleManager().startModule(ev.module);
            }
        };

        this.formModuleInputChangedHandler = function(ev) {

            if(ev.module === self.getModule().getFeederLoadFormModule()) {

                var powerTransformerFeeder =  self.getModule().getFeederLoadFormModule().getFormFieldValue('powerTransformerFeeder');
                var date = self.getModule().getFeederLoadFormModule().getFormFieldValue('date');

                if(powerTransformerFeeder && date) {

                    if(ev.formFieldName === 'powerTransformerFeeder' || ev.formFieldName === 'date') {

                        var date = moment(date);

                        self.getModule().setCurrentStartDateForDateRangeSearch(date);
                        self.getModule().setCurrentDateLabel(date);
                        self.loadFeederLoadForDate(powerTransformerFeeder, date);
                    }

                    self.getModule().getPreviousDayButton().removeClass('inactive');
                    self.getModule().getNextDayButton().removeClass('inactive');
                }
                else {
                    self.getModule().getPreviousDayButton().addClass('inactive');
                    self.getModule().getNextDayButton().addClass('inactive');   
                }
            }
        };

        this.readFeederLoadCompleteEventHandler = function(ev) {

            if(ev.scope === self.getModule()) {

                self.getModule().getSchedulerHelper().resetTooltipContent();

                self.getModule().getFeederLoadScheduler().scheduler('removeAllEvents');

                var rowId = self.getModule().getSchedulerHelper().getDayNameFromDate(self.getModule().getCurrentStartDateForDateRangeSearch());

                var newModel = {};
                    newModel.id = rowId;
                    newModel.title = rowId.toString().firstCharacterToUpperCase();

                self.getModule().getFeederLoadScheduler().scheduler('reinitializeWithNewModel', [newModel]);

                var data = ev.responseData;

                if(ev.responseData.listData) {
                    data = ev.responseData.listData;
                }

                if(self.isArray(data)) {

                    for(var i = 0, len = data.length; i < len; i++) {

                        self.getModule().getSchedulerHelper().addRecordToScheduler(
                            self.getModule().getFeederLoadScheduler(),
                            data[i]
                        );
                    }
                }
            }
        };

        this.readFeederLoadErrorEventHandler = function(ev) {

        };

        this.feederLoadCreateCompleteEventHandler = function(ev) {
            self.getModule().previewFeederLoad(ev.responseData);
        };

        this.feederLoadCreateErrorEventHandler = function(ev) {

        };

        this.feederLoadUpdateCompleteEventHandler = function(ev) {

            var rowId = self.getModule().getSchedulerHelper().getSchedulerRowIdFromRecord(ev.responseData);
            self.getModule().getFeederLoadScheduler().scheduler('removeEventFromRow', rowId, ev.responseData.id);
            self.getModule().getSchedulerHelper().removeTooltipContent(ev.responseData.id);

            self.getModule().getSchedulerHelper().addRecordToScheduler(
                self.getModule().getFeederLoadScheduler(),
                ev.responseData
            );
        };

        this.feederLoadUpdateErrorEventHandler = function(ev) {

        };

        this.schedulerEventClickHandler = function(ev, ui) {

            self.getModule().getFeederLoadFormModule().setEditingRecordData(ui.event);
            self.getModule().getFeederLoadFormModule().setFormMode('edit');
            self.getModule().getFeederLoadFormModule().updateSubmitButtonLabel('Edit');
            self.getModule().getFeederLoadFormModule().populateForm(ui.event.recordData);

            self.getModule().getFeederLoadFormModule().blockFormField('time');
        };
    },

    methods: {

        startController: function() {

            this.getModule().getPreviousDayButton().addClass('inactive');
            this.getModule().getNextDayButton().addClass('inactive');

            var startDate = this.getModule().getDateForFeederLoad();

            this.getModule().setCurrentStartDateForDateRangeSearch(startDate);

            this.getModule().setCurrentDateLabel(startDate);

            this.super('startController');
        },

        addEvents: function() {
            
            this.super('addEvents');

            var modelEvents = this.getModule().getFeederLoadModel().events;

            $(document).on(modelEvents.READ_ALL_COMPLETE, this.readFeederLoadCompleteEventHandler);
            $(document).on(modelEvents.READ_ALL_ERROR, this.readFeederLoadErrorEventHandler);

            $(document).on(modelEvents.CREATE_COMPLETE, this.feederLoadCreateCompleteEventHandler);
            $(document).on(modelEvents.CREATE_ERROR, this.feederLoadCreateErrorEventHandler);

            $(document).on(modelEvents.UPDATE_COMPLETE, this.feederLoadUpdateCompleteEventHandler);
            $(document).on(modelEvents.UPDATE_ERROR, this.feederLoadUpdateErrorEventHandler);

            this.getModule().getPreviousDayButton().on('click', this.previousDayButtonClickedHandler);
            this.getModule().getNextDayButton().on('click', this.nextDayButtonClickedHandler);

            var constants = this.getApp().getConstants();

            $(document).on(constants.MODULE_READY_EVENT, this.moduleReadyEventHandler);
            $(document).on(constants.FORM_INPUT_CHANGED_EVENT, this.formModuleInputChangedHandler);

            this.getModule().getFeederLoadScheduler().scheduler({
                onEventClick: this.schedulerEventClickHandler
            })
        },

        removeEvents: function() {
            
            this.super('removeEvents');

            var modelEvents = this.getModule().getFeederLoadModel().events;

            $(document).off(modelEvents.READ_ALL_COMPLETE, this.readFeederLoadCompleteEventHandler);
            $(document).off(modelEvents.READ_ALL_ERROR, this.readFeederLoadErrorEventHandler);

            $(document).off(modelEvents.CREATE_COMPLETE, this.feederLoadCreateCompleteEventHandler);
            $(document).off(modelEvents.CREATE_ERROR, this.feederLoadCreateErrorEventHandler);

            $(document).off(modelEvents.UPDATE_COMPLETE, this.feederLoadUpdateCompleteEventHandler);
            $(document).off(modelEvents.UPDATE_ERROR, this.feederLoadUpdateErrorEventHandler);


            this.getModule().getPreviousDayButton().off('click', this.previousDayButtonClickedHandler);
            this.getModule().getNextDayButton().off('click', this.nextDayButtonClickedHandler);

            var constants = this.getApp().getConstants();

            $(document).off(constants.MODULE_READY_EVENT, this.moduleReadyEventHandler);
            $(document).off(constants.FORM_INPUT_CHANGED_EVENT, this.formModuleInputChangedHandler);

            this.getModule().getFeederLoadScheduler().scheduler({
                onEventClick: null
            })
        },

        

        loadFeederLoadForDate: function(feeder, date, parameters) {

            this.getModule().getFeederLoadDateRangeHolder().find('.current-date').text(date.format('MMMM Do, dddd YYYY'));

            this.getModule().readFeederLoadForDateRange(
                feeder,
                date.format('YYYY-MM-DD'),
                parameters
            );
        }
    }
});
