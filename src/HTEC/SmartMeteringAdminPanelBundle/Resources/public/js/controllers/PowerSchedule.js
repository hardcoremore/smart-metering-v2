HCM.define({

    name: 'Controllers.Module.PowerSchedule',

    extendFrom: 'Controllers.Crud',

    construct: function(app, module) {

        this.callConstructor('Controllers.Crud', [app, module]);

        var self = this;

        var superModuleReadyEventHandler = this.moduleReadyEventHandler;
        this.moduleReadyEventHandler = function(ev) {

            if(ev.module.getNamespace() === 'Modules.Form.Filters.PowerScheduleFiltersForm') {

                ev.module.setModel(self.getModule().getModel());
                ev.module.setFormMode('read');
                ev.module.setPowerScheduleModule(self.getModule());
                ev.module.hideModule();

                self.getModule().setPowerScheduleFiltersFormModule(ev.module);

                self.getApp().getModuleManager().startModule(ev.module);
            }
            else {
                superModuleReadyEventHandler(ev);
            }
        };

        this.approvePowerSchedulePopupSubmitHandler = function(ev) {
            self.getModule().approvePowerSchedule(self.getModule().getSelectedRecord());
            self.getModule().getPowerScheduleApprovePopup().popup('close');
        };

        var superRecordPatchCompleteEventHandler = this.recordPatchCompleteEventHandler;

        this.recordPatchCompleteEventHandler = function(ev) {
            
            superRecordPatchCompleteEventHandler(ev);

            if(ev.scope.fieldModelData.name === 'status') {

                var selectedData = self.getModule().getSelectedRecord();
                    selectedData['status'] = 'approved';

                self.getModule().getSchedulerHelper().removeTooltipContent(selectedData.id);
                self.getModule().setSelectedRecord(selectedData);
                self.getModule().previewPowerScheduleEvent(selectedData);

                self.getModule().getMainViewComponent().grid(
                    'updateRow',
                    self.getModule().getSelectedRecordIndex(), self.getModule().getSelectedRecord()
                );

                self.getApp().showNotification('success', 'You have successfully approved schedule');
            }
        };

        var superRowSelectedEventHandler = this.rowSelectedEventHandler;
        this.rowSelectedEventHandler = function(ev, ui) {

            superRowSelectedEventHandler(ev, ui);

            self.getModule().previewPowerScheduleEvent(ui.rowData);
        };

        this.powerSchedulesViewTabSelectedHandler = function(ev, ui) {

            if(ui.tabIndex === 0) {
                self.getModule().getPagingComponent().show();
                self.getModule().getPowerSchedulePreviewScheduler().show();
                self.getModule().getPowerScheduleFiltersFormModule().hideModule();
            }
            else if(ui.tabIndex === 1) {
                self.getModule().getPagingComponent().hide();
                self.getModule().getPowerSchedulePreviewScheduler().hide();
                self.getModule().getPowerScheduleFiltersFormModule().showModule();
            }
        };

        this.onPowerScheduleWeeklyPreviewEventClick = function(ev, ui) {
            console.log("SCHEDULE EVENT CLICK HANDLER");
            console.log(ui);
        };

        this.previousWeekButtonClickedHandler = function(ev) {

            var startDate = self.getModule().getStartDateForLoadingPowerSchedules(true);
            var endDate = self.getModule().getEndDateForLoadingPowerSchedules(startDate);
            
            var filters;

            if(self.getModule().getPowerScheduleFiltersFormModule()) {
                filters = self.getModule().getPowerScheduleFiltersFormModule().getFormData();
            }
            
            self.getModule().setCurrentStartDateForDateRangeSearch(startDate);

            self.loadPowerSchedulesForDateRange(startDate, endDate, filters);
        };

        this.nextWeekButtonClickedHandler = function(ev) {

            var startDate = self.getModule().getStartDateForLoadingPowerSchedules(false);
            var endDate = self.getModule().getEndDateForLoadingPowerSchedules(startDate);
            
            var filters;

            if(self.getModule().getPowerScheduleFiltersFormModule()) {
                filters = self.getModule().getPowerScheduleFiltersFormModule().getFormData();
            }
            
            self.getModule().setCurrentStartDateForDateRangeSearch(startDate);

            self.loadPowerSchedulesForDateRange(startDate, endDate, filters);
        };

        this.readPowerSchedulesCompleteEventHandler = function(ev) {

            if(ev.scope === self.getModule().getPowerScheduleFiltersFormModule()) {

                self.getModule().getSchedulerHelper().resetTooltipContent();

                self.getModule().getPowerSchedulesWeeklyPreviewScheduler().scheduler('removeAllEvents');

                var data = ev.responseData;

                if(ev.responseData.listData) {
                    data = ev.responseData.listData;
                }

                if(self.isArray(data)) {

                    for(var i = 0, len = data.length; i < len; i++) {

                        self.getModule().getSchedulerHelper().addRecordToScheduler(
                            self.getModule().getPowerSchedulesWeeklyPreviewScheduler(),
                            data[i]
                        );
                    }
                }
            }
        };

        this.readPowerSchedulesErrorEventHandler = function() {
            self.getApp().showNotification('error', 'Loading power schedules for week preview failed :(');
        };
    },

    methods: {

        startController: function() {

            var startDate = this.getModule().getStartDateForLoadingPowerSchedules();
            var endDate = this.getModule().getEndDateForLoadingPowerSchedules(startDate);

            this.getModule().setCurrentStartDateForDateRangeSearch(startDate);

            this.getModule().getPowerSchedulesDateRangeHolder().find('.start-date').text(startDate.format('MMMM Do, dddd YYYY'));
            this.getModule().getPowerSchedulesDateRangeHolder().find('.end-date').text(endDate.format('MMMM Do, dddd YYYY'));

            this.super('startController');
        },

        addEvents: function() {

            this.super('addEvents');

            this.getModule().getPowerScheduleApprovePopup().popup('option', {
                onFormSubmitted: this.approvePowerSchedulePopupSubmitHandler,
            });

            this.getModule().getPowerSchedulesViewTab().tab({
                onTabSelected: this.powerSchedulesViewTabSelectedHandler
            });

            this.getModule().getPowerSchedulesWeeklyPreviewScheduler().scheduler({
                onEventClick: this.onPowerScheduleWeeklyPreviewEventClick
            });

            this.getModule().getPreviousWeekButton().on('click', this.previousWeekButtonClickedHandler);
            this.getModule().getNextWeekButton().on('click', this.nextWeekButtonClickedHandler);

            var modelEvents = this.getModule().getModel().events;

            $(document).on(modelEvents.READ_ALL_COMPLETE, this.readPowerSchedulesCompleteEventHandler);
            $(document).on(modelEvents.READ_ALL_ERROR, this.readPowerSchedulesErrorEventHandler);

            this.addRelationFieldEvent(this.getModule().getModel().getFieldByName('businessDistrict'));
            this.addRelationFieldEvent(this.getModule().getModel().getFieldByName('injectionSubstation'));
            this.addRelationFieldEvent(this.getModule().getModel().getFieldByName('powerTransformer'));
        },

        removeEvents: function() {

            this.super('removeEvents');
            
            this.getModule().getPowerScheduleApprovePopup().popup('option', {
                onFormSubmitted: null,
            });

            this.getModule().getPowerSchedulesViewTab().tab({
                onTabSelected: null
            });

            this.getModule().getPowerSchedulesWeeklyPreviewScheduler().scheduler({
                onEventClick: null
            });

            this.getModule().getPreviousWeekButton().off('click', this.previousWeekButtonClickedHandler);
            this.getModule().getNextWeekButton().off('click', this.nextWeekButtonClickedHandler);

            var modelEvents = this.getModule().getModel().events;

            $(document).off(modelEvents.READ_ALL_COMPLETE, this.readPowerSchedulesCompleteEventHandler);
            $(document).off(modelEvents.READ_ALL_ERROR, this.readPowerSchedulesErrorEventHandler);

            this.removeRelationFieldEvent(this.getModule().getModel().getFieldByName('businessDistrict'));
            this.removeRelationFieldEvent(this.getModule().getModel().getFieldByName('injectionSubstation'));
            this.removeRelationFieldEvent(this.getModule().getModel().getFieldByName('powerTransformer'));
            this.removeRelationFieldEvent(this.getModule().getModel().getFieldByName('powerTransformerFeeder'));
        },

        

        handleModuleControlButtonClick: function(actionType) {

            if(this.getModule().getMainViewComponent().grid('getTableState') === 'editing') {
                this.getApp().showNotification('warning', 'Please finish the grid cell editing first.');
                return;
            }

            if(actionType === 'approve-record') {

                if(this.getModule().getSelectedRecord()) {

                    if(this.getModule().getSelectedRecord().status !== 'approved') {
                        this.getModule().getPowerScheduleApprovePopup().popup('open');
                    }
                    else {
                        this.getApp().showNotification('information', 'Selected schedule is already approved');
                    }
                }
                else {
                    this.getApp().showNotification('warning', 'Please select power schedule to be approved');
                }
                
            }
            else {
                this.super('handleModuleControlButtonClick', [actionType]);
            }
        },

        loadPowerSchedulesForDateRange: function(startDate, endDate, parameters) {

            this.getModule().getPowerSchedulesDateRangeHolder().find('.start-date').text(startDate.format('MMMM Do, dddd YYYY'));
            this.getModule().getPowerSchedulesDateRangeHolder().find('.end-date').text(endDate.format('MMMM Do, dddd YYYY'));

            this.getModule().readPowerSchedulesForDateRange(
                startDate.format('YYYY-MM-DD'),
                endDate.format('YYYY-MM-DD'),
                parameters
            );
        }
    }
});
