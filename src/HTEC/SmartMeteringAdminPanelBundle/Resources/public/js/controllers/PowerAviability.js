HCM.define({

    name: 'Controllers.Module.PowerAviability',

    extendFrom: 'Controllers.RelationFieldForm',

    construct: function(app, module) {

        this.callConstructor('Controllers.RelationFieldForm', [app, module]);

        var self = this;

        this.formSubmittedEventHandler = function(ev) {

            var formData = self.getModule().getFormData(false);
            var formMode = self.getModule().getFormMode();

            self.submitForm(formData, formMode, 'read');

            // stop native from submitting
            return false;
        };

        var superFormChangedEventHandler = this.formChangedEventHandler;
        this.formChangedEventHandler = function(ev) {

            superFormChangedEventHandler(ev);

            var formFieldIinput = $(ev.currentTarget);
            var formFieldName = formFieldIinput.attr('name');

            switch(formFieldName)
            {
                case 'businessDistrict':
                    self.getModule().loadBusinessDistrictInjectionSubstations(self.getModule().getFormFieldValue(formFieldName, formFieldIinput));
                break;

                case 'injectionSubstation':
                    self.getModule().loadInjectionSubstationPowerTransformers(self.getModule().getFormFieldValue(formFieldName, formFieldIinput));
                break;

                case 'powerTransformer':
                    self.getModule().loadPowerTransformerOutputFeeders(self.getModule().getFormFieldValue(formFieldName, formFieldIinput));
                break;
            }

            if(self.getModule().getFormFieldValue('businessDistrict')) {
                self.getModule().getPreviousWeekButton().removeClass('inactive');
                self.getModule().getNextWeekButton().removeClass('inactive');
                self.getModule().getSearchFeedersButton().removeClass('inactive');
            }
            else {
                self.getModule().getPreviousWeekButton().addClass('inactive');
                self.getModule().getNextWeekButton().addClass('inactive');
                self.getModule().getSearchFeedersButton().addClass('inactive');
            }
        };

        this.previousWeekButtonClickedHandler = function(ev) {

            var startDate = self.getModule().getStartDateForLoadingPoverAviability(true, true);
            var endDate = self.getModule().getEndDateForLoadingPowerAviability(startDate);
            
            self.getModule().setCurrentStartDateForDateRangeSearch(startDate);

            self.loadPowerAviabilityForDateRange(startDate, endDate, self.getModule().getFormData(false));
        };

        this.nextWeekButtonClickedHandler = function(ev) {

            var startDate = self.getModule().getStartDateForLoadingPoverAviability(true);
            var endDate = self.getModule().getEndDateForLoadingPowerAviability(startDate);

            self.getModule().setCurrentStartDateForDateRangeSearch(startDate);

            self.loadPowerAviabilityForDateRange(startDate, endDate, self.getModule().getFormData(false));
        };

        this.readPowerActivateDeactivateEventHandler = function(ev) {

            if(ev.scope === self.getModule()) {

                self.getModule().getSchedulerHelper().resetTooltipContent();

                self.getModule().getPowerAviabilityScheduler().scheduler('removeAllEvents');

                var data = ev.responseData;

                if(ev.responseData.listData) {
                    data = ev.responseData.listData;
                }

                var mergedRecords;
                var errorMessage = 'Data is not consistent. Someone has forgot to activate or deactivate Feeder: ';
                var powerActivate, powerDeactivate;
                var skipIndexes = [];

                if(self.isArray(data)) {

                    for(var i = 0, len = data.length; i < len; i++) {

                        if(skipIndexes.indexOf(i) !== -1) {
                            continue;
                        }

                        if((i + 1 + skipIndexes.length) % 2 === 0) {

                            if(data[i - 1].type !== 'power-activate' && data[i].type !== 'power-deactivate') {
                                self.getApp().showNotification(
                                    'error',
                                    errorMessage + 
                                    data[i - 1].powerTransformerFeeder.name + ' or Feeder: ' + 
                                    data[i].powerTransformerFeeder.name + ' properly on time. Please fix the data.'
                                );
                            }
                            else if(data[i].type !== 'power-deactivate') {

                                mergedRecords = {
                                    powerActivate: $.extend({}, data[i - 1]),
                                    powerDeactivate: {
                                        id: data[i - 1].id,
                                        time: moment().format(),
                                        type: 'feeder-is-live',
                                        powerTransformerFeeder: data[i - 1].powerTransformerFeeder
                                    }
                                };

                                self.getModule().getSchedulerHelper().addRecordToScheduler(
                                    self.getModule().getPowerAviabilityScheduler(),
                                    mergedRecords
                                );

                                skipIndexes.push(i);
                                i--;
                            }
                            else {

                                mergedRecords = {
                                    powerActivate: $.extend({}, data[i - 1]),
                                    powerDeactivate: $.extend({}, data[i])
                                };

                                self.getModule().getSchedulerHelper().addRecordToScheduler(
                                    self.getModule().getPowerAviabilityScheduler(),
                                    mergedRecords
                                );
                            }
                        }
                        else if(i === len - 1 && data[i].type == 'power-activate') {

                            mergedRecords = {
                                powerActivate: $.extend({}, data[i]),
                                powerDeactivate: {
                                    id: data[i].id,
                                    time: moment().format(),
                                    type: 'feeder-is-live',
                                    powerTransformerFeeder: data[i].powerTransformerFeeder
                                }
                            };

                            self.getModule().getSchedulerHelper().addRecordToScheduler(
                                self.getModule().getPowerAviabilityScheduler(),
                                mergedRecords
                            );
                        }
                    }
                }
            }
        };
    },

    methods: {

        startController: function() {

            if(this.getModule().getFormMode() === 'create') {

                this.getModule().resetRelationFormField(
                    this.getModule().getModel().getFieldByName('injectionSubstation'),
                    this.getModule().getFormFieldByName('injectionSubstation')
                );

                this.getModule().resetRelationFormField(
                    this.getModule().getModel().getFieldByName('powerTransformer'),
                    this.getModule().getFormFieldByName('powerTransformer')
                );

                this.getModule().resetRelationFormField(
                    this.getModule().getModel().getFieldByName('powerTransformerFeeder'),
                    this.getModule().getFormFieldByName('powerTransformerFeeder')
                );
            }

            this.getModule().getSearchFeedersButton().addClass('inactive');
            this.getModule().getPreviousWeekButton().addClass('inactive');
            this.getModule().getNextWeekButton().addClass('inactive');

            var startDate = this.getModule().getStartDateForLoadingPoverAviability();
            var endDate = this.getModule().getEndDateForLoadingPowerAviability(startDate);

            this.getModule().setCurrentStartDateForDateRangeSearch(startDate);

            this.getModule().getPowerAviabilityDateRangeHolder().find('.start-date').text(startDate.format('MMMM Do, dddd YYYY'));
            this.getModule().getPowerAviabilityDateRangeHolder().find('.end-date').text(endDate.format('MMMM Do, dddd YYYY'));

            this.super('startController');
        },

        submitForm: function(formData, formMode, role) {

            var startDate = this.getModule().getStartDateForLoadingPoverAviability();
            var endDate = this.getModule().getEndDateForLoadingPowerAviability(startDate);

            this.loadPowerAviabilityForDateRange(startDate, endDate, formData);
        },

        addEvents: function() {
            
            this.super('addEvents');

            var modelEvents = this.getModule().getModel().events;

            $(document).on(modelEvents.READ_ALL_COMPLETE, this.readPowerActivateDeactivateEventHandler);
            $(document).on(modelEvents.READ_ALL_ERROR, this.readPowerSchedulesErrorEventHandler);

            this.getModule().getPreviousWeekButton().on('click', this.previousWeekButtonClickedHandler);
            this.getModule().getNextWeekButton().on('click', this.nextWeekButtonClickedHandler);
        },

        removeEvents: function() {
            
            this.super('removeEvents');

            var modelEvents = this.getModule().getModel().events;

            $(document).off(modelEvents.READ_ALL_COMPLETE, this.readPowerActivateDeactivateEventHandler);
            $(document).off(modelEvents.READ_ALL_ERROR, this.readPowerSchedulesErrorEventHandler);

            this.getModule().getPreviousWeekButton().off('click', this.previousWeekButtonClickedHandler);
            this.getModule().getNextWeekButton().off('click', this.nextWeekButtonClickedHandler);
        },

        addRelationFieldEvent: function(fieldModelData) {

            switch(fieldModelData.name)
            {
                case 'businessDistrict':
                case 'injectionSubstation':
                case 'powerTransformer':
                case 'powerTransformerFeeder':

                    $(document).on(
                        fieldModelData.relationModel.events.READ_FOR_SELECT_COMPLETE,
                        {fieldModelData: fieldModelData},
                        this.relationDataLoadCompleteEventHandler
                    );

                    $(document).on(
                        fieldModelData.relationModel.events.READ_FOR_SELECT_ERROR,
                        {fieldModelData: fieldModelData},
                        this.relationDataLoadCompleteEventHandler
                    );

                break;

                default:
                    this.super('addRelationFieldEvent', [fieldModelData]);
                break;
            }
        },

        removeRelationFieldEvent: function(fieldModelData) {

            switch(fieldModelData.name)
            {
                case 'businessDistrict':
                case 'injectionSubstation':
                case 'powerTransformer':
                case 'powerTransformerFeeder':
                    $(document).off(fieldModelData.relationModel.events.READ_FOR_SELECT_COMPLETE, this.relationDataLoadCompleteEventHandler);
                    $(document).off(fieldModelData.relationModel.events.READ_FOR_SELECT_ERROR, this.relationDataLoadErrorEventHandler);
                break;

                default:
                    this.super('addRelationFieldEvent', [fieldModelData]);
                break;
            }
        },

        loadPowerAviabilityForDateRange: function(startDate, endDate, parameters) {

            this.getModule().getPowerAviabilityDateRangeHolder().find('.start-date').text(startDate.format('MMMM Do, dddd YYYY'));
            this.getModule().getPowerAviabilityDateRangeHolder().find('.end-date').text(endDate.format('MMMM Do, dddd YYYY'));

            this.getModule().readPowerAviabilityForDateRange(
                startDate.format('YYYY-MM-DD'),
                endDate.format('YYYY-MM-DD'),
                parameters
            );
        }
    }
});
