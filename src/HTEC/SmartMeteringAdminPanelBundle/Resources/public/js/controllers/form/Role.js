HCM.define({

    name: 'Controllers.Form.Role',

    extendFrom: 'Controllers.RelationFieldForm',

    construct: function(app, module) {

        this.callConstructor('Controllers.RelationFieldForm', [app, module]);
    },

    methods: {

        addRelationFieldEvent: function(fieldModelData) {

            if(fieldModelData.name === 'permissions') {
                
                $(document).on(
                    fieldModelData.relationModel.events.READ_ALL_COMPLETE,
                    {fieldModelData: fieldModelData},
                    this.relationDataLoadCompleteEventHandler
                );

                $(document).on(
                    fieldModelData.relationModel.events.READ_ALL_ERROR,
                    {fieldModelData: fieldModelData},
                    this.relationDataLoadCompleteEventHandler
                );
            }
            else {
                this.super('addRelationFieldEvent', [fieldModelData]);
            }
        },

        removeRelationFieldEvent: function(fieldModelData) {

            if(fieldModelData.name === 'permissions') {
                $(document).off(fieldModelData.relationModel.events.READ_ALL_COMPLETE, this.relationDataLoadCompleteEventHandler);
                $(document).off(fieldModelData.relationModel.events.READ_ALL_ERROR, this.relationDataLoadErrorEventHandler);
            }
            else {
                this.super('removeRelationFieldEvent', [fieldModelData]);
            }
        },
    }
});
