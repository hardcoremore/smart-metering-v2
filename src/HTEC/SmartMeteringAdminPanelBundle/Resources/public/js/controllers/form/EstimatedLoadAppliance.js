HCM.define({

    name: 'Controllers.Form.EstimatedLoadAppliance',

    extendFrom: 'Controllers.RelationFieldForm',

    construct: function(app, module) {

        this.callConstructor('Controllers.RelationFieldForm', [app, module]);

        var self = this;

        var superFormChangedEventHandler = this.formChangedEventHandler;
        this.formChangedEventHandler = function(ev) {

            superFormChangedEventHandler(ev);

            var formFieldIinput = $(ev.currentTarget);
            var formFieldName = formFieldIinput.attr('name');

            switch(formFieldName)
            {
                case 'applianceType':
                    self.getModule().loadAppliancesForApplianceType(self.getModule().getFormFieldValue(formFieldName, formFieldIinput));
                break;

                case 'applianceCount':

                    var totalConsumptionInputField = self.getModule().getFormFieldByName('totalConsumption');
                    var applianceData = self.getModule().getRelationFieldSelectedData('appliance');

                    if(applianceData) {
                        totalConsumptionInputField.val(formFieldIinput.val() * applianceData.consumption);    
                    }
                    else {
                        totalConsumptionInputField.val(0);   
                    }

                break;
            }
        };

        var superFormResetButtonClickHandler = this.formResetButtonClickHandler;
        this.formResetButtonClickHandler = function(ev) {

            superFormResetButtonClickHandler(ev);

            self.getModule().setFormMode('create');
            self.getModule().getFormSubmitButton().find('.button-label').text('Create');
        };

        this.currentEstimatedLoadChangedEventHandler = function(ev) {

            var currentEstimatedLoad = self.getModule().getModel().getCurrentEstimatedLoad();
            var fieldModelData = self.getModule().getModel().getFieldByName('applianceType');

            self.getModule().resetForm();

            if(currentEstimatedLoad) {
                self.getModule().loadRelationFormFieldData(fieldModelData);
            }
            else {

                var formFieldInput = self.getModule().getFormFieldByName('applianceType');

                self.getModule().resetRelationFormField(fieldModelData, formFieldInput);
            }
        };
    },

    methods: {

        startController: function() {

            if(this.getModule().getFormMode() === 'create') {

                this.getModule().resetRelationFormField(
                    this.getModule().getModel().getFieldByName('applianceType'),
                    this.getModule().getFormFieldByName('applianceType')
                );

                this.getModule().resetRelationFormField(
                    this.getModule().getModel().getFieldByName('appliance'),
                    this.getModule().getFormFieldByName('appliance')
                );
            }

            this.super('startController');
        },

        addEvents: function() {

            this.super('addEvents');

            var modelEvents = this.getModule().getModel().events;
            $(document).on(modelEvents.ESTIMATED_LOAD_CHANGED, this.currentEstimatedLoadChangedEventHandler);
            
        },

        removeEvents: function() {

            this.super('removeEvents');

            var modelEvents = this.getModule().getModel().events;
            $(document).off(modelEvents.ESTIMATED_LOAD_CHANGED, this.currentEstimatedLoadChangedEventHandler);
        },

        addRelationFieldEvent: function(fieldModelData) {

            if(fieldModelData.name === 'appliance') {

                $(document).on(
                        fieldModelData.relationModel.events.READ_FOR_SELECT_COMPLETE,
                        {fieldModelData: fieldModelData},
                        this.relationDataLoadCompleteEventHandler
                );

                $(document).on(
                    fieldModelData.relationModel.events.READ_FOR_SELECT_ERROR,
                    {fieldModelData: fieldModelData},
                    this.relationDataLoadCompleteEventHandler
                );
            }
            else {
                this.super('addRelationFieldEvent', [fieldModelData]);
            }
        },

        removeRelationFieldEvent: function(fieldModelData) {
            if(fieldModelData.name === 'appliance') {
                $(document).off(fieldModelData.relationModel.events.READ_FOR_SELECT_COMPLETE, this.relationDataLoadCompleteEventHandler);
                $(document).off(fieldModelData.relationModel.events.READ_FOR_SELECT_ERROR, this.relationDataLoadErrorEventHandler);
            }
            else {
                this.super('addRelationFieldEvent', [fieldModelData]);
            }
        }
    }
});
