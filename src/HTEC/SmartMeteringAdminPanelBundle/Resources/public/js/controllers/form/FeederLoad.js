HCM.define({

    name: 'Controllers.Form.FeederLoad',

    extendFrom: 'Controllers.RelationFieldForm',

    construct: function(app, module) {

        this.callConstructor('Controllers.RelationFieldForm', [app, module]);

         var self = this;

        var superFormChangedEventHandler = this.formChangedEventHandler;
        this.formChangedEventHandler = function(ev) {

            superFormChangedEventHandler(ev);

            var formFieldIinput = $(ev.currentTarget);
            var formFieldName = formFieldIinput.attr('name');

            switch(formFieldName)
            {
                case 'businessDistrict':
                    self.getModule().loadBusinessDistrictInjectionSubstations(self.getModule().getFormFieldValue(formFieldName, formFieldIinput));
                break;

                case 'injectionSubstation':
                    self.getModule().loadInjectionSubstationPowerTransformers(self.getModule().getFormFieldValue(formFieldName, formFieldIinput));
                break;

                case 'powerTransformer':
                    self.getModule().loadPowerTransformerOutputFeeders(self.getModule().getFormFieldValue(formFieldName, formFieldIinput));
                break;
            }
        };

        var superFormResetButtonClickHandler = this.formResetButtonClickHandler;
        this.formResetButtonClickHandler = function(ev) {

            superFormResetButtonClickHandler(ev);

            self.getModule().setFormMode('create');
            self.getModule().getFormSubmitButton().find('.button-label').text('Create');
        };

        this.recordCreateCompleteEventHandler = function(ev) {

            self.getModule().setFormMode('edit');
            self.getModule().setEditingRecordData(ev.responseData);
            self.getModule().getFormSubmitButton().find('.button-label').text('Update');
        };
    },

    methods: {

        startController: function() {

            if(this.getModule().getFormMode() === 'create') {

                this.getModule().resetRelationFormField(
                    this.getModule().getModel().getFieldByName('injectionSubstation'),
                    this.getModule().getFormFieldByName('injectionSubstation')
                );

                this.getModule().resetRelationFormField(
                    this.getModule().getModel().getFieldByName('powerTransformer'),
                    this.getModule().getFormFieldByName('powerTransformer')
                );

                this.getModule().resetRelationFormField(
                    this.getModule().getModel().getFieldByName('powerTransformerFeeder'),
                    this.getModule().getFormFieldByName('powerTransformerFeeder')
                );
            }

            this.super('startController');
        },

        addRelationFieldEvent: function(fieldModelData) {

            switch(fieldModelData.name)
            {
                case 'businessDistrict':
                case 'injectionSubstation':
                case 'powerTransformer':
                case 'powerTransformerFeeder':

                    $(document).on(
                        fieldModelData.relationModel.events.READ_FOR_SELECT_COMPLETE,
                        {fieldModelData: fieldModelData},
                        this.relationDataLoadCompleteEventHandler
                    );

                    $(document).on(
                        fieldModelData.relationModel.events.READ_FOR_SELECT_ERROR,
                        {fieldModelData: fieldModelData},
                        this.relationDataLoadCompleteEventHandler
                    );

                break;

                default:
                    this.super('addRelationFieldEvent', [fieldModelData]);
                break;
            }
        },

        removeRelationFieldEvent: function(fieldModelData) {

            switch(fieldModelData.name)
            {
                case 'businessDistrict':
                case 'injectionSubstation':
                case 'powerTransformer':
                case 'powerTransformerFeeder':
                    $(document).off(fieldModelData.relationModel.events.READ_FOR_SELECT_COMPLETE, this.relationDataLoadCompleteEventHandler);
                    $(document).off(fieldModelData.relationModel.events.READ_FOR_SELECT_ERROR, this.relationDataLoadErrorEventHandler);
                break;

                default:
                    this.super('addRelationFieldEvent', [fieldModelData]);
                break;
            }
        }
    }
});
