HCM.define({

    name: 'Controllers.Form.PowerPurchase',

    extendFrom: 'Controllers.RelationFieldForm',

    construct: function(app, module) {

        this.callConstructor('Controllers.RelationFieldForm', [app, module]);
    },

    methods: {

        addEvents: function() {
            
            this.super('addEvents');

            this.getModule().getFormFieldByName('amount').on('keydown', this.onlyNumbersKeyDownHandler);
        },

        removeEvents: function() {
            
            this.super('removeEvents');

            this.getModule().getFormFieldByName('amount').off('keydown', this.onlyNumbersKeyDownHandler);
        }
    }
});
