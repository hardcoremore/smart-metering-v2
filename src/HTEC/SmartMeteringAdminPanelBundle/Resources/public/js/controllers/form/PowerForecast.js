HCM.define({

    name: 'Controllers.Form.PowerForecast',

    extendFrom: 'Controllers.RelationFieldForm',

    construct: function(app, module) {

        this.callConstructor('Controllers.RelationFieldForm', [app, module]);

        var self = this;

        var superFormChangedEventHandler = this.formChangedEventHandler;
        this.formChangedEventHandler = function(ev) {

            superFormChangedEventHandler(ev);

            var formFieldIinput = $(ev.currentTarget);
            var formFieldName = formFieldIinput.attr('name');

            switch(formFieldName)
            {
                case 'powerPurchase':

                    var powerPurchase = self.getModule().getRelationFieldSelectedData('powerPurchase');

                    var amountToForecastInput = self.getModule().getFormFieldByName('amountToForecast');
                    var amountToForecast = self.getModule().getFormFieldValue('amountToForecast', amountToForecastInput);

                    if(powerPurchase) {
                        self.getModule().setFormFieldValue('amountToForecast', powerPurchase.amountPurchased, amountToForecastInput);
                    }
                    else {
                        self.getModule().setFormFieldValue('amountToForecast', '', amountToForecastInput);
                    }

                break;
            }
        };
    },

    methods: {

        addEvents: function() {
            
            this.super('addEvents');

            this.getModule().getFormFieldByName('amountForecasted').on('keydown', this.onlyNumbersKeyDownHandler);
        },

        removeEvents: function() {
            
            this.super('removeEvents');

            this.getModule().getFormFieldByName('amountForecasted').off('keydown', this.onlyNumbersKeyDownHandler);
        },

        addRelationFieldEvent: function(fieldModelData) {

            if(fieldModelData.name === 'powerPurchase') {
                $(document).on(
                    fieldModelData.relationModel.events.READ_ALL_FOR_TODAY_COMPLETE,
                    {fieldModelData: fieldModelData},
                    this.relationDataLoadCompleteEventHandler
                );
                
                $(document).on(
                    fieldModelData.relationModel.events.READ_ALL_FOR_TODAY_ERROR,
                    {fieldModelData: fieldModelData},
                    this.relationDataLoadErrorEventHandler
                );
            }
            else {
                this.super('addRelationFieldEvent', [fieldModelData]);
            }
        },

        removeRelationFieldEvent: function(fieldModelData) {

            if(fieldModelData.name === 'powerPurchase') {
                $(document).off(fieldModelData.relationModel.events.READ_ALL_FOR_TODAY_COMPLETE, this.relationDataLoadCompleteEventHandler);
                $(document).off(fieldModelData.relationModel.events.READ_ALL_FOR_TODAY_ERROR, this.relationDataLoadErrorEventHandler);
            }
            else {
                this.super('removeRelationFieldEvent', [fieldModelData]);
            }
        },

    }
});
