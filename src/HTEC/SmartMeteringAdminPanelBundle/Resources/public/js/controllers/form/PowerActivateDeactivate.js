HCM.define({

    name: 'Controllers.Form.PowerActivateDeactivate',

    extendFrom: 'Controllers.RelationFieldForm',

    construct: function(app, module) {

        this.callConstructor('Controllers.RelationFieldForm', [app, module]);

        var self = this;

        var selectedFeedersGridIndexes = {};

        this.setFeederGridIndex = function(index, feeder) {
            selectedFeedersGridIndexes[feeder.id] = index;
        };

        this.getFeederGridIndex = function(feeder) {
            return selectedFeedersGridIndexes[feeder.id];
        };

        var superRelationDataLoadCompleteEventHandler = this.relationDataLoadCompleteEventHandler;
        this.relationDataLoadCompleteEventHandler = function(ev) {

            if(ev.scope.fieldModelData.name === 'powerTransformerFeeder') {

                self.getModule().getPowerTransformerFeedersGrid().grid('removeAllRows');

                for(var i = 0, len = ev.responseData.length; i < len; i++) {
                    self.getModule().getPowerTransformerFeedersGrid().grid('addRow', ev.responseData[i]);
                }
            }
            else {
                superRelationDataLoadCompleteEventHandler(ev);
            }
        };

        var superFormChangedEventHandler = this.formChangedEventHandler;
        this.formChangedEventHandler = function(ev) {

            superFormChangedEventHandler(ev);

            var formFieldIinput = $(ev.currentTarget);
            var formFieldName = formFieldIinput.attr('name');

            switch(formFieldName)
            {
                case 'businessDistrict':
                    self.getModule().loadBusinessDistrictInjectionSubstations(self.getModule().getFormFieldValue(formFieldName, formFieldIinput));
                break;

                case 'injectionSubstation':
                    self.getModule().loadInjectionSubstationPowerTransformers(self.getModule().getFormFieldValue(formFieldName, formFieldIinput));
                break;

                case 'powerTransformer':

                    if(self.getModule().getFormFieldValue('powerTransformer')) {
                        self.getModule().getSearchFeedersButton().removeClass('inactive');
                    }
                    else {
                        self.getModule().getSearchFeedersButton().addClass('inactive');
                    }
                    
                break;
            }
        };

        this.recordCreateCompleteEventHandler = function(ev) {
            
            var type = '';

            if(ev.responseData.type === 'power-activate') {
                type = 'activated';
            }
            else {
                type = 'deactivated';
            }

            self.getModule().getPowerTransformerFeedersGrid().grid('updateRow',
                self.getFeederGridIndex(ev.responseData.powerTransformerFeeder),
                ev.responseData.powerTransformerFeeder
            );

            self.getApp().showNotification('success', 'You have successfully ' + type + ' feeder:  ' + ev.responseData.powerTransformerFeeder.name);

            self.getModule().setIsFormChanged(false);
        };
    },

    methods: {

        startController: function() {

            if(this.getModule().getFormMode() === 'create') {

                this.getModule().resetRelationFormField(
                    this.getModule().getModel().getFieldByName('injectionSubstation'),
                    this.getModule().getFormFieldByName('injectionSubstation')
                );

                this.getModule().resetRelationFormField(
                    this.getModule().getModel().getFieldByName('powerTransformer'),
                    this.getModule().getFormFieldByName('powerTransformer')
                );
            }

            this.getModule().getSearchFeedersButton().addClass('inactive');

            this.getModule().getPowerTransformerFeedersGrid().grid('removeAllRows');

            this.super('startController');
        },

        submitForm: function(formData, formMode, role) {

            if(role === 'form-search-feeders') {
                this.getModule().loadPowerTransformerOutputFeeders(this.getModule().getFormFieldValue('powerTransformer'));
            }
            else {

                if(role === "feeder-activate") {
                    formData.type = 'power-activate';
                }
                else {
                    formData.type = 'power-deactivate';
                }

                var selectedFeeders = this.getModule().getPowerTransformerFeedersGrid().grid('getSelectedRowsData');
                var selectedIndexes = this.getModule().getPowerTransformerFeedersGrid().grid('getSelectedRowsIndexes');

                if(this.isArray(selectedFeeders)) {

                    var feeder = null;

                    for(var i = 0, len = selectedFeeders.length; i < len; i++) {

                        feeder = selectedFeeders[i];

                        if(role === 'feeder-activate' && feeder.status === 'live') {
                            this.getApp().showNotification('information', 'You can not activate feeder: ' + feeder.name + ' because feeder is already live.');
                            continue;
                        }
                        else if(role === 'feeder-deactivate' && feeder.status === 'offline') {
                            this.getApp().showNotification('information', 'You can not deactivate feeder: ' + feeder.name + ' because feeder is already offline.');
                            continue;
                        }

                        this.setFeederGridIndex(selectedIndexes[i], feeder);

                        formData = $.extend({}, formData, {powerTransformerFeeder: feeder.id});

                        this.super('submitForm', [formData, formMode, 'form-save']);
                    }    
                }
            }
        },

        addRelationFieldEvent: function(fieldModelData) {

            switch(fieldModelData.name)
            {
                case 'businessDistrict':
                case 'injectionSubstation':
                case 'powerTransformer':
                case 'powerTransformerFeeder':

                    $(document).on(
                        fieldModelData.relationModel.events.READ_FOR_SELECT_COMPLETE,
                        {fieldModelData: fieldModelData},
                        this.relationDataLoadCompleteEventHandler
                    );

                    $(document).on(
                        fieldModelData.relationModel.events.READ_FOR_SELECT_ERROR,
                        {fieldModelData: fieldModelData},
                        this.relationDataLoadCompleteEventHandler
                    );

                break;

                default:
                    this.super('addRelationFieldEvent', [fieldModelData]);
                break;
            }
        },

        removeRelationFieldEvent: function(fieldModelData) {

            switch(fieldModelData.name)
            {
                case 'businessDistrict':
                case 'injectionSubstation':
                case 'powerTransformer':
                case 'powerTransformerFeeder':
                    $(document).off(fieldModelData.relationModel.events.READ_FOR_SELECT_COMPLETE, this.relationDataLoadCompleteEventHandler);
                    $(document).off(fieldModelData.relationModel.events.READ_FOR_SELECT_ERROR, this.relationDataLoadErrorEventHandler);
                break;

                default:
                    this.super('addRelationFieldEvent', [fieldModelData]);
                break;
            }
        }
    }
});
