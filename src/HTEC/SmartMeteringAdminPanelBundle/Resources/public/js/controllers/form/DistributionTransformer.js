HCM.define({

    name: 'Controllers.Form.DistributionTransformer',

    extendFrom: 'Controllers.RelationFieldForm',

    construct: function(app, module) {

        this.callConstructor('Controllers.RelationFieldForm', [app, module]);

        var self = this;

         var superFormChangedEventHandler = this.formChangedEventHandler;
        this.formChangedEventHandler = function(ev) {

            superFormChangedEventHandler(ev);

            var formFieldIinput = $(ev.currentTarget);
            var formFieldName = formFieldIinput.attr('name');

            switch(formFieldName)
            {
                case 'businessDistrict':

                    self.getModule().resetRelationFormField(
                        self.getModule().getModel().getFieldByName('injectionSubstation'),
                        self.getModule().getFormFieldByName('injectionSubstation')
                    );

                    self.getModule().resetRelationFormField(
                        self.getModule().getModel().getFieldByName('powerTransformer'),
                        self.getModule().getFormFieldByName('powerTransformer')
                    );

                    self.getModule().loadBusinessDistrictInjectionSubstations(self.getModule().getFormFieldValue(formFieldName, formFieldIinput));

                break;

                case 'injectionSubstation':

                    self.getModule().resetRelationFormField(
                        self.getModule().getModel().getFieldByName('powerTransformer'),
                        self.getModule().getFormFieldByName('powerTransformer')
                    );

                    self.getModule().loadInjectionSubstationPowerTransformers(self.getModule().getFormFieldValue(formFieldName, formFieldIinput));

                break;

                case 'powerTransformer':

                    self.getModule().resetRelationFormField(
                        self.getModule().getModel().getFieldByName('inputFeeder'),
                        self.getModule().getFormFieldByName('inputFeeder')
                    );

                    self.getModule().loadPowerTransformerOutputFeeders(self.getModule().getFormFieldValue(formFieldName, formFieldIinput));

                break;
            }
        };

        this.powerTransformerSelectedEventHandler = function(ev, ui) {

            self.getModule().setIsFormChanged(true);
            self.getModule().loadPowerTransformerOutputFeeders(ui.originalItem.id);

            $(ev.target).closest('.form-field-set').next().find('input, select, textarea').focus();
        };
    },

    methods: {

        startController: function() {

            if(this.getModule().getFormMode() === 'create') {

                this.getModule().resetRelationFormField(
                    this.getModule().getModel().getFieldByName('injectionSubstation'),
                    this.getModule().getFormFieldByName('injectionSubstation')
                );

                this.getModule().resetRelationFormField(
                    this.getModule().getModel().getFieldByName('powerTransformer'),
                    this.getModule().getFormFieldByName('powerTransformer')
                );
            }

            this.super('startController');
        },

        addRelationFieldEvent: function(fieldModelData) {

            switch(fieldModelData.name)
            {
                case 'businessDistrict':
                case 'injectionSubstation':
                case 'powerTransformer':
                case 'inputFeeder':
                    $(document).on(
                        fieldModelData.relationModel.events.READ_FOR_SELECT_COMPLETE,
                        {fieldModelData: fieldModelData},
                        this.relationDataLoadCompleteEventHandler
                    );

                    $(document).on(
                        fieldModelData.relationModel.events.READ_FOR_SELECT_ERROR,
                        {fieldModelData: fieldModelData},
                        this.relationDataLoadCompleteEventHandler
                    );
                break;

                default:
                    this.super('addRelationFieldEvent', [fieldModelData]);
                break;
            }
        },

        removeRelationFieldEvent: function(fieldModelData) {
            switch(fieldModelData.name)
            {
                case 'businessDistrict':
                case 'injectionSubstation':
                case 'powerTransformer':
                case 'inputFeeder':
                    $(document).off(fieldModelData.relationModel.events.READ_FOR_SELECT_COMPLETE, this.relationDataLoadCompleteEventHandler);
                    $(document).off(fieldModelData.relationModel.events.READ_FOR_SELECT_ERROR, this.relationDataLoadErrorEventHandler);
                break;

                default:
                    this.super('addRelationFieldEvent', [fieldModelData]);
                break;
            }
        }
    }
});
