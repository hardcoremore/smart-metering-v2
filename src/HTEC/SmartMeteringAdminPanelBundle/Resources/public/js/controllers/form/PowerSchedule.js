HCM.define({

    name: 'Controllers.Form.PowerSchedule',

    extendFrom: 'Controllers.RelationFieldForm',

    construct: function(app, module) {

        this.callConstructor('Controllers.RelationFieldForm', [app, module]);

        var self = this;

        var superFormChangedEventHandler = this.formChangedEventHandler;
        this.formChangedEventHandler = function(ev) {

            superFormChangedEventHandler(ev);

            var formFieldIinput = $(ev.currentTarget);
            var formFieldName = formFieldIinput.attr('name');

            switch(formFieldName)
            {
                case 'businessDistrict':

                    self.getModule().resetRelationFormField(
                        self.getModule().getModel().getFieldByName('injectionSubstation'),
                        self.getModule().getFormFieldByName('injectionSubstation')
                    );

                    self.getModule().resetRelationFormField(
                        self.getModule().getModel().getFieldByName('powerTransformer'),
                        self.getModule().getFormFieldByName('powerTransformer')
                    );

                    self.getModule().resetRelationFormField(
                        self.getModule().getModel().getFieldByName('powerTransformerFeeder'),
                        self.getModule().getFormFieldByName('powerTransformerFeeder')
                    );

                    self.getModule().loadBusinessDistrictInjectionSubstations(self.getModule().getFormFieldValue(formFieldName, formFieldIinput));
                break;

                case 'injectionSubstation':

                    self.getModule().resetRelationFormField(
                        self.getModule().getModel().getFieldByName('powerTransformer'),
                        self.getModule().getFormFieldByName('powerTransformer')
                    );

                    self.getModule().resetRelationFormField(
                        self.getModule().getModel().getFieldByName('powerTransformerFeeder'),
                        self.getModule().getFormFieldByName('powerTransformerFeeder')
                    );

                    self.getModule().loadInjectionSubstationPowerTransformers(self.getModule().getFormFieldValue(formFieldName, formFieldIinput));
                break;

                case 'powerTransformer':

                    self.getModule().resetRelationFormField(
                        self.getModule().getModel().getFieldByName('powerTransformerFeeder'),
                        self.getModule().getFormFieldByName('powerTransformerFeeder')
                    );

                    self.getModule().loadPowerTransformerOutputFeeders(self.getModule().getFormFieldValue(formFieldName, formFieldIinput));
                break;
            }

            switch(formFieldName)
            {
                case 'businessDistrict':
                case 'injectionSubstation':
                case 'powerTransformer':
                case 'powerTransformerFeeder':
                case 'distributionTransformerFeeder':
                case 'date':
                    if(self.getModule().getFormFieldValue('businessDistrict') && self.getModule().getFormFieldValue('date')) {
                        self.loadPowerSchedulesForWeekPreview();
                    }
                break;
            }
            
        };

        this.distributionTransformerSelectedEventHandler = function(ev, ui) {

            self.getModule().setIsFormChanged(true);
            self.getModule().loadDistributionTransformerOutputFeeders(ui.originalItem.id);

            $(ev.target).closest('.form-field-set').next().find('input, select, textarea').focus();

            if(self.getModule().getFormFieldValue('businessDistrict') && self.getModule().getFormFieldValue('date')) {
                self.loadPowerSchedulesForWeekPreview();
            }
        };

        this.readAllRecordsCompleteEventHandler = function(ev) {

            if(ev.scope === self.getModule()) {

                self.getModule().getPowerScheduleForWeekScheduler().scheduler('removeAllEvents');

                if(self.isArray(ev.responseData)) {

                    for(var i = 0, len = ev.responseData.length; i < len; i++) {

                        self.getModule().getSchedulerHelper().addRecordToScheduler(
                            self.getModule().getPowerScheduleForWeekScheduler(),
                            ev.responseData[i]
                        );
                    }
                }
            }
        };

        var superRecordCreateCompleteEventHandler = this.recordCreateCompleteEventHandler;
        this.recordCreateCompleteEventHandler = function(ev) {

            superRecordCreateCompleteEventHandler(ev);

            var filters = self.getModule().getFiltersForLoadingPowerSchedules();
                filters = self.getModule().getFiltersFromPowerSchedule(ev.responseData, filters);

            self.loadPowerSchedulesForWeekPreview(filters, ev.responseData.date);
        };

        var superRecordUpdateCompleteEventHandler = this.recordUpdateCompleteEventHandler;
        this.recordUpdateCompleteEventHandler = function(ev) {
            superRecordUpdateCompleteEventHandler(ev);
            self.loadPowerSchedulesForWeekPreview();
        };
    },

    methods: {

        addEvents: function() {

            this.super('addEvents');

            this.getModule().getDistributionTransformerAutoComplete().autocomplete({
                select: this.distributionTransformerSelectedEventHandler
            });

            var modelEvents = this.getModule().getModel().events;

            $(document).on(modelEvents.READ_ALL_COMPLETE, this.readAllRecordsCompleteEventHandler);
            $(document).on(modelEvents.READ_ALL_ERROR, this.readAllRecordsErrorEventHandler);
        },

        removeEvents: function() {
            
            this.super('removeEvents');

            this.getModule().getDistributionTransformerAutoComplete().autocomplete({
                select: null
            });

            var modelEvents = this.getModule().getModel().events;

            $(document).off(modelEvents.READ_ALL_COMPLETE, this.readAllRecordsCompleteEventHandler);
            $(document).off(modelEvents.READ_ALL_ERROR, this.readAllRecordsErrorEventHandler);
        },

        startController: function() {

            if(this.getModule().getFormMode() === 'create') {

                this.getModule().resetRelationFormField(
                    this.getModule().getModel().getFieldByName('injectionSubstation'),
                    this.getModule().getFormFieldByName('injectionSubstation')
                );

                this.getModule().resetRelationFormField(
                    this.getModule().getModel().getFieldByName('powerTransformer'),
                    this.getModule().getFormFieldByName('powerTransformer')
                );

                this.getModule().resetRelationFormField(
                    this.getModule().getModel().getFieldByName('powerTransformerFeeder'),
                    this.getModule().getFormFieldByName('powerTransformerFeeder')
                );

                this.getModule().resetRelationFormField(
                    this.getModule().getModel().getFieldByName('distributionTransformerFeeder'),
                    this.getModule().getFormFieldByName('distributionTransformerFeeder')
                );
            }

            this.super('startController');

            if(this.getModule().getFormMode() === 'edit') {

                var editingRecordData = this.getModule().getEditingRecordData();

                var filters = this.getModule().getFiltersForLoadingPowerSchedules();
                    filters = this.getModule().getFiltersFromPowerSchedule(editingRecordData, filters);

                this.loadPowerSchedulesForWeekPreview(filters);
            }
        },

        loadPowerSchedulesForWeekPreview: function(filters, date) {

            var startDate = this.getModule().getStartDateForLoadingPowerSchedules(date);
            var endDate = this.getModule().getEndDateForLoadingPowerSchedules(startDate);

            this.getModule().readPowerSchedulesForDateRange(
                startDate.format('YYYY-MM-DD'),
                endDate.format('YYYY-MM-DD'),
                filters || this.getModule().getFiltersForLoadingPowerSchedules()
            );
        },

        addRelationFieldEvent: function(fieldModelData) {

            switch(fieldModelData.name)
            {
                case 'businessDistrict':
                case 'injectionSubstation':
                case 'powerTransformer':
                case 'powerTransformerFeeder':
                case 'distributionTransformerFeeder':
                    $(document).on(
                        fieldModelData.relationModel.events.READ_FOR_SELECT_COMPLETE,
                        {fieldModelData: fieldModelData},
                        this.relationDataLoadCompleteEventHandler
                    );

                    $(document).on(
                        fieldModelData.relationModel.events.READ_FOR_SELECT_ERROR,
                        {fieldModelData: fieldModelData},
                        this.relationDataLoadCompleteEventHandler
                    );
                break;

                default:
                    this.super('addRelationFieldEvent', [fieldModelData]);
                break;
            }
        },

        removeRelationFieldEvent: function(fieldModelData) {

            switch(fieldModelData.name)
            {
                case 'businessDistrict':
                case 'injectionSubstation':
                case 'powerTransformer':
                case 'powerTransformerFeeder':
                case 'distributionTransformerFeeder':
                    $(document).off(fieldModelData.relationModel.events.READ_FOR_SELECT_COMPLETE, this.relationDataLoadCompleteEventHandler);
                    $(document).off(fieldModelData.relationModel.events.READ_FOR_SELECT_ERROR, this.relationDataLoadErrorEventHandler);
                break;

                default:
                    this.super('addRelationFieldEvent', [fieldModelData]);
                break;
            }
        }
    }
});
