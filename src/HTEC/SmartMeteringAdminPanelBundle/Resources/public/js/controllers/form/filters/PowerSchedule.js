HCM.define({

    name: 'Controllers.Form.Filters.PowerSchedule',

    extendFrom: 'Controllers.RelationFieldForm',

    construct: function(app, module) {

        this.callConstructor('Controllers.RelationFieldForm', [app, module]);

        var self = this;

        var superFormChangedEventHandler = this.formChangedEventHandler;
        this.formChangedEventHandler = function(ev) {

            superFormChangedEventHandler(ev);

            var formFieldIinput = $(ev.currentTarget);
            var formFieldName = formFieldIinput.attr('name');

            switch(formFieldName)
            {
                case 'businessDistrict':

                    self.getModule().resetRelationFormField(
                        self.getModule().getModel().getFieldByName('injectionSubstation'),
                        self.getModule().getFormFieldByName('injectionSubstation')
                    );

                    self.getModule().resetRelationFormField(
                        self.getModule().getModel().getFieldByName('powerTransformer'),
                        self.getModule().getFormFieldByName('powerTransformer')
                    );

                    self.getModule().resetRelationFormField(
                        self.getModule().getModel().getFieldByName('powerTransformerFeeder'),
                        self.getModule().getFormFieldByName('powerTransformerFeeder')
                    );

                    self.getModule().loadBusinessDistrictInjectionSubstations(
                        self.getModule().getFormFieldValue(formFieldName, formFieldIinput)
                    );

                break;

                case 'injectionSubstation':

                    self.getModule().resetRelationFormField(
                        self.getModule().getModel().getFieldByName('powerTransformer'),
                        self.getModule().getFormFieldByName('powerTransformer')
                    );

                    self.getModule().resetRelationFormField(
                        self.getModule().getModel().getFieldByName('powerTransformerFeeder'),
                        self.getModule().getFormFieldByName('powerTransformerFeeder')
                    );

                    self.getModule().loadInjectionSubstationPowerTransformers(
                        self.getModule().getFormFieldValue(formFieldName, formFieldIinput)
                    );

                break;

                case 'powerTransformer':

                    self.getModule().resetRelationFormField(
                        self.getModule().getModel().getFieldByName('powerTransformerFeeder'),
                        self.getModule().getFormFieldByName('powerTransformerFeeder')
                    );

                    self.getModule().loadPowerTransformerOutputFeeders(
                        self.getModule().getFormFieldValue(formFieldName, formFieldIinput)
                    );

                break;
            }
        };
    },

    methods: {

        // we do not need any crud events for filters form
        addCrudEvents: function() {},

        addRelationFieldEvent: function(fieldModelData) {

            switch(fieldModelData.name)
            {
                case 'powerSource':
                case 'businessDistrict':
                case 'injectionSubstation':
                case 'powerTransformer':
                case 'powerTransformerFeeder':
                    $(document).on(
                        fieldModelData.relationModel.events.READ_FOR_SELECT_COMPLETE,
                        {fieldModelData: fieldModelData},
                        this.relationDataLoadCompleteEventHandler
                    );

                    $(document).on(
                        fieldModelData.relationModel.events.READ_FOR_SELECT_ERROR,
                        {fieldModelData: fieldModelData},
                        this.relationDataLoadCompleteEventHandler
                    );
                break;

                default:
                    this.super('addRelationFieldEvent', [fieldModelData]);
                break;
            }
        },

        removeRelationFieldEvent: function(fieldModelData) {

            switch(fieldModelData.name)
            {
                case 'powerSource':
                case 'businessDistrict':
                case 'injectionSubstation':
                case 'powerTransformer':
                case 'powerTransformerFeeder':
                    $(document).off(
                        fieldModelData.relationModel.events.READ_FOR_SELECT_COMPLETE,
                        this.relationDataLoadCompleteEventHandler
                    );
                    $(document).off(
                        fieldModelData.relationModel.events.READ_FOR_SELECT_ERROR,
                        this.relationDataLoadErrorEventHandler
                    );
                break;

                default:
                    this.super('addRelationFieldEvent', [fieldModelData]);
                break;
            }
        }
    }
});
