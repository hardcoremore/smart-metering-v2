HCM.define({

    name: 'Controllers.Form.PowerAllocate',

    extendFrom: 'Controllers.RelationFieldForm',

    construct: function(app, module) {

        this.callConstructor('Controllers.RelationFieldForm', [app, module]);

        var self = this;

        var powerAllocationsToSave = {};
        var powerAllocationLastSavedIndex = -1;
        var currentPowerAllocationDataBatchSaveCount = -1;

        var powerAllocationsDataForPowerForecast = {};

        this.addPowerAllocationToSave = function(index, powerAllocationData) {
            powerAllocationsToSave[index] = powerAllocationData;
        };

        this.removePowerAllocationToSave = function(index) {
            delete powerAllocationsToSave[index];
        };

        this.getPowerAllocationToSave = function(index) {
            return powerAllocationsToSave[index];
        };

        this.getAllPowerAllocationsToSave = function() {
            return powerAllocationsToSave;
        };


        this.setPowerAllocationLastSavedIndex = function(savedIndex) {
            powerAllocationLastSavedIndex = savedIndex;
        };

        this.getPowerAllocationLastSavedIndex = function() {
            return powerAllocationLastSavedIndex;
        };

        this.setCurrentPowerAllocationDataBatchSaveCount = function(count) {
            currentPowerAllocationDataBatchSaveCount = count;
        };

        this.getCurrentPowerAllocationDataBatchSaveCount = function() {
            return currentPowerAllocationDataBatchSaveCount;
        };


        this.setPowerAllocationsDataForPowerForecast = function(powerForecastId, data) {
            powerAllocationsDataForPowerForecast[powerForecastId] = data;
        };

        this.getPowerAllocationsDataForPowerForecast = function(powerForecastId) {
            return powerAllocationsDataForPowerForecast[powerForecastId];  
        };


        var superFormChangedEventHandler = this.formChangedEventHandler;
        this.formChangedEventHandler = function(ev) {

            superFormChangedEventHandler(ev);

            var formFieldIinput = $(ev.currentTarget);
            var formFieldName = formFieldIinput.attr('name');

            if(formFieldName === 'powerForecast') {

                var selectedData = self.getModule().getRelationFieldSelectedData(formFieldName);

                self.getModule().getBusinessDistrictAllocationGrid().grid('removeAllRows');

                if(selectedData) {

                    var dataForPowerForecast = self.getPowerAllocationsDataForPowerForecast(selectedData.id);

                    if(dataForPowerForecast) {
                        for(var i = 0, len = dataForPowerForecast.length; i < len; i++) {
                            self.getModule().getBusinessDistrictAllocationGrid().grid('addRow', dataForPowerForecast[i]);
                        }
                    }
                    else {

                        self.getModule().updatePowerAllocationsGrid(selectedData);
                    }
                }
            }
        };

        this.businessDistrictAllocationGridCellEditStarted = function(ev, ui) {

            if(Number(ui.rowData.powerOverride) > 0) {
                self.getModule().updateMaxOverrideValue(self.getModule().getTotalRemainingAvailablePowerAllocation() + Number(ui.rowData.powerOverride));
            }
        };

        var superFormSubmittedEventHandler = this.formSubmittedEventHandler;
        this.formSubmittedEventHandler = function(ev) {

            var overridenBusinessDistricts = self.getModule().getOverridenBusinessDistrictData();

            if(overridenBusinessDistricts.length > 0) {
                self.getModule().displayOverridePowerPopup(overridenBusinessDistricts);
                self.getModule().getConfirmPowerAllocationPopup().popup('option', {
                    onFormSubmitted: self.confirmPowerAllocationPopupSubmitHandler,
                });
            }
            else {
                superFormSubmittedEventHandler(ev);
            }
        };

        this.confirmPowerAllocationPopupSubmitHandler = function(ev, ui) {

            self.getModule().getConfirmPowerAllocationPopup().popup('close');

            var role = self.getModule().getFormSubmitButton().data('role');
            var formData = self.getModule().getFormData();
            var formMode = self.getModule().getFormMode();

            self.submitForm(formData, formMode, role);
        };

        this.recordCreateCompleteEventHandler = function(ev) {

            self.getModule().blockFormField('powerForecast');
            self.getModule().getFormSubmitButton().addClass('inactive');
            self.getModule().getFormCloseButton().addClass('inactive');

            self.getModule().setIsFormChanged(false);

            self.addPowerAllocationsToSaveQuee(ev.responseData);
        };

        this.recordUpdateCompleteEventHandler = function(ev) {

            if(ev.scope === self.getModule()) {
                self.getModule().setIsFormChanged(false);

                self.getModule().blockFormField('powerForecast');
                self.getModule().getFormSubmitButton().addClass('inactive');
                self.getModule().getFormCloseButton().addClass('inactive');

                self.getModule().setEditingRecordData(ev.responseData);
                self.addPowerAllocationsToSaveQuee(ev.responseData);
            }
        };

        this.powerAllocateDataSaveCompleteEventHandler = function(ev) {

            var message = 'You have successfully allocated <strong>';
                message += ev.responseData.powerAllocated.toFixed(4);
                message += ' MW </strong> power to <strong>' + ev.responseData.businessDistrict.name + '</strong>';

            self.getApp().showNotification('success', message, null, 10000);

            var data = $.extend({}, ev.responseData, ev.scope.gridData);
                data.isAllocated = true;

            self.getModule().getBusinessDistrictAllocationGrid().grid('updateRow', ev.scope.gridIndex, data);

            self.removePowerAllocationToSave(ev.scope.gridIndex);

            var remainingIndexes = Object.keys(self.getAllPowerAllocationsToSave());            

            if(remainingIndexes.length === 0) {

                var allData = self.getModule().getBusinessDistrictAllocationGrid().grid('getAllData');

                self.setPowerAllocationsDataForPowerForecast(self.getModule().getFormFieldValue('powerForecast'), allData);
                self.setPowerAllocationLastSavedIndex(-1);
                self.setCurrentPowerAllocationDataBatchSaveCount(-1);

                self.getModule().unblockFormField('powerForecast');
                self.getModule().getFormSubmitButton().removeClass('inactive');
                self.getModule().getFormCloseButton().removeClass('inactive');

                self.getModule().setEditingRecordData(ev.responseData.powerAllocate);
                self.getModule().setFormMode('edit');
                self.getModule().updateFormSubmitButtonLabel(self.getModule().getFormMode());
            }
            else {

                if(self.getPowerAllocationLastSavedIndex() % self.getCurrentPowerAllocationDataBatchSaveCount() === 2) {

                    self.setPowerAllocationLastSavedIndex(self.getPowerAllocationLastSavedIndex() + 1);

                    if(remainingIndexes.length > 2) {
                        self.savePowerAllocations(3);
                    }
                    else {
                        self.savePowerAllocations(remainingIndexes.length);
                    }
                }
                else {
                    self.setPowerAllocationLastSavedIndex(self.getPowerAllocationLastSavedIndex() + 1);
                }
            }
        };

        this.powerAllocateDataSaveErrorEventHandler = function(ev) {
            self.getApp().showNotification('error', 'Error occurred while allocating power to business district :(');
        };

        this.powerAllocateDataReadAllCompleteEventHandler = function(ev) {

            self.getModule().getBusinessDistrictAllocationGrid().grid('removeAllRows');

            var editingPowerAllocationsData = self.getModule().getEditingPowerAllocationsData();
            var recrodData;
            var mergedData = [];

            for(var i = 0, len = ev.responseData.length; i < len; i++) {

                recordData = ev.responseData[i];
                recordData.isAllocated = true;

                if(editingPowerAllocationsData) {
                    for(var c = 0, clen = editingPowerAllocationsData.length; c < clen; c++) {
                        if(editingPowerAllocationsData[c].businessDistrict.id === recordData.businessDistrict.id) {
                            recordData = $.extend(recordData, editingPowerAllocationsData[c]);
                        }
                    }
                }

                mergedData.push(recordData);

                self.getModule().getBusinessDistrictAllocationGrid().grid('addRow', recordData);
            }

            self.getModule().setEditingPowerAllocationsData(mergedData);
        };

        this.powerAllocateDataReadAllErrorEventHandler = function(ev) {
            self.getApp().showNotification('error', 'Reading power allocation data failed. :(');
        };
    },

    methods: {

        addEvents: function() {
            
            this.super('addEvents');

            this.getModule().getFormFieldByName('amountForecasted').on('keydown', this.onlyNumbersKeyDownHandler);

            this.getModule().getBusinessDistrictAllocationGrid().grid('option', {
                onCellEdited: this.businessDistrictAllocationGridCellEdited.bind(this),
                onCellEditStarted: this.businessDistrictAllocationGridCellEditStarted.bind(this)
            });

            var powerAllocateDataEvents = this.getModule().getPowerAllocateDataModel().events;

            $(document).on(powerAllocateDataEvents.READ_ALL_COMPLETE, this.powerAllocateDataReadAllCompleteEventHandler);
            $(document).on(powerAllocateDataEvents.READ_ALL_ERROR, this.powerAllocateDataReadAllErrorEventHandler);

            $(document).on(powerAllocateDataEvents.CREATE_COMPLETE, this.powerAllocateDataSaveCompleteEventHandler);
            $(document).on(powerAllocateDataEvents.CREATE_ERROR, this.powerAllocateDataCreateErrorEventHandler);

            $(document).on(powerAllocateDataEvents.UPDATE_COMPLETE, this.powerAllocateDataSaveCompleteEventHandler);
            $(document).on(powerAllocateDataEvents.UPDATE_ERROR, this.powerAllocateDataPatchErrorEventHandler);
        },

        removeEvents: function() {
            
            this.super('removeEvents');

            this.getModule().getFormFieldByName('amountForecasted').off('keydown', this.onlyNumbersKeyDownHandler);

            this.getModule().getBusinessDistrictAllocationGrid().grid('option', {
                onCellEdited: null
            });

            if(this.getModule().getConfirmPowerAllocationPopup()) {
                this.getModule().getConfirmPowerAllocationPopup().popup('option', {
                    onFormSubmitted: null,
                });
            }

            var powerAllocateDataEvents = this.getModule().getPowerAllocateDataModel().events;

            $(document).off(powerAllocateDataEvents.READ_ALL_COMPLETE, this.powerAllocateDataReadAllCompleteEventHandler);
            $(document).off(powerAllocateDataEvents.READ_ALL_ERROR, this.powerAllocateDataReadAllErrorEventHandler);

            $(document).off(powerAllocateDataEvents.CREATE_COMPLETE, this.powerAllocateDataSaveCompleteEventHandler);
            $(document).off(powerAllocateDataEvents.CREATE_ERROR, this.powerAllocateDataCreateErrorEventHandler);

            $(document).off(powerAllocateDataEvents.UPDATE_COMPLETE, this.powerAllocateDataSaveCompleteEventHandler);
            $(document).off(powerAllocateDataEvents.UPDATE_ERROR, this.powerAllocateDataPatchErrorEventHandler);
        },

        startController: function() {

            this.super('startController');

            this.getModule().setEditingPowerAllocationsData(null);

            if(this.getModule().getFormMode() === 'edit') {
                this.getModule().getPowerAllocateDataModel().readAll({
                    parameters: {
                        powerAllocate: this.getModule().getEditingRecordData().id
                    },
                    scope: this.getModule()
                });
            }
        },

        stopController: function() {
            this.super('stopController');
            this.getModule().getBusinessDistrictAllocationGrid().grid('removeAllRows');
        },

        addPowerAllocationsToSaveQuee: function(powerAllocation) {

            var allBusinessDistrictData = this.getModule().getBusinessDistrictAllocationGrid().grid('getAllData');
            var allocationData;

            for(var i = 0, len = allBusinessDistrictData.length; i < len; i++) {

                allocationData = {
                    powerAllocate: powerAllocation.id,
                    businessDistrict: allBusinessDistrictData[i].businessDistrict.id,
                    powerAllocated: allBusinessDistrictData[i].powerAllocated,
                    powerForecasted: allBusinessDistrictData[i].powerForecasted,
                    powerOverride: allBusinessDistrictData[i].powerOverride
                };

                if(this.getModule().getFormMode() === 'edit') {
                    allBusinessDistrictData[i].isAllocated = false;
                    this.getModule().getBusinessDistrictAllocationGrid().grid('updateRow', i, allBusinessDistrictData[i]);
                }

                this.addPowerAllocationToSave(i, allocationData);
            }

            this.savePowerAllocations(3);
        },

        savePowerAllocations: function(count) {

            if(count < 1) return;

            if(this.getPowerAllocationLastSavedIndex() === -1) {
                this.setPowerAllocationLastSavedIndex(0);
            }

            this.setCurrentPowerAllocationDataBatchSaveCount(count);

            var powerAllocationData, gridData, scope;

            for(var i = this.getPowerAllocationLastSavedIndex(), len = this.getPowerAllocationLastSavedIndex() + count; i < len; i++) {

                powerAllocationData = this.getPowerAllocationToSave(i);

                if(powerAllocationData) {

                    gridData = this.getModule().getBusinessDistrictAllocationGrid().grid('getRowDataByIndex', i);

                    scope = {
                        module: this.getModule(),
                        gridIndex: i,
                        gridData: {
                            powerOverride: gridData.powerOverride,
                            numberOfCustomers: gridData.numberOfCustomers,
                            powerForecasted: gridData.powerForecasted
                        }
                    };

                    if(this.getModule().getFormMode() === 'create') {

                        this.getModule().getPowerAllocateDataModel().create(
                            powerAllocationData,
                            {
                                scope: scope,
                                showLoader: false
                            }
                        );
                    }
                    else {
                        this.getModule().getPowerAllocateDataModel().update(
                            gridData.id,                            
                            powerAllocationData,
                            {
                                scope: scope,
                                showLoader: false
                            }
                        );   
                    }
                }
            }
        },

        addRelationFieldEvent: function(fieldModelData) {

            if(fieldModelData.name === 'powerForecast') {
                $(document).on(
                    fieldModelData.relationModel.events.READ_ALL_FOR_TODAY_WITH_DETAILS_COMPLETE,
                    {fieldModelData: fieldModelData},
                    this.relationDataLoadCompleteEventHandler
                );
                
                $(document).on(
                    fieldModelData.relationModel.events.READ_ALL_FOR_TODAY_WITH_DETAILS_ERROR,
                    {fieldModelData: fieldModelData},
                    this.relationDataLoadErrorEventHandler
                );
            }
            else {
                this.super('addRelationFieldEvent', [fieldModelData]);
            }
        },

        removeRelationFieldEvent: function(fieldModelData) {

            if(fieldModelData.name === 'powerForecast') {
                $(document).off(fieldModelData.relationModel.events.READ_ALL_FOR_TODAY_WITH_DETAILS_COMPLETE, this.relationDataLoadCompleteEventHandler);
                $(document).off(fieldModelData.relationModel.events.READ_ALL_FOR_TODAY_WITH_DETAILS_ERROR, this.relationDataLoadErrorEventHandler);
            }
            else {
                this.super('removeRelationFieldEvent', [fieldModelData]);
            }
        },

        submitForm: function(formData, formMode, role) {

            delete formData.businessDistrictAllocations;

            var allBusinessDistrictData = this.getModule().getBusinessDistrictAllocationGrid().grid('getAllData');

            var highestAllocationBusinessDistrict = 0;
            var highestBusinessDistrictAllocationValue = 0;

            for(var i in allBusinessDistrictData) {

                if(allBusinessDistrictData[i].powerAllocated > highestBusinessDistrictAllocationValue) {
                    highestBusinessDistrictAllocationValue = allBusinessDistrictData[i].powerAllocated;
                    highestAllocationBusinessDistrict = allBusinessDistrictData[i].businessDistrict.id;
                }
            }

            formData.highestAllocationBusinessDistrict = highestAllocationBusinessDistrict;
            formData.highestBusinessDistrictAllocationValue = highestBusinessDistrictAllocationValue;


            this.super('submitForm', [formData, formMode, role]);
        },

        businessDistrictAllocationGridCellEdited: function(ev, ui) {

            var allData = this.getModule().getBusinessDistrictAllocationGrid().grid('getAllData');
            var totalAllocation = 0, totalOverride = 0, i = 0;
            var overrideIndexes = [];
            var allocationPercentages = [];
            var remainingNumberOfCustomers = [];
            var remainingTotalNumberOfCustomers=  0;
            var record = null;

            for(i = 0, len = allData.length; i < len; i++) {

                record = allData[i];

                if(record.powerOverride > 0) {
                    totalOverride += Number(record.powerOverride);
                    overrideIndexes.push(i);
                }
                else {
                    remainingTotalNumberOfCustomers += record.numberOfCustomers;
                }

                totalAllocation += Number(record.powerForecasted);
            }

            var remainingAllocation = Math.abs(totalAllocation - totalOverride);
            var allocationPercentage = 0;

            this.getModule().updateMaxOverrideValue(remainingAllocation);

            for(i = 0, len = allData.length; i < len; i++) {

                if(overrideIndexes.length > 0) {

                    // if record is not overriden
                    if(overrideIndexes.indexOf(i) === -1) {

                        allocationPercentage = 100 / remainingTotalNumberOfCustomers * allData[i].numberOfCustomers;
                        allData[i].powerAllocated = remainingAllocation * allocationPercentage  / 100;
                    }
                    else if(Number(ui.rowIndex) === i) {
                        allData[i].powerAllocated = Number(ui.newValue);
                    }

                    this.getModule().getBusinessDistrictAllocationGrid().grid('updateRow', i, allData[i]);
                }
                else {
                    allData[i].powerAllocated = allData[i].powerForecasted;
                    this.getModule().getBusinessDistrictAllocationGrid().grid('updateRow', i, allData[i]);
                }
            }
        }
    }
});
