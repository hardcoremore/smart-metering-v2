HCM.define({

    name: 'Controllers.Form.EstimatedLoad',

    extendFrom: 'Controllers.RelationFieldForm',

    construct: function(app, module) {

        this.callConstructor('Controllers.RelationFieldForm', [app, module]);

        var self = this;

        this.customerSelectedEventHandler = function(ev, ui) {

            self.getModule().setIsFormChanged(true);
            $(ev.target).closest('.form-field-set').next().find('input, select, textarea').focus();
        };

        this.recordCreateCompleteEventHandler = function(ev) {

            self.getApp().showNotification('success', 'You have successfully created estimated load.');

            self.getModule().setIsFormChanged(false);
            self.getModule().setEditingRecordData(ev.responseData);
            self.getModule().setFormMode('edit');
            self.getModule().getFormSubmitButton().find('.button-label').text('Save');

            self.getModule().getEstimatedLoadApplianceModule().getModel().setCurrentEstimatedLoad(self.getModule().getEditingRecordData());
            self.getModule().unblockEstimatedLoadApplianceModule();
        };

        this.moduleReadyEventHandler = function(ev) {

            if(ev.module.getNamespace() === 'Modules.EstimatedLoadAppliance') {

                self.getModule().setEstimatedLoadApplianceModule(ev.module);
                self.getApp().getModuleManager().startModule(ev.module);

                if(self.getModule().getEditingRecordData() === null || self.getModule().getEditingRecordData() === undefined) {
                    self.getModule().blockEstimatedLoadApplianceModule();
                }
                else {

                    ev.module.getModel().setCurrentEstimatedLoad(self.getModule().getEditingRecordData());
                    self.getModule().unblockEstimatedLoadApplianceModule();

                    if(self.getModule().getEditingRecordData().applianceCount > 0) {
                        self.getModule().blockCustomerCategoryField();
                    }
                    else {
                        self.getModule().unblockFormField('customerCategory');
                    }
                }

                if(self.getModule().getEditingRecordData()) {
                    ev.module.getModel().setCurrentEstimatedLoad(self.getModule().getEditingRecordData());
                }

                self.addEstimatedLoadApplianceEvents();
            }
        };

        this.customerCategoryChangedEventHandler = function(ev) {

            if(self.getModule().getFormMode() === 'edit') {

                var editingRecordData = $.extend({}, self.getModule().getEditingRecordData());
                    editingRecordData.customerCategory = self.getModule().getRelationFieldSelectedData('customerCategory');

                self.getModule().getEstimatedLoadApplianceModule().getModel().setCurrentEstimatedLoad(editingRecordData);
            }
        };

        this.applianceCreateCompleteEventHandler = function(ev) {

            var estimatedLoad = self.getModule().getEditingRecordData();

            if(estimatedLoad) {

                estimatedLoad.applianceCount = estimatedLoad.applianceCount + 1;

                self.getModule().setEditingRecordData(estimatedLoad);
                self.getModule().blockCustomerCategoryField();
            }
        };

        this.applianceDeleteCompleteEventHandler = function(ev) {

            var estimatedLoad = self.getModule().getEditingRecordData();

            if(estimatedLoad) {

                estimatedLoad.applianceCount = estimatedLoad.applianceCount - 1;
                self.getModule().setEditingRecordData(estimatedLoad);

                if(estimatedLoad.applianceCount === 0) {
                    self.getModule().unblockFormField('customerCategory');
                }
            }
        };
    },

    methods: {

        startController: function() {

            if(this.getModule().getEstimatedLoadApplianceModule()) {

                this.addEstimatedLoadApplianceEvents();

                if(this.getModule().getEditingRecordData() === null || this.getModule().getEditingRecordData() === undefined) {

                    this.getModule().getEstimatedLoadApplianceModule().emptyMainViewComponent();
                    this.getModule().blockEstimatedLoadApplianceModule();
                    this.getModule().getEstimatedLoadApplianceModule().getModel().setCurrentEstimatedLoad(null);
                }
                else {

                    this.getModule().unblockEstimatedLoadApplianceModule();
                    this.getModule().getEstimatedLoadApplianceModule().getModel().setCurrentEstimatedLoad(this.getModule().getEditingRecordData());

                    if(this.getModule().getEditingRecordData().applianceCount > 0) {
                        this.getModule().blockCustomerCategoryField();
                    }
                    else {
                        this.getModule().unblockFormField('customerCategory');
                    }
                }
            }

            this.super('startController');
        },

        addEstimatedLoadApplianceEvents: function() {

            var moduleEvents = this.getModule().getEstimatedLoadApplianceModule().getModel().events;

            $(document).on(moduleEvents.CREATE_COMPLETE, this.applianceCreateCompleteEventHandler);
            $(document).on(moduleEvents.DELETE_COMPLETE, this.applianceDeleteCompleteEventHandler);
        },

        removeEstimatedLoadApplianceEvents: function() {

            var moduleEvents = this.getModule().getEstimatedLoadApplianceModule().getModel().events;

            $(document).off(moduleEvents.CREATE_COMPLETE, this.applianceCreateCompleteEventHandler);
            $(document).off(moduleEvents.DELETE_COMPLETE, this.applianceDeleteCompleteEventHandler);
        },

        addEvents: function() {
            
            this.super('addEvents');

            this.getModule().getCustomerAutoComplete().autocomplete({
                select: this.customerSelectedEventHandler
            });

            var constants = this.getApp().getConstants();

            $(document).on(constants.MODULE_READY_EVENT, this.moduleReadyEventHandler);

            this.getModule().getFormFieldByName('customerCategory').on('change', this.customerCategoryChangedEventHandler);
        },

        removeEvents: function() {
            
            this.super('removeEvents');

            this.getModule().getCustomerAutoComplete().autocomplete({
                select: null
            });

            var constants = this.getApp().getConstants();

            $(document).off(constants.MODULE_READY_EVENT, this.moduleReadyEventHandler);

            this.getModule().getFormFieldByName('customerCategory').off('change', this.customerCategoryChangedEventHandler);

            this.removeEstimatedLoadApplianceEvents();
        }
    }
});
