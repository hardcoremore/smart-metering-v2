HCM.define({

    name: 'Controllers.Form.Meter',

    extendFrom: 'Controllers.RelationFieldForm',

    construct: function(app, module) {

        this.callConstructor('Controllers.RelationFieldForm', [app, module]);

        var self = this;

        this.customerSelectedEventHandler = function(ev, ui) {
            self.getModule().setIsFormChanged(true);
            $(ev.target).closest('.form-field-set').next().find('input, select, textarea').focus();
        };

        this.distributionTransformerSelectedEventHandler = function(ev, ui) {
            self.getModule().setIsFormChanged(true);
            self.getModule().loadDistributionTransformerOutputFeeders(ui.originalItem.id);
            $(ev.target).closest('.form-field-set').next().find('input, select, textarea').focus();
        };
    },

    methods: {

        addEvents: function() {
            
            this.super('addEvents');

            this.getModule().getCustomerAutoComplete().autocomplete({
                select: this.customerSelectedEventHandler
            });

            this.getModule().getDistributionTransformerAutoComplete().autocomplete({
                select: this.distributionTransformerSelectedEventHandler
            });
        },

        removeEvents: function() {
            
            this.super('removeEvents');

            this.getModule().getCustomerAutoComplete().autocomplete({
                select: null
            });

            this.getModule().getDistributionTransformerAutoComplete().autocomplete({
                select: null
            });
        },

         startController: function() {

            if(this.getModule().getFormMode() === 'create') {

                this.getModule().resetRelationFormField(
                    this.getModule().getModel().getFieldByName('feeder'),
                    this.getModule().getFormFieldByName('feeder')
                );
            }

            this.super('startController');
        },

        addRelationFieldEvent: function(fieldModelData) {

            if(fieldModelData.name === 'feeder') {

                $(document).on(
                        fieldModelData.relationModel.events.READ_FOR_SELECT_COMPLETE,
                        {fieldModelData: fieldModelData},
                        this.relationDataLoadCompleteEventHandler
                );

                $(document).on(
                    fieldModelData.relationModel.events.READ_FOR_SELECT_ERROR,
                    {fieldModelData: fieldModelData},
                    this.relationDataLoadCompleteEventHandler
                );
            }
            else {
                this.super('addRelationFieldEvent', [fieldModelData]);
            }
        },

        removeRelationFieldEvent: function(fieldModelData) {
            if(fieldModelData.name === 'feeder') {
                $(document).off(fieldModelData.relationModel.events.READ_FOR_SELECT_COMPLETE, this.relationDataLoadCompleteEventHandler);
                $(document).off(fieldModelData.relationModel.events.READ_FOR_SELECT_ERROR, this.relationDataLoadErrorEventHandler);
            }
            else {
                this.super('addRelationFieldEvent', [fieldModelData]);
            }
        },
    }
});
