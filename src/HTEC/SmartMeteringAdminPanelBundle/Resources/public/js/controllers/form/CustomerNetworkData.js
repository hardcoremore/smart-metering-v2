HCM.define({

    name: 'Controllers.Form.CustomerNetworkData',

    extendFrom: 'Controllers.RelationFieldForm',

    construct: function(app, module) {

        this.callConstructor('Controllers.RelationFieldForm', [app, module]);

        var self = this;

        var superFormChangedEventHandler = this.formChangedEventHandler;
        this.formChangedEventHandler = function(ev) {

            superFormChangedEventHandler(ev);

            var formFieldIinput = $(ev.currentTarget);
            var formFieldName = formFieldIinput.attr('name');

            switch(formFieldName)
            {
                case 'injectionSubstation':

                    self.getModule().resetRelationFormField(
                        self.getModule().getModel().getFieldByName('powerTransformer'),
                        self.getModule().getFormFieldByName('powerTransformer')
                    );

                    self.getModule().resetRelationFormField(
                        self.getModule().getModel().getFieldByName('powerTransformerFeeder'),
                        self.getModule().getFormFieldByName('powerTransformerFeeder')
                    );

                    self.getModule().loadInjectionSubstationPowerTransformers(self.getModule().getFormFieldValue(formFieldName, formFieldIinput));

                break;

                case 'powerTransformer':
                    self.getModule().resetRelationFormField(
                        self.getModule().getModel().getFieldByName('powerTransformerFeeder'),
                        self.getModule().getFormFieldByName('powerTransformerFeeder')
                    );
                    self.getModule().loadPowerTransformerOutputFeeders(self.getModule().getFormFieldValue(formFieldName, formFieldIinput));
                break;
            }
        };

        this.customerSelectedEventHandler = function(ev, ui) {

            self.getModule().setIsFormChanged(true);
            self.getModule().loadBusinessDistrictInjectionSubstations(ui.originalItem.businessDistrict.id);

            $(ev.target).closest('.form-field-set').next().find('input, select, textarea').focus();
        };

        this.distributionTransformerSelectedEventHandler = function(ev, ui) {

            self.getModule().setIsFormChanged(true);
            self.getModule().loadDistributionTransformerOutputFeeders(ui.originalItem.id);

            $(ev.target).closest('.form-field-set').next().find('input, select, textarea').focus();
        };
    },

    methods: {

        addEvents: function() {
            
            this.super('addEvents');

            this.getModule().getCustomerAutoComplete().autocomplete({
                select: this.customerSelectedEventHandler
            });

            this.getModule().getDistributionTransformerAutoComplete().autocomplete({
                select: this.distributionTransformerSelectedEventHandler
            });
        },

        removeEvents: function() {
            
            this.super('removeEvents');

            this.getModule().getCustomerAutoComplete().autocomplete({
                select: null
            });

            this.getModule().getDistributionTransformerAutoComplete().autocomplete({
                select: null
            });
        },

        startController: function() {

            if(this.getModule().getFormMode() === 'create') {

                this.getModule().resetRelationFormField(
                    this.getModule().getModel().getFieldByName('injectionSubstation'),
                    this.getModule().getFormFieldByName('injectionSubstation')
                );

                this.getModule().resetRelationFormField(
                    this.getModule().getModel().getFieldByName('powerTransformer'),
                    this.getModule().getFormFieldByName('powerTransformer')
                );

                this.getModule().resetRelationFormField(
                    this.getModule().getModel().getFieldByName('powerTransformerFeeder'),
                    this.getModule().getFormFieldByName('powerTransformerFeeder')
                );

                this.getModule().resetRelationFormField(
                    this.getModule().getModel().getFieldByName('distributionTransformerFeeder'),
                    this.getModule().getFormFieldByName('distributionTransformerFeeder')
                );
            }

            this.super('startController');
        },

        addRelationFieldEvent: function(fieldModelData) {

            switch(fieldModelData.name)
            {
                case 'injectionSubstation':
                case 'powerTransformer':
                case 'powerTransformerFeeder':
                case 'distributionTransformerFeeder':

                    $(document).on(
                        fieldModelData.relationModel.events.READ_FOR_SELECT_COMPLETE,
                        {fieldModelData: fieldModelData},
                        this.relationDataLoadCompleteEventHandler
                    );

                    $(document).on(
                        fieldModelData.relationModel.events.READ_FOR_SELECT_ERROR,
                        {fieldModelData: fieldModelData},
                        this.relationDataLoadErrorEventHandler
                    );

                break;

                default:
                    this.super('addRelationFieldEvent', [fieldModelData]);
                break;
            }
        },

        removeRelationFieldEvent: function(fieldModelData) {

            switch(fieldModelData.name)
            {
                case 'injectionSubstation':
                case 'powerTransformer':
                case 'powerTransformerFeeder':
                case 'distributionTransformerFeeder':
                    $(document).off(fieldModelData.relationModel.events.READ_FOR_SELECT_COMPLETE, this.relationDataLoadCompleteEventHandler);
                    $(document).off(fieldModelData.relationModel.events.READ_FOR_SELECT_ERROR, this.relationDataLoadErrorEventHandler);
                break;

                default:
                    this.super('addRelationFieldEvent', [fieldModelData]);
                break;
            }
        }
    }
});
