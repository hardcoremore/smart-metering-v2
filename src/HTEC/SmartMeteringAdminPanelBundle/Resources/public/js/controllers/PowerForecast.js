HCM.define({

    name: 'Controllers.Module.PowerForecast',

    extendFrom: 'Controllers.Crud',

    construct: function(app, module) {

        this.callConstructor('Controllers.Crud', [app, module]);

        var self = this;

        this.readFullDetailsCompleteEventHandler = function(ev) {
            self.getModule().setCurrentPowerForecastDetailsLoaded(ev.responseData);
            self.getModule().viewPowerForecastDetails(ev.responseData);
        };

        this.readFullDetailsErrorEventHandler = function(ev) {
            self.getApp().showNotification('error', 'Error occurred while reading power forecast details :(');
        };

        this.closeForecastDetailsViewEventHandler = function(ev) {
            self.getModule().closePowerForecastDetailsView();
        };
    },

    methods: {

        addEvents: function() {

            this.super('addEvents');

            var modelEvents = this.getModule().getModel().events;

            $(document).on(modelEvents.READ_FULL_DETAILS_COMPLETE, this.readFullDetailsCompleteEventHandler);
            $(document).on(modelEvents.READ_FULL_DETAILS_ERROR, this.readFullDetailsErrorEventHandler);

            this.getModule().getCloseForecastDetailsViewButton().on('click', this.closeForecastDetailsViewEventHandler);
        },

        removeEvents: function() {

            this.super('removeEvents');

            var modelEvents = this.getModule().getModel().events;

            $(document).off(modelEvents.READ_FULL_DETAILS_COMPLETE, this.readFullDetailsCompleteEventHandler);
            $(document).off(modelEvents.READ_FULL_DETAILS_ERROR, this.readFullDetailsErrorEventHandler);

            this.getModule().getCloseForecastDetailsViewButton().off('click', this.closeForecastDetailsViewEventHandler);
        },

        handleModuleControlButtonClick: function(actionType) {

            if(this.getModule().getMainViewComponent().grid('getTableState') === 'editing') {
                this.getApp().showNotification('warning', 'Please finish the grid cell editing first.');
                return;
            }

            if(actionType === 'view-record') {

                if(this.getModule().getSelectedRecord()) {

                    var currentPowerForecastDetails = this.getModule().getCurrentPowerForecastDetailsLoaded();

                    if(currentPowerForecastDetails && currentPowerForecastDetails.id === this.getModule().getSelectedRecord().id) {
                        this.getModule().viewPowerForecastDetails(currentPowerForecastDetails);
                    }
                    else {
                        this.getModule().loadPowerForecastDetails(this.getModule().getSelectedRecord().id);    
                    }
                }
                else {
                    this.getApp().showNotification('warning', 'Please select power forecast for which you want to see details');
                }
                
            }
            else {
                this.super('handleModuleControlButtonClick', [actionType]);
            }
        },
    }
});
