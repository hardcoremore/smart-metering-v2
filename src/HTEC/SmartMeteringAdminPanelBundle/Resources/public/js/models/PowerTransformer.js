HCM.define({

    name: 'Model.PowerTransformer',

    extendFrom: 'Base.Model',

    extendPrototypeWith: {
        events: HCM.getNamespaceValue('Events.Model.PowerTransformer')
    },

    construct: function(injectionSubstation, feederModel) {

        this.callConstructor('Base.Model');

        var urlPart = '/power-transformer';

        var fields = [

            {name: 'id', type: 'integer'},
            {name: 'name', type: 'string'},
            {name: 'status',  type: 'string'},
            {name: 'code',  type: 'string'},
            {
                name: 'injectionSubstation',
                type: 'relation',
                relationType: 'oneToMany',
                relationModel: injectionSubstation,
                labelPropertyName: 'name',
                valuePropertyName: 'id'
            },
            {
                name: 'outputFeeders',
                type: 'relation',
                relationType: 'manyToMany',
                relationModel: feederModel,
                relationDataLoadParams: {
                    type: 'high-voltage'
                },
                labelPropertyName: 'name',
                valuePropertyName: 'id'
            }
        ];

        var validationRules = [

            {type: 'presence',  field: 'name'},
            {type: 'presence',  field: 'status'},
            {type: 'presence',  field: 'code'},
            {type: 'presence',  field: 'injectionSubstation'},
            {type: 'presence',  field: 'outputFeeders'}
        ];

        this.getUrlPart = function() {
            return urlPart;
        };

        this.getFields = function() {
            return fields;
        };

        this.getValidationRules = function() {
            return validationRules;
        };
    }
});