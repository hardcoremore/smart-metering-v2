HCM.define({

    name: 'Model.DistributionTransformer',

    extendFrom: 'Base.Model',

    extendPrototypeWith: {
        events: HCM.getNamespaceValue('Events.Model.DistributionTransformer')
    },

    construct: function(businessDistrictModel, injectionSubstationModel, powerTransformerModel, feederModel) {

        this.callConstructor('Base.Model');

        var urlPart = '/distribution-transformer';

        var fields = [

            {name: 'id', type: 'integer'},
            {name: 'name', type: 'string'},
            {name: 'status',  type: 'string'},
            {name: 'type',  type: 'string'},
            {name: 'code',  type: 'string'},
            {
                name: 'businessDistrict',
                type: 'relation',
                relationType: 'oneToMany',
                relationModel: businessDistrictModel,
                labelPropertyName: 'name',
                valuePropertyName: 'id'
            },
            {
                name: 'injectionSubstation',
                type: 'relation',
                relationType: 'oneToMany',
                relationModel: injectionSubstationModel,
                labelPropertyName: 'name',
                valuePropertyName: 'id'
            },
            {
                name: 'powerTransformer',
                type: 'relation',
                relationType: 'oneToMany',
                relationModel: powerTransformerModel,
                labelPropertyName: 'name',
                valuePropertyName: 'id'
            },
            {
                name: 'inputFeeder',
                type: 'relation',
                relationType: 'manyToOne',
                relationModel: feederModel,
                relationDataLoadParams: {
                    type: 'high-voltage'
                },
                labelPropertyName: 'name',
                valuePropertyName: 'id'
            },
            {
                name: 'outputFeeders',
                type: 'relation',
                relationType: 'manyToMany',
                relationModel: feederModel,
                relationDataLoadParams: {
                    type: 'low-voltage'
                },
                labelPropertyName: 'name',
                valuePropertyName: 'id'
            }
        ];

        var validationRules = [

            {type: 'presence',  field: 'name'},
            {type: 'presence',  field: 'status'},
            {type: 'presence',  field: 'type'},
            {type: 'presence',  field: 'code'},
            {type: 'presence',  field: 'businessDistrict'},
            {type: 'presence',  field: 'injectionSubstation'},
            {type: 'presence',  field: 'powerTransformer'},
            {type: 'presence',  field: 'inputFeeder'},
            {type: 'presence',  field: 'outputFeeders'}
        ];

        this.getUrlPart = function() {
            return urlPart;
        };

        this.getFields = function() {
            return fields;
        };

        this.getValidationRules = function() {
            return validationRules;
        };
    }
});