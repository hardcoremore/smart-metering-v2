HCM.define({

    name: 'Model.OfflinePrepaidVendReading',

    extendFrom: 'Base.Model',

    extendPrototypeWith: {
        events: HCM.getNamespaceValue('Events.Model.OfflinePrepaidVendReading')
    },

    construct: function() {

        this.callConstructor('Base.Model');

        var urlPart = '/offline-prepaid-vend-reading';

        var fields = [];

        var validationRules = [];

        this.getUrlPart = function() {
            return urlPart;
        };

        this.getFields = function() {
            return fields;
        };

        this.getValidationRules = function() {
            return validationRules;
        };
    }
});