HCM.define({

    name: 'Model.BusinessDistrict',

    extendFrom: 'Base.Model',

    extendPrototypeWith: {
        events: HCM.getNamespaceValue('Events.Model.BusinessDistrict')
    },

    construct: function(cityModel) {

        this.callConstructor('Base.Model');

        var urlPart = '/business-district';

        var fields = [
            {
                name: 'id',
                type: 'integer'
            },
            {
                name: 'name',
                type: 'string'
            },
            {
                name: 'city',
                type: 'relation',
                relationType: 'manyToOne',
                relationModel: cityModel,
                labelPropertyName: 'name',
                valuePropertyName: 'id'
            },
            {
                name: 'manager',
                type: 'string'
            },
            {
                name: 'phoneNumber',
                type: 'string'
            },
            {
                name: 'address',
                type: 'string'
            },
            {
                name: 'code',
                type: 'string'
            }
        ];

        var validationRules = [
            {type: 'presence',  field: 'name'},
            {type: 'presence',  field: 'city'}
        ];

        this.getUrlPart = function() {
            return urlPart;
        };

        this.getFields = function() {
            return fields;
        };

        this.getValidationRules = function() {
            return validationRules;
        };
    }
});
