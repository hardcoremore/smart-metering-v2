HCM.define({

    name: 'Model.Appliance',

    extendFrom: 'Base.Model',

    arguments: ['Model.ApplianceType'],

    extendPrototypeWith: {
        events: HCM.getNamespaceValue('Events.Model.Appliance')
    },

    construct: function(applianceTypeModel) {

        this.callConstructor('Base.Model');

        var urlPart = '/appliance';

        var fields = [
            {
                name: 'id',
                type: 'integer'
            },
            {
                name: 'applianceType',
                type: 'relation',
                relationType: 'manyToOne',
                relationModel: applianceTypeModel,
                labelPropertyName: 'name',
                valuePropertyName: 'id'
            },
            {
                name: 'make',
                type: 'string'
            },
            {
                name: 'consumption',
                type: 'integer'
            }
        ];

        var validationRules = [
            {type: 'presence',  field: 'applianceType'},
            {type: 'presence',  field: 'make'},
            {type: 'presence',  field: 'consumption'}
        ];

        this.getUrlPart = function() {
            return urlPart;
        };

        this.getFields = function() {
            return fields;
        };

        this.getValidationRules = function() {
            return validationRules;
        };
    }
});