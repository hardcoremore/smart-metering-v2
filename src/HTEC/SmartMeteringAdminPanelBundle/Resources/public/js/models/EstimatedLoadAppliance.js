HCM.define({

    name: 'Model.EstimatedLoadAppliance',

    extendFrom: 'Base.Model',

    extendPrototypeWith: {
        events: HCM.getNamespaceValue('Events.Model.EstimatedLoadAppliance')
    },

    construct: function(estimatedLoadModel, applianceTypeModel, applianceModel) {

        this.callConstructor('Base.Model');

        var urlPart = '/estimated-load-appliance';
        var currentEstimatedLoad = null;
        var currentTotalLoad = 0;

        var fields = [

            {name: 'id', type: 'integer'},
            {
                name: 'estimatedLoad',
                type: 'relation',
                relationType: 'oneToMany',
                relationModel: estimatedLoadModel,
                labelPropertyName: 'name',
                valuePropertyName: 'id'
            },
            {
                name: 'applianceType',
                type: 'relation',
                relationType: 'oneToMany',
                relationModel: applianceTypeModel,
                labelPropertyName: 'name',
                valuePropertyName: 'id'
            },
            {
                name: 'appliance',
                type: 'relation',
                relationType: 'oneToMany',
                relationModel: applianceModel,
                labelPropertyName: 'make',
                valuePropertyName: 'id'
            },
            {
                name: 'applianceCount',
                type: 'integer'
            },
            {
                name: 'totalConsumption',
                type: 'integer'
            }
        ];

        var validationRules = [
            {type: 'presence',  field: 'estimatedLoad'},
            {type: 'presence',  field: 'applianceType'},
            {type: 'presence',  field: 'appliance'},
            {type: 'presence',  field: 'applianceCount'},
            {
                type: 'range', 
                field: 'applianceCount',
                min:1,
                message: "Appliance count must be 1 or greater."
            },
            {type: 'presence',  field: 'totalConsumption'}
        ];

        this.getUrlPart = function() {
            return urlPart;
        };

        this.getFields = function() {
            return fields;
        };

        this.getValidationRules = function() {
            return validationRules;
        };

        this.setCurrentEstimatedLoad = function(estimatedLoad) {

            if(currentEstimatedLoad !== estimatedLoad) {

                currentEstimatedLoad = estimatedLoad;

                var e = $.Event(this.events.ESTIMATED_LOAD_CHANGED, {estimatedLoad: currentEstimatedLoad});
                $(document).trigger(e);
            }
        };

        this.getCurrentEstimatedLoad = function() {
            return currentEstimatedLoad;
        };

        this.setCurrentTotalLoad = function(totalLoad) {
            currentTotalLoad = totalLoad;
        };

        this.getCurrentTotalLoad = function() {
            return currentTotalLoad;
        };
    },

    methods: {

        getMergedPathedRecord: function(id, recordData, patchData) {

            var data = $.extend(recordData, patchData);

            data.totalConsumption = data.applianceCount * data.applianceConsumption;

            return data;
        },
    }
});
