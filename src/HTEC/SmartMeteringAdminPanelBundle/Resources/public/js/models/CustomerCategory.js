HCM.define({

    name: 'Model.CustomerCategory',

    extendFrom: 'Base.Model',

    extendPrototypeWith: {
        events: HCM.getNamespaceValue('Events.Model.CustomerCategory')
    },

    construct: function() {

        this.callConstructor('Base.Model');

        var that = this;
        var urlPart = '/customer-category';

        var fields = [
            {
                name: 'id',
                type: 'integer'
            },
            {
                name: 'name',
                type: 'string'
            },
            {
                name: 'diversityFactor',
                type: 'decimal'
            }
        ];

        var validationRules = [
            {type: 'presence',  field: 'name'},
            {type: 'presence',  field: 'diversityFactor'}
        ];

        this.getUrlPart = function() {
            return urlPart;
        };

        this.getFields = function() {
            return fields;
        };

        this.getValidationRules = function() {
            return validationRules;
        };
    }
});
