HCM.define({

    name: 'Model.PowerSchedule',

    extendFrom: 'Base.Model',

    extendPrototypeWith: {
        events: HCM.getNamespaceValue('Events.Model.PowerSchedule')
    },

    construct: function(powerSourceModel, businessDistrictModel, injectionSubstationModel, powerTransformerModel, feederModel, distributionTransformerModel) {

        this.callConstructor('Base.Model');

        var urlPart = '/power-schedule';

        var fields = [

            {name: 'id', type: 'integer'},
            {name: 'powerType', type: 'string'},
            {
                name: 'powerSource',
                type: 'relation',
                relationType: 'oneToMany',
                relationModel: powerSourceModel,
                labelPropertyName: 'name',
                valuePropertyName: 'id'
            },
            {
                name: 'businessDistrict',
                type: 'relation',
                relationType: 'oneToMany',
                relationModel: businessDistrictModel,
                labelPropertyName: 'name',
                valuePropertyName: 'id'
            },
            {
                name: 'injectionSubstation',
                type: 'relation',
                relationType: 'oneToMany',
                relationModel: injectionSubstationModel,
                labelPropertyName: 'name',
                valuePropertyName: 'id'
            },
            {
                name: 'powerTransformer',
                type: 'relation',
                relationType: 'oneToMany',
                relationModel: powerTransformerModel,
                labelPropertyName: 'name',
                valuePropertyName: 'id'
            },
            {
                name: 'powerTransformerFeeder',
                type: 'relation',
                relationType: 'oneToMany',
                relationModel: feederModel,
                labelPropertyName: 'name',
                valuePropertyName: 'id'
            },
            {
                name: 'distributionTransformer',
                type: 'relation',
                relationType: 'oneToMany',
                relationModel: distributionTransformerModel,
                labelPropertyName: 'name',
                valuePropertyName: 'id'
            },
            {
                name: 'distributionTransformerFeeder',
                type: 'relation',
                relationType: 'oneToMany',
                relationModel: feederModel,
                labelPropertyName: 'name',
                valuePropertyName: 'id'
            },
            {name: 'date', type: 'date'},
            {name: 'startTime', type: 'time'},
            {name: 'endTime', type: 'time'},
            {name: 'status', type: 'string'}
        ];

        var validationRules = [
            {type: 'presence',  field: 'powerType'},
            {type: 'presence',  field: 'powerSource'},
            {type: 'presence',  field: 'businessDistrict'},
            {type: 'presence',  field: 'injectionSubstation'},
            {type: 'presence',  field: 'powerTransformer'},
            {type: 'presence',  field: 'powerTransformerFeeder'},
            {type: 'presence',  field: 'date'},
            {type: 'presence',  field: 'startTime'},
            {type: 'presence',  field: 'endTime'},
        ];

        this.getUrlPart = function() {
            return urlPart;
        };

        this.getFields = function() {
            return fields;
        };

        this.getValidationRules = function() {
            return validationRules;
        };
    },

    methods: {

        create: function(data, options) {

            if(data.repeatType && !data.repeatEnd) {
                $(document).trigger(this.getRepeatEndValidationErrorEvent(options.scope));
            }
            else {
                this.super('create', [data, options]);
            }
        },

        update: function(id, data, options) {

            if(data.repeatType && !data.repeatEnd) {
                $(document).trigger(this.getStartEndValidationErrorEvent(options.scope));
            }
            else {
                this.super('update', [id, data, options]);
            }
        },

        getRepeatEndValidationErrorEvent: function(scope) {

            var e = $.Event(
                this.events.VALIDATION_ERROR,
                {
                    validationErrors: { repeatEnd: ['This field is required if Repeat Type is selected.']},
                    validationErrorMessage: 'Form is invalid. Repeat End must be selected if Repeat Type is used.',
                    scope: scope
                }
            );

            return e;
        },

        getPowerScheduleTitle: function(powerSchedule) {

            var tariff = powerSchedule.tariff;
            var tariffFieldModelData = this.getFieldByName('tariff');

            var powerSource = powerSchedule.powerSource;
            var businessDistrict = powerSchedule.businessDistrict;
            var distributionTransformerFeeder = powerSchedule.distributionTransformerFeeder;
            var distributionTransformer = powerSchedule.distributionTransformer;
            var powerTransformerFeeder = powerSchedule.powerTransformerFeeder;
            var powerTransformer = powerSchedule.powerTransformer;
            var injectionSubstation = powerSchedule.injectionSubstation;


            var titleParts = [];

            if(tariff) {
                titleParts.push(tariff.name);
            }          

            if(distributionTransformerFeeder) {

                var distributionTransformerFeederTitle = distributionTransformerFeeder.name;

                if(distributionTransformerFeeder.code) {
                    distributionTransformerFeederTitle += ' (' + distributionTransformerFeeder.code + ')';
                }

                titleParts.push(distributionTransformerFeederTitle);
            }

            if(distributionTransformer) {
               titleParts.push(distributionTransformer.name);
            }

            if(powerTransformerFeeder) {

                var powerTransformerFeederTitle = powerTransformerFeeder.name;

                if(powerTransformerFeeder.code) {
                    powerTransformerFeederTitle += ' (' + powerTransformerFeeder.code + ')';
                }

                titleParts.push(powerTransformerFeederTitle);
            }

            if(powerTransformer) {
                titleParts.push(powerTransformer.name);
            }

            if(injectionSubstation) {
                titleParts.push(injectionSubstation.name);
            }

            if(businessDistrict) {
                titleParts.push(businessDistrict.name);
            }

            if(powerSource) {
                titleParts.push(powerSource.name);
            }

            if(powerSchedule.status === 'approved') {
                titleParts.push('Approved');
            }
            else {
                titleParts.push('Waiting approval');
            }

            return titleParts.join(' - ');
        }
    }
});
