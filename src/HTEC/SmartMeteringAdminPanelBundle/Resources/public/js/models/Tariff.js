HCM.define({

    name: 'Model.Tariff',

    extendFrom: 'Base.Model',

    extendPrototypeWith: {
        events: HCM.getNamespaceValue('Events.Model.Tariff')
    },

    construct: function() {

        this.callConstructor('Base.Model');

        var urlPart = '/tariff';

        var fields = [
            {
                name: 'id',
                type: 'integer'
            },
            {
                name: 'name',
                type: 'string'
            },
            {
                name: 'code',
                type: 'string'
            },
            {
                name: 'price',
                type: 'decimal'
            },
            {
                name: 'description',
                type: 'string'
            },
            {
                name: 'averageConsumptionPerDay',
                type: 'integer'
            }
        ];

        var validationRules = [
            {type: 'presence',  field: 'name'},
            {type: 'presence',  field: 'code'},
            {type: 'presence',  field: 'price'}
        ];
        
        this.getUrlPart = function() {
            return urlPart;
        };

        this.getFields = function() {
            return fields;
        };

        this.getValidationRules = function() {
            return validationRules;
        };
    }
});
