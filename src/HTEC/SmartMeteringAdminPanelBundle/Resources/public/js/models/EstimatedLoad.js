HCM.define({

    name: 'Model.EstimatedLoad',

    extendFrom: 'Base.Model',

    extendPrototypeWith: {
        events: HCM.getNamespaceValue('Events.Model.EstimatedLoad')
    },

    construct: function(customerModel, customerCategoryModel) {

        this.callConstructor('Base.Model');

        var urlPart = '/estimated-load';

        var fields = [

            {name: 'id', type: 'integer'},
            {
                name: 'customer',
                type: 'relation',
                relationType: 'oneToMany',
                relationModel: customerModel,
                labelPropertyName: 'name',
                valuePropertyName: 'id'
            },
            {
                name: 'customerCategory',
                type: 'relation',
                relationType: 'oneToMany',
                relationModel: customerCategoryModel,
                labelPropertyName: 'name',
                valuePropertyName: 'id'
            }
        ];

        var validationRules = [
            {type: 'presence',  field: 'customer'},
            {type: 'presence',  field: 'customerCategory'}
        ];

        this.getUrlPart = function() {
            return urlPart;
        };

        this.getFields = function() {
            return fields;
        };

        this.getValidationRules = function() {
            return validationRules;
        };
    }
});
