HCM.define({

    name: 'Model.Feeder',

    extendFrom: 'Base.Model',

    extendPrototypeWith: {
        events: HCM.getNamespaceValue('Events.Model.Feeder')
    },

    construct: function() {

        this.callConstructor('Base.Model');

        var that = this;
        var urlPart = '/feeder';

        var fields = [
            {
                name: 'id',
                type: 'integer'
            },
            {
                name: 'name',
                type: 'string'
            },
            {
                name: 'type',
                type: 'string'
            },
            {
                name: 'code',
                type: 'string'
            },
            {
                name: 'formation',
                type: 'string'
            },
            {
                name: 'length',
                type: 'integer'
            },
            {
                name: 'streetCoverage',
                type: 'array'
            },
            {
                name: 'kilovolts',
                type: 'integer'
            }
        ];

        var validationRules = [
            {type: 'presence',  field: 'name'},
            {type: 'presence',  field: 'type'},
            {type: 'presence',  field: 'code'}
        ];

        this.getUrlPart = function() {
            return urlPart;
        };

        this.getFields = function() {
            return fields;
        };

        this.getValidationRules = function() {
            return validationRules;
        };
    }
});
