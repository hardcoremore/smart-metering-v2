HCM.define({

    name: 'Model.PowerPurchase',

    extendFrom: 'Base.Model',

    extendPrototypeWith: {
        events: HCM.getNamespaceValue('Events.Model.PowerPurchase')
    },

    construct: function(powerSourceModel) {

        this.callConstructor('Base.Model');

        var that = this;
        var urlPart = '/power-purchase';


        this.POWER_TYPE_NATIONAL = 'national';
        this.POWER_TYPE_CAPTIVE = 'captive';
        this.POWER_TYPE_EMBEDDED = 'embedded';

        var fields = [
            {
                name: 'id',
                type: 'integer'
            },
            {
                name: 'powerType',
                type: 'string',
                availableOptions: [this.POWER_TYPE_NATIONAL, this.POWER_TYPE_CAPTIVE, this.POWER_TYPE_EMBEDDED]
            },
            {
                name: 'powerSource',
                type: 'relation',
                relationType: 'manyToOne',
                relationModel: powerSourceModel,
                labelPropertyName: 'name',
                valuePropertyName: 'id'
            },
            {
                name: 'amountPurchased',
                type: 'integer'
            },
            {
                name: 'datePurchased',
                type: 'date'
            },
            {name: 'status', type: 'string'}
        ];

        var validationRules = [
            {type: 'presence',  field: 'powerType'},
            {type: 'presence',  field: 'powerSource'},
            {type: 'presence',  field: 'amountPurchased'},
            {type: 'presence',  field: 'datePurchased'}
        ];

        this.getUrlPart = function() {
            return urlPart;
        };

        this.getFields = function() {
            return fields;
        };

        this.getValidationRules = function() {
            return validationRules;
        };
    },

    methods: {

        approvePowerPurchase: function(powerPurchaseId, options) {

            var that = this;
            
            $.extend(options, true, {

                url: this.getConfig().getBaseAPIUrl() + this.getUrl() + '/' + powerPurchaseId + '/approve',

                success: function(data, textStatus, jqXHR){
                    that.dispatchSuccess(that.getEvents().APPROVE_COMPLETE, data, options, jqXHR);
                },

                error: function(error, jqXHR, textStatus) {
                    that.dispatchError(that.getEvents().APPROVE_ERROR, error, options, jqXHR);
                }
            });

            this.getRequester().PATCH(options);
        },

        readAllForToday: function(options) {

            var self = this;

            $.extend(options, true, {

                url: this.getUrl('/list-all-for-today'),

                success: function(responseData, textStatus, jqXHR) {
                    self.dispatchSuccess(self.events.READ_ALL_FOR_TODAY_COMPLETE, responseData, options, jqXHR);
                },

                error: function(error, jqXHR, textStatus) {
                    self.dispatchError(self.events.READ_ALL_FOR_TODAY_ERROR, error, options, jqXHR);
                }
            });

            this.getRequester().GET(options);
        }
    }
});
