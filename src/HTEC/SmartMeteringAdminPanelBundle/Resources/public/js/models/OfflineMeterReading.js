HCM.define({

    name: 'Model.OfflineMeterReading',

    extendFrom: 'Base.Model',

    extendPrototypeWith: {
        events: HCM.getNamespaceValue('Events.Model.OfflineMeterReading')
    },

    construct: function() {

        this.callConstructor('Base.Model');

        var urlPart = '/offline-meter-reading';

        var fields = [
            { name: 'suspect', type: 'string'}
        ];

        var validationRules = [];

        this.getUrlPart = function() {
            return urlPart;
        };

        this.getFields = function() {
            return fields;
        };

        this.getValidationRules = function() {
            return validationRules;
        };
    }
});