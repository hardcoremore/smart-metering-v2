HCM.define({

    name: 'Model.EstimatedConsumption',

    extendFrom: 'Base.Model',

    extendPrototypeWith: {
        events: HCM.getNamespaceValue('Events.Model.EstimatedConsumption')
    },

    construct: function(customerModel) {

        this.callConstructor('Base.Model');

        var urlPart = '/estimated-consumption';

        var fields = [

            {name: 'id', type: 'integer'},
            {
                name: 'customer',
                type: 'relation',
                relationType: 'oneToMany',
                relationModel: customerModel,
                labelPropertyName: 'name',
                valuePropertyName: 'id'
            },
            {name: 'start', type: 'string'},
            {name: 'end', type: 'string'}
        ];

        var validationRules = [

            {type: 'presence',  field: 'customer'},
            {type: 'presence',  field: 'start'},
            {type: 'presence',  field: 'end'}
        ];

        this.getUrlPart = function() {
            return urlPart;
        };

        this.getFields = function() {
            return fields;
        };

        this.getValidationRules = function() {
            return validationRules;
        };
    },

    methods: {

        create: function(data, options) {

            if(moment(data.start).unix() > moment(data.end).unix()) {
                $(document).trigger(this.getStartEndValidationErrorEvent(options.scope));
            }
            else {
                this.super('create', [data, options]);
            }
        },

        update: function(id, data, options) {

            if(moment(data.start).unix() > moment(data.end).unix()) {
                $(document).trigger(this.getStartEndValidationErrorEvent(options.scope));
            }
            else {
                this.super('update', [id, data, options]);
            }
        },

        getStartEndValidationErrorEvent: function(scope) {

            var e = $.Event(
                this.events.VALIDATION_ERROR,
                {
                    validationErrors: { start: ['This value is invalid.']},
                    validationErrorMessage: 'Start date cannot be greater than End date',
                    scope: scope
                }
            );

            return e;
        }
    }
});