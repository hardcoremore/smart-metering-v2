HCM.define({

    name: 'Model.PowerAllocate',

    extendFrom: 'Base.Model',

    extendPrototypeWith: {
        events: HCM.getNamespaceValue('Events.Model.PowerAllocate')
    },

    construct: function(powerForecastModel) {

        this.callConstructor('Base.Model');

        var urlPart = '/power-allocate';

        var fields = [

            {name: 'id', type: 'integer'},
            {
                name: 'powerForecast',
                type: 'relation',
                relationType: 'oneToMany',
                relationModel: powerForecastModel,
                labelPropertyName: function(recordData) {

                    var label = recordData.powerType +  ' - ' + moment(recordData.createdDatetime).format('YYYY-MM-DD');
                        label += ' - ' + recordData.amountToForecast;
                        label += ' MW';

                    return label;
                },
                valuePropertyName: 'id'
            },
            {name: 'amountToAllocate', type: 'integer'}
        ];

        var validationRules = [
            {type: 'presence',  field: 'powerForecast'}
        ];

        this.getUrlPart = function() {
            return urlPart;
        };

        this.getFields = function() {
            return fields;
        };

        this.getValidationRules = function() {
            return validationRules;
        };
    }
});