HCM.define({

    name: 'Model.FeederLoad',

    extendFrom: 'Base.Model',

    extendPrototypeWith: {
        events: HCM.getNamespaceValue('Events.Model.FeederLoad')
    },

    construct: function(businessDistrictModel, injectionSubstationModel, powerTransformerModel, feederModel) {

        this.callConstructor('Base.Model');

        var urlPart = '/feeder-load';

        var fields = [

            {name: 'id', type: 'integer'},
            {
                name: 'businessDistrict',
                type: 'relation',
                relationType: 'oneToMany',
                relationModel: businessDistrictModel,
                labelPropertyName: 'name',
                valuePropertyName: 'id'
            },
            {
                name: 'injectionSubstation',
                type: 'relation',
                relationType: 'oneToMany',
                relationModel: injectionSubstationModel,
                labelPropertyName: 'name',
                valuePropertyName: 'id'
            },
            {
                name: 'powerTransformer',
                type: 'relation',
                relationType: 'oneToMany',
                relationModel: powerTransformerModel,
                labelPropertyName: 'name',
                valuePropertyName: 'id'
            },
            {
                name: 'powerTransformerFeeder',
                type: 'relation',
                relationType: 'oneToMany',
                relationModel: feederModel,
                labelPropertyName: 'name',
                valuePropertyName: 'id'
            },
            {name: 'date', type: 'date'},
            {name: 'time', type: 'time'},
            {name: 'feederLoad', type: 'integer'}
        ];

        var validationRules = [
            {type: 'presence',  field: 'powerTransformerFeeder'},
            {type: 'presence',  field: 'date'},
            {type: 'presence',  field: 'time'},
            {type: 'presence',  field: 'feederLoad'}
        ];

        this.getUrlPart = function() {
            return urlPart;
        };

        this.getFields = function() {
            return fields;
        };

        this.getValidationRules = function() {
            return validationRules;
        };
    }
});