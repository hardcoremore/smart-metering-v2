HCM.define({

    name: 'Model.ApplianceType',

    extendFrom: 'Base.Model',

    arguments: ['Model.CustomerCategory'],

    extendPrototypeWith: {
        events: HCM.getNamespaceValue('Events.Model.ApplianceType')
    },

    construct: function(cutomerCategoryModel) {

        this.callConstructor('Base.Model');

        var that = this;
        var urlPart = '/appliance-type';

        var fields = [
            {
                name: 'id',
                type: 'integer'
            },
            {
                name: 'name',
                type: 'string'
            },
            {
                name: 'customerCategories',
                type: 'relation',
                relationType: 'manyToMany',
                relationModel: cutomerCategoryModel,
                labelPropertyName: 'name',
                valuePropertyName: 'id'
            }
        ];

        var validationRules = [
            {type: 'presence',  field: 'name'},
            {type: 'presence',  field: 'customerCategories'}
        ];

        this.getUrlPart = function() {
            return urlPart;
        };

        this.getFields = function() {
            return fields;
        };

        this.getValidationRules = function() {
            return validationRules;
        };
    }
});