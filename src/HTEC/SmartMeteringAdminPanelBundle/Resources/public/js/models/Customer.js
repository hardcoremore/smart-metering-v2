HCM.define({

    name: 'Model.Customer',

    extendFrom: 'Base.Model',

    extendPrototypeWith: {
        events: HCM.getNamespaceValue('Events.Model.Customer')
    },

    construct: function(tariffModel, businessDistrictModel) {

        this.callConstructor('Base.Model');

        var urlPart = '/customer';

        var fields = [

            {name: 'id', type: 'integer'},
            {name: 'name', type: 'string'},
            {name: 'accountNumber',  type: 'string'},
            {name: 'accountType',  type: 'string'},
            {name: 'address',  type: 'string'},
            {
                name: 'tariff',
                type: 'relation',
                relationType: 'oneToMany',
                relationModel: tariffModel,
                labelPropertyName: 'name',
                valuePropertyName: 'id'
            },
            {
                name: 'businessDistrict',
                type: 'relation',
                relationType: 'oneToMany',
                relationModel: businessDistrictModel,
                labelPropertyName: 'name',
                valuePropertyName: 'id'
            }
        ];

        var validationRules = [

            {type: 'presence',  field: 'name'},
            {type: 'presence',  field: 'accountNumber'},
            {type: 'presence',  field: 'accountType'},
            {type: 'presence',  field: 'tariff'},
            {type: 'presence',  field: 'businessDistrict'},
            {type: 'presence',  field: 'address'}
        ];

        this.getUrlPart = function() {
            return urlPart;
        };

        this.getFields = function() {
            return fields;
        };

        this.getValidationRules = function() {
            return validationRules;
        };
    }
});