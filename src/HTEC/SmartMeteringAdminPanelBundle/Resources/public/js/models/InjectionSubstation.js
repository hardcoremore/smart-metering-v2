HCM.define({

    name: 'Model.InjectionSubstation',

    extendFrom: 'Base.Model',

    extendPrototypeWith: {
        events: HCM.getNamespaceValue('Events.Model.InjectionSubstation')
    },

    construct: function(businessDistrictModel, powerSourceModel, feederModel) {

        this.callConstructor('Base.Model');

        var urlPart = '/injection-substation';

        var fields = [

            {name: 'id', type: 'integer'},
            {name: 'name', type: 'string'},
            {name: 'address',  type: 'string'},
            {name: 'code',  type: 'string'},
            {
                name: 'businessDistrict',
                type: 'relation',
                relationType: 'oneToMany',
                relationModel: businessDistrictModel,
                labelPropertyName: 'name',
                valuePropertyName: 'id'
            },
            {
                name: 'powerSource',
                type: 'relation',
                relationType: 'oneToMany',
                relationModel: powerSourceModel,
                labelPropertyName: 'name',
                valuePropertyName: 'id'
            },
            {
                name: 'inputFeeders',
                type: 'relation',
                relationType: 'manyToMany',
                relationModel: feederModel,
                relationDataLoadParams: {
                    type: 'high-voltage'
                },
                labelPropertyName: 'name',
                valuePropertyName: 'id'
            }
        ];

        var validationRules = [

            {type: 'presence',  field: 'name'},
            {type: 'presence',  field: 'address'},
            {type: 'presence',  field: 'code'},
            {type: 'presence',  field: 'businessDistrict'},
            {type: 'presence',  field: 'powerSource'}
        ];

        this.getUrlPart = function() {
            return urlPart;
        };

        this.getFields = function() {
            return fields;
        };

        this.getValidationRules = function() {
            return validationRules;
        };
    }
});