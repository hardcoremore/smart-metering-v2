HCM.define({

    name: 'Model.CustomerNetworkData',

    extendFrom: 'Base.Model',

    extendPrototypeWith: {
        events: HCM.getNamespaceValue('Events.Model.CustomerNetworkData')
    },

    construct: function(customerModel, injectionSubstationModel, powerTransformerModel, feederModel, electricPoleModel, distributionTransformerModel) {

        this.callConstructor('Base.Model');

        var urlPart = '/customer-network-data';

        var fields = [

            {name: 'id', type: 'integer'},
            {
                name: 'customer',
                type: 'relation',
                relationType: 'oneToMany',
                relationModel: customerModel,
                labelPropertyName: 'name',
                valuePropertyName: 'id'
            },
            {
                name: 'injectionSubstation',
                type: 'relation',
                relationType: 'oneToMany',
                relationModel: injectionSubstationModel,
                labelPropertyName: 'name',
                valuePropertyName: 'id'
            },
            {
                name: 'powerTransformer',
                type: 'relation',
                relationType: 'oneToMany',
                relationModel: powerTransformerModel,
                labelPropertyName: 'name',
                valuePropertyName: 'id'
            },
            {
                name: 'powerTransformerFeeder',
                type: 'relation',
                relationType: 'oneToMany',
                relationModel: feederModel,
                labelPropertyName: 'name',
                valuePropertyName: 'id'
            },
            {
                name: 'highTensionPole',
                type: 'relation',
                relationType: 'oneToMany',
                relationModel: electricPoleModel,
                labelPropertyName: 'code',
                valuePropertyName: 'id'
            },
            {
                name: 'distributionTransformer',
                type: 'relation',
                relationType: 'oneToMany',
                relationModel: distributionTransformerModel,
                labelPropertyName: 'name',
                valuePropertyName: 'id'
            },
            {
                name: 'distributionTransformerFeeder',
                type: 'relation',
                relationType: 'oneToMany',
                relationModel: feederModel,
                labelPropertyName: 'name',
                valuePropertyName: 'id'
            },
            {
                name: 'lowTensionPole',
                type: 'relation',
                relationType: 'oneToMany',
                relationModel: electricPoleModel,
                labelPropertyName: 'code',
                valuePropertyName: 'id'
            }
        ];

        var validationRules = [

            {type: 'presence',  field: 'customer'},
            {type: 'presence',  field: 'injectionSubstation'},
            {type: 'presence',  field: 'powerTransformer'},
            {type: 'presence',  field: 'powerTransformerFeeder'}
        ];

        this.getUrlPart = function() {
            return urlPart;
        };

        this.getFields = function() {
            return fields;
        };

        this.getValidationRules = function() {
            return validationRules;
        };
    }
});