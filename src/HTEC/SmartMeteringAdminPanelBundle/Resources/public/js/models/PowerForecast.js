HCM.define({

    name: 'Model.PowerForecast',

    extendFrom: 'Base.Model',

    extendPrototypeWith: {
        events: HCM.getNamespaceValue('Events.Model.PowerForecast')
    },

    construct: function(powerPurchaseModel) {

        this.callConstructor('Base.Model');

        var urlPart = '/power-forecast';

        var fields = [

            {name: 'id', type: 'integer'},
            {
                name: 'powerPurchase',
                type: 'relation',
                relationType: 'oneToMany',
                relationModel: powerPurchaseModel,
                relationDataLoadParams: {
                    status: 'approved'
                },
                labelPropertyName: function(recordData) {

                    var label = recordData.powerSource.name + ' - ';
                        label += recordData.powerType +  ' - ' + moment(recordData.datePurchased).format('YYYY-MM-DD');
                        label += ' - ' + recordData.amountPurchased;
                        label += ' MW';

                    return label;
                },
                valuePropertyName: 'id'
            },
            {name: 'amountToForecast', type: 'integer'}
        ];

        var validationRules = [

            {type: 'presence',  field: 'powerPurchase'},
            {type: 'presence',  field: 'amountToForecast'}
        ];

        this.getUrlPart = function() {
            return urlPart;
        };

        this.getFields = function() {
            return fields;
        };

        this.getValidationRules = function() {
            return validationRules;
        };
    },

    methods: {

        readPowerForecastFullDetails: function(powerForecastId, options) {

            var self = this;

            $.extend(options, true, {

                url: this.getUrl('/' + powerForecastId + '/full-details'),

                success: function(responseData, textStatus, jqXHR) {
                    self.dispatchSuccess(self.events.READ_FULL_DETAILS_COMPLETE, responseData, options, jqXHR);
                },

                error: function(error, jqXHR, textStatus) {
                    self.dispatchError(self.events.READ_FULL_DETAILS_ERROR, error, options, jqXHR);
                }
            });

            this.getRequester().GET(options);
        },

        readAllForTodayWithDetails: function(options) {

            var self = this;

            $.extend(options, true, {

                url: this.getUrl('/list-all-for-today-with-details'),

                success: function(responseData, textStatus, jqXHR) {
                    self.dispatchSuccess(self.events.READ_ALL_FOR_TODAY_WITH_DETAILS_COMPLETE, responseData, options, jqXHR);
                },

                error: function(error, jqXHR, textStatus) {
                    self.dispatchError(self.events.READ_ALL_FOR_TODAY_WITH_DETAILS_ERROR, error, options, jqXHR);
                }
            });

            this.getRequester().GET(options);
        },

        extractDistinctTariffCodes: function(data) {
                        
            var tarriffCodes = [];

            for(var i in data) {

                if(tarriffCodes.indexOf(data[i].tariff.code) === -1) {
                    tarriffCodes.push(data[i].tariff.code);
                }
            }

            return tarriffCodes;
        },

        groupByBusinessDistrict: function(data) {

            var groupedData = [];

            var groupData = null;
            var currentBusinessDistrict = null;

            for(var i in data) {

                if(currentBusinessDistrict && currentBusinessDistrict.id !== data[i].businessDistrict.id) {

                    groupedData.push({
                        businessDistrict: currentBusinessDistrict,
                        data: groupData
                    });

                    groupData = [data[i]];
                    currentBusinessDistrict = data[i].businessDistrict;
                }
                else
                {
                    if(groupData === null) {
                        groupData = [data[i]];
                    }
                    else {
                        groupData.push(data[i]);
                    }

                    currentBusinessDistrict = data[i].businessDistrict;
                }
            }            

            // adding the last record into collection
            groupedData.push({
                businessDistrict: currentBusinessDistrict,
                data: groupData
            });

            return groupedData;
        }
    }
});