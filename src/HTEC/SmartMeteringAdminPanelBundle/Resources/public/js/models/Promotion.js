HCM.define({

    name: 'Model.Promotion',

    extendFrom: 'Base.Model',

    extendPrototypeWith: {
        events: HCM.getNamespaceValue('Events.Model.Promotion')
    },

    construct: function() {

        this.callConstructor('Base.Model');

        var urlPart = '/promotion';

        var fields = [];

        var validationRules = [];

        this.getUrlPart = function() {
            return urlPart;
        };

        this.getFields = function() {
            return fields;
        };

        this.getValidationRules = function() {
            return validationRules;
        };
    }
});