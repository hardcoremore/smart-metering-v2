HCM.define({

    name: 'Model.PowerSource',

    extendFrom: 'Base.Model',

    extendPrototypeWith: {
        events: HCM.getNamespaceValue('Events.Model.PowerSource')
    },

    construct: function(feederModel) {

        this.callConstructor('Base.Model');

        var urlPart = '/power-source';

        var fields = [

            {name: 'id',        type: 'integer'},
            {name: 'name', type: 'string'},
            {name: 'capacity',  type: 'string'},
            {name: 'address',  type: 'string'},
            {
                name: 'outputFeeders',
                type: 'relation',
                relationType: 'manyToMany',
                relationModel: feederModel,
                labelPropertyName: 'name',
                valuePropertyName: 'id'
            }
        ];

        var validationRules = [

            {type: 'presence',  field: 'name'},
            {type: 'presence',  field: 'capacity'},
            {type: 'format',    field: 'address'},
            {type: 'format',    field: 'outputFeeders'}
        ];

        this.getUrlPart = function() {
            return urlPart;
        };

        this.getFields = function() {
            return fields;
        };

        this.getValidationRules = function() {
            return validationRules;
        };
    }
});