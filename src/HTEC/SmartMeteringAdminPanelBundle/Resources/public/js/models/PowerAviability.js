HCM.define({

    name: 'Model.PowerAviability',

    extendFrom: 'Base.Model',

    extendPrototypeWith: {
        events: HCM.getNamespaceValue('Events.Model.PowerAviability')
    },

    construct: function(businessDistrictModel, injectionSubstationModel, powerTransformerModel, feederModel) {

        this.callConstructor('Base.Model');

        var urlPart = '/power-aviability';

        var fields = [

            {name: 'id', type: 'integer'},
            {
                name: 'businessDistrict',
                type: 'relation',
                relationType: 'oneToMany',
                relationModel: businessDistrictModel,
                labelPropertyName: 'name',
                valuePropertyName: 'id'
            },
            {
                name: 'injectionSubstation',
                type: 'relation',
                relationType: 'oneToMany',
                relationModel: injectionSubstationModel,
                labelPropertyName: 'name',
                valuePropertyName: 'id'
            },
            {
                name: 'powerTransformer',
                type: 'relation',
                relationType: 'oneToMany',
                relationModel: powerTransformerModel,
                labelPropertyName: 'name',
                valuePropertyName: 'id'
            },
            {
                name: 'powerTransformerFeeder',
                type: 'relation',
                relationType: 'oneToMany',
                relationModel: feederModel,
                labelPropertyName: 'name',
                valuePropertyName: 'id'
            }
        ];

        var validationRules = [];

        this.getUrlPart = function() {
            return urlPart;
        };

        this.getFields = function() {
            return fields;
        };

        this.getValidationRules = function() {
            return validationRules;
        };
    },

    methods: {

        getPowerAviabilityTitle: function(record) {

            var businessDistrict = record.businessDistrict;
            var powerTransformerFeeder = record.powerTransformerFeeder;
            var powerTransformer = record.powerTransformer;
            var injectionSubstation = record.injectionSubstation;

            var titleParts = [];


            if(powerTransformerFeeder) {

                var powerTransformerFeederTitle = powerTransformerFeeder.name;

                if(powerTransformerFeeder.code) {
                    powerTransformerFeederTitle += ' (' + powerTransformerFeeder.code + ')';
                }

                titleParts.push(powerTransformerFeederTitle);
            }

            if(powerTransformer) {
                titleParts.push(powerTransformer.name);
            }

            if(injectionSubstation) {
                titleParts.push(injectionSubstation.name);
            }

            if(businessDistrict) {
                titleParts.push(businessDistrict.name);
            }

            if(record.powerDeactivate) {
                record.status = 'offline';
            }
            else {
                record.status = 'online';
            }

            return titleParts.join(' - ');
        }
    }
});