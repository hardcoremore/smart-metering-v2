HCM.define({

    name: 'Model.Meter',

    extendFrom: 'Base.Model',

    extendPrototypeWith: {
        events: HCM.getNamespaceValue('Events.Model.Meter')
    },

    construct: function(customerModel, distributionTransformerModel, feederModel) {

        this.callConstructor('Base.Model');

        var urlPart = '/meter';

        var fields = [

            {name: 'id', type: 'integer'},
            {name: 'type',  type: 'string'},
            {name: 'status',  type: 'string'},
            {
                name: 'customer',
                type: 'relation',
                relationType: 'oneToMany',
                relationModel: customerModel,
                labelPropertyName: 'name',
                valuePropertyName: 'id'
            },
            {
                name: 'distributionTransformer',
                type: 'relation',
                relationType: 'oneToMany',
                relationModel: distributionTransformerModel,
                labelPropertyName: 'name',
                valuePropertyName: 'id'
            },
            {
                name: 'feeder',
                type: 'relation',
                relationType: 'oneToMany',
                relationModel: feederModel,
                labelPropertyName: 'name',
                valuePropertyName: 'id'
            },
            {name: 'serialNumber',  type: 'string'}
        ];

        var validationRules = [

            {type: 'presence',  field: 'customer'},
            {type: 'presence',  field: 'type'},
            {type: 'presence',  field: 'status'},
            {type: 'presence',  field: 'distributionTransformer'},
            {type: 'presence',  field: 'feeder'}
        ];

        this.getUrlPart = function() {
            return urlPart;
        };

        this.getFields = function() {
            return fields;
        };

        this.getValidationRules = function() {
            return validationRules;
        };
    }
});