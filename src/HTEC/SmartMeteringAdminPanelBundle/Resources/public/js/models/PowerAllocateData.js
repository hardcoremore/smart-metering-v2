HCM.define({

    name: 'Model.PowerAllocateData',

    extendFrom: 'Base.Model',

    extendPrototypeWith: {
        events: HCM.getNamespaceValue('Events.Model.PowerAllocateData')
    },

    construct: function(powerAllocateModel, businessDistrictModel) {

        this.callConstructor('Base.Model');

        var urlPart = '/power-allocate-data';

        var fields = [

            {name: 'id', type: 'integer'},
            {
                name: 'powerAllocate',
                type: 'relation',
                relationType: 'oneToMany',
                relationModel: powerAllocateModel,
                labelPropertyName: function(recordData) {

                    var label = recordData.powerForecast.powerType +  ' - ' + moment(recordData.createdDatetime).format('YYYY-MM-DD');
                        label += ' - ' + recordData.totalPowerAllocated;
                        label += ' MW';

                    return label;
                },
                valuePropertyName: 'id'
            },
            {
                name: 'businessDistrict',
                type: 'relation',
                relationType: 'oneToMany',
                relationModel: businessDistrictModel,
                labelPropertyName: 'name',
                valuePropertyName: 'id'
            },
            {name: 'powerAllocated',  type: 'float'},
            {name: 'powerForecasted',  type: 'float'},
            {name: 'powerOverride',  type: 'float'}
        ];

        var validationRules = [
            {type: 'presence',  field: 'powerAllocate'},
            {type: 'presence',  field: 'businessDistrict'},
            {type: 'presence',  field: 'powerAllocated'},
            {type: 'presence',  field: 'powerForecasted'}
        ];

        this.getUrlPart = function() {
            return urlPart;
        };

        this.getFields = function() {
            return fields;
        };

        this.getValidationRules = function() {
            return validationRules;
        };
    }
});