HCM.define({

    name: 'Model.PowerActivateDeactivate',

    extendFrom: 'Base.Model',

    extendPrototypeWith: {
        events: HCM.getNamespaceValue('Events.Model.PowerActivateDeactivate')
    },

    construct: function(businessDistrictModel, injectionSubstationModel, powerTransformerModel, feederModel) {

        this.callConstructor('Base.Model');

        var urlPart = '/power-activate-deactivate';

        var fields = [

            {name: 'id', type: 'integer'},
            {
                name: 'businessDistrict',
                type: 'relation',
                relationType: 'oneToMany',
                relationModel: businessDistrictModel,
                labelPropertyName: 'name',
                valuePropertyName: 'id'
            },
            {
                name: 'injectionSubstation',
                type: 'relation',
                relationType: 'oneToMany',
                relationModel: injectionSubstationModel,
                labelPropertyName: 'name',
                valuePropertyName: 'id'
            },
            {
                name: 'powerTransformer',
                type: 'relation',
                relationType: 'oneToMany',
                relationModel: powerTransformerModel,
                labelPropertyName: 'name',
                valuePropertyName: 'id'
            },
            {
                name: 'powerTransformerFeeder',
                type: 'relation',
                relationType: 'oneToMany',
                relationModel: feederModel,
                labelPropertyName: 'name',
                valuePropertyName: 'id'
            },
            {name: 'type', type: 'string'},
            {name: 'date', type: 'date'},
            {name: 'time', type: 'time'}
        ];

        var validationRules = [
            {type: 'presence',  field: 'type'},
            {type: 'presence',  field: 'businessDistrict'},
            {type: 'presence',  field: 'injectionSubstation'},
            {type: 'presence',  field: 'powerTransformer'},
            {type: 'presence',  field: 'powerTransformerFeeder'}
        ];

        this.getUrlPart = function() {
            return urlPart;
        };

        this.getFields = function() {
            return fields;
        };

        this.getValidationRules = function() {
            return validationRules;
        };
    }
});