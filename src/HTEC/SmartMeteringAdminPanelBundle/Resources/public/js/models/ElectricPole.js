HCM.define({

    name: 'Model.ElectricPole',

    extendFrom: 'Base.Model',

    extendPrototypeWith: {
        events: HCM.getNamespaceValue('Events.Model.ElectricPole')
    },

    construct: function(feederModel) {

        this.callConstructor('Base.Model');

        var that = this;
        var urlPart = '/electric-pole';

        var fields = [
            {
                name: 'id',
                type: 'integer'
            },
            {
                name: 'type',
                type: 'string'
            },
            {
                name: 'code',
                type: 'string'
            },
            {
                name: 'status',
                type: 'string'
            },
            {
                name: 'address',
                type: 'string'
            },
            {
                name: 'feeder',
                type: 'relation',
                relationType: 'oneToMany',
                relationModel: feederModel,
                labelPropertyName: 'name',
                valuePropertyName: 'id'
            }
        ];

        var validationRules = [
            {type: 'presence',  field: 'type'},
            {type: 'presence',  field: 'code'},
            {type: 'presence',  field: 'status'}
        ];

        this.getUrlPart = function() {
            return urlPart;
        };

        this.getFields = function() {
            return fields;
        };

        this.getValidationRules = function() {
            return validationRules;
        };
    }
});
