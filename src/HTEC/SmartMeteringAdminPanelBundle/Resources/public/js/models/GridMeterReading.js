HCM.define({

    name: 'Model.GridMeterReading',

    extendFrom: 'Base.Model',

    extendPrototypeWith: {
        events: HCM.getNamespaceValue('Events.Model.GridMeterReading')
    },

    construct: function() {

        this.callConstructor('Base.Model');

        var urlPart = '/grid-meter-reading';

        var fields = [];

        var validationRules = [];

        this.getUrlPart = function() {
            return urlPart;
        };

        this.getFields = function() {
            return fields;
        };

        this.getValidationRules = function() {
            return validationRules;
        };
    }
});