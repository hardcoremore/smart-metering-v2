HCM.define({

    name: 'Model.OnlineMeterReading',

    extendFrom: 'Base.Model',

    extendPrototypeWith: {
        events: HCM.getNamespaceValue('Events.Model.OnlineMeterReading')
    },

    construct: function() {

        this.callConstructor('Base.Model');

        var urlPart = '/online-meter-reading';

        var fields = [];

        var validationRules = [];

        this.getUrlPart = function() {
            return urlPart;
        };

        this.getFields = function() {
            return fields;
        };

        this.getValidationRules = function() {
            return validationRules;
        };
    }
});