HCM.define({

    name: 'Model.AdminPanelUser',

    extendFrom: 'Model.User',

    extendPrototypeWith: {
        events: HCM.getNamespaceValue('Events.Model.AdminPanelUser')
    },

    construct: function(roleModel) {

        this.callConstructor('Model.User');

        this.USERNAME_LOGIN_POST_PROPERTY_NAME = '__smart_metering_admin_api_login_username__';
        this.PASSWORD_LOGIN_POST_PROPERTY_NAME = '__smart_metering_admin_api_login_password__';

        this.STATUS_ACTIVE = 'active';
        this.STATUS_INACTIVE = 'inactive';

        var fields = [

            {name: 'id',        type: 'integer'},
            {name: 'firstName', type: 'string'},
            {name: 'lastName',  type: 'string'},
            {name: 'email',     type: 'string'},
            {name: 'username',  type: 'string'},
            {name: 'password',  type: 'string'},
            {
                name: 'status',  
                type: 'string',
                availableOptions: [this.STATUS_ACTIVE, this.STATUS_INACTIVE]
            },
            {
                name: 'roles',
                type: 'relation',
                relationType: 'manyToMany',
                relationModel: roleModel,
                labelPropertyName: 'name',
                valuePropertyName: 'id'
            }
        ];

        var validationRules = [

            {type: 'presence',  field: 'firstName'},
            {type: 'presence',  field: 'lastName'},

            {type: 'presence',  field: 'email'},
            {type: 'format',    field: 'email',     pattern: this.constants.EMAIL_VALIDATION_REGEX, message:"Wrong Email Format"},
            
            {type: 'presence',  field: 'username'},
            {
                type: 'length',
                field: 'username',
                min:5, 
                max:64, 
                message: "Username must be between 5 and 64 characters."
            },
            {
                type: 'presence',
                field: 'password',
                forOperation: [
                    this.constants.CREATE_OPERATION,
                    this.constants.UPDATE_USER_PASSWORD_OPERATION
                ]
            },
            {
                type: 'length',
                field: 'password',
                min:5, 
                max:64, 
                message: "Password must be between 5 and 64 characters.",
                forOperation: [
                    this.constants.CREATE_OPERATION,
                    this.constants.UPDATE_USER_PASSWORD_OPERATION
                ]
            },

            {type: 'presence',  field: 'status'},
            {type: 'presence',  field: 'roles'}
        ];

        this.getFields = function() {
            return fields;
        };

        this.getValidationRules = function() {
            return validationRules;
        };
    }
});