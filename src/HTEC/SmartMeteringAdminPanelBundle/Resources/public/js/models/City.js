HCM.define({

    name: 'Model.City',

    extendFrom: 'Base.Model',

    extendPrototypeWith: {
        events: HCM.getNamespaceValue('Events.Model.City')
    },

    construct: function() {

        this.callConstructor('Base.Model');

        var that = this;
        var urlPart = '/city';

        var fields = [
            {
                name: 'id',
                type: 'integer'
            },
            {
                name: 'name',
                type: 'string'
            }
        ];

        var validationRules = [
            {type: 'presence',  field: 'name'}
        ];

        this.getUrlPart = function() {
            return urlPart;
        };

        this.getFields = function() {
            return fields;
        };

        this.getValidationRules = function() {
            return validationRules;
        };
    }
});
