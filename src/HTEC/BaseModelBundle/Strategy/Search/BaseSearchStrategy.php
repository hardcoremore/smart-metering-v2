<?php

namespace HTEC\BaseModelBundle\Strategy\Search;

use Doctrine\ORM\QueryBuilder;

use Symfony\Component\Serializer\Encoder\JsonDecode;
use Symfony\Component\HttpFoundation\RequestStack;

abstract class BaseSearchStrategy
{
    private $searchQueryParser;

    public function __construct(SearchQueryParser $queryParser)
    {
        $this->searchQueryParser = $queryParser;
        $this->searchQueryParser->parse();
    }

    abstract public function bindParameters(QueryBuilder $queryBuilder);
   
    protected function bindSearchRule(QueryBuilder $queryBuilder, $searchRule, $entityAliasAndFieldName, $returnParameters = FALSE)
    {
        $searchFields = $this->searchQueryParser->getSearchFields();

        $searchFieldAlreadyExists = in_array($searchRule->field, $searchFields);
        
        if($searchFieldAlreadyExists)
        {
            $uniqueParameterName = $this->getUniqueParameterName($searchRule->field);
        }
        else
        {
            array_push($searchFields, $searchRule->field);
            $uniqueParameterName = $searchRule->field;
        }
        
        if(isset($searchRule->groupOp) === FALSE)
        {
            if($this->searchQueryParser->getDecodedFilters() && $this->searchQueryParser->getDecodedFilters()->groupOp)
            {
                $searchRule->groupOp = strtolower($this->searchQueryParser->getDecodedFilters()->groupOp);    
            }
            else
            {
                $searchRule->groupOp = 'or';   
            }
        }

        if($searchRule->groupOp === "and")
        {
            $operationName = "andWhere";
        }
        else
        {   
            $operationName = "orWhere";
        }

        
        if($searchRule->data === 'true' || $searchRule->data === 'false')
        {
            $searchRule->data = ($searchRule->data === 'true');
        }

        $parameterArgumentStrings = $this->buildParameter(
            $searchRule->op, 
            $entityAliasAndFieldName,
            $uniqueParameterName,
            $searchRule->data
        );

        if($returnParameters)
        {
            $returnData = new \stdClass();
            $returnData->doctrineOperationName = $operationName;
            $returnData->operationArgument = $parameterArgumentStrings["operationArgument"];
            $returnData->parameterDataArgument = $parameterArgumentStrings["parameterDataArgument"];
            $returnData->uniqueParameterName = $uniqueParameterName;

            return $returnData;
        }
        else
        {
            $queryBuilder->$operationName($parameterArgumentStrings["operationArgument"])
                         ->setParameter($uniqueParameterName, $parameterArgumentStrings["parameterDataArgument"]);
        }
    }

    public function getUniqueParameterName($parameterName)
    {
        return $parameterName . "_" . str_replace(array(' ', '.'), '', microtime());
    }

    protected function buildParameter($operation, $entityAliasAndFieldName, $uniqueParameterName, $data)
    {
        $operationArgumentString = "";
        $parameterDataArgumentString = "";

        switch($operation)
        {
            // equals
            case "eq":

                $operationArgumentString = $entityAliasAndFieldName . ' = ' . ':' . $uniqueParameterName;
                $parameterDataArgumentString = $data;

            break;

            // not equals
            case "ne":

                $operationArgumentString = $entityAliasAndFieldName . ' != ' . ':' . $uniqueParameterName;
                $parameterDataArgumentString = $data;

            break;

            // begins with
            case "bw":

                $operationArgumentString = $entityAliasAndFieldName . ' LIKE ' . ':' . $uniqueParameterName;
                $parameterDataArgumentString = $data . "%";

            break;

            // not begins with
            case "bn":

                $operationArgumentString = $entityAliasAndFieldName . ' NOT LIKE ' . ':' . $uniqueParameterName;
                $parameterDataArgumentString = $data . "%";

            break;

            // ends with
            case "ew":

                $operationArgumentString = $entityAliasAndFieldName . ' LIKE ' . ':' . $uniqueParameterName;
                $parameterDataArgumentString = "%" . $data;

            break;

            // not ends with
            case "en":

                $operationArgumentString = $entityAliasAndFieldName . ' NOT LIKE ' . ':' . $uniqueParameterName;
                $parameterDataArgumentString = "%" . $data;

            break;

            // contains
            case "cn":

                $operationArgumentString = $entityAliasAndFieldName . ' LIKE ' . ':' . $uniqueParameterName;
                $parameterDataArgumentString = "%" . $data . "%";

            break;

            // not contains
            case "nc":

                $operationArgumentString = $entityAliasAndFieldName . ' NOT LIKE ' . ':' . $uniqueParameterName;
                $parameterDataArgumentString = "%" . $data . "%";

                
            break;

        } // end of switch

        return array(
            "operationArgument" => $operationArgumentString,
            "parameterDataArgument" => $parameterDataArgumentString
        );
    }

    protected function bindTagsField(QueryBuilder $queryBuilder, $searchRule)
    {
        if($searchRule->op !== "ne")
        {
            $queryBuilder->leftJoin("pr.tags", 't');
        }

        $searchTags = explode(',', $searchRule->data);
        
        $searchTagsLength = count($searchTags);
        $uniqueParamName = "";
        $tagSearchRule = NULL;
        $bindedSearchParameters = NULL;
        $operationParameterString = "";

        for($i = 0; $i < $searchTagsLength; $i++)
        {

            $tagSearchRule = new \stdClass();
            $tagSearchRule->op = $searchRule->op;
            $tagSearchRule->data = $searchTags[$i];
            $tagSearchRule->field = "tag_name";

            if($searchRule->op !== "ne")
            {
                $bindedSearchParameters = $this->bindSearchRule($queryBuilder, $tagSearchRule, 't.name', TRUE);

                if($i === $searchTagsLength - 1)
                {
                    $operationParameterString .= $bindedSearchParameters->operationArgument;
                }
                else
                {
                    $operationParameterString .= $bindedSearchParameters->operationArgument . " OR ";
                }

                $queryBuilder->setParameter(
                    $bindedSearchParameters->uniqueParameterName,
                    $bindedSearchParameters->parameterDataArgument
                );
            }
            else
            {
                $this->bindTagNotExists($queryBuilder, $tagSearchRule);
            }
            
        }
        
        if($searchRule->op !== "ne")
        {
            if($this->searchQueryParser->getDecodedFilters()->groupOp === "AND")
            {
                $queryBuilder->andWhere($operationParameterString);
            }
            else
            {
                $queryBuilder->orWhere($operationParameterString);
            }
        }
    }

    public function getQuickSearchRule()
    {
        $searchRule = new \stdClass();
        $searchRule->op = "cn";
        $searchRule->data = $this->searchQueryParser->getQuickSearchQuery();
        $searchRule->field = $this->searchQueryParser->getQuickSearchPropertyName();

        return $searchRule;
    }
}