<?php

namespace HTEC\BaseModelBundle\Strategy\Search;

use Doctrine\ORM\QueryBuilder;

use Symfony\Component\Serializer\Encoder\JsonDecode;
use Symfony\Component\HttpFoundation\RequestStack;

class SearchQueryParser
{   
    private $isQuickSearch;

    private $requestStack;
    private $searchFields;

    private $quickSearchQuery;
    private $quickSearchPropertyName;

    protected $decodedFilters;

    protected $searchParameters;

    public function __construct(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
        $this->searchFields = array();
    }

    public function getIsQuickSearch()
    {
        return $this->isQuickSearch;
    }

    public function getSearchFields()
    {
        return $this->searchFields;
    }

    public function getQuickSearchQuery()
    {
        return $this->quickSearchQuery;
    }

    public function getQuickSearchPropertyName()
    {
        return $this->quickSearchPropertyName;
    }

    public function setDecodedFilters($filters)
    {
        $this->decodedFilters = $filters;
    }

    public function getDecodedFilters()
    {
        return $this->decodedFilters;
    }

    public function parse()
    {
        $currentRequest = $this->requestStack->getCurrentRequest();

        $searchFilters = null;

        if($currentRequest && property_exists($currentRequest, 'query'))
        {
            $searchFilters = $currentRequest->query->get('filters');

            if($searchFilters AND strlen($searchFilters) > 0)
            {
                $this->isQuickSearch = false;

                $jsonDecode = new JsonDecode();
                $decodedFilters = $jsonDecode->decode($searchFilters, $format = "");

                if(is_object($decodedFilters) AND is_array($decodedFilters->rules) AND count($decodedFilters->rules) > 0)
                {
                    $this->setDecodedFilters($decodedFilters);

                    foreach($decodedFilters->rules as $key => $searchRule)
                    {
                        if($decodedFilters->groupOp)
                        {
                            $searchRule->groupOp = strtolower($decodedFilters->groupOp);
                        }

                        if(!in_array($searchRule->field, $this->searchFields))
                        {
                            array_push($this->searchFields, $searchRule->field);
                        }
                    }
                }
            }
            else if($currentRequest->query->get('quickSearch'))
            {
                $this->quickSearchPropertyName = $currentRequest->query->get('quickSearchPropertyName');
                $this->quickSearchQuery = $currentRequest->query->get('quickSearchQuery', '');
                $this->isQuickSearch = true;
            }
        }
    }
}