<?php

namespace HTEC\BaseModelBundle\Entity;

class PagedData
{
    public $rowsCount;
    public $totalRowsCount;
    public $pageNumber;
    public $totalPageCount;
    public $listData;
}