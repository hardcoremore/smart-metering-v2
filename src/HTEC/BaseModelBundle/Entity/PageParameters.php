<?php

namespace HTEC\BaseModelBundle\Entity;   

class PageParameters
{
    public $pageNumber;
    public $rowsPerPage;
    public $sortColumnName;
    public $sortColumnOrder;
}
