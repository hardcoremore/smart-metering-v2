<?php

namespace HTEC\BaseModelBundle\Services;

use Symfony\Component\DependencyInjection\ContainerInterface;

use HTEC\BaseModelBundle\Strategy\Search\BaseSearchStrategy;
use HTEC\BaseModelBundle\Entity\PageParameters;
use HTEC\BaseModelBundle\Entity\PagedData;

use Doctrine\ORM\EntityRepository;

class PageGenerator
{
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function getPagedData(EntityRepository $repository, PageParameters $pageParams, array $joinColumns = null, BaseSearchStrategy $searchStrategy = NULL)
    {
        $entitiesConfig = $this->container->getParameter('entity.config');
        $entityConfig = array();
        $entityConfigFields = array();

        if(array_key_exists($repository->getClassName(), $entitiesConfig))
        {
            $entityConfig = $entitiesConfig[$repository->getClassName()];
            $entityConfigFields = $entityConfig['fields'];
        }
        
        if($pageParams->rowsPerPage < 1)
        {
            $pageParams->rowsPerPage = 10;
        }

        $queryBuilder = $repository->createQueryBuilder('pr')
                                   ->select('count(pr)');

        if($searchStrategy)
        {
            $searchStrategy->bindParameters($queryBuilder);
        }

        $offset = 0;
        $totalRowsCount = $queryBuilder->getQuery()->getSingleScalarResult();

        if((int)$pageParams->pageNumber === 1)
        {
            $offset = 0;
        }
        else if($pageParams->pageNumber > 1)
        {
            $offset = ($pageParams->pageNumber - 1) * $pageParams->rowsPerPage;
        }

        if(strlen($pageParams->sortColumnName) > 0 AND strlen($pageParams->sortColumnOrder) > 0)
        {

            $queryBuilder = $repository->createQueryBuilder('pr')
                                       
                                       ->setFirstResult($offset)
                                       ->setMaxResults($pageParams->rowsPerPage)
                                       ->orderBy('t.name', $pageParams->sortColumnOrder);

            if(isset($entityConfigFields[$pageParams->sortColumnName]) && isset($entityConfigFields[$pageParams->sortColumnName]['orderBy']))
            {
                $config = $entityConfigFields[$pageParams->sortColumnName];

                $orderByFieldName = $config['orderBy'];
                $joinByFieldName = isset($config['joinBy']) === true ? $config['joinBy'] : null;

                $isSortColumnRelationFieldColumn = false;

                if(isset($config['isSortColumnRelationFieldColumn']) && $config['isSortColumnRelationFieldColumn'] === true)
                {
                    $isSortColumnRelationFieldColumn = true;
                }
                
                if($joinByFieldName)
                {
                    $queryBuilder->leftJoin("pr." . $joinByFieldName, 'rel', 'WITH', 'rel = pr.' . $joinByFieldName);
                }
                else
                {
                    $queryBuilder->leftJoin("pr." . $pageParams->sortColumnName, 'rel', 'WITH', 'rel = pr.' . $pageParams->sortColumnName);
                }

                if($isSortColumnRelationFieldColumn)
                {
                    $queryBuilder->leftJoin("rel." . $pageParams->sortColumnName, 'relationColumn', 'WITH', 'relationColumn = rel.' . $pageParams->sortColumnName);
                    $queryBuilder->orderBy('relationColumn.' . $orderByFieldName, $pageParams->sortColumnOrder);
                }
                else
                {
                    $queryBuilder->orderBy('rel.' . $orderByFieldName, $pageParams->sortColumnOrder);
                }
            }
            else
            {
                $queryBuilder->orderBy('pr.' . $pageParams->sortColumnName, $pageParams->sortColumnOrder);
            }
        }
        else
        {
            $queryBuilder = $repository->createQueryBuilder('pr')
                                       ->setFirstResult($offset)
                                       ->setMaxResults($pageParams->rowsPerPage);
        }

        if($joinColumns)
        {
            foreach ($joinColumns AS $value)
            {
                $queryBuilder->leftJoin('pr.' . $value, $value)->addSelect($value);
            }
        }

        if($searchStrategy)
        {
            $searchStrategy->bindParameters($queryBuilder);
        }
            
        $query = $queryBuilder->getQuery();


        $pagedData = new PagedData();

        $pagedData->listData = $query->getResult();
        $pagedData->rowsCount = count($pagedData->listData);
        $pagedData->totalRowsCount = (int)$totalRowsCount;
        $pagedData->pageNumber = (int)$pageParams->pageNumber;
        $pagedData->totalPageCount = ceil($totalRowsCount / $pageParams->rowsPerPage);

        return $pagedData;
    }
}