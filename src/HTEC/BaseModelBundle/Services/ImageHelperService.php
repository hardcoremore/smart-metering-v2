<?php

namespace HTEC\BaseModelBundle\Services;

class ImageHelperService
{

    public function readImageProperties($filePath)
    {
        if(empty($filePath) === false)
        {
            $imageType = exif_imagetype($filePath);

            $imageProperties = array();

            $imageProperties['path'] = $filePath;
            $imageProperties['type'] = $imageType;

            if($imageType)
            {
                $imageProperties['extension'] = image_type_to_extension($imageType);
                $imageDimensions = getimagesize($filePath);

                $imageProperties['width']    = $imageDimensions[0];
                $imageProperties['height']   = $imageDimensions[1];

                if(isset($imageDimensions['channels']))
                {
                    $imageProperties['bitRate']  = $imageDimensions['bits'] * $imageDimensions['channels'];
                }
                
                $imageProperties['mimeType'] = $imageDimensions['mime'];
                $imageProperties['size'] = filesize($filePath);
            }

            return $imageProperties;
        }

        return null;
    }

    public function getImageResourceFromPath($imagePath, $imageType)
    {
        if(empty($imagePath))
        {
            throw new \Exception("Invalid image path");
        }

        $imageResource = null;

        switch($imageType)
        {
            case IMAGETYPE_GIF:
                $imageResource = imagecreatefromgif($imagePath);
            break;

            case IMAGETYPE_JPEG:
                $imageResource = imagecreatefromjpeg($imagePath);
            break;

            case IMAGETYPE_PNG:
                $imageResource = imagecreatefrompng($imagePath);
            break;
        }

        if(!$imageResource)
        {
            throw new \Exception("Failed to read the image file: " . $imagePath);
        }

        return $imageResource;
    }

    public function cropImage($image, $cropParameters, $imageProperties = null)
    {
        if(is_array($imageProperties) === null)
        {
            $imageProperties = $this->readImageProperties($image);
        }

        if(is_string($image) && strlen($image) > 0)
        {
            $sourceImage = $this->getImageResourceFromPath($image, $imageProperties['type']);
        }
        else if(is_resource($image))
        {
            $sourceImage = $image;
        }
        else
        {
            throw new Exception("Provided image is invalid, please provide path to the image or image resource");
        }

        if(empty($cropParameters))
        {
            throw new Exception("You have to provide crop parameters");
        }        

        $originalImageWidth = $imageProperties['width'];
        $originalImageHeight = $imageProperties['height'];

        $sourceImageWidth = $originalImageWidth;
        $sourceImageHeight = $originalImageHeight;

        $degrees = $cropParameters['rotate'];

        // Rotate the source image
        if(is_numeric($degrees) && $degrees != 0)
        {
            // PHP's degrees is opposite to CSS's degrees
            $backgroundColor = imagecolorallocatealpha($sourceImage, 0, 0, 0, 127);

            if(isset($cropParameters['isCssDegrees']) && $cropParameters['isCssDegrees'] === true)
            {
                $newImage = imagerotate($sourceImage, -$degrees, $backgroundColor);
            }
            else
            {
                $newImage = imagerotate($sourceImage, $degrees, $backgroundColor);   
            }
            

            imagedestroy($sourceImage);
            $sourceImage = $newImage;

            $deg = abs($degrees) % 180;
            $arc = ($deg > 90 ? (180 - $deg) : $deg) * M_PI / 180;

            $sourceImageWidth = $originalImageWidth * cos($arc) + $originalImageHeight * sin($arc);
            $sourceImageHeight = $originalImageWidth * sin($arc) + $originalImageHeight * cos($arc);

            // Fix rotated image miss 1px issue when degrees < 0
            $sourceImageWidth -= 1;
            $sourceImageHeight -= 1;
        }

        $tmpImageWidth = $cropParameters['width'];
        $tmpImageHeight = $cropParameters['height'];
        $destinationImageWidth = $cropParameters['width'];
        $destinationImageHeight = $cropParameters['height'];

        $sourceImageX = $cropParameters['x'];
        $sourceImageY = $cropParameters['y'];

        if($sourceImageX <= -$tmpImageWidth || $sourceImageX > $sourceImageWidth)
        {
            $sourceImageX = $sourceWidth = $destinationX = $destinationWidth = 0;
        }
        else if($sourceImageX <= 0)
        {
            $destinationX = -$sourceImageX;
            $sourceImageX = 0;
            $sourceWidth = $destinationWidth = min($sourceImageWidth, $tmpImageWidth + $sourceImageX);
        }
        else if($sourceImageX <= $sourceImageWidth)
        {
            $destinationX = 0;
            $sourceWidth = $destinationWidth = min($tmpImageWidth, $sourceImageWidth - $sourceImageX);
        }

        if($sourceWidth <= 0 || $sourceImageY <= -$tmpImageHeight || $sourceImageY > $sourceImageHeight)
        {
            $sourceImageY = $sourceHeight = $destinationY = $destinationHeight = 0;
        }
        else if($sourceImageY <= 0)
        {
            $destinationY = -$sourceImageY;
            $sourceImageY = 0;
            $sourceHeight = $destinationHeight = min($sourceImageHeight, $tmpImageHeight + $sourceImageY);
        }
        else if($sourceImageY <= $sourceImageHeight)
        {
            $destinationY = 0;
            $sourceHeight = $destinationHeight = min($tmpImageHeight, $sourceImageHeight - $sourceImageY);
        }

        // Scale to destination position and size
        $ratio = $tmpImageWidth / $destinationImageWidth;
        $destinationX /= $ratio;
        $destinationY /= $ratio;
        $destinationWidth /= $ratio;
        $destinationHeight /= $ratio;

        $croppedImageResource = null;

        $croppedImageResource = imagecreatetruecolor($destinationImageWidth, $destinationImageHeight);

        // Add transparent background to destination image
        imagefill($croppedImageResource, 0, 0, imagecolorallocatealpha($croppedImageResource, 0, 0, 0, 127));
        imagesavealpha($croppedImageResource, true);

        $result = imagecopyresampled($croppedImageResource, $sourceImage, $destinationX, $destinationY, $sourceImageX, $sourceImageY, $destinationWidth, $destinationHeight, $sourceWidth, $sourceHeight);

        if(isset($cropParameters['maxWidth']))
        {
            $maxWidth = $cropParameters['maxWidth'];
        }
        else
        {
            $maxWidth = $originalImageHeight;
        }

        if(isset($cropParameters['maxHeight']))
        {
            $maxHeight = $cropParameters['maxHeight'];
        }
        else
        {
            $maxHeight = $originalImageHeight;
        }

        $newWidth = 0;
        $newHeight = 0;

        $ratio = 0;
        $resize = false;

        if(($destinationWidth > $maxWidth) && ($destinationWidth >= $destinationHeight))
        {
            $ratio = $maxWidth / $destinationWidth;
            $newHeight = round($destinationHeight * $ratio);
            $newWidth = $maxWidth;
            $resize = true;
        }

        if(($destinationHeight > $maxHeight) && ($destinationHeight > $destinationWidth))
        {
            $ratio = $maxHeight / $destinationHeight;
            $newWidth = round($destinationWidth * $ratio);
            $newheight = $maxHeight;
            $resize = true;
        }

        if($resize)
        {
            $resizedImage = imagecreatetruecolor($newWidth, $newHeight);
            $result = imagecopyresampled($resizedImage, $croppedImageResource, 0, 0, 0, 0, $newWidth, $newHeight, $destinationWidth, $destinationHeight);
        }

        if($resize)
        {
            return $resizedImage;
        }
        else
        {
            return $croppedImageResource;
        }
    }

    public function saveImage($imageResource, $saveImagePath, $imageProperties)
    {
        $imageSaved = false;

        switch ($imageProperties['extension'])
        {
            case '.jpeg':
                $imageSaved = imagejpeg($imageResource, $saveImagePath);
            break;
            
            case '.png':
                $imageSaved = imagepng($imageResource, $saveImagePath);
            break;

            case '.gif':
                $imageSaved = imagegif($imageResource, $saveImagePath);
            break;
        }

        if(false === $imageSaved)
        {
            throw new \Exception('Failed to save image file to path: "$fileUrl"');
        }
    }
}
