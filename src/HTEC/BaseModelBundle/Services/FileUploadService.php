<?php

namespace HTEC\BaseModelBundle\Services;

use \Exception;

class FileUploadService
{
    protected $uploadConfig;

    public function __construct($uploadConfig)
    {
        $this->uploadConfig = $uploadConfig;
    }

    /**
     * Saves uploaded file
     * 
     * @param string $uploadedFilePath Path of the uploaded file
     * @param string $fileName Name of the uploaded file
     * @throws Exception If file could not be saved
     *
     * @return string Hashed file name
     */
    public function saveUploadedFile($uploadedFilePath, $fileName, $updateFile = false)
    {
        if(strlen($uploadedFilePath) < 1) 
        {
            throw new Exception("Invalid upload file path.");
        }
        
        if($updateFile)
        {
            $hashedFileName = $fileName;
            $fileDirectoryPath = $this->getPathFromFileName($hashedFileName);
        }
        else
        {
            $hashedFileName = $this->getHashedFileName($fileName);
            $fileDirectoryPath = $this->createFileFolder($hashedFileName);    
        }
        
        $destinationPath = $fileDirectoryPath . '/' . $hashedFileName;

        if(rename($uploadedFilePath, $destinationPath) === TRUE)
        {
            return $hashedFileName;
        }
        else
        {
            throw new \Exception("Failed to save uploaded file.");
        }
    }

     /**
     * Saves file from url
     * 
     * @param string $fileUrl Url of the file
     * @throws Exception If file could not be saved
     *
     * @return string Hashed file name
     */
    public function saveFileFromUrl($fileUrl)
    {
        $hashedFileName = $this->getHashedFileName($fileUrl);
        $fileDirectoryPath = $this->createFileFolder($hashedFileName);
        $destinationPath = $fileDirectoryPath . '/' . $hashedFileName;

        if(file_put_contents($destinationPath, fopen($fileUrl, 'r')) === TRUE)
        {
            return $hashedFileName;
        }
        else
        {
            throw new Exception('Failed to save file from url: $fileUrl');
        }
    }

    /**
     * Return full or relative path for file from file name
     * 
     * @param string $fileName Name of the file file with extension
     * @param boolean $relative If set to true it will suply relative path to file 
     *                          otherwise it will return full path on the server.
     * @return string Full or relative path of the file
     */
    public function getPathFromFileName($fileName, $relative = false)
    {   
        $fileDirectory = $relative === false ? $this->uploadConfig['upload_root_dir_full_path'] : $this->uploadConfig['upload_root_dir_relative_path'];
        $fileDirectory .= $this->uploadConfig['upload_base_dir'];

        return $fileDirectory . '/' . $fileName[0] . '/' . $fileName[1] . '/' . $fileName[2];
    }

    /**
     * Return full or relative url for file from file name
     * 
     * @param string $fileName Name of the file file with extension
     * @param boolean $relative If set to true it will suply relative url to file 
     *                          otherwise it will return full url to the file.
     * @return string Full or relative url of the file
     */
    public function getUrlFromFileName($fileName, $relative = false)
    {
        $fileUrl = $relative === false ? $this->uploadConfig['upload_root_full_url'] : $this->uploadConfig['upload_root_relative_url'];
        $fileUrl .= $this->uploadConfig['upload_base_dir'];

        return $fileUrl . '/' . $fileName[0] . '/' . $fileName[1] . '/' . $fileName[2] . '/' . $fileName;
    }

     /**
     * Create folder for the file
     * 
     * @param string $fileName Name of the file. Usually file with hashed file name.
     * @return string Full path of the created folder
     *
     * @throws Exception if $uploadType is invalid
     */
    public function createFileFolder($fileName)
    {   
        $path = $this->getPathFromFileName($fileName);

        if(is_dir($path) === false)
        {
            if(mkdir($path, 0775, true) === false)
            {
                throw new Exception("Could not create directory.");
            }
        }

        return $path;
    }

    /**
     * Get hashed name from the filename.
     * 
     * @param string $fileName Name of the file.
     * @return string Rerturn hashed name from the file name including file's extension.
     *
     * @throws Exception if $uploadType is invalid
     */
    public function getHashedFileName($fileName)
    {
        $extension = pathinfo($fileName, PATHINFO_EXTENSION);
        $randomFileName = md5(date('Y-m-d H:i:s') . ':' . time() . '-' . mt_rand(0, mt_getrandmax()) . '-' . $fileName);

        if(strlen($extension) > 0)
        {
            $randomFileName = $randomFileName . '.' . $extension;
        }

        return  $randomFileName;
    }

    public function getFileNameFromUrl($url)
    {
        return pathinfo($url, PATHINFO_FILENAME) . '.' . pathinfo($url, PATHINFO_EXTENSION);
    }
}
