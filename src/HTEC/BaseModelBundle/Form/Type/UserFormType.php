<?php

namespace HTEC\BaseModelBundle\Form\Type;

use HTEC\BaseModelBundle\Form\Type\ConfigFormType;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class UserFormType extends ConfigFormType
{
    public function __construct(ContainerInterface $container, RequestStack $requestStack, $formConfigName = 'user')
    {
        parent::__construct($container, $requestStack, $formConfigName);
    }
}