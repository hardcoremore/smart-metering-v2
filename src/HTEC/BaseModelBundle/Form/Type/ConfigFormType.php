<?php

namespace HTEC\BaseModelBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Loader;

use HTEC\BaseModelBundle\Exception\Form\FormFieldNotUpdatableException;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class ConfigFormType extends AbstractType
{
    protected $container;
    protected $requestStack;

    protected $formConfig;

    protected $formFields;
    protected $formConfigName;
    protected $formServiceNamespace;

    public function __construct(ContainerInterface $container, RequestStack $requestStack, $formConfigName)
    {
        $this->container = $container;
        $this->requestStack = $requestStack;
        $this->formConfigName = $formConfigName;
    }

    public function setFormServiceNamespace($nameSpace)
    {
        $this->formServiceNamespace = $nameSpace; 
    }

    public function getFormServiceNamespace()
    {
        return $this->formServiceNamespace;
    }

    public function getFormConfiguration()
    {
        return $this->formConfig;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if(isset($this->formConfig['extendFieldsFrom']))
        {
            $extendFieldsFormConfig = $this->getFormConfigFromParameterName($this->formConfig['extendFieldsFrom']);

            if(is_array($extendFieldsFormConfig['fields']) && count($extendFieldsFormConfig['fields']) > 0)
            {
                $formMetadata = $this->getFormMetadata($extendFieldsFormConfig['default_options']["data_class"]);
                $this->processFields($builder, $extendFieldsFormConfig["fields"], $formMetadata, $options);
            }
        }

        if(isset($this->formConfig['fields']))
        {
            $this->formFields = $this->formConfig['fields'];

            $formMetadata = $this->getFormMetadata($this->formConfig['default_options']['data_class']);
            $this->processFields($builder, $this->formFields, $formMetadata, $options);
        }
        
    }

    public function getFormMetadata($formClassName)
    {
        return $this->container->get('validator')
                               ->getMetadataFor($formClassName);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults($this->formConfig["default_options"]);
    }

    public function getName() 
    {
        return $this->formConfig["name"];
    }
    
    protected function processFields(FormBuilderInterface $builder, array $formFields, $formMetadata, array $options)
    {
        // create form dinamically from config
        foreach($formFields as $key => $val)
        {
            $this->addField($builder, $key, $val);
        }
    }

    protected function addField(FormBuilderInterface $builder, $fieldName, $fieldOptions)
    {
        if(isset($fieldOptions["options"]))
        {
            if($fieldOptions['type'] === 'collection' && isset($fieldOptions['options']['entry_type']))
            {
                $fieldOptions['options']['entry_type'] = $this->getFieldClassTypeFromFieldName($fieldOptions['options']['entry_type']);
            }

            if(isset($fieldOptions["options"]['empty_data']))
            {
                $emptyData = $fieldOptions["options"]['empty_data'];

                if($emptyData === 'null')
                {
                    $fieldOptions["options"]['empty_data'] = null;
                }
                else if($emptyData === 'false')
                {
                    $fieldOptions["options"]['empty_data'] = false;
                }
                else if($emptyData === 'true')
                {
                    $fieldOptions["options"]['empty_data'] = true;   
                }
            }

            $builder->add(
                $fieldName,
                $this->getFieldClassTypeFromFieldName($fieldOptions["type"]),
                $fieldOptions["options"]
            );
        }
        else
        {
            $builder->add($fieldName, $this->getFieldClassTypeFromFieldName($fieldOptions["type"]));
        }
    }

    protected function checkIfFieldIsRequired($key, $formMetadata) 
    {
        $fieldMetadata = NULL;

        if($formMetadata->hasMemberMetadatas($key))
        {  
            $fieldMetadata = $formMetadata->getMemberMetadatas($key);
 
            foreach($fieldMetadata as $key => $val)
            {
                if(is_a($val, "Symfony\Component\Validator\Constraints\NotBlank"))
                {
                    return TRUE;
                }
            }

            return FALSE;
        }
        
        return FALSE;
    }

    public function loadFormConfig()
    {
        $formConfigParameterName = $this->getFormServiceNamespace() . '.' . $this->formConfigName;
        $this->formConfig = $this->getFormConfigFromParameterName($formConfigParameterName);
    }

    public function getFormConfigFromParameterName($formConfigParameterName)
    {
        return $this->container->getParameter($formConfigParameterName);
    }

    public function getFieldClassTypeFromFieldName($fieldName)
    {
        switch($fieldName)
        {
            case 'text':
                return TextType::class;
            break;

            case 'number':
                return NumberType::class;
            break;

            case 'entity':
                return EntityType::class;
            break;

            case 'choice':
                return ChoiceType::class;
            break;

            case 'datetime':
                return DateTimeType::class;
            break;

            case 'time':
                return TimeType::class;
            break;

            case 'collection':
                return CollectionType::class;
            break;

            case 'date':
                return DateType::class;
            break;
        }
    }
}