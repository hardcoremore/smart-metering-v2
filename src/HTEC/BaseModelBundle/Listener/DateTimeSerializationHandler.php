<?php

namespace HTEC\BaseModelBundle\Listener;

use JMS\Serializer\Handler\DateHandler;

class DateTimeSerializationHandler extends DateHandler
{
    public function __construct($defaultFormat = \DateTime::ISO8601, $defaultTimezone = 'UTC', $xmlCData = true)
    {
        parent::__construct('Y-m-d H:i:s', $defaultTimezone, $xmlCData);
    }
}