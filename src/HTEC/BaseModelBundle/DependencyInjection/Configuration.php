<?php

namespace HTEC\BaseModelBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('base_model');

        $rootNode->children()
                    ->arrayNode('data_fixtures')
                        ->requiresAtLeastOneElement()
                        ->prototype('array')
                            ->children()
                                ->scalarNode('namespace')
                                    ->cannotBeEmpty()
                                ->end()
                                ->scalarNode('connection')
                                    ->cannotBeEmpty()
                                ->end()
                                ->scalarNode('extends_from')
                                    ->cannotBeEmpty()
                                ->end()
                                ->arrayNode('fixtures')
                                    ->requiresAtLeastOneElement()
                                    ->prototype('scalar')
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end();

        return $treeBuilder;
    }
}
