<?php

namespace HTEC\BaseModelBundle\Exception\Entity;

class EntityNotFoundException extends \Exception {}