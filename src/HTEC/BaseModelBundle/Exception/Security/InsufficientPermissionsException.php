<?php

namespace HTEC\BaseModelBundle\Exception\Security;

class InsufficientPermissionsException extends \Exception
{
}