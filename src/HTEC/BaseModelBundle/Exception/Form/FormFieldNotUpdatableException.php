<?php

namespace HTEC\BaseModelBundle\Exception\Form;

class FormFieldNotUpdatableException extends \Exception
{
}