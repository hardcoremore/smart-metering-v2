<?php

namespace HTEC\BaseModelBundle\Exception\Form;

class InvalidFormDataException extends \Exception
{
    protected $formErrorMessage = '';
    protected $errorMessages;
    protected $form;

    public function setFormErrorMessage(string $message)
    {
        $this->formErrorMessage = $message;
    }

    public function getFormErrorMessage():string
    {
        return $this->formErrorMessage;
    }

    public function setErrorMessages($errorMessages)
    {
        $this->errorMessages = $errorMessages;
    }

    public function getErrorMessages()
    {
        return $this->errorMessages;
    }

    public function setForm($form)
    {
        $this->form = $form;
    }

    public function getForm()
    {
        return $this->form;
    }
}