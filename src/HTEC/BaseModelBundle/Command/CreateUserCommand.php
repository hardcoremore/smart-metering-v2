<?php

namespace HTEC\BaseModelBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;

use HTEC\BaseModelBundle\Model\BaseModel;
use HTEC\BaseModelBundle\Exception\Form\InvalidFormDataException;
use HTEC\BaseAPIBundle\Helpers\ExtractFormErrorsHelper;

class CreateUserCommand extends Command
{
    protected $userModel;

    public function setUserModel(BaseModel $userModel)
    {
        $this->userModel = $userModel;
    }

    public function getUserModel()
    {
        return $this->userModel;
    }

    protected function configure()
    {
        $this->setName('user:create')
             ->setDescription('Add new user to the database')
             ->addArgument(
                'first_name',
                InputArgument::REQUIRED,
                'First name of the user'
            )
             ->addArgument(
                'last_name',
                InputArgument::REQUIRED,
                'Last name of the user'
            )
             ->addArgument(
                'username',
                InputArgument::REQUIRED,
                'Username of the user to be created'
            )
             ->addArgument(
                'email',
                InputArgument::REQUIRED,
                'Email of the user'
            )
             ->addOption(
                'password',
                '-p',
                InputOption::VALUE_REQUIRED,
                'Password for the user'
            )
             ->addOption(
                'status',
                '-s',
                InputOption::VALUE_REQUIRED,
                'Status of the user'
            );
    }

    protected function interact(InputInterface $input, OutputInterface $output)
    {
        $password = $input->getOption('password');

        if(strlen($password) < 1)
    	{
            $questionHelper = $this->getHelper('question');

            $question = new Question('<info>Please enter user password:</info>');
            $question->setHidden(true);
            $question->setHiddenFallback(false);

            $question->setValidator(function ($answer) {
                if(strlen($answer) < 1) {
                    throw new \RuntimeException(
                        'You can not create user without password. Password must be minimum 8 characters long'
                    );
                }
                return $answer;
            });

            $password = $questionHelper->ask($input, $output, $question);

            $input->setOption('password', $password);
    	}
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $firstName = $input->getArgument('first_name');
        $lastName = $input->getArgument('last_name');
        $username = $input->getArgument('username');
        $email = $input->getArgument('email');
        $password = $input->getOption('password');
        $status = $input->getOption('status', 'active');

        if(strlen($status) < 1)
        {
            $status = 'active';
        }

        $formatter = $this->getHelperSet()->get('formatter');

        $title = 'Creating user with following data:';

        $output->writeln('');
        $output->writeln('<comment>' . $title . '</comment>');
        $output->writeln(str_repeat('=', strlen($title)));
        $output->writeln('');

        $output->writeln('<info>First name: </info>' . $firstName);
        $output->writeln('<info>Last name: </info>' . $lastName);
        $output->writeln('<info>Username: </info>' . $username);
        $output->writeln('<info>Email: </info>' . $email);
        $output->writeln('<info>Status: </info>' . $status);

        $output->writeln('');
        $output->writeln('');

        try
        {
            $this->getUserModel()->create([
                'firstName' => $firstName,
                'lastName' => $lastName,
                'username' => $username,
                'email' => $email,
                'password' => $password,
                'status' => $status
            ]);

            $output->writeln(
                '<comment>You have successfully created new user: </comment>' . $firstName . ' ' . $lastName . ' (' . $username . ')'
            );

            $output->writeln('');
        }
        catch(InvalidFormDataException $error)
        {
            $formErrorHelper = new ExtractFormErrorsHelper();
            $formErrors = $formErrorHelper->getErrorMessages($error);

            $formErrorMessage = 'User data is not valid';

            if(isset($formErrors['message']))
            {
                $formErrorMessage = $formErrors['message'];
            }

            $output->writeln('<error>'.$formErrorMessage.'</error>');
            $output->writeln('');


            foreach($formErrors['errors'] AS $key => $val)
            {
                $output->writeln('Field <info>'.$key.'</info> has an error: <error>' .$val[0]. '</error>');
            }

            $output->writeln('');
        }
        catch(\Exception $error)
        {
            $output->writeln('<error>'.$error->getMessage().'</error>');
        }
    }
}
