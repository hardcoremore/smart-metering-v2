<?php

namespace HTEC\BaseModelBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

use HTEC\BaseModelBundle\DataFixtures\BaseDataFixture;
use HTEC\BaseModelBundle\DataFixtures\DoctrineAwareFixtureInterface;

class LoadDataFixturesCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('fixtures:load')
             ->setDescription('Load predefined data')
             ->addOption(
                'groups',
                '-g',
                InputOption::VALUE_OPTIONAL,
                'Set fixture groups to be loaded. Separate each group with coma. E.g default,test,loadMilionUsers'
            )
             ->addOption(
                'connection',
                '-c',
                InputOption::VALUE_REQUIRED,
                'Set the connection to be used for loading data'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $env = $this->getContainer()->get('kernel')->getEnvironment();

        $groupsToLoad = ['default'];

        $groupsOptions = $input->getOption('groups');

        if(strlen($groupsOptions) > 1)
        {
            $groupsToLoad = explode(',', $groupsOptions);
        }
        else
        {
            $groupsOptions = 'default';
        }

        $helper = $this->getHelper('question');
        $question = new ConfirmationQuestion(
            '<question>Loading fixtures for group ' . $groupsOptions . ' and '. 'Are you sure n/Y?</question>  ',
            false,
            '/^Y$/'
        );

        $answer = $helper->ask($input, $output, $question);

        if($answer === true)
        {
            $output->writeln("Loading fixtures for: " . $env . ' enviorment.');
            $fixtures = $this->getContainer()->getParameter('data_fixtures');

            $fixtureGroup = null;

            $parentGroup = null;
            $fixtureParents = null;

            foreach ($groupsToLoad AS $fixtureGroupName)
            {
                if(array_key_exists($fixtureGroupName, $fixtures))
                {
                    $output->writeln('');
                    $title = 'Loading fixture group: ';
                    $output->writeln($title . '<comment>'.$fixtureGroupName.'</comment>');
                    $output->writeln(str_repeat('=', strlen($title . $fixtureGroupName)));
                    $output->writeln('');

                    $fixtureGroup = $fixtures[$fixtureGroupName];

                    if($this->isFixtureGroupParentValid($fixtures, $fixtureGroup))
                    {
                        $fixtureParents = [$fixtureGroup['extends_from']];
                        $parentGroup = $fixtures[$fixtureGroup['extends_from']];

                        while($this->isFixtureGroupParentValid($fixtures, $parentGroup))
                        {
                            $fixtureParents[] = $parentGroup['extends_from'];
                            $parentGroup = $fixtures[$parentGroup['extends_from']];
                        }

                        $fixtureParentNames = array_reverse($fixtureParents);
                        $parentFixtures = [];

                        foreach ($fixtureParentNames AS $parentFixtureName)
                        {
                            $parentFixtures[] = $this->getParentFixtures($fixtures[$parentFixtureName]);
                        }

                        $fixtureGroup['fixtures'] = array_merge($parentFixtures, $fixtureGroup['fixtures']);
                    }

                    $this->loadFixtureData($fixtureGroup, $input, $output);
                }
                else
                {
                    $output->writeln("<error>Group '".$fixtureGroupName."' does not exist in fixture configuration for environment ".$env.".</error>");
                }
            }

            $output->writeln('');
            $output->writeln('<comment>Loading data fixtures complete</comment>');
            $output->writeln('');
        }
    }

    protected function loadFixtureData(array $fixtureConfig, InputInterface $input, OutputInterface $output)
    {
        $fixtures = $fixtureConfig['fixtures'];
        $fixtureCount = count($fixtures);

        $fixtureName;
        $fixtureClass;
        $fixtureInstance;

        for($i = 0; $i < $fixtureCount; $i++)
        {
            $fixtureName = $fixtures[$i];

            if(strpos($fixtureName, '@') === false)
            {
                $fixtureClass = $fixtureConfig['namespace'] . '\\' . $fixtureName;
                $fixtureInstance = new $fixtureClass();
            }
            else
            {
                $fixtureInstance = $this->getContainer()->get(ltrim($fixtureName, '@'));    
            }

            $this->loadFixture($fixtureInstance, $fixtureConfig, $input, $output);
        }
    }

    protected function isFixtureGroupParentValid(array $allFixtures, array $fixtureConfig)
    {
        return array_key_exists('extends_from', $fixtureConfig) && array_key_exists($fixtureConfig['extends_from'], $allFixtures);
    }

    protected function getParentFixtures(array $parentFixture)
    {
        $parentNamespace = null;

        if(isset($parentFixture['namespace']))
        {
            $parentNamespace = $parentFixture['namespace'];
        }

        $parentFixtures = [];

        foreach($parentFixture['fixtures'] AS $fixture)
        {
            if($parentNamespace && strpos($fixture, '@') === false)
            {
                $parentFixtures[] = array('namespace' => $parentNamespace, 'fixture' => $fixture);
            }
            else
            {
                $parentFixtures[] = $fixture;
            }
        }

        return $parentFixtures;
    }

    protected function loadFixture($fixtureInstance, array $fixtureConfig, InputInterface $input, OutputInterface $output)
    {
        if($fixtureInstance instanceof ContainerAwareInterface)
        {
            $fixtureInstance->setContainer($this->getContainer());
        }

        if($fixtureInstance instanceof DoctrineAwareFixtureInterface)
        {
            if(strlen($fixtureInstance->getConnectionName()) < 1 && isset($fixtureConfig['connection']))
            {
                $fixtureInstance->setConnectionName($fixtureConfig['connection']);
            }

            $entityManager = null;

            if(strlen($fixtureInstance->getConnectionName()) > 1)
            {
                $entityManager = $this->getContainer()->get('doctrine')->getManager($fixtureInstance->getConnectionName());
            }
            else
            {
                $entityManager = $this->getContainer()->get('doctrine')->getManager();
            }

            $fixtureInstance->setEntityManager($entityManager);
        }

        $output->writeln('   Loading data fixture: <info>' . get_class($fixtureInstance) . '</info>');

        try
        {
            $fixtureInstance->load($input, $output);
        }
        catch(\Exception $error)
        {
            $output->writeln('<error>'.$error->getMessage().'</error>');
        }
    }
}
