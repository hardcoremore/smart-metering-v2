<?php

namespace HTEC\BaseModelBundle\DataFixtures;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Interface contract for fixture classes to implement.
 *
 * @author Caslav Sabani <caslav.sabani@gmail.com>
 */
interface FixtureInterface
{
    /**
     * Load data fixtures
     *
     */
    public function load(InputInterface $input, OutputInterface $output);
}