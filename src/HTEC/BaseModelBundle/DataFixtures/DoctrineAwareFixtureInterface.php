<?php

namespace HTEC\BaseModelBundle\DataFixtures;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use \Doctrine\ORM\EntityManager;

/**
 * Interface contract for fixture classes to implement.
 *
 * @author Caslav Sabani <caslav.sabani@gmail.com>
 */
interface DoctrineAwareFixtureInterface
{
    /**
     * Set the name of the connection for doctrine manager
     *
     * @param string
     *
     */
    public function setConnectionName($connectionName);

    /**
     * Get the name of the connection for doctrine manager
     *
     * @return string
     *
     */
    public function getConnectionName();

    /**
     * Set the doctrine entity manager
     *
     * @param $manager \Doctrine\ORM\EntityManager
     *
     */
    public function setEntityManager(EntityManager $manager);

    /**
     * Get the doctrine entity manager
     *
     * @return \Doctrine\ORM\EntityManager
     *
     */
    public function getEntityManager();
}