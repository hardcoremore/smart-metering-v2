<?php

namespace HTEC\BaseModelBundle\DataFixtures;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use Doctrine\ORM\EntityManager;

class BaseDataFixture implements FixtureInterface, ContainerAwareInterface, DoctrineAwareFixtureInterface
{
    /**
     * @var Symfony\Component\DependencyInjection\ContainerInterface
     */
    private $container;

    /**
     * @var string
     *
     *  Name of the connection name to the database
     */
    private $connectionName;

    /**
     * @var Doctrine\ORM\EntityManager
     *
     *  Doctrine entity manager
     */
    private $entityManager;


    /**
     * @inheritDoc
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * @inheritDoc
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * @inheritDoc
     */
    public function setConnectionName($connectionName)
    {
        $this->connectionName = $connectionName;
    }

    /**
     * @inheritDoc
     */
    public function getConnectionName()
    {
        return $this->connectionName;
    }

    /**
     * @inheritDoc
     */
    public function setEntityManager(EntityManager $manager)
    {
        $this->entityManager = $manager;
    }

    /**
     * @inheritDoc
     */
    public function getEntityManager()
    {
        return $this->entityManager;
    }

    public function load(InputInterface $input, OutputInterface $output)
    {
        throw new \Exception("Load method needs to be overridend.");
    }
}

