<?php

namespace HTEC\BaseModelBundle\Model;

use Doctrine\ORM\EntityManager;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\DependencyInjection\ContainerInterface;

use HTEC\BaseModelBundle\Model\BaseModel;
use HTEC\BaseModelBundle\Exception\Form\InvalidFormDataException;

class UserModel extends BaseModel
{
    public function encodeUserPassword($entity, $plainPassword)
    {
        $factory = $this->container->get('security.encoder_factory');
        $encoder = $factory->getEncoder($entity);

        // encode password
        $password = $encoder->encodePassword($plainPassword, $entity->getSalt());

        return $password;
    }

    public function find($email)
    {
        return $this->entityManager->getRepository($this->entityName)->findOneByEmail($email);
    }

    protected function updateEntityBeforeCreate($entity, array $data)
    {
        parent::updateEntityBeforeCreate($entity, $data);

        // set encoded password
        $entity->setPassword($this->encodeUserPassword($entity, $entity->getPassword()));

        $entity->setStatus('active');
    }

    protected function save($id, array $data, bool $isPatch = false, bool $checkPermissions = true)
    {
        // password will be updated through separate method
        if(is_array($data) && array_key_exists('password', $data))
        {
            unset($data['password']);
        }

        return parent::save($id, $data, $isPatch, $checkPermissions);
    }
}
