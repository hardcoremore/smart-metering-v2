<?php

namespace HTEC\BaseModelBundle\Model;

use Doctrine\ORM\EntityManager;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\FormFactoryInterface;

use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Symfony\Component\Security\Acl\Domain\UserSecurityIdentity;
use Symfony\Component\Security\Acl\Permission\MaskBuilder;

use HTEC\BaseModelBundle\Exception\Security\InsufficientPermissionsException;
use HTEC\SmartMeteringAPIBundle\Strategy\Search\SearchStrategy;
use HTEC\BaseModelBundle\Exception\Form\InvalidFormDataException;
use HTEC\BaseModelBundle\Entity\User;
use HTEC\BaseModelBundle\Services\FileUploadService;
use HTEC\BaseModelBundle\Strategy\Search\BaseSearchStrategy;
use HTEC\BaseModelBundle\Entity\PageParameters;
use HTEC\SmartMeteringModelBundle\Services;

class BaseModel
{
    protected $container;
    protected $entityManager;
    protected $formFactory;

    protected $entityName;
    protected $formType;
    protected $formTypeClass;

    protected $quickSearchStrategy;
    protected $advancedSearchStrategy;

    public function __construct(ContainerInterface $container, EntityManager $entityManager, FormFactoryInterface $formFactory)
    {
        $this->container = $container;
        $this->entityManager = $entityManager;
        $this->formFactory = $formFactory;
    }

    public function getLoggedInUser()
    {
        if(php_sapi_name() !== 'cli')
        {
            // retrieving the security identity of the currently logged-in user
            $tokenStorage = $this->container->get('security.token_storage');
            return $tokenStorage->getToken()->getUser();
        }
    }

    public function setEntityName($entityName)
    {
        $this->entityName = $entityName;
    }

    public function getEntityName()
    {
        return $this->entityName;
    }

    public function getPageGenerator()
    {
        return $this->container->get('htec.basemodelbundle.page_generator');
    }

    public function setFormType(AbstractType $formType)
    {
        $this->formType = $formType;
    }

    public function setFormTypeClass($className)
    {
        $this->formTypeClass = $className;
    }

    public function getFormTypeClass()
    {
        return $this->formTypeClass;
    }

    public function setQuickSearchStrategy(BaseSearchStrategy $strategy)
    {
        $this->quickSearchStrategy = $strategy;
    }

    public function getQuickSearchStrategy()
    {
        return $this->quickSearchStrategy;
    }

    public function setAdvancedSearchStrategy(BaseSearchStrategy $strategy)
    {
        $this->advancedSearchStrategy = $strategy;
    }

    public function getAdvancedSearchStrategy()
    {
        return $this->advancedSearchStrategy;
    }

    public function getDomain():string
    {
        return $this->entityManager->getClassMetadata($this->entityName)->getName();
    }

    protected function updateEntityBeforeCreate($entity, array $data)
    {
        if(method_exists($entity, 'setCreatedBy') && php_sapi_name() !== 'cli')
        {
            $token = $this->container->get('security.token_storage')->getToken();
            $entity->setCreatedBy($token->getUser());
        }
    }

    protected function updateEntityBeforeUpdate($entity, array $data)
    {
        if(method_exists($entity, 'setLastEditedBy'))
        {
            $token = $this->container->get('security.token_storage')->getToken();
            $entity->setLastEditedBy($token->getUser());
        }

        if(method_exists($entity, 'setLastEditedDatetime'))
        {
            $entity->setLastEditedDatetime(new \DateTime());
        }
    }

    public function create(array $data, bool $checkPermissions = true)
    {
        $entity = $this->getValidatedEntityForNewRecord($data);

        if($checkPermissions)
        {
            $this->checkCreatePermission($this->getDomain());
        }

        $this->updateEntityBeforeCreate($entity, $data);

        $this->entityManager->persist($entity);
        $this->entityManager->flush();

        if(php_sapi_name() !== 'cli')
        {
            $this->assignOwnerToEntity($entity);
        }

        return $entity;
    }

    public function getValidatedEntityForNewRecord(array $data)
    {
        $form = $this->formFactory->create($this->getFormTypeClass());

        // fill the form with data
        $form->submit($data);
        $entity = $form->getData();

        if($form->isValid())
        {
            return $entity;
        }
        else
        {
            $exception = new InvalidFormDataException("Form data is invalid", 400);
            $exception->setForm($form);
            $exception->setErrorMessages($form->all());

            throw $exception;
        }
    }

    public function get($id)
    {
        $entity = $this->entityManager->find($this->entityName, $id);

        if($entity)
        {
            $this->checkViewPermission($this->getDomain());
        }

        return $entity;
    }
    
    public function getPagedData(PageParameters $pageParameters, BaseSearchStrategy $searchStrategy = NULL)
    {
        $this->checkViewPermission($this->getDomain());

        return $this->getPageGenerator()->getPagedData(
            $this->entityManager->getRepository($this->entityName),
            $pageParameters,
            null,
            $searchStrategy
        );
    }

    public function update($id, array $data, bool $checkPermissions = true)
    {
        return $this->save($id, $data, $checkPermissions);
    }

    public function patch($id, array $data, bool $checkPermissions = true)
    {
        return $this->save($id, $data, true, $checkPermissions);
    }

    protected function save($id, array $data, bool $isPatch = false, bool $checkPermissions = true)
    {
        $entity = $this->getValidatedEntityForEditRecord($id, $data, $isPatch);

        if($checkPermissions)
        {
            $this->checkEditPermission($this->getDomain());
        }

        $this->updateEntityBeforeUpdate($entity, $data);
        $this->entityManager->flush();

        return $entity;
    }

    public function getValidatedEntityForEditRecord($id, array $data, $isPatch = false)
    {
        $entity = $this->get($id);

        if($entity)
        {
            $form = $this->formFactory->create($this->getFormTypeClass(), $entity);

            // fill the form from the request data
            // http://api.symfony.com/3.0/Symfony/Component/Form/Form.html#method_submit
            $form->submit($data, !$isPatch);

            if($form->isValid())
            {
                return $entity;
            }
            else
            {
                $exception = new InvalidFormDataException("Form data is invalid", 400);
                $exception->setForm($form);
                $exception->setErrorMessages($form->all());

                throw $exception;
            }
        }
        else
        {
            throw new \Exception("Record is not found", 404);
        }
    }

    public function readForSelect(array $queryParameters = null)
    {
        $this->checkViewPermission($this->getDomain());

        if($queryParameters !== null AND count($queryParameters) > 0)
        {
            return $this->entityManager->getRepository($this->entityName)->findBy($queryParameters);
        }
        else
        {
            return $this->entityManager->getRepository($this->entityName)->findAll();
        }
    }

    public function readForAutoComplete(string $queryParameter, array $filterParameters)
    {
        $this->checkViewPermission($this->getDomain());

        return $this->entityManager->getRepository($this->entityName)->searchForAutoComplete($queryParameter, ['name'], $filterParameters);
    }

    public function readAll(array $searchParameters = [], int $limit = -1)
    {
        $this->checkViewPermission($this->getDomain());

        $methodName = 'findAll';
        $isSearchParameters = count($searchParameters) > 0;

        if($isSearchParameters)
        {
            if($limit === 1)
            {
                $data = $this->entityManager->getRepository($this->entityName)->findOneBy($searchParameters);
            }
            else if($limit > 1)
            {
                $data = $this->entityManager->getRepository($this->entityName)->findBy($searchParameters, null, $limit);
            }
            else
            {
                $data = $this->entityManager->getRepository($this->entityName)->findBy($searchParameters);
            }
        }
        else if($limit > 0)
        {
            $data = $this->entityManager->getRepository($this->entityName)->findBy(null, null, $limit);
        }
        else 
        {
            $data = $this->entityManager
                    ->getRepository($this->entityName)
                    ->findAll();
        }

        return $data;
    }

    public function count(array $searchParameters = [])
    {
        return $this->entityManager->getRepository($this->entityName)->count($searchParameters);
    }

    public function quickSearch(PageParameters $pageData)
    {
        $this->checkViewPermission($this->getDomain());

        return $this->getPagedData($pageData, $this->getQuickSearchStrategy());
    }

    public function delete($id)
    {
        $entity = $this->entityManager
                        ->getRepository($this->entityName)
                        ->find($id);
        

        $this->entityManager->remove($entity);
        $this->entityManager->flush();

        return $entity;
    }

    protected function assignOwnerToEntity($entity)
    {
         // creating the ACL
        $aclProvider = $this->container->get('security.acl.provider');
        $objectIdentity = ObjectIdentity::fromDomainObject($entity);
        $acl = $aclProvider->createAcl($objectIdentity);

        $securityIdentity = UserSecurityIdentity::fromAccount($this->getLoggedInUser());

        // grant owner access
        $acl->insertObjectAce($securityIdentity, MaskBuilder::MASK_OWNER);
        $aclProvider->updateAcl($acl);
    }

    protected function checkCreatePermission(string $domain):bool
    {
        //if invokation is from cli skip checking
        if(php_sapi_name() === 'cli')
        {
            return true;
        }

        $permissions = $this->getLoggedInUserPermissions($domain);

        if(count($permissions) > 0 AND isset($permissions['actions']) AND count($permissions['actions']) > 0 AND in_array('create', $permissions['actions']))
        {
            return true;
        }
        else
        {
            $message = "You do not have access to create this entity";

            if(isset($permissions['name']))
            {
                $message .= ': ' . $permissions['name'];
            }

            throw new InsufficientPermissionsException($message);
        }
    }

    protected function checkEditPermission(string $domain):bool
    {
        //if invokation is from cli skip checking
        if(php_sapi_name() === 'cli')
        {
            return true;
        }

        $permissions = $this->getLoggedInUserPermissions($domain);

        if(count($permissions) > 0 AND isset($permissions['actions']) AND count($permissions['actions']) > 0 AND in_array('edit', $permissions['actions']))
        {
            return true;
        }
        else
        {
            $message = "You do not have access to edit this entity";

            if(isset($permissions['name']))
            {
                $message .= ': ' . $permissions['name'];
            }

            throw new InsufficientPermissionsException($message);
        }
    }

    protected function checkViewPermission(string $domain):bool
    {
        //if invokation is from cli skip checking
        if(php_sapi_name() === 'cli')
        {
            return true;
        }

        $permissions = $this->getLoggedInUserPermissions($domain);

        if(count($permissions) > 0 AND isset($permissions['actions']) AND count($permissions['actions']) > 0 AND in_array('view', $permissions['actions']))
        {
            return true;
        }
        else
        {
            $message = "You do not have access to view this entity";

            if(isset($permissions['name']))
            {
                $message .= ': ' . $permissions['name'];
            }

            throw new InsufficientPermissionsException($message);
        }
    }

    public function getLoggedInUserPermissions(string $className):array
    {
        $permissions = $this->container->get('session')->get('user_permissions');

        if(is_array($permissions) AND count($permissions) > 0)
        {
            foreach($permissions AS $domainPermission)
            {
                if($className === $domainPermission['domain'])
                {
                    return $domainPermission;
                }
            }

            return [];
        }
        else
        {
            return [];
        }
    }
}
