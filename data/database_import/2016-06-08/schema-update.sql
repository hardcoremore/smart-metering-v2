/*Table structure changes for table `customers` */
ALTER TABLE `customers` MODIFY `name` VARCHAR(128) NOT NULL, MODIFY `customerType` VARCHAR(32) DEFAULT NULL;


/*Table structure changes for table `distribution_transformers` */

ALTER TABLE `distribution_transformers` 
ADD COLUMN `rating` DOUBLE DEFAULT NULL,
ADD COLUMN `type` ENUM('PRIVATE','PUBLIC') NOT NULL DEFAULT 'PRIVATE',
ADD COLUMN `location` VARCHAR(512) DEFAULT NULL;


