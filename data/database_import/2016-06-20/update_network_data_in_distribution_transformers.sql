UPDATE distribution_transformers SET injection_substation_id = 76 WHERE power_transformer_id = 200 OR power_transformer_id = 201 OR power_transformer_id = 202;
UPDATE distribution_transformers SET injection_substation_id = 78 WHERE power_transformer_id = 203 OR power_transformer_id = 204 OR power_transformer_id = 205;
UPDATE distribution_transformers SET injection_substation_id = 79 WHERE power_transformer_id = 206 OR power_transformer_id = 207;
UPDATE distribution_transformers SET injection_substation_id = 79 WHERE power_transformer_id = 206 OR power_transformer_id = 207;
UPDATE distribution_transformers SET injection_substation_id = 80 WHERE power_transformer_id = 208 OR power_transformer_id = 209 OR power_transformer_id = 210;
UPDATE distribution_transformers SET injection_substation_id = 81 WHERE power_transformer_id = 211;
UPDATE distribution_transformers SET injection_substation_id = 82 WHERE power_transformer_id = 212 OR power_transformer_id = 213;
UPDATE distribution_transformers SET injection_substation_id = 83 WHERE power_transformer_id = 214 OR power_transformer_id = 215 OR power_transformer_id = 216;
UPDATE distribution_transformers SET injection_substation_id = 84 WHERE power_transformer_id = 217 OR power_transformer_id = 218 OR power_transformer_id = 219;
UPDATE distribution_transformers SET injection_substation_id = 85 WHERE power_transformer_id = 220 OR power_transformer_id = 221;
UPDATE distribution_transformers SET injection_substation_id = 86 WHERE power_transformer_id = 223 OR power_transformer_id = 224;
UPDATE distribution_transformers SET injection_substation_id = 89 WHERE power_transformer_id = 230;
UPDATE distribution_transformers SET injection_substation_id = 90 WHERE power_transformer_id = 231 OR power_transformer_id = 232;
UPDATE distribution_transformers SET injection_substation_id = 91 WHERE power_transformer_id = 233;
UPDATE distribution_transformers SET injection_substation_id = 92 WHERE power_transformer_id = 235 OR power_transformer_id = 236;
UPDATE distribution_transformers SET injection_substation_id = 93 WHERE power_transformer_id = 237 OR power_transformer_id = 236;
UPDATE distribution_transformers SET injection_substation_id = 94 WHERE power_transformer_id = 241 OR power_transformer_id = 242;
UPDATE distribution_transformers SET injection_substation_id = 95 WHERE power_transformer_id = 243 OR power_transformer_id = 244;
UPDATE distribution_transformers SET injection_substation_id = 96 WHERE power_transformer_id = 245 OR power_transformer_id = 246;
UPDATE distribution_transformers SET injection_substation_id = 96 WHERE power_transformer_id = 245 OR power_transformer_id = 246;
UPDATE distribution_transformers SET injection_substation_id = 97 WHERE power_transformer_id = 247 OR power_transformer_id = 248 OR power_transformer_id = 249;
UPDATE distribution_transformers SET injection_substation_id = 100 WHERE power_transformer_id = 256;
UPDATE distribution_transformers SET injection_substation_id = 108 WHERE power_transformer_id = 272 OR power_transformer_id = 274;


UPDATE distribution_transformers
SET business_district_id = 15 
WHERE injection_substation_id = 76 OR 
      injection_substation_id = 78 OR
      injection_substation_id = 79 OR
      injection_substation_id = 80 OR
      injection_substation_id = 81 OR
      injection_substation_id = 82 OR
      injection_substation_id = 83 OR
      injection_substation_id = 84 OR
      injection_substation_id = 85 OR
      injection_substation_id = 86;

UPDATE distribution_transformers
SET business_district_id = 16
WHERE injection_substation_id = 89 OR 
      injection_substation_id = 90 OR
      injection_substation_id = 91;

UPDATE distribution_transformers
SET business_district_id = 17 
WHERE injection_substation_id = 92 OR 
      injection_substation_id = 93 OR
      injection_substation_id = 94 OR
      injection_substation_id = 95 OR
      injection_substation_id = 96 OR
      injection_substation_id = 97 OR
      injection_substation_id = 100;

UPDATE distribution_transformers
SET business_district_id = 21
WHERE injection_substation_id = 108;

UPDATE distribution_transformers SET status = 'live';